package com.zoundindustries.marshallbt.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.squareup.leakcanary.RefWatcher;
import com.tym.tymappplatform.TAProtocol.TAProtocol.TAProtocol;
import com.tym.tymappplatform.TAService.TADigitalSignalProcessingService.TADigitalSignalProcessingService;
import com.tym.tymappplatform.TAService.TAOtaSignalDataProcessingService.TAOtaSignalDataProcessingService;
import com.tym.tymappplatform.TAService.TAPlayControlService.TAPlayControlService;
import com.tym.tymappplatform.TAService.TASystemService.TASystemService;
import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.model.adapters.mock.MockSDK;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.BaseDevice.ConnectionState;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.state.TymphanyDeviceStateController;
import com.zoundindustries.marshallbt.model.device.tymphany.SpeakerInfo;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyResourceProvider;
import com.zoundindustries.marshallbt.model.devicesettings.EQData;
import com.zoundindustries.marshallbt.model.devicesettings.EQPreset;
import com.zoundindustries.marshallbt.model.devicesettings.ICouplingService;
import com.zoundindustries.marshallbt.model.discovery.IDiscoveryService;
import com.zoundindustries.marshallbt.model.discovery.adapter.joplin.TymphanyDeviceDiscoveryAdapter;
import com.zoundindustries.marshallbt.model.discovery.adapter.mock.MockDeviceDiscoveryAdapter;
import com.zoundindustries.marshallbt.model.ota.IOTAService;
import com.zoundindustries.marshallbt.model.ota.SpeakerCacheManager;
import com.zoundindustries.marshallbt.model.ota.adapter.joplin.TymphanyOTAAdapter;
import com.zoundindustries.marshallbt.model.ota.adapter.mock.MockOTAAdapter;
import com.zoundindustries.marshallbt.model.ota.pojo.FirmwareFileMetaData;
import com.zoundindustries.marshallbt.model.ota.pojo.SpeakerInfoCache;
import com.zoundindustries.marshallbt.ui.onboarding.BaseOnboardingContainerFragment;
import com.zoundindustries.marshallbt.ui.onboarding.JoplinLiteOnboardingContainerFragment;
import com.zoundindustries.marshallbt.ui.onboarding.OzzyOnboardingContainerFragment;
import com.zoundindustries.marshallbt.utils.BluetoothSystemUtils;
import com.zoundindustries.marshallbt.utils.EqPresetType;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;
import com.zoundindustries.marshallbt.utils.FirmwareFileHelper;
import com.zoundindustries.marshallbt.utils.joplin.JoplinEQPresetListManager;
import com.zoundindustries.marshallbt.utils.joplin.SpeakerColorUtils;
import com.zoundindustries.marshallbt.utils.ozzy.OzzyEQPresetListManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import retrofit2.Response;

/**
 * DeviceService is handling devices discovery, devices list updates and SDKs configuration.
 */
public class DeviceService extends Service implements IDiscoveryService,
        IOTAService,
        ICouplingService {

    private static final String JOPLIN_CONFIG_FILE = "QCC3008.json";
    private static final int CONNECTION_OBSERVER_TIMEOUT_IN_SEC = 60;
    private static final int PERIODIC_SCAN_INTERVAL_IN_MS = 30000;
    private static final int PERIODIC_SCAN_TIME_IN_MS = 5000;

    private final IBinder binder = new LocalBinder();
    private List<BaseDevice> homeDevices;

    private final BehaviorSubject<Boolean> serviceReadySubject = BehaviorSubject.create();
    private final BehaviorSubject<Boolean> taServiceReadySubject = BehaviorSubject.create();
    private final BehaviorSubject<Boolean> colorJsonReadySubject = BehaviorSubject.create();
    private final BehaviorSubject<String> dividerIdChangedSubject = BehaviorSubject.create();
    private final PublishSubject<BaseDevice> deviceAddedSubject = PublishSubject.create();
    private final PublishSubject<BaseDevice> deviceRemovedSubject = PublishSubject.create();
    private PublishSubject<BaseDevice> coupledDevicesSubject;

    private Map<String, BehaviorSubject<Boolean>> otaIndicatorVisibileMap;

    private MockDeviceDiscoveryAdapter mockScanForDevicesAdapter;
    private TymphanyDeviceDiscoveryAdapter tymhpanyDeviceDiscoveryAdapter;

    private boolean isCouplingScanInProgress;
    private boolean isDeviceScanInProgress;
    private boolean isScanAllowed = true;
    private TASystemService taSystemService;
    private TADigitalSignalProcessingService taDspService;
    private TAPlayControlService taPlayControlService;
    private TAOtaSignalDataProcessingService taOtaSignalDataProcessingService;

    private MockOTAAdapter mockOTAAdapter;
    private TymphanyOTAAdapter joplinOTAAdapter;

    private BluetoothSystemUtils bluetoothSystemUtils;
    private SpeakerCacheManager speakerCacheManager;
    private JoplinEQPresetListManager joplinEqPresetListManager;
    private OzzyEQPresetListManager ozzyEqPresetListManager;

    private Disposable mockScanDisposable;
    private Disposable joplinDiscoveredDevicesDisposable;
    private ArrayList<Disposable> couplingDisposables;
    private ArrayList<Disposable> otaCheckConnectionDisposables;
    private Handler handler;

    private Disposable joplinDisappearedDevicesDisposable;
    private Disposable reconnectDisposable;
    private Disposable joplinReconnectDisposable;
    private Disposable serviceReadyDisposable;
    private Disposable joplinOtaStateDisposable;
    private Disposable initServiceDisposable;

    /**
     * enum that declare possible sizes of Joplin speaker image used around the application
     */
    public enum DeviceImageType {
        SMALL,
        MEDIUM,
        LARGE,
        ONBOARDING
    }

    @Override
    public void onCreate() {
        super.onCreate();
        speakerCacheManager = new SpeakerCacheManager(this);
        joplinEqPresetListManager = new JoplinEQPresetListManager(this);
        ozzyEqPresetListManager = new OzzyEQPresetListManager(this);
        initTAServices(this);
        initDeviceColorJsonObjects(this);
        MockSDK mockSDK = ((BluetoothApplication) getApplication()).getAppComponent().mockSDK();
        mockScanForDevicesAdapter = new MockDeviceDiscoveryAdapter(mockSDK);
        homeDevices = Collections.synchronizedList(new ArrayList<>());
        couplingDisposables = new ArrayList<>();
        otaCheckConnectionDisposables = new ArrayList<>();
        bluetoothSystemUtils = new BluetoothSystemUtils();
        otaIndicatorVisibileMap = new HashMap<>();
        handler = new Handler();
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacksAndMessages(null);

        if (mockScanDisposable != null) {
            mockScanDisposable.dispose();
        }

        if (joplinDiscoveredDevicesDisposable != null) {
            joplinDiscoveredDevicesDisposable.dispose();
        }


        if (joplinDisappearedDevicesDisposable != null) {
            joplinDisappearedDevicesDisposable.dispose();
        }

        if (reconnectDisposable != null) {
            reconnectDisposable.dispose();
        }

        if (joplinReconnectDisposable != null) {
            joplinReconnectDisposable.dispose();
        }

        if (serviceReadyDisposable != null) {
            serviceReadyDisposable.dispose();
        }

        if (joplinOtaStateDisposable != null) {
            joplinOtaStateDisposable.dispose();
        }

        if (initServiceDisposable != null) {
            initServiceDisposable.dispose();
        }

        clearCouplingDisposables();
        clearOtaCheckConnectionDisposables();

        mockOTAAdapter.dispose();
        joplinOTAAdapter.dispose();

        tymhpanyDeviceDiscoveryAdapter.dispose();

        for (BaseDevice device : homeDevices) {
            device.dispose();
        }

        clearCouplingDisposables();

        super.onDestroy();

        RefWatcher refWatcher = BluetoothApplication.getRefWatcher(this);
        refWatcher.watch(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void startScanForDevices() {
        if (tymhpanyDeviceDiscoveryAdapter != null && isScanAllowed && !isReconnectInProgress()) {
            isDeviceScanInProgress = true;
            tymhpanyDeviceDiscoveryAdapter.startScanForDevices();
        }
    }

    @Override
    public void stopScanForDevices() {
        isDeviceScanInProgress = false;
        tymhpanyDeviceDiscoveryAdapter.stopScanForDevices();
    }

    @NonNull
    @Override
    public Observable<BaseDevice> getDiscoveredDevices() {
        return deviceAddedSubject;
    }

    @NonNull
    @Override
    public Observable<BaseDevice> getDisappearedDevices() {
        return deviceRemovedSubject;
    }

    /**
     * Get {@link BaseDevice} object for specific id.
     *
     * @param id to be searched for.
     * @return {@link BaseDevice} or null if not found.
     */
    @Nullable
    public synchronized BaseDevice getDeviceFromId(@NonNull String id) {
        for (BaseDevice device : homeDevices) {
            if (device.getDeviceInfo().getId().equals(id)) {
                return device;
            }
        }
        return null;
    }

    @Nullable
    @Override
    public Observable<Double> getFirmwareUpdateProgress(@NonNull BaseDevice device) {

        switch (device.getDeviceInfo().getDeviceType()) {
            case MOCK:
                return mockOTAAdapter.getFirmwareUpdateProgress(device);
            case TYMPHANY_DEVICE:
                return joplinOTAAdapter.getFirmwareUpdateProgress(device);
        }
        return null;
    }

    @Nullable
    @Override
    public Observable<IOTAService.UpdateState>
    getFirmwareUpdateState(@NonNull BaseDevice device) {
        switch (device.getDeviceInfo().getDeviceType()) {
            case MOCK:
                return mockOTAAdapter.getFirmwareUpdateState(device);
            case TYMPHANY_DEVICE:
                return joplinOTAAdapter.getFirmwareUpdateState(device);
        }
        return null;
    }

    @Nullable
    @Override
    public Observable<Boolean> getFirmwareUpdateAvailability(@NonNull BaseDevice device) {
        return otaIndicatorVisibileMap.get(device.getBaseDeviceStateController()
                .outputs
                .getSpeakerInfoCurrentValue()
                .getMacAddress());
    }

    @Nullable
    @Override
    public BehaviorSubject<Response<FirmwareFileMetaData>>
    checkForFirmwareUpdate(@NonNull String deviceId, boolean isForced) {
        BaseDevice device = getDeviceFromId(deviceId);
        if (device != null) {
            switch (device.getDeviceInfo().getDeviceType()) {
                case MOCK:
                    return null;
                case TYMPHANY_DEVICE:
                    boolean forced = isForced;
                    TymphanyDevice tymphanyDevice = (TymphanyDevice) device;
                    if (shouldDisableForcedOTA(tymphanyDevice)) {
                        forced = false;
                    }
                    return joplinOTAAdapter.checkForFirmwareUpdate(device, forced);
            }
        }
        return null;
    }

    public boolean shouldDisableForcedOTA(TymphanyDevice tymphanyDevice) {
        return tymphanyDevice.getDeviceSubType() == TymphanyDevice.DeviceSubType.OZZY ||
                tymphanyDevice.isLiteDevice();
    }

    @Override
    public void startOTAUpdate(@NonNull BaseDevice device,
                               @NonNull FirmwareFileMetaData response) {
        switch (device.getDeviceInfo().getDeviceType()) {
            case MOCK:
                mockOTAAdapter.startOTAUpdate(device, response);
                break;
            case TYMPHANY_DEVICE:
                joplinOTAAdapter.startOTAUpdate(device, response);
                break;
        }
    }

    @NonNull
    @Override
    public synchronized Observable<BaseDevice>
    startScanForReadyToCoupleDevices(@NonNull BaseDevice device) {
        if (!isCouplingScanInProgress) {
            coupledDevicesSubject = PublishSubject.create();
        }
        couplingDisposables = new ArrayList<>();
        isCouplingScanInProgress = true;

        ListIterator<BaseDevice> iterator = homeDevices.listIterator();
        getCouplingConnectionInfo(device, iterator);

        return coupledDevicesSubject;
    }

    @Override
    public synchronized void stopGetDevicesReadyToCouple() {
        isCouplingScanInProgress = false;
    }

    @Override
    public boolean isOTAInProgress() {
        //todo This should be extended with device parameter for detailed check.
        return joplinOTAAdapter.isOTAInProgress();
    }

    @Override
    public void finalizeForcedOTA() {
        joplinOTAAdapter.finalizeForcedOTA();
    }

    /**
     * Get observable for service availability.
     * Client should wait for service to be ready and perform any further calls on service
     * after getting true value from this observable.
     * <p>
     * Service will initialize all dependent services in the background.
     *
     * @return true when service is ready to handle incoming requests, false if initialization
     * failed.
     */
    public Observable<Boolean> isServiceReady() {
        return serviceReadySubject;
    }

    /**
     * Get observable for the availability of the Json objects, which ones contain the MAC addresses
     * of the white speakers
     *
     * @return true when the color Json objects created from file
     */
    public Observable<Boolean> isColorJsonReady() {
        return colorJsonReadySubject;
    }

    /**
     * Get currently discovered devices. Used to allow clients to get snapshot of the current device
     * list and register for further changes.
     *
     * @return List with BaseDevices.
     */
    public List<BaseDevice> getCurrentDevices() {
        return homeDevices;
    }

    /**
     * Disconnect all currently connected devices.
     */
    public synchronized void disconnectConnectedDevices() {
        for (BaseDevice device : homeDevices) {
            if (device.getBaseDeviceStateController().outputs.getConnectionInfoCurrentValue() ==
                    ConnectionState.CONNECTED) {
                device.getBaseDeviceStateController().inputs
                        .setConnectionInfo(ConnectionState.DISCONNECTED);
            }
        }
    }

    /**
     * Remove the {@link BaseDevice} from the {@link DeviceService}.
     */
    public void removeDevice(@NonNull BaseDevice device) {
        homeDevices.remove(device);
        tymhpanyDeviceDiscoveryAdapter.removeDevice(device);
        deviceRemovedSubject.onNext(device);
    }

    /**
     * Returns flag indicating if there is ongoing device scan in the system.
     *
     * @return scanning state.
     */
    public boolean isDeviceScanInProgress() {
        return isDeviceScanInProgress;
    }

    /**
     * Get {@link io.reactivex.Observable} for changed Id of the last connected device.
     * This indicates which device should show the divider view.
     *
     * @return observable on the current divider device.
     */
    @NonNull
    public Observable<String> getDividerDeviceId() {
        return dividerIdChangedSubject;
    }

    /**
     * Set currently last connected device to show it with the divider view.
     *
     * @param deviceId to be set as last connected.
     */
    public void setDividerDeviceId(@NonNull String deviceId) {
        dividerIdChangedSubject.onNext(deviceId);
    }

    /**
     * Binder class to expose service to clients.
     */
    public class LocalBinder extends Binder {
        public DeviceService getService() {
            return DeviceService.this;
        }
    }

    /**
     * Returns image resource for provided device ID
     *
     * @param deviceId  id of device for which image should be provided
     * @param imageSize image size that should be returned
     * @return image resource ID
     */
    public int getImageResourceId(@NonNull String deviceId,
                                  @NonNull DeviceImageType imageSize) {
        BaseDevice device = getDeviceFromId(deviceId);

        if (device == null) {

            return 0;
        }

        DeviceInfo.DeviceType deviceType = device.getDeviceInfo().getDeviceType();
        switch (deviceType) {
            case MOCK:
                return 0;
            case TYMPHANY_DEVICE:
                return TymphanyResourceProvider.getImageResourceId(device, imageSize);
            default:
                return 0;
        }
    }

    public BaseOnboardingContainerFragment getOnboardingFragment(String deviceId) {
        BaseDevice device = getDeviceFromId(deviceId);

        if (device == null) {
            return null;
        }
        if(device.getDeviceInfo().getDeviceType() == DeviceInfo.DeviceType.TYMPHANY_DEVICE) {
            switch(((TymphanyDevice) device).getDeviceSubType()) {

                case JOPLIN_S:
                case JOPLIN_M:
                case JOPLIN_L:
                    return null;
                case JOPLIN_S_LITE:
                case JOPLIN_M_LITE:
                case JOPLIN_L_LITE:
                    return new JoplinLiteOnboardingContainerFragment();
                case OZZY:
                    return new OzzyOnboardingContainerFragment();
            }
        }
        return  null;
    }

    /**
     * Method used for adding new {@link SpeakerInfoCache} to storage.
     *
     * @param macAddress of speaker that should be added to storage
     * @return true in case when adding had succeed, otherwise false
     */
    public boolean saveSpeakerInfoCache(@NonNull String macAddress) {
        return speakerCacheManager.addSpeakerInfoCache(macAddress);
    }

    /**
     * Method used for removing {@link SpeakerInfoCache} from storage.
     *
     * @param macAddress of speaker that should be removed from storage
     * @return true in case when removing had succeed, otherwise false
     */
    public boolean removeSpeakerInfoCache(@NonNull String macAddress) {
        return speakerCacheManager.removeSpeakerInfoCache(macAddress);
    }

    /**
     * Method used for updating stored {@link SpeakerInfoCache} information
     *
     * @param macAddress        of updated Speaker
     * @param isTwsCoupled      status of speaker
     * @param twsChannelType    might be null
     * @param twsConnectionInfo might be null
     */
    public void updateSpeakerInfoCache(@NonNull String macAddress,
                                       boolean isTwsCoupled,
                                       @Nullable String twsChannelType,
                                       @Nullable String twsConnectionInfo) {
        speakerCacheManager.updateTwsStatusOfSpeaker(macAddress, isTwsCoupled,
                twsChannelType, twsConnectionInfo);
    }

    /**
     * Method updating isConfigured information in device cache. Not yet configured devices will
     * invoke additional UI onboarding for the user.
     *
     * @param macAddress   of the device to be updated
     */
    public void setDeviceConfigured(@NonNull String macAddress) {
        speakerCacheManager.updateIsConfigured(macAddress, true);

    }


    /**
     * Method used for retrieving {@link SpeakerInfoCache} from storage based on MAC Address
     *
     * @param macAddress of speaker
     * @return {@link SpeakerInfoCache} in case when speaker exist otherwise null
     */
    @Nullable
    public SpeakerInfoCache getSpeakerInfoCache(@NonNull String macAddress) {
        return speakerCacheManager.getSpeakerInfoCache(macAddress);
    }

    /**
     * Method used for retrieving boolean based on if {@link SpeakerInfoCache} exist
     *
     * @param macAddress of speaker
     * @return true in case when {@link SpeakerInfoCache}  exist otherwise false
     */
    public boolean isSpeakerInfoCached(@NonNull String macAddress) {
        return speakerCacheManager.isSpeakerInfoCached(macAddress);
    }

    /**
     * Method used to update the name of a device
     *
     * @param id      the id of the device
     * @param newName the new name of the device
     */
    public void updateDeviceName(@NonNull String id, @NonNull String newName) {
        BaseDevice device = getDeviceFromId(id);
        if (device != null) {
            SpeakerInfo speakerInfo = device.getBaseDeviceStateController().outputs
                    .getSpeakerInfoCurrentValue();
            speakerInfo.setDeviceName(newName);
            device.getBaseDeviceStateController().inputs.setSpeakerInfo(speakerInfo);
        }
    }

    /**
     * Get the list of the equalizer preset types
     *
     * @return the list of presets for the specific device type or null if type doesn't match.
     */
    @Nullable
    public List<EqPresetType> getEqualizerPresetTypes(@NonNull BaseDevice device) {
        if (device.getDeviceInfo().getDeviceType() == DeviceInfo.DeviceType.TYMPHANY_DEVICE) {
            if (((TymphanyDevice) device).isJoplinDevice()) {
                return joplinEqPresetListManager.getPresetTypes();
            } else {
                return ozzyEqPresetListManager.getPresetTypes();
            }
        }
        return null;
    }

    /**
     * Get the preset with the chosen index
     *
     * @param device to distinguish different preset providers
     * @param index  the index of the chosen preset
     * @return the preset
     */
    @Nullable
    public EQPreset getEqualizerPresetByIndex(@NonNull BaseDevice device, int index) {
        if (device.getDeviceInfo().getDeviceType() == DeviceInfo.DeviceType.TYMPHANY_DEVICE) {
            if (((TymphanyDevice) device).isJoplinDevice()) {
                return joplinEqPresetListManager.getPresetByIndex(index);
            } else {
                return ozzyEqPresetListManager.getPresetByIndex(index);
            }
        }
        return null;
    }

    /**
     * Save the equalizer values for the custom preset
     *
     * @param device                to distinguish different preset providers
     * @param customEqualizerPreset the custom preset
     */
    public void saveCustomEqualizerPreset(@NonNull BaseDevice device,
                                          @NonNull EQPreset customEqualizerPreset) {
        if (device.getDeviceInfo().getDeviceType() == DeviceInfo.DeviceType.TYMPHANY_DEVICE) {
            if (device.getDeviceInfo().getDeviceType() == DeviceInfo.DeviceType.TYMPHANY_DEVICE) {
                if (((TymphanyDevice) device).isJoplinDevice()) {
                    joplinEqPresetListManager.saveCustomPreset(customEqualizerPreset);
                } else {
                    ozzyEqPresetListManager.saveCustomPreset(customEqualizerPreset);
                }
            }
        }
    }

    /**
     * Get the index of the given preset
     *
     * @param device          to distinguish different preset providers
     * @param equalizerPreset the preset which ones index is needed
     * @return the index of the preset or custom if not found matching provider.
     */
    public int getIndexOfEqualizerPreset(@NonNull BaseDevice device,
                                         @NonNull EQPreset equalizerPreset) {
        if (device.getDeviceInfo().getDeviceType() == DeviceInfo.DeviceType.TYMPHANY_DEVICE) {
            if (device.getDeviceInfo().getDeviceType() == DeviceInfo.DeviceType.TYMPHANY_DEVICE) {
                if (((TymphanyDevice) device).isJoplinDevice()) {
                    return joplinEqPresetListManager.getIndexOfPreset(equalizerPreset);
                } else {
                    return ozzyEqPresetListManager.getIndexOfPreset(equalizerPreset);
                }
            }
        }
        return 0;
    }

    /**
     * Get the type for the given preset values
     *
     * @param device to distinguish different preset providers
     * @param eqData the preset values
     * @return the type of the preset
     */
    @Nullable
    public EqPresetType getEqualizerPresetType(@NonNull BaseDevice device,
                                               @NonNull EQData eqData) {
        if (device.getDeviceInfo().getDeviceType() == DeviceInfo.DeviceType.TYMPHANY_DEVICE) {
            if (device.getDeviceInfo().getDeviceType() == DeviceInfo.DeviceType.TYMPHANY_DEVICE) {
                if (((TymphanyDevice) device).isJoplinDevice()) {
                    return joplinEqPresetListManager.getEqualizerPresetType(eqData);
                } else {
                    return ozzyEqPresetListManager.getEqualizerPresetType(eqData);
                }
            }
        }
        return null;
    }

    /**
     * Start periodic scan. It will continue until the service object is alive.
     * Periodic scan is turned off when OTA is running.
     */
    public void startPeriodicScan() {
        handler.postDelayed(() -> {
            new ScanRunnable().run();
            startPeriodicScan();
        }, PERIODIC_SCAN_INTERVAL_IN_MS);
    }

    /**
     * Control scanning from UI flow. Allows or disallows scanning for certain UX cases.
     *
     * @param isAllowed toggle flag.
     */
    public void setScanAllowed(boolean isAllowed) {
        isScanAllowed = isAllowed;
    }

    private void reconnectToDevice(@NonNull String deviceId) {
        if (tymhpanyDeviceDiscoveryAdapter == null) {
            return;
        }
        BaseDevice device = getDeviceFromId(deviceId);

        if (device == null) {
            return;
        }

        if (!isDeviceScanInProgress) {
            startScanForDevices();
        }

        reconnectDisposable = device.getBaseDeviceStateController().outputs.getConnectionInfo()
                .observeOn(AndroidSchedulers.mainThread())
                .timeout(CONNECTION_OBSERVER_TIMEOUT_IN_SEC, TimeUnit.SECONDS)
                .takeUntil(connectionState -> connectionState == ConnectionState.CONNECTING)
                .subscribe(connectionState -> {
                    stopScanIfOtaFinished(device);
                    stopScanOnDeviceConnecting(connectionState);
                }, throwable -> stopScanForDevices());
    }

    private void stopScanOnDeviceConnecting(@NonNull ConnectionState connectionState) {
        if (connectionState == ConnectionState.CONNECTING && isDeviceScanInProgress) {
            stopScanForDevices();
            joplinOtaStateDisposable.dispose();
        }
    }

    private void stopScanIfOtaFinished(@NonNull BaseDevice device) {
        joplinOtaStateDisposable = joplinOTAAdapter.getFirmwareUpdateState(device)
                .subscribe(throwable -> {
                }, throwable -> {
                    if (isDeviceScanInProgress) {
                        stopScanForDevices();

                        if (joplinOtaStateDisposable != null) {
                            joplinOtaStateDisposable.dispose();
                        }
                    }
                }, () -> {
                    if (isDeviceScanInProgress) {
                        stopScanForDevices();
                        if (joplinOtaStateDisposable != null) {
                            joplinOtaStateDisposable.dispose();
                        }
                    }
                });
    }

    private void initObservers() {
        joplinDiscoveredDevicesDisposable = tymhpanyDeviceDiscoveryAdapter.getDisappearedDevices()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(device -> {
                    homeDevices.remove(device);
                    deviceRemovedSubject.onNext(device);
                });

        joplinDisappearedDevicesDisposable = tymhpanyDeviceDiscoveryAdapter.getDiscoveredDevices()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleDiscoveredDevice);

        joplinReconnectDisposable = joplinOTAAdapter.getReconnectDevice()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(device -> reconnectToDevice(device.getDeviceInfo().getId()));

        serviceReadyDisposable = Observable.combineLatest(
                taServiceReadySubject,
                colorJsonReadySubject,
                (isTaServiceReady, isColorJsonReady) -> isTaServiceReady && isColorJsonReady)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(serviceReadySubject::onNext);
    }

    private synchronized void handleDiscoveredDevice(@NonNull BaseDevice device) {
        if (!handleTwsConnectedDevice(device)) {
            handleDisconnectedDevice(device);
            checkForForceOta(device);
        }
        addToHomeListIfNeeded(device);
    }

    private void handleDisconnectedDevice(@NonNull BaseDevice device) {
        ConnectionState currentState =
                device.getBaseDeviceStateController().outputs.getConnectionInfoCurrentValue();
        if (currentState == ConnectionState.DISCONNECTED ||
                currentState == ConnectionState.TIMEOUT) {
            if (bluetoothSystemUtils.isDevicePaired(device)) {
                device.getBaseDeviceStateController().inputs
                        .setConnectionInfo(ConnectionState.CONNECTED);
            } else {
                speakerCacheManager.removeSpeakerInfoCache(device.getDeviceInfo().getId());
                device.getBaseDeviceStateController().inputs
                        .setConnectionInfo(ConnectionState.READY_TO_CONNECT);
            }
        }
    }

    private void addToHomeListIfNeeded(@NonNull BaseDevice device) {
        if (!homeDevices.contains(device)) {
            homeDevices.add(device);
            otaIndicatorVisibileMap.put(device.getBaseDeviceStateController()
                            .outputs
                            .getSpeakerInfoCurrentValue()
                            .getMacAddress(),
                    BehaviorSubject.createDefault(false));
            deviceAddedSubject.onNext(device);
        }
    }

    private void checkForForceOta(@NonNull BaseDevice device) {
        if (bluetoothSystemUtils.isDevicePaired(device) &&
                !speakerCacheManager.isSpeakerInfoCached(device.getDeviceInfo().getId())) {
            startForceOtaCheck(device);
        }
    }

    private void startForceOtaCheck(@NonNull BaseDevice device) {
        Disposable disposable = device.getBaseDeviceStateController().outputs
                .getConnectionInfo()
                .observeOn(AndroidSchedulers.mainThread())
                .takeUntil(connectionState -> connectionState ==
                        ConnectionState.CONNECTED || connectionState ==
                        ConnectionState.TIMEOUT)
                .subscribe(connectionState -> {
                    if (connectionState == ConnectionState.CONNECTED) {
                        if (device.getDeviceInfo().getDeviceType() ==
                                DeviceInfo.DeviceType.TYMPHANY_DEVICE) {
                            joplinOTAAdapter.addDeviceToOtaCheckList((TymphanyDevice) device);
                        }
                        checkForDisposedObservables();
                    }
                });
        otaCheckConnectionDisposables.add(disposable);
    }

    private boolean handleTwsConnectedDevice(@NonNull BaseDevice device) {
        if (bluetoothSystemUtils.isDevicePaired(device) && isDeviceCoupled(device)) {
            device.getBaseDeviceStateController().inputs
                    .setConnectionInfo(ConnectionState.TWS_CONNECTED);
            return true;
        }
        return false;
    }

    private boolean isDeviceCoupled(@NonNull BaseDevice device) {
        SpeakerInfoCache speakerInfoCache =
                speakerCacheManager.getSpeakerInfoCache(device.getDeviceInfo().getId());
        return speakerInfoCache != null && speakerInfoCache.getIsTwsCoupled();
    }

    private void checkForDisposedObservables() {
        List<Disposable> disposablesToBeRemoved = new ArrayList<>();

        for (Disposable disposable : otaCheckConnectionDisposables) {
            if (disposable.isDisposed()) {
                disposablesToBeRemoved.add(disposable);
            }
        }
        otaCheckConnectionDisposables.removeAll(disposablesToBeRemoved);
    }

    private void initPlayControlService(@NonNull Context context) {
        taPlayControlService = new TAPlayControlService(context, TAProtocol.ProtocolType.BLE);
    }

    private void initSystemService(@NonNull Context context) {
        HashMap<String, Object> map = TASystemService.getModeMap(context, JOPLIN_CONFIG_FILE);
        taSystemService = new TASystemService(context, TAProtocol.ProtocolType.BLE, map);
    }

    private void initDspService(@NonNull Context context) {
        taDspService = new TADigitalSignalProcessingService(context,
                TAProtocol.ProtocolType.BLE);
    }

    private void initDspOtaService(@NonNull Context context) {
        taOtaSignalDataProcessingService = new TAOtaSignalDataProcessingService(context,
                TAProtocol.ProtocolType.BLE);
    }

    private void initTAServices(@NonNull Context context) {
        initDspService(context);
        initSystemService(context);
        initPlayControlService(context);
        initDspOtaService(context);
        initServiceDisposable = taServiceReadySubject.
                observeOn(AndroidSchedulers.mainThread())
                .subscribe(isReady -> {
                    if (isReady) {
                        initAdapters();
                        initObservers();
                    }
                });
        taServiceReadySubject.onNext(true);
    }

    private void initAdapters() {
        joplinOTAAdapter = new TymphanyOTAAdapter(taOtaSignalDataProcessingService,
                ((BluetoothApplication) getApplication()).getAppComponent().otaApi(),
                this,
                speakerCacheManager,
                new FirmwareFileHelper(this),
                otaIndicatorVisibileMap);

        tymhpanyDeviceDiscoveryAdapter = new TymphanyDeviceDiscoveryAdapter(taSystemService,
                taPlayControlService, taDspService);
        joplinOTAAdapter.initOtaObserver();
        tymhpanyDeviceDiscoveryAdapter.initDiscoveryObserver();
    }

    private void initDeviceColorJsonObjects(@NonNull Context context) {
        new Thread(() -> {
            SpeakerColorUtils.saveJsonFormFiles(context);
            colorJsonReadySubject.onNext(true);
        }).start();
    }

    private void getCouplingConnectionInfo(@NonNull BaseDevice device,
                                           @NonNull ListIterator<BaseDevice> iterator) {
        BaseDevice coupleDevice;

        if (iterator.hasNext() && isCouplingScanInProgress) {
            coupleDevice = iterator.next();
            if (!coupleDevice.getBaseDeviceStateController()
                    .isFeatureSupported(FeaturesDefs.COUPLING_CONNECTION) || coupleDevice.equals(device)) {
                getCouplingConnectionInfo(device, iterator);
                return;
            }
        } else {
            coupledDevicesSubject.onComplete();
            isCouplingScanInProgress = false;
            return;
        }

        startCouplingInfoCheckForConnection(device, iterator, coupleDevice);
    }

    private void startCouplingInfoCheckForConnection(@NonNull BaseDevice device,
                                                     @NonNull ListIterator<BaseDevice> iterator,
                                                     @NonNull BaseDevice coupleDevice) {
        if (bluetoothSystemUtils.isDevicePaired(coupleDevice)) {
            couplingDisposables.add(coupleDevice.getBaseDeviceStateController().outputs
                    .getConnectionInfo()
                    .takeUntil(connectionState -> connectionState == ConnectionState.CONNECTED ||
                            connectionState == ConnectionState.TIMEOUT ||
                            connectionState == ConnectionState.DISCONNECTED)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(connectionState -> {
                        switch (connectionState) {
                            case CONNECTED:
                                if (!isDeviceCoupled(coupleDevice)) {
                                    coupledDevicesSubject.onNext(coupleDevice);
                                    getCouplingConnectionInfo(device, iterator);
                                }
                                break;
                            case TIMEOUT:
                            case DISCONNECTED:
                                getCouplingConnectionInfo(device, iterator);
                                break;
                        }
                    }));
        } else {
            getCouplingConnectionInfo(device, iterator);
        }
    }

    private boolean isReconnectInProgress() {
        for (BaseDevice device : homeDevices) {
            if (device.getBaseDeviceStateController().isFeatureSupported(FeaturesDefs.AUTO_CONNECT)) {
                if (device.getDeviceInfo().getDeviceType() == DeviceInfo.DeviceType.TYMPHANY_DEVICE &&
                        ((TymphanyDeviceStateController) device.getBaseDeviceStateController())
                                .isReconnectionInProgress()) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isRescanNeeded() {
        for (BaseDevice device : homeDevices) {
            ConnectionState connectionState =
                    device.getBaseDeviceStateController().outputs.getConnectionInfoCurrentValue();
            if (connectionState == ConnectionState.DISCONNECTED ||
                    connectionState == ConnectionState.READY_TO_CONNECT) {
                return true;
            }
        }
        return false;
    }

    private void clearCouplingDisposables() {
        for (Disposable disposable : couplingDisposables) {
            disposable.dispose();
        }
    }

    private void clearOtaCheckConnectionDisposables() {
        for (Disposable disposable : otaCheckConnectionDisposables) {
            disposable.dispose();
        }
    }

    /**
     * Class defining behavior for periodic scan.
     * Periodic scan is used to secure random MAC changes for the fast pairing devices.
     */
    private class ScanRunnable implements Runnable {

        @Override
        public void run() {
            if (!isDeviceScanInProgress && isScanAllowed &&
                    !isOTAInProgress() && !isReconnectInProgress() && isRescanNeeded()) {
                isDeviceScanInProgress = true;
                tymhpanyDeviceDiscoveryAdapter.startScanForDevices();
                handler.postDelayed(() -> {
                    tymhpanyDeviceDiscoveryAdapter.stopScanForDevices();
                    isDeviceScanInProgress = false;
                }, PERIODIC_SCAN_TIME_IN_MS);
            }
        }
    }
}
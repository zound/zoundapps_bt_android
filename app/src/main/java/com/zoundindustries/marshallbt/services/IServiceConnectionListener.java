package com.zoundindustries.marshallbt.services;

/**
 * Interface for registering callbacks on DeviceService connection.
 */
public interface IServiceConnectionListener {

    /**
     * Called when service is playingInfoText.
     */
    void onServiceConnected();

    /**
     * Called when service is disconnected.
     */
    void onServiceDisconnected();
}

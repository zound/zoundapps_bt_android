package com.zoundindustries.marshallbt.di;

import android.content.Context;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.BuildConfig;
import com.zoundindustries.marshallbt.model.adapters.mock.MockSDK;
import com.zoundindustries.marshallbt.model.bronto.BrontoApi;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.model.ota.OtaApi;
import com.zoundindustries.marshallbt.model.salesforce.SalesforceWebAPI;
import com.zoundindustries.marshallbt.model.salesforce.SalesforceWebAPIController;
import com.zoundindustries.marshallbt.utils.CommonPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Class that provides singletons for the app wide components
 */
@Module(includes = ViewModelModule.class)
public class AppModule {

    private static final String SOFTWARE_DOWNLOAD_URL =
            "https://usk4y0tsk2.execute-api.us-east-1.amazonaws.com/";
    private static final String BRONTO_BASE_URL =
            "http://email.marshallheadphones.com/";

    private Context context;

    public AppModule(BluetoothApplication context) {
        this.context = context;
    }

    @Provides
    @Singleton
    MockSDK provideMockSDK() {
        return new MockSDK();
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.create();
    }

    @Singleton
    @Provides
    OtaApi provideOtaApi(Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(SOFTWARE_DOWNLOAD_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build().create(OtaApi.class);
    }

    @Singleton
    @Provides
    BrontoApi provideBrontoApi(Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(BRONTO_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build().create(BrontoApi.class);
    }

    @Singleton
    @Provides
    SalesforceWebAPI provideSalesforceWebAPI() {
        Gson gson = new GsonBuilder().setLenient().create();
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.DEBUG ? SalesforceWebAPIController.TEST_BASE_URL
                        : SalesforceWebAPIController.PROD_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build().create(SalesforceWebAPI.class);
    }

    @Singleton
    @Provides
    SalesforceWebAPIController provideSalesforceWebAPIController(SalesforceWebAPI api) {
        return new SalesforceWebAPIController(api);
    }


    @Provides
    @Singleton
    FirebaseAnalyticsManager provideFirebaseWrapper(FirebaseAnalytics firebaseAnalytics) {
        return new FirebaseAnalyticsManager(firebaseAnalytics);
    }

    @Provides
    @Singleton
    CommonPreferences provideCommonPreferences() {
        return new CommonPreferences(context);
    }

    @Provides
    @Singleton
    FirebaseAnalytics provideFirebaseAnalytics() {
        return FirebaseAnalytics.getInstance(context);
    }
}

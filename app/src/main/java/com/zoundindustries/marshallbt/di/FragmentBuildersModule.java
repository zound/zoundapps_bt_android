package com.zoundindustries.marshallbt.di;

import com.zoundindustries.marshallbt.ui.devicesettings.CouplingListFragment;
import com.zoundindustries.marshallbt.ui.devicesettings.DeviceSettingsFragment;
import com.zoundindustries.marshallbt.ui.homescreen.DeviceListFragment;
import com.zoundindustries.marshallbt.ui.homescreen.ScanForSpeakersFragment;
import com.zoundindustries.marshallbt.ui.mainmenu.UnsubscribeFragment;
import com.zoundindustries.marshallbt.ui.ota.OTACompletedFragment;
import com.zoundindustries.marshallbt.ui.ota.OTAProgressFragment;
import com.zoundindustries.marshallbt.ui.player.sources.AuxSourceFragment;
import com.zoundindustries.marshallbt.ui.setup.SaveDataFragment;
import com.zoundindustries.marshallbt.ui.setup.ShareDataFragment;
import com.zoundindustries.marshallbt.ui.setup.StayUpdatedFragment;
import com.zoundindustries.marshallbt.ui.setup.TacFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Class that adds fragments for injection.
 */
@Module
public abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract DeviceListFragment contributeDeviceListFragment();

    @ContributesAndroidInjector
    abstract ScanForSpeakersFragment contributeScanForSpeakersFragment();

    // Setup screen fragments
    @ContributesAndroidInjector
    abstract StayUpdatedFragment contributeStayUpdatedFragment();

    @ContributesAndroidInjector
    abstract ShareDataFragment contributeShareDataFragment();

    @ContributesAndroidInjector
    abstract UnsubscribeFragment contributeUnsubscribeFragment();

    @ContributesAndroidInjector
    abstract TacFragment contributeTacFragment();

    @ContributesAndroidInjector
    abstract SaveDataFragment contributeSaveDataFragment();

    //OTA fragments
    @ContributesAndroidInjector
    abstract OTACompletedFragment contributeOTACompletedFragment();

    @ContributesAndroidInjector
    abstract OTAProgressFragment contributeOTAProgressFragment();

    //DeviceSettings fragments
    @ContributesAndroidInjector
    abstract DeviceSettingsFragment contributeDeviceSettingsFragment();

    // Source Fragments
    @ContributesAndroidInjector
    abstract AuxSourceFragment contributeAuxSourceFragment();

    @ContributesAndroidInjector
    abstract CouplingListFragment contributeCoupleFragment();

}
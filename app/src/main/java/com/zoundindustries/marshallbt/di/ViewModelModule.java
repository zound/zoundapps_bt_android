package com.zoundindustries.marshallbt.di;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.zoundindustries.marshallbt.viewmodels.ViewModelFactory;
import com.zoundindustries.marshallbt.viewmodels.homescreen.HomeActivityViewModel;
import com.zoundindustries.marshallbt.viewmodels.setup.SaveDataViewModel;
import com.zoundindustries.marshallbt.viewmodels.setup.ShareDataViewModel;
import com.zoundindustries.marshallbt.viewmodels.setup.StayUpdatedViewModel;
import com.zoundindustries.marshallbt.viewmodels.setup.TacViewModel;
import com.zoundindustries.marshallbt.viewmodels.welcome.WelcomeViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * Class that defines ViewModels injection with ViewModelFactory
 */
@Module
abstract public class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(WelcomeViewModel.Body.class)
    abstract ViewModel bindWelcomeViewModel(WelcomeViewModel.Body castWelcomeViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(HomeActivityViewModel.Body.class)
    abstract ViewModel bindHomeActivityViewModel(HomeActivityViewModel.Body deviceListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(StayUpdatedViewModel.Body.class)
    abstract ViewModel bindStayUpdatedViewModel(StayUpdatedViewModel.Body stayUpdatedViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ShareDataViewModel.Body.class)
    abstract ViewModel bindShareDataViewModel(ShareDataViewModel.Body shareDataViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(TacViewModel.Body.class)
    abstract ViewModel bindTacViewModel(TacViewModel.Body tacViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SaveDataViewModel.Body.class)
    abstract ViewModel bindSaveDataViewModel(SaveDataViewModel.Body saveDataViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}

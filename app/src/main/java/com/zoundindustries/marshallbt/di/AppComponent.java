package com.zoundindustries.marshallbt.di;


import android.app.Application;

import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.model.adapters.mock.MockSDK;
import com.zoundindustries.marshallbt.model.bronto.BrontoApi;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.model.ota.OtaApi;
import com.zoundindustries.marshallbt.model.salesforce.SalesforceWebAPIController;
import com.zoundindustries.marshallbt.utils.CommonPreferences;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

/**
 * Application component refers to application level modules only
 */

@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AppModule.class,
        ActivityModule.class})
public interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        Builder appModule(AppModule appModule);

        AppComponent build();
    }

    void inject(BluetoothApplication bluetoothApplication);

    MockSDK mockSDK();
    OtaApi otaApi();
    BrontoApi brontoApi();
    FirebaseAnalyticsManager firebaseWrapper();
    CommonPreferences commonPreferences();
    SalesforceWebAPIController saleforceApi();
}

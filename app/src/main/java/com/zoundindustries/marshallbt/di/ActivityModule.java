package com.zoundindustries.marshallbt.di;

import com.zoundindustries.marshallbt.ui.SplashScreenActivity;
import com.zoundindustries.marshallbt.ui.devicesettings.DeviceSettingsActivity;
import com.zoundindustries.marshallbt.ui.mainmenu.MainMenuActivity;
import com.zoundindustries.marshallbt.ui.welcome.WelcomeActivity;
import com.zoundindustries.marshallbt.ui.homescreen.HomeActivity;
import com.zoundindustries.marshallbt.ui.ota.OTAActivity;
import com.zoundindustries.marshallbt.ui.setup.SetupActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Class that defines fragments submodules for activity injections.
 */
@Module
public abstract class ActivityModule {
    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract HomeActivity contributeHomeActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract SetupActivity contributeSetupActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract SplashScreenActivity contributeSplashScreenActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract WelcomeActivity contributeWelcomeActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract DeviceSettingsActivity contributeDeviceSettingsActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract MainMenuActivity contributeMainMenuActivity();
}

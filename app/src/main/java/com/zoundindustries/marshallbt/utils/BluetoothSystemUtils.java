package com.zoundindustries.marshallbt.utils;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.model.device.BaseDevice;

import java.util.Set;


/**
 * Utility class providing system bluetooth utilities.
 */
public final class BluetoothSystemUtils {

    /**
     * Get the set of devices that are paired(bonded) in the system.
     *
     * @return Set of {@link BluetoothDevice} that are bonded.
     */
    public boolean isDevicePaired(@NonNull BaseDevice device) {
        for (BluetoothDevice bluetoothDevice : getPairedDevices()) {
            if (bluetoothDevice.getAddress().equals(device.getDeviceInfo().getId())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the set of devices that are paired(bonded) in the system.
     *
     * @return Set of {@link BluetoothDevice} that are bonded.
     */
    @NonNull
    public Set<BluetoothDevice> getPairedDevices() {
        BluetoothAdapter bluetoothAdapter
                = BluetoothAdapter.getDefaultAdapter();
        return bluetoothAdapter.getBondedDevices();
    }
}

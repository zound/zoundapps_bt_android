package com.zoundindustries.marshallbt.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.zoundindustries.marshallbt.model.devicesettings.EQData;
import com.zoundindustries.marshallbt.ui.setup.ShareDataFragment;
import com.zoundindustries.marshallbt.ui.setup.StayUpdatedFragment;

/**
 * Utility class helping accessing common app sharedPreferences
 */
public class CommonPreferences {

    private final String CAST_TOS_ENABLED = "CURRENT_THEME_KEY";
    private final String USER_EMAIL = "USER_EMAIL";
    private final String INITIAL_USER_SUBSCRIPTION = "INITIAL_USER_SUBSCRIPTION";
    private final String INITIAL_USER_SHARE_DATA = "INITIAL_USER_SHARE_DATA";
    private final String USER_AGREEMENT_TO_SHARE_DATA = "USER_AGREEMENT_TO_SHARE_DATA";
    private final String EQ_CUSTOM_PRESET_VALUES = "EQ_CUSTOM_PRESET_VALUES";

    private Context context;

    /**
     * Constructor for CommonPreferences.
     *
     * @param context for opening getPreferences from the app.
     */
    public CommonPreferences(@NonNull Context context) {
        this.context = context;
    }

    /**
     * Set Marshall greeting view on and off
     *
     * @param enable showing Welcome screen enabled disabled
     */
    public void setWelcomeScreenVisible(boolean enable) {
        getEditor().putBoolean(CAST_TOS_ENABLED, enable).apply();
    }

    /**
     * Set Welcome screen view enabled state
     *
     * @return greeting view enabled state
     */
    public boolean shouldDisplayWelcomeScreen() {
        return getPreferences().getBoolean(CAST_TOS_ENABLED, true);
    }

    /**
     * Set user email address after subscription
     */
    public void setUserEmail(@Nullable String userEmail) {
        getEditor().putString(USER_EMAIL, userEmail).apply();
    }

    /**
     * return user email address if exists
     *
     * @return String userEmail
     */
    @Nullable
    public String getUserEmail() {
        return getPreferences().getString(USER_EMAIL, null);
    }

    /**
     * Used for getting boolean information about email subscription
     *
     * @return boolean true in case when userMail exist.
     */
    public boolean isEmailSubscribed() {
        return getPreferences().getString(USER_EMAIL, null) != null;
    }

    /**
     * Used for getting boolean information about is
     * {@link StayUpdatedFragment} first time launched
     *
     * @return boolean true in case when it is first time launched.
     */
    public boolean isFirstTimeSubscription() {
        return getPreferences().getBoolean(INITIAL_USER_SUBSCRIPTION, true);
    }

    public void setFirstRunOfSubscription(boolean firstTimeSubscription) {
        getEditor().putBoolean(INITIAL_USER_SUBSCRIPTION, firstTimeSubscription).apply();
    }

    /**
     * Used for getting boolean information about is
     * {@link ShareDataFragment} first time launched
     *
     * @return boolean true in case when it is first time launched.
     */
    public boolean isFirstTimeShareData() {
        return getPreferences().getBoolean(INITIAL_USER_SHARE_DATA, true);
    }

    public void setFirstRunOfShareData(boolean firstTimeShareData) {
        getEditor().putBoolean(INITIAL_USER_SHARE_DATA, firstTimeShareData).apply();
    }

    /**
     * Used for getting boolean information about user approval about sharing anonymous data
     *
     * @return boolean true in case when user agree to share data. Must be false by default due to
     * GDPR regulations.
     */
    public boolean isUserSharingData() {
        return getPreferences().getBoolean(USER_AGREEMENT_TO_SHARE_DATA, false);
    }

    public void setIsUserSharingData(boolean isUserSharingData) {
        getEditor().putBoolean(USER_AGREEMENT_TO_SHARE_DATA, isUserSharingData).apply();
    }

    /**
     * Save currently modified custom EQ values
     *
     * @param data EQData to be saved
     */
    public void setEqCustomPresetValues(EQData data) {
        getEditor().putString(EQ_CUSTOM_PRESET_VALUES, new Gson().toJson(data)).apply();
    }

    /**
     * Get current custom EQ value saved by the user.
     *
     * @return current EQData for custom rpeset.
     */
    public EQData getEqCustomPresetValues() {
        String data = getPreferences().getString(EQ_CUSTOM_PRESET_VALUES, "");
        if (TextUtils.isEmpty(data)) {
            return new EQData(new int[]{10, 10, 10, 10, 10});
        }
        return new Gson().fromJson(data, EQData.class);
    }

    @NonNull
    private SharedPreferences getPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @NonNull
    private SharedPreferences.Editor getEditor() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit();
    }
}

package com.zoundindustries.marshallbt.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class used to validate email address
 */
public class EmailValidator {

    /**
     * Method checking validity of email
     *
     * @param email string with input address
     * @return boolean if email is correct
     */
    public static boolean isValid(String email){
        return Pattern.compile("(?:[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+/=" +
                "?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f" +
                "]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*" +
                "[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?|\\[(?:(?:25[0-5]|2[0" +
                "-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-" +
                "9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\" +
                "x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)])").matcher(email).matches();
    }
}

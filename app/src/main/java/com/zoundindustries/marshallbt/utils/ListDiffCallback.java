package com.zoundindustries.marshallbt.utils;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

import com.zoundindustries.marshallbt.model.device.BaseDevice;

import java.util.List;

/**
 * Util class for list comparison.
 * Used to properly update devices list in the system.
 */
public class ListDiffCallback extends DiffUtil.Callback {

    private List<BaseDevice> oldDevices;
    private List<BaseDevice> newDevices;

    /**
     * Constructor for ListDiffCallback.
     *
     * @param oldDevices list before change occured.
     * @param newDevices list containing changes.
     */
    public ListDiffCallback(List<BaseDevice> oldDevices, List<BaseDevice> newDevices) {
        this.oldDevices = oldDevices;
        this.newDevices = newDevices;
    }

    @Override
    public int getOldListSize() {
        return oldDevices.size();
    }

    @Override
    public int getNewListSize() {
        return newDevices.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldDevices.get(oldItemPosition).getDeviceInfo().getId().equals(newDevices
                .get(newItemPosition).getDeviceInfo().getId());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldDevices.get(oldItemPosition).equals(newDevices.get(newItemPosition));
    }

    @Nullable
    @Override
    public BaseDevice getChangePayload(int oldItemPosition, int newItemPosition) {
        return (BaseDevice) super.getChangePayload(oldItemPosition, newItemPosition);
    }
}

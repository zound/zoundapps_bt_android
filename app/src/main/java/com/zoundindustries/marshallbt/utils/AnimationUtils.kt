package com.zoundindustries.marshallbt.utils

import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.widget.TextView

const val ANIM_BLINK_DURATION_MS = 970L

/**
 * Animating visibility change based on alpha animation.
 *
 * @param view       to be animated
 * @param visibility target visibility form
 */
fun animateVisibilityChange(view: View, visibility: Int) {
    if (view.visibility != visibility) {
        if (visibility == View.INVISIBLE || visibility == View.GONE) {
            view.animate().alpha(0.0f)
            view.visibility = visibility
        } else {
            view.visibility = visibility
            view.animate().alpha(1.0f)
        }
    }
}

/**
 * Update a textView with a fade
 */
fun setTextWithFade(view: TextView, stringId: Int) {
    view.alpha = 0f
    view.setText(stringId)
    view.animate().alpha(1f).start()
}

/**
 * Get blinking animation with a 1 second duration
 */
fun getBlinkAnimation(): AlphaAnimation {
    val anim = AlphaAnimation(1f, 0f)
    anim.duration = ANIM_BLINK_DURATION_MS
    anim.repeatCount = Animation.INFINITE
    anim.repeatMode = Animation.ABSOLUTE
    return anim
}

package com.zoundindustries.marshallbt.utils.comparators;

import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceManager;

import java.util.Comparator;

/**
 * Utility class to sort home list based coupling state.
 * Sorting is grouping coupled devices into couples.
 */
public class HomeListCouplingComparator implements Comparator<BaseDevice> {

    private DeviceManager deviceManager;

    public HomeListCouplingComparator(@NonNull DeviceManager deviceManager) {
        this.deviceManager = deviceManager;
    }

    @Override
    public int compare(BaseDevice device1, BaseDevice device2) {
        if (isDeviceCoupled(device1) && !isDeviceCoupled(device2)) {
            return -1;
        } else if (!isDeviceCoupled(device1) && isDeviceCoupled(device2)) {
            return 1;
        } else {
            return 0;
        }
    }

    private boolean isDeviceCoupled(@NonNull BaseDevice device) {
        return deviceManager.isSpeakerInfoCached(device.getDeviceInfo().getId()) &&
                deviceManager.getSpeakerInfoCache(device.getDeviceInfo().getId()).getIsTwsCoupled();
    }
}

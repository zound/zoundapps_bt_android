package com.zoundindustries.marshallbt.utils;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.zoundindustries.marshallbt.model.devicesettings.EQPreset;
import com.zoundindustries.marshallbt.model.devicesettings.EQData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Base class to handle EQ presets
 */
public abstract class BaseEQPresetListManager {

    protected CommonPreferences preferences;

    private ArrayList<EQPreset> presets;

    public BaseEQPresetListManager(@NonNull Context context) {
        this(new CommonPreferences(context));
    }

    @VisibleForTesting
    public BaseEQPresetListManager(@NonNull CommonPreferences preferences) {
        this.preferences = preferences;
        presets = new ArrayList<>();
        presets.addAll(getPredefinedPresets());
    }

    /**
     * Method used to get the preset types for equaliser
     *
     * @return list with preset values
     */
    @NonNull
    public List<EqPresetType> getPresetTypes() {
        return Arrays.asList(EqPresetType.values());
    }

    /**
     * Method used to get the preset with the chosen index
     * @param index the index of the chosen preset
     * @return the preset
     */
    @NonNull
    public EQPreset getPresetByIndex(int index) {
        return presets.get(index).cloneEQPreset();
    }

    /**
     * Modify the custom preset values
     * @param predefinedEqPreset the values of the custom preset
     */
    public void saveCustomPreset(@NonNull EQPreset predefinedEqPreset) {
       presets.set(0, predefinedEqPreset);
    }

    /**
     * Get the index of the given preset
     * @param predefinedPreset the preset which ones index is needed
     * @return the index of the preset or the custom index if the preset is not found
     */
    public int getIndexOfPreset(@NonNull EQPreset predefinedPreset) {
        int index = presets.indexOf(predefinedPreset);
        return index != -1 ? index : 0;
    }

    /**
     * Get the type of the given preset
     * @param eqData the EQ preset values
     * @return the type belong to the given preset
     */
    public EqPresetType getEqualizerPresetType(@NonNull EQData eqData) {
        for (EQPreset preset : presets) {
            if(preset.getEqData().equals(eqData)) {
                return preset.getEqType();
            }
        }
        return EqPresetType.CUSTOM;
    }

    protected abstract List<EQPreset> getPredefinedPresets();
}

package com.zoundindustries.marshallbt.utils;

/**
 * Utility class for all possible device features.
 */
public final class FeaturesDefs {
    public static final int VOLUME = 1 << 1;
    public static final int PLAY_CONTROL = 1 << 2;
    public static final int CONNECTION_INFO = 1 << 3;
    public static final int OTA_UPDATE = 1 << 4;
    public static final int SPEAKER_INFO = 1 << 5;
    public static final int EQ = 1 << 6;
    public static final int LIGHT = 1 << 7;
    public static final int RENAME = 1 << 8;
    public static final int COUPLING_CONNECTION = 1 << 9;
    public static final int COUPLING_CHANNEL = 1 << 10;
    public static final int SOUNDS_SETTINGS = 1 << 11;
    public static final int BLUETOOTH_SOURCE = 1 << 12;
    public static final int AUX_SOURCE = 1 << 13;
    public static final int RCA_SOURCE = 1 << 14;
    public static final int SONG_INFO = 1 << 15;
    public static final int M_BUTTON = 1 << 16;
    public static final int ANC = 1 << 17;
    public static final int TIMER_OFF = 1 << 18;
    public static final int EQ_EXTENDED = 1 << 19;
    public static final int BATTERY_LEVEL = 1 << 20;
    public static final int AUTO_CONNECT = 1 << 21;
    public static final int PAIRING_MODE_STATUS = 1 << 22;
    /**
     * Answers the question: Can AUX source be triggered from the app? Some devices can
     * switch to bt source when aux is plugged in, some can't
     */
    public static final int AUX_SOURCE_CONFIGURABLE = 1 << 23;
    /**
     * Some switches in SOUNDS_SETTINGS are features on their own
     */
    public static final int SOUNDS_SETTINGS_MEDIA_CONTROL = 1 << 24;
}
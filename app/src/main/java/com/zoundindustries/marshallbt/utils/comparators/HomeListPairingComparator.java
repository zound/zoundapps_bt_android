package com.zoundindustries.marshallbt.utils.comparators;

import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.utils.BluetoothSystemUtils;

import java.util.Comparator;

/**
 * Utility class to sort home list based on the pairing state.
 */
public class HomeListPairingComparator implements Comparator<BaseDevice> {

    private BluetoothSystemUtils bluetoothSystemUtils;

    /**
     * Constructor using {@link BluetoothSystemUtils}.
     *
     * @param bluetoothSystemUtils to be used.
     */
    public HomeListPairingComparator(@NonNull BluetoothSystemUtils bluetoothSystemUtils) {
        this.bluetoothSystemUtils = bluetoothSystemUtils;
    }

    @Override
    public int compare(BaseDevice device1, BaseDevice device2) {
        if (bluetoothSystemUtils.isDevicePaired(device1) &&
                !bluetoothSystemUtils.isDevicePaired(device2)) {
            return -1;
        } else if (!bluetoothSystemUtils.isDevicePaired(device1) &&
                bluetoothSystemUtils.isDevicePaired(device2)) {
            return 1;
        } else {
            return 0;
        }
    }
}

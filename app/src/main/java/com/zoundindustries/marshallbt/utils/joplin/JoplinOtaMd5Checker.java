package com.zoundindustries.marshallbt.utils.joplin;

import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.utils.FirmwareFileHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Class used for checking md5 checksum of ota update file imported from Tym reference app.
 * Some parts may be mysterious and shady
 */
public class JoplinOtaMd5Checker {

    private FirmwareFileHelper fwFileHelper;

    public JoplinOtaMd5Checker(@NonNull FirmwareFileHelper fwFileHelper) {
         this.fwFileHelper = fwFileHelper;
    }

    /**
     * Check if upgrade file generated md5 checksum is the same as the provided one
     *
     * @return true value if checksums are matching, false if not
     */
    public boolean checkHexFileMD5() {

        File binFile = fwFileHelper.getFirmwareBinaryFile();
        File md5File = fwFileHelper.getFirmwareMd5File();

        if (binFile.isFile() && md5File.isFile()) {
            FileInputStream fis;
            try {
                fis = new FileInputStream(md5File);
                byte[] buffer = new byte[fis.available()];
                fis.read(buffer);
                String res = new String(buffer, "UTF-8");
                return res.contains(countMd5(binFile));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    private static String countMd5(File f) {

        byte[] rb = null;
        DigestInputStream digestInputStream = null;
        try (FileInputStream fis = new FileInputStream(f)) {
            MessageDigest md5Checksum = MessageDigest.getInstance("md5");
            digestInputStream = new DigestInputStream(fis, md5Checksum);

            // why empty while body and unused buffer? who knows
            byte[] buffer = new byte[4096];
            while (digestInputStream.read(buffer) > 0) ;

            md5Checksum = digestInputStream.getMessageDigest();
            rb = md5Checksum.digest();
        } catch (IOException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        StringBuilder sb = new StringBuilder();
        if (rb != null) {
            for (int i = 0; i < rb.length; i++) {
                String a = Integer.toHexString(0XFF & rb[i]);
                if (a.length() < 2) {
                    a = '0' + a;
                }
                sb.append(a);
            }
        }
        return sb.toString();
    }
}

package com.zoundindustries.marshallbt.utils;

import android.os.Environment;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import io.reactivex.annotations.NonNull;
import okhttp3.ResponseBody;

/**
 * Helper class for network operations
 */
public class NetworkUtils {

    private static final String VENDOR_NAME = "/Marshall";

    public interface IFileDownloadProgressListener {
        void onDownloadProgress(long progress);
    }

    private static final int BUFFER_SIZE = 4096;

    public static String getStoragePathBase(Context context) {
        return context.getFilesDir().getPath();
    }

    /**
     * Used to handle data downloaded via Retrofit and save it to specific location
     *
     * @param body     response of download server
     * @param saveFile location on which data will be saved
     * @return boolean with success state
     */
    public static boolean writeResponseBodyToDisk(ResponseBody body, @NonNull File saveFile) {
        try {
            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[BUFFER_SIZE];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                File parentFolder = new File(saveFile.getParent());
                if(!parentFolder.exists()) {
                    parentFolder.mkdirs();
                }

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(saveFile);
                int read;

                while ((read = inputStream.read(fileReader)) != -1) {
                    outputStream.write(fileReader, 0, read);
                    fileSizeDownloaded += read;
                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

    /**
     * Used to get current network status. Does not check actual internet connection, though!
     * @param context needed to get system service
     * @return boolean value if network is on
     */
    public static boolean isOnline(Context context) {
        ConnectivityManager manager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = manager != null ? manager.getActiveNetworkInfo() : null;
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}

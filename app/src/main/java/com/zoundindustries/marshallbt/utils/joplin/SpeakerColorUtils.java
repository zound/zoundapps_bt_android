package com.zoundindustries.marshallbt.utils.joplin;

import android.content.Context;
import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice.DeviceSubType;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

/**
 * Class to check the color of the speakers by MAC address
 */
public class SpeakerColorUtils {

    private static final String WHITE_ACTONS_JSON_FILE = "whiteActons.json";
    private static final String WHITE_STANMORES_JSON_FILE = "whiteStanmores.json";
    private static final String WHITE_WOBURNS_JSON_FILE = "whiteWoburns.json";

    private static JSONObject whiteActonJsonObject;
    private static JSONObject whiteStanmoreJsonObject;
    private static JSONObject whiteWoburnJsonObject;


    /**
     * Method to read and save the JSON files with MAC addresses
     *
     * @param context context for file reading
     */
    public static void saveJsonFormFiles(Context context) {
        whiteActonJsonObject = getJsonFromFile(context, WHITE_ACTONS_JSON_FILE);
        whiteStanmoreJsonObject = getJsonFromFile(context, WHITE_STANMORES_JSON_FILE);
        whiteWoburnJsonObject = getJsonFromFile(context, WHITE_WOBURNS_JSON_FILE);
    }

    /**
     * Method to give back the speaker color decided by MAC address
     *
     * @param type       type of the speaker
     * @param macAddress MAC address of the speaker
     * @return the color of the speaker
     */
    public static DeviceInfo.DeviceColor isWhiteSpeaker(@NonNull DeviceSubType type,
                                                        @NonNull String macAddress) {
        boolean isWhite = false;
        switch (type) {
            case JOPLIN_S:
                if (whiteActonJsonObject != null) {
                    isWhite = whiteActonJsonObject.has(macAddress);
                }
                break;
            case JOPLIN_M:
                if (whiteStanmoreJsonObject != null) {
                    isWhite = whiteStanmoreJsonObject.has(macAddress);
                }
                break;
            case JOPLIN_L:
                if (whiteWoburnJsonObject != null) {
                    isWhite = whiteWoburnJsonObject.has(macAddress);
                }
                break;
        }
        return isWhite ? DeviceInfo.DeviceColor.WHITE : DeviceInfo.DeviceColor.BLACK;
    }

    @NonNull
    private static JSONObject getJsonFromFile(@NonNull Context context, @NonNull String fileName) {
        try {
            InputStream is = context.getAssets().open(fileName);
            byte[] buffer = new byte[is.available()];

            is.read(buffer);
            is.close();

            return new JSONObject(new String(buffer, "UTF-8"));
        } catch (IOException | JSONException ex) {
            ex.printStackTrace();
            return new JSONObject();
        }
    }
}

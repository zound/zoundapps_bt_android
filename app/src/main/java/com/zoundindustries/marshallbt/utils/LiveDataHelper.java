package com.zoundindustries.marshallbt.utils;

import androidx.lifecycle.MutableLiveData;

import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;

/**
 * Class used to unbox MutableLiveDa
 */
public class LiveDataHelper {

    public static Boolean unboxBoolean(MutableLiveData<Boolean> data) {
        return data == null ? false : data.getValue();
    }

    public static Integer unboxInteger(MutableLiveData<Integer> data) {
        return data == null ? 0 : data.getValue();
    }

    public static Float unboxFloat(MutableLiveData<Float> data) {
        return data == null ? 0f : data.getValue();
    }

    public static ViewType unboxViewType(MutableLiveData<ViewType> data) {
        return data == null ? ViewType.UNKNOWN : data.getValue();
    }
}

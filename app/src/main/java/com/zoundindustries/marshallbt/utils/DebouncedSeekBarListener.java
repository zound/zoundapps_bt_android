package com.zoundindustries.marshallbt.utils;

import android.os.SystemClock;
import androidx.annotation.NonNull;
import android.widget.SeekBar;

/**
 * This is created to simplify new seekbar listener creations
 */
public abstract class DebouncedSeekBarListener implements SeekBar.OnSeekBarChangeListener {
    private long lastChangeTimeMs = 0;
    private static final long DEBOUNCE_INTERVAL_MS = 100;

    @Override
    public void onProgressChanged(SeekBar seekBar, int value, boolean fromUser) {
        long currentChangeTimeMs = SystemClock.uptimeMillis();

        if(currentChangeTimeMs - lastChangeTimeMs > DEBOUNCE_INTERVAL_MS) {
            runTask(seekBar, value, fromUser);
            lastChangeTimeMs = currentChangeTimeMs;
        }
    }

    @Override
    public void onStartTrackingTouch(@NonNull SeekBar seekBar) {
        //NOP
    }

    @Override
    public void onStopTrackingTouch(@NonNull SeekBar seekBar) {
        runTask(seekBar, seekBar.getProgress(), true);
        lastChangeTimeMs = 0;
    }

    public abstract void runTask(@NonNull SeekBar seekBar, int progress, boolean fromUser);
}

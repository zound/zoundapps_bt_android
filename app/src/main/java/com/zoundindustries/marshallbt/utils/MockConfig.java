package com.zoundindustries.marshallbt.utils;

import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.model.device.BaseDevice;

import java.util.List;

/**
 * Class keeping mock configuration for the system.
 */
public class MockConfig {
    private boolean continuousScan;
    private int scanDelay;
    private List<BaseDevice> devices;
    private List<String> updateDevices;

    /**
     * Constructor for MockConfig.
     *
     * @param builder used to create MockConfig.
     */
    public MockConfig(@NonNull Builder builder) {
        this.continuousScan = builder.continuousScan;
        this.scanDelay = builder.scanDelay;
        this.devices = builder.devices;
        this.updateDevices = builder.updateDevices;
    }

    /**
     * Get value for continuous scan (devices will be added to the list continuously).
     *
     * @return continuous scan value.
     */
    public boolean isContinuousScan() {
        return continuousScan;
    }

    /**
     * Get value for devices scanning delay.
     *
     * @return scan delay value.
     */
    public int getScanDelay() {
        return scanDelay;
    }

    /**
     * Get list of mocked devices.
     *
     * @return list of mock devices.
     */
    @NonNull
    public List<BaseDevice> getDevices() {
        return devices;
    }

    /**
     * Get list of mocked devices to be updated.
     *
     * @return list of mock devices to be updated.
     */
    @NonNull
    public List<String> getUpdateDevices() {
        return updateDevices;
    }

    /**
     * Builder pattern class for {@link MockConfig}.
     */
    static public class Builder {
        private boolean continuousScan;
        private int scanDelay;
        private List<BaseDevice> devices;
        private List<String> updateDevices;

        /**
         * Setter for continuous scan value.
         *
         * @param continuousScan new value.
         */
        public Builder setContinuousScan(boolean continuousScan) {
            this.continuousScan = continuousScan;
            return this;
        }

        /**
         * Setter for scan delay value.
         *
         * @param scanDelay value.
         * @return builder instance.
         */
        @NonNull
        public Builder setScanDelay(int scanDelay) {
            this.scanDelay = scanDelay;
            return this;
        }

        /**
         * Setter for device list.
         *
         * @param devices value.
         * @return builder instance.
         */
        @NonNull
        public Builder setDevices(List<BaseDevice> devices) {
            this.devices = devices;
            return this;
        }

        /**
         * Setter for device to be updated list.
         *
         * @param updateDevices value.
         * @return builder instance.
         */
        @NonNull
        public Builder setUpdateDevices(List<String> updateDevices) {
            this.updateDevices = updateDevices;
            return this;
        }

        /**
         * Creates {@link MockConfig} instance for set values.
         *
         * @return instance of MockConfig.
         */
        @NonNull
        public MockConfig create() {
            return new MockConfig(this);
        }
    }
}
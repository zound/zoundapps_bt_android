package com.zoundindustries.marshallbt.utils;

import com.tym.tymappplatform.utils.TACommonDefinitions;

/**
 * class keeping types of presets for the equaliser.
 *
 * Provides way to translate the presetType to the framework eqSettingsType
 */
public enum EqPresetType {
    CUSTOM {
        @Override
        public TACommonDefinitions.EQSettingsType toSettingsType() {
            return TACommonDefinitions.EQSettingsType.Custom;
        }
    },
    FLAT {
        @Override
        public TACommonDefinitions.EQSettingsType toSettingsType() {
            return TACommonDefinitions.EQSettingsType.Flat;
        }
    },
    ROCK {
        @Override
        public TACommonDefinitions.EQSettingsType toSettingsType() {
            return TACommonDefinitions.EQSettingsType.Rock;
        }
    },
    METAL {
        @Override
        public TACommonDefinitions.EQSettingsType toSettingsType() {
            return TACommonDefinitions.EQSettingsType.Metal;
        }
    },
    POP {
        @Override
        public TACommonDefinitions.EQSettingsType toSettingsType() {
            return TACommonDefinitions.EQSettingsType.Pop;
        }
    },
    HIPHOP {
        @Override
        public TACommonDefinitions.EQSettingsType toSettingsType() {
            return TACommonDefinitions.EQSettingsType.HipHop;
        }
    },
    ELECTRONIC {
        @Override
        public TACommonDefinitions.EQSettingsType toSettingsType() {
            return TACommonDefinitions.EQSettingsType.Electronic;
        }
    },
    JAZZ {
        @Override
        public TACommonDefinitions.EQSettingsType toSettingsType() {
            return TACommonDefinitions.EQSettingsType.Jazz;
        }
    };

    public abstract TACommonDefinitions.EQSettingsType toSettingsType();

    public static EqPresetType from(TACommonDefinitions.EQSettingsType eqSettingsType) {
        switch(eqSettingsType) {
            case Flat:
                return FLAT;
            case Custom:
                return CUSTOM;
            case Rock:
                return ROCK;
            case Metal:
                return METAL;
            case Pop:
                return POP;
            case HipHop:
                return HIPHOP;
            case Electronic:
                return ELECTRONIC;
            case Jazz:
                return JAZZ;
        }
        return null;
    }

}

package com.zoundindustries.marshallbt.utils;

import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import androidx.annotation.NonNull;

/**
 * Used to provide some bluetooth utils
 */
public class SystemServicesUtils {

    private static final String ANDROID_SETTINGS_PACKAGE = "com.android.settings";
    private static final String BLUETOOTH_SETTINGS_PACKAGE
            = "com.android.settings.bluetooth.BluetoothSettings";

    /**
     * Checks if the bluetooth is enabled
     *
     * @return boolean value with bluetooth state
     */
    public static boolean isBluetoothEnabled() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        return mBluetoothAdapter != null && mBluetoothAdapter.isEnabled();
    }

    public static boolean isLocationEnabled(@NonNull Context context) {
        LocationManager locationManager = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    /**
     * Get Intent for starting Android settings on Bluetooth page.
     *
     * @return Intent to be used for starting Activity.
     */
    @NonNull
    public static Intent getBluetoothSettingsIntent() {
        final Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setComponent(new ComponentName(ANDROID_SETTINGS_PACKAGE,
                BLUETOOTH_SETTINGS_PACKAGE));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    /**
     * Get Intent for starting Android settings on Global page.
     *
     * @return Intent to be used for starting Activity.
     */
    @NonNull
    public static Intent getGlobalSettingsIntent() {
        final Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    /**
     * Get Intent for starting Android settings on Location page.
     *
     * @return Intent to be used for starting Activity.
     */
    @NonNull
    public static Intent getLocationSettingsIntent() {
        final Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }
}
package com.zoundindustries.marshallbt.utils;

import androidx.viewpager.widget.ViewPager;

/**
 * his is created to simplify new OnPageChangeListener creations
 */
public interface SimplifiedOnPageChangeListener extends ViewPager.OnPageChangeListener {

    @Override
    default void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        //NOP
    }

    @Override
    default void onPageSelected(int position) {
        //NOP
    }

    @Override
    default void onPageScrollStateChanged(int state) {
        //NOP
    }
}

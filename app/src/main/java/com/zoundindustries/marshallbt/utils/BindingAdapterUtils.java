package com.zoundindustries.marshallbt.utils;

import androidx.databinding.BindingAdapter;
import android.graphics.Color;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.homescreen.HomeScreenItemLinkResources;
import com.zoundindustries.marshallbt.model.ota.IOTAService;
import com.zoundindustries.marshallbt.ui.HomeItemLinkLayout;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;

/**
 * Util class for {@link BindingAdapter} methods
 */
public class BindingAdapterUtils {

    public static final int MAX_BRIGHTNESS = 100;

    /**
     * Concat a spannable string to the {@link TextView}
     *
     * @param textView the {@link TextView} view
     * @param concat   the second part of the text, which will use a different style
     */
    @BindingAdapter("concatSpannableText")
    public static void concatSpannableText(@NonNull TextView textView, String concat) {
        if (!TextUtils.isEmpty(concat)) {
            Typeface typeface = Typeface.createFromAsset(textView.getContext().getAssets(),
                    "fonts/Roboto-Light.ttf");
            String tempResultValue = textView.getText().toString();

            if (!TextUtils.isEmpty(tempResultValue)) {
                tempResultValue = tempResultValue.split(":")[0];
            }
            Spannable spannable = new SpannableString(tempResultValue + ": " + concat);
            spannable.setSpan(new CalligraphyTypefaceSpan(typeface),
                    spannable.length() - concat.length(),
                    spannable.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            );
            textView.setText(spannable);
        }
    }

    /**
     * Concat a spannable string to the {@link TextView} for artist and album of track info
     *
     * @param textView the {@link TextView} view
     * @param artist   the second part of the text, which will use a different style
     */
    @BindingAdapter("concatArtistAlbumText")
    public static void concatArtistAlbumText(@NonNull TextView textView, String artist) {
        if (!TextUtils.isEmpty(artist)) {
            String album = textView.getText().toString();
            Typeface typeface = Typeface.createFromAsset(textView.getContext().getAssets(),
                    "fonts/Roboto-Light.ttf");

            Spannable spannable = new SpannableString(album + (TextUtils.isEmpty(album) ?
                    "" : " - ") + artist);
            spannable.setSpan(new CalligraphyTypefaceSpan(typeface),
                    spannable.length() - artist.length(),
                    spannable.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            );
            textView.setText(spannable);
        }
    }

    /**
     * Change the alpha of a color, which is used to make darker the original image. As the color
     * will be more transparent, the image will look like more bright.
     *
     * @param imageView  the {@link ImageView} view
     * @param brightness the factor which will cause the image to be brighter.
     *                   The image will be brighter as the factor is bigger.
     */
    @BindingAdapter("brightness")
    public static void setBrightnessFilter(@NonNull ImageView imageView, int brightness) {
        int color = imageView.getResources().getColor(R.color.half_transparent_grey);
        int alpha = Math.round(Color.alpha(color) * (1 - ((float) brightness / MAX_BRIGHTNESS)));

        imageView.setColorFilter(Color.argb(alpha, Color.red(color), Color.green(color),
                Color.blue(color)));
    }

    /**
     * Set string corresponding to specific ota state text to a view.
     *
     * @param textView view that will get the string
     * @param state    current state of update
     */
    @BindingAdapter({"otaStatusText", "percentValue"})
    public static void setOtaStatusText(@NonNull TextView textView, IOTAService.UpdateState state,
                                        Double percentValue) {
        String progressValue;
        switch (state) {
            case NOT_STARTED:
                textView.setText(textView.getContext()
                        .getString(R.string.ota_screen_state_not_started));
                break;
            case DOWNLOADING_FROM_SERVER:
                textView.setText(textView.getContext()
                        .getString(R.string.ota_screen_downloading_firmware));
                break;
            case UNZIPPING:
                textView.setText(textView.getContext()
                        .getString(R.string.ota_screen_state_unzipping));
                break;
            case UPLOADING_TO_DEVICE:
                progressValue = textView.getContext()
                        .getString(R.string.ota_screen_state_uploading_to_device) +
                        getPercentString(percentValue);
                textView.setText(progressValue);

                break;
            case FLASHING_DEVICE:
                progressValue = textView.getContext()
                        .getString(R.string.ota_screen_state_updating) +
                        getPercentString(percentValue);
                textView.setText(progressValue);
                break;
            case COMPLETED:
                textView.setText(textView.getContext()
                        .getString(R.string.ota_screen_state_completed));
                break;
        }
    }

    /**
     * Set label and image for home screen list links extracted from {@link HomeScreenItemLinkResources}.
     *
     * @param view view that will be setup.
     * @param data to be set on that view.
     */
    @BindingAdapter("linkData")
    public static void setLinkData(@NonNull HomeItemLinkLayout view, HomeScreenItemLinkResources data) {
        view.setLinkResources(data);
    }

    @NonNull
    private static String getPercentString(@Nullable Double percentValue) {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());
        otherSymbols.setDecimalSeparator('.');
        otherSymbols.setGroupingSeparator(',');
        DecimalFormat decimalFormat = new DecimalFormat("##0.00", otherSymbols);
        return " (" + decimalFormat.format(percentValue != null ? percentValue : 0.0) + "%)";
    }
}

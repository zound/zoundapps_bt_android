package com.zoundindustries.marshallbt.utils.comparators;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

import java.util.Comparator;

/**
 * Utility class to sort home list based on the device connection state.
 */
public class HomeListConnectionComparator implements Comparator<BaseDevice> {

    @Override
    public int compare(BaseDevice device1, BaseDevice device2) {
        if (isDeviceConnected(device1) && !isDeviceConnected(device2)) {
            return -1;
        } else if (!isDeviceConnected(device1) && isDeviceConnected(device2)) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Check if the device is in CONNECTED state.
     *
     * @param device to be checked.
     * @return true if CONNECTED, false otherwise
     */
    private boolean isDeviceConnected(BaseDevice device) {
        return device.getBaseDeviceStateController()
                .isFeatureSupported(FeaturesDefs.CONNECTION_INFO) &&
                device.getBaseDeviceStateController().outputs.getConnectionInfoCurrentValue() ==
                        BaseDevice.ConnectionState.CONNECTED;
    }
}

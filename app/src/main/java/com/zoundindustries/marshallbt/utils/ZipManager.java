package com.zoundindustries.marshallbt.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Utility class for handling zipped files.
 */
public class ZipManager {

    /**
     * Method extracting files from selected destination.
     *
     * @param zipFile     source file to be extracted.
     * @param destination output folder of zip extraction.
     * @throws IOException
     */
    public static void unzipFiles(File zipFile, String destination) throws IOException {
        File file = new File(destination);
        if (!file.isDirectory()) {
            file.mkdirs();
        }

        try (ZipInputStream zipInputStream = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFile)))) {
            saveZipStream(destination, zipInputStream);
        }
    }

    private static void saveZipStream(String destination, ZipInputStream zipInputStream) throws IOException {
        ZipEntry zipEntry;

        while ((zipEntry = zipInputStream.getNextEntry()) != null) {
            String path = destination + File.separator + zipEntry.getName();

            if (zipEntry.isDirectory()) {
                File unzipFile = new File(path);
                if (!unzipFile.isDirectory()) {
                    unzipFile.mkdirs();
                }
            } else {
                writeToPath(zipInputStream, path);
            }
        }
    }

    private static void writeToPath(ZipInputStream zipInputStream, String path) throws IOException {
        byte buffer[] = new byte[1024];
        int n;

        try (BufferedOutputStream fileOutputStream =
                     new BufferedOutputStream(new FileOutputStream(path, false))) {
            //todo think of getting full progress from here.
            while ((n = zipInputStream.read(buffer, 0, 1024)) >= 0) {
                fileOutputStream.write(buffer, 0, n);
            }
            zipInputStream.closeEntry();
        }
    }
}
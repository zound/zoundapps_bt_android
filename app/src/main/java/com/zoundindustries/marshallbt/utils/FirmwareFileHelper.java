package com.zoundindustries.marshallbt.utils;

import android.content.Context;
import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.model.ota.pojo.FirmwareFileMetaData;

import java.io.File;
import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import okhttp3.ResponseBody;

/**
 * Wrapper class for encapsulating FW file handling and unit testing.
 */
public class FirmwareFileHelper {

    private static final String BIN_FW_FILE_PATH = "/unzipped";
    private static final String BIN_FW_FILE_NAME = "upgrade.bin";
    private static final String MD5_FW_FILE_NAME = "upgrade.md5";

    private final Context context;
    private PublishSubject<Boolean> filesUnzipSubject = PublishSubject.create();

    public FirmwareFileHelper(@NonNull Context context) {
        this.context = context;
    }

    /**
     * Method for getting base for storing firmware related files.
     *
     * @return base path.
     */
    public String getStoragePathBase() {
        return NetworkUtils.getStoragePathBase(context);
    }

    /**
     * Method creating firmware update file for download.
     *
     * @return newly created firmware file.
     */
    public File getFirmwareBinaryFile() {
        return new File(getStoragePathBase() + BIN_FW_FILE_PATH + File.separator +
                BIN_FW_FILE_NAME);
    }

    /**
     * Method returning file for md5 sum checking.
     *
     * @return md5 file with upgrade file checksum
     */
    public File getFirmwareMd5File() {
        return new File(getStoragePathBase() + BIN_FW_FILE_PATH + File.separator +
                MD5_FW_FILE_NAME);
    }

    /**
     * Method used to delete whole unzip directory
     */
    public void deleteUnzippedFiles() {
        deleteRecursive(new File(getStoragePathBase() + BIN_FW_FILE_PATH));
    }

    /**
     * Creates a new zip {@link File} based on the firmware file properties.
     * The file name will be as follows:
     * <brand>_<model>_<version>.zip
     *
     * @param firmwareFile object use to create a {@link File}.
     * @return newly created {@link File}
     */
    @NonNull
    public File getFirmwareZipFile(@NonNull FirmwareFileMetaData firmwareFile) {
        return FirmwareFileMetaData.getFirmwareFile(firmwareFile, this);
    }

    /**
     * Unzip firmware files into 'unzipped' folder.
     *
     * @param firmwareZipFile file to be unzipped.
     * @throws IOException
     */
    @NonNull
    public Observable<Boolean> unzipFirmwareFiles(@NonNull File firmwareZipFile) {
        new Thread(() -> {
            try {
                ZipManager.unzipFiles(firmwareZipFile, getStoragePathBase() +
                        BIN_FW_FILE_PATH);
            } catch (IOException e) {
                filesUnzipSubject.onNext(false);
                e.printStackTrace();
                return;
            }
            filesUnzipSubject.onNext(true);
        }).start();
        return filesUnzipSubject;
    }

    /**
     * Used to handle data downloaded via Retrofit and save it to specific location
     *
     * @param body     response of download server
     * @param saveFile location on which data will be saved
     * @return boolean with success state
     */
    public boolean writeResponseBodyToDisk(@NonNull ResponseBody body,
                                           @NonNull File saveFile) {
        return NetworkUtils.writeResponseBodyToDisk(body, saveFile);
    }

    private static void deleteRecursive(File file) {
        if (file.isDirectory()) {
            for (File child : file.listFiles()) {
                deleteRecursive(child);
            }
        }

        file.delete();
    }
}

package com.zoundindustries.marshallbt.utils.joplin;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.zoundindustries.marshallbt.model.devicesettings.EQData;
import com.zoundindustries.marshallbt.model.devicesettings.EQPreset;
import com.zoundindustries.marshallbt.utils.BaseEQPresetListManager;
import com.zoundindustries.marshallbt.utils.CommonPreferences;
import com.zoundindustries.marshallbt.utils.EqPresetType;

import java.util.ArrayList;
import java.util.List;

/**
 * This is used to handle list of EQ presets for Joplin Speakers.
 */
public class JoplinEQPresetListManager extends BaseEQPresetListManager {

    public JoplinEQPresetListManager(Context context) {
        super(context);
    }

    @VisibleForTesting
    public JoplinEQPresetListManager(@NonNull CommonPreferences preferences) {
        super(preferences);
    }

    @Override
    protected List<EQPreset> getPredefinedPresets() {
        List<EQPreset> predefinedPresets = new ArrayList<>();

        predefinedPresets.add(new EQPreset(new EQData(new int[]{10, 10, 10, 10, 10}), EqPresetType.CUSTOM));
        predefinedPresets.add(new EQPreset(new EQData(new int[]{50, 50, 50, 50, 50}), EqPresetType.FLAT));
        predefinedPresets.add(new EQPreset(new EQData(new int[]{80, 60, 30, 50, 70}), EqPresetType.ROCK));
        predefinedPresets.add(new EQPreset(new EQData(new int[]{80, 30, 50, 70, 80}), EqPresetType.METAL));
        predefinedPresets.add(new EQPreset(new EQData(new int[]{60, 70, 80, 40, 50}), EqPresetType.POP));
        predefinedPresets.add(new EQPreset(new EQData(new int[]{80, 70, 60, 50, 50}), EqPresetType.HIPHOP));
        predefinedPresets.add(new EQPreset(new EQData(new int[]{70, 40, 40, 70, 60}), EqPresetType.ELECTRONIC));
        predefinedPresets.add(new EQPreset(new EQData(new int[]{40, 70, 50, 40, 50}), EqPresetType.JAZZ));

        return predefinedPresets;
    }
}
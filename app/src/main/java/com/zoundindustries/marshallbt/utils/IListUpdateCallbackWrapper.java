package com.zoundindustries.marshallbt.utils;

import androidx.recyclerview.widget.ListUpdateCallback;

public interface IListUpdateCallbackWrapper extends ListUpdateCallback {

    @Override
    default void onInserted(int position, int count) {
        //nop
    }

    @Override
    default void onRemoved(int position, int count) {
        //nop
    }

    @Override
    default void onMoved(int fromPosition, int toPosition) {
        //nop
    }

    @Override
    default void onChanged(int position, int count, Object payload) {
        //nop
    }
}

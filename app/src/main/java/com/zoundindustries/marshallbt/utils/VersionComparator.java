package com.zoundindustries.marshallbt.utils;

import androidx.annotation.NonNull;

/**
 * Utility class for FW version checking
 */
public class VersionComparator {

    public static final String SPLIT_REGEX = "\\.";
    public static final String VERSION_TRIM_CHAR = "(";

    /**
     * Compare two string versions of FW in format iof <value1>.<value2>.<value3>.etc
     *
     * @param currentVersion version that is used as a base of comparison.
     * @param newVersion     currentVersion is compared to this value.
     * @return true if newVersion is newer than currentVersion, flase otherwise.
     */
    public static boolean isNewerVersion(@NonNull String currentVersion, @NonNull String newVersion) {
        if (currentVersion.length() == 0
                || newVersion.length() == 0
                || currentVersion.equals(newVersion)) {
            return false;
        }

        String[] currentValues = currentVersion.split(SPLIT_REGEX);
        String[] newValues = newVersion.split(SPLIT_REGEX);

        int i = 0;

        // iterate to where numbers don't match anymore
        while (i < currentValues.length && i < newValues.length &&
                currentValues[i].equals(newValues[i])) {
            i++;
        }

        if (i < currentValues.length && i < newValues.length) {
            // check the value of the first different number
            return Integer.valueOf(currentValues[i]).compareTo(Integer.valueOf(newValues[i])) < 0;
        } else {
            // if newValues is longer, then it's newer
            return currentValues.length - newValues.length < 0;
        }
    }

    /**
     * Trim speaker version from the following format:
     * <value1>.<value2>.<value3>(<subvalues>)
     * to the following format
     * <value1>.<value2>.<value3>
     *
     * @param version {@link String} to be trimmed.
     * @return trimmed {@link String}.
     */
    public static String trimVersion(String version) {
        return version.substring(0, version.indexOf(VERSION_TRIM_CHAR));
    }
}

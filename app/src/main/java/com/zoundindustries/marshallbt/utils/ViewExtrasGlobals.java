package com.zoundindustries.marshallbt.utils;

public final class ViewExtrasGlobals {
    public static final String EXTRA_DEVICE_ID = "extra_device_id";
    public static final String EXTRA_OTA_FIRMWARE_META_DATA = "extra_firmware_meta_data";
    public static final String EXTRA_PAIRING_MODE_ENABLED = "extra_pairing_mode_enabled";
    public static final String EXTRA_PAIRING_MODE_ENABLED_DEVICE_NAME = "extra_pairing_mode_enabled_device_name";
    public static final String EXTRA_QUICK_GUIDE_ENTER_ANIMATION = "extra_quick_guide_enter_animation";
}

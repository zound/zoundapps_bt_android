package com.zoundindustries.marshallbt.utils;

import android.os.Environment;
import android.util.Base64;

import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.model.adapters.mock.MockSDK;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.mock.MockSpeaker;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for getting mock configuration from web server.
 * Used for testing purpose to allow remote input data configuration.
 */
public class MockConfigurator {

    private static final String CONFIG_FILE_NAME = "mock_config.json";
    private static final String WEB_USER_NAME = "srv4cntub";
    private static final String WEB_PASSWORD = "qwerty123#";
    private static final int BUFFER_SIZE = 4096;
    private static final String DEFAULT_CODING = "UTF-8";

    private static final String basicAuth = "Basic " + Base64.encodeToString((WEB_USER_NAME + ":" +
            WEB_PASSWORD).getBytes(), Base64.NO_WRAP);

    private static final String INPUT_CONFIGURATION_PATH =
            "https://artifactorypro.shared.pub.tds.tieto.com/artifactory/zoundindustries/test/" +
                    CONFIG_FILE_NAME;
    private static final String OUTPUT_CONFIGURATION_PATH =
            Environment.getExternalStorageDirectory().getPath();

    //JSON keys
    private static final String JSON_KEY_CONNECTED = "connected";
    private static final String JSON_KEY_CONTINUOUS_SCAN = "continuousScan";
    private static final String JSON_KEY_DEVS = "devs";
    private static final String JSON_KEY_HAS_UPDATE = "hasUpdate";
    private static final String JSON_KEY_ID = "id";
    private static final String JSON_KEY_NAME = "name";
    private static final String JSON_KEY_PLAYING = "playing";
    private static final String JSON_KEY_SCAN_DELAY = "scanDelay";

    private MockSDK mockSDK;


    /**
     * Constructor for MockConfigurator
     *
     * @param mockSDK mock sdk instance used for calling SDK specific code
     */
    public MockConfigurator(@NonNull MockSDK mockSDK) {
        this.mockSDK = mockSDK;
    }

    /**
     * Gets config JSON file (CONFIG_FILE_NAME) from the server (INPUT_CONFIGURATION_PATH),
     * saves it into filesystem (OUTPUT_CONFIGURATION_PATH) and parse JSON values into mock objects.
     *
     * @return config received from the remote server
     */
    @NonNull
    public MockConfig getMockConfiguration() throws IOException, ParseException {
        MockConfig config;
        getConfigurationFile();
        config = parseConfigFile();
        return config;
    }

    private void getConfigurationFile() throws IOException {
        URL url;
        url = new URL(INPUT_CONFIGURATION_PATH);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("Authorization", basicAuth);

        InputStream input = connection.getInputStream();

        File outputDir = new File(OUTPUT_CONFIGURATION_PATH);

        if (!outputDir.exists()) {
            outputDir.mkdirs();
        }

        FileOutputStream output = new FileOutputStream(OUTPUT_CONFIGURATION_PATH + "/"
                + CONFIG_FILE_NAME);

        byte data[] = new byte[BUFFER_SIZE];
        int count;
        while ((count = input.read(data)) != -1) {
            output.write(data, 0, count);
        }
        output.close();
        input.close();
    }

    private MockConfig parseConfigFile() throws IOException, ParseException {
        File initialFile = new File(OUTPUT_CONFIGURATION_PATH + "/" + CONFIG_FILE_NAME);
        List<String> updateDevices = new ArrayList<>();

        JSONObject jsonObject = (JSONObject) new JSONParser()
                .parse(new InputStreamReader(new FileInputStream(initialFile), DEFAULT_CODING));

        MockConfig.Builder builder = new MockConfig.Builder()
                .setScanDelay(((Long) jsonObject.get(JSON_KEY_SCAN_DELAY)).intValue())
                .setContinuousScan((boolean) jsonObject.get(JSON_KEY_CONTINUOUS_SCAN));

        JSONArray devices = (JSONArray) jsonObject.get(JSON_KEY_DEVS);
        List<BaseDevice> baseDevices = new ArrayList<>();

        for (Object obj : devices) {
            String name = (String) ((JSONObject) obj).get(JSON_KEY_NAME);
            String id = (String) ((JSONObject) obj).get(JSON_KEY_ID);
            boolean isPlaying = (boolean) ((JSONObject) obj).get(JSON_KEY_PLAYING);
            boolean connected = (boolean) ((JSONObject) obj).get(JSON_KEY_CONNECTED);
            boolean hasUpdate = (boolean) ((JSONObject) obj).get(JSON_KEY_HAS_UPDATE);

            BaseDevice newDevice = new MockSpeaker(new DeviceInfo(id,
                    DeviceInfo.DeviceType.MOCK,
                    DeviceInfo.DeviceGroup.SPEAKER,
                    name,
                    DeviceInfo.DeviceColor.BLACK),
                    mockSDK);

            newDevice.getBaseDeviceStateController().inputs.setPlaying(isPlaying);
            //todo mock configurator should be redesigned to represent Connection states
            newDevice.getBaseDeviceStateController().inputs.setConnectionInfo(connected ?
                    BaseDevice.ConnectionState.READY_TO_CONNECT :
                    BaseDevice.ConnectionState.DISCONNECTED);
            baseDevices.add(newDevice);
            if (hasUpdate) {
                updateDevices.add(id);
            }

        }
        builder.setDevices(baseDevices).setUpdateDevices(updateDevices);
        return builder.create();
    }
}
package com.zoundindustries.marshallbt.utils.joplin;

import androidx.annotation.NonNull;

import static com.tym.tymappplatform.utils.ToneEQ.HexString2Bytes;

/**
 * Method of this class inherited from Tymphany code.
 */
public class ConversionUtils {

    /**
     * Get MAC address in form of bytes.
     *
     * @param mac in form of {@link String}
     * @return bytes for MAC address.
     */
    public static byte[] getBytesFromMac(@NonNull String mac) {
        String[] macSting = mac.split(":");
        byte[] macBytes = new byte[6];
        for (int i = 0; i < macSting.length; i++) {
            byte[] newByte = HexString2Bytes(macSting[i]);
            macBytes[i] = newByte[0];
        }
        return macBytes;
    }

    /**
     * Get {@link String} value for MAC address passed as bytes array.
     *
     * @param bytes array with MAC address.
     * @return String form of MAC address with added ":" divider.
     */
    @NonNull
    public static String bytes2HexString(@NonNull byte[] bytes) {
        byte[] hex = "0123456789ABCDEF".getBytes();
        byte[] buff = new byte[2 * bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            buff[2 * i] = hex[(bytes[i] >> 4) & 0x0f];
            buff[2 * i + 1] = hex[bytes[i] & 0x0f];
        }

        String address = new String(buff);
        return address.replaceAll(".{2}(?=.)", "$0:");
    }
}

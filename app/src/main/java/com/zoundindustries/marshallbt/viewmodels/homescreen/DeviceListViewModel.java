package com.zoundindustries.marshallbt.viewmodels.homescreen;

import android.app.Application;
import android.content.Context;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.DiffUtil;

import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.discovery.DeviceDiscoveryManager;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.model.ota.pojo.SpeakerInfoCache;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.ui.homescreen.DeviceListFragment;
import com.zoundindustries.marshallbt.utils.BluetoothSystemUtils;
import com.zoundindustries.marshallbt.utils.ListDiffCallback;
import com.zoundindustries.marshallbt.utils.comparators.HomeListConnectionComparator;
import com.zoundindustries.marshallbt.utils.comparators.HomeListCouplingComparator;
import com.zoundindustries.marshallbt.utils.comparators.HomeListPairingComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;


public interface DeviceListViewModel {

    interface Inputs {

        /**
         * Triggers when use swipe gesture on SwipeRefreshLayout
         */
        void onRefresh();

        /**
         * Trigger start of scan of devices. Default time period for scan is 5 seconds.
         */
        void startScanDevices();

        /**
         * Indicates empty list to the viewModel.
         */
        void onListEmpty();

        /**
         * Send firebase event for no devices found if location is disabled scenario.
         */
        void notifyFirebaseNoDevicesFoundIfNoLocation();

        /**
         * Notify viewModel that list view was created.
         */
        void onListCreated();
    }

    interface Outputs {

        /**
         * Provides LiveData with diff object for the home devices list.
         * {@link androidx.recyclerview.widget.DiffUtil.DiffResult} is handling needed operations
         * on the list: adding, removing, moving.
         *
         * @return LiveData list object with disappeared device.
         */
        @NonNull
        MutableLiveData<DiffUtil.DiffResult> getListDiff();

        /**
         * Provides LiveData with {@link BaseDevice} object that was removed from home screen.
         *
         * @return LiveData with removed device.
         */
        @NonNull
        MutableLiveData<BaseDevice> getDeviceRemoved();

        /**
         * Get home devices list.
         *
         * @return home devices list.
         */
        @NonNull
        List<BaseDevice> getHomeDevices();

        /**
         * Provides LiveData to show when the scan stopped.
         *
         * @return LiveData boolean, true if scan stopped
         */
        @NonNull
        MutableLiveData<Boolean> hasScanStopped();

        /**
         * Get {@link io.reactivex.Observable} for changed view.
         *
         * @return viewType to be started.
         */
        @NonNull
        MutableLiveData<ViewType> isViewChanged();
    }

    /**
     * Class providing data to {@link DeviceListFragment}.
     * It handles changes in the main device list in the system.
     */
    class Body extends AndroidViewModel implements Outputs, Inputs {

        private static final int SCAN_PERIOD_MILLIS = 5000;

        public final Outputs outputs = this;
        public final Inputs inputs = this;

        private final MutableLiveData<ViewType> viewChange;
        private final MutableLiveData<Boolean> hasScanStopped;
        private final MutableLiveData<DiffUtil.DiffResult> listDiffResult;
        private final MutableLiveData<BaseDevice> deviceRemoved;

        private final List<BaseDevice> homeDevices;
        private final DeviceManager deviceManager;
        private final Comparator<BaseDevice> homeListComparator;
        private final Handler handler;

        private DeviceDiscoveryManager deviceDiscoveryManager;
        private final FirebaseAnalyticsManager firebaseAnalyticsManager;

        private HashMap<String, Disposable> deviceCouplingStateDisposables;
        private HashMap<String, Disposable> deviceConnectionStateDisposables;

        private Disposable serviceReadyDisposable;
        private Disposable serviceReadyForScanDisposable;
        private Disposable scanAddDeviceDisposable;
        private Disposable scanRemoveDeviceDisposable;

        /**
         * Constructor for DeviceListViewModel.
         *
         * @param application context needed for starting DeviceService.
         * @param homeDevices devices list to be shown in UI.
         */
        public Body(@NonNull Application application, @NonNull List<BaseDevice> homeDevices) {
            this(application,
                    homeDevices,
                    new DeviceDiscoveryManager(application),
                    new DeviceManager(application),
                    new BluetoothSystemUtils(),
                    ((BluetoothApplication) application).getAppComponent().firebaseWrapper());
        }

        /**
         * Constructor with {@link DeviceDiscoveryManager} parameter.
         *
         * @param application              context needed for starting DeviceService.
         * @param homeDevices              devices list to be shown in UI.
         * @param speakersDiscoveryManager to be used to discover devices.
         * @param deviceManager            to be used to get devices info.
         */
        @VisibleForTesting
        public Body(@NonNull Application application,
                    @NonNull List<BaseDevice> homeDevices,
                    @NonNull DeviceDiscoveryManager speakersDiscoveryManager,
                    @NonNull DeviceManager deviceManager,
                    @NonNull BluetoothSystemUtils bluetoothSystemUtils,
                    @NonNull FirebaseAnalyticsManager firebaseAnalyticsManager) {
            super(application);

            viewChange = new MutableLiveData<>();
            hasScanStopped = new MutableLiveData<>();
            listDiffResult = new MutableLiveData<>();
            deviceRemoved = new MutableLiveData<>();

            this.homeDevices = homeDevices;
            this.deviceDiscoveryManager = speakersDiscoveryManager;
            this.deviceManager = deviceManager;
            handler = new Handler(Looper.getMainLooper());

            deviceCouplingStateDisposables = new HashMap<>();
            deviceConnectionStateDisposables = new HashMap<>();

            this.homeListComparator = createHomeListComparator(new HomeListPairingComparator(
                            bluetoothSystemUtils),
                    new HomeListConnectionComparator(),
                    new HomeListCouplingComparator(deviceManager));

            this.firebaseAnalyticsManager = firebaseAnalyticsManager;


        }

        @NonNull
        @Override
        public MutableLiveData<DiffUtil.DiffResult> getListDiff() {
            return listDiffResult;
        }

        @NonNull
        @Override
        public MutableLiveData<BaseDevice> getDeviceRemoved() {
            return deviceRemoved;
        }

        @NonNull
        @Override
        public List<BaseDevice> getHomeDevices() {
            return homeDevices;
        }

        @NonNull
        @Override
        public MutableLiveData<Boolean> hasScanStopped() {
            return hasScanStopped;
        }

        @NonNull
        @Override
        public MutableLiveData<ViewType> isViewChanged() {
            return viewChange;
        }

        @Override
        public void onRefresh() {
            if (!deviceDiscoveryManager.isDeviceScanInProgress()) {
                deviceDiscoveryManager.startScanForDevices();
                hasScanStopped.setValue(false);

                handler.postDelayed(() -> {
                    deviceDiscoveryManager.stopScanForDevices();
                    hasScanStopped.setValue(true);
                }, SCAN_PERIOD_MILLIS);
            } else {
                hasScanStopped.setValue(true);
            }

            for (BaseDevice homeDevice : homeDevices) {
                homeDevice.getBaseDeviceStateController().inputs.readVolume();
                homeDevice.getBaseDeviceStateController().inputs.readAudioStatus();
            }
        }

        @Override
        public void startScanDevices() {
            setupServiceReadyObserver();
        }

        @Override
        public void onListEmpty() {
            viewChange.setValue(ViewType.EMPTY_DEVICE_LIST);
        }

        @Override
        public void notifyFirebaseNoDevicesFoundIfNoLocation() {
            LocationManager locationManager = (LocationManager)
                    getApplication().getSystemService(Context.LOCATION_SERVICE);
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (Build.VERSION.SDK_INT >= 23) {
                    viewChange.setValue(ViewType.GLOBAL_SETTINGS_LOCATION);
                }

                firebaseAnalyticsManager.eventAppNoDevicesFoundWithNoLocation(
                        String.valueOf(Build.VERSION.SDK_INT));
            }
        }

        @Override
        public void onListCreated() {
            serviceReadyForScanDisposable = deviceDiscoveryManager.isServiceReady()
                    .observeOn(AndroidSchedulers.mainThread())
                    .take(1)
                    .subscribe(serviceConnected -> {
                        if (serviceConnected) {
                            deviceManager.setScanAllowed(true);
                        }
                    });
        }

        @Override
        protected void onCleared() {
            dispose();
        }

        private static Comparator<BaseDevice> createHomeListComparator(
                @NonNull HomeListPairingComparator homeListPairingComparator,
                @NonNull HomeListConnectionComparator homeListConnectionComparator,
                @NonNull HomeListCouplingComparator homeListCouplingComparator) {

            return (device1, device2) -> {
                int result = homeListCouplingComparator.compare(device1, device2);
                if (result != 0) {
                    return result;
                } else {
                    result = homeListPairingComparator.compare(device1, device2);
                    if (result != 0) {
                        return result;
                    } else {
                        return homeListConnectionComparator.compare(device1, device2);
                    }
                }
            };
        }

        private void setupServiceReadyObserver() {
            serviceReadyDisposable = deviceDiscoveryManager.isServiceReady()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(serviceConnected -> {
                        if (serviceConnected) {
                            initObservers();
                        }
                    });
        }

        private void initObservers() {
            scanAddDeviceDisposable = deviceDiscoveryManager.getDiscoveredDevices()
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(disposable -> {
                        for (BaseDevice device : deviceManager.getCurrentDevices()) {
                            handleNewDeviceDiscovered(device);
                        }
                    })
                    .subscribe(this::handleNewDeviceDiscovered);

            scanRemoveDeviceDisposable = deviceDiscoveryManager.getDisappearedDevices()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(device -> {
                        ArrayList<BaseDevice> tempDevices = new ArrayList<>();
                        tempDevices.addAll(homeDevices);
                        if (homeDevices.contains(device)) {
                            homeDevices.remove(device);
                            deviceRemoved.setValue(device);
                            deviceCouplingStateDisposables.remove(device.getDeviceInfo().getId());
                            deviceConnectionStateDisposables.remove(device.getDeviceInfo().getId());
                            if (homeDevices.isEmpty()) {
                                onListEmpty();
                            }
                        }
                    });
        }

        private void handleNewDeviceDiscovered(@NonNull BaseDevice device) {
            if (viewChange.getValue() == null ||
                    viewChange.getValue() == ViewType.EMPTY_DEVICE_LIST) {
                viewChange.setValue(ViewType.NON_EMPTY_DEVICE_LIST);
            }

            if (!homeDevices.contains(device)) {
                ArrayList<BaseDevice> tempDevices = new ArrayList<>();
                tempDevices.addAll(homeDevices);
                homeDevices.add(device);
                deviceConnectionStateDisposables.put(device.getDeviceInfo().getId(),
                        device.getBaseDeviceStateController().outputs.getConnectionInfo()
                                .observeOn(AndroidSchedulers.mainThread())
                                .distinctUntilChanged()
                                .subscribe(connectionState -> {
                                            Collections.sort(homeDevices, homeListComparator);
                                            handleDividerPosition();
                                        }
                                ));
                handleListChange(tempDevices);
            }
        }

        private void handleDividerPosition() {
            boolean dividerReset = false;
            for (int i = 0; i < homeDevices.size(); i++) {

                //Check if there is divider needed on the list for the following logic:
                //  - Show divider if current device is the last connected one
                //  - Do nto show divider if the last connected one is the last in the list.
                if ((isDeviceConnected(homeDevices.get(i)) ||
                        isDeviceCoupled(homeDevices.get(i))) &&
                        checkIfNextDeviceAvailable(i) &&
                        !isDeviceConnected(homeDevices.get(i + 1)) &&
                        !isDeviceCoupled(homeDevices.get(i + 1))) {
                    dividerReset = true;
                    deviceManager.setDividerDeviceId(homeDevices.get(i).getDeviceInfo().getId());
                    break;
                }
            }

            if (!dividerReset) {
                deviceManager.setDividerDeviceId("");
            }
        }

        private boolean isDeviceConnected(@NonNull BaseDevice baseDevice) {
            return baseDevice
                    .getBaseDeviceStateController().outputs.getConnectionInfoCurrentValue() ==
                    BaseDevice.ConnectionState.CONNECTED;
        }

        private boolean isDeviceCoupled(@NonNull BaseDevice device) {
            SpeakerInfoCache speakerInfoCache =
                    deviceManager.getSpeakerInfoCache(device.getDeviceInfo().getId());
            return speakerInfoCache != null && speakerInfoCache.getIsTwsCoupled();
        }

        private boolean checkIfNextDeviceAvailable(int i) {
            return homeDevices.size() > 1 && homeDevices.size() >= i + 2;
        }

        private void handleListChange(@NonNull ArrayList<BaseDevice> oldList) {
            Collections.sort(homeDevices, homeListComparator);
            ListDiffCallback listDiffCallback =
                    new ListDiffCallback(oldList, homeDevices);
            listDiffResult.setValue(DiffUtil.calculateDiff(listDiffCallback));
        }

        private void dispose() {
            deviceDiscoveryManager.dispose();
            handler.removeCallbacksAndMessages(null);

            for (Map.Entry<String, Disposable> deviceEntry :
                    deviceConnectionStateDisposables.entrySet()) {
                deviceEntry.getValue().dispose();
            }

            for (Map.Entry<String, Disposable> deviceEntry :
                    deviceCouplingStateDisposables.entrySet()) {
                deviceEntry.getValue().dispose();
            }

            if (scanAddDeviceDisposable != null) {
                scanAddDeviceDisposable.dispose();
            }

            if (scanRemoveDeviceDisposable != null) {
                scanRemoveDeviceDisposable.dispose();
            }

            if (serviceReadyDisposable != null) {
                serviceReadyDisposable.dispose();
            }

            if (serviceReadyForScanDisposable != null) {
                serviceReadyForScanDisposable.dispose();
            }
        }
    }
}

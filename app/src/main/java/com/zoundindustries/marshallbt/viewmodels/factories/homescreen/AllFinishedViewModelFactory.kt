package com.zoundindustries.marshallbt.viewmodels.factories.homescreen

import android.app.Application
import androidx.lifecycle.ViewModel
import com.zoundindustries.marshallbt.viewmodels.factories.BaseDeviceViewModelFactory
import com.zoundindustries.marshallbt.viewmodels.homescreen.AllFinishedViewModel

class AllFinishedViewModelFactory(application: Application, deviceId: String ) : BaseDeviceViewModelFactory(application, deviceId) {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return AllFinishedViewModel.Body(application, deviceId) as T
    }
}
package com.zoundindustries.marshallbt.viewmodels.factories.ota;

import android.app.Application;
import androidx.lifecycle.ViewModel;
import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.viewmodels.factories.BaseDeviceViewModelFactory;
import com.zoundindustries.marshallbt.viewmodels.ota.OTACompletedViewModel;

/**
 * Factory providing new OTACompleted ViewModel if needed or old object if it exists
 */
public class OTACompletedViewModelFactory extends BaseDeviceViewModelFactory {
    public OTACompletedViewModelFactory(Application application, String deviceId) {
        super(application, deviceId);
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new OTACompletedViewModel.Body(application, deviceId);
    }
}

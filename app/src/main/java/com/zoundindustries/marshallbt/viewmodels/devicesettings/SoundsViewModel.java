package com.zoundindustries.marshallbt.viewmodels.devicesettings;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import android.view.View;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.device.SoundNotificationInfo;
import com.zoundindustries.marshallbt.model.device.state.BaseDeviceStateController;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * Class used to handle the business logic for setting sound notifications on the device.
 */
public interface SoundsViewModel {
    interface Inputs {

        /**
         * Indicates that user changed switch value for power notifications.
         *
         * @param view that was clicked.
         */
        void onPowerSwitched(@NonNull View view, boolean isChecked);

        /**
         * Indicates that user changed switch value for media control notifications.
         *
         * @param view that was clicked.
         */
        void onMediaControlSwitched(@NonNull View view, boolean isChecked);
    }

    interface Outputs {

        /**
         * Notify about power notifications switch state
         *
         * @return switch state of power sound notifications.
         */
        @NonNull
        MutableLiveData<Boolean> isPowerSoundEnabled();

        /**
         * Notify about media control notifications switch state.
         *
         * @return switch state of media control sound notifications.
         */
        @NonNull
        MutableLiveData<Boolean> isMediaControlSoundEnabled();

        @NonNull
        LiveData<Boolean> isMediaControlSoundSupported();

        /**
         * Get {@link MutableLiveData} for changed view.
         *
         * @return viewType for screen change
         */
        @NonNull
        MutableLiveData<ViewType> isViewChanged();
    }

    final class Body extends AndroidViewModel implements Inputs, Outputs {

        public final Inputs inputs = this;
        public final Outputs outputs = this;

        private final MutableLiveData<Boolean> powerSwitched;
        private final MutableLiveData<Boolean> mediaControlSwitched;
        private final MutableLiveData<Boolean> mediaControlSupported;
        private final MutableLiveData<ViewType> viewChanged;

        private final DeviceManager deviceManager;
        private BaseDevice device;

        private Disposable serviceReadyDisposable;
        private Disposable soundNotificationDisposable;
        private Disposable connectionDisposable;

        /**
         * Constructor for {@link SoundsViewModel} Body.
         *
         * @param application context to be used for Managers initialization.
         * @param deviceId    for changing sounds settings.
         */
        public Body(@NonNull Application application, @NonNull String deviceId) {
            this(application, deviceId, new DeviceManager(application));
        }

        /**
         * Constructor for {@link SoundsViewModel} Body.
         *
         * @param application   context to be used for Managers initialization.
         * @param deviceId      for changing sounds settings.
         * @param deviceManager to get device.
         */
        @VisibleForTesting
        public Body(@NonNull Application application,
                    @NonNull String deviceId,
                    @NonNull DeviceManager deviceManager) {
            super(application);

            powerSwitched = new MutableLiveData<>();
            mediaControlSwitched = new MutableLiveData<>();
            mediaControlSupported = new MutableLiveData<>();
            viewChanged = new MutableLiveData<>();

            this.deviceManager = deviceManager;

            initDefaultLiveDataStates();

            initObservers(deviceId);
        }

        @Override
        public void onPowerSwitched(@NonNull View view, boolean isChecked) {
            powerSwitched.setValue(isChecked);
            updateSoundNotificationValues();
        }

        @Override
        public void onMediaControlSwitched(@NonNull View view, boolean isChecked) {
            mediaControlSwitched.setValue(isChecked);
            updateSoundNotificationValues();
        }

        @NonNull
        @Override
        public MutableLiveData<Boolean> isPowerSoundEnabled() {
            return powerSwitched;
        }

        @NonNull
        @Override
        public MutableLiveData<Boolean> isMediaControlSoundEnabled() {
            return mediaControlSwitched;
        }

        @NonNull
        @Override
        public LiveData<Boolean> isMediaControlSoundSupported() {
            return mediaControlSupported;
        }

        @NonNull
        @Override
        public MutableLiveData<ViewType> isViewChanged() {
            return viewChanged;
        }

        @Override
        protected void onCleared() {
            super.onCleared();
            dispose();
        }

        private void updateSoundNotificationValues() {
            if (device != null) {
                device.getBaseDeviceStateController()
                        .inputs.setSoundNotification(
                        new SoundNotificationInfo(powerSwitched.getValue(),
                                mediaControlSwitched.getValue()));
            }
        }

        private void initDefaultLiveDataStates() {
            powerSwitched.setValue(false);
            mediaControlSwitched.setValue(false);
        }

        private void initObservers(String deviceId) {
            setupDevice(deviceId);
        }

        private void setupDevice(String deviceId) {
            serviceReadyDisposable = deviceManager
                    .isServiceReady()
                    .subscribe(isReady -> {
                        if (isReady) {
                            device = deviceManager.getDeviceFromId(deviceId);
                            if (device != null) {
                                initConnectionObserver();
                                initControlStates();
                            } else {
                                viewChanged.setValue(ViewType.HOME_SCREEN);
                            }
                        }
                    });
        }

        private void initControlStates() {
            if (device != null) {
                BaseDeviceStateController dsc = device.getBaseDeviceStateController();
                mediaControlSupported.setValue(dsc.isFeatureSupported(FeaturesDefs.SOUNDS_SETTINGS_MEDIA_CONTROL));
            }
        }

        private void initConnectionObserver() {
            connectionDisposable = device.getBaseDeviceStateController().outputs
                    .getConnectionInfo()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(connectionState -> {
                        if (connectionState == BaseDevice.ConnectionState.CONNECTED) {
                            device.getBaseDeviceStateController().inputs.readSoundInfoStatus();
                            initSoundNotificationObserver();
                        } else if (connectionState == BaseDevice.ConnectionState.DISCONNECTED) {
                            viewChanged.setValue(ViewType.HOME_SCREEN);
                        }
                    });
        }

        private void initSoundNotificationObserver() {
            soundNotificationDisposable = device.getBaseDeviceStateController().outputs
                    .getSoundNotificationInfo()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(soundNotificationInfo -> {
                        powerSwitched
                                .setValue(soundNotificationInfo.isPowerSoundEnabled());
                        mediaControlSwitched
                                .setValue(soundNotificationInfo.isMediaControlSoundEnabled());
                    });
        }

        private void dispose() {
            if (serviceReadyDisposable != null) {
                serviceReadyDisposable.dispose();
            }

            if (soundNotificationDisposable != null) {
                soundNotificationDisposable.dispose();
            }

            if (connectionDisposable != null) {
                connectionDisposable.dispose();
            }
        }
    }
}

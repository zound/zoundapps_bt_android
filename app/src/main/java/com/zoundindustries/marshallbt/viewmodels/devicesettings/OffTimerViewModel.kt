package com.zoundindustries.marshallbt.viewmodels.devicesettings

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.zoundindustries.marshallbt.model.device.BaseDevice
import com.zoundindustries.marshallbt.model.device.DeviceManager
import com.zoundindustries.marshallbt.ui.ViewFlowController
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

const val TIMER_HOUR_MAX_RANGE = 3
const val TIMER_MINUTE_MAX_RANGE = 55
const val TIMER_MINUTE_STEP = 5
/**
 * Class representing the possible states of timer
 */
enum class TimerState {
    RUNNING,
    STOPPED
}
/**
 * ViewModel for timer off functionality
 */
interface OffTimerViewModel {

    interface Inputs {
        fun startTimer(minuteValue: Int)
        fun stopTimer()
    }

    interface Outputs {
        /**
         * Notify state of timer - running or stopped
         */
        val notifyTimerState: LiveData<TimerState>

        /**
         * Notify actual value of timer
         */
        val notifyTimerValue: LiveData<Int>
        val notifyViewChanged: LiveData<ViewFlowController.ViewType>
    }

    class Body(app: Application, deviceId: String) : AndroidViewModel(app), Inputs, Outputs {

        val inputs = this
        val outputs = this

        private var device: BaseDevice?
        private var deviceManager = DeviceManager(app)

        private var serviceReadyDisposable: Disposable?
        private var timerValueDisposable: Disposable? = null
        private var connectionDisposable: Disposable? = null

        override fun startTimer(minuteValue: Int) {
            device?.baseDeviceStateController?.inputs?.writeTimerValue(minuteValue)
        }

        override fun stopTimer() {
            device?.baseDeviceStateController?.inputs?.writeTimerValue(0)
        }

        override val notifyTimerValue: LiveData<Int>
            get() = timerValue

        override val notifyTimerState: LiveData<TimerState>
            get() = timerState

        override val notifyViewChanged: LiveData<ViewFlowController.ViewType>
            get() = viewChanged

        val viewChanged = MutableLiveData<ViewFlowController.ViewType>()


        private val timerState: MutableLiveData<TimerState> = MutableLiveData()
        private val timerValue: MutableLiveData<Int> = MutableLiveData()

        init {
            device = deviceManager.getDeviceFromId(deviceId)

            serviceReadyDisposable = deviceManager
                    .isServiceReady
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { isReady ->
                        isReady?.let {
                            initObservers()
                            device?.baseDeviceStateController?.inputs?.readAutoOffTimer()
                        }
                    }
        }

        private fun initObservers() {
            timerValueDisposable = device?.baseDeviceStateController?.outputs?.timerValue
                    ?.observeOn(AndroidSchedulers.mainThread())
                    ?.subscribe { value ->
                        if(value == 0) {
                            timerState.value = TimerState.STOPPED
                        } else {
                            timerState.value = TimerState.RUNNING
                            timerValue.value = value
                        }
                    }


            connectionDisposable = device?.baseDeviceStateController?.outputs
                    ?.connectionInfo
                    ?.observeOn(AndroidSchedulers.mainThread())
                    ?.subscribe { connectionState ->
                        if (connectionState == BaseDevice.ConnectionState.DISCONNECTED) {
                            viewChanged.value = ViewFlowController.ViewType.HOME_SCREEN
                        }
                    }
        }

        override fun onCleared() {
            super.onCleared()
            if (serviceReadyDisposable != null) {
                serviceReadyDisposable!!.dispose()
            }

            if (timerValueDisposable != null) {
                timerValueDisposable!!.dispose()
            }

            connectionDisposable?.let{
                if (!it.isDisposed)
                    it.dispose()
            }
        }
    }
}
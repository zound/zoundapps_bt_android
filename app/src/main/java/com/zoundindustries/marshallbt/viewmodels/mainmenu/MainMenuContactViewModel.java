package com.zoundindustries.marshallbt.viewmodels.mainmenu;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.annotation.NonNull;
import android.view.View;

import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.ui.mainmenu.MainMenuContactFragment;



/**
 * ViewModel for {@link MainMenuContactFragment}
 * responsible for handling events from this fragment related to buttons
 */
public interface MainMenuContactViewModel {

    interface Inputs {

        /**
         * Handle GoToWebsite(Contact) button being clicked
         *
         * @param view view on which the method is run
         */
        void onGoToWebsiteButtonClicked(@NonNull View view);

        /**
         * Handle contact support button being clicked
         *
         * @param view view on which the method is run
         */
        void onContactSupportButtonClicked(@NonNull View view);
    }

    interface Outputs {

        /**
         * Get {@link MutableLiveData} for toolbar display text ID.
         *
         * @return LiveData observable
         */
        @NonNull
        MutableLiveData<ViewType> isViewChanged();
    }

    class Body extends ViewModel implements Inputs, Outputs {

        private final MutableLiveData<ViewType> isViewChanged;
        public final Inputs inputs = this;
        public final Outputs outputs = this;

        public Body() {
            isViewChanged = new MutableLiveData<>();
        }

        @Override
        public void onGoToWebsiteButtonClicked(@NonNull View view) {
            isViewChanged.setValue(ViewType.MAIN_MENU_CONTACT_WEBSITE);
        }

        @Override
        public void onContactSupportButtonClicked(@NonNull View view) {
            isViewChanged.setValue(ViewType.MAIN_MENU_CONTACT_SUPPORT);
        }

        @NonNull
        @Override
        public MutableLiveData<ViewType> isViewChanged() {
            return isViewChanged;
        }
    }
}

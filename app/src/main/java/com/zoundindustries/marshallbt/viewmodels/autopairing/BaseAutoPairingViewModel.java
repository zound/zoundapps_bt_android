package com.zoundindustries.marshallbt.viewmodels.autopairing;

import android.app.Application;
import android.bluetooth.BluetoothDevice;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * Constructor extending standard AndroidViewModel class and implementing Auto Pairing callbacks.
 */
public abstract class BaseAutoPairingViewModel extends AndroidViewModel {

    /**
     * Pairing mode is disabled on the device. User needs to enable it with HW button.
     */
    protected abstract void onPairingModeDisabled();

    /**
     * Pairing mode is enabled on the device. Show Fast Pairing steps guide.
     */
    protected abstract void onPairingModeEnabled();

    /**
     * Pairing has failed due to BOND_NONE state or timeout.
     */
    protected abstract void onPairingFailed();

    /**
     * Pairing is done. Device is BT Classic paired.
     */
    protected abstract void onPairingDone();

    protected final DeviceManager deviceManager;

    protected boolean isBonding;

    private final int BONDING_DELAY_IN_MS = 1000;
    private final int BONDING_TIMEOUT_IN_S = 120;
    private final int PAIRING_MODE_TIMEOUT_IN_S = 20;

    private final BaseDevice device;
    private final Handler handler;

    private Disposable pairingModeDisposable;
    private Disposable bondingStateDisposable;

    public BaseAutoPairingViewModel(@NonNull Application application,
                                    @NonNull DeviceManager deviceManager,
                                    @NonNull String deviceId) {
        super(application);

        this.deviceManager = deviceManager;
        device = deviceManager.getDeviceFromId(deviceId);
        handler = new Handler(Looper.getMainLooper());
    }

    @Override
    protected void onCleared() {
        dispose();
        super.onCleared();
    }

    /**
     * Method initializing process of Auto Pairing. Should be used by the inheriting classes
     * to start Auto Pairing.
     */
    protected void initEnablePairingModeObserver() {
        if (device.getBaseDeviceStateController()
                .isFeatureSupported(FeaturesDefs.PAIRING_MODE_STATUS)) {
            Observable<Boolean> pairingModeObservable =
                    device.getBaseDeviceStateController().outputs.getPairingModeStatus();


            pairingModeDisposable = pairingModeObservable
                    .observeOn(AndroidSchedulers.mainThread())
                    .timeout(PAIRING_MODE_TIMEOUT_IN_S, TimeUnit.SECONDS)
                    .take(1)
                    .distinctUntilChanged()
                    .subscribe(value -> {
                                if (value) {
                                    onPairingModeEnabled();
                                } else {
                                    onPairingModeDisabled();
                                    startPairing();
                                }
                            }, throwable -> {
                                device.getBaseDeviceStateController().inputs
                                        .setConnectionInfo(BaseDevice.ConnectionState.DISCONNECTED);
                                onPairingFailed();
                            }
                    );
            device.getBaseDeviceStateController().inputs.readPairingMode();
        }
    }

    protected void startPairing() {
        isBonding = true;

        device.getBaseDeviceStateController().inputs
                .setConnectionInfo(BaseDevice.ConnectionState.DISCONNECTED);

        handler.postDelayed(() ->
                bondingStateDisposable = deviceManager.getSystemBondingState()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(bondState -> {
                                    if (bondState.getAddress().equals(device.getDeviceInfo().getId())) {
                                        if (bondState.getActionId() == BluetoothDevice.BOND_BONDED) {
                                            device.getBaseDeviceStateController().inputs
                                                    .setConnectionInfo(BaseDevice.ConnectionState.DISCONNECTED);
                                            bondingStateDisposable.dispose();
                                            onPairingDone();
                                            isBonding = false;
                                        } else if (bondState.getActionId() == BluetoothDevice.BOND_NONE) {
                                            bondingStateDisposable.dispose();
                                            onPairingFailed();
                                            isBonding = false;
                                        }
                                    }
                                }, throwable -> {
                                    device.getBaseDeviceStateController().inputs
                                            .setConnectionInfo(BaseDevice.ConnectionState.DISCONNECTED);
                                    onPairingFailed();
                                    isBonding = false;
                                }
                        ), BONDING_DELAY_IN_MS);
    }

    private void dispose() {
        handler.removeCallbacksAndMessages(null);

        if (pairingModeDisposable != null) {
            pairingModeDisposable.dispose();
        }

        if (bondingStateDisposable != null) {
            bondingStateDisposable.dispose();
        }
    }
}

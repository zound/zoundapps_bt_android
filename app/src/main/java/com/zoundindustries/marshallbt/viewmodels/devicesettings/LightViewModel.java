package com.zoundindustries.marshallbt.viewmodels.devicesettings;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import android.widget.SeekBar;

import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * Class handling logic for light view
 */
public interface LightViewModel {

    interface Inputs {
        /**
         * Will be triggered when the progress value changes
         *
         * @param seekBar       the {@link com.zoundindustries.marshallbt.ui.devicesettings.LightSeekBar}
         * @param progressValue the progress
         * @param fromUser      a boolean about that is an user input or not
         */
        void lightSeekBarProgressChanged(@NonNull SeekBar seekBar, int progressValue,
                                         boolean fromUser);
    }

    interface Outputs {
        /**
         * Provides the actual state of brightnessProgress
         *
         * @return Integer value of brightnessProgress
         */
        @NonNull
        MutableLiveData<Integer> getBrightnessProgress();

        /**
         * Get {@link io.reactivex.Observable} for changed view.
         *
         * @return viewType to be started.
         */
        MutableLiveData<ViewType> isViewChanged();
    }

    class Body extends AndroidViewModel implements Inputs, Outputs {

        public final Inputs inputs = this;
        public final Outputs outputs = this;

        private MutableLiveData<Integer> brightnessProgress;
        private final MutableLiveData<ViewType> viewChanged;
        private final FirebaseAnalyticsManager firebaseAnalyticsManager;

        private DeviceManager deviceManager;
        private String deviceId;
        private BaseDevice device;

        private Disposable lightDisposable;
        private Disposable connectionDisposable;
        private Disposable serviceReadyDisposable;

        public Body(@NonNull Application application,
                    @NonNull String deviceId) {
            this(application, deviceId,
                    new DeviceManager(application),
                    ((BluetoothApplication) application).getAppComponent().firebaseWrapper());
        }

        @VisibleForTesting
        public Body(@NonNull Application application,
                    @NonNull String deviceId,
                    @NonNull DeviceManager deviceManager,
                    @NonNull FirebaseAnalyticsManager firebaseAnalyticsManager) {
            super(application);

            viewChanged = new MutableLiveData<>();
            brightnessProgress = new MutableLiveData<>();

            this.deviceId = deviceId;
            this.deviceManager = deviceManager;

            this.firebaseAnalyticsManager = firebaseAnalyticsManager;
            initDefaultLiveDataStates();
            initDevice();
        }

        @Override
        public void lightSeekBarProgressChanged(@NonNull SeekBar seekBar, int progressValue,
                                                boolean fromUser) {
            if (fromUser) {
                setBrightnessProgress(progressValue);
                if (device != null) {
                    firebaseAnalyticsManager.eventLightSettingChanged(progressValue);
                    device.getBaseDeviceStateController().inputs.setBrightness(progressValue);
                }
            }
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getBrightnessProgress() {
            return brightnessProgress;
        }

        @Override
        public MutableLiveData<ViewType> isViewChanged() {
            return viewChanged;
        }

        @Override
        protected void onCleared() {
            dispose();
        }

        private void initDefaultLiveDataStates() {
            setBrightnessProgress(0);
        }

        private void initDevice() {
            serviceReadyDisposable = deviceManager
                    .isServiceReady()
                    .subscribe(isReady -> {
                        if (isReady) {
                            device = deviceManager.getDeviceFromId(deviceId);
                            if (device != null) {
                                initObservers();
                            } else {
                                viewChanged.setValue(ViewType.HOME_SCREEN);
                            }
                        }
                    });
        }

        private void initObservers() {
            if (device != null && device.getBaseDeviceStateController()
                    .isFeatureSupported(FeaturesDefs.CONNECTION_INFO)) {
                connectionDisposable = device.getBaseDeviceStateController()
                        .outputs.getConnectionInfo()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(connectionState -> {
                            if (connectionState == BaseDevice.ConnectionState.DISCONNECTED) {
                                viewChanged.setValue(ViewType.HOME_SCREEN);
                            }
                        });

                lightDisposable = device.getBaseDeviceStateController().outputs.getBrightness()
                        .observeOn(AndroidSchedulers.mainThread())
                        .take(1)
                        .subscribe(this::setBrightnessProgress);
            }
        }

        private void setBrightnessProgress(int value) {
            brightnessProgress.setValue(value);
        }

        private void dispose() {
            deviceManager.dispose();

            if (connectionDisposable != null) {
                connectionDisposable.dispose();
            }

            if (lightDisposable != null) {
                lightDisposable.dispose();
            }

            if (serviceReadyDisposable != null) {
                serviceReadyDisposable.dispose();
            }
        }
    }
}

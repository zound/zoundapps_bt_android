package com.zoundindustries.marshallbt.viewmodels.factories.autopairing;

import android.app.Application;
import androidx.lifecycle.ViewModel;
import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.viewmodels.autopairing.EnablePairingViewModel;
import com.zoundindustries.marshallbt.viewmodels.factories.BaseDeviceViewModelFactory;

/**
 * Factory providing new EnablePairingViewModel if needed or old object if it exists
 */
public class EnablePairingViewModelFactory extends BaseDeviceViewModelFactory {

    public EnablePairingViewModelFactory(Application application, String deviceId) {
        super(application, deviceId);
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new EnablePairingViewModel.Body(application, deviceId);
    }
}

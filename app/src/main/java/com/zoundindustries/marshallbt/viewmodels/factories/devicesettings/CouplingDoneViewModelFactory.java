package com.zoundindustries.marshallbt.viewmodels.factories.devicesettings;

import android.app.Application;
import androidx.lifecycle.ViewModel;
import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.viewmodels.devicesettings.CouplingDoneViewModel;
import com.zoundindustries.marshallbt.viewmodels.factories.BaseDeviceViewModelFactory;

/**
 * Factory providing new CouplingDone ViewModel if needed or old object if it exists
 */
public class CouplingDoneViewModelFactory extends BaseDeviceViewModelFactory {

    private final String slaveId;

    public CouplingDoneViewModelFactory(Application application, String masterId, String slaveId) {
        super(application, masterId);
        this.slaveId = slaveId;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new CouplingDoneViewModel.Body(application, deviceId, slaveId);
    }
}

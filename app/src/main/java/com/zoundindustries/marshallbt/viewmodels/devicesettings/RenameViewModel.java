package com.zoundindustries.marshallbt.viewmodels.devicesettings;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.annotation.VisibleForTesting;
import android.view.View;

import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.device.tymphany.SpeakerInfo;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.services.DeviceService.DeviceImageType;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * Class used to handle the business logic for renaming speakers.
 */
public interface RenameViewModel {

    interface Inputs {
        /**
         * Handle text in rename input field being changed. Four parameters are needed for matching
         * the TextWatcher onTextChanged method - for hooking up to dataBinding via xml
         *
         * @param sequence sequence of characters from rename input field
         * @param start    begin of replaced text
         * @param before   previous length of text
         * @param count    the number of changed characters from start
         */
        void nameChanged(@NonNull CharSequence sequence, int start, int before, int count);

        /**
         * Handle next button being tapped
         *
         * @param v view on which the method is run
         */
        void nextButtonTapped(View v);
    }

    interface Outputs {
        /**
         * Notify about next button visibility
         *
         * @return visibility of next button
         */
        @NonNull
        MutableLiveData<Integer> getNextButtonVisibility();

        /**
         * Notify about warning message
         *
         * @return warning message about validity
         */
        @NonNull
        MutableLiveData<String> getWarningMessage();

        /**
         * Get observable for device name.
         *
         * @return current device name.
         */
        @NonNull
        MutableLiveData<String> getDeviceName();

        /**
         * Get the image resource ID for speaker
         *
         * @return observable on resource ID for the speaker image.
         */
        @NonNull
        MutableLiveData<Integer> getSpeakerImageResourceId();

        /**
         * Get {@link MutableLiveData} for changed view.
         *
         * @return viewType for screen change
         */
        @NonNull
        MutableLiveData<ViewType> isViewChanged();
    }

    class Body extends AndroidViewModel implements Inputs, Outputs {

        public final Inputs inputs = this;
        public final Outputs outputs = this;

        private final MutableLiveData<ViewType> viewChanged;
        private final MutableLiveData<Integer> nextButtonVisibility;
        private final MutableLiveData<String> deviceName;
        private final MutableLiveData<String> warningMessage;
        private final FirebaseAnalyticsManager firebaseAnalyticsManager;
        private final MutableLiveData<Integer> speakerImageId;

        private BaseDevice device;
        private DeviceManager deviceManager;
        private String newDeviceName;
        private final String deviceId;

        private Disposable serviceReadyDisposable;
        private Disposable connectionDisposable;

        public Body(@NonNull Application app, @NonNull String deviceId) {
            this(app, deviceId, new DeviceManager(app),
                    ((BluetoothApplication) app).getAppComponent().firebaseWrapper());
        }

        @VisibleForTesting
        public Body(@NonNull Application app,
                    @NonNull String deviceId,
                    @NonNull DeviceManager deviceManager,
                    @NonNull FirebaseAnalyticsManager firebaseAnalyticsManager) {
            super(app);
            this.deviceId = deviceId;

            viewChanged = new MutableLiveData<>();
            nextButtonVisibility = new MutableLiveData<>();
            warningMessage = new MutableLiveData<>();
            deviceName = new MutableLiveData<>();
            speakerImageId = new MutableLiveData<>();

            this.deviceManager = deviceManager;
            this.firebaseAnalyticsManager = firebaseAnalyticsManager;
            setupDeviceName(deviceId);
        }

        @Override
        public void nameChanged(@NonNull CharSequence sequence, int start, int before, int count) {
            int characterLeftCounter = device.getMaxNameLength() - sequence.length();
            newDeviceName = String.valueOf(sequence);

            switch (device.getValidityState(newDeviceName)) {
                case VALID:
                    nextButtonVisibility.setValue(View.VISIBLE);

                    if (characterLeftCounter != 0) {
                        setWarningMessage(R.string.rename_screen_characters_left,
                                characterLeftCounter);
                    } else {
                        setWarningMessage(R.string.rename_screen_reached_limit,
                                characterLeftCounter);
                    }

                    break;
                case TOO_LONG:
                    nextButtonVisibility.setValue(View.INVISIBLE);
                    setWarningMessage(R.string.rename_screen_name_too_long,
                            characterLeftCounter);
                    break;
                case EMPTY:
                    nextButtonVisibility.setValue(View.INVISIBLE);
                    setWarningMessage(R.string.rename_screen_characters_left,
                            characterLeftCounter);
            }
        }

        @Override
        public void nextButtonTapped(View v) {
            SpeakerInfo speakerInfo = new SpeakerInfo();
            speakerInfo.setDeviceName(newDeviceName);
            device.getBaseDeviceStateController().inputs.setSpeakerInfo(speakerInfo);
            deviceManager.updateDeviceName(deviceId, newDeviceName);

            firebaseAnalyticsManager.eventSpeakerNameChanged(device.getDeviceInfo().getProductName());
            viewChanged.setValue(ViewType.BACK);
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getNextButtonVisibility() {
            return nextButtonVisibility;
        }

        @NonNull
        @Override
        public MutableLiveData<String> getWarningMessage() {
            return warningMessage;
        }

        @NonNull
        @Override
        public MutableLiveData<String> getDeviceName() {
            return deviceName;
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getSpeakerImageResourceId() {
            return speakerImageId;
        }

        @NonNull
        @Override
        public MutableLiveData<ViewType> isViewChanged() {
            return viewChanged;
        }

        @Override
        protected void onCleared() {
            dispose();
        }

        private void initStates() {
            viewChanged.setValue(ViewType.UNKNOWN);
            nextButtonVisibility.setValue(View.INVISIBLE);
            setWarningMessage(R.string.rename_screen_characters_left,
                    device.getMaxNameLength());
        }

        private void setupDeviceName(String deviceId) {
            serviceReadyDisposable = deviceManager
                    .isServiceReady()
                    .subscribe(isReady -> {
                        if (isReady) {
                            device = deviceManager.getDeviceFromId(deviceId);
                            if (device != null) {
                                deviceName.setValue(device.getBaseDeviceStateController()
                                        .outputs.getSpeakerInfoCurrentValue()
                                        .getDeviceName());
                                initConnectionObserver();
                                initStates();
                                speakerImageId.setValue(deviceManager.getImageResourceId(deviceId,
                                        DeviceImageType.MEDIUM));
                            } else {
                                viewChanged.setValue(ViewType.HOME_SCREEN);
                            }
                        }
                    });
        }

        private void initConnectionObserver() {
            connectionDisposable = device.getBaseDeviceStateController().outputs
                    .getConnectionInfo()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(connectionState -> {
                        if (connectionState == BaseDevice.ConnectionState.DISCONNECTED) {
                            viewChanged.setValue(ViewType.HOME_SCREEN);
                        }
                    });
        }

        private void setWarningMessage(@StringRes int id, int characterLeftCounter) {
            Resources resources = getApplication().getResources();

            warningMessage.setValue(characterLeftCounter <= 0 ? resources.getString(id) :
                    resources.getString(id, characterLeftCounter));
        }

        private void dispose() {
            if (serviceReadyDisposable != null) {
                serviceReadyDisposable.dispose();
            }

            if (connectionDisposable != null) {
                connectionDisposable.dispose();
            }
        }
    }
}

package com.zoundindustries.marshallbt.viewmodels.player.sources;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.player.sources.SourceFeatures;
import com.zoundindustries.marshallbt.ui.ViewFlowController;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * This class handle the logic behind sources.
 */
public interface SourcesViewModel {

    interface Outputs {

        /**
         * Get the source features of the device
         *
         * @return class contains the availability of sources
         */
        MutableLiveData<SourceFeatures> getSourceFeatures();

        /**
         * determine the possibility to switch to bt source from app when aux is plugged in
         * @return cable source configurability
         */
        boolean isAuxConfigurable();

        /**
         * Get {@link MutableLiveData} for changed view.
         *
         * @return viewType for screen change
         */
        @NonNull
        MutableLiveData<ViewFlowController.ViewType> isViewChanged();
    }

    class Body extends AndroidViewModel implements Outputs {

        public Outputs outputs = this;

        private MutableLiveData<SourceFeatures> sourceFeatures;
        private MutableLiveData<ViewFlowController.ViewType> viewChanged;

        private String deviceId;
        private BaseDevice device;
        private DeviceManager deviceManager;

        private Disposable connectionDisposable;
        private Disposable serviceReadyDisposable;
        private boolean isAuxConfigurable = true;

        /**
         * Constructor for Body.
         *
         * @param application needed to get resources for strings.
         * @param deviceId  id of the device
         */
        public Body(@NonNull Application application, @NonNull String deviceId) {
            this(application, deviceId, new DeviceManager(application.getApplicationContext()));
        }

        /**
         * Constructor for Body.
         *
         * @param application needed to get resources for strings.
         * @param deviceId  id of the device
         * @param deviceManager manager to handle devices
         */
        @VisibleForTesting
        public Body(@NonNull Application application, @NonNull String deviceId,
                    @NonNull DeviceManager deviceManager) {
            super(application);

            this.deviceManager = deviceManager;
            this.deviceId = deviceId;
            viewChanged = new MutableLiveData<>();
            sourceFeatures = new MutableLiveData<>();

            initDevice();
        }

        @Override
        public MutableLiveData<SourceFeatures> getSourceFeatures() {
            return sourceFeatures;
        }

        @Override
        public boolean isAuxConfigurable() {
            return isAuxConfigurable;
        }

        @NonNull
        @Override
        public MutableLiveData<ViewFlowController.ViewType> isViewChanged() {
            return viewChanged;
        }

        @Override
        protected void onCleared() {
            dispose();
        }

        private void initDevice() {
            serviceReadyDisposable = deviceManager
                    .isServiceReady()
                    .subscribe(isReady -> {
                        if (isReady) {
                            device = deviceManager.getDeviceFromId(deviceId);
                            if (device != null) {
                                sourceFeatures.setValue(new SourceFeatures(
                                        device.getBaseDeviceStateController()
                                                .isFeatureSupported(FeaturesDefs.BLUETOOTH_SOURCE),
                                        device.getBaseDeviceStateController()
                                                .isFeatureSupported(FeaturesDefs.RCA_SOURCE),
                                        device.getBaseDeviceStateController()
                                                .isFeatureSupported(FeaturesDefs.AUX_SOURCE)
                                        ));
                                if(!device.getBaseDeviceStateController().outputs.isAuxConfigurable()) {
                                    isAuxConfigurable = false;
                                }

                                connectionDisposable = device.getBaseDeviceStateController()
                                        .outputs.getConnectionInfo()
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(connectionState -> {
                                            if (connectionState == BaseDevice.ConnectionState.DISCONNECTED) {
                                                viewChanged.setValue(ViewFlowController.ViewType.BACK);
                                            }
                                        });

                            } else {
                                //what todo now?
                            }
                        }
                    });
        }

        private void dispose() {
            if (connectionDisposable != null) {
                connectionDisposable.dispose();
            }

            if (serviceReadyDisposable != null) {
                serviceReadyDisposable.dispose();
            }
        }
    }
}

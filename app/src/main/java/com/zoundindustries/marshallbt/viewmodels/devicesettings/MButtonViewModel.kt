package com.zoundindustries.marshallbt.viewmodels.devicesettings

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.zoundindustries.marshallbt.R
import com.zoundindustries.marshallbt.model.device.BaseDevice
import com.zoundindustries.marshallbt.model.device.DeviceManager
import com.zoundindustries.marshallbt.model.devicesettings.MButtonSettingsItem
import com.zoundindustries.marshallbt.model.devicesettings.MButtonSettingsItem.MButtonActionType.*
import com.zoundindustries.marshallbt.ui.ViewFlowController
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

interface MButtonViewModel {
    interface Inputs {
        /**
         * Indicates that tab was changed
         */
        fun onMButtonRowSelected(type: MButtonSettingsItem.MButtonActionType)
    }

    interface Outputs {
        /**
         * Get list of settings item to be shown to the user.
         */
        val settingsItemList: List<MButtonSettingsItem>

        /**
         * Notify about changing the active tab type
         */
        val notifyMButtonValue: MutableLiveData<Int>

        val notifyDetailedViewSelected: LiveData<MButtonSettingsItem.MButtonActionType>
        val notifyIsAuxSourceActive: LiveData<Boolean>

        /**
         * Get Observable for changed view.
         */
        val viewChanged: MutableLiveData<ViewFlowController.ViewType>
    }

    class Body(val app: Application, val deviceId: String) : AndroidViewModel(app), Inputs, Outputs {
        override val notifyDetailedViewSelected: LiveData<MButtonSettingsItem.MButtonActionType>
            get() = detailedViewSelected

        override val notifyIsAuxSourceActive: LiveData<Boolean>
            get() = isAuxSourceActive

        val inputs = this
        val outputs = this
        private var device: BaseDevice?

        private val detailedViewSelected = MutableLiveData<MButtonSettingsItem.MButtonActionType>()
        private val isAuxSourceActive = MutableLiveData<Boolean>()

        private var deviceManager: DeviceManager = DeviceManager(app)
        private var serviceReadyDisposable: Disposable?
        private var mButtonDataDisposable: Disposable? = null
        private var connectionDisposable: Disposable? = null
        private var auxSourceActiveDisposable: Disposable? = null

        //inputs
        override fun onMButtonRowSelected(type: MButtonSettingsItem.MButtonActionType) {
            device?.baseDeviceStateController?.inputs?.setMButtonActionType(type)
            when (type) {
                GOOGLE_ASSISTANT -> {
                    viewChanged.value = ViewFlowController.ViewType.M_BUTTON_GVA_DETAIL
                    viewChanged.value = ViewFlowController.ViewType.UNKNOWN
                }
            }
        }

        //outputs
        override val settingsItemList = listOf(
                MButtonSettingsItem(EQUALIZER,
                        app.resources.getString(R.string.m_button_screen_option_eq_title),
                        app.resources.getString(R.string.m_button_screen_option_eq_subtitle)),

                MButtonSettingsItem(GOOGLE_ASSISTANT,
                        app.resources.getString(R.string.m_button_screen_option_gva_title),
                        app.resources.getString(R.string.m_button_screen_option_gva_subtitle)),

                MButtonSettingsItem(NATIVE_ASSISTANT,
                        app.resources.getString(R.string.m_button_screen_option_nva_title),
                        app.resources.getString(R.string.m_button_screen_option_nva_subtitle)))

        override val notifyMButtonValue: MutableLiveData<Int> = MutableLiveData()
        override val viewChanged: MutableLiveData<ViewFlowController.ViewType> = MutableLiveData()

        init {
            device = deviceManager.getDeviceFromId(deviceId)

            serviceReadyDisposable = deviceManager
                    .isServiceReady
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { isReady ->
                        isReady?.let {
                            initObservers()
                        }
                    }
        }

        private fun initObservers() {
            connectionDisposable = device?.baseDeviceStateController?.outputs?.connectionInfo
                    ?.observeOn(AndroidSchedulers.mainThread())
                    ?.subscribe { connectionState ->
                        if (connectionState == BaseDevice.ConnectionState.DISCONNECTED) {
                            viewChanged.value = ViewFlowController.ViewType.HOME_SCREEN
                        }
                    }

            mButtonDataDisposable = device?.baseDeviceStateController?.outputs?.mButtonActionType
                    ?.observeOn(AndroidSchedulers.mainThread())
                    ?.distinctUntilChanged()
                    ?.subscribe { action ->
                        notifyMButtonValue.value = getIndexFromActionType(action)
                    }

            device?.baseDeviceStateController?.let { baseController ->
                if (!baseController.outputs.isAuxConfigurable) {
                    auxSourceActiveDisposable = baseController.outputs
                            .isSourceActive(BaseDevice.SourceType.AUX)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe { handleAuxSourceActive(it) }
                }
            }

            device?.baseDeviceStateController?.inputs?.readMButtonActionType()
        }

        private fun handleAuxSourceActive(it: Boolean?) {
            isAuxSourceActive.value = it
        }

        override fun onCleared() {
            serviceReadyDisposable?.dispose()
            mButtonDataDisposable?.dispose()
        }

        private fun getIndexFromActionType(type: MButtonSettingsItem.MButtonActionType): Int {
            return when (type) {
                NONE -> -1
                EQUALIZER -> 0
                GOOGLE_ASSISTANT -> 1
                NATIVE_ASSISTANT -> 2
            }
        }
    }
}
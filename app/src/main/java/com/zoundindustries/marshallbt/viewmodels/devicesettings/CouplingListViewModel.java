package com.zoundindustries.marshallbt.viewmodels.devicesettings;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.view.View;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.devicesettings.CouplingManager;
import com.zoundindustries.marshallbt.services.DeviceService.DeviceImageType;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

import static com.zoundindustries.marshallbt.ui.devicesettings.DeviceSettingsActivity.EXTRA_DEVICE_COUPLING_SLAVE_ID;
import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

/**
 * Class handling logic behind couple speakers function.
 */

public interface CouplingListViewModel {

    interface Inputs {

        /**
         * Indicates that user selected device to couple with.
         * This enables coupling procedure to continue.
         *
         * @param device to couple with. Null can be passed to indicate that no item
         *               is currently selected.
         */
        void onItemSelected(@Nullable BaseDevice device);

        /**
         * Indicates that user clicked Next button to proceed with coupling.
         * Selected device will be used to couple with it.
         */
        void onNextButtonClicked(@NonNull View view);
    }

    interface Outputs {

        /**
         * Provides LiveData with main device list used across the system.
         *
         * @return LiveData list object with devices in the system
         */
        @NonNull
        MutableLiveData<List<BaseDevice>> getDevicesAvailableForCoupling();

        /**
         * Provides live data with flag indicating if the device was selected for coupling.
         *
         * @return LiveData flag object indicating selection state.
         */
        @NonNull
        MutableLiveData<Boolean> isItemSelected();

        /**
         * Get the image resource ID for speaker
         *
         * @return int which is resource ID
         * @param id
         */
        int getSpeakerImageResourceId(String id);

        /**
         * Get {@link io.reactivex.Observable} for changed view.
         *
         * @return viewType to be started.
         */
        @NonNull
        MutableLiveData<ViewType> isViewChanged();
    }

    final class Body extends AndroidViewModel implements Inputs, Outputs {

        public final Inputs inputs = this;
        public final Outputs outputs = this;

        private final MutableLiveData<Boolean> isDeviceSelected;
        private final MutableLiveData<List<BaseDevice>> readyToCoupleDevices;
        private final MutableLiveData<ViewType> viewChanged;

        private final CouplingManager couplingManager;
        private final DeviceManager deviceManager;
        private final String deviceId;
        private final List<BaseDevice> devicesList;

        private BaseDevice masterDevice;
        private BaseDevice slaveDevice;

        private Disposable serviceReadyDisposable;
        private Disposable connectionDisposable;
        private Disposable couplingDevicesDisposable;

        /**
         * Constructor for {@link CouplingListViewModel} Body.
         *
         * @param application context used for instantiating managers.
         * @param deviceId    that the coupling was invoked for.
         */
        public Body(@NonNull Application application, @NonNull String deviceId) {
            this(application,
                    deviceId,
                    new CouplingManager(application),
                    new DeviceManager(application),
                    new ArrayList<>());
        }

        /**
         * Constructor for {@link CouplingListViewModel} Body with test injections.
         * *
         *
         * @param application     context used for instantiating managers.
         * @param deviceId        that the coupling was invoked for.
         * @param couplingManager used to inject in test.
         * @param deviceManager   used to inject in test.
         * @param devicesList     used to inject in test.
         */
        @VisibleForTesting
        public Body(@NonNull Application application,
                    @NonNull String deviceId,
                    @NonNull CouplingManager couplingManager,
                    @NonNull DeviceManager deviceManager,
                    @NonNull List<BaseDevice> devicesList) {
            super(application);
            this.deviceId = deviceId;

            isDeviceSelected = new MutableLiveData<>();
            readyToCoupleDevices = new MutableLiveData<>();
            viewChanged = new MutableLiveData<>();

            this.couplingManager = couplingManager;
            this.deviceManager = deviceManager;
            this.devicesList = devicesList;
            initDefaultLiveDataStates();
            initObservers();
        }

        @Override
        public void onItemSelected(@Nullable BaseDevice device) {
            slaveDevice = device;
            isDeviceSelected.setValue(device != null);
        }

        @Override
        public void onNextButtonClicked(@NonNull View view) {
            couplingManager.stopGetDevicesReadyToCouple();
            Bundle argBundle = new Bundle();

            if (masterDevice != null) {
                argBundle.putString(EXTRA_DEVICE_ID, masterDevice.getDeviceInfo().getId());
            }

            argBundle.putString(EXTRA_DEVICE_COUPLING_SLAVE_ID,
                    slaveDevice.getDeviceInfo().getId());
            viewChanged.setValue(ViewType.COUPLE_MODE_SELECTION.setArgs(argBundle));

            if (connectionDisposable != null) {
                connectionDisposable.dispose();
            }
        }

        @NonNull
        @Override
        public MutableLiveData<List<BaseDevice>> getDevicesAvailableForCoupling() {
            return readyToCoupleDevices;
        }

        @NonNull
        @Override
        public MutableLiveData<Boolean> isItemSelected() {
            return isDeviceSelected;
        }

        @Override
        public int getSpeakerImageResourceId(String id) {
            return deviceManager.getImageResourceId(id, DeviceImageType.SMALL);
        }

        @NonNull
        @Override
        public MutableLiveData<ViewType> isViewChanged() {
            return viewChanged;
        }

        @Override
        protected void onCleared() {
            dispose();
        }

        private void initDefaultLiveDataStates() {
            readyToCoupleDevices.setValue(devicesList);
            isDeviceSelected.setValue(false);
        }

        private void initObservers() {
            serviceReadyDisposable = couplingManager
                    .isServiceReady()
                    .subscribe(isReady -> {
                        if (isReady) {
                            setupMasterDevice();
                            initCoupleDevicesList();
                        }
                    });
        }

        private void setupMasterDevice() {
            masterDevice = deviceManager.getDeviceFromId(deviceId);
            if (masterDevice != null) {
                devicesList.add(masterDevice);
            } else {
                viewChanged.setValue(ViewType.HOME_SCREEN);
            }

        }

        private void initConnectionObserver() {
            if (masterDevice!= null ) {
                connectionDisposable = masterDevice.getBaseDeviceStateController().outputs.getConnectionInfo()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(connectionState -> {
                            if (connectionState == BaseDevice.ConnectionState.DISCONNECTED) {
                                viewChanged.setValue(ViewType.HOME_SCREEN);
                            }
                        });
            }
        }

        private void initCoupleDevicesList() {
            if (masterDevice != null) {
                couplingDevicesDisposable = couplingManager.startScanForReadyToCoupleDevices(masterDevice)
                        .subscribe(device -> {
                            devicesList.add(device);
                            readyToCoupleDevices.setValue(devicesList);
                            initConnectionObserver();
                        });
            }
        }

        private void dispose() {
            if (serviceReadyDisposable != null) {
                serviceReadyDisposable.dispose();
            }

            if (connectionDisposable != null) {
                connectionDisposable.dispose();
            }

            if (couplingDevicesDisposable != null) {
                couplingDevicesDisposable.dispose();
            }
            couplingManager.stopGetDevicesReadyToCouple();
        }
    }
}

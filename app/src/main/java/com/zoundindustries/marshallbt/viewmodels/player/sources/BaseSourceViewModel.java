package com.zoundindustries.marshallbt.viewmodels.player.sources;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import android.view.View;

import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.services.DeviceService;
import com.zoundindustries.marshallbt.ui.ViewFlowController;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * Base class to handle logic behind sources
 */
public interface BaseSourceViewModel {

    interface Inputs {
        /**
         * Activate button is clicked
         *
         * @param view button that is receiving the click
         */
        void onActivateButtonClicked(View view);

        /**
         * Set the volume value
         *
         * @param progress value to be set
         */
        void setVolume(int progress);
    }

    interface Outputs {
        /**
         * Reflects the state of the source
         *
         * @return true if device is activated on the device, false otherwise
         */
        @NonNull
        MutableLiveData<Boolean> isSourceActivated();

        /**
         * Get the value of volume
         *
         * @return integer with volume value
         */
        @NonNull
        MutableLiveData<Integer> getVolume();

        /**
         * Get the image resource ID for speaker
         *
         * @return observable on resource ID for the speaker image.
         */
        @NonNull
        MutableLiveData<Integer> getSpeakerImageResourceId();

        /**
         * Get correct image for speaker source type imageView
         *
         * @return image resource Id
         */
        int getSourceTypeImage();

        /**
         * Get displayed (with prefix) speaker name
         *
         * @return prefix + speaker name used by user
         */
        MutableLiveData<String> getDisplayedDeviceName();

        /**
         * Get {@link MutableLiveData} for changed view.
         *
         * @return viewType for screen change
         */
        @NonNull
        MutableLiveData<ViewFlowController.ViewType> isViewChanged();
    }

    /**
     * Body for base source view model
     */
    abstract class Body extends AndroidViewModel implements Inputs, Outputs {

        public static final int READ_AUDIO_DELAY_MILLIS = 1000;
        public final Inputs inputs = this;
        public final Outputs outputs = this;
        protected final FirebaseAnalyticsManager firebaseAnalyticsManager;

        protected BaseDevice device;

        private MutableLiveData<Boolean> isSourceActivated;
        private MutableLiveData<Integer> volume;
        private MutableLiveData<Integer> speakerImageId;
        protected MutableLiveData<ViewFlowController.ViewType> viewChanged;

        private String deviceId;

        private DeviceManager deviceManager;

        private Disposable serviceReadyDisposable;
        private Disposable isSourceActiveDisposable;
        private Disposable volumeDisposable;

        /**
         * Constructor for Body.
         *
         * @param application needed to get resources for strings.
         * @param deviceId    id of the device
         */
        public Body(@NonNull Application application, @NonNull String deviceId) {
            this(application, deviceId,
                    new DeviceManager(application.getApplicationContext()),
                    ((BluetoothApplication) application).getAppComponent().firebaseWrapper());
        }

        /**
         * Constructor for Body.
         *
         * @param application   needed to get resources for strings.
         * @param deviceId      id of the device
         * @param deviceManager manager to handle devices
         */
        @VisibleForTesting
        public Body(@NonNull Application application, @NonNull String deviceId,
                    @NonNull DeviceManager deviceManager,
                    @NonNull FirebaseAnalyticsManager firebaseAnalyticsManager) {
            super(application);

            initLiveData();

            this.firebaseAnalyticsManager = firebaseAnalyticsManager;
            this.deviceManager = deviceManager;
            this.deviceId = deviceId;

            initDevice();
        }

        @Override
        public void onActivateButtonClicked(View view) {
            firebaseAnalyticsManager.eventSourceActivated(getSourceType());
            device.getBaseDeviceStateController().inputs.setAudioSource(getSourceType());
            new Handler().postDelayed(() ->
                            device.getBaseDeviceStateController().inputs.readAudioStatus(),
                    READ_AUDIO_DELAY_MILLIS);
        }

        @Override
        public void setVolume(int volume) {
            this.volume.setValue(volume);
            device.getBaseDeviceStateController().inputs.setVolume(volume);
            firebaseAnalyticsManager.eventAppVolumeChanged(volume);
        }

        @Override
        @NonNull
        public MutableLiveData<Boolean> isSourceActivated() {
            return isSourceActivated;
        }

        @Override
        @NonNull
        public MutableLiveData<Integer> getVolume() {
            return volume;
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getSpeakerImageResourceId() {
            return speakerImageId;
        }

        @Override
        protected void onCleared() {
            super.onCleared();
            dispose();
        }

        protected void initObservers() {
            isSourceActiveDisposable = device.getBaseDeviceStateController()
                    .outputs.isSourceActive(getSourceType())
                    .observeOn(AndroidSchedulers.mainThread())
                    .filter(isActivated -> isActivated != null)
                    .subscribe(isActivated -> {
                        isSourceActivated.setValue(isActivated);
                    });

            volumeDisposable = device.getBaseDeviceStateController()
                    .outputs.getVolume()
                    .observeOn(AndroidSchedulers.mainThread())
                    .filter(volume -> volume != null)
                    .subscribe(volume -> this.volume.setValue(volume));
        }

        protected void initLiveData() {
            isSourceActivated = new MutableLiveData<>();
            volume = new MutableLiveData<>();
            speakerImageId = new MutableLiveData<>();
            viewChanged = new MutableLiveData<>();

            isSourceActivated.setValue(false);
        }

        protected void dispose() {
            if (serviceReadyDisposable != null) {
                serviceReadyDisposable.dispose();
            }

            if (isSourceActiveDisposable != null) {
                isSourceActiveDisposable.dispose();
            }

            if (volumeDisposable != null) {
                volumeDisposable.dispose();
            }
        }

        private void initDevice() {
            serviceReadyDisposable = deviceManager
                    .isServiceReady()
                    .filter(isReady -> isReady)
                    .subscribe(isReady -> {
                        device = deviceManager.getDeviceFromId(deviceId);
                        if (device != null) {
                            initObservers();
                            device.getBaseDeviceStateController().inputs.readAudioStatus();
                            speakerImageId.setValue(deviceManager.getImageResourceId(deviceId,
                                    DeviceService.DeviceImageType.MEDIUM));
                        } else {
                            viewChanged.setValue(ViewType.HOME_SCREEN);
                        }
                    });
        }

        private boolean isBluetooth() {
            return getSourceType() == BaseDevice.SourceType.BLUETOOTH;
        }

        protected abstract BaseDevice.SourceType getSourceType();

        public abstract int getSourceTypeImage();

        public abstract MutableLiveData<String> getDisplayedDeviceName();
    }
}

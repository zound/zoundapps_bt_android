package com.zoundindustries.marshallbt.viewmodels.devicesettings;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.view.View;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.BaseDevice.CouplingChannelType;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.services.DeviceService;
import com.zoundindustries.marshallbt.services.DeviceService.DeviceImageType;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.ui.devicesettings.DeviceSettingsActivity;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public interface CouplingDoneViewModel {

    interface Inputs {

        /**
         * Indicates that user changed channel setting for coupling.
         *
         * @param mode to be set when coupling.
         */
        void onChannelModeChanged(@NonNull CouplingChannelType mode);

        /**
         * Indicates that user clicked Done button to complete coupling process.
         */
        void onDoneButtonClicked(@NonNull View view);
    }

    interface Outputs {

        /**
         * Get {@link Observable} for getting master device name from the {@link DeviceService}.
         *
         * @return name observable.
         */
        @NonNull
        MutableLiveData<String> getMasterName();

        /**
         * Get {@link Observable} for getting slave device name from the {@link DeviceService}.
         *
         * @return name observable.
         */
        @NonNull
        MutableLiveData<String> getSlaveName();

        /**
         * Get current channel mode selected by the user.
         *
         * @return currently selected channel type.
         */
        @NonNull
        CouplingChannelType getCurrentChannelType();

        /**
         * Get the image resource ID for master speaker
         *
         * @return observable on resource ID for the master speaker image.
         */
        @NonNull
        MutableLiveData<Integer> getMasterImageResourceId();

        /**
         * Get the image resource ID for slave speaker
         *
         * @return observable on resource ID for the slave speaker image.
         */
        @NonNull
        MutableLiveData<Integer> getSlaveImageResourceId();

        /**
         * Get {@link Observable} for changed view.
         *
         * @return viewType to be started.
         */
        @NonNull
        MutableLiveData<ViewType> isViewChanged();
    }

    final class Body extends AndroidViewModel implements Inputs, Outputs {

        public static final int CHANNEL_SWITCH_DELAY_MILLIS = 1000;
        public final Inputs inputs = this;
        public final Outputs outputs = this;

        private final MutableLiveData<String> masterName;
        private final MutableLiveData<String> slaveName;
        private final MutableLiveData<Integer> masterSpeakerImageId;
        private final MutableLiveData<Integer> slaveSpeakerImageId;
        private final MutableLiveData<ViewType> viewChanged;

        private final DeviceManager deviceManager;
        private final String masterDeviceId;

        private BaseDevice masterDevice;
        private BaseDevice slaveDevice;
        private CouplingChannelType channelMode;

        private Disposable serviceReadyDisposable;
        private Disposable connectionDisposable;

        /**
         * Constructor for {@link CouplingDoneViewModel} Body.
         *
         * @param application    context to be sued for Managers initialization.
         * @param masterDeviceId id for coupling master device.
         * @param slaveDeviceId  id for coupling slave device.
         */
        public Body(@NonNull Application application,
                    @NonNull String masterDeviceId,
                    @NonNull String slaveDeviceId) {
            this(application,
                    masterDeviceId,
                    slaveDeviceId,
                    new DeviceManager(application));
        }

        /**
         * Constructor for {@link CouplingDoneViewModel} Body with test injections.
         *
         * @param application    context to be sued for Managers initialization.
         * @param masterDeviceId id for coupling master device.
         * @param slaveDeviceId  id for coupling slave device.
         */
        @VisibleForTesting
        public Body(@NonNull Application application,
                    @NonNull String masterDeviceId,
                    @NonNull String slaveDeviceId,
                    @NonNull DeviceManager deviceManager) {
            super(application);

            masterName = new MutableLiveData<>();
            slaveName = new MutableLiveData<>();
            masterSpeakerImageId = new MutableLiveData<>();
            slaveSpeakerImageId = new MutableLiveData<>();
            viewChanged = new MutableLiveData<>();

            this.deviceManager = deviceManager;
            this.masterDeviceId = masterDeviceId;
            initObservers(masterDeviceId, slaveDeviceId);
        }

        @Override
        public void onChannelModeChanged(@NonNull CouplingChannelType mode) {
            channelMode = mode;
        }

        @Override
        public void onDoneButtonClicked(@NonNull View view) {

            masterDevice.getBaseDeviceStateController().inputs.
                    setCouplingMasterConnected(channelMode, slaveDevice.getDeviceInfo().getId());

            deviceManager.updateSpeakerInfoCache(masterDevice.getDeviceInfo().getId(),
                    true,
                    channelMode.toString(),
                    BaseDevice.CouplingConnectionState.CONNECTED_AS_MASTER.toString());

            deviceManager.updateSpeakerInfoCache(slaveDevice.getDeviceInfo().getId(),
                    true,
                    channelMode == CouplingChannelType.RIGHT ? CouplingChannelType.LEFT.toString() :
                            channelMode == CouplingChannelType.LEFT ?
                                    CouplingChannelType.RIGHT.toString() :
                                    CouplingChannelType.PARTY.toString(),
                    BaseDevice.CouplingConnectionState.CONNECTED_AS_SLAVE.toString());

            if (connectionDisposable != null) {
                connectionDisposable.dispose();
            }

            finalizeCoupling();
        }

        private void finalizeCoupling() {
            new Handler().postDelayed(() -> {
                masterDevice.getBaseDeviceStateController().inputs
                        .setConnectionInfo(BaseDevice.ConnectionState.DISCONNECTED);
                slaveDevice.getBaseDeviceStateController().inputs
                        .setConnectionInfo(BaseDevice.ConnectionState.DISCONNECTED);

                Bundle args = new Bundle();
                args.putString(DeviceSettingsActivity.EXTRA_DEVICE_MASTER_NAME,
                        masterDevice.getBaseDeviceStateController().outputs
                                .getSpeakerInfoCurrentValue().getDeviceName());
                args.putString(DeviceSettingsActivity.EXTRA_DEVICE_SLAVE_NAME,
                        slaveDevice.getBaseDeviceStateController().outputs
                                .getSpeakerInfoCurrentValue().getDeviceName());

                viewChanged.setValue(ViewType.COUPLE_HEADS_UP.setArgs(args));
            }, CHANNEL_SWITCH_DELAY_MILLIS);
        }

        @NonNull
        @Override
        public MutableLiveData<ViewType> isViewChanged() {
            return viewChanged;
        }

        @NonNull
        @Override
        public MutableLiveData<String> getMasterName() {
            return masterName;
        }

        @NonNull
        @Override
        public MutableLiveData<String> getSlaveName() {
            return slaveName;
        }

        @NonNull
        @Override
        public CouplingChannelType getCurrentChannelType() {
            return channelMode;
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getMasterImageResourceId() {
            return masterSpeakerImageId;
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getSlaveImageResourceId() {
            return slaveSpeakerImageId;
        }

        @Override
        protected void onCleared() {
            super.onCleared();
            dispose();
        }

        private void initObservers(String masterDeviceId, String slaveDeviceId) {
            serviceReadyDisposable = deviceManager
                    .isServiceReady()
                    .subscribe(isReady -> {
                        if (isReady) {
                            masterDevice = deviceManager.getDeviceFromId(masterDeviceId);
                            if (masterDevice != null) {
                                masterSpeakerImageId.setValue(deviceManager
                                        .getImageResourceId(masterDeviceId,
                                                DeviceImageType.SMALL));
                                masterName.setValue(masterDevice.getBaseDeviceStateController()
                                        .outputs.getSpeakerInfoCurrentValue().getDeviceName());
                            } else {
                                viewChanged.setValue(ViewType.HOME_SCREEN);
                                return;
                            }

                            if (!TextUtils.isEmpty(slaveDeviceId)) {
                                slaveDevice = deviceManager.getDeviceFromId(slaveDeviceId);
                                if (slaveDevice != null) {
                                    slaveSpeakerImageId.setValue(deviceManager.getImageResourceId(
                                            slaveDeviceId, DeviceImageType.SMALL));
                                    slaveName.setValue(slaveDevice.getBaseDeviceStateController()
                                            .outputs.getSpeakerInfoCurrentValue().getDeviceName());
                                }
                            } else {
                                //todo think of a way to handle coupled device that was coupled
                                // todo from a different device
                                slaveName.setValue("Unknown");
                            }
                            initConnectionObserver();
                        }
                    });
        }

        private void initConnectionObserver() {
            connectionDisposable = masterDevice.getBaseDeviceStateController().outputs
                    .getConnectionInfo()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(connectionState -> {
                        if (connectionState == BaseDevice.ConnectionState.DISCONNECTED) {
                            viewChanged.setValue(ViewType.HOME_SCREEN);
                        }
                    });
        }

        private void dispose() {
            if (serviceReadyDisposable != null) {
                serviceReadyDisposable.dispose();
            }

            if (connectionDisposable != null) {
                connectionDisposable.dispose();
            }
        }
    }
}

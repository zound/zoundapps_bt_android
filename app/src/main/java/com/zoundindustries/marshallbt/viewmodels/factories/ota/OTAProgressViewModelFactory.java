package com.zoundindustries.marshallbt.viewmodels.factories.ota;

import android.app.Application;
import androidx.lifecycle.ViewModel;
import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.viewmodels.factories.BaseDeviceViewModelFactory;
import com.zoundindustries.marshallbt.viewmodels.ota.OTAProgressViewModel;

/**
 * Factory providing new  OTAProgress ViewModel if needed or old object if it exists
 */
public class OTAProgressViewModelFactory extends BaseDeviceViewModelFactory {
    public OTAProgressViewModelFactory(Application application, String deviceId) {
        super(application, deviceId);
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new OTAProgressViewModel.Body(application, deviceId);
    }
}

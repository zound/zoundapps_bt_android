package com.zoundindustries.marshallbt.viewmodels.factories.player.sources;

import android.app.Application;
import androidx.lifecycle.ViewModel;
import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.viewmodels.factories.BaseDeviceViewModelFactory;
import com.zoundindustries.marshallbt.viewmodels.player.sources.SourcesViewModel;

/**
 * Factory providing new  Sources ViewModel if needed or old object if it exists
 */
public class SourcesViewModelFactory extends BaseDeviceViewModelFactory {
    public SourcesViewModelFactory(Application application, String deviceId) {
        super(application, deviceId);
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new SourcesViewModel.Body(application, deviceId);
    }
}

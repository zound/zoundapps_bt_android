package com.zoundindustries.marshallbt.viewmodels.devicesettings;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.BaseDevice.ConnectionState;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.services.DeviceService.DeviceImageType;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;

import io.reactivex.disposables.Disposable;

public interface ForgetDeviceViewModel {

    interface Inputs {

        /**
         * Handle forget button being clicked.
         *
         * @param view view on which the method is run
         */
        void onForgetButtonClicked(View view);
    }

    interface Outputs {

        /**
         * Get observable for device name.
         *
         * @return current device name.
         */
        @NonNull
        MutableLiveData<String> getDeviceName();

        /**
         * Get {@link MutableLiveData} for changed view.
         *
         * @return viewType for screen change
         */
        @NonNull
        MutableLiveData<ViewType> isViewChanged();

        /**
         * Get the image resource ID for speaker.
         *
         * @return observable on resource ID for the speaker image.
         */
        @NonNull
        MutableLiveData<Integer> getSpeakerImageResourceId();

        /**
         * Get current description based on the device type.
         */
        String getDescription();

        /**
         * Get current upper case title based on the device type.
         */
        int getTitle();
    }

    class Body extends AndroidViewModel implements Inputs, Outputs {

        public final Inputs inputs = this;
        public final Outputs outputs = this;

        private final MutableLiveData<String> deviceName;
        private final MutableLiveData<ViewType> viewChanged;
        private final MutableLiveData<Integer> speakerImageId;


        private final DeviceManager deviceManager;
        private BaseDevice device;

        private Disposable serviceReadyDisposable;
        private final String deviceId;

        /**
         * Constructor for {@link ForgetDeviceViewModel} Body
         *
         * @param application to be used for initializing managers.
         * @param deviceId    for the settings context.
         */
        public Body(@NonNull Application application,
                    @NonNull String deviceId) {
            this(application, deviceId, new DeviceManager(application));
        }

        /**
         * Constructor for {@link ForgetDeviceViewModel} Body with injections for tests.
         *
         * @param application   to be used for initializing managers.
         * @param deviceId      for the settings context.
         * @param deviceManager to be injected.
         */
        public Body(@NonNull Application application,
                    @NonNull String deviceId,
                    @NonNull DeviceManager deviceManager) {
            super(application);
            this.deviceId = deviceId;

            deviceName = new MutableLiveData<>();
            viewChanged = new MutableLiveData<>();
            speakerImageId = new MutableLiveData<>();

            this.deviceManager = deviceManager;

            setupDeviceName(deviceId);
        }

        @Override
        public void onForgetButtonClicked(View view) {
            //todo redirect to settings to let user unpair
            device.getBaseDeviceStateController().inputs.setCouplingDisconnected();
            device.getBaseDeviceStateController().inputs
                    .setConnectionInfo(ConnectionState.DISCONNECTED);
            deviceManager.removeSpeakerInfoCache(deviceId);
            viewChanged.setValue(ViewType.GLOBAL_SETTINGS_BLUETOOTH);
        }

        @NonNull
        @Override
        public MutableLiveData<String> getDeviceName() {
            return deviceName;
        }

        @NonNull
        @Override
        public MutableLiveData<ViewType> isViewChanged() {
            return viewChanged;
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getSpeakerImageResourceId() {
            return speakerImageId;
        }

        @Override
        public String getDescription() {
            String description = "";

            if (device.getDeviceInfo().getDeviceGroup() == DeviceInfo.DeviceGroup.HEADPHONES) {
                description = getApplication().getResources()
                        .getString(R.string.forget_headphone_screen_subtitle);
            } else if (device.getDeviceInfo().getDeviceGroup() == DeviceInfo.DeviceGroup.SPEAKER) {
                description = getApplication().getResources()
                        .getString(R.string.forget_screen_subtitle);
            }
            return description;
        }

        @Override
        public int getTitle() {
            BaseDevice device = deviceManager.getDeviceFromId(deviceId);

            int title = 0;

            if (device.getDeviceInfo().getDeviceGroup() == DeviceInfo.DeviceGroup.HEADPHONES) {
                title = R.string.device_settings_menu_item_forget_headphone;
            } else if (device.getDeviceInfo().getDeviceGroup() == DeviceInfo.DeviceGroup.SPEAKER) {
                title = R.string.device_settings_menu_item_forget;
            }
            return title;
        }

        @Override
        protected void onCleared() {
            super.onCleared();

            dispose();
        }

        private void setupDeviceName(String deviceId) {
            serviceReadyDisposable = deviceManager
                    .isServiceReady()
                    .subscribe(isReady -> {
                        if (isReady) {
                            device = deviceManager.getDeviceFromId(deviceId);
                            if (device != null) {
                                deviceName.setValue(device.getBaseDeviceStateController()
                                        .outputs.getSpeakerInfoCurrentValue().getDeviceName());
                                speakerImageId.setValue(deviceManager.getImageResourceId(deviceId,
                                        DeviceImageType.MEDIUM));
                            } else {
                                viewChanged.setValue(ViewType.HOME_SCREEN);
                            }
                        }
                    });
        }

        private void dispose() {
            if (serviceReadyDisposable != null) {
                serviceReadyDisposable.dispose();
            }
        }
    }
}

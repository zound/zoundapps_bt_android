package com.zoundindustries.marshallbt.viewmodels.factories.devicesettings;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;

import com.zoundindustries.marshallbt.viewmodels.devicesettings.ANCViewModel;
import com.zoundindustries.marshallbt.viewmodels.factories.BaseDeviceViewModelFactory;

public class AncViewModelFactory extends BaseDeviceViewModelFactory {
    public AncViewModelFactory(Application application, String deviceId) {
        super(application, deviceId);
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new ANCViewModel.Body(application, deviceId);
    }
}

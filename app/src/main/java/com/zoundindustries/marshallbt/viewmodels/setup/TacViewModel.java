package com.zoundindustries.marshallbt.viewmodels.setup;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.view.View;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;

/**
 * Class used to handle trivial logic behind terms and conditions setup fragment
 */
public interface TacViewModel {

    interface Inputs {
        /**
         * Handle back button being tapped
         *
         * @param v view on which the method is run
         */
        void backTapped(View v);

        /**
         * Reset the view transition variables
         */
        void resetViewTransitionVariables();

        /**
         * Handle scrollview reached its own bottom
         *
         * @param isBottomReached boolean of scrollview at the bottom
         */
        void scrollViewBottomReached(boolean isBottomReached);
    }

    interface Outputs {
        /**
         * Notify about view returning to the previous screen
         *
         * @return LiveData observable
         */
        @NonNull
        MutableLiveData<Boolean> getBack();

        /**
         * Notify about scrollview footer visibility
         *
         * @return LiveData observable
         */
        @NonNull
        MutableLiveData<Integer> getFooterVisibility();
    }

    final class Body extends ViewModel implements Inputs, Outputs {

        public final Inputs inputs = this;
        public final Outputs outputs = this;

        private final MutableLiveData<Boolean> back;
        private final MutableLiveData<Integer> footerVisibility;

        @Inject
        public Body() {
            back = new MutableLiveData<>();
            footerVisibility = new MutableLiveData<>();
            resetViewTransitionVariables();
        }

        /*      inputs     */
        @Override
        public void backTapped(View v) {
            back.setValue(true);
        }

        @Override
        public void resetViewTransitionVariables() {
            back.setValue(false);
        }

        @Override
        public void scrollViewBottomReached(boolean isBottomReached) {
            footerVisibility.setValue(isBottomReached ? View.GONE : View.VISIBLE);
        }

        /*      outputs     */
        @Override
        @NonNull
        public MutableLiveData<Boolean> getBack() {
            return back;
        }

        @Override
        public MutableLiveData<Integer> getFooterVisibility() {
            return footerVisibility;
        }

    }
}

package com.zoundindustries.marshallbt.viewmodels.factories.mainmenu;

import android.app.Application;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.viewmodels.mainmenu.MainMenuUnsubscribeViewModel;

/**
 * Factory providing new  MainMenuUnsubscribe ViewModel if needed or old object if it exists
 */
public class MainMenuUnsubscribeViewModelFactory implements ViewModelProvider.Factory {
    private Application application;

    public MainMenuUnsubscribeViewModelFactory(Application application) {
        this.application = application;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new MainMenuUnsubscribeViewModel.Body(application);
    }
}

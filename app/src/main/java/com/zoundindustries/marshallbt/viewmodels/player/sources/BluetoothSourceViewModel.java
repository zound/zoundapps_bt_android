package com.zoundindustries.marshallbt.viewmodels.player.sources;

import android.app.Application;
import androidx.lifecycle.MutableLiveData;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import android.view.View;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.model.player.sources.SongInfo;
import com.zoundindustries.marshallbt.ui.ViewFlowController;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * Class used to handle activation of bluetooth audio source
 */
public interface BluetoothSourceViewModel {

    interface Inputs {

        /**
         * Triggers when the previous button tapped
         *
         * @param view button that is receiving the click
         */
        void onPreviousButtonClicked(View view);

        /**
         * Triggers when the next button tapped
         *
         * @param view button that is receiving the click
         */
        void onNextButtonClicked(View view);

        /**
         * Triggers when the play button tapped
         *
         * @param view button that is receiving the click
         */
        void onPlayButtonClicked(View view);

        /**
         * Triggers when the pause button tapped
         *
         * @param view button that is receiving the click
         */
        void onPauseButtonClicked(View view);
    }

    interface Outputs {

        /**
         * determine the possibility to switch to bt source from app when aux is plugged in
         * @return cable source configurability
         */
        @NonNull
        MutableLiveData<Boolean> isAuxConfigurable();

        /**
         * Provides that a song is currently playing
         *
         * @return live data object with boolean for current playing state
         */
        @NonNull
        MutableLiveData<Boolean> isPlaying();

        /**
         *Provides the data object of song information
         *
         * @return live data object with SongInfo object for information about the current song
         */
        @NonNull
        MutableLiveData<SongInfo> getSongInfo();
    }

    /**
     * Body for bluetooth source view model
     */
    final class Body extends BaseSourceViewModel.Body implements Inputs, Outputs {

        public final Inputs playerInputs = this;
        public final Outputs playerOutputs = this;

        private MutableLiveData<SongInfo> songInfo;
        private MutableLiveData<Boolean> isPlaying;
        private MutableLiveData<String> deviceName;

        private Disposable songInfoDisposable;
        private Disposable isPlayingDisposable;

        private MutableLiveData<Boolean> isAuxConfigurable;

        public Body(@NonNull Application application,
                    @NonNull String deviceId) {
            super(application, deviceId);
        }

        @VisibleForTesting
        public Body(@NonNull Application application,
                    @NonNull String deviceId,
                    @NonNull DeviceManager deviceManager,
                    @NonNull FirebaseAnalyticsManager firebaseAnalyticsManager ) {
            super(application, deviceId, deviceManager, firebaseAnalyticsManager);
        }

        @Override
        public MutableLiveData<Boolean> isAuxConfigurable() {
            return isAuxConfigurable;
        }

        @Override
        public void onPreviousButtonClicked(View view) {
            device.getBaseDeviceStateController().inputs.playPreviousSong();
            firebaseAnalyticsManager.eventMediaButtonTouchedPrev();
        }

        @Override
        public void onNextButtonClicked(View view) {
            device.getBaseDeviceStateController().inputs.playNextSong();
            firebaseAnalyticsManager.eventMediaButtonTouchedNext();
        }

        @Override
        public void onPlayButtonClicked(View view) {
            isPlaying.setValue(true);
            device.getBaseDeviceStateController().inputs.setPlaying(true);

            firebaseAnalyticsManager.eventMediaButtonTouchedPlay();
        }

        @Override
        public void onPauseButtonClicked(View view) {
            isPlaying.setValue(false);
            device.getBaseDeviceStateController().inputs.setPlaying(false);

            firebaseAnalyticsManager.eventMediaButtonTouchedPause();
        }

        @NonNull
        @Override
        public MutableLiveData<Boolean> isPlaying() {
            return isPlaying;
        }

        @NonNull
        @Override
        public MutableLiveData<SongInfo> getSongInfo() {
            return songInfo;
        }

        @Override
        protected void initObservers() {
            super.initObservers();
            setDeviceName();

            songInfoDisposable = device.getBaseDeviceStateController().outputs.getSongInfo()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(songInfoResult -> songInfo.setValue(songInfoResult));

            isPlayingDisposable = device.getBaseDeviceStateController().outputs.getPlaying()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(isPlayingResult -> isPlaying.setValue(isPlayingResult));

            if(device.getBaseDeviceStateController().outputs.isAuxConfigurable()) {
                isAuxConfigurable.setValue(true);
            }
        }

        @Override
        protected void dispose() {
            super.dispose();

            if (songInfoDisposable != null) {
                songInfoDisposable.dispose();
            }

            if (isPlayingDisposable != null) {
                isPlayingDisposable.dispose();
            }
        }

        @NonNull
        @Override
        protected BaseDevice.SourceType getSourceType() {
            return BaseDevice.SourceType.BLUETOOTH;
        }

        @Override
        public int getSourceTypeImage() {
            return R.drawable.source_bluetooth;
        }

        @NonNull
        @Override
        public MutableLiveData<String> getDisplayedDeviceName() {
            return deviceName;
        }

        @NonNull
        @Override
        public MutableLiveData<ViewFlowController.ViewType> isViewChanged() {
            return super.viewChanged;
        }

        @Override
        protected void initLiveData() {
            super.initLiveData();
            songInfo = new MutableLiveData<>();
            isPlaying = new MutableLiveData<>();
            deviceName = new MutableLiveData<>();
            isAuxConfigurable = new MutableLiveData<>();

            songInfo.setValue(new SongInfo.SongInfoBuilder().build());
            isPlaying.setValue(false);
            deviceName.setValue("");
            isAuxConfigurable.setValue(false);
        }

        private void setDeviceName() {
            deviceName.setValue(device.getBaseDeviceStateController().outputs
                    .getSpeakerInfoCurrentValue().getDeviceName());
        }
    }
}

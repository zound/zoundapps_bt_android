package com.zoundindustries.marshallbt.viewmodels.homescreen

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.zoundindustries.marshallbt.model.device.BaseDevice
import com.zoundindustries.marshallbt.model.device.DeviceManager
import com.zoundindustries.marshallbt.services.DeviceService.DeviceImageType

interface AllFinishedViewModel {
    interface Outputs {
        fun getDeviceName() : String?

        fun getOnboardingFinishedImageId(): Int
    }

    class Body(application: Application, val deviceId: String) : AndroidViewModel(application), Outputs {

        val outputs: Outputs = this
        val deviceManager = DeviceManager(application)

        private var device : BaseDevice? = null

        init {
            device = deviceManager.getDeviceFromId(deviceId)
        }

        override fun getDeviceName(): String? {
            return device?.baseDeviceStateController
                    ?.outputs?.speakerInfoCurrentValue
                    ?.deviceName
        }

        override fun getOnboardingFinishedImageId(): Int {
            return deviceManager.getImageResourceId(deviceId, DeviceImageType.ONBOARDING)
        }
    }
}
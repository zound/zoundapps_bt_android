package com.zoundindustries.marshallbt.viewmodels.factories.devicesettings

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.zoundindustries.marshallbt.viewmodels.devicesettings.ScanningViewModel

/**
 * Factory providing new DeviceSettingsActivity ViewModel if needed or old object if it exists
 */
class ScanningViewModelFactory(val application: Application) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return ScanningViewModel.Body(application) as T
    }
}

package com.zoundindustries.marshallbt.viewmodels.factories.devicesettings

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.zoundindustries.marshallbt.viewmodels.devicesettings.OffTimerViewModel

class OffTimerViewModelFactory (val application: Application, val deviceId: String) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return OffTimerViewModel.Body(application, deviceId) as T
    }
}

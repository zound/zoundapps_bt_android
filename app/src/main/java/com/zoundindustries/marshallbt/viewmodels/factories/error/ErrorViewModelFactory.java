package com.zoundindustries.marshallbt.viewmodels.factories.error;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.ui.ViewFlowController;
import com.zoundindustries.marshallbt.viewmodels.error.ErrorViewModel;

/**
 * Factory providing new Error ViewModel if needed or old object if it exists
 */
public class ErrorViewModelFactory implements ViewModelProvider.Factory {
    private ViewFlowController.ViewType errorType;
    private ViewFlowController.ViewType errorOrigin;

    public ErrorViewModelFactory(ViewFlowController.ViewType errorType, ViewFlowController.ViewType errorOrigin) {
        this.errorType = errorType;
        this.errorOrigin = errorOrigin;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new ErrorViewModel.Body(errorType, errorOrigin);
    }
}

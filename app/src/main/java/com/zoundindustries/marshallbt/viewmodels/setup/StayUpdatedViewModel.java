package com.zoundindustries.marshallbt.viewmodels.setup;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.annotation.RestrictTo;
import androidx.annotation.VisibleForTesting;

import android.util.Log;
import android.view.View;

import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.model.salesforce.SalesforceWebAPIController;
import com.zoundindustries.marshallbt.ui.ViewFlowController;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.utils.CommonPreferences;
import com.zoundindustries.marshallbt.utils.EmailValidator;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;


/**
 * Class used to handle logic behind stay updated screen
 */
public interface StayUpdatedViewModel {

    interface Inputs {
        /**
         * Handle text in email input field being changed. Four parameters are needed for matching
         * the TextWatcher onTextChanged method - for hooking up to dataBinding via xml
         *
         * @param sequence sequence of characters from email input field
         * @param start    begin of replaced text
         * @param before   previous length of text
         * @param count    number of characters added to text
         */
        void emailChanged(CharSequence sequence, int start, int before, int count);

        /**
         * Handle next button being tapped
         *
         * @param v view on which the method is run
         */
        void nextButtonTapped(@NonNull View v);

        /**
         * Handle skip button being tapped
         *
         * @param v view on which the method is run
         */
        void skipButtonTapped(@NonNull View v);

        /**
         * Informs about user tap on Privacy Policy link
         */
        void privacyPolicyLinkTapped();


        /**
         * Reset variables responsible for view transitions
         */
        void resetViewTransitionVariables();

        /**
         * Handle unsubscribe button being tapped
         *
         * @param unsubscribeButton view on which the method is run
         */
        void unsubscribeButtonClicked(@NonNull View unsubscribeButton);
    }

    interface Outputs {
        /**
         * Get {@link MutableLiveData} for changed view.
         *
         * @return LiveData observable
         */
        @NonNull
        MutableLiveData<ViewType> isViewChanged();

        /**
         * Notify about next button visibility
         *
         * @return LiveData observable
         */
        @NonNull
        MutableLiveData<Integer> getNextButtonVisibility();

        /**
         * Notify about wrong email indicator in the email editText being visible
         *
         * @return LiveData observable
         */
        @NonNull
        MutableLiveData<Integer> isWrongEmailIndicatorVisible();

        /**
         * Notify about progress bar visibility
         *
         * @return LiveData observable
         */
        @NonNull
        MutableLiveData<Integer> getProgressBarVisibility();

        /**
         * Set text of edit text email
         *
         * @return LiveData observable
         */
        @NonNull
        MutableLiveData<String> setEmailWarningText();

        /**
         * Notify about subscribed user email textview visibility
         *
         * @return LiveData observable
         */
        @NonNull
        MutableLiveData<Integer> getSubscribedUserEmailTextViewVisibility();

        /**
         * Notify about edit text for input user email visibility
         *
         * @return LiveData observable
         */
        @NonNull
        MutableLiveData<Integer> getEditTextForEmailVisibility();

        /**
         * Notify about unsubscribe button visibility
         *
         * @return LiveData observable
         */
        @NonNull
        MutableLiveData<Integer> getUnsubscribeButtonVisibility();

        /**
         * Notify about skip button visibility
         *
         * @return LiveData observable
         */
        @NonNull
        MutableLiveData<Integer> getSkipButtonVisibility();

        /**
         * Notify about LayoutType which (one layout, but with different views visibility)
         *
         * @return LiveData observable
         */
        @NonNull
        MutableLiveData<StayUpdateLayoutType> getLayoutType();
    }

    final class Body extends AndroidViewModel implements Inputs, Outputs {

        private final static int MIN_CHECKING_LENGTH = 3;

        private static final String TAG = Body.class.getSimpleName();

        public final Inputs inputs = this;

        public final Outputs outputs = this;

        private MutableLiveData<Integer> nextButtonVisible;
        private MutableLiveData<Integer> progressBarVisible;
        private MutableLiveData<Integer> wrongEmailIndicatorVisible;
        private MutableLiveData<ViewType> viewChanged;
        private MutableLiveData<String> emailEditTextValue;
        private MutableLiveData<Integer> subscribedUserEmailVisibility;
        private MutableLiveData<Integer> editTextForEmailVisibility;
        private MutableLiveData<Integer> unsubscribeButtonVisibility;
        private MutableLiveData<Integer> skipButtonVisibility;
        private MutableLiveData<StayUpdateLayoutType> layoutType;

        private FirebaseAnalyticsManager firebaseAnalyticsManager;
        private String userEmail;
        private SalesforceWebAPIController salesforceWebAPIController;
        private CompositeDisposable disposables = new CompositeDisposable();
        private CommonPreferences preferences;
        private static final long OPERATION_TIMEOUT = 10L;

        /**
         * Constructor for Body class.
         *
         * @param application to be used asd Context for getting resources and BrontoApi.
         */
        @Inject
        public Body(@androidx.annotation.NonNull Application application) {
            this(application,
                    ((BluetoothApplication) application).getAppComponent().saleforceApi(),
                    "",
                    new CommonPreferences(application),
                    ((BluetoothApplication) application).getAppComponent().firebaseWrapper());
        }

        /**
         * Constructor for Tests only because of dependencies between application, api and helper
         * which cause to create extra constructor,
         *
         * @param application to be used asd Context for getting resources and BrontoApi.
         */
        @VisibleForTesting
        public Body(@NonNull Application application,
                    @NonNull SalesforceWebAPIController salesforceWebAPIController,
                    @NonNull String userEmail,
                    @NonNull CommonPreferences preferences,
                    @NonNull FirebaseAnalyticsManager firebaseAnalyticsManager) {
            super(application);
            this.preferences = preferences;
            viewChanged = new MutableLiveData<>();
            nextButtonVisible = new MutableLiveData<>();
            wrongEmailIndicatorVisible = new MutableLiveData<>();
            progressBarVisible = new MutableLiveData<>();
            emailEditTextValue = new MutableLiveData<>();
            subscribedUserEmailVisibility = new MutableLiveData<>();
            editTextForEmailVisibility = new MutableLiveData<>();
            unsubscribeButtonVisibility = new MutableLiveData<>();
            skipButtonVisibility = new MutableLiveData<>();
            layoutType = new MutableLiveData<>();

            nextButtonVisible.setValue(View.INVISIBLE);
            progressBarVisible.setValue(View.INVISIBLE);
            wrongEmailIndicatorVisible.setValue(View.GONE);
            this.firebaseAnalyticsManager = firebaseAnalyticsManager;
            this.salesforceWebAPIController = salesforceWebAPIController;
            this.userEmail = userEmail;
            initStates();
        }

        /*      inputs      */
        @Override
        public void emailChanged(CharSequence sequence, int start, int before, int count) {
            if (sequence.length() <= MIN_CHECKING_LENGTH) {
                nextButtonVisible.setValue(View.INVISIBLE);
                wrongEmailIndicatorVisible.setValue(View.GONE);
                return;
            }

            if (EmailValidator.isValid(sequence.toString())) {
                nextButtonVisible.setValue(View.VISIBLE);
                wrongEmailIndicatorVisible.setValue(View.GONE);
                userEmail = sequence.toString();
            } else {
                nextButtonVisible.setValue(View.INVISIBLE);
                setWrongEmailWarningText();
                wrongEmailIndicatorVisible.setValue(View.VISIBLE);
            }
        }

        @Override
        public void nextButtonTapped(@NonNull View v) {
            progressBarVisible.setValue(View.VISIBLE);
            nextButtonVisible.setValue(View.INVISIBLE);
            setFirstTimeSubscription();
            registerUserEmail();
        }

        @Override
        public void skipButtonTapped(@NonNull View v) {
            firebaseAnalyticsManager.eventStayUpdatedSkipped();
            viewChanged.setValue(ViewType.SHARE_DATA_SCREEN);
            setFirstTimeSubscription();
        }

        @Override
        public void privacyPolicyLinkTapped() {
            viewChanged.setValue(ViewType.PRIVACY_POLICY_SCREEN);
        }

        @Override
        public void resetViewTransitionVariables() {
            viewChanged.setValue(ViewType.UNKNOWN);
        }

        @Override
        public void unsubscribeButtonClicked(@NonNull View unsubscribeButton) {
            viewChanged.setValue(ViewType.MAIN_MENU_UNSUBSCRIBE);
        }

        /*      outputs     */

        @Override
        @NonNull
        public MutableLiveData<ViewType> isViewChanged() {
            return viewChanged;
        }

        @Override
        @NonNull
        public MutableLiveData<Integer> getNextButtonVisibility() {
            return nextButtonVisible;
        }

        @Override
        @NonNull
        public MutableLiveData<Integer> isWrongEmailIndicatorVisible() {
            return wrongEmailIndicatorVisible;
        }

        @Override
        public MutableLiveData<Integer> getProgressBarVisibility() {
            return progressBarVisible;
        }

        @Override
        public MutableLiveData<String> setEmailWarningText() {
            return emailEditTextValue;
        }

        @Override
        public MutableLiveData<Integer> getSubscribedUserEmailTextViewVisibility() {
            return subscribedUserEmailVisibility;
        }

        @Override
        public MutableLiveData<Integer> getEditTextForEmailVisibility() {
            return editTextForEmailVisibility;
        }

        @Override
        public MutableLiveData<Integer> getUnsubscribeButtonVisibility() {
            return unsubscribeButtonVisibility;
        }

        @Override
        public MutableLiveData<Integer> getSkipButtonVisibility() {
            return skipButtonVisibility;
        }

        @Override
        public MutableLiveData<StayUpdateLayoutType> getLayoutType() {
            return layoutType;
        }

        private void setWrongEmailWarningText() {
            String wrongEmail = getApplication().getResources()
                    .getString(R.string.stay_updated_email_validation_fail);
            emailEditTextValue.setValue(wrongEmail);
        }

        private void initStates() {
            setLayoutType();
            resetViewTransitionVariables();
            arrangeFragmentBasedOnLayoutType();
        }


        private void setLayoutType() {
            if (preferences.isFirstTimeSubscription()) {
                layoutType.setValue(StayUpdateLayoutType.INITIAL_SUBSCRIBE);
            } else if (preferences.isEmailSubscribed()) {
                layoutType.setValue(StayUpdateLayoutType.UNSUBSCRIBE);
            } else {
                layoutType.setValue(StayUpdateLayoutType.SUBSCRIBE_AGAIN);
            }
        }

        private void registerUserEmail() {
            disposables.add(salesforceWebAPIController.requestAccessToken()
                    .timeout(OPERATION_TIMEOUT, TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .take(1)
                    .subscribe(response -> {
                        if (response.isSuccessful() && response.body() != null) {
                            scheduleCustomerAccountCheck(response.body().getAccess_token());
                        } else {
                            handleFailedRegistration();
                        }
                    }, e -> {
                        handleFailedRegistration();
                    }));
        }


        private void scheduleCustomerAccountCheck(String token) {
            disposables.add(salesforceWebAPIController.checkSubsriptionStatus(userEmail, token)
                    .timeout(OPERATION_TIMEOUT, TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .take(1)
                    .subscribe(customerData -> {
                        if(customerData.isSuccessful() && customerData.body() != null) {
                            if (!customerData.body().getAccountId().isEmpty()) {
                                updateCustomerAccount(customerData.body().getAccountId(), token);
                            }
                        } else {
                            createCustomerAccount(token);
                        }
                    }, e -> {
                        handleFailedRegistration();

                    }));
        }

        private void createCustomerAccount(String token) {
            disposables.add(salesforceWebAPIController.createAccount(userEmail, token)
                    .timeout(OPERATION_TIMEOUT, TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .take(1).subscribe( creationStatus -> {
                        if (creationStatus.isSuccessful()) {
                            handleSuccessfulRegistration();
                        } else {
                            handleFailedRegistration();
                        }
                    }, e -> {
                        handleFailedRegistration();
                    }));
        }

        private void updateCustomerAccount(String accountId, String token) {
            disposables.add(salesforceWebAPIController.updateAccountToSubscribe(accountId, userEmail, token)
                    .timeout(OPERATION_TIMEOUT, TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .take(1).subscribe( updateStatus -> {
                        if (updateStatus.isSuccessful()) {
                            handleSuccessfulRegistration();
                        } else {
                            handleFailedRegistration();
                        }
                    }, e -> {
                        handleFailedRegistration();
                    }));
        }

        private void handleSuccessfulRegistration() {
            preferences.setUserEmail(userEmail);
            firebaseAnalyticsManager.eventNewsletterSignedUp();
            switch (layoutType.getValue()) {
                case INITIAL_SUBSCRIBE:
                    changeScreenToShareData();
                    break;
                case SUBSCRIBE_AGAIN:
                    isViewChanged().setValue(ViewType.HOME_SCREEN);
            }
        }

        private void handleFailedRegistration() {
            progressBarVisible.setValue(View.INVISIBLE);
            nextButtonVisible.setValue(View.VISIBLE);
            setErrorRegistrationMessage();
        }

        private void setErrorRegistrationMessage() {
            String registrationError = getApplication().getResources()
                    .getString(R.string.error_common);
            emailEditTextValue.setValue(registrationError);
            wrongEmailIndicatorVisible.setValue(View.VISIBLE);
        }

        private void changeScreenToShareData() {
            viewChanged.setValue(ViewFlowController.ViewType.SHARE_DATA_SCREEN);
        }

        private void arrangeFragmentBasedOnLayoutType() {
            switch (layoutType.getValue()) {
                case INITIAL_SUBSCRIBE:
                    setViewVisibilityForInitialSubscribe();
                    break;
                case SUBSCRIBE_AGAIN:
                    setViewVisibilityForSubscribeAgain();
                    break;
                case UNSUBSCRIBE:
                    setViewVisibilityForUnsubscribe();
                    break;
            }
            setViewVisibilityForCommonViews();
        }

        private void setViewVisibilityForInitialSubscribe() {
            subscribedUserEmailVisibility.setValue(View.GONE);
            unsubscribeButtonVisibility.setValue(View.INVISIBLE);
        }

        private void setViewVisibilityForSubscribeAgain() {
            subscribedUserEmailVisibility.setValue(View.GONE);
            unsubscribeButtonVisibility.setValue(View.INVISIBLE);
            skipButtonVisibility.setValue(View.GONE);
        }

        private void setViewVisibilityForUnsubscribe() {
            subscribedUserEmailVisibility.setValue(View.VISIBLE);
            editTextForEmailVisibility.setValue(View.GONE);
            unsubscribeButtonVisibility.setValue(View.VISIBLE);
            skipButtonVisibility.setValue(View.GONE);
        }

        private void setViewVisibilityForCommonViews() {
            nextButtonVisible.setValue(View.INVISIBLE);
            progressBarVisible.setValue(View.INVISIBLE);
            wrongEmailIndicatorVisible.setValue(View.GONE);
        }

        private void setFirstTimeSubscription() {
            preferences.setFirstRunOfSubscription(false);
        }

        @Override
        protected void onCleared() {
            super.onCleared();
            dispose();
        }

        private void dispose() {
            disposables.dispose();
        }

        @RestrictTo(RestrictTo.Scope.TESTS)
        String getUserEmail() {
            return userEmail;
        }
    }

    enum StayUpdateLayoutType {
        INITIAL_SUBSCRIBE, SUBSCRIBE_AGAIN, UNSUBSCRIBE
    }
}

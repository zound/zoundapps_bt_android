package com.zoundindustries.marshallbt.viewmodels.factories.devicesettings;

import android.app.Application;
import androidx.lifecycle.ViewModel;
import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.viewmodels.devicesettings.EQViewModel;
import com.zoundindustries.marshallbt.viewmodels.factories.BaseDeviceViewModelFactory;

/**
 * Factory providing new EQ ViewModel if needed or old object if it exists
 */
public class EQViewModelFactory extends BaseDeviceViewModelFactory {

    public EQViewModelFactory(Application application, String deviceId) {
        super(application, deviceId);
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new EQViewModel.Body(application, deviceId);
    }
}

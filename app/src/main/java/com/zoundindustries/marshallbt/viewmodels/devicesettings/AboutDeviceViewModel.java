package com.zoundindustries.marshallbt.viewmodels.devicesettings;

import android.app.Application;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.device.tymphany.SpeakerInfo;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.model.ota.IOTAService;
import com.zoundindustries.marshallbt.model.ota.OTAManager;
import com.zoundindustries.marshallbt.model.ota.OtaErrorThrowable;
import com.zoundindustries.marshallbt.model.ota.OtaErrorThrowable.ErrorType;
import com.zoundindustries.marshallbt.model.ota.pojo.FirmwareFileMetaData;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;
import com.zoundindustries.marshallbt.utils.ViewExtrasGlobals;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import retrofit2.Response;

import static com.zoundindustries.marshallbt.model.ota.IOTAService.UpdateState;


public interface AboutDeviceViewModel {

    interface Inputs {

        /**
         * Triggers when update button be clicked
         *
         * @param view the update button
         */
        void onUpdateButtonClicked(View view);
    }

    interface Outputs {
        /**
         * Get observable for update button visibility.
         * Value reflects that there is new update for speaker.
         *
         * @return observable for update button visibility.
         */
        @NonNull
        MutableLiveData<Integer> getUpdateButtonVisibility();

        /**
         * Get observable for updating progress visibility.
         * Value reflects that the update process is ongoing.
         *
         * @return observable for updating process visibility.
         */
        @NonNull
        MutableLiveData<Integer> getUpdatingProgressVisibility();

        /**
         * Get observable for updating progress visibility.
         * Value reflects that the update process is ongoing.
         *
         * @return observable for updating process visibility.
         */
        @NonNull
        MutableLiveData<Boolean> getEnableBackFunctionality();

        /**
         * Get liveData observable for update state from device
         *
         * @return observable with device update state
         */
        @NonNull
        MutableLiveData<IOTAService.UpdateState> getUpdateState();

        /**
         * Get liveData observable for update progress.
         *
         * @return liveData observable.
         */
        @NonNull
        MutableLiveData<Double> getFirmwareUpdateProgress();

        /**
         * Get current FirmwareFileMetaData.
         *
         * @return FirmwareFileMetaData.
         */
        @Nullable
        FirmwareFileMetaData getFirmwareUpdateMetaData();

        /**
         * Get {@link io.reactivex.Observable} for changed view.
         *
         * @return viewType to be started.
         */
        MutableLiveData<ViewType> isViewChanged();

        /**
         * Get current upper case title based on the device type.
         */
        int getTitle();
    }

    class Body extends AndroidViewModel implements Inputs, Outputs {

        private static final String TAG = AboutDeviceViewModel.class.getSimpleName();

        public final Inputs inputs = this;
        public final Outputs outputs = this;

        private final MutableLiveData<Double> firmwareUpdateProgress;
        private final MutableLiveData<SpeakerInfo> speakerInfo;
        private final MutableLiveData<Integer> updateButtonVisibility;
        private final MutableLiveData<IOTAService.UpdateState> updateState;
        private final MutableLiveData<Integer> updatingProcessVisibility;
        private final MutableLiveData<Boolean> enableBackFunctionality;
        private final MutableLiveData<ViewType> viewChanged;

        private final DeviceManager deviceManager;
        private final OTAManager otaManager;
        private final FirebaseAnalyticsManager firebaseAnalyticsManager;

        private final boolean shouldRetryOta;
        private String deviceId;
        private BaseDevice device;
        private FirmwareFileMetaData firmwareFileMetaData;

        private Disposable connectionDisposable;
        private Disposable deviceServiceReadyDisposable;
        private Disposable firmwareUpdateProgressDisposable;
        private Disposable firmwareUpdateStateDisposable;
        private Disposable otaServiceReadyDisposable;
        private Disposable speakerInfoDisposable;
        private Disposable checkForUpdateDisposable;

        public Body(@NonNull Application app,
                    @NonNull String deviceId, @Nullable FirmwareFileMetaData firmwareFileMetaData,
                    boolean shouldRetryOta) {
            this(app, deviceId,
                    new DeviceManager(app),
                    new OTAManager(app),
                    firmwareFileMetaData,
                    shouldRetryOta,
                    ((BluetoothApplication) app).getAppComponent().firebaseWrapper());
        }

        @VisibleForTesting
        public Body(@NonNull Application app,
                    @NonNull String deviceId,
                    @NonNull DeviceManager deviceManager,
                    @NonNull OTAManager otaManager,
                    @Nullable FirmwareFileMetaData firmwareFileMetaData,
                    boolean shouldRetryOta,
                    @NonNull FirebaseAnalyticsManager firebaseAnalyticsManager) {
            super(app);
            speakerInfo = new MutableLiveData<>();
            updateButtonVisibility = new MutableLiveData<>();
            updateState = new MutableLiveData<>();
            updatingProcessVisibility = new MutableLiveData<>();
            firmwareUpdateProgress = new MutableLiveData<>();
            enableBackFunctionality = new MutableLiveData<>();
            viewChanged = new MutableLiveData<>();

            this.firebaseAnalyticsManager = firebaseAnalyticsManager;

            initDefaultLiveDataStates();

            this.deviceId = deviceId;
            this.deviceManager = deviceManager;
            this.otaManager = otaManager;
            this.firmwareFileMetaData = firmwareFileMetaData;
            this.shouldRetryOta = shouldRetryOta;

            initObservers();
        }

        public MutableLiveData<SpeakerInfo> getSpeakerInfo() {
            return speakerInfo;
        }

        @Override
        public void onUpdateButtonClicked(View view) {
            startOta();
        }

        private void startOta() {
            enableBackFunctionality.setValue(false);
            updateButtonVisibility.setValue(View.GONE);
            updatingProcessVisibility.setValue(View.VISIBLE);
            firebaseAnalyticsManager.eventOtaStarted();
            otaServiceReadyDisposable = otaManager
                    .isServiceReady()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(serviceConnected -> {
                        if (serviceConnected && firmwareFileMetaData != null) {
                            deviceManager.setScanAllowed(true);
                            otaManager.startOTAUpdate(deviceId, firmwareFileMetaData);
                            initOTAObservers();
                            initFirmwareUpdateObserver();
                        }
                    });
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getUpdateButtonVisibility() {
            return updateButtonVisibility;
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getUpdatingProgressVisibility() {
            return updatingProcessVisibility;
        }

        @NonNull
        @Override
        public MutableLiveData<Boolean> getEnableBackFunctionality() {
            return enableBackFunctionality;
        }

        @NonNull
        @Override
        public MutableLiveData<IOTAService.UpdateState> getUpdateState() {
            return updateState;
        }

        @NonNull
        @Override
        public MutableLiveData<Double> getFirmwareUpdateProgress() {
            return firmwareUpdateProgress;
        }

        @Nullable
        @Override
        public FirmwareFileMetaData getFirmwareUpdateMetaData() {
            return firmwareFileMetaData;
        }

        @Override
        public MutableLiveData<ViewType> isViewChanged() {
            return viewChanged;
        }

        @Override
        public int getTitle() {
            BaseDevice device = deviceManager.getDeviceFromId(deviceId);
            int title = 0;

            if (device.getDeviceInfo().getDeviceGroup() == DeviceInfo.DeviceGroup.HEADPHONES) {
                title = R.string.device_settings_menu_item_about_headphone_uc;
            } else if (device.getDeviceInfo().getDeviceGroup() == DeviceInfo.DeviceGroup.SPEAKER) {
                title = R.string.device_settings_menu_item_about_speaker_uc;
            }
            return title;
        }

        @Override
        protected void onCleared() {
            dispose();
        }

        private void initDefaultLiveDataStates() {
            updatingProcessVisibility.setValue(View.GONE);
            updateButtonVisibility.setValue(View.GONE);
            updateState.setValue(UpdateState.NOT_STARTED);
            firmwareUpdateProgress.setValue(0.0);
            enableBackFunctionality.setValue(true);
        }

        private void initFirmwareUpdateObserver() {
            Observable<UpdateState> otaStateObservable =
                    otaManager.getFirmwareUpdateState(deviceId);

            if (firmwareUpdateStateDisposable != null) {
                firmwareUpdateStateDisposable.dispose();
            }
            firmwareUpdateStateDisposable = otaStateObservable
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::handleUpdateStateChange,
                            this::handleOtaError);
        }

        private void initObservers() {

            deviceServiceReadyDisposable = deviceManager
                    .isServiceReady()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(serviceConnected -> {
                        Log.d(TAG, "initObservers: serviceConnected: " + serviceConnected);
                        if (serviceConnected) {
                            Log.d(TAG, "initObservers: deviceId: " + deviceId);
                            device = deviceManager.getDeviceFromId(deviceId);
                            Log.d(TAG, "initObservers: device: " + device);
                            if (device != null) {
                                speakerInfoDisposable = device.getBaseDeviceStateController()
                                        .outputs.getSpeakerInfo()
                                        .filter(speakerInfo -> !TextUtils.isEmpty(
                                                speakerInfo.getFirmwareVersion()))
                                        .subscribe(speakerInfo -> {
                                            this.speakerInfo.setValue(speakerInfo);
                                            this.speakerInfo.getValue();
                                            logSpeakerInfo();
                                        });
                                initConnectionObserver();

                                if (device.getDeviceInfo().getDeviceType() ==
                                        DeviceInfo.DeviceType.TYMPHANY_DEVICE &&
                                        !((TymphanyDevice)device).isJoplinDevice()) {
                                    return;
                                }

                                if (!otaManager.isOTAInProgress()) {
                                    initCheckForFirmwareUpdateObserver();
                                } else {
                                    initOTAObservers();
                                    initFirmwareUpdateObserver();
                                }
                            } else {
                                viewChanged.setValue(ViewType.HOME_SCREEN);
                            }
                        }
                    });
        }

        private void logSpeakerInfo() {
            SpeakerInfo speakerInfoValue = speakerInfo.getValue();
            Log.d(TAG, "logSpeakerInfo: speakerInfoValue: " + speakerInfoValue);
            if (speakerInfoValue != null) {
                Log.d(TAG, "initObservers: \n"
                        + "speakerInfo.getDeviceName()" + speakerInfoValue.getDeviceName()
                        + "\n speakerInfo.getFirmwareVersion()" + speakerInfoValue
                        .getFirmwareVersion()
                        + "\n speakerInfo.getMacAddress()" + speakerInfoValue.getMacAddress()
                        + "speakerInfo.getModelName()" + speakerInfoValue.getModelName());
            }
        }

        private void allowUpdateOrRetry() {
            if (shouldRetryOta && firmwareFileMetaData != null && device != null) {
                startOta();
            } else {
                handleOtaError(new OtaErrorThrowable(ErrorType.UNDEFINED_FLASHING));
            }
        }

        private void initOTAObservers() {
            Observable<Double> progressObservable = otaManager.getFirmwareUpdateProgress(deviceId);
            if (progressObservable != null) {
                firmwareUpdateProgressDisposable = progressObservable
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(firmwareUpdateProgress::setValue,
                                onError -> {
                                    updatingProcessVisibility.setValue(View.GONE);
                                    updateButtonVisibility.setValue(View.VISIBLE);
                                },
                                () -> enableBackFunctionality.setValue(true));
            }
        }

        private void initCheckForFirmwareUpdateObserver() {
            Observable<Response<FirmwareFileMetaData>> firmwareObservable =
                    otaManager.checkForFirmwareUpdate(deviceId, false);

            if (firmwareObservable != null) {
                checkForUpdateDisposable = firmwareObservable
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(firmwareFileResponse -> {
                                    firmwareFileMetaData = firmwareFileResponse.body();
                                    updateButtonVisibility.setValue(View.VISIBLE);
                                },
                                throwable -> updateButtonVisibility.setValue(View.GONE));
            }
        }

        private void initConnectionObserver() {
            if (device != null && device.getBaseDeviceStateController()
                    .isFeatureSupported(FeaturesDefs.CONNECTION_INFO)) {

                Observable<BaseDevice.ConnectionState> connectionInfoObservable =
                        device.getBaseDeviceStateController()
                                .outputs.getConnectionInfo();

                connectionDisposable = connectionInfoObservable
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(connectionState -> {
                            // during flashing device may reboot, in other case - go back.
                            if (!otaManager.isOTAInProgress() && !shouldRetryOta &&
                                    connectionState == BaseDevice.ConnectionState.DISCONNECTED) {
                                viewChanged.setValue(ViewType.HOME_SCREEN);
                            }
                        });
            }
        }

        private void handleUpdateStateChange(UpdateState updateState) {
            this.updateState.setValue(updateState);

            updatingProcessVisibility.setValue(View.VISIBLE);
            updateButtonVisibility.setValue(View.GONE);

            if (updateState == UpdateState.COMPLETED) {
                enableBackFunctionality.setValue(true);
                firebaseAnalyticsManager.eventOtaCompleted();
            }
            Log.d(TAG, "update state changed = " + updateState.name());
        }

        private void handleOtaError(Throwable throwable) {
            enableBackFunctionality.setValue(true);
            firebaseAnalyticsManager.eventOtaFailed(throwable.getMessage());
            firebaseAnalyticsManager.eventErrorOccurredOta(throwable);

            Bundle args = new Bundle();
            args.putParcelable(ViewExtrasGlobals.EXTRA_OTA_FIRMWARE_META_DATA,
                    firmwareFileMetaData);

            if (connectionDisposable != null) {
                connectionDisposable.dispose();
            }

            if (throwable instanceof OtaErrorThrowable) {
                switch (((OtaErrorThrowable) throwable)
                        .getErrorType().getGeneralErrorType()) {
                    case DOWNLOAD_ERROR:
                        viewChanged.setValue(ViewType.ERROR_OTA_DOWNLOAD.setArgs(args));
                        break;
                    case FLASHING_ERROR:
                        viewChanged.setValue(ViewType.ERROR_OTA_FLASHING.setArgs(args));
                        break;
                    case UNDEFINED:
                        viewChanged.setValue(ViewType.ERROR_OTA_UNDEFINED.setArgs(args));
                        break;
                }
            } else {
                viewChanged.setValue(ViewType.ERROR_OTA_UNDEFINED.setArgs(args));
            }
            firmwareUpdateProgress.setValue(0.0);
        }

        private void dispose() {
            otaManager.dispose();

            if (firmwareUpdateProgressDisposable != null) {
                firmwareUpdateProgressDisposable.dispose();
            }

            if (otaServiceReadyDisposable != null) {
                otaServiceReadyDisposable.dispose();
            }

            if (firmwareUpdateStateDisposable != null) {
                firmwareUpdateStateDisposable.dispose();
            }

            if (speakerInfoDisposable != null) {
                speakerInfoDisposable.dispose();
            }

            if (deviceServiceReadyDisposable != null) {
                deviceServiceReadyDisposable.dispose();
            }

            if (connectionDisposable != null) {
                connectionDisposable.dispose();
            }

            if (checkForUpdateDisposable != null) {
                checkForUpdateDisposable.dispose();
            }
        }
    }
}

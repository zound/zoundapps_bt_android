package com.zoundindustries.marshallbt.viewmodels.error;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.annotation.NonNull;
import android.view.View;

import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;

public interface ErrorViewModel {

    interface Inputs {
        /**
         * OnClick event from the back button.
         *
         * @param view that was clicked.
         */
        void backButtonClicked(@NonNull View view);

        /**
         * OnClick event from the main action button.
         *
         * @param view that was clicked.
         */
        void actionButtonClicked(@NonNull View view);

        /**
         * OnBackPressed event from system back button.
         */
        void systemBackButtonPressed();
    }

    interface Outputs {

        /**
         * Get {@link io.reactivex.Observable} for changed view.
         *
         * @return viewType to be started.
         */
        @NonNull
        MutableLiveData<ViewType> isViewChanged();
    }

    class Body extends ViewModel implements Inputs, Outputs {

        public Inputs inputs = this;
        public Outputs outputs = this;

        private final MutableLiveData<ViewType> viewChange;

        private final ViewType errorType;
        private final ViewType errorOrigin;

        public Body(@NonNull ViewType errorType, @NonNull ViewType errorOrigin) {
            this.errorType = errorType;
            this.errorOrigin = errorOrigin;
            viewChange = new MutableLiveData<>();
            initStates();
        }

        @Override
        public void backButtonClicked(@NonNull View view) {
            switch (errorType) {
                case ERROR_OTA_FLASHING:
                case ERROR_OTA_DOWNLOAD:
                    viewChange.setValue(ViewType.HOME_SCREEN);
            }
        }

        @Override
        public void actionButtonClicked(@NonNull View view) {
            switch (errorType) {
                case ERROR_OTA_FLASHING:
                case ERROR_OTA_DOWNLOAD:
                case ERROR_OTA_UNDEFINED:
                    changeViewForErrorOrigin();
                    break;
                case ERROR_BLUETOOTH:
                    viewChange.setValue(ViewType.GLOBAL_SETTINGS_BLUETOOTH);
                    break;
                case ERROR_NO_NETWORK:
                    viewChange.setValue(ViewType.GLOBAL_SETTINGS_NETWORK);
                    break;
                case ERROR_LOCATION:
                    viewChange.setValue(ViewType.GLOBAL_SETTINGS_LOCATION);
                    break;
            }
        }

        private void changeViewForErrorOrigin() {
            if (errorOrigin == ViewType.ABOUT_DEVICE) {
                viewChange.setValue(ViewType.ABOUT_DEVICE);
            } else if (errorOrigin == ViewType.OTA_PROGRESS) {
                viewChange.setValue(ViewType.OTA_PROGRESS);
            }
        }

        @Override
        public void systemBackButtonPressed() {
            viewChange.setValue(ViewType.HOME_SCREEN);
        }

        @NonNull
        @Override
        public MutableLiveData<ViewType> isViewChanged() {
            return viewChange;
        }

        private void initStates() {

        }
    }
}



package com.zoundindustries.marshallbt.viewmodels.factories;

import android.app.Application;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

public class BaseDeviceViewModelFactory  implements ViewModelProvider.Factory {

    protected final Application application;
    protected final String deviceId;

    public BaseDeviceViewModelFactory(Application application, String deviceId) {
        this.application = application;
        this.deviceId = deviceId;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        throw new UnsupportedOperationException("Unsupported operation");
    }
}

package com.zoundindustries.marshallbt.viewmodels.factories.homescreen;

import android.app.Application;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.viewmodels.homescreen.DeviceListViewModel;

import java.util.List;

/**
 * Factory providing new DeviceList ViewModel if needed or old object if it exists
 */
public class DeviceListViewModelFactory implements ViewModelProvider.Factory {
    private Application application;
    private List<BaseDevice> homeDevices;

    public DeviceListViewModelFactory(Application application, List<BaseDevice> homeDevices) {
        this.application = application;
        this.homeDevices = homeDevices;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new DeviceListViewModel.Body(application, homeDevices);
    }
}

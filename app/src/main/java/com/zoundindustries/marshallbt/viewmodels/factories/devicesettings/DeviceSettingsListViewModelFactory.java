package com.zoundindustries.marshallbt.viewmodels.factories.devicesettings;

import android.app.Application;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.viewmodels.devicesettings.DeviceSettingsListViewModel;
import com.zoundindustries.marshallbt.viewmodels.factories.BaseDeviceViewModelFactory;

/**
 * Factory providing new DeviceSettingsList ViewModel if needed or old object if it exists
 */
public class DeviceSettingsListViewModelFactory extends BaseDeviceViewModelFactory {

    public DeviceSettingsListViewModelFactory(Application application, String deviceId) {
        super(application, deviceId);
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new DeviceSettingsListViewModel.Body(application, deviceId);
    }
}

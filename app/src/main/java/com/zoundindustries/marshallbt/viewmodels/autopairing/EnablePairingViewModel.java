package com.zoundindustries.marshallbt.viewmodels.autopairing;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.discovery.DeviceDiscoveryManager;
import com.zoundindustries.marshallbt.ui.ViewFlowController;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * View model wrapping enabling pairing mode and creating BT classic bond.
 * This view is shown when pairing mode is disabled on the device.
 * The flow contains of two steps:
 * <p>
 * - Show indication that user must turn off headphones (turnOffDescriptionAlpha).
 * Turn off description is highlighted (full alpha)
 * - After disconnection show indication that user must turn on headphones in pairing mode
 * (turnOnWithPairingModeDescriptionAlpha). Turn on in pairing mode  description is
 * highlighted (full alpha)
 * - Wait for reconnection and proceed with BT classic bonding when pairing mode is enabled.
 */
public interface EnablePairingViewModel {

    interface Inputs {
        public void onDoneClicked();
    }

    interface Outputs {

        /**
         * Get observable for enable pairing mode description part 1(switch off device) alpha.
         *
         * @return observable for playing info alpha.
         */
        @NonNull
        MutableLiveData<Float> getTurnOffDescriptionAlpha();

        /**
         * Get observable for enable pairing mode description part 2 alpha.
         *
         * @return observable for playing info alpha.
         */
        @NonNull
        MutableLiveData<Float> getTurnOnPairingModeDescriptionAlpha();

        /**
         * Get observable for pairing screen states.
         *
         * @return observable for pairing screen states.
         */
        @NonNull
        MutableLiveData<Body.EnablePairingViewState> getPairingState();

        /**
         * Get {@link io.reactivex.Observable} for changed view.
         *
         * @return viewType to be started.
         */
        MutableLiveData<ViewFlowController.ViewType> isViewChanged();

        /**
         * Get the name of paired device.
         *
         * @return paired device name.
         */
        String getDeviceName();
    }

    class Body extends BaseAutoPairingViewModel implements Inputs, Outputs {

        private final int RECONNECTION_DELAY_IN_MS = 2000;
        private final int PAIRING_MODE_READ_DELAY_IN_MS = 3000;
        private final int READY_TO_CONNECT_DELAY_IN_MS = 1000;

        @Override
        public void onDoneClicked() {
            deviceManager.setScanAllowed(true);
        }

        /**
         * Enum reflecting possible UI states of the Enable Pairing Fragment.
         * <p>
         * ONGOING - user is guided to turn on pairing mode,
         * DONE    - pairing is done,
         * RETRY   - error occurred due to failed bonding or timeout.
         */
        public enum EnablePairingViewState {
            ONGOING,
            DONE,
            RETRY
        }

        public final Inputs inputs = this;
        public final Outputs outputs = this;

        private final MutableLiveData<Float> turnOffDescriptionAlpha;
        private final MutableLiveData<Float> turnOnWithPairingModeDescriptionAlpha;
        private final MutableLiveData<EnablePairingViewState> enablePairingViewState;
        private final MutableLiveData<ViewFlowController.ViewType> viewChanged;

        private final BaseDevice device;
        private final DeviceDiscoveryManager deviceDiscoveryManager;
        private final Handler handler;

        private Disposable serviceReadyDisposable;
        private Disposable connectionStateDisposable;

        public Body(@NonNull Application application,
                    @NonNull String deviceId) {
            super(application, new DeviceManager(application), deviceId);

            turnOffDescriptionAlpha = new MutableLiveData<>();
            turnOnWithPairingModeDescriptionAlpha = new MutableLiveData<>();
            enablePairingViewState = new MutableLiveData<>();
            viewChanged = new MutableLiveData<>();

            deviceDiscoveryManager = new DeviceDiscoveryManager(getApplication());
            device = deviceManager.getDeviceFromId(deviceId);
            handler = new Handler(Looper.getMainLooper());

            deviceManager.setScanAllowed(false);
            device.getBaseDeviceStateController().inputs.setConnectionInfo(BaseDevice.ConnectionState.DISCONNECTED);

            setupLiveData();
            initDeviceServiceObserver();
        }

        @NonNull
        @Override
        public MutableLiveData<Float> getTurnOffDescriptionAlpha() {
            return turnOffDescriptionAlpha;
        }

        @NonNull
        @Override
        public MutableLiveData<Float> getTurnOnPairingModeDescriptionAlpha() {
            return turnOnWithPairingModeDescriptionAlpha;
        }

        @NonNull
        @Override
        public MutableLiveData<EnablePairingViewState> getPairingState() {
            return enablePairingViewState;
        }

        @Override
        public MutableLiveData<ViewFlowController.ViewType> isViewChanged() {
            return viewChanged;
        }

        @Override
        public String getDeviceName() {
            return device.getBaseDeviceStateController()
                    .outputs.getSpeakerInfoCurrentValue()
                    .getDeviceName();
        }


        @Override
        public void onPairingModeDisabled() {
            turnOffDescriptionAlpha.setValue(1.0f);
            turnOnWithPairingModeDescriptionAlpha.setValue(0.5f);
        }

        @Override
        protected void onPairingModeEnabled() {
            //nop
        }


        @Override
        public void onPairingFailed() {
            deviceManager.setScanAllowed(true);
            enablePairingViewState.postValue(EnablePairingViewState.RETRY);
            device.getBaseDeviceStateController().inputs
                    .setConnectionInfo(BaseDevice.ConnectionState.DISCONNECTED);
        }

        @Override
        public void onPairingDone() {
            enablePairingViewState.setValue(EnablePairingViewState.DONE);

        }

        @Override
        protected void onCleared() {
            dispose();
            super.onCleared();
        }

        private void setupLiveData() {
            turnOffDescriptionAlpha.setValue(1.0f);
            turnOnWithPairingModeDescriptionAlpha.setValue(0.5f);
        }

        private void initDeviceServiceObserver() {
            serviceReadyDisposable = deviceManager.isServiceReady()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(serviceConnected -> {
                        if (serviceConnected) {
                            initObservers();
                        }
                    });
        }

        private void initObservers() {
            startPairing();
        }

        private void dispose() {
            deviceManager.setScanAllowed(true);
            device.getBaseDeviceStateController().inputs
                    .setConnectionInfo(BaseDevice.ConnectionState.DISCONNECTED);

            handler.removeCallbacksAndMessages(null);

            if (serviceReadyDisposable != null) {
                serviceReadyDisposable.dispose();
            }

            if (connectionStateDisposable != null) {
                connectionStateDisposable.dispose();
            }
        }
    }
}

package com.zoundindustries.marshallbt.viewmodels.player.sources;

import android.app.Application;
import androidx.lifecycle.MutableLiveData;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.ui.ViewFlowController;

/**
 * Class to handle logic behind RCA audio source
 */
public interface RcaSourceViewModel {

    /**
     * Body for rca source view model
     */
    final class Body extends BaseSourceViewModel.Body {

        public Body(Application app, @NonNull String deviceId) {
            super(app, deviceId);
        }

        @VisibleForTesting
        public Body(Application app, @NonNull String deviceId,
                    @NonNull DeviceManager deviceManager,
                    @NonNull FirebaseAnalyticsManager firebaseAnalyticsManager) {
            super(app, deviceId, deviceManager, firebaseAnalyticsManager);
        }

        @Override
        protected BaseDevice.SourceType getSourceType() {
            return BaseDevice.SourceType.RCA;
        }

        @Override
        public int getSourceTypeImage() {
            return R.drawable.source_rca;
        }

        @Override
        public MutableLiveData<String> getDisplayedDeviceName() {
            return null;
        }

        @NonNull
        @Override
        public MutableLiveData<ViewFlowController.ViewType> isViewChanged() {
            return super.viewChanged;
        }
    }

}


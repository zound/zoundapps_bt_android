package com.zoundindustries.marshallbt.viewmodels.setup;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.zoundindustries.marshallbt.model.setup.SaveDataManager;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

/**
 * Class used to handle save data UI logic
 */
public interface SaveDataViewModel {

    // no inputs needed for this view

    interface Outputs {
        /**
         * Notify about view completed its actions and should end
         *
         * @return liveData observable
         */
        @NonNull
        MutableLiveData<Boolean> isViewCompleted();
    }

    final class Body extends ViewModel implements Outputs {

        public final Outputs outputs = this;

        private final MutableLiveData<Boolean> viewCompleted;

        private SaveDataManager saveDataManager;

        private Disposable saveDataDisposable;

        @Inject
        public Body() {
            this(new SaveDataManager());
        }

        public Body(SaveDataManager saveDataManager) {
            this.saveDataManager = saveDataManager;
            viewCompleted = new MutableLiveData<>();
            initStates();
            initSubscribers();
        }

        /*      outputs     */
        @Override
        @NonNull
        public MutableLiveData<Boolean> isViewCompleted() {
            return viewCompleted;
        }

        @Override
        protected void onCleared() {
            super.onCleared();
            saveDataDisposable.dispose();
        }

        private void initStates() {
            viewCompleted.setValue(false);
        }

        private void initSubscribers() {
            saveDataDisposable = saveDataManager.saveData()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(aBoolean -> viewCompleted.setValue(true));
        }
    }
}

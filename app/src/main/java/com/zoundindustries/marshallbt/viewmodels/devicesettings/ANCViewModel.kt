package com.zoundindustries.marshallbt.viewmodels.devicesettings

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.zoundindustries.marshallbt.model.device.BaseDevice
import com.zoundindustries.marshallbt.model.device.DeviceManager
import com.zoundindustries.marshallbt.model.devicesettings.AncMode
import com.zoundindustries.marshallbt.ui.ViewFlowController
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

// writing this class in Kotlin as a test of the combined languages

interface ANCViewModel {
    interface Inputs {
        /**
         * Indicates that tab was changed
         */
        fun onAncTabChanged(ancType: AncMode)

        /**
         * Indicates that cancelling value was changed
         */
        fun onAncValueChanged(value: Int)

        /**
         * Indicates that monitoring value was changed
         */
        fun onMonitorValueChanged(value: Int)
    }

    interface Outputs {
        /**
         * Notify about changing the active tab type
         */
        val notifyTabType: MutableLiveData<AncMode>

        /**
         * Notify the cancelling level value
         */
        val notifyAncValue: MutableLiveData<Int>

        /**
         * Notify the monitoring level value
         */
        val notifyMonitorValue: MutableLiveData<Int>

        val notifyViewChanged: LiveData<ViewFlowController.ViewType>
    }

    class Body(val app: Application, val deviceId: String) : AndroidViewModel(app), Inputs, Outputs {
        val inputs = this
        val outputs = this
        private var device: BaseDevice?

        private var deviceManager: DeviceManager = DeviceManager(app)
        private var serviceReadyDisposable: Disposable?
        private var ancDataDisposable: Disposable? = null
        private var connectionDisposable: Disposable? = null

        //inputs
        override fun onAncTabChanged(ancType: AncMode) {
            if (ancType != notifyTabType.value) {
                device?.baseDeviceStateController?.inputs?.writeAncMode(ancType)
                notifyTabType.value = ancType
            }
        }

        override fun onAncValueChanged(value: Int) {
            device?.baseDeviceStateController?.inputs?.writeAncLevel(value)
        }

        override fun onMonitorValueChanged(value: Int) {
            device?.baseDeviceStateController?.inputs?.writeMonitorLevel(value)
        }

        // outputs
        override val notifyTabType: MutableLiveData<AncMode> = MutableLiveData()
        override val notifyAncValue: MutableLiveData<Int> = MutableLiveData()
        override val notifyMonitorValue: MutableLiveData<Int> = MutableLiveData()

        override val notifyViewChanged: LiveData<ViewFlowController.ViewType>
            get() = viewChanged

        val viewChanged = MutableLiveData<ViewFlowController.ViewType>()

        init {
            device = deviceManager.getDeviceFromId(deviceId)

            serviceReadyDisposable = deviceManager
                    .isServiceReady
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { isReady ->
                        isReady?.let {
                            initObserver()
                        }
                    }
        }

        private fun initObserver() {
            device?.baseDeviceStateController?.inputs?.readAncStatus()

            connectionDisposable = device?.baseDeviceStateController?.outputs
                    ?.connectionInfo
                    ?.observeOn(AndroidSchedulers.mainThread())
                    ?.subscribe { connectionState ->
                        if (connectionState == BaseDevice.ConnectionState.DISCONNECTED) {
                            viewChanged.value = ViewFlowController.ViewType.HOME_SCREEN
                        }
                    }

            ancDataDisposable = device?.baseDeviceStateController?.outputs?.ancData
                    ?.observeOn(AndroidSchedulers.mainThread())
                    ?.subscribe { ancData ->
                        notifyTabType.value = ancData.ancMode
                        notifyAncValue.value = ancData.ancValue
                        notifyMonitorValue.value = ancData.monitorValue
                    }
        }

        override fun onCleared() {
            ancDataDisposable?.let {
                if (!it.isDisposed)
                    it.dispose()
            }

            connectionDisposable?.let{
                if (!it.isDisposed)
                    it.dispose()
            }
        }
    }
}

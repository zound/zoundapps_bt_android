package com.zoundindustries.marshallbt.viewmodels.setup;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import android.content.res.Resources;
import androidx.annotation.VisibleForTesting;
import android.view.View;

import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.utils.CommonPreferences;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;

/**
 * ViewModel for ShareData setup view
 */
public interface ShareDataViewModel {

    enum ShareDataLayoutType {
        INITIAL_SHARE_DATA,
        MAIN_MENU_SHARE_DATA
    }

    interface Inputs {
        /**
         * Handle share data option being switched
         *
         * @param v view on which the method is run
         */
        void shareDataSwitched(@NonNull View v, boolean isChecked);

        /**
         * Handle next button being tapped
         *
         * @param v view on which the method is run
         */
        void nextButtonTapped(@NonNull View v);

        /**
         * Reset variables responsible for view transitions
         */
        void resetViewTransitionVariables();

        /**
         * Handle skip button being tapped
         *
         * @param v view on which the method is run
         */
        void skipButtonTapped(View v);
    }

    interface Outputs {

        /**
         * Notify about state of shareData switch
         *
         * @return LiveData observable
         */
        @NonNull
        MutableLiveData<Boolean> isShareDataSwitchEnabled();

        /**
         * Get {@link MutableLiveData} for changed view.
         *
         * @return LiveData observable
         */
        @NonNull
        MutableLiveData<ViewType> isViewChanged();

        /**
         * Get correct shareData layout type.
         *
         * @return ShareDataLayoutType object
         */
        @NonNull
        ShareDataLayoutType getLayoutType();

        /**
         * Get string for correct confirmation button text.
         */
        @NonNull
        String getConfirmationButtonText();

        /**
         * Get {@link MutableLiveData} for visibility of skip button.
         *
         * @return LiveData observable
         */
        @NonNull
        MutableLiveData<Integer> getSkipButtonVisibility();
    }

    final class Body extends AndroidViewModel implements Inputs, Outputs {

        public final Inputs inputs = this;
        public final Outputs outputs = this;

        private final MutableLiveData<Boolean> shareDataSwitchEnabled;
        private final MutableLiveData<ViewType> viewChanged;
        private final MutableLiveData<Integer> skipButtonVisibility;

        private final CommonPreferences preferences;
        private final FirebaseAnalyticsManager firebaseAnalyticsManager;
        private static ShareDataLayoutType layoutType;

        private static String confirmationButtonText;

        @Inject
        public Body(Application application) {
            this(application,
                    ((BluetoothApplication) application).getAppComponent().commonPreferences(),
                    ((BluetoothApplication) application).getAppComponent().firebaseWrapper());
        }

        @VisibleForTesting
        public Body(@NonNull Application application,
                    @NonNull CommonPreferences preferences,
                    @NonNull FirebaseAnalyticsManager firebaseAnalyticsManager) {
            super(application);
            shareDataSwitchEnabled = new MutableLiveData<>();
            viewChanged = new MutableLiveData<>();
            skipButtonVisibility = new MutableLiveData<>();
            this.preferences = preferences;
            this.firebaseAnalyticsManager = firebaseAnalyticsManager;
            initStates(application.getResources());
        }

        /*      inputs      */
        @Override
        public void shareDataSwitched(@NonNull View v, boolean isChecked) {
            preferences.setIsUserSharingData(isChecked);
            shareDataSwitchEnabled.setValue(isChecked);
        }

        @Override
        public void nextButtonTapped(@NonNull View v) {
            firebaseAnalyticsManager.eventShareAnalyticsStatusChanged(preferences.isUserSharingData());
            setCorrectViewChanged();
            setFirstTimeShareData();
        }

        private void setCorrectViewChanged() {
            switch (layoutType) {
                case MAIN_MENU_SHARE_DATA:
                    viewChanged.setValue(ViewType.HOME_SCREEN);
                    break;
                case INITIAL_SHARE_DATA:
                    viewChanged.setValue(ViewType.SAVE_DATA_SCREEN);
                    break;
            }
        }

        @Override
        public void resetViewTransitionVariables() {
            viewChanged.setValue(ViewType.UNKNOWN);
        }

        @Override
        public void skipButtonTapped(View v) {
            firebaseAnalyticsManager.eventShareDataSkipped();
            setCorrectViewChanged();
            setFirstTimeShareData();
        }

        private void setFirstTimeShareData() {
            preferences.setFirstRunOfShareData(false);
        }

        /*      outputs     */
        @Override
        @NonNull
        public MutableLiveData<Boolean> isShareDataSwitchEnabled() {
            return shareDataSwitchEnabled;
        }

        @Override
        public MutableLiveData<ViewType> isViewChanged() {
            return viewChanged;
        }

        @Override
        public ShareDataLayoutType getLayoutType() {
            return layoutType;
        }

        @Override
        public String getConfirmationButtonText() {
            return confirmationButtonText;
        }

        @Override
        public MutableLiveData<Integer> getSkipButtonVisibility() {
            return skipButtonVisibility;
        }

        private void initStates(@NonNull Resources resources) {
            setLayoutType();
            setVisibilityAndResourcesOfViews(resources);
            shareDataSwitchEnabled.setValue(preferences.isUserSharingData());
            resetViewTransitionVariables();
        }

        private void setLayoutType() {
            layoutType = preferences.isFirstTimeShareData() ?
                    ShareDataLayoutType.INITIAL_SHARE_DATA : ShareDataLayoutType.MAIN_MENU_SHARE_DATA;
        }

        private void setVisibilityAndResourcesOfViews(Resources resources) {
            switch (layoutType) {
                case INITIAL_SHARE_DATA:
                    confirmationButtonText = resources.getString(R.string.appwide_next_uc);
                    skipButtonVisibility.setValue(View.VISIBLE);
                    break;
                case MAIN_MENU_SHARE_DATA:
                    skipButtonVisibility.setValue(View.GONE);
                    confirmationButtonText = resources.getString(R.string.appwide_done_uc);
                    break;
            }
        }
    }
}

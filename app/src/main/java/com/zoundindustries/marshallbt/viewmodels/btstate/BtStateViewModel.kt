package com.zoundindustries.marshallbt.viewmodels.btstate

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import com.zoundindustries.marshallbt.model.device.DeviceManager
import com.zoundindustries.marshallbt.model.discovery.DeviceDiscoveryManager

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

interface BtStateViewModel {

    interface Outputs {

        /**
         * Get [io.reactivex.Observable] for BT availability.
         *
         * @return viewType to be started.
         */
        val isBtAvailable: MutableLiveData<Boolean>
    }

    class Body(application: Application) : ViewModel(), Outputs {

        var outputs: Outputs = this

        override val isBtAvailable: MutableLiveData<Boolean> = MutableLiveData()

        private val deviceManager: DeviceManager = DeviceManager(application)
        private val deviceDiscoveryManager: DeviceDiscoveryManager =
                DeviceDiscoveryManager(application)

        private var btStateDisposable: Disposable? = null
        private var serviceReadyDisposable: Disposable? = null

        init {
            serviceReadyDisposable = deviceDiscoveryManager.isServiceReady
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { serviceConnected ->
                        if (serviceConnected) {
                            initObservers()
                        }
                    }
        }

        override fun onCleared() {
            dispose()
        }

        private fun initObservers() {
            btStateDisposable = deviceManager.systemBtState
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { systemBtOn ->
                        isBtAvailable.setValue(systemBtOn)
                    }
        }

        private fun dispose() {
            deviceDiscoveryManager.dispose()
            serviceReadyDisposable?.dispose()
            btStateDisposable?.dispose()
        }
    }
}



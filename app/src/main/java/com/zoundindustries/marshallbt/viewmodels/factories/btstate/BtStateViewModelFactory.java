package com.zoundindustries.marshallbt.viewmodels.factories.btstate;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.zoundindustries.marshallbt.viewmodels.btstate.BtStateViewModel;

/**
 * Factory providing new BtStateViewModel if needed or old object if it exists
 */
public class BtStateViewModelFactory implements ViewModelProvider.Factory {

    protected final Application application;

    public BtStateViewModelFactory(Application application) {
        this.application = application;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new BtStateViewModel.Body(application);
    }
}

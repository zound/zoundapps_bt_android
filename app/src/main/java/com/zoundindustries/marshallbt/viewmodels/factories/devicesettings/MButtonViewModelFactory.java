package com.zoundindustries.marshallbt.viewmodels.factories.devicesettings;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;

import com.zoundindustries.marshallbt.viewmodels.devicesettings.MButtonViewModel;
import com.zoundindustries.marshallbt.viewmodels.factories.BaseDeviceViewModelFactory;

public class MButtonViewModelFactory extends BaseDeviceViewModelFactory {
    public MButtonViewModelFactory(Application application, String deviceId) {
        super(application, deviceId);
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new MButtonViewModel.Body(application, deviceId);
    }
}

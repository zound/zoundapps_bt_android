package com.zoundindustries.marshallbt.viewmodels.welcome;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.annotation.NonNull;
import android.view.View;

import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.utils.CommonPreferences;

import javax.inject.Inject;

public interface WelcomeViewModel {
    interface Inputs {

        /**
         * Notifies errorViewModel about Accept button clicked.
         */
        void onAcceptButtonClicked(@NonNull View v);

        /**
         * Notifies about tac agreement checkbox status changed.
         */
        void onTacAgreementChecked(@NonNull View v, boolean isChecked);

        /**
         * Handle terms and conditions link click
         */
        void onTacLinkClicked();

        /**
         * Reset variables responsible for view transitions
         */
        void resetViewTransitionVariables();
    }

    interface Outputs {

        /**
         * Get {@link io.reactivex.Observable} for changed view.
         *
         * @return viewType to be started.
         */
        @NonNull
        MutableLiveData<ViewType> isViewChanged();

        /**
         * Get {@link io.reactivex.Observable} that inform about current visibility of confirm button.
         *
         * @return visibility of confirm button.
         */
        @NonNull
        MutableLiveData<Integer> getConfirmButtonVisibility();
    }

    /**
     * Body for WelcomeViewModel. Used for greetings screen display.
     */
    final class Body extends AndroidViewModel implements Inputs, Outputs {

        public final Inputs inputs = this;

        public final Outputs outputs = this;

        private final MutableLiveData<ViewType> viewChanged;
        private final MutableLiveData<Integer> confirmButtonVisibility;

        private final CommonPreferences commonPreferences;

        @Inject
        public Body(@NonNull Application application) {
            this(application, new CommonPreferences(application));
        }

        public Body(@NonNull Application application,
                    @NonNull CommonPreferences commonPreferences) {
            super((application));
            this.commonPreferences = commonPreferences;
            viewChanged = new MutableLiveData<>();
            confirmButtonVisibility = new MutableLiveData<>();
        }

        @Override
        public void onAcceptButtonClicked(@NonNull View v) {
            commonPreferences.setWelcomeScreenVisible(false);
            viewChanged.setValue(ViewType.SETUP_SCREEN);
        }

        @Override
        public void onTacAgreementChecked(@NonNull View v, boolean isChecked) {
            confirmButtonVisibility.setValue(isChecked ? View.VISIBLE : View.INVISIBLE);
        }

        @Override
        public void onTacLinkClicked() {
            viewChanged.setValue(ViewType.TAC_SCREEN);
        }

        @Override
        public void resetViewTransitionVariables() {
            viewChanged.setValue(ViewType.UNKNOWN);
        }

        @NonNull
        @Override
        public MutableLiveData<ViewType> isViewChanged() {
            return viewChanged;
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getConfirmButtonVisibility() {
            return confirmButtonVisibility;
        }
    }
}

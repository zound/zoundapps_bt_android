package com.zoundindustries.marshallbt.viewmodels.ota;

import android.app.Application;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import android.view.View;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.ota.OTAManager;
import com.zoundindustries.marshallbt.services.DeviceService;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;



import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public interface OTACompletedViewModel {
    interface Inputs {

        /**
         * Notifies errorViewModel about Next button clicked.
         */
        void onNextButtonClicked(View v);
    }

    interface Outputs {

        /**
         * Get {@link io.reactivex.Observable} for changed view.
         *
         * @return viewType to be started.
         */
        @NonNull
        MutableLiveData<ViewType> isViewChanged();

        /**
         * Get the image resource ID for speaker
         *
         * @return int which is resource ID
         */
        @NonNull
        MutableLiveData<Integer> getSpeakerImageResourceId();
    }

    /**
     * Body for OTACompletedViewModel. Used to show OTA completed screen.
     */
    final class Body extends ViewModel implements Inputs, Outputs {

        public final Inputs inputs = this;
        public final Outputs outputs = this;

        private final MutableLiveData<ViewType> viewChanged;
        private final MutableLiveData<Integer> speakerImageResourceId;

        private final DeviceManager deviceManager;
        private final OTAManager otaManager;

        private final String deviceId;
        private BaseDevice device;

        private Disposable serviceReadyDisposable;

        public Body(@NonNull Application application, @NonNull String deviceId) {
            this(deviceId, new DeviceManager(application), new OTAManager(application));
        }

        @VisibleForTesting
        protected Body(@NonNull String deviceId,
                       @NonNull DeviceManager deviceManager,
                       @NonNull OTAManager otaManager) {
            viewChanged = new MutableLiveData<>();
            speakerImageResourceId = new MutableLiveData<>();

            this.deviceId = deviceId;
            this.deviceManager = deviceManager;
            this.otaManager = otaManager;

            setupServiceConnectionObserver();
        }

        @Override
        public void onNextButtonClicked(View v) {
            if (device != null) {
                viewChanged.setValue(ViewType.BACK);
                otaManager.finalizeForcedOTA();
            }
        }

        @NonNull
        @Override
        public MutableLiveData<ViewType> isViewChanged() {
            return viewChanged;
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getSpeakerImageResourceId() {
            return speakerImageResourceId;
        }

        @Override
        protected void onCleared() {
            dispose();
        }

        private void setupServiceConnectionObserver() {
            serviceReadyDisposable = deviceManager
                    .isServiceReady()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(serviceConnected -> {
                        if (serviceConnected) {
                            device = deviceManager.getDeviceFromId(deviceId);
                            getSpeakerImage();
                        }
                    });
        }

        private void getSpeakerImage() {
            speakerImageResourceId.setValue(deviceManager.getImageResourceId(
                    deviceId, DeviceService.DeviceImageType.MEDIUM));
        }

        private void dispose() {
            if (serviceReadyDisposable != null) {
                serviceReadyDisposable.dispose();
            }
        }
    }
}

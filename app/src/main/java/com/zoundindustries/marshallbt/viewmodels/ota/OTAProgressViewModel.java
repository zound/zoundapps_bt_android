package com.zoundindustries.marshallbt.viewmodels.ota;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.model.ota.IOTAService;
import com.zoundindustries.marshallbt.model.ota.IOTAService.UpdateState;
import com.zoundindustries.marshallbt.model.ota.OTAManager;
import com.zoundindustries.marshallbt.model.ota.OtaErrorThrowable;
import com.zoundindustries.marshallbt.model.ota.pojo.FirmwareFileMetaData;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.utils.ViewExtrasGlobals;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import retrofit2.Response;

public interface OTAProgressViewModel {

    interface Inputs {

        /**
         * Notifies errorViewModel about changed page in viewPager.
         * Updates dots menu drawable for the slides.
         *
         * @param currentPage id of the currently active page.
         */
        void onPageChanged(int currentPage);
    }

    interface Outputs {

        /**
         * Get liveData observable for dots menu {@link Drawable}.
         *
         * @return liveData observable.
         */
        @NonNull
        MutableLiveData<Drawable> getDots();

        /**
         * Get liveData observable for update progress.
         *
         * @return liveData observable.
         */
        @NonNull
        MutableLiveData<Double> getFirmwareUpdateProgress();

        /**
         * Get liveData observable for update state {@link Drawable}.
         *
         * @return liveData observable.
         */
        @NonNull
        MutableLiveData<UpdateState> getFirmwareUpdateState();

        /**
         * Get current FirmwareFileMetaData.
         *
         * @return FirmwareFileMetaData.
         */
        @Nullable
        FirmwareFileMetaData getFirmwareUpdateMetaData();

        /**
         * Get observable on completed OTA.
         * Temporary code. Will be moved to separate pattern/class.
         *
         * @return flag indicating completion state.
         */
        @NonNull
        MutableLiveData<ViewType> isViewChanged();
    }

    /**
     * ViewModel providing data for OTA firmware update.
     */
    final class Body extends AndroidViewModel implements Inputs, Outputs {
        private static final String TAG = OTAProgressViewModel.class.getSimpleName();

        public final Inputs inputs = this;
        public final Outputs outputs = this;

        private final MutableLiveData<Drawable> dots;
        private final MutableLiveData<Double> firmwareUpdateProgress;
        private final MutableLiveData<UpdateState> firmwareUpdateState;
        private final MutableLiveData<ViewType> viewChange;

        private OTAManager otaManager;
        private final FirebaseAnalyticsManager firebaseAnalyticsManager;
        private String deviceId;

        private Disposable firmwareUpdateProgressDisposable;
        private Disposable firmwareUpdateStateDisposable;
        private Disposable serviceReadyDisposable;
        private Disposable checkForFirmwareDisposable;
        private FirmwareFileMetaData firmwareFileMetaData;

        /**
         * Constructor for Body class.
         *
         * @param application to be used asd Context for starting DeviceService.
         */
        public Body(@NonNull Application application,
                    @NonNull String deviceId) {
            this(application,
                    deviceId,
                    new OTAManager(application),
                    ((BluetoothApplication) application).getAppComponent().firebaseWrapper());
        }

        /**
         * Constructor with {@link OTAManager} parameter.
         *
         * @param application to be used asd Context for starting DeviceService.
         * @param otaManager  to be used for OTA SDK calls.
         */
        @VisibleForTesting
        public Body(@NonNull Application application,
                    @NonNull String deviceId,
                    @NonNull OTAManager otaManager,
                    @NonNull FirebaseAnalyticsManager firebaseAnalyticsManager) {
            super(application);
            this.deviceId = deviceId;

            this.otaManager = otaManager;
            this.firebaseAnalyticsManager = firebaseAnalyticsManager;

            dots = new MutableLiveData<>();
            firmwareUpdateProgress = new MutableLiveData<>();
            firmwareUpdateState = new MutableLiveData<>();
            viewChange = new MutableLiveData<>();

            initDefaultLiveDataValues();

            setupServiceConnectionObserver();
        }

        @Override
        public void onPageChanged(int currentPage) {
            Resources resources = getApplication().getResources();

            switch (currentPage) {
                case 0:
                    dots.setValue(resources.getDrawable(R.drawable.ota_slide_dot1));
                    break;
                case 1:
                    dots.setValue(resources.getDrawable(R.drawable.ota_slide_dot2));
                    break;
                case 2:
                    dots.setValue(resources.getDrawable(R.drawable.ota_slide_dot3));
                    break;
            }
        }

        @NonNull
        @Override
        public MutableLiveData<Drawable> getDots() {
            return dots;
        }

        @NonNull
        @Override
        public MutableLiveData<Double> getFirmwareUpdateProgress() {
            return firmwareUpdateProgress;
        }

        @NonNull
        @Override
        public MutableLiveData<UpdateState> getFirmwareUpdateState() {
            return firmwareUpdateState;
        }

        @Nullable
        @Override
        public FirmwareFileMetaData getFirmwareUpdateMetaData() {
            return firmwareFileMetaData;
        }

        @NonNull
        @Override
        public MutableLiveData<ViewType> isViewChanged() {
            return viewChange;
        }

        @Override
        protected void onCleared() {
            dispose();
        }

        private void initDefaultLiveDataValues() {
            firmwareUpdateState.setValue(UpdateState.NOT_STARTED);
            firmwareUpdateProgress.setValue(0.0);
        }

        private void initObservers() {
            Observable<Double> progressObservable = otaManager.getFirmwareUpdateProgress(deviceId);
            if (progressObservable != null) {
                firmwareUpdateProgressDisposable = progressObservable
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(firmwareUpdateProgress::setValue,
                                Throwable::printStackTrace);
            }

            Observable<IOTAService.UpdateState> stateObservable =
                    otaManager.getFirmwareUpdateState(deviceId);
            if (stateObservable != null) {
                firmwareUpdateStateDisposable = stateObservable
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(this::handleOtaStateChange,
                                this::handleOtaError);
            }
        }

        private void handleOtaStateChange(UpdateState updateState) {
            firmwareUpdateState.setValue(updateState);

            if (updateState == UpdateState.COMPLETED) {
                viewChange.setValue(ViewType.OTA_COMPLETED);
                firebaseAnalyticsManager.eventOtaCompleted();
            }
        }

        private void handleOtaError(Throwable throwable) {
            firebaseAnalyticsManager.eventOtaFailed(throwable.getMessage());
            firebaseAnalyticsManager.eventErrorOccurredOta(throwable);

            Bundle args = new Bundle();
            args.putParcelable(ViewExtrasGlobals.EXTRA_OTA_FIRMWARE_META_DATA,
                    firmwareFileMetaData);

            if (throwable instanceof OtaErrorThrowable) {
                switch (((OtaErrorThrowable) throwable)
                        .getErrorType().getGeneralErrorType()) {
                    case DOWNLOAD_ERROR:
                        viewChange.setValue(ViewType.ERROR_OTA_DOWNLOAD.setArgs(args));
                        break;
                    case FLASHING_ERROR:
                        viewChange.setValue(ViewType.ERROR_OTA_FLASHING.setArgs(args));
                        break;
                    case UNDEFINED:
                        viewChange.setValue(ViewType.ERROR_OTA_UNDEFINED.setArgs(args));
                }
            } else {
                viewChange.setValue(ViewType.ERROR_OTA_UNDEFINED.setArgs(args));
            }
            firmwareUpdateProgress.setValue(0.0);
        }

        private void setupServiceConnectionObserver() {
            serviceReadyDisposable = otaManager
                    .isServiceReady()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(serviceConnected -> {
                        if (serviceConnected) {
                            initFirmwareFileMetaDataObserver();
                        }
                    });
        }

        private void initFirmwareFileMetaDataObserver() {
            Observable<Response<FirmwareFileMetaData>> firmwareObservable =
                    otaManager.checkForFirmwareUpdate(deviceId, true);

            if (firmwareObservable != null) {
                checkForFirmwareDisposable = firmwareObservable
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(firmwareFileMetaDataResponse -> {
                            this.firmwareFileMetaData = firmwareFileMetaDataResponse.body();
                            if (firmwareFileMetaDataResponse != null &&
                                    firmwareFileMetaDataResponse.body() != null) {
                                firebaseAnalyticsManager.eventOtaStarted();
                                otaManager.startOTAUpdate(deviceId,
                                        firmwareFileMetaDataResponse.body());
                                initObservers();
                            } else {
                                viewChange.setValue(ViewType.ERROR_OTA_DOWNLOAD);
                            }
                        });
            } else {
                viewChange.setValue(ViewType.ERROR_OTA_DOWNLOAD);
            }
        }

        private void dispose() {
            otaManager.dispose();

            if (firmwareUpdateProgressDisposable != null) {
                firmwareUpdateProgressDisposable.dispose();
            }

            if (serviceReadyDisposable != null) {
                serviceReadyDisposable.dispose();
            }

            if (firmwareUpdateStateDisposable != null) {
                firmwareUpdateStateDisposable.dispose();
            }

            if (checkForFirmwareDisposable != null) {
                checkForFirmwareDisposable.dispose();
            }
        }
    }
}

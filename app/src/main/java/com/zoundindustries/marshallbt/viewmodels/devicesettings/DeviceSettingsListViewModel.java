package com.zoundindustries.marshallbt.viewmodels.devicesettings;

import android.app.Application;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.coupling.DeviceCouplingInfo;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.BaseDevice.ConnectionState;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.devicesettings.DeviceSettingsItem;
import com.zoundindustries.marshallbt.model.ota.OTAManager;
import com.zoundindustries.marshallbt.model.ota.pojo.SpeakerInfoCache;
import com.zoundindustries.marshallbt.services.DeviceService;
import com.zoundindustries.marshallbt.services.DeviceService.DeviceImageType;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.utils.BluetoothSystemUtils;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

public interface DeviceSettingsListViewModel {

    interface Inputs {

        /**
         * OnClick event for entering device settings menu
         *
         * @param settingType defining which type of setting was clicked. This type is used to
         *                    customize behaviour across different settings types.
         */
        void onSettingsItemClicked(@NonNull DeviceSettingsItem.SettingType settingType);

        /**
         * Check the name of the device changed
         */
        void setDeviceNameIfChanged();
    }

    interface Outputs {

        /**
         * Get observable for device name.
         *
         * @return current device name.
         */
        @NonNull
        MutableLiveData<String> getDeviceName();

        /**
         * Get observable for selected device connection state.
         *
         * @return current device connection state.
         */
        @NonNull
        MutableLiveData<ConnectionState> isConnected();

        /**
         * Return boolean indicating if device is already paired in classic BT.
         *
         * @return current device connection state.
         */
        @NonNull
        Boolean isPaired();

        /**
         * Get live data of active state of non configurable cable source.
         * <p>
         * Non configurable cable source (aux, rca) is a source which can't be switched via app.
         * For example - switching to "aux" on some device is only done via inserting the 3.5 mm plug.
         * There is no option to change to bt source when 3.5mm is plugged in.
         *
         * @return active state of non configurable Aux source
         */
        MutableLiveData<Boolean> isNonConfigurableAuxSourceActive();

        /**
         * Get configured, static setting list.
         *
         * @return list of settings items.
         */
        @NonNull
        List<DeviceSettingsItem> getSettingsList();

        /**
         * Get {@link io.reactivex.Observable} for changed view.
         *
         * @return viewType to be started.
         */
        MutableLiveData<ViewType> isViewChanged();

        /**
         * Get coupling state ready. This will notify View that we can start using coupling.
         *
         * @return coupling state.
         */
        MutableLiveData<Boolean> isCouplingStateAvailable();

        @NonNull
        MutableLiveData<Boolean> isUpdateAvailable();

        /**
         * Get the image resource ID for speaker
         *
         * @return observable on resource ID for the speaker image.
         */
        @NonNull
        MutableLiveData<Integer> getSpeakerImageResourceId();
    }

    /**
     * Class providing data to device settings views.
     */
    final class Body extends AndroidViewModel implements Inputs, Outputs {

        private static final int TWS_TIMEOUT_SECONDS = 15;
        private static final int TWS_STATUS_READ_DELAY_MILLIS = 1500;

        public final Inputs inputs = this;
        public final Outputs outputs = this;

        public final MutableLiveData<String> deviceName;
        public final MutableLiveData<ConnectionState> connectionState;
        private final MutableLiveData<ViewType> viewChanged;
        private final MutableLiveData<Boolean> isCouplingStateAvailable;
        private final MutableLiveData<Integer> speakerImageId;
        private final MutableLiveData<Boolean> isUpdateAvailabile;
        private MutableLiveData<Boolean> isNonConfigurableAuxSourceActive;

        private final OTAManager otaManager;

        private boolean isDeviceAlreadyCoupled;
        private final List<DeviceSettingsItem> settingsList;
        private BaseDevice device;
        private final DeviceManager deviceManager;
        private final String deviceId;

        private Disposable serviceReadyDisposable;
        private Disposable connectionDisposable;
        private Disposable twsConnectionDisposable;
        private Disposable updateVisibilityDisposable;
        private Disposable AuxSourceActiveDisposable;

        /**
         * Constructor for Body.
         *
         * @param application context used for Android specific resources access.
         * @param deviceId    for which settings are provided.
         */
        public Body(@NonNull Application application, @NonNull String deviceId) {
            this(application, deviceId,
                    new DeviceManager(application),
                    new OTAManager(application));
        }

        /**
         * Constructor for Body with {@link DeviceManager} parameter.
         * Used for test purpose.
         *
         * @param application   context used for Android specific resources access.
         * @param deviceId      for which settings are provided.
         * @param deviceManager instance to get device specific data from
         *                      the{@link DeviceService}
         */
        @VisibleForTesting
        public Body(@NonNull Application application,
                    @NonNull String deviceId,
                    @NonNull DeviceManager deviceManager,
                    @NonNull OTAManager otaManager) {
            super(application);
            this.deviceId = deviceId;

            settingsList = new ArrayList<>();

            this.deviceManager = deviceManager;
            this.otaManager = otaManager;

            deviceName = new MutableLiveData<>();
            connectionState = new MutableLiveData<>();
            viewChanged = new MutableLiveData<>();
            isCouplingStateAvailable = new MutableLiveData<>();
            speakerImageId = new MutableLiveData<>();
            isUpdateAvailabile = new MutableLiveData<>();
            isNonConfigurableAuxSourceActive = new MutableLiveData<>();

            setupDeviceName(deviceId);
            initLiveData();
        }

        @Override
        public void onSettingsItemClicked(@NonNull DeviceSettingsItem.SettingType settingType) {
            Bundle argBundle = new Bundle();
            argBundle.putString(EXTRA_DEVICE_ID, device.getDeviceInfo().getId());

            switch (settingType) {
                case EQUALISER:
                    viewChanged.setValue(ViewType.EQUALISER.setArgs(argBundle));
                    break;
                case EQ_EXTENDED:
                    viewChanged.setValue(ViewType.EQ_EXTENDED.setArgs(argBundle));
                    break;
                case LIGHT:
                    viewChanged.setValue(ViewType.LIGHT.setArgs(argBundle));
                    break;
                case ABOUT:
                    viewChanged.setValue(ViewType.ABOUT_DEVICE.setArgs(argBundle));
                    break;
                case M_BUTTON:
                    viewChanged.setValue(ViewType.M_BUTTON.setArgs(argBundle));
                    break;
                case RENAME:
                    viewChanged.setValue(ViewType.RENAME.setArgs(argBundle));
                    break;
                case COUPLE:
                    if (isDeviceCoupled(device)) {
                        viewChanged.setValue(ViewType.DECOUPLING_SPINNER);
                        device.getBaseDeviceStateController().inputs
                                .setConnectionInfo(ConnectionState.CONNECTED);
                    } else {
                        startCoupleScreen(argBundle);
                    }
                    break;
                case FORGET:
                    viewChanged.setValue(ViewType.FORGET_DEVICE.setArgs(argBundle));
                    break;
                case SOUNDS:
                    viewChanged.setValue(ViewType.SOUNDS.setArgs(argBundle));
                    break;
                case ANC:
                    viewChanged.setValue(ViewType.ANC.setArgs(argBundle));
                    break;
                case TIMER_OFF:
                    viewChanged.setValue(ViewType.TIMER_OFF.setArgs(argBundle));
                default:
                    break;
            }
        }

        @NonNull
        @Override
        public MutableLiveData<String> getDeviceName() {
            return deviceName;
        }

        @NonNull
        @Override
        public MutableLiveData<ConnectionState> isConnected() {
            return connectionState;
        }

        @NonNull
        @Override
        public Boolean isPaired() {
            return device != null && new BluetoothSystemUtils().isDevicePaired(device);
        }

        @Override
        public MutableLiveData<Boolean> isNonConfigurableAuxSourceActive() {
            return isNonConfigurableAuxSourceActive;
        }

        @NonNull
        @Override
        public List<DeviceSettingsItem> getSettingsList() {
            return settingsList;
        }

        @Override
        public MutableLiveData<ViewType> isViewChanged() {
            return viewChanged;
        }

        @Override
        public MutableLiveData<Boolean> isCouplingStateAvailable() {
            return isCouplingStateAvailable;
        }

        @Nullable
        @Override
        public MutableLiveData<Boolean> isUpdateAvailable() {
            return isUpdateAvailabile;
        }

        @Override
        public void setDeviceNameIfChanged() {
            if (device != null) {
                String newDeviceName = device.getBaseDeviceStateController().outputs
                        .getSpeakerInfoCurrentValue().getDeviceName();

                if (!newDeviceName.equals(deviceName.getValue())) {
                    deviceName.setValue(newDeviceName);
                }
            }
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getSpeakerImageResourceId() {
            return speakerImageId;
        }

        @Override
        protected void onCleared() {
            dispose();
        }

        private void setupDeviceName(String deviceId) {
            serviceReadyDisposable = deviceManager
                    .isServiceReady()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(isReady -> {
                        if (isReady) {
                            device = deviceManager.getDeviceFromId(deviceId);
                            if (device != null) {
                                setupSettingsList();
                                deviceName.setValue(device.getBaseDeviceStateController().outputs
                                        .getSpeakerInfoCurrentValue().getDeviceName());
                                initObservers();
                                speakerImageId.setValue(deviceManager.getImageResourceId(deviceId,
                                        DeviceImageType.MEDIUM));

                                if (!device.getBaseDeviceStateController().outputs
                                        .isAuxConfigurable()) {
                                    setupAuxSourceObservers();
                                }
                            } else {
                                viewChanged.setValue(ViewType.HOME_SCREEN);
                                //todo what to do if device not found? should we close the view?
                            }
                        }
                    });
        }

        private void initLiveData() {
            isCouplingStateAvailable.setValue(false);
        }

        private void initObservers() {
            if (device.getBaseDeviceStateController()
                    .isFeatureSupported(FeaturesDefs.CONNECTION_INFO)) {
                connectionDisposable = device.getBaseDeviceStateController().outputs
                        .getConnectionInfo()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(value -> handleConnectionChanged(value));
            }

            Observable<Boolean> updateAvailableObservable =
                    otaManager.getFirmwareUpdateAvailability(device);

            if (updateAvailableObservable != null) {
                updateVisibilityDisposable = updateAvailableObservable
                        .observeOn(AndroidSchedulers.mainThread())
                        .filter(isVisible -> isVisible != null)
                        .subscribe(isUpdateAvailabile::setValue);
            }
        }

        private void handleConnectionChanged(@NonNull ConnectionState connectionState) {
            if (isDeviceAlreadyCoupled) {
                if (connectionState == ConnectionState.CONNECTED) {
                    device.getBaseDeviceStateController().inputs.readCouplingConnectionStatus();
                    new Handler().postDelayed(() ->
                            initTwsCouplingState(device), TWS_STATUS_READ_DELAY_MILLIS);
                } else if (connectionState == ConnectionState.TIMEOUT ||
                        connectionState == ConnectionState.DISCONNECTED) {
                    viewChanged.setValue(ViewType.HOME_SCREEN);
                }
            } else {
                checkForCoupledState();
                this.connectionState.setValue(connectionState);
            }
        }

        private void initTwsCouplingState(@NonNull BaseDevice device) {
            twsConnectionDisposable = device.getBaseDeviceStateController().outputs
                    .getCouplingConnectionInfo()
                    .observeOn(AndroidSchedulers.mainThread())
                    .timeout(TWS_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                    .subscribe(deviceCouplingInfo -> {
                        handleTwsDisconnection(device, deviceCouplingInfo);
                    }, throwable -> {
                        throwable.printStackTrace();
                        viewChanged.postValue(ViewType.HOME_SCREEN);
                    });
        }

        private void handleTwsDisconnection(@NonNull BaseDevice device, DeviceCouplingInfo deviceCouplingInfo) {
            device.getBaseDeviceStateController().inputs.setCouplingDisconnected();
            clearCouplingCache(device, deviceCouplingInfo);
            BaseDevice peerDevice =
                    deviceManager.getDeviceFromId(deviceCouplingInfo.getPeerMacAddress());

            if (peerDevice != null) {
                peerDevice.getBaseDeviceStateController().inputs
                        .setConnectionInfo(ConnectionState.CONNECTED);
            }

            isDeviceAlreadyCoupled = false;
            viewChanged.setValue(ViewType.HOME_SCREEN);
        }

        private void clearCouplingCache(@NonNull BaseDevice device,
                                        @NonNull DeviceCouplingInfo deviceCouplingInfo) {
            deviceManager.updateSpeakerInfoCache(device.getDeviceInfo().getId(),
                    false,
                    null,
                    null);
            deviceManager.updateSpeakerInfoCache(deviceCouplingInfo
                            .getPeerMacAddress(),
                    false,
                    null,
                    null);
        }

        private void checkForCoupledState() {
            device.getBaseDeviceStateController().inputs.readCouplingConnectionStatus();

            if (device.getBaseDeviceStateController()
                    .isFeatureSupported(FeaturesDefs.COUPLING_CONNECTION)) {
                isDeviceAlreadyCoupled = isDeviceCoupled(device);
                isCouplingStateAvailable.setValue(true);
            }
        }

        private void setupSettingsList() {
            Resources resources = getApplication().getResources();

            if (device.getBaseDeviceStateController().isFeatureSupported(FeaturesDefs.SPEAKER_INFO)) {
                settingsList.add(new DeviceSettingsItem(
                        resources.getString(R.string.appwide_about),
                        DeviceSettingsItem.SettingType.ABOUT));
            }

            if (device.getBaseDeviceStateController().isFeatureSupported(FeaturesDefs.M_BUTTON)) {
                settingsList.add(new DeviceSettingsItem(
                        resources.getString(R.string.device_settings_menu_item_m_button),
                        DeviceSettingsItem.SettingType.M_BUTTON));
            }

            if (device.getBaseDeviceStateController().isFeatureSupported(FeaturesDefs.ANC)) {
                settingsList.add(new DeviceSettingsItem(
                        resources.getString(R.string.device_settings_menu_item_anc_uc),
                        DeviceSettingsItem.SettingType.ANC));
            }

            if (device.getBaseDeviceStateController().isFeatureSupported(FeaturesDefs.TIMER_OFF)) {
                settingsList.add(new DeviceSettingsItem(
                        resources.getString(R.string.device_settings_menu_item_auto_off_timer),
                        DeviceSettingsItem.SettingType.TIMER_OFF));
            }

            if (device.getBaseDeviceStateController().isFeatureSupported(FeaturesDefs.RENAME)) {
                settingsList.add(new DeviceSettingsItem(
                        resources.getString(R.string.device_settings_menu_item_rename),
                        DeviceSettingsItem.SettingType.RENAME));
            }

            if (device.getBaseDeviceStateController()
                    .isFeatureSupported(FeaturesDefs.COUPLING_CONNECTION)) {
                settingsList.add(new DeviceSettingsItem(
                        resources.getString(isDeviceCoupled(device) ?
                                R.string.android_device_settings_menu_item_decouple :
                                R.string.device_settings_menu_item_couple),
                        DeviceSettingsItem.SettingType.COUPLE));
            }

            settingsList.add(new DeviceSettingsItem(
                    getForgetTitle(resources),
                    DeviceSettingsItem.SettingType.FORGET));

            if (device.getBaseDeviceStateController().isFeatureSupported(FeaturesDefs.EQ_EXTENDED)) {
                settingsList.add(new DeviceSettingsItem(
                        resources.getString(R.string.device_settings_menu_item_equaliser),
                        DeviceSettingsItem.SettingType.EQ_EXTENDED));
            } else if (device.getBaseDeviceStateController().isFeatureSupported(FeaturesDefs.EQ)) {
                settingsList.add(new DeviceSettingsItem(
                        resources.getString(R.string.device_settings_menu_item_equaliser),
                        DeviceSettingsItem.SettingType.EQUALISER));
            }

            if (device.getBaseDeviceStateController().isFeatureSupported(FeaturesDefs.LIGHT)) {
                settingsList.add(new DeviceSettingsItem(
                        resources.getString(R.string.device_settings_menu_item_light),
                        DeviceSettingsItem.SettingType.LIGHT));
            }

            if (device.getBaseDeviceStateController().isFeatureSupported(FeaturesDefs.SOUNDS_SETTINGS)) {
                settingsList.add(new DeviceSettingsItem(
                        resources.getString(R.string.device_settings_menu_item_sounds),
                        DeviceSettingsItem.SettingType.SOUNDS));
            }
        }

        private String getForgetTitle(Resources resources) {
            String title = "";
            if (device.getDeviceInfo().getDeviceGroup() == DeviceInfo.DeviceGroup.SPEAKER) {
                title = resources.getString(R.string.device_settings_menu_item_forget);
            } else if (device.getDeviceInfo().getDeviceGroup() == DeviceInfo.DeviceGroup.HEADPHONES) {
                title = resources.getString(R.string.device_settings_menu_item_forget_headphone);
            }
            return title;
        }

        private void setupAuxSourceObservers() {
            if (device.getBaseDeviceStateController().isFeatureSupported(FeaturesDefs.AUX_SOURCE)) {
                AuxSourceActiveDisposable = device.getBaseDeviceStateController().outputs
                        .isSourceActive(BaseDevice.SourceType.AUX)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(isActive ->
                                isNonConfigurableAuxSourceActive.setValue(isActive));
            }
        }

        private boolean isDeviceCoupled(@NonNull BaseDevice device) {
            SpeakerInfoCache speakerInfoCache =
                    deviceManager.getSpeakerInfoCache(device.getDeviceInfo().getId());
            return speakerInfoCache != null && speakerInfoCache.getIsTwsCoupled();
        }

        private void startCoupleScreen(Bundle argBundle) {
            viewChanged.setValue(ViewType.COUPLE_SPEAKERS.setArgs(argBundle));
        }

        private void dispose() {
            if (serviceReadyDisposable != null && !serviceReadyDisposable.isDisposed()) {
                serviceReadyDisposable.dispose();
            }

            if (connectionDisposable != null && !connectionDisposable.isDisposed()) {
                connectionDisposable.dispose();
            }

            if (twsConnectionDisposable != null && !twsConnectionDisposable.isDisposed()) {
                twsConnectionDisposable.dispose();
            }

            if (updateVisibilityDisposable != null && !updateVisibilityDisposable.isDisposed()) {
                updateVisibilityDisposable.dispose();
            }

            if (AuxSourceActiveDisposable != null && !AuxSourceActiveDisposable.isDisposed()) {
                AuxSourceActiveDisposable.dispose();
            }
        }
    }
}

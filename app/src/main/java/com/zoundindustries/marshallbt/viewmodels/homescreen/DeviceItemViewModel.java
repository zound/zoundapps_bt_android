package com.zoundindustries.marshallbt.viewmodels.homescreen;

import android.app.Application;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.lifecycle.MutableLiveData;

import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.coupling.DeviceCouplingInfo;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.BaseDevice.ConnectionState;
import com.zoundindustries.marshallbt.model.device.BaseDevice.CouplingConnectionState;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.device.state.BaseDeviceStateController;
import com.zoundindustries.marshallbt.model.device.state.RowItemState;
import com.zoundindustries.marshallbt.model.discovery.DeviceDiscoveryManager;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.model.homescreen.HomeScreenItemLinkResources;
import com.zoundindustries.marshallbt.model.ota.OTAManager;
import com.zoundindustries.marshallbt.model.ota.pojo.FirmwareFileMetaData;
import com.zoundindustries.marshallbt.model.ota.pojo.SpeakerInfoCache;
import com.zoundindustries.marshallbt.services.DeviceService.DeviceImageType;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.ui.homescreen.DeviceListFragment;
import com.zoundindustries.marshallbt.ui.onboarding.BaseOnboardingContainerFragment;
import com.zoundindustries.marshallbt.utils.BluetoothSystemUtils;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;
import com.zoundindustries.marshallbt.utils.ViewExtrasGlobals;
import com.zoundindustries.marshallbt.viewmodels.autopairing.BaseAutoPairingViewModel;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;
import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_PAIRING_MODE_ENABLED;
import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_PAIRING_MODE_ENABLED_DEVICE_NAME;

public interface DeviceItemViewModel {

    interface Inputs {

        /**
         * Change volume for the device that is bounded to this model
         *
         * @param volume new volume value
         */
        void setVolume(int volume);

        /**
         * Change playing state for the device that is bounded to this model
         *
         * @param playing new volume value
         */
        void setPlaying(boolean playing);

        /**
         * Change has update state for the device that is bounded to this model
         *
         * @param hasUpdate new has update value
         */
        void setUpdate(boolean hasUpdate);

        /**
         * Change playingInfoText state for the device that is bounded to this model
         *
         * @param state connection state
         */
        void setConnectionState(ConnectionState state);

        /**
         * OnClick event for the whole item row on the list.
         *
         * @param view that was clicked.
         */
        void onItemClicked(@NonNull View view);

        /**
         * OnClick event for the connect button.
         *
         * @param view that was clicked
         */
        void onConnectButtonClicked(@NonNull View view);

        /**
         * OnClick event for the overflow device menu.
         *
         * @param view that was clicked.
         */
        void onDeviceMenuClicked(@NonNull View view);

        /**
         * OnClick event for the right link.
         *
         * @param view that was clicked.
         */
        void onAncLinkClicked(@NonNull View view);

        /**
         * OnClick event for the left link.
         *
         * @param view that was clicked.
         */
        void onEqLinkClicked(@NonNull View view);
    }

    interface Outputs {

        /**
         * Get observable for playing info text.
         * Can be the following state:
         * 'Connected'
         * 'Disconnected'
         * 'Playing' - currently playing song
         * 'Connect' - OTA update available for the device.
         *
         * @return observable for playing info text.
         */
        @NonNull
        MutableLiveData<String> getPlayingInfoText();

        /**
         * Get observable for playing info alpha.
         * Value reflects device state (as described in getPlayingInfo method).
         *
         * @return observable for playing info alpha.
         */
        @NonNull
        MutableLiveData<Float> getPlayingInfoAlpha();

        /**
         * Get observable for playing info visibility.
         * Value reflects device state (as described in getPlayingInfo method).
         *
         * @return observable for playing info visibility.
         */
        @NonNull
        MutableLiveData<Integer> getPlayingInfoVisibility();

        /**
         * Get observable for check mark visibility.
         * Value reflects device connection state.
         *
         * @return observable for check mark visibility.
         */
        @NonNull
        MutableLiveData<Integer> getCheckMarkVisibility();

        /**
         * Get observable for battery level visibility.
         *
         * @return observable for battery level visibility.
         */
        @NonNull
        MutableLiveData<Integer> getBatteryLevelVisibility();

        /**
         * Get observable for check mark visibility.
         * Value reflects device connection state.
         *
         * @return observable for check mark visibility.
         */
        @NonNull
        MutableLiveData<Integer> getDividerVisibility();

        /**
         * Get observable for ota dot indicator visibility.
         * Value reflects device connection state.
         *
         * @return observable for ota indicator visibility.
         */
        @NonNull
        MutableLiveData<Integer> getOtaIndicatorVisibility();

        /**
         * Get observable for device name. Can be changed by the user.
         *
         * @return current device name.
         */
        @NonNull
        MutableLiveData<String> getDeviceName();

        /**
         * Get observable for device availability.
         * This state indicates that device has been discovered and is available for connection.
         *
         * @return observable - value is true if device is ready to be connected.
         */
        @NonNull
        MutableLiveData<Boolean> isDeviceReadyForConnection();

        /**
         * Get observable for current volume value for the device.
         *
         * @return current volume value observable.
         */
        @NonNull
        MutableLiveData<Integer> getVolume();

        /**
         * Get observable for current battery level value.
         *
         * @return current battery level value observable
         */
        @NonNull
        MutableLiveData<String> getBatteryValue();

        /**
         * Get observable for metadata of ota firmware file.
         *
         * @return current metadata for ota file observable
         */
        @NonNull
        MutableLiveData<FirmwareFileMetaData> getOtaFileMetaData();

        /**
         * Get device object composed with this errorViewModel.
         *
         * @return device object.
         */
        @NonNull
        BaseDevice getDevice();

        /**
         * Get the image resource ID for speaker
         *
         * @return observable on resource ID for the speaker image.
         */
        @NonNull
        MutableLiveData<Integer> getSpeakerImageResourceId();

        /**
         * Get observable for current battery level image.
         *
         * @return current battery level image observable
         */
        @NonNull
        MutableLiveData<Integer> getBatteryValueImage();

        /**
         * Get observable for right link data.
         *
         * @return right link data observable
         */
        @NonNull
        MutableLiveData<HomeScreenItemLinkResources> getAncLinkData();

        /**
         * Get observable for left link data.
         *
         * @return left link data observable
         */
        @NonNull
        MutableLiveData<HomeScreenItemLinkResources> getEqLinkData();

        /**
         * Get observable for background color.
         *
         * @return background color observable
         */
        @NonNull
        MutableLiveData<Integer> getItemBackground();

        /**
         * Get {@link io.reactivex.Observable} for changed view.
         *
         * @return viewType to be started.
         */
        @NonNull
        MutableLiveData<ViewType> isViewChanged();

        /**
         * Get live data of active state of non configurable cable source.
         * <p>
         * Non configurable cable source (aux, rca) is a source which can't be switched via app.
         * For example - switching to "aux" on some device is only done via inserting the 3.5 mm plug.
         * There is no option to change to bt source when 3.5mm is plugged in.
         *
         * @return active state of non configurable cable source
         */
        MutableLiveData<Boolean> isNonConfigurableAuxSourceActive();

        /**
         * Get base fragment needed for onboarding flow
         * @return base onboarding tab
         */
        BaseOnboardingContainerFragment getOnboardingFragment();
    }


    /**
     * Class providing data to every item in the main devices list in {@link DeviceListFragment}.
     */
    final class Body extends BaseAutoPairingViewModel implements Inputs, Outputs {

        public final Inputs inputs = this;
        public final Outputs outputs = this;

        public static final int STATE_ANIMATION_DELAY = 100;

        private final int COUPLING_INFO_SUBSCRIPTION_DELAY_IN_MS = 1500;
        private final int BONDING_DELAY_IN_MS = 1000;

        private final MutableLiveData<String> deviceName;
        private final MutableLiveData<Float> playingInfoAlpha;
        private final MutableLiveData<String> playingInfoText;
        private final MutableLiveData<Integer> checkMarkVisibility;
        private final MutableLiveData<Integer> batteryLevelVisibility;
        private final MutableLiveData<Integer> linksVisibility;
        private final MutableLiveData<Integer> playingInfoVisibility;
        private final MutableLiveData<Integer> dividerVisibility;
        private final MutableLiveData<Boolean> readyToConnectVisibility;
        private final MutableLiveData<ViewType> viewChanged;
        private final MutableLiveData<Integer> volume;
        private final MutableLiveData<String> batteryLevelValue;
        private final MutableLiveData<FirmwareFileMetaData> otaFileMetaData;
        private final MutableLiveData<Integer> speakerImageId;
        private final MutableLiveData<Integer> batteryImageId;
        private final MutableLiveData<Integer> otaIndicatorVisibility;
        private final MutableLiveData<HomeScreenItemLinkResources> ancLink;
        private final MutableLiveData<HomeScreenItemLinkResources> eqLink;
        private final MutableLiveData<Integer> backgroundColor;
        private final MutableLiveData<Boolean> isNonConfigurableAuxSourceActive;

        private boolean pairingStarted;
        private final BaseDevice device;
        private final OTAManager otaManager;
        private final Handler handler;

        private final DeviceDiscoveryManager deviceDiscoveryManager;
        private final FirebaseAnalyticsManager firebaseAnalyticsManager;

        private RowItemState rowItemState;
        private BluetoothSystemUtils bluetoothSystemUtils;

        private Disposable volumeDisposable;
        private Disposable batteryLevelDisposable;
        private Disposable stateDisposable;
        private Disposable checkForUpdateDisposable;
        private Disposable serviceReadyDisposable;
        private Disposable dividerIdDisposable;
        private Disposable speakerInfoDisposable;
        private Disposable twsConnectionDisposable;
        private Disposable updateAvailabilityDisposable;
        private Disposable AuxSourceActiveDisposable;

        /**
         * Constructor for Body.
         *
         * @param application needed to get resources for strings.
         * @param device      device that reference the view for this errorViewModel instance.
         */
        public Body(@NonNull Application application,
                    @NonNull BaseDevice device) {
            this(application, device,
                    new BluetoothSystemUtils(),
                    new DeviceManager(application),
                    new DeviceDiscoveryManager(application),
                    new OTAManager(application),
                    ((BluetoothApplication) application).getAppComponent().firebaseWrapper(),
                    new Handler());
        }

        /**
         * Constructor for Body with injections for tests.
         *
         * @param application          needed to get resources for strings.
         * @param device               device that reference the view for this errorViewModel instance.
         * @param bluetoothSystemUtils to get list of paired devices.
         */
        @VisibleForTesting
        public Body(@NonNull Application application,
                    @NonNull BaseDevice device,
                    @NonNull BluetoothSystemUtils bluetoothSystemUtils,
                    @NonNull DeviceManager deviceManager,
                    @NonNull DeviceDiscoveryManager deviceDiscoveryManager,
                    @NonNull OTAManager otaManager,
                    @NonNull FirebaseAnalyticsManager firebaseAnalyticsManager,
                    @NonNull Handler handler) {
            super(application, deviceManager, device.getDeviceInfo().getId());

            this.device = device;
            this.deviceDiscoveryManager = deviceDiscoveryManager;
            this.bluetoothSystemUtils = bluetoothSystemUtils;
            this.otaManager = otaManager;
            this.firebaseAnalyticsManager = firebaseAnalyticsManager;
            this.handler = handler;

            viewChanged = new MutableLiveData<>();
            playingInfoText = new MutableLiveData<>();
            playingInfoAlpha = new MutableLiveData<>();
            playingInfoVisibility = new MutableLiveData<>();
            checkMarkVisibility = new MutableLiveData<>();
            batteryLevelVisibility = new MutableLiveData<>();
            linksVisibility = new MutableLiveData<>();
            dividerVisibility = new MutableLiveData<>();
            deviceName = new MutableLiveData<>();
            readyToConnectVisibility = new MutableLiveData<>();
            volume = new MutableLiveData<>();
            batteryLevelValue = new MutableLiveData<>();
            otaFileMetaData = new MutableLiveData<>();
            speakerImageId = new MutableLiveData<>();
            batteryImageId = new MutableLiveData<>();
            otaIndicatorVisibility = new MutableLiveData<>();
            ancLink = new MutableLiveData<>();
            eqLink = new MutableLiveData<>();
            backgroundColor = new MutableLiveData<Integer>();
            isNonConfigurableAuxSourceActive = new MutableLiveData<>();

            initDefaultLiveDataStates();
            rowItemState = new RowItemState.RowItemStateBuilder().build();

            deviceName.setValue(device.getBaseDeviceStateController().outputs.
                    getSpeakerInfoCurrentValue().getDeviceName());
            checkForPairingState();
            initDeviceServiceObserver();
        }

        @Override
        public void setVolume(int volume) {
            device.getBaseDeviceStateController().inputs.setVolume(volume);
            firebaseAnalyticsManager.eventAppVolumeChanged(volume);
        }

        @Override
        public void setPlaying(boolean playing) {
            device.getBaseDeviceStateController().inputs.setPlaying(playing);
        }

        @Override
        public void setUpdate(boolean hasUpdate) {
            device.getBaseDeviceStateController().inputs.setHasUpdate(hasUpdate);
        }

        @Override
        public void setConnectionState(ConnectionState state) {
            device.getBaseDeviceStateController().inputs.setConnectionInfo(state);
        }

        @Override
        public void onItemClicked(@NonNull View view) {

            if (rowItemState.getConnectionState() == ConnectionState.CONNECTED &&
                    deviceManager.isSpeakerInfoCached(device.getDeviceInfo().getId())) {
                viewChanged.setValue(ViewType.SOURCES.setArgs(getDeviceIdBundle()));
            }
        }

        @Override
        public void onConnectButtonClicked(@NonNull View view) {
            if (rowItemState.getConnectionState() == ConnectionState.READY_TO_CONNECT ||
                    (!bluetoothSystemUtils.isDevicePaired(device) &&
                            rowItemState.getConnectionState() != ConnectionState.DISCONNECTED &&
                            rowItemState.getConnectionState() != ConnectionState.TIMEOUT)) {
                deviceDiscoveryManager.stopScanForDevices();
                device.getBaseDeviceStateController().inputs
                        .setConnectionInfo(ConnectionState.CONNECTED);
            }
        }

        @Override
        public void onDeviceMenuClicked(@NonNull View view) {
            if (deviceManager.isSpeakerInfoCached(device.getDeviceInfo().getId())) {
                viewChanged.setValue(ViewType.DEVICE_SETTINGS.setArgs(getDeviceIdBundle()));
            }
        }

        @Override
        public void onAncLinkClicked(@NonNull View view) {
            viewChanged.setValue(ViewType.ANC.setArgs(getDeviceIdBundle()));
        }

        @Override
        public void onEqLinkClicked(@NonNull View view) {
            viewChanged.setValue(ViewType.EQ_EXTENDED.setArgs(getDeviceIdBundle()));
        }

        @NonNull
        private Bundle getDeviceIdBundle() {
            Bundle args = new Bundle();
            args.putString(ViewExtrasGlobals.EXTRA_DEVICE_ID, device.getDeviceInfo().getId());
            return args;
        }

        @NonNull
        @Override
        public MutableLiveData<String> getPlayingInfoText() {
            return playingInfoText;
        }

        @NonNull
        @Override
        public MutableLiveData<Float> getPlayingInfoAlpha() {
            return playingInfoAlpha;
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getPlayingInfoVisibility() {
            return playingInfoVisibility;
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getCheckMarkVisibility() {
            return checkMarkVisibility;
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getDividerVisibility() {
            return dividerVisibility;
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getBatteryLevelVisibility() {
            return batteryLevelVisibility;
        }

        @NonNull
        @Override
        public MutableLiveData<String> getDeviceName() {
            return deviceName;
        }

        @NonNull
        @Override
        public MutableLiveData<Boolean> isDeviceReadyForConnection() {
            return readyToConnectVisibility;
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getVolume() {
            return volume;
        }

        @NonNull
        @Override
        public BaseDevice getDevice() {
            return device;
        }

        @NonNull
        @Override
        public MutableLiveData<FirmwareFileMetaData> getOtaFileMetaData() {
            // FIXME how can we say it's nonNull if we return null metaData if something goes
            // FIXME wrong during checking for update?
            return otaFileMetaData;
        }

        @NonNull
        @Override
        public MutableLiveData<String> getBatteryValue() {
            return batteryLevelValue;
        }

        @NonNull
        @Override
        public MutableLiveData<ViewType> isViewChanged() {
            return viewChanged;
        }

        @Override
        public MutableLiveData<Boolean> isNonConfigurableAuxSourceActive() {
            return isNonConfigurableAuxSourceActive;
        }

        @Override
        public BaseOnboardingContainerFragment getOnboardingFragment() {
            return deviceManager.getOnboardingFragment(device.getDeviceInfo().getId());
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getSpeakerImageResourceId() {
            return speakerImageId;
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getBatteryValueImage() {
            return batteryImageId;
        }

        @NonNull
        @Override
        public MutableLiveData<HomeScreenItemLinkResources> getAncLinkData() {
            return ancLink;
        }

        @NonNull
        @Override
        public MutableLiveData<HomeScreenItemLinkResources> getEqLinkData() {
            return eqLink;
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getItemBackground() {
            return backgroundColor;
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getOtaIndicatorVisibility() {
            return otaIndicatorVisibility;
        }

        @Override
        public void onPairingModeDisabled() {
            handlePairingModeState(false);
        }

        @Override
        protected void onPairingModeEnabled() {
            handlePairingModeState(true);
        }

        @Override
        public void onPairingFailed() {
            device.getBaseDeviceStateController().inputs
                    .setConnectionInfo(ConnectionState.DISCONNECTED);
        }

        @Override
        public void onPairingDone() {

        }

        @Override
        protected void onCleared() {
            dispose();
        }

        private void initDefaultLiveDataStates() {
            playingInfoText.setValue(getApplication().getString(R.string.speaker_status_not_connected));
            playingInfoAlpha.setValue(0.5f);
            playingInfoVisibility.setValue(View.INVISIBLE);
            checkMarkVisibility.setValue(View.GONE);
            batteryLevelVisibility.setValue(View.GONE);
            dividerVisibility.setValue(View.GONE);
            readyToConnectVisibility.setValue(false);
            volume.setValue(0);
            viewChanged.setValue(ViewType.HOME_SCREEN);
            otaIndicatorVisibility.setValue(View.GONE);
            eqLink.setValue(new HomeScreenItemLinkResources(R.drawable.ic_eq,
                    getApplication().getResources().getString(R.string.speaker_list_item_EQ_link_uc)));
            ancLink.setValue(new HomeScreenItemLinkResources(R.drawable.ic_anc,
                    getApplication().getResources().getString(R.string.speaker_list_item_ANC_link_uc)));
            backgroundColor.setValue(getApplication().getResources().getColor(R.color.colorPrimary));
        }

        private void initObservers() {

            // if we can't connect, we can't really do anything
            if (!deviceSupports(FeaturesDefs.CONNECTION_INFO)) {
                playingInfoVisibility.setValue(View.INVISIBLE);
                readyToConnectVisibility.setValue(false);
                return;
            }

            int deviceStateHolder = FeaturesDefs.CONNECTION_INFO;

            Observable<ConnectionState> connectionStateObservable = device
                    .getBaseDeviceStateController()
                    .outputs.getConnectionInfo().distinctUntilChanged();

            Observable<Boolean> isPlayingObservable = null;

            if (deviceSupports(FeaturesDefs.PLAY_CONTROL)) {
                isPlayingObservable = device.getBaseDeviceStateController().outputs.getPlaying().distinctUntilChanged();
                deviceStateHolder |= FeaturesDefs.PLAY_CONTROL;
            }

            switch (deviceStateHolder) {
                case FeaturesDefs.CONNECTION_INFO |
                        FeaturesDefs.PLAY_CONTROL:
                    stateDisposable = Observable.combineLatest(
                            isPlayingObservable,
                            connectionStateObservable,
                            (isPlaying, connectionState) -> new RowItemState.RowItemStateBuilder()
                                    .isPlaying(isPlaying)
                                    .connectionState(connectionState)
                                    .build())
                            .observeOn(AndroidSchedulers.mainThread())
                            .distinctUntilChanged()
                            .subscribe(rowItemState1 -> refreshStates(rowItemState1));
                    break;
                case FeaturesDefs.CONNECTION_INFO:
                    stateDisposable = connectionStateObservable
                            .observeOn(AndroidSchedulers.mainThread())
                            .distinctUntilChanged()
                            .subscribe(connectionState ->
                                    refreshStates(new RowItemState.RowItemStateBuilder()
                                            .isPlaying(false)
                                            .connectionState(connectionState)
                                            .build()));
                    return;
            }

            initVolumeFeature();
            initBatteryObserver();

            Observable<Boolean> updateAvailableObservable =
                    otaManager.getFirmwareUpdateAvailability(device);

            // could this be null?
            if (updateAvailableObservable != null) {
                updateAvailabilityDisposable = updateAvailableObservable
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(isVisible ->
                                otaIndicatorVisibility
                                        .setValue(isVisible ? View.VISIBLE : View.GONE));
            }
        }

        private void handlePairingModeState(boolean isEnabled) {
            Bundle argBundle = new Bundle();
            argBundle.putString(EXTRA_DEVICE_ID, device.getDeviceInfo().getId());
            argBundle.putBoolean(EXTRA_PAIRING_MODE_ENABLED, isEnabled);
            argBundle.putString(EXTRA_PAIRING_MODE_ENABLED_DEVICE_NAME,
                    device.getBaseDeviceStateController().outputs.getSpeakerInfoCurrentValue()
                            .getDeviceName());

            viewChanged.setValue(ViewType.ENABLE_PAIRING.setArgs(argBundle));
            viewChanged.setValue(ViewType.UNKNOWN);
            device.getBaseDeviceStateController().inputs
                    .setConnectionInfo(ConnectionState.SETTING_PAIRING_MODE);
            pairingStarted = false;
        }

        private void checkForPairingState() {
            if (device.getBaseDeviceStateController().outputs
                    .getConnectionInfoCurrentValue() == ConnectionState.SETTING_PAIRING_MODE) {
                device.getBaseDeviceStateController().inputs
                        .setConnectionInfo(ConnectionState.DISCONNECTED);
            }
        }

        private void initDeviceServiceObserver() {
            serviceReadyDisposable = deviceManager.isServiceReady()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(serviceConnected -> {
                        if (serviceConnected) {
                            initObservers();
                            initSpeakerInfoObserver();

                            if (!device.getBaseDeviceStateController().outputs.isAuxConfigurable()) {
                                initAuxSourceObservers();
                            }
                            //initDividerDeviceIdObserver();
                            speakerImageId.setValue(deviceManager
                                    .getImageResourceId(device.getDeviceInfo().getId(),
                                            DeviceImageType.SMALL));
                        }
                    });
        }

        private void initSpeakerInfoObserver() {
            speakerInfoDisposable = device.getBaseDeviceStateController().outputs
                    .getSpeakerInfo()
                    .filter(speakerInfo -> !TextUtils.isEmpty(speakerInfo.getFirmwareVersion()))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(speakerInfo ->
                            deviceName.setValue(speakerInfo.getDeviceName()));
        }

        private void initDividerDeviceIdObserver() {
            dividerIdDisposable = deviceManager.getDividerDeviceId()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::handleDividerPositionChange);
        }

        private void initVolumeFeature() {
            if (device.getBaseDeviceStateController()
                    .isFeatureSupported(FeaturesDefs.VOLUME)) {
                volumeDisposable = device.getBaseDeviceStateController().outputs.getVolume()
                        .observeOn(AndroidSchedulers.mainThread())
                        .distinctUntilChanged()
                        .subscribe(volume::setValue);
            } else {
                //TODO Is it even possible to have only PLAY_CONTROL feature without VOLUME?
                //TODO If yes, should we have 0 progress or hidden the bar? Setting 0 for now.
                volume.setValue(0);
            }
        }

        private void initBatteryObserver() {
            if (device.getBaseDeviceStateController()
                    .isFeatureSupported(FeaturesDefs.BATTERY_LEVEL)) {
                batteryLevelDisposable = device.getBaseDeviceStateController().outputs.getBatteryLevel()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(value -> {
                                    batteryLevelValue.setValue(value.toString() + "%");
                                    batteryImageId.setValue(getBatteryImageId(value));
                                }
                        );
            }
        }

        private int getBatteryImageId(int value) {
            int ret = 0;
            if (value >= 0 && value <= 10) {
                ret = R.drawable.ic_battery10;
            } else if (value > 10 && value <= 20) {
                ret = R.drawable.ic_battery20;
            } else if (value > 20 && value <= 30) {
                ret = R.drawable.ic_battery30;
            } else if (value > 30 && value <= 40) {
                ret = R.drawable.ic_battery40;
            } else if (value > 40 && value <= 50) {
                ret = R.drawable.ic_battery50;
            } else if (value > 50 && value <= 60) {
                ret = R.drawable.ic_battery60;
            } else if (value > 60 && value <= 70) {
                ret = R.drawable.ic_battery70;
            } else if (value > 70 && value <= 80) {
                ret = R.drawable.ic_battery80;
            } else if (value > 80 && value <= 90) {
                ret = R.drawable.ic_battery90;
            } else if (value > 90 && value <= 100) {
                ret = R.drawable.ic_battery100;
            }
            return ret;
        }

        private void handleDividerPositionChange(String deviceId) {
            dividerVisibility.setValue(deviceId.equals(device.getDeviceInfo().getId()) ?
                    View.VISIBLE : View.GONE);
        }

        private boolean deviceSupports(int feature) {
            return device.getBaseDeviceStateController().isFeatureSupported(feature);
        }

        private Boolean refreshStates(RowItemState rowItemState) {
            updatePlayingViewState(rowItemState.isPlaying(), rowItemState.getConnectionState());

            this.rowItemState.setConnectionState(rowItemState.getConnectionState());
            this.rowItemState.setPlaying(rowItemState.isPlaying());

            // Return true for the combined observable. This means that we have received all
            // contained sub observables.
            return true;
        }

        private void updatePlayingViewState(boolean isPlaying, ConnectionState connectionState) {
            if (pairingStarted) return;

            switch (connectionState) {
                case TWS_CONNECTED:
                    setRowStateLiveData(R.string.speaker_status_coupled_uc,
                            1.0f,
                            View.INVISIBLE,
                            View.GONE,
                            device.getBaseDeviceStateController().isFeatureSupported(FeaturesDefs.BATTERY_LEVEL) ? View.VISIBLE : View.GONE,
                            View.GONE,
                            false,
                            getApplication().getResources().getColor(R.color.colorPrimary));
                    break;
                case CONNECTING:
                    setRowStateLiveData(R.string.speaker_status_connecting,
                            1.0f,
                            View.INVISIBLE,
                            View.GONE,
                            View.GONE,
                            View.VISIBLE,
                            false,
                            getApplication().getResources().getColor(R.color.colorPrimary));
                    break;
                case CONNECTED:
                    if (rowItemState.getConnectionState() != ConnectionState.CONNECTED) {
                        if (bluetoothSystemUtils.isDevicePaired(device)) {
                            configurePairedDevice();
                        } else {
                            if (device.getBaseDeviceStateController()
                                    .isFeatureSupported(FeaturesDefs.AUTO_CONNECT)) {
                                pairingStarted = true;
                                configureAutoConnectDevice();
                                return;
                            } else {
                                forcePairingModeAndRedirectToSettings();
                            }
                        }
                    }

                    setRowStateLiveData(getRowStateDescription(isPlaying),
                            1.0f,
                            isPlaying ? View.VISIBLE : View.INVISIBLE,
                            isPlaying ? View.GONE : View.VISIBLE,
                            device.getBaseDeviceStateController()
                                    .isFeatureSupported(FeaturesDefs.BATTERY_LEVEL) ?
                                    View.VISIBLE :
                                    View.GONE,
                            View.GONE,
                            false,
                            getApplication().getResources().getColor(R.color.home_list_item_background));
                    break;
                case READY_TO_CONNECT:
                    setRowStateLiveData(R.string.appwide_connect_uc,
                            1.0f,
                            View.INVISIBLE,
                            View.GONE,
                            View.GONE,
                            View.GONE,
                            true,
                            getApplication().getResources().getColor(R.color.colorPrimary));
                    break;
                case DISCONNECTED:
                case TIMEOUT:
                    setRowStateLiveData(R.string.speaker_status_not_connected,
                            0.5f,
                            View.INVISIBLE,
                            View.GONE,
                            View.GONE,
                            View.GONE,
                            false,
                            getApplication().getResources().getColor(R.color.colorPrimary));
                    break;
            }
        }

        private int getRowStateDescription(boolean isPlaying) {
            Boolean isNonConfigurableAuxActive = isNonConfigurableAuxSourceActive.getValue();

            if (isNonConfigurableAuxActive != null && isNonConfigurableAuxActive) {
                return R.string.speaker_status_aux;
            } else {
                return isPlaying ? R.string.speaker_status_playing :
                        R.string.speaker_status_connected;
            }
        }

        private void forcePairingModeAndRedirectToSettings() {
            device.getBaseDeviceStateController().inputs.startPairingMode();
            viewChanged.setValue(ViewType.GLOBAL_SETTINGS_BLUETOOTH);
        }

        private void configureAutoConnectDevice() {
            setRowStateLiveData(R.string.speaker_status_connecting,
                    1.0f,
                    View.INVISIBLE,
                    View.GONE,
                    View.GONE,
                    View.VISIBLE,
                    false,
                    getApplication().getResources().getColor(R.color.colorPrimary));
            initEnablePairingModeObserver();
        }

        private void configurePairedDevice() {
            if (device.getBaseDeviceStateController()
                    .isFeatureSupported(FeaturesDefs.COUPLING_CONNECTION)) {
                if (!isDeviceCoupled(device)) {
                    initTwsObserver();
                }
            }

            if (device.getBaseDeviceStateController()
                    .isFeatureSupported(FeaturesDefs.BATTERY_LEVEL)) {
                device.getBaseDeviceStateController().inputs.readBatteryLevel();
            }

            SpeakerInfoCache infoCache =
                    deviceManager.getSpeakerInfoCache(device.getDeviceInfo().getId());

            if (device.getBaseDeviceStateController().isFeatureSupported(FeaturesDefs.PLAY_CONTROL) &&
                    (infoCache == null || !infoCache.isConfigured())) {
                deviceManager.setDeviceConfigured(device.getDeviceInfo().getId());
                viewChanged.setValue(ViewType.ONBOARDING_GUIDE.setArgs(getDeviceIdBundle()));
            }
        }

        private void initTwsObserver() {
            twsConnectionDisposable = device.getBaseDeviceStateController().outputs
                    .getCouplingConnectionInfo()
                    .take(1)
                    .observeOn(AndroidSchedulers.mainThread())
                    .delaySubscription(COUPLING_INFO_SUBSCRIPTION_DELAY_IN_MS, TimeUnit.MILLISECONDS)
                    .subscribe(deviceCouplingInfo -> {
                        if (deviceCouplingInfo.getCouplingConnectionInfoState() !=
                                CouplingConnectionState.NOT_COUPLED) {
                            updateTwsConnectedState(deviceCouplingInfo);
                        } else {
                            clearTwsConnectedState();
                        }
                    });
        }

        private void updateTwsConnectedState(@NonNull DeviceCouplingInfo deviceCouplingInfo) {
            deviceManager.saveSpeakerInfoCache(device.getDeviceInfo().getId());
            deviceManager.updateSpeakerInfoCache(device.getDeviceInfo().getId(),
                    true,
                    deviceCouplingInfo.getChannelType().toString(),
                    deviceCouplingInfo.getCouplingConnectionInfoState()
                            .toString());
            device.getBaseDeviceStateController().inputs
                    .setConnectionInfo(ConnectionState.TWS_CONNECTED);
        }

        private void clearTwsConnectedState() {
            String deviceId = device.getDeviceInfo().getId();
            if (deviceManager.isSpeakerInfoCached(deviceId)) {
                deviceManager.saveSpeakerInfoCache(deviceId);
                deviceManager.updateSpeakerInfoCache(deviceId,
                        false,
                        null,
                        null);
                device.getBaseDeviceStateController().inputs
                        .setConnectionInfo(ConnectionState.CONNECTED);
            }
        }

        private void setRowStateLiveData(int playingInfoTextIdValue,
                                         float playingInfoAlphaValue,
                                         int playingInfoVisibilityValue,
                                         int checkMarkVisibilityValue,
                                         int batteryLevelVisibilityValue,
                                         int connectingVisibilityValue,
                                         boolean isReadyForConnectionValue,
                                         int backgroundColorValue) {
            playingInfoAlpha.setValue(playingInfoAlphaValue);
            playingInfoVisibility.setValue(playingInfoVisibilityValue);
            checkMarkVisibility.setValue(checkMarkVisibilityValue);
            handler.postDelayed(() -> playingInfoText.setValue(getApplication()
                            .getString(playingInfoTextIdValue)),
                    STATE_ANIMATION_DELAY);

            batteryLevelVisibility.setValue(batteryLevelVisibilityValue);
            readyToConnectVisibility.setValue(isReadyForConnectionValue);
            backgroundColor.setValue(backgroundColorValue);
        }

        private boolean isDeviceCoupled(@NonNull BaseDevice device) {
            SpeakerInfoCache speakerInfoCache =
                    deviceManager.getSpeakerInfoCache(device.getDeviceInfo().getId());
            return speakerInfoCache != null && speakerInfoCache.getIsTwsCoupled();
        }

        private void initAuxSourceObservers() {
            // todo remove this check, it's unnecessary
            if (isAuxSupportedOnDevice()) {
                AuxSourceActiveDisposable = device.getBaseDeviceStateController().outputs
                        .isSourceActive(BaseDevice.SourceType.AUX)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .subscribe(this::handleNonConfigurableAuxSourceActive);
            }
        }

        private boolean isAuxSupportedOnDevice() {
            BaseDeviceStateController stateController = device.getBaseDeviceStateController();
            return stateController.isFeatureSupported(FeaturesDefs.AUX_SOURCE);
        }

        private void handleNonConfigurableAuxSourceActive(Boolean isActive) {
            RowItemState state = rowItemState;
            isNonConfigurableAuxSourceActive.setValue(isActive);
            refreshStates(state);
        }

        public void dispose() {
            handler.removeCallbacksAndMessages(null);

            if (volumeDisposable != null) {
                volumeDisposable.dispose();
            }

            if (batteryLevelDisposable != null) {
                batteryLevelDisposable.dispose();
            }

            if (stateDisposable != null) {
                stateDisposable.dispose();
            }

            if (serviceReadyDisposable != null) {
                serviceReadyDisposable.dispose();
            }

            if (checkForUpdateDisposable != null) {
                checkForUpdateDisposable.dispose();
            }

            if (dividerIdDisposable != null) {
                dividerIdDisposable.dispose();
            }

            if (speakerInfoDisposable != null) {
                speakerInfoDisposable.dispose();
            }

            if (twsConnectionDisposable != null) {
                twsConnectionDisposable.dispose();
            }

            if (updateAvailabilityDisposable != null) {
                updateAvailabilityDisposable.dispose();
            }

            if (AuxSourceActiveDisposable != null && AuxSourceActiveDisposable.isDisposed()) {
                AuxSourceActiveDisposable.dispose();
            }
        }
    }
}
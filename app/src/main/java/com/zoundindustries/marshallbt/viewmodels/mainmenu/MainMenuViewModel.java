package com.zoundindustries.marshallbt.viewmodels.mainmenu;

import android.app.Application;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.VisibleForTesting;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.zoundindustries.marshallbt.BuildConfig;
import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice.DeviceSubType;
import com.zoundindustries.marshallbt.model.mainmenu.MainMenuItem;
import com.zoundindustries.marshallbt.model.mainmenu.MainMenuItemManager;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.ui.adapters.MainMenuAdapter;
import com.zoundindustries.marshallbt.utils.ViewExtrasGlobals;

import java.util.List;

import io.reactivex.annotations.NonNull;

/**
 * ViewModel responsible for displaying list of available options for each of MainMenu item
 * For displaying correct list viewModel is using mainMenuItemManager ({@link MainMenuItemManager}
 * Items are using {@link MainMenuAdapter}
 * for display.
 * Communication between Adapter and ViewModel are based on {@link Inputs#onMainMenuItemClicked(MainMenuItem.MenuItemType, boolean)}
 */
public interface MainMenuViewModel {

    String QUICK_GUIDE_LAYOUT_TYPE = "QUICK_GUIDE_LAYOUT_TYPE";

    /**
     * Enums used for passing as arguments to fragment to display correct type of layout.
     */

    enum QuickGuideFragmentLayoutType {
        BLUETOOTH_PAIRING,
        COUPLE_SPEAKERS,
        PLAY_PAUSE_BUTTON
    }

    /**
     * Enum defining main menu animation types
     */
    enum MainMenuAnmationType {
        RIGHT_IN,
        LEFT_IN
    }

    interface Inputs {

        /**
         * Represents single click on item from list created in {@link MainMenuItemManager}
         * that was passed to {@link MainMenuAdapter}
         *
         * @param itemType    type of item from enum
         * @param isBackPress flag indicating if the click should act as a result of back press.
         *                    Animation should be reversed for back press.
         */
        void onMainMenuItemClicked(@NonNull MainMenuItem.MenuItemType itemType, boolean isBackPress);
    }

    interface Outputs {

        /**
         * @return text that contains information about version of app
         */
        @NonNull
        MutableLiveData<String> getAppVersionText();

        /**
         * @return list of items available in current menu (or sub-menu)
         * from {@link MainMenuItemManager}
         */
        @NonNull
        MutableLiveData<List<MainMenuItem>> getMainMenuItemList();

        /**
         * Get {@link io.reactivex.Observable} for changed view.
         *
         * @return MainMenuFragmentviewType to be started.
         */
        @NonNull
        MutableLiveData<ViewType> isViewChanged();

        /**
         * @return string resource ID which is displayed in Toolbar
         */
        @NonNull
        MutableLiveData<Integer> getToolbarDisplayTextResourceId();

        MainMenuItem.MenuItemType getCurrentViewType();

    }

    final class Body extends AndroidViewModel implements Outputs, Inputs {

        private final MutableLiveData<String> appVersionText;
        private final MutableLiveData<ViewType> viewChanged;
        private final MainMenuItemManager mainMenuItemManager;

        private MutableLiveData<List<MainMenuItem>> mainMenuItemList;
        private MutableLiveData<Integer> toolbarDisplayTextResourceId;
        private MainMenuItem.MenuItemType currentViewType = MainMenuItem.MenuItemType.ROOT;

        public final Inputs inputs = this;
        public final Outputs outputs = this;
        private final ViewType mainMenuFragmentViewType;

        public Body(@NonNull Application application, @NonNull ViewType MainMenuFragmentViewType) {
            this(application, MainMenuFragmentViewType, application.getResources());
        }

        @VisibleForTesting
        public Body(@NonNull Application application,
                    @NonNull ViewType MainMenuFragmentViewType,
                    @NonNull Resources resources) {
            super(application);
            this.mainMenuFragmentViewType = MainMenuFragmentViewType;
            appVersionText = new MutableLiveData<>();
            viewChanged = new MutableLiveData<>();
            mainMenuItemList = new MutableLiveData<>();
            toolbarDisplayTextResourceId = new MutableLiveData<>();

            mainMenuItemManager = new MainMenuItemManager(resources);
            initStates(resources);
        }

        @Override
        public void onMainMenuItemClicked(@NonNull MainMenuItem.MenuItemType itemType,
                                          boolean isBackPress) {
            Bundle bundle = new Bundle();
            currentViewType = itemType;
            switch (itemType) {
                case EMAIL_SUBSCRIPTION:
                    viewChanged.setValue(ViewType.MAIN_MENU_EMAIL_SUBSCRIPTION);
                    break;
                case ANALYTICS:
                    viewChanged.setValue(ViewType.MAIN_MENU_ANALYTICS);
                    break;
                case QUICK_GUIDE:
                    bundle.putInt(ViewExtrasGlobals.EXTRA_QUICK_GUIDE_ENTER_ANIMATION,
                            isBackPress ? MainMenuAnmationType.LEFT_IN.ordinal() :
                                    MainMenuAnmationType.RIGHT_IN.ordinal());
                    toolbarDisplayTextResourceId.setValue(R.string.main_menu_item_quick_guide_uc);
                    setupMainMenuList(ViewType.MAIN_MENU_QUICK_GUIDE_LIST);
                    viewChanged.setValue(ViewType.MAIN_MENU_QUICK_GUIDE_LIST.setArgs(bundle));
                    break;
                case QUICK_GUIDE_ACTON_II:
                case QUICK_GUIDE_WOBURN_II:
                case QUICK_GUIDE_STANMORE_II:
                    toolbarDisplayTextResourceId.setValue(R.string.main_menu_item_quick_guide_uc);
                    setupMainMenuList(ViewType.MAIN_MENU_QUICK_GUIDE_JOPLIN);
                    viewChanged.setValue(ViewType.MAIN_MENU_QUICK_GUIDE_JOPLIN);
                    break;
                case QUICK_GUIDE_MONITOR_II:
                    toolbarDisplayTextResourceId.setValue(R.string.main_menu_item_quick_guide_uc);
                    setupMainMenuList(ViewType.MAIN_MENU_QUICK_GUIDE_OZZY);
                    viewChanged.setValue(ViewType.MAIN_MENU_QUICK_GUIDE_OZZY);
                    break;
                case ONLINE_MANUAL:
                    toolbarDisplayTextResourceId.setValue(R.string.main_menu_item_online_manual_uc);
                    setupMainMenuList(ViewType.MAIN_MENU_ONLINE_MANUAL);
                    viewChanged.setValue(ViewType.MAIN_MENU_ONLINE_MANUAL);
                    break;
                case ONLINE_MANUAL_ACTON:
                    bundle.putInt(TymphanyDevice.EXTRA_DEVICE_TYPES, DeviceSubType.JOPLIN_S.ordinal());
                    bundle.putInt(ViewExtrasGlobals.EXTRA_QUICK_GUIDE_ENTER_ANIMATION,
                            MainMenuAnmationType.LEFT_IN.ordinal());
                    viewChanged.setValue(ViewType.MAIN_MENU_ONLINE_MANUAL_DEVICE.setArgs(bundle));
                    break;
                case ONLINE_MANUAL_STANMORE:
                    bundle.putInt(TymphanyDevice.EXTRA_DEVICE_TYPES, DeviceSubType.JOPLIN_M.ordinal());
                    bundle.putInt(ViewExtrasGlobals.EXTRA_QUICK_GUIDE_ENTER_ANIMATION,
                            MainMenuAnmationType.LEFT_IN.ordinal());
                    viewChanged.setValue(ViewType.MAIN_MENU_ONLINE_MANUAL_DEVICE.setArgs(bundle));
                    break;
                case ONLINE_MANUAL_WOBURN:
                    bundle.putInt(TymphanyDevice.EXTRA_DEVICE_TYPES, DeviceSubType.JOPLIN_L.ordinal());
                    bundle.putInt(ViewExtrasGlobals.EXTRA_QUICK_GUIDE_ENTER_ANIMATION,
                            MainMenuAnmationType.LEFT_IN.ordinal());
                    viewChanged.setValue(ViewType.MAIN_MENU_ONLINE_MANUAL_DEVICE.setArgs(bundle));
                    break;
                case ONLINE_MANUAL_MONITOR_II:
                    bundle.putInt(TymphanyDevice.EXTRA_DEVICE_TYPES, DeviceSubType.OZZY.ordinal());
                    bundle.putInt(ViewExtrasGlobals.EXTRA_QUICK_GUIDE_ENTER_ANIMATION,
                            MainMenuAnmationType.LEFT_IN.ordinal());
                    viewChanged.setValue(ViewType.MAIN_MENU_ONLINE_MANUAL_DEVICE.setArgs(bundle));
                    break;
                case CONTACT:
                    viewChanged.setValue(ViewType.MAIN_MENU_CONTACT);
                    break;
                case END_USER_LICENSE_AGREEMENT:
                    viewChanged.setValue(ViewType.MAIN_MENU_EULA);
                    break;
                case FREE_AND_OPEN_SOURCE_SOFTWARE:
                    viewChanged.setValue(ViewType.MAIN_MENU_FOSS);
                    break;
                case BLUETOOTH_PAIRING_JOPLIN:
                    toolbarDisplayTextResourceId.setValue(R.string.main_menu_item_bluetooth_pairing_uc);
                    viewChanged.setValue(ViewType.MAIN_MENU_QUICK_GUIDE_JOPLIN_BLUETOOTH_PAIRING);
                    break;
                case BLUETOOTH_PAIRING_OZZY:
                    toolbarDisplayTextResourceId.setValue(R.string.main_menu_item_bluetooth_pairing_uc);
                    viewChanged.setValue(ViewType.MAIN_MENU_QUICK_GUIDE_OZZY_BLUETOOTH_PAIRING);
                    break;
                case ANC:
                    toolbarDisplayTextResourceId.setValue(R.string.main_menu_item_anc_button_uc);
                    viewChanged.setValue(ViewType.MAIN_MENU_QUICK_GUIDE_OZZY_ANC);
                    break;
                case M_BUTTON:
                    toolbarDisplayTextResourceId.setValue(R.string.main_menu_item_m_button_uc);
                    viewChanged.setValue(ViewType.MAIN_MENU_QUICK_GUIDE_OZZY_M_BUTTON);
                    break;
                case CONTROL_KNOB:
                    toolbarDisplayTextResourceId.setValue(R.string.main_menu_item_control_knob_uc);
                    viewChanged.setValue(ViewType.MAIN_MENU_QUICK_GUIDE_OZZY_CONTROL_KNOB);
                    break;
                case COUPLE_SPEAKERS:
                    toolbarDisplayTextResourceId.setValue(R.string.main_menu_item_couple_speakers);
                    viewChanged.setValue(ViewType.MAIN_MENU_QUICK_GUIDE_JOPLIN_COUPLE_SPEAKERS);
                    break;
                case PLAY_PAUSE_BUTTON:
                    toolbarDisplayTextResourceId.setValue(R.string.main_menu_item_play_pause_button_uc);
                    viewChanged.setValue(ViewType.MAIN_MENU_QUICK_GUIDE_JOPLIN_PLAY_PAUSE_BUTTON);
                    break;
                case MAIN_MENU_HELP:
                    toolbarDisplayTextResourceId.setValue(R.string.main_menu_item_help_uc);
                    setupMainMenuList(ViewType.MAIN_MENU_HELP);
                    viewChanged.setValue(ViewType.MAIN_MENU_HELP);
                    break;
            }
        }

        @NonNull
        @Override
        public MutableLiveData<String> getAppVersionText() {
            return appVersionText;
        }

        @NonNull
        @Override
        public MutableLiveData<List<MainMenuItem>> getMainMenuItemList() {
            return mainMenuItemList;
        }

        @Override
        @NonNull
        public MutableLiveData<ViewType> isViewChanged() {
            return viewChanged;
        }

        @Override
        @NonNull
        public MutableLiveData<Integer> getToolbarDisplayTextResourceId() {
            return toolbarDisplayTextResourceId;
        }

        @Override
        public MainMenuItem.MenuItemType getCurrentViewType() {
            return currentViewType;
        }

        private void initStates(@NonNull Resources resources) {
            setupMainMenuList(mainMenuFragmentViewType);
            setupToolbarText();
            setVersionTextIfNeeded(resources);
        }

        private void setupToolbarText() {
            switch (mainMenuFragmentViewType) {
                case MAIN_MENU_SETTINGS:
                    toolbarDisplayTextResourceId.setValue(R.string.main_menu_item_settings_uc);
                    break;
                case MAIN_MENU_HELP:
                    toolbarDisplayTextResourceId.setValue(R.string.main_menu_item_help_uc);
                    break;
                case MAIN_MENU_ABOUT:
                    toolbarDisplayTextResourceId.setValue(R.string.appwide_about_uc);
                    break;
            }
        }

        private void setupMainMenuList(ViewType mainMenuFragmentViewType) {
            mainMenuItemList.setValue(
                    mainMenuItemManager.getMainMenuItemList(mainMenuFragmentViewType));
        }

        private void setVersionTextIfNeeded(@NonNull Resources resources) {
            if (mainMenuFragmentViewType == ViewType.MAIN_MENU_ABOUT) {
                String appVersion = resources.getString(R.string.main_menu_about_name)
                        + "\n" + resources.getString(R.string.main_menu_about_version)
                        + " " + BuildConfig.VERSION_NAME;
                appVersionText.setValue(appVersion);
            }
        }
    }
}

package com.zoundindustries.marshallbt.viewmodels.homescreen;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.discovery.DeviceDiscoveryManager;
import com.zoundindustries.marshallbt.ui.homescreen.DeviceListFragment;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * ViewModel for the HomeActivity.
 * The flow of discovering devices is the following:
 * - HomeActivityViewModel is responsible for starting the first scan of the devices in the app.
 * - DeviceListViewModel is registering for devices discovery but is not handling initial scan.
 * - DeviceListViewModel is responsible for rescanning the device list on user demand
 * (swipe to refresh).
 * - Only one scan is possible at once.
 */
public interface HomeActivityViewModel {

    interface Outputs {

        /**
         * Provides LiveData boolean indicating that devices were found.
         * After finding first device {@link DeviceListFragment} is shown.
         *
         * @return LiveData boolean object indicatinf that some device was found.
         */
        @NonNull
        MutableLiveData<Boolean> areHomeDevicesFound();
    }

    /**
     * Class seeking for first device in the system and invoking needed fragments.
     */
    class Body extends AndroidViewModel implements HomeActivityViewModel.Outputs {

        static final int SCAN_PERIOD_MILLIS = 5000;

        public final Outputs outputs = this;

        private final MutableLiveData<Boolean> areHomeDevicesFound;

        private DeviceDiscoveryManager deviceDiscoveryManager;
        private DeviceManager deviceManager;

        private Disposable serviceConnectionDisposable;
        private Disposable scanDisposable;
        private Disposable serviceReadyDisposable;

        /**
         * Constructor for {@link HomeActivityViewModel}.
         *
         * @param application context needed for starting DeviceService.
         */
        @Inject
        public Body(@NonNull Application application) {
            this(application,
                    new DeviceDiscoveryManager(application),
                    new DeviceManager(application));
        }

        /**
         * Constructor with {@link DeviceDiscoveryManager} parameter.
         *
         * @param application              context needed for starting DeviceService.
         * @param speakersDiscoveryManager to be used to discover devices.
         * @param deviceManager            to be used to get info about devices.
         */
        @VisibleForTesting
        public Body(@NonNull Application application,
                    @NonNull DeviceDiscoveryManager speakersDiscoveryManager,
                    @NonNull DeviceManager deviceManager) {
            super(application);

            areHomeDevicesFound = new MutableLiveData<>();

            this.deviceDiscoveryManager = speakersDiscoveryManager;
            this.deviceManager = deviceManager;

            setupServiceConnectionObserver();
        }

        @NonNull
        @Override
        public MutableLiveData<Boolean> areHomeDevicesFound() {
            return areHomeDevicesFound;
        }

        @Override
        protected void onCleared() {
            dispose();
        }

        private void setupServiceConnectionObserver() {
            serviceConnectionDisposable = deviceDiscoveryManager.isServiceConnected()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(serviceConnected -> {
                        if (serviceConnected) {
                            setupServiceReadyObserver();
                        }
                    });
        }

        private void setupServiceReadyObserver() {
            serviceReadyDisposable = deviceDiscoveryManager.isServiceReady()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(serviceConnected -> {
                        if (serviceConnected) {
                            initObservers();
                        }
                    });
        }

        private void initObservers() {
            if (!deviceDiscoveryManager.isDeviceScanInProgress()) {
                deviceDiscoveryManager.startScanForDevices();

                if (deviceManager.getCurrentDevices().isEmpty()) {
                    scanDisposable = deviceDiscoveryManager.getDiscoveredDevices()
                            .observeOn(AndroidSchedulers.mainThread())
                            .take(1)
                            .subscribe(devices -> areHomeDevicesFound.setValue(true));
                } else {
                    areHomeDevicesFound.setValue(true);
                }
                scheduleDevicesScanTimeoutCheck();
            } else {
                areHomeDevicesFound.setValue(!deviceManager.getCurrentDevices().isEmpty());
            }
        }

        private void scheduleDevicesScanTimeoutCheck() {
            new Handler().postDelayed(() -> {
                        deviceDiscoveryManager.stopScanForDevices();
                        if (deviceManager.getCurrentDevices().isEmpty()) {
                            areHomeDevicesFound.setValue(false);
                        }
                        deviceManager.startPeriodicScan();
                    },
                    SCAN_PERIOD_MILLIS);
        }


        private void dispose() {
            if (scanDisposable != null) {
                scanDisposable.dispose();
            }

            if (serviceConnectionDisposable != null) {
                serviceConnectionDisposable.dispose();
            }

            if (serviceReadyDisposable != null) {
                serviceReadyDisposable.dispose();
            }
        }


    }
}

package com.zoundindustries.marshallbt.viewmodels.devicesettings;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.devicesettings.EQData;
import com.zoundindustries.marshallbt.model.devicesettings.EQPreset;
import com.zoundindustries.marshallbt.model.devicesettings.EQTabType;
import com.zoundindustries.marshallbt.ui.ViewFlowController;
import com.zoundindustries.marshallbt.utils.EqPresetType;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

import java.util.Arrays;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;


/**
 * ViewModel for tabs of equaliser.
 * <p>
 * As there are three tabs - M1, M2, M3, their names are used in the code.
 */
public interface EQTabViewModel {
    interface Inputs {

        /**
         * EQ preset was selected from spinner.
         *
         * @param position position of the clicked item
         * @param tabType  number of tab which invoked the selection
         */
        void onEqPresetSelected(int position, EQTabType tabType, boolean fromUser);


        /**
         * EQ tab was changed. Settings needs to be udpated on the device.
         *
         * @param tabType that was selected.
         */
        void onEqTabChanged(EQTabType tabType);

        /**
         * Invoke reading current EQ presets state.
         */
        void readEQPreset();

        /**
         * Write custom EQ preset values after change made by the user.
         *
         * @param data newly set by the user.
         */
        void writeEQData(EQData data);
    }

    interface Outputs {
        /**
         * Notify that value for M2 tab preset was updated.
         *
         * @return position of the preset in the presets list
         */
        MutableLiveData<Integer> notifyM2PresetValue();

        /**
         * Notify that value for M2 tab preset was updated.
         *
         * @return position of the preset in the presets list
         */
        MutableLiveData<Integer> notifyM3PresetValue();

        /**
         * Notify that custom preset values M2 tab preset were updated.
         *
         * @return new custom preset values.
         */
        MutableLiveData<EQData> notifyM2CustomPresetValues();

        /**
         * Notify that custom preset values M3 tab preset were updated.
         *
         * @return new custom preset values.
         */
        MutableLiveData<EQData> notifyM3CustomPresetValues();

        /**
         * Notify currently selected EQ preset on the HW.
         *
         * @return currently selected tab.
         */
        MutableLiveData<EQTabType> notifyActiveTab();

        /**
         * Notify that initial value read was done and UI can be fully inflated.
         *
         * @return flag indicating values read.
         */
        MutableLiveData<Boolean> notifyValuesLoaded();


        LiveData<Boolean> notifyIsAuxSourceActive();

        /**
         * @return list of eq presets
         */
        List<EqPresetType> getPresets();

        /**
         * Is EQ_EXTENDED feature supported.
         *
         * @return flag indicating support of EQ_EXTENDED feature.
         */
        public boolean isEQExtended();

        /**
         * Get {@link io.reactivex.Observable} for changed view.
         *
         * @return viewType to be started.
         */
        MutableLiveData<ViewFlowController.ViewType> isViewChanged();
    }

    class Body extends AndroidViewModel implements Inputs, Outputs {

        private static final String TAG = "EQTabVM";
        private static final int CUSTOM_VALUES_DELAY_MS = 200;

        private final List<EqPresetType> presets;
        private DeviceManager deviceManager;
        private BaseDevice device;
        private Handler handler;
        private EqPresetType currentM2PresetType = EqPresetType.CUSTOM;
        private EqPresetType currentM3PresetType = EqPresetType.CUSTOM;

        public Inputs inputs = this;
        public Outputs outputs = this;

        private MutableLiveData<Integer> m2PresetPosition;
        private MutableLiveData<Integer> m3PresetPosition;
        private MutableLiveData<EQTabType> activeTab;
        private MutableLiveData<EQData> m2CustomPresetValues;
        private MutableLiveData<EQData> m3CustomPresetValues;
        private MutableLiveData<Boolean> valuesLoaded;
        private MutableLiveData<Boolean> isAuxSourceActive;
        private final MutableLiveData<ViewFlowController.ViewType> viewChanged;

        private Disposable eqPresetDisposable;
        private Disposable serviceReadyDisposable;
        private Disposable eqCustomPresetDisposable;
        private Disposable connectionDisposable;
        private Disposable auxSourceActiveDisposable;

        public Body(Application app, String deviceId) {
            super(app);

            deviceManager = new DeviceManager(app);
            device = deviceManager.getDeviceFromId(deviceId);
            handler = new Handler(Looper.getMainLooper());

            presets = Arrays.asList(EqPresetType.values());

            activeTab = new MutableLiveData<>();
            m2PresetPosition = new MutableLiveData<>();
            m3PresetPosition = new MutableLiveData<>();
            m2CustomPresetValues = new MutableLiveData<>();
            m3CustomPresetValues = new MutableLiveData<>();
            valuesLoaded = new MutableLiveData<>();
            viewChanged = new MutableLiveData<>();
            isAuxSourceActive = new MutableLiveData<>();

            serviceReadyDisposable = deviceManager
                    .isServiceReady()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(isReady -> {
                        if (isReady) {
                            initObserver();
                        }
                    });
        }

        @Override
        public void readEQPreset() {
            device.getBaseDeviceStateController().inputs.readEQPreset();
        }

        @Override
        public void writeEQData(EQData data) {
            switchToCustomPreset();

            device.getBaseDeviceStateController().inputs.setEQData(data);
            deviceManager.saveCustomEqualizerPreset(device, new EQPreset(data, EqPresetType.CUSTOM));

            setCustomValuesIfNeeded(data);
        }

        @Override
        public void onEqPresetSelected(int position, EQTabType tabType, boolean fromUser) {
            Log.d(TAG, "preset " + presets.get(position).name() +
                    " chosen on tab " + tabType.name());

            if (activeTab.getValue() == EQTabType.M2) {
                currentM2PresetType = presets.get(position);
            } else if (activeTab.getValue() == EQTabType.M3) {
                currentM3PresetType = presets.get(position);
            }

            if (fromUser) {
                device.getBaseDeviceStateController().inputs
                        .setEQPreset(tabType, presets.get(position));
            }
            EQPreset preset = deviceManager.getEqualizerPresetByIndex(device, position);

            setPresetBarsValues(tabType, preset);
        }

        @Override
        public void onEqTabChanged(EQTabType tabType) {
            device.getBaseDeviceStateController().inputs.setEQTabType(tabType);
            activeTab.setValue(tabType);
            if (tabType != EQTabType.M1) {
                handler.removeCallbacksAndMessages(null);
            }
        }

        @Override
        public List<EqPresetType> getPresets() {
            return presets;
        }

        @Override
        public MutableLiveData<ViewFlowController.ViewType> isViewChanged() {
            return viewChanged;
        }

        @Override
        public MutableLiveData<Integer> notifyM2PresetValue() {
            return m2PresetPosition;
        }

        @Override
        public MutableLiveData<Integer> notifyM3PresetValue() {
            return m3PresetPosition;
        }

        @Override
        public MutableLiveData<EQData> notifyM2CustomPresetValues() {
            return m2CustomPresetValues;
        }

        @Override
        public MutableLiveData<EQData> notifyM3CustomPresetValues() {
            return m3CustomPresetValues;
        }

        @Override
        public MutableLiveData<EQTabType> notifyActiveTab() {
            return activeTab;
        }

        @Override
        public MutableLiveData<Boolean> notifyValuesLoaded() {
            return valuesLoaded;
        }

        @Override
        public LiveData<Boolean> notifyIsAuxSourceActive() {
            return isAuxSourceActive;
        }

        @Override
        public boolean isEQExtended() {
            if (device != null) {
                return device.getBaseDeviceStateController()
                        .isFeatureSupported(FeaturesDefs.EQ_EXTENDED);
            } else {
                return false;
            }
        }

        @Override
        protected void onCleared() {
            dispose();
        }

        private void initObserver() {
            connectionDisposable = device.getBaseDeviceStateController().outputs.getConnectionInfo()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(connectionState -> {
                        if (connectionState == BaseDevice.ConnectionState.DISCONNECTED) {
                            viewChanged.setValue(ViewFlowController.ViewType.HOME_SCREEN);
                        }
                    });

            eqPresetDisposable = device.getBaseDeviceStateController().outputs.getEQPreset()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(preset -> {
                        EQTabType tabType = preset.getTabType();
                        activeTab.setValue(tabType);

                        EqPresetType presetTypeM2 = preset.getPresetTypeM2();
                        EqPresetType presetTypeM3 = preset.getPresetTypeM3();

                        if (presetTypeM2 == EqPresetType.CUSTOM || presetTypeM3 == EqPresetType.CUSTOM) {
                            handleCustomEqPresetRead(presetTypeM2, presetTypeM3);
                        } else {
                            setActiveTabPreset(presetTypeM2, presetTypeM3);
                        }
                    });

            if (!device.getBaseDeviceStateController().outputs.isAuxConfigurable()) {
                auxSourceActiveDisposable = device.getBaseDeviceStateController().outputs
                        .isSourceActive(BaseDevice.SourceType.AUX)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .subscribe(this::handleAuxSourceActive);
            }

            device.getBaseDeviceStateController().inputs.readEQPreset();
        }

        private void handleAuxSourceActive(Boolean value) {
            isAuxSourceActive.setValue(value);
        }

        private void setActiveTabPreset(@NonNull EqPresetType presetTypeM2,
                                        @NonNull EqPresetType presetTypeM3) {
            if (presets.contains(presetTypeM2)) {
                currentM2PresetType = presetTypeM2;
                m2PresetPosition.setValue(presets.indexOf(presetTypeM2));
            }

            if (presets.contains(presetTypeM3)) {
                currentM3PresetType = presetTypeM3;
                m3PresetPosition.setValue(presets.indexOf(presetTypeM3));
            }

            valuesLoaded.setValue(true);
        }

        private void setPresetBarsValues(@NonNull EQTabType tabType, @Nullable EQPreset preset) {
            if (preset != null) {
                switch (tabType) {
                    case M2:
                        m2CustomPresetValues.setValue(preset.getEqData());
                        break;
                    case M3:
                        m3CustomPresetValues.setValue(preset.getEqData());
                        break;
                }
            }
        }

        private void switchM2ToCustomPreset() {
            currentM2PresetType = EqPresetType.CUSTOM;
            m2PresetPosition.setValue(presets.indexOf(EqPresetType.CUSTOM));
            device.getBaseDeviceStateController().inputs
                    .setEQPreset(EQTabType.M2, EqPresetType.CUSTOM);
        }

        private void switchM3ToCustomPreset() {
            currentM3PresetType = EqPresetType.CUSTOM;
            m3PresetPosition.setValue(presets.indexOf(EqPresetType.CUSTOM));
            device.getBaseDeviceStateController().inputs
                    .setEQPreset(EQTabType.M3, EqPresetType.CUSTOM);
        }

        private void switchToCustomPreset() {
            if (activeTab.getValue() == EQTabType.M2 &&
                    (m2PresetPosition.getValue() == null || currentM2PresetType != EqPresetType.CUSTOM)) {
                switchM2ToCustomPreset();
            } else if (activeTab.getValue() == EQTabType.M3 &&
                    (m3PresetPosition.getValue() == null || currentM3PresetType != EqPresetType.CUSTOM)) {
                switchM3ToCustomPreset();
            }
        }

        private void setCustomValuesIfNeeded(@NonNull EQData data) {
            if (currentM2PresetType == EqPresetType.CUSTOM) {
                m2CustomPresetValues.setValue(data);
            }

            if (currentM3PresetType == EqPresetType.CUSTOM) {
                m3CustomPresetValues.setValue(data);
            }
        }

        private void handleCustomEqPresetRead(@NonNull EqPresetType presetTypeM2,
                                              @NonNull EqPresetType presetTypeM3) {
            eqCustomPresetDisposable = device.getBaseDeviceStateController().outputs.getEQ()
                    .observeOn(AndroidSchedulers.mainThread())
                    .take(1)
                    .subscribe(presetData -> {
                        EQData customData =
                                deviceManager.getEqualizerPresetByIndex(device, 0).getEqData();
                        EQPreset preset = new EQPreset(presetData, EqPresetType.CUSTOM);
                        if (!presetData.equals(customData)) {
                            deviceManager.saveCustomEqualizerPreset(device, preset);
                        }
                        setActiveTabPreset(presetTypeM2, presetTypeM3);

                        handler.postDelayed(() ->
                                setCustomValuesIfNeeded(preset.getEqData()), CUSTOM_VALUES_DELAY_MS);
                    });
            device.getBaseDeviceStateController().inputs.readEQData();
        }

        private void dispose() {
            if (eqPresetDisposable != null) {
                eqPresetDisposable.dispose();
            }

            if (serviceReadyDisposable != null) {
                serviceReadyDisposable.dispose();
            }

            if (eqCustomPresetDisposable != null) {
                eqCustomPresetDisposable.dispose();
            }

            if (auxSourceActiveDisposable != null) {
                auxSourceActiveDisposable.dispose();
            }
        }
    }
}

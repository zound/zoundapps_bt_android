package com.zoundindustries.marshallbt.viewmodels.factories.devicesettings;

import android.app.Application;
import androidx.lifecycle.ViewModel;
import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.model.ota.pojo.FirmwareFileMetaData;
import com.zoundindustries.marshallbt.viewmodels.devicesettings.AboutDeviceViewModel;
import com.zoundindustries.marshallbt.viewmodels.factories.BaseDeviceViewModelFactory;

/**
 * Factory providing new AboutDeviceView ViewModel if needed or old object if it exists
 */
public class AboutDeviceViewModelFactory extends BaseDeviceViewModelFactory {

    private final FirmwareFileMetaData firmwareFileMetaData;
    private final boolean shouldRetryOta;

    public AboutDeviceViewModelFactory(Application application,
                                       String deviceId,
                                       FirmwareFileMetaData firmwareFileMetaData,
                                       boolean shouldRetryOta) {
        super(application, deviceId);
        this.firmwareFileMetaData = firmwareFileMetaData;
        this.shouldRetryOta = shouldRetryOta;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new AboutDeviceViewModel.Body(
                application,
                deviceId,
                firmwareFileMetaData,
                shouldRetryOta);
    }
}

package com.zoundindustries.marshallbt.viewmodels.devicesettings;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.view.View;

import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

import static com.zoundindustries.marshallbt.ui.devicesettings.DeviceSettingsActivity.EXTRA_DEVICE_COUPLING_MODE;
import static com.zoundindustries.marshallbt.ui.devicesettings.DeviceSettingsActivity.EXTRA_DEVICE_COUPLING_SLAVE_ID;
import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

public interface CouplingModeViewModel {

    interface Inputs {

        /**
         * Indicates that user selected coupling mode which is of type CouplingMode.
         * Possible modes are:
         * STANDARD - devices will play the same audio on both channels,
         * STEREO - devices will act as one stereo device - one device is one channel.
         *
         * @param couplingMode to use for coupling.
         */
        void onCouplingModeSelected(@Nullable Body.CouplingMode couplingMode);

        /**
         * Indicates that user clicked Next button to proceed with coupling.
         * Selected mode will be used when coupling devices.
         */
        void onNextButtonClicked(@NonNull View view);
    }

    interface Outputs {

        /**
         * Provides live data with flag indicating if the CouplingMode.STANDARD was
         * selected/deselected.
         *
         * @return LiveData flag object indicating STANDARD mode selection state.
         */
        @NonNull
        MutableLiveData<Boolean> getStandardModeSelected();

        /**
         * Provides live data with flag indicating if the CouplingMode.STEREO was
         * selected/deselected.
         *
         * @return LiveData flag object indicating STEREO mode selection state.
         */
        @NonNull
        MutableLiveData<Boolean> getStereoModeSelected();

        /**
         * Provides live data with description of current chosen mode
         * or default description if non of available modes is selected.
         *
         * @return LiveData String object with mode description.
         */
        @NonNull
        MutableLiveData<String> getModeDescription();

        /**
         * Get {@link Observable} for changed view.
         *
         * @return viewType to be started.
         */
        @NonNull
        MutableLiveData<ViewType> isViewChanged();
    }

    final class Body extends AndroidViewModel implements Inputs, Outputs {

        /**
         * Enum indicating coupling mode selected by the user.
         * NONE - no mode selected,
         * STANDARD - devices will play the same audio on both channels,
         * STEREO - devices will act as one stereo device - one device is one channel.
         */
        public enum CouplingMode {
            NONE {
                @Override
                public CouplingMode setCouplingMode(CouplingMode selectedMode) {
                    return selectedMode;
                }
            },
            STANDARD {
                @Override
                public CouplingMode setCouplingMode(CouplingMode selectedMode) {
                    return selectedMode != STANDARD ? STANDARD : NONE;
                }
            },
            STEREO {
                @Override
                public CouplingMode setCouplingMode(CouplingMode selectedMode) {
                    return selectedMode != STEREO ? STEREO : NONE;
                }
            };

            public abstract CouplingMode setCouplingMode(CouplingMode mode);
        }

        public final Inputs inputs = this;
        public final Outputs outputs = this;

        private final MutableLiveData<Boolean> standardModeSelected;
        private final MutableLiveData<Boolean> stereoModeSelected;
        private final MutableLiveData<String> modeDescription;
        private final MutableLiveData<ViewType> viewChanged;

        private final String masterDeviceId;
        private final String slaveDeviceId;

        private final Resources resources;

        private CouplingMode selectedMode = CouplingMode.NONE;
        private DeviceManager deviceManager;

        private final FirebaseAnalyticsManager firebaseAnalyticsManager;

        private Disposable serviceReadyDisposable;
        private Disposable connectionDisposable;

        /**
         * Constructor for {@link CouplingModeViewModel} Body.
         *
         * @param application    context to be sued for Managers initialization.
         * @param masterDeviceId id for coupling master device.
         * @param slaveDeviceId  id for coupling slave device.
         */
        public Body(@NonNull Application application,
                    @NonNull String masterDeviceId,
                    @NonNull String slaveDeviceId) {
            this(application, masterDeviceId, slaveDeviceId,
                    new DeviceManager(application),
                    ((BluetoothApplication) application).getAppComponent().firebaseWrapper());
        }

        /**
         * Constructor for {@link CouplingModeViewModel} Body with test injections.
         *
         * @param application    context to be sued for Managers initialization.
         * @param masterDeviceId id for coupling master device.
         * @param slaveDeviceId  id for coupling slave device.
         * @param deviceManager  used to inject in tests.
         */
        @VisibleForTesting
        public Body(@NonNull Application application,
                    @NonNull String masterDeviceId,
                    @NonNull String slaveDeviceId,
                    @NonNull DeviceManager deviceManager,
                    @NonNull FirebaseAnalyticsManager firebaseAnalyticsManager) {
            super(application);

            standardModeSelected = new MutableLiveData<>();
            stereoModeSelected = new MutableLiveData<>();
            modeDescription = new MutableLiveData<>();
            viewChanged = new MutableLiveData<>();

            resources = getApplication().getResources();

            this.masterDeviceId = masterDeviceId;
            this.slaveDeviceId = slaveDeviceId;

            this.deviceManager = deviceManager;
            this.firebaseAnalyticsManager = firebaseAnalyticsManager;

            initDefaultLiveDataStates();
            initObservers();
        }

        @Override
        public void onCouplingModeSelected(@NonNull CouplingMode couplingMode) {
            if (couplingMode != null) {
                firebaseAnalyticsManager.eventCouplingModeSwitched(couplingMode);
                handleSelectedMode(couplingMode.setCouplingMode(selectedMode));
            }
        }

        @Override
        public void onNextButtonClicked(@NonNull View view) {
            Bundle argBundle = new Bundle();
            argBundle.putString(EXTRA_DEVICE_ID, masterDeviceId);
            argBundle.putString(EXTRA_DEVICE_COUPLING_SLAVE_ID, slaveDeviceId);
            argBundle.putInt(EXTRA_DEVICE_COUPLING_MODE, selectedMode.ordinal());

            viewChanged.setValue(ViewType.COUPLE_DONE.setArgs(argBundle));

            connectionDisposable.dispose();
        }

        @NonNull
        @Override
        public MutableLiveData<Boolean> getStandardModeSelected() {
            return standardModeSelected;
        }

        @NonNull
        @Override
        public MutableLiveData<Boolean> getStereoModeSelected() {
            return stereoModeSelected;
        }

        @NonNull
        @Override
        public MutableLiveData<String> getModeDescription() {
            return modeDescription;
        }

        @NonNull
        @Override
        public MutableLiveData<ViewType> isViewChanged() {
            return viewChanged;
        }

        @Override
        protected void onCleared() {
            super.onCleared();
            dispose();
        }

        private void initObservers() {
            serviceReadyDisposable = deviceManager
                    .isServiceReady()
                    .subscribe(isReady -> {
                        if (isReady) {
                            BaseDevice device = deviceManager.getDeviceFromId(masterDeviceId);
                            if (device != null) {
                                initConnectionObserver(device);
                            } else {
                                viewChanged.setValue(ViewType.HOME_SCREEN);
                            }
                        }
                    });
        }

        private void initConnectionObserver(@NonNull BaseDevice device) {
            connectionDisposable = device.getBaseDeviceStateController().outputs.getConnectionInfo()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(connectionState -> {
                        if (connectionState == BaseDevice.ConnectionState.DISCONNECTED) {
                            viewChanged.setValue(ViewType.HOME_SCREEN);
                        }
                    });
        }

        private void initDefaultLiveDataStates() {
            standardModeSelected.setValue(false);
            stereoModeSelected.setValue(false);
            modeDescription.setValue(resources.getString(
                    R.string.couple_screen_select_mode_ambient_and_stereo_desc));
        }

        private void handleSelectedMode(CouplingMode mode) {
            selectedMode = mode;
            standardModeSelected.setValue(mode == CouplingMode.STANDARD);
            stereoModeSelected.setValue(mode == CouplingMode.STEREO);
            setDescriptionForMode();
        }

        private void setDescriptionForMode() {
            // TODO: Provide translations for new strings - only eng is available as for now.
            switch (selectedMode) {
                case NONE:
                    modeDescription.setValue(resources.getString(
                            R.string.couple_screen_select_mode_ambient_and_stereo_desc));
                    break;
                case STEREO:
                    modeDescription.setValue(resources.getString(
                            R.string.couple_screen_select_mode_stereo_desc));
                    break;
                case STANDARD:
                    modeDescription.setValue(resources.getString(
                            R.string.couple_screen_select_mode_ambient_desc));
                    break;
            }
        }

        private void dispose() {
            if (connectionDisposable != null) {
                connectionDisposable.dispose();
            }

            if (serviceReadyDisposable != null) {
                serviceReadyDisposable.dispose();
            }
        }
    }
}

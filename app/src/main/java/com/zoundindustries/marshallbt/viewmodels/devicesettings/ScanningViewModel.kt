package com.zoundindustries.marshallbt.viewmodels.devicesettings

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.zoundindustries.marshallbt.model.device.DeviceManager

/**
 * Class used to handle the business logic for controlling scanning in the SettingsActivity
 * lifecycle context. It allows to forbid scanning.
 */
interface ScanningViewModel {
    interface Inputs {
        /**
         * Restrict scanning. Used to let UI control scanning.
         *
         * @param isAllowed flag toggling scanning allowed/disallowed.
         */
        fun setScanningAllowed(isAllowed: Boolean)
    }

    interface Outputs

    class Body(val app: Application) : AndroidViewModel(app), Inputs, Outputs {

        val inputs = this
        val outputs = this

        private var deviceManager: DeviceManager = DeviceManager(app)

        override fun setScanningAllowed(isAllowed: Boolean) {
            deviceManager.setScanAllowed(isAllowed)
        }
    }
}

package com.zoundindustries.marshallbt.viewmodels.factories.devicesettings;

import android.app.Application;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.viewmodels.devicesettings.CouplingModeViewModel;
import com.zoundindustries.marshallbt.viewmodels.factories.BaseDeviceViewModelFactory;

/**
 * Factory providing new CouplingMode ViewModel if needed or old object if it exists
 */
public class CouplingModeViewModelFactory extends BaseDeviceViewModelFactory {

    private final String slaveId;

    public CouplingModeViewModelFactory(Application application, String masterId, String slaveId) {
        super(application, masterId);
        this.slaveId = slaveId;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new CouplingModeViewModel.Body(application, deviceId, slaveId);
    }
}

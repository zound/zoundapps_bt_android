package com.zoundindustries.marshallbt.viewmodels.factories.mainmenu;

import android.app.Application;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.ui.ViewFlowController;
import com.zoundindustries.marshallbt.viewmodels.mainmenu.MainMenuViewModel;

public class MainMenuViewModelFactory implements ViewModelProvider.Factory {
    private Application application;
    private ViewFlowController.ViewType viewType;

    public MainMenuViewModelFactory(Application application, ViewFlowController.ViewType viewType) {
        this.application = application;
        this.viewType = viewType;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new MainMenuViewModel.Body(application, viewType);
    }
}

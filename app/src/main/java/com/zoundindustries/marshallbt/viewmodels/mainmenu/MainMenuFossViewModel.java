package com.zoundindustries.marshallbt.viewmodels.mainmenu;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.view.View;

import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;

import io.reactivex.annotations.NonNull;

public interface MainMenuFossViewModel {

    interface Inputs {

        /**
         * Handle GoToWebsite button being clicked
         *
         * @param v view on which the method is run
         */
        void onGoToWebsiteButtonClicked(@NonNull View v);
    }

    interface Outputs {

        /**
         * Get {@link MutableLiveData} for changed view.
         *
         * @return LiveData observable
         */
        @NonNull
        MutableLiveData<ViewType> isViewChanged();
    }

    class Body extends ViewModel implements Inputs, Outputs {

        public final Inputs inputs = this;

        public final Outputs outputs = this;

        private final MutableLiveData<ViewType> isViewChanged;

        public Body() {
            isViewChanged = new MutableLiveData<>();
        }

        @Override
        public void onGoToWebsiteButtonClicked(@NonNull View v) {
            isViewChanged.setValue(ViewType.MAIN_MENU_FOSS_WEBSITE);
        }

        @Override
        public MutableLiveData<ViewType> isViewChanged() {
            return isViewChanged;
        }
    }
}

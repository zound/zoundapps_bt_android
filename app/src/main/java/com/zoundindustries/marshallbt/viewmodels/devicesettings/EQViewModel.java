package com.zoundindustries.marshallbt.viewmodels.devicesettings;

import android.app.Application;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.devicesettings.EQData;
import com.zoundindustries.marshallbt.model.devicesettings.EQPreset;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.utils.EqPresetType;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * Class handling logic behind equaliser view
 */
public interface EQViewModel {

    interface Inputs {

        /**
         * Triggers when select one item on spinner
         */
        void onSpinnerItemSelected(int position);

        /**
         * Invoked when EQ value is updated
         *
         * @param type  type of EQ parameter that is changed
         * @param value value of parameter
         */
        void eqValueUpdatedByUser(EQData.ValueType type, int value);
    }

    interface Outputs {

        /**
         * Provides the the actual state of equalizers
         *
         * @return EQData object
         */
        @NonNull
        MutableLiveData<EQPreset> getEqPreset();

        /**
         * Provides a way of delivering spinner selection value to the view
         *
         * @return live data object with integer spinner selection
         */
        @NonNull
        MutableLiveData<Integer> getSpinnerSelection();

        /**
         * Get {@link io.reactivex.Observable} for changed view.
         *
         * @return viewType to be started.
         */
        MutableLiveData<ViewType> isViewChanged();

        /**
         * Provides the list of presets
         *
         * @return the presets list
         */
        @NonNull
        MutableLiveData<List<EqPresetType>> getEqualizerPresetTypeList();

        /**
         * Is EQ_EXTENDED feature supported.
         *
         * @return flag indicating support of EQ_EXTENDED feature.
         */
        boolean isEQExtended();
    }

    class Body extends AndroidViewModel implements Outputs, Inputs {

        private static final int PRESET_ANIMATION_DELAY_MS = 500;

        public final Inputs inputs = this;
        public final Outputs outputs = this;
        private final FirebaseAnalyticsManager firebaseAnalyticsManager;

        private final MutableLiveData<EQPreset> eqPreset;
        private final MutableLiveData<Integer> spinnerSelection;
        private final MutableLiveData<ViewType> viewChanged;
        private final MutableLiveData<List<EqPresetType>> presetTypeList;

        private String deviceId;
        private BaseDevice device;

        private DeviceManager deviceManager;

        private Disposable connectionDisposable;
        private Disposable eqDisposable;
        private Disposable serviceReadyDisposable;

        public Body(Application app, @NonNull String deviceId) {
            this(app, deviceId, new DeviceManager(app.getApplicationContext()),
                    ((BluetoothApplication) app).getAppComponent().firebaseWrapper());
        }

        @VisibleForTesting
        public Body(@NonNull Application app,
                    @NonNull String deviceId,
                    @NonNull DeviceManager deviceManager,
                    @NonNull FirebaseAnalyticsManager firebaseAnalyticsManager) {
            super(app);

            eqPreset = new MutableLiveData<>();
            spinnerSelection = new MutableLiveData<>();
            viewChanged = new MutableLiveData<>();
            presetTypeList = new MutableLiveData<>();

            this.deviceManager = deviceManager;
            this.deviceId = deviceId;
            this.firebaseAnalyticsManager = firebaseAnalyticsManager;

            initLiveData();

            initDevice();
            readEQData();
        }

        // --- Inputs ---

        @Override
        public void onSpinnerItemSelected(int position) {
            new Handler().postDelayed(() -> setPredefinedEQPreset(position),
                    PRESET_ANIMATION_DELAY_MS);
        }

        @Override
        public void eqValueUpdatedByUser(@NonNull EQData.ValueType type, int value) {
            EQPreset preset = eqPreset.getValue();
            if (preset != null) {
                preset.getEqData().setValue(type, value);

                if (deviceManager.getEqualizerPresetType(device, preset.getEqData()) != null) {
                    preset.setEqType(deviceManager.getEqualizerPresetType(device, preset.getEqData()));
                }

                spinnerSelection.setValue(deviceManager.getIndexOfEqualizerPreset(device, preset));
                eqPreset.setValue(preset);
                saveIfCustomPreset(preset);
                sendEQValue(preset.getEqData());
            }
        }

        // --- Outputs ---

        @NonNull
        @Override
        public MutableLiveData<EQPreset> getEqPreset() {
            return eqPreset;
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getSpinnerSelection() {
            return spinnerSelection;
        }

        @Override
        public MutableLiveData<ViewType> isViewChanged() {
            return viewChanged;
        }

        @NonNull
        @Override
        public MutableLiveData<List<EqPresetType>> getEqualizerPresetTypeList() {
            return presetTypeList;
        }

        @Override
        public boolean isEQExtended() {
            if (device != null) {
                return device.getBaseDeviceStateController()
                        .isFeatureSupported(FeaturesDefs.EQ_EXTENDED);
            } else {
                return false;
            }
        }

        @Override
        protected void onCleared() {
            super.onCleared();
            dispose();
        }

        // this is triggered for the first time by system, event is sent to firebase
        // `fromUser` flag should prevent that if it's a problem
        void setPredefinedEQPreset(int position) {

            // call to device manager is done only to handle the custom preset values
            EQPreset preset = deviceManager.getEqualizerPresetByIndex(device, position);
            if (preset != null) {
                prepareToSendFirebaseEvent(preset.getEqType());
                sendEQValue(preset.getEqData());
                eqPreset.setValue(preset);
            }
        }

        private void initLiveData() {
            eqPreset.setValue(new EQPreset(new EQData(new int[]{0, 0, 0, 0, 0}),
                    EqPresetType.CUSTOM));
        }

        private void initDevice() {
            serviceReadyDisposable = deviceManager
                    .isServiceReady()
                    .subscribe(isReady -> {
                        if (isReady) {
                            device = deviceManager.getDeviceFromId(deviceId);
                            if (device != null) {
                                initObservers();
                            } else {
                                viewChanged.setValue(ViewType.HOME_SCREEN);
                            }
                        }
                    });
        }

        private void initObservers() {
            if (device != null && device.getBaseDeviceStateController()
                    .isFeatureSupported(FeaturesDefs.CONNECTION_INFO)) {

                List<EqPresetType> presetList = deviceManager.getEqualizerPresetTypes(device);
                if (presetList != null) {
                    presetTypeList.setValue(presetList);
                }

                connectionDisposable = device.getBaseDeviceStateController()
                        .outputs.getConnectionInfo()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(connectionState -> {
                            if (connectionState == BaseDevice.ConnectionState.DISCONNECTED) {
                                viewChanged.setValue(ViewType.HOME_SCREEN);
                            }
                        });

                eqDisposable = device.getBaseDeviceStateController().outputs.getEQ()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(presetValues -> {
                            EQPreset preset = eqPreset.getValue();
                            if (preset != null) {
                                preset.setEqData(presetValues);
                                EqPresetType presetType = deviceManager.getEqualizerPresetType(device,
                                        preset.getEqData());
                                if (presetType != null) {
                                    preset.setEqType(presetType);
                                }
                                spinnerSelection.setValue(deviceManager.getIndexOfEqualizerPreset(
                                        device,
                                        preset));
                                eqPreset.setValue(preset);
                                saveIfCustomPreset(preset);
                            }
                        });
            }
        }

        private void sendEQValue(EQData eqData) {
            device.getBaseDeviceStateController()
                    .inputs
                    .setEQData(eqData);
        }

        private void readEQData() {
            if (device != null)
                device.getBaseDeviceStateController()
                        .inputs
                        .readEQData();
        }

        private void prepareToSendFirebaseEvent(@NonNull EqPresetType presetType) {
            firebaseAnalyticsManager.eventEqualizerPresetChanged(
                    device.getDeviceInfo().getProductName(), presetType);
        }

        private void saveIfCustomPreset(@NonNull EQPreset preset) {
            if (preset.getEqType().equals(EqPresetType.CUSTOM)) {
                deviceManager.saveCustomEqualizerPreset(device, preset);
            }
        }

        private void dispose() {
            if (connectionDisposable != null) {
                connectionDisposable.dispose();
            }

            if (eqDisposable != null) {
                eqDisposable.dispose();
            }

            if (serviceReadyDisposable != null) {
                serviceReadyDisposable.dispose();
            }
        }
    }
}

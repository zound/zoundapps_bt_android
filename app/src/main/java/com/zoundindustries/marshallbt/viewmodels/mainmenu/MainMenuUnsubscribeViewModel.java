package com.zoundindustries.marshallbt.viewmodels.mainmenu;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import android.util.Log;
import android.view.View;

import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.salesforce.SalesforceWebAPIController;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.utils.CommonPreferences;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

public interface MainMenuUnsubscribeViewModel {

    interface Inputs {

        /**
         * Handle confirm button being clicked - unsubscribe request
         *
         * @param view view on which the method is run
         */
        void onConfirmButtonClicked(@NonNull View view);

        /**
         * Handle cancel button being clicked - unsubscribe request
         *
         * @param view view on which the method is run
         */
        void onCancelButtonClicked(@NonNull View view);

    }

    interface Outputs {

        /**
         * Notify about correct display text for confirm button
         *
         * @return LiveData observable
         */
        @NonNull
        MutableLiveData<String> getConfirmButtonText();

        /**
         * Notify about possibility to click button
         *
         * @return LiveData observable
         */
        @NonNull
        MutableLiveData<Boolean> isButtonClickable();

        /**
         * Notify about error text visibility
         *
         * @return LiveData observable
         */
        @NonNull
        MutableLiveData<Integer> getErrorTextVisibility();

        /**
         * Notify about progress bar visibility
         *
         * @return LiveData observable
         */
        @NonNull
        MutableLiveData<Integer> getProgressBarVisibility();

        /**
         * Get {@link MutableLiveData} for changed view.
         *
         * @return LiveData observable
         */
        @NonNull
        MutableLiveData<ViewType> isViewChanged();
    }


    class Body extends AndroidViewModel implements Inputs, Outputs {

        private static final String TAG = Body.class.getSimpleName();
        private static final long OPERATION_TIMEOUT = 10L;

        public final Inputs inputs = this;
        public final Outputs outputs = this;

        private final CommonPreferences preferences;
        private final Resources resources;
        private final SalesforceWebAPIController salesforceWebAPIController;

        private final MutableLiveData<ViewType> isViewChanged;
        private final MutableLiveData<Integer> progressBarVisible;
        private final MutableLiveData<Integer> errorTextVisible;
        private final MutableLiveData<Boolean> isButtonClickable;
        private final MutableLiveData<String> confirmButtonText;

        private CompositeDisposable disposables = new CompositeDisposable();

        public Body(@NonNull Application application) {
            this(application,
                    ((BluetoothApplication) application).getAppComponent().saleforceApi(),
                    application.getResources(),
                    new CommonPreferences(application));
        }

        @VisibleForTesting
        public Body(@NonNull Application application,
                    @NonNull SalesforceWebAPIController salesforceWebAPIController,
                    @NonNull Resources resources,
                    @NonNull CommonPreferences preferences) {
            super(application);
            this.preferences = preferences;
            isViewChanged = new MutableLiveData<>();
            progressBarVisible = new MutableLiveData<>();
            errorTextVisible = new MutableLiveData<>();
            isButtonClickable = new MutableLiveData<>();
            confirmButtonText = new MutableLiveData<>();
            this.salesforceWebAPIController = salesforceWebAPIController;
            this.resources = resources;
            initDefaultLiveDataStates();
        }

        private void initDefaultLiveDataStates() {
            errorTextVisible.setValue(View.GONE);
            progressBarVisible.setValue(View.GONE);
            isButtonClickable.setValue(true);
            confirmButtonText.setValue(resources.getString(R.string.stay_updated_confirm_yes_uc));
        }

        @Override
        public void onConfirmButtonClicked(@NonNull View view) {
            prepareViewsForUnsubsription();
            unsubscribeUser();
        }


        @Override
        public void onCancelButtonClicked(@NonNull View view) {
            isViewChanged.setValue(ViewType.BACK);
        }

        @NonNull
        @Override
        public MutableLiveData<String> getConfirmButtonText() {
            return confirmButtonText;
        }

        @NonNull
        @Override
        public MutableLiveData<Boolean> isButtonClickable() {
            return isButtonClickable;
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getErrorTextVisibility() {
            return errorTextVisible;
        }

        @NonNull
        @Override
        public MutableLiveData<Integer> getProgressBarVisibility() {
            return progressBarVisible;
        }

        @NonNull
        @Override
        public MutableLiveData<ViewType> isViewChanged() {
            return isViewChanged;
        }

        @Override
        protected void onCleared() {
            super.onCleared();
            disposables.dispose();
        }

        private void unsubscribeUser() {
            String userEmail = preferences.getUserEmail();
            if (userEmail == null) {
                onUnsubscribeError();
            }

            disposables.add(salesforceWebAPIController.requestAccessToken()
                    .timeout(OPERATION_TIMEOUT, TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .take(1)
                    .subscribe(response -> {
                        if (response.isSuccessful() && response.body() != null) {
                            scheduleCustomerAccountCheckForUnsubscribe(userEmail, response.body().getAccess_token());
                        } else {
                            onUnsubscribeError();
                        }
                    }, e -> {
                        onUnsubscribeError();
                    }));
        }

        private void scheduleCustomerAccountCheckForUnsubscribe(String userEmail, String token) {
            disposables.add(salesforceWebAPIController.checkSubsriptionStatus(userEmail, token)
                    .timeout(OPERATION_TIMEOUT, TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .take(1)
                    .subscribe(customerData -> {
                        if(customerData.isSuccessful() && customerData.body() != null) {
                            if (!customerData.body().getAccountId().isEmpty()) {
                                updateCustomerAccountForUnsubscribe(customerData.body().getAccountId(), userEmail, token);
                            } else {
                                onUnsubscribeError();
                            }
                        } else {
                            onUnsubscribeError();
                        }
                    }, e -> {
                        onUnsubscribeError();
                    }));
        }

        private void updateCustomerAccountForUnsubscribe(String accountId, String userEmail, String token) {
            disposables.add(salesforceWebAPIController.updateAccountToUnsubscribe(accountId, userEmail, token)
                    .timeout(OPERATION_TIMEOUT, TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .take(1).subscribe( updateStatus -> {
                        if (updateStatus.isSuccessful()) {
                            onUnsubscribeSuccess();
                        } else {
                            onUnsubscribeError();
                        }
                    }, e -> {
                        onUnsubscribeError();
                    }));
        }

        private void prepareViewsForUnsubsription() {
            progressBarVisible.setValue(View.VISIBLE);
            errorTextVisible.setValue(View.GONE);
            isButtonClickable.setValue(false);
        }

        private void onUnsubscribeSuccess() {
            preferences.setUserEmail(null);
            isViewChanged.setValue(ViewType.HOME_SCREEN);
            progressBarVisible.setValue(View.GONE);
        }

        private void onUnsubscribeError() {
            isButtonClickable.setValue(true);
            progressBarVisible.setValue(View.GONE);
            errorTextVisible.setValue(View.VISIBLE);
            confirmButtonText.setValue(resources.getString(R.string.appwide_try_again_uc));
        }
    }
}


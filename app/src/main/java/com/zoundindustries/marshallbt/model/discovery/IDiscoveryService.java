package com.zoundindustries.marshallbt.model.discovery;

import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.model.device.BaseDevice;

import io.reactivex.Observable;

/**
 * Interface defining discovery service methods.
 */
public interface IDiscoveryService {

    /**
     * Scan for all available devices in the system.
     */
    void startScanForDevices();

    /**
     * Stop scanning for devices in the system.
     */
    void stopScanForDevices();

    /**
     * Scan for all available devices in the system and notify when device is found.
     *
     * @return observable on found device.
     */
    @NonNull
    Observable<BaseDevice> getDiscoveredDevices();

    /**
     * Scan for all available devices in the system and notify when device disappears.
     *
     * @return observable on disappeared device.
     */
    @NonNull
    Observable<BaseDevice> getDisappearedDevices();
}

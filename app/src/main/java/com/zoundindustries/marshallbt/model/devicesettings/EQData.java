package com.zoundindustries.marshallbt.model.devicesettings;

import androidx.annotation.NonNull;

import java.util.Objects;

/**
 * This represents values of equaliser preset
 */
public class EQData {

    private int bass;
    private int low;
    private int mid;
    private int upper;
    private int high;

    public enum ValueType {
        BASS,
        LOW,
        MID,
        UPPER,
        HIGH
    }

    /**
     * Create a EQData object with given five eq values. Preset type is set automatically,
     * basing on EQPresetManager defined list of presets.
     *
     * @param data eq parameters values should be of size 5
     */
    public EQData(@NonNull int[] data) {
        if (data.length != 5) {
            throw new IllegalArgumentException();
        }

        this.bass = data[0];
        this.low = data[1];
        this.mid = data[2];
        this.upper = data[3];
        this.high = data[4];
    }

    /**
     * Set specific eq parameter
     *
     * @param type  type of EQ parameter that is changed
     * @param value value of parameter
     */
    public void setValue(@NonNull ValueType type, int value) {
        switch (type) {
            case BASS:
                bass = value;
                break;
            case LOW:
                low = value;
                break;
            case MID:
                mid = value;
                break;
            case UPPER:
                upper = value;
                break;
            case HIGH:
                high = value;
                break;
        }
    }

    /**
     * Returns value of given EQ parameter
     *
     * @param type requested type of EQ parameter
     */
    public int getValue(@NonNull ValueType type) {
        switch (type) {
            case BASS:
                return bass;
            case LOW:
                return low;
            case MID:
                return mid;
            case UPPER:
                return upper;
            case HIGH:
                return high;
            default:
                return -1;
        }
    }

    public int getBass() {
        return bass;
    }

    public void setBass(int bass) {
        this.bass = bass;
    }

    public int getLow() {
        return low;
    }

    public void setLow(int low) {
        this.low = low;
    }

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public int getUpper() {
        return upper;
    }

    public void setUpper(int upper) {
        this.upper = upper;
    }

    public int getHigh() {
        return high;
    }

    public void setHigh(int high) {
        this.high = high;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EQData eqData = (EQData) o;

        return valuesEqual(eqData.toIntArray());
    }

    @Override
    public int hashCode() {
        return Objects.hash(bass, low, mid, upper, high);
    }

    public int[] toIntArray() {
        return new int[]{bass, low, mid, upper, high};
    }

    private boolean valuesEqual(int presetValues[]) {
        return bass == presetValues[0]
                && low == presetValues[1]
                && mid == presetValues[2]
                && upper == presetValues[3]
                && high == presetValues[4];
    }

}
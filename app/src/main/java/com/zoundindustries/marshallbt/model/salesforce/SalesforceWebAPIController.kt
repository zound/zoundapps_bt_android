package com.zoundindustries.marshallbt.model.salesforce

import com.zoundindustries.marshallbt.BuildConfig
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import org.json.JSONObject
import okhttp3.RequestBody
import okhttp3.ResponseBody
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


const val TAG = "SalesforceWebAPIController"
private const val TEST_CLIENT_ID = "3MVG9ZPHiJTk7yFyo2kgZvLTvphs0oJkBygviuHVXHaOnV7XEKWjjq7wsTrZRExf2vDvamVAgz2BYYzX6sUcn"
private const val TEST_CLIENT_SECRET = "2390149004901668929"
private const val TEST_API_USERNAME = "zound@crmtoolbox.com.SB1"
private const val TEST_API_PASSWORD = "ag8TXzgja3vsbvZcfpFfGRk5bWyEr7NDdIlRIpMpL"
// Salesforce credentials for production server
private const val PROD_CLIENT_ID = "3MVG9T46ZAw5GTfX0cJhyCIHKWHl5UxX482ihrxONmvEUS9BRskbI4ZeTQ4b87VlFBcW84damp0Mj3EgtLgnL"
private const val PROD_CLIENT_SECRET = "3631653585457419922"
private const val PROD_API_USERNAME = "zound@crmtoolbox.com"
private const val PROD_API_PASSWORD = "A5JM5Ny6GjQjVwhZr8E1SROWoINfFmiJaNOWB7ph"
// Constants used in requests
private const val GRANT_TYPE = "password"
private const val BRAND_PREFIX = "MH_"
private const val MARSHALL_RECORD_TYPE_ID = "0121i000000bqoEAAQ"
private const val BRAND_PC = "Marshall"
private const val CUSTOMER_NAME_FOR_SIGNUP = "Newsletter Signup"
private const val CONTENT_TYPE_FOR_CREATE_OR_UPDATE = "application/json"
private const val CONTENT_TYPE_FOR_REQUEST_BODY = "application/json; charset=utf-8"
private const val OPT_IN_OUT_DATE_FORMAT = "yyyy-MM-dd"
private const val REGISTRATION_SOURCE_NAME = "Marshall BT App"
// Field names for request body
private const val FIELD_PERSON_EMAIL = "PersonEmail"
private const val FIELD_RECORD_TYPE_ID = "RecordTypeId"
private const val FIELD_CUSTOMER_ID = "customerID__pc"
private const val FIELD_BRAND = "brand__pc"
private const val FIELD_LAST_NAME = "LastName"
private const val FIELD_PERMISSION_EMAIL = "PermissionEmail__pc"
private const val FIELD_PERMISSION_OFFERS = "PermissionEmailOffers__pc"
private const val FIELD_PERMISSION_UPDATES = "PermissionEmailProductUpdates__pc"
private const val FIELD_DATE_OPT_IN = "PermissionEmailOptinDate__pc"
private const val FIELD_DATE_OPT_OUT = "PermissionEmailOptoutDate__pc"
private const val FIELD_COUNTRY_CODE = "CountryCode__pc"
private const val FIELD_REGISTRATION_SOURCE = "RegistrationSourceId__pc"

/**
 * Class controlling Salesforce web API connection via retrofit
 */
class SalesforceWebAPIController @Inject constructor(var api: SalesforceWebAPI) {

    companion object {
        const val PROD_BASE_URL = "https://eu19.salesforce.com"
        const val TEST_BASE_URL = "https://cs85.salesforce.com"
    }

    /**
     * Get current access token
     */
    fun requestAccessToken() : Observable<Response<SalesforceAccessToken>> {
        return if (BuildConfig.DEBUG) {
            api.getAccessToken(GRANT_TYPE,
                    TEST_CLIENT_ID,
                    TEST_CLIENT_SECRET,
                    TEST_API_USERNAME,
                    TEST_API_PASSWORD)
        } else {
            api.getAccessToken(GRANT_TYPE,
                    PROD_CLIENT_ID,
                    PROD_CLIENT_SECRET,
                    PROD_API_USERNAME,
                    PROD_API_PASSWORD)
        }.subscribeOn(Schedulers.io())
    }

    /**
     * Check if user email address is tied to existing customer account
     */
    fun checkSubsriptionStatus(address: String, token : String) : Observable<Response<SalesforceCustomerData>> {
        return api.checkAddress(getCustomerId(address),
                getFullToken(token))
                .subscribeOn(Schedulers.io())
    }

    /**
     * Create customer account tied to given address
     */
    fun createAccount(address: String, token : String) : Observable<Response<SalesforceCustomerCreationStatus>> {
        return api.createAccount(getFullToken(token),
                CONTENT_TYPE_FOR_CREATE_OR_UPDATE,
                getBodyForCreate(address))
                .subscribeOn(Schedulers.io())
    }

    /**
     * Update customer account tied to given address by setting all subscriptions
     */
    fun updateAccountToSubscribe(accountId: String, address: String, token : String) : Observable<Response<ResponseBody>> {
        return api.updateAccount(accountId,
                getFullToken(token),
                CONTENT_TYPE_FOR_CREATE_OR_UPDATE,
                getBodyForUpdateSubscribe(address))
                .subscribeOn(Schedulers.io())
    }

    /**
     * Update customer account tied to given address by resetting all subscriptions
     */
    fun updateAccountToUnsubscribe(accountId: String, address: String, token : String) : Observable<Response<ResponseBody>> {
        return api.updateAccount(accountId, getFullToken(token),
                CONTENT_TYPE_FOR_CREATE_OR_UPDATE,
                getBodyForUpdateUnsubscribe(address))
                .subscribeOn(Schedulers.io())
    }

    private fun getCustomerId(address: String): String {
        return BRAND_PREFIX + address
    }

    private fun getFullToken(token: String): String {
        return "Bearer $token"
    }

    private fun getOptInOutDate(): String {
        val format = SimpleDateFormat(OPT_IN_OUT_DATE_FORMAT)
        return format.format(Date())
    }

    /**
     * Return 2-letter country code according to ISO 3166-1 alpha-2 standard, or null.
     */
    private fun getUserCountryCode(): String? {
        val countryCode = Locale.getDefault().country.toUpperCase(Locale.US)

        if (countryCode.isNotEmpty() && countryCode.length == 2 && countryCode[0].isLetter()) {
            return countryCode
        }

        return null
    }

    private fun getBodyForCreate(address: String): RequestBody {
        val jsonParams = listOfNotNull(FIELD_PERSON_EMAIL to address,
                FIELD_RECORD_TYPE_ID to MARSHALL_RECORD_TYPE_ID,
                FIELD_CUSTOMER_ID to getCustomerId(address),
                FIELD_BRAND to BRAND_PC,
                FIELD_LAST_NAME to CUSTOMER_NAME_FOR_SIGNUP,
                FIELD_PERMISSION_EMAIL to "true",
                FIELD_PERMISSION_OFFERS to "true",
                FIELD_PERMISSION_UPDATES to "true",
                FIELD_REGISTRATION_SOURCE to REGISTRATION_SOURCE_NAME,
                FIELD_COUNTRY_CODE to getUserCountryCode(),
                FIELD_DATE_OPT_IN to getOptInOutDate()
        ).toMap()

        return RequestBody.create(okhttp3.MediaType.parse(
                CONTENT_TYPE_FOR_REQUEST_BODY), JSONObject(jsonParams).toString())
    }

    private fun getBodyForUpdateSubscribe(address: String): RequestBody {
        val jsonParams = listOfNotNull(FIELD_PERSON_EMAIL to address,
                FIELD_PERMISSION_EMAIL to "true",
                FIELD_PERMISSION_OFFERS to "true",
                FIELD_PERMISSION_UPDATES to "true",
                FIELD_REGISTRATION_SOURCE to REGISTRATION_SOURCE_NAME,
                FIELD_COUNTRY_CODE to getUserCountryCode(),
                FIELD_DATE_OPT_IN to getOptInOutDate()
        ).toMap()

        return RequestBody.create(okhttp3.MediaType.parse(
                CONTENT_TYPE_FOR_REQUEST_BODY), JSONObject(jsonParams).toString())
    }

    private fun getBodyForUpdateUnsubscribe(address: String): RequestBody {
        val jsonParams = mapOf(FIELD_PERSON_EMAIL to address,
                FIELD_PERMISSION_EMAIL to "false",
                FIELD_PERMISSION_OFFERS to "false",
                FIELD_PERMISSION_UPDATES to "false",
                FIELD_DATE_OPT_OUT to getOptInOutDate())

        return RequestBody.create(okhttp3.MediaType.parse(
                CONTENT_TYPE_FOR_REQUEST_BODY), JSONObject(jsonParams).toString())
    }

}
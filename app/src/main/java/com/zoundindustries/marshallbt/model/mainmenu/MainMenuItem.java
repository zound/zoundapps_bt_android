package com.zoundindustries.marshallbt.model.mainmenu;


import io.reactivex.annotations.NonNull;

public class MainMenuItem {

    private String name;
    private MenuItemType type;

    MainMenuItem(@NonNull String name, @NonNull MenuItemType type) {
        this.name = name;
        this.type = type;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public MenuItemType getType() {
        return type;
    }

    /**
     * Enum containing types of possible item in all Main Menu categories (also nested ones)
     */
    public enum MenuItemType {
        ROOT,
        EMAIL_SUBSCRIPTION,
        ANALYTICS,
        MAIN_MENU_HELP,
        QUICK_GUIDE,
        BLUETOOTH_PAIRING_JOPLIN,
        COUPLE_SPEAKERS,
        PLAY_PAUSE_BUTTON,
        BLUETOOTH_PAIRING_OZZY,
        ANC,
        M_BUTTON,
        CONTROL_KNOB,
        CONTACT,
        ONLINE_MANUAL,
        ONLINE_MANUAL_STANMORE,
        ONLINE_MANUAL_WOBURN,
        ONLINE_MANUAL_ACTON,
        ONLINE_MANUAL_MONITOR_II,
        END_USER_LICENSE_AGREEMENT,
        FREE_AND_OPEN_SOURCE_SOFTWARE,
        QUICK_GUIDE_ACTON_II,
        QUICK_GUIDE_STANMORE_II,
        QUICK_GUIDE_WOBURN_II,
        QUICK_GUIDE_MONITOR_II
    }
}
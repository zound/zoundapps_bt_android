package com.zoundindustries.marshallbt.model.device.tymphany

import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import com.tym.tymappplatform.TAProtocol.TAProtocol.TASystem
import com.tym.tymappplatform.TAService.TASystemService.TASystemService
import com.zoundindustries.marshallbt.model.discovery.adapter.joplin.ITymphanySystemServiceListener
import io.reactivex.subjects.PublishSubject

private const val SCAN_TIMEOUT = 30000L
private const val RECONNECTION_DELAY = 1000L

/**
 * CLass managing scan of the specific device.
 * Used to trigger reconnection for random address device.
 *
 * @property taSystemService used to control scanning.
 * @property device to be filtered out from scanning.
 */
class ReconnectionScanController(private val taSystemService: TASystemService,
                                 private val device: TASystem) :
        ITymphanySystemServiceListener {

    /**
     * PublishSubject notifying client of found device.
     */
    val reconnectDeviceFound: PublishSubject<Boolean> = PublishSubject.create()

    var isReconnectInProgress: Boolean = false
        private set

    private lateinit var scanTimer: CountDownTimer
    private val handler: Handler = Handler(Looper.getMainLooper())

    init {
        taSystemService.registerObserver(this)
        initConnectionTimeout()
    }

    override fun didDiscoverSystem(taSystem: TASystem?, p1: Int, p2: ByteArray?) {
        if (taSystem?.deviceAddress == device.deviceAddress && isReconnectInProgress) {
            isReconnectInProgress = false
            taSystemService.stopScanSystems()
            scanTimer.cancel()
            handler.postDelayed({
                reconnectDeviceFound.onNext(true)
            }, RECONNECTION_DELAY)
        }
    }

    /**
     * Start reconnecting to current tasSystem using scan and address matching.
     */
    fun startReconnectionWithScan() {
        isReconnectInProgress = true
        taSystemService.stopScanSystems()

        //TODO: Wait for SDK API with scan filters and start scan with manufacturer data mask for static address.
        //TODO: Until then use property to cache currently used device.
        handler.postDelayed({
            taSystemService.startScanSystems()
            scanTimer.start()
        }, RECONNECTION_DELAY)
    }

    /**
     * Dispose any ongoing worker threads and timers.
     */
    fun dispose() {
        scanTimer.cancel()
        handler.removeCallbacksAndMessages(null)
    }

    private fun initConnectionTimeout() {
        scanTimer = object : CountDownTimer(SCAN_TIMEOUT, SCAN_TIMEOUT) {
            override fun onTick(millisUntilFinished: Long) {}

            override fun onFinish() {
                taSystemService.stopScanSystems()
                isReconnectInProgress = false
                reconnectDeviceFound.onNext(false)
            }
        }
    }
}
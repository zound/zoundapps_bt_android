package com.zoundindustries.marshallbt.model.firebase;

public class FirebaseEventConstants {

    //region Event Name
    // Fields represents available events names that are send to FirebaseAnalytics

    // device settings
    static final String EVENT_APP_SPEAKER_NAME_CHANGED = "appSpeakerNameChanged";
    static final String EVENT_APP_EQUALIZER_PRESET_CHANGED = "appGraphicalEqualizerPresetChanged";
    static final String EVENT_APP_COUPLING_SWITCHED_AMBIENT = "appCouplingSwitchedAmbient";
    static final String EVENT_APP_COUPLING_SWITCHED_STEREO = "appCouplingSwitchedStereo";
    static final String EVENT_APP_LIGHT_SETTING_CHANGED = "appLightSettingChanged";
    // onboarding events
    static final String EVENT_APP_STAY_UPDATED_SKIPPED = "appOnboardingStayUpdatedSkipped";
    static final String EVENT_APP_SHARE_DATA_SKIPPED = "appOnboardingShareDataSkipped";
    static final String EVENT_APP_SHARE_DATA_SWITCHED = "appShareDataSwitched";
    static final String EVENT_APP_NEWSLETTER_SIGNUP = "appNewsletterSignedUp";
    // sources
    static final String EVENT_APP_SOURCE_BLUETOOTH_ACTIVATED = "appSourceBluetoothActivated";
    static final String EVENT_APP_SOURCE_AUX_ACTIVATED = "appSourceAuxActivated";
    static final String EVENT_APP_SOURCE_RCA_ACTIVATED = "appSourceRcaActivated";
    static final String EVENT_APP_MEDIA_BUTTON_TOUCHED_PLAY = "appMediaButtonTouchedPlay";
    static final String EVENT_APP_MEDIA_BUTTON_TOUCHED_PAUSE = "appMediaButtonTouchedPause";
    static final String EVENT_APP_MEDIA_BUTTON_TOUCHED_NEXT = "appMediaButtonTouchedNext";
    static final String EVENT_APP_MEDIA_BUTTON_TOUCHED_PREV = "appMediaButtonTouchedPrev";
    // ota events
    static final String EVENT_APP_OTA_STARTED = "appOtaStarted";
    static final String EVENT_APP_OTA_COMPLETED = "appOtaCompleted";
    static final String EVENT_APP_OTA_FAILED = "appOtaFailed";
    static final String EVENT_APP_FORCED_OTA_STARTED = "appForcedOtaStarted";
    static final String EVENT_APP_FORCED_OTA_CHECK_STARTED = "appForcedOtaCheckStarted";
    static final String EVENT_APP_FORCED_OTA_CHECK_FINISHED = "appForcedOtaCheckFinished";
    // errors
    static final String EVENT_APP_ERROR_OCCURRED_OTA_DOWNLOAD = "appErrorOccurredOtaDownload";
    static final String EVENT_APP_ERROR_OCCURRED_OTA_FLASHING = "appErrorOccurredOtaFlashing";
    static final String EVENT_APP_ERROR_OCCURRED_OTA_UNDEFINED = "appErrorOccurredOtaUndefined";
    static final String EVENT_APP_ERROR_OCCURRED_BLUETOOTH = "appErrorOccurredBluetooth";
    static final String EVENT_APP_ERROR_OCCURRED_NETWORK = "appErrorOccurredNetwork";
    //location events
    static final String EVENT_APP_NO_DEVICES_FOUND_WITH_NO_LOCATION = "appNoDevicesFoundWithNoLocation";

    static final String EVENT_VOLUME_CHANGED = "appVolumeChanged";
    //endregion

    //region Parameter Key Name
    // Fields represents available parameters key names that are send to FirebaseAnalytics
    static final String PARAMETER_KEY_DEVICE_MODEL = "deviceModel";
    static final String PARAMETER_KEY_SWITCH_STATE = "switchState";
    static final String PARAMETER_KEY_PRESET = "preset";
    static final String PARAMETER_KEY_OTA_ERROR_TYPE = "otaError";
    static final String PARAMETER_LIGHT_VALUE = "light";
    static final String PARAMETERS_VOLUME_VALUE = "volume";
    //endregion

    //region Switch State
    // Fields represents available state of switch that is send to FirebaseAnalytics
    static final String SWITCH_STATE_ON = "on";
    static final String SWITCH_STATE_OFF = "off";
    //endregion
}

package com.zoundindustries.marshallbt.model.device.state.coupling;

import com.zoundindustries.marshallbt.model.device.BaseDevice.CouplingChannelType;
import com.zoundindustries.marshallbt.model.device.state.BaseDeviceState;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

/**
 * Class extending {@link BaseDeviceState} used to monitor coupling channel state of the device.
 */
public class CouplingChannelInfoState extends BaseDeviceState<CouplingChannelType> {

    /**
     * Constructor for {@link CouplingChannelInfoState}.
     */
    public CouplingChannelInfoState() {
        super(FeaturesDefs.COUPLING_CHANNEL);
    }
}

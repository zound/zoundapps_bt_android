package com.zoundindustries.marshallbt.model.device.state;

import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.model.coupling.DeviceCouplingInfo;
import com.zoundindustries.marshallbt.model.device.BaseDevice.ConnectionState;
import com.zoundindustries.marshallbt.model.device.BaseDevice.CouplingChannelType;
import com.zoundindustries.marshallbt.model.device.BaseDevice.SourceType;
import com.zoundindustries.marshallbt.model.device.SoundNotificationInfo;
import com.zoundindustries.marshallbt.model.device.state.battery.BatteryLevelState;
import com.zoundindustries.marshallbt.model.device.state.connection.ConnectionInfoState;
import com.zoundindustries.marshallbt.model.device.state.coupling.CouplingChannelInfoState;
import com.zoundindustries.marshallbt.model.device.state.coupling.CouplingInfoState;
import com.zoundindustries.marshallbt.model.device.state.deviceinfo.SpeakerInfoState;
import com.zoundindustries.marshallbt.model.device.state.dsp.EQExtendedState;
import com.zoundindustries.marshallbt.model.device.state.dsp.EQState;
import com.zoundindustries.marshallbt.model.device.state.light.LightState;
import com.zoundindustries.marshallbt.model.device.state.mbutton.MButtonState;
import com.zoundindustries.marshallbt.model.device.state.ota.OTAUpdateState;
import com.zoundindustries.marshallbt.model.device.state.pairingmode.PairingModeState;
import com.zoundindustries.marshallbt.model.device.state.playcontrol.AncState;
import com.zoundindustries.marshallbt.model.device.state.playcontrol.PlayControlState;
import com.zoundindustries.marshallbt.model.device.state.playcontrol.SongInfoState;
import com.zoundindustries.marshallbt.model.device.state.sounds.SoundNotificationState;
import com.zoundindustries.marshallbt.model.device.state.sources.AuxSource;
import com.zoundindustries.marshallbt.model.device.state.sources.BluetoothSource;
import com.zoundindustries.marshallbt.model.device.state.sources.RcaSource;
import com.zoundindustries.marshallbt.model.device.state.volume.VolumeState;
import com.zoundindustries.marshallbt.model.device.tymphany.SpeakerInfo;
import com.zoundindustries.marshallbt.model.devicesettings.AncData;
import com.zoundindustries.marshallbt.model.devicesettings.AncMode;
import com.zoundindustries.marshallbt.model.devicesettings.EQData;
import com.zoundindustries.marshallbt.model.devicesettings.EQExtendedData;
import com.zoundindustries.marshallbt.model.devicesettings.EQTabType;
import com.zoundindustries.marshallbt.model.devicesettings.MButtonSettingsItem;
import com.zoundindustries.marshallbt.model.player.sources.SongInfo;
import com.zoundindustries.marshallbt.utils.EqPresetType;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Class configuring and accessing devices states.
 */
public abstract class BaseDeviceStateController {

    public interface Inputs {

        /**
         * Change volume for the state.
         *
         * @param value new volume value.
         *              Throws UnsupportedOperationException if feature is not supported
         *              for inheriting state controller.
         */
        default void setVolume(int value) {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Set device playing state.
         *
         * @param playing new playing state value.
         *                Throws UnsupportedOperationException if feature is not supported
         *                for inheriting state controller.
         */
        default void setPlaying(boolean playing) {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Set device playingInfoText.
         *
         * @param state new connection state value.
         *              Throws UnsupportedOperationException if feature is not supported
         *              for inheriting state controller.
         */
        default void setConnectionInfo(ConnectionState state) {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Set device coupling connection state..
         *
         * @param channelInfoState channel used for setting up the connection.
         * @param MAC              slave device MAC address.
         *                         Throws UnsupportedOperationException if feature is not supported
         *                         for inheriting state controller.
         */
        default void setCouplingMasterConnected(@NonNull CouplingChannelType channelInfoState,
                                                @NonNull String MAC) {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Disconnect any coupled device for this device.
         * Throws UnsupportedOperationException if feature is not supported
         * for inheriting state controller.
         */
        default void setCouplingDisconnected() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Set device into pairing mode.
         * Throws UnsupportedOperationException if feature is not supported
         * for inheriting state controller.
         */
        default void startPairingMode() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Set sound notification info.
         * Throws UnsupportedOperationException if feature is not supported
         * for inheriting state controller.
         */
        default void setSoundNotification(SoundNotificationInfo soundNotification) {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Set device has update state.
         *
         * @param hasUpdate new has update state value.
         *                  Throws UnsupportedOperationException if feature is not supported
         *                  for inheriting state controller.
         */
        default void setHasUpdate(boolean hasUpdate) {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Set device speaker info state.
         *
         * @param speakerInfo new speaker info state value.
         *                    Throws UnsupportedOperationException if feature is not supported
         *                    for inheriting state controller.
         */
        default void setSpeakerInfo(SpeakerInfo speakerInfo) {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Set the brightness of the speaker LED
         *
         * @param brightness state of brightness
         *                   Throws UnsupportedOperationException if feature is not supported
         *                   for inheriting state controller.
         */
        default void setBrightness(int brightness) {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Set an audio source on the device
         *
         * @param sourceType type of
         */
        default void setAudioSource(SourceType sourceType) {
            throw new UnsupportedOperationException("Feature not supported");
        }

        // --------- eq ---------

        /**
         * Trigger data read for EQ
         */
        default void readEQData() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Set EQ preset to device
         *
         * @param preset EQData that will be set
         *               Throws UnsupportedOperationException if feature is not supported
         *               for inheriting state controller.
         */
        default void setEQData(EQData preset) {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Set eq preset for given tab type
         *
         * @param tabType
         * @param preset
         */
        default void setEQPreset(EQTabType tabType, EqPresetType preset) {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * set active eq tab type
         *
         * @param tabType
         */
        default void setEQTabType(EQTabType tabType) {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Trigger data read for EQ
         */
        default void readEQPreset() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        // --------- anc ---------
        default void readAncStatus() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        default void writeAncMode(AncMode ancMode) {
            throw new UnsupportedOperationException("Feature not supported");
        }

        default void writeAncLevel(int level) {
            throw new UnsupportedOperationException("Feature not supported");
        }

        default void writeMonitorLevel(int level) {
            throw new UnsupportedOperationException("Feature not supported");
        }

        // --------- off timer ---------

        default void readAutoOffTimer() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        default void writeTimerValue(int value) {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Trigger data read for audio status
         */
        default void readAudioStatus() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Trigger data read for coupling connection status
         */
        default void readCouplingConnectionStatus() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Trigger data read for sound notifications
         */
        default void readSoundInfoStatus() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Trigger data read for volume
         */
        default void readVolume() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Trigger data read for battery level
         */
        default void readBatteryLevel() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Trigger data read for pairing mode state.
         */
        default void readPairingMode() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Trigger to play the next song
         */
        default void playNextSong() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Trigger to play the previous song
         */
        default void playPreviousSong() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        //M-Button

        /**
         * Set active M-Button setting that is used on the HW.
         *
         * @param type to be set as active.
         */
        default void setMButtonActionType(MButtonSettingsItem.MButtonActionType type) {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Trigger data read for M-Button action type state.
         */
        default void readMButtonActionType() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Clear all device specific data (listeners, observables, etc.).
         * No default implementation. Exception not thrown since this is not feature specific input.
         */
        default void dispose() {
            //nop
        }

        void reconnect();
    }

    public interface Outputs {

        /**
         * Get volume observable to monitor volume state changes.
         *
         * @return observable for volume changes for the state or throws
         * UnsupportedOperationException if feature is not supported for
         * inheriting state controller.
         */
        @NonNull
        default Observable<Integer> getVolume() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Get battery level observable to monitor battery level state changes.
         *
         * @return observable for battery level changes for the state or throws
         * UnsupportedOperationException if feature is not supported for
         * inheriting state controller.
         */
        @NonNull
        default Observable<Integer> getBatteryLevel() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Get pairing mode observable to monitor pairing mode state changes.
         *
         * @return observable for pairing mode changes for the state or throws
         * UnsupportedOperationException if feature is not supported for
         * inheriting state controller.
         */
        @NonNull
        default Observable<Boolean> getPairingModeStatus() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Get playing observable to monitor playing state changes
         *
         * @return observable for playing state changes for the state or throws
         * UnsupportedOperationException if feature is not supported for
         * inheriting state controller.
         */
        @NonNull
        default Observable<Boolean> getPlaying() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Get connection observable to monitor connection state changes
         *
         * @return observable for connection state changes for the state or throws
         * UnsupportedOperationException if feature is not supported for
         * inheriting state controller.
         */
        @NonNull
        default Observable<ConnectionState> getConnectionInfo() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        @NonNull
        default ConnectionState getConnectionInfoCurrentValue() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Get connection observable to monitor coupling connection state info.
         *
         * @return observable for coupling connection state changes for the state or throws
         * UnsupportedOperationException if feature is not supported for
         * inheriting state controller.
         */
        @NonNull
        default Observable<DeviceCouplingInfo> getCouplingConnectionInfo() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        @NonNull
        default DeviceCouplingInfo getCouplingConnectionInfoCurrentValue() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Get channel observable to monitor coupling channel state info.
         * Possible channel configuration is: LEFT, RIGHT,
         * PARTY - both devices play the same content on both channels
         *
         * @return observable for coupling channel state changes for the state or throws
         * UnsupportedOperationException if feature is not supported for
         * inheriting state controller.
         */
        @NonNull
        default Observable<CouplingChannelType> getCouplingChannelInfo() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Get observable on the coupling peer device Id.
         *
         * @return observable for coupling peer device Id or throws
         * UnsupportedOperationException if feature is not supported for
         * inheriting state controller.
         */
        @NonNull
        default Observable<String> getCoupleId() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Get sounds notification state observable to monitor this settings.
         * Possible values to be set:
         * Power - sound notification on the device when it is powered on/off
         * Media Control - other events sound notifications (e.g. pairing, coupling).
         *
         * @return observable for sound notification state changes for the state or throws
         * UnsupportedOperationException if feature is not supported for
         * inheriting state controller.
         */
        @NonNull
        default Observable<SoundNotificationInfo> getSoundNotificationInfo() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Get has update observable to monitor available firmware updates for device.
         *
         * @return observable for ota update state changes or throws
         * UnsupportedOperationException if feature is not supported for
         * inheriting state controller.
         */
        @NonNull
        default Observable<Boolean> getHasUpdate() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Get EQData observable to monitor device equaliser values.
         *
         * @return observable for eq state changes or throws
         * UnsupportedOperationException if feature is not supported for
         * inheriting state controller.
         */
        @NonNull
        default Observable<EQData> getEQ() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        @NonNull
        default Observable<EQExtendedData> getEQPreset() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        // ------- anc -------

        default Observable<AncData> getAncData() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        // ------- off timer -------
        default Observable<Integer> getTimerValue() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Get device info observable to monitor information about speaker.
         *
         * @return observable for speaker info state changes or throws
         * UnsupportedOperationException if feature is not supported for
         * inheriting state controller.
         */
        @NonNull
        default Observable<SpeakerInfo> getSpeakerInfo() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Get current device info value
         *
         * @return {@link SpeakerInfo} object
         * UnsupportedOperationException if feature is not supported for
         * inheriting state controller.
         */
        @NonNull
        default SpeakerInfo getSpeakerInfoCurrentValue() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Get brightness of the speaker LED
         *
         * @return observable for brightness of the speaker
         * UnsupportedOperationException if feature is not supported for
         * inheriting state controller.
         */
        @NonNull
        default Observable<Integer> getBrightness() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Check if given audio source is activated.
         * <p>
         * We needed some kind of abstraction between source types for view models to stay generic.
         * That's why {@link SourceType} is used. This way we don't have to use source
         * type from specific SDK.
         *
         * @param sourceType type of source for checking
         * @return true if source is active, false other
         */
        default Observable<Boolean> isSourceActive(SourceType sourceType) {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Get information about the song
         *
         * @return observable for information of the actual song
         * UnsupportedOperationException if feature is not supported for
         * inheriting state controller.
         */
        @NonNull
        default Observable<SongInfo> getSongInfo() {
            throw new UnsupportedOperationException("Feature not supported");
        }

        /**
         * Get information about the current M-Button action type setting.
         *
         * @return observable for information of the actual song
         * UnsupportedOperationException if feature is not supported for
         * inheriting state controller.
         */
        @NonNull
        default Observable<MButtonSettingsItem.MButtonActionType> getMButtonActionType() {
            throw new UnsupportedOperationException("Feature not supported");
        }


        /**
         * determine the possibility to switch to bt source from app when aux is plugged in
         * @return cable source configurability
         */
        @NonNull
        default boolean isAuxConfigurable() {
            throw new UnsupportedOperationException("Feature not supported");
        }
    }

    // todo: is there a way to make i/o final, but be able to mock them anyway?
    public Inputs inputs = (Inputs) this;
    public Outputs outputs = (Outputs) this;

    private List<BaseDeviceState> states;
    private int features;

    /**
     * Constructor for BaseDeviceStateController.
     *
     * @param features supported by the device.
     */
    public BaseDeviceStateController(int features) {
        this.features = features;
        initStates();
    }

    /**
     * Get DeviceState for the requested feature. Throws {@link UnsupportedOperationException}
     * if feature not supported.
     *
     * @param feature for which state is returned.
     * @return state for the requested feature if available.
     */
    @NonNull
    protected BaseDeviceState getStateFromFeature(int feature) {
        for (BaseDeviceState state : states) {
            if (state.getStateType() == feature) {
                return state;
            }
        }
        throw new UnsupportedOperationException("Feature not supported");
    }

    /**
     * Method checking if feature is supported. in the device.
     *
     * @param feature type of {@link FeaturesDefs} feature that will be checked.
     * @return true if the feature is implemented, false otherwise
     */
    public boolean isFeatureSupported(int feature) {
        return (features & feature) == feature;
    }

    private void initStates() {
        states = new ArrayList<>();

        if (isFeatureSupported(FeaturesDefs.VOLUME)) {
            states.add(new VolumeState());
        }

        if (isFeatureSupported(FeaturesDefs.PLAY_CONTROL)) {
            states.add(new PlayControlState());
        }

        if (isFeatureSupported(FeaturesDefs.SONG_INFO)) {
            states.add(new SongInfoState());
        }

        if (isFeatureSupported(FeaturesDefs.CONNECTION_INFO)) {
            states.add(new ConnectionInfoState());
        }

        if (isFeatureSupported(FeaturesDefs.OTA_UPDATE)) {
            states.add(new OTAUpdateState());
        }

        if (isFeatureSupported(FeaturesDefs.SPEAKER_INFO)) {
            states.add(new SpeakerInfoState());
        }

        if (isFeatureSupported(FeaturesDefs.COUPLING_CONNECTION)) {
            states.add(new CouplingInfoState());
        }

        if (isFeatureSupported(FeaturesDefs.COUPLING_CHANNEL)) {
            states.add(new CouplingChannelInfoState());
        }

        if (isFeatureSupported(FeaturesDefs.SOUNDS_SETTINGS)) {
            states.add(new SoundNotificationState());
        }

        if (isFeatureSupported(FeaturesDefs.SOUNDS_SETTINGS_MEDIA_CONTROL)) {
            states.add(new SoundNotificationState());
        }

        if (isFeatureSupported(FeaturesDefs.EQ)) {
            states.add(new EQState());
        }

        if (isFeatureSupported(FeaturesDefs.EQ_EXTENDED)) {
            states.add(new EQExtendedState());
        }

        if (isFeatureSupported(FeaturesDefs.ANC)) {
            states.add(new AncState());
        }

        if (isFeatureSupported(FeaturesDefs.LIGHT)) {
            states.add(new LightState());
        }

        if (isFeatureSupported(FeaturesDefs.AUX_SOURCE)) {
            states.add(new AuxSource());
        }

        if (isFeatureSupported(FeaturesDefs.BLUETOOTH_SOURCE)) {
            states.add(new BluetoothSource());
        }

        if (isFeatureSupported(FeaturesDefs.RCA_SOURCE)) {
            states.add(new RcaSource());
        }

        if (isFeatureSupported(FeaturesDefs.BATTERY_LEVEL)) {
            states.add(new BatteryLevelState());
        }

        if (isFeatureSupported(FeaturesDefs.PAIRING_MODE_STATUS)) {
            states.add(new PairingModeState());
        }

        if (isFeatureSupported(FeaturesDefs.TIMER_OFF)) {
            states.add(new OffTimerState());
        }

        if (isFeatureSupported(FeaturesDefs.M_BUTTON)) {
            states.add(new MButtonState());
        }
    }
}

package com.zoundindustries.marshallbt.model.bronto;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BrontoApi {

    @GET(BrontoUserSubscriptionHelper.BRONTO_USER_REGISTER_API_URL_PATH)
    Observable<Response<Void>> userMailRegisterRequest(@Query("email") String userMail,
                                                       @Query("list1") String listId);

    @GET(BrontoUserSubscriptionHelper.BRONTO_USER_UNREGISTER_API_URL_PATH)
    Observable<Response<Void>> userMailUnregisterRequest(@Query("email") String userMail);
}

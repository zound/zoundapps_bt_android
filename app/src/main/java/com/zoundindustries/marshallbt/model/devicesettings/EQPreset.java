package com.zoundindustries.marshallbt.model.devicesettings;

import androidx.annotation.NonNull;

import com.tym.tymappplatform.utils.TACommonDefinitions;
import com.zoundindustries.marshallbt.utils.EqPresetType;

import java.util.Objects;

public class EQPreset {

    private EQData eqData;
    private EqPresetType eqType;

    public EQPreset(EQData eqData, EqPresetType eqType) {
        this.eqData = eqData;
        this.eqType = eqType;
    }

    public EQData getEqData() {
        return eqData;
    }

    public void setEqData(EQData eqData) {
        this.eqData = eqData;
    }

    public EqPresetType getEqType() {
        return eqType;
    }

    public void setEqType(EqPresetType eqType) {
        this.eqType = eqType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EQPreset)) return false;
        EQPreset that = (EQPreset) o;
        return Objects.equals(getEqData(), that.getEqData()) &&
                Objects.equals(getEqType(), that.getEqType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEqData(), getEqType());
    }

    /**
     * Return with a clone object of the current EQPreset
     * @return the clone object
     */
    @NonNull
    public EQPreset cloneEQPreset() {
        return new EQPreset(new EQData(eqData.toIntArray()), eqType);
    }
}

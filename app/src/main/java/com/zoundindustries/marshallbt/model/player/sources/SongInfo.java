package com.zoundindustries.marshallbt.model.player.sources;

/**
 * Class wrapping the information about songs
 */
public class SongInfo {

    private final String name;
    private final String album;
    private final String artist;
    private final String artworkUrl;
    private final int duration;

    private SongInfo(String name, String album, String artist, String artworkUrl, int duration) {
        this.name = name;
        this.album = album;
        this.artist = artist;
        this.artworkUrl = artworkUrl;
        this.duration = duration;
    }

    public String getName() {
        return name;
    }

    public String getAlbum() {
        return album;
    }

    public String getArtist() {
        return artist;
    }

    public String getArtworkUrl() {
        return artworkUrl;
    }

    public int getDuration() {
        return duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SongInfo songInfo = (SongInfo) o;

        if (duration != songInfo.duration) return false;
        if (name != null ? !name.equals(songInfo.name) : songInfo.name != null) return false;
        if (album != null ? !album.equals(songInfo.album) : songInfo.album != null) return false;
        if (artist != null ? !artist.equals(songInfo.artist) : songInfo.artist != null)
            return false;
        return artworkUrl != null ? artworkUrl.equals(songInfo.artworkUrl) : songInfo.artworkUrl == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (album != null ? album.hashCode() : 0);
        result = 31 * result + (artist != null ? artist.hashCode() : 0);
        result = 31 * result + (artworkUrl != null ? artworkUrl.hashCode() : 0);
        result = 31 * result + duration;
        return result;
    }

    public static class SongInfoBuilder {

        private String name = "";
        private String album = "";
        private String artist = "";
        private String artworkUrl = "";
        private int duration = 0;

        public SongInfoBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public SongInfoBuilder setAlbum(String album) {
            this.album = album;
            return this;
        }

        public SongInfoBuilder setArtist(String artist) {
            this.artist = artist;
            return this;
        }

        public SongInfoBuilder setArtworkUrl(String artworkUrl) {
            this.artworkUrl = artworkUrl;
            return this;
        }

        public SongInfoBuilder setDuration(int duration) {
            this.duration = duration;
            return this;
        }

        public SongInfo build() {
            return new SongInfo(name, album, artist, artworkUrl, duration);
        }
    }
}

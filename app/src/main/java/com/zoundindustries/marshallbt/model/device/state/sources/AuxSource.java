package com.zoundindustries.marshallbt.model.device.state.sources;

import com.zoundindustries.marshallbt.model.device.state.BaseDeviceState;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

/**
 * Class extending {@link BaseDeviceState} used to set and monitor aux source
 */
public class AuxSource extends BaseDeviceState<Boolean> {
    /**
     * Constructor for BaseDeviceState.
     */
    public AuxSource() {
        super(FeaturesDefs.AUX_SOURCE);
    }
}

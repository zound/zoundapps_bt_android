package com.zoundindustries.marshallbt.model.device.mock;

import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.model.adapters.mock.MockSDK;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.state.MockDeviceStateController;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

/**
 * Class defining Mock SDK speaker with specific features.
 */
public class MockSpeaker extends BaseDevice {

    private static final int FEATURES = FeaturesDefs.VOLUME |
            FeaturesDefs.CONNECTION_INFO |
            FeaturesDefs.PLAY_CONTROL |
            FeaturesDefs.OTA_UPDATE |
            FeaturesDefs.COUPLING_CONNECTION |
            FeaturesDefs.COUPLING_CHANNEL |
            FeaturesDefs.SPEAKER_INFO;

    private static final int MOCK_MAX_NAME_LENGTH = 17;

    /**
     * Constructor for MockSpeaker.
     *
     * @param deviceInfo unique device info for the device.
     */
    public MockSpeaker(@NonNull DeviceInfo deviceInfo, @NonNull MockSDK mockSDK) {
        super(deviceInfo, FEATURES, new MockDeviceStateController(FEATURES, mockSDK));
        ((MockDeviceStateController) getBaseDeviceStateController()).setDevice(this);
    }

    @Override
    public int getMaxNameLength() {
        return MOCK_MAX_NAME_LENGTH;
    }
}

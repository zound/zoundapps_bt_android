package com.zoundindustries.marshallbt.model.device.tymphany;

import androidx.annotation.NonNull;

import com.tym.tymappplatform.TAProtocol.TAProtocol.TASystem;
import com.tym.tymappplatform.TAService.TADigitalSignalProcessingService.TADigitalSignalProcessingService;
import com.tym.tymappplatform.TAService.TAPlayControlService.TAPlayControlService;
import com.tym.tymappplatform.TAService.TASystemService.TASystemService;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.state.TymphanyDeviceStateController;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.model.mainmenu.joplin.WebsiteLinkHolder;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

/**
 * Class defining Joplin SDK speaker with specific features.
 */
public class TymphanyDevice extends BaseDevice {

    public static final String EXTRA_DEVICE_TYPES = "EXTRA_DEVICE_TYPES";

    static final int JOPLIN_FEATURES = FeaturesDefs.COUPLING_CONNECTION |
            FeaturesDefs.COUPLING_CHANNEL |
            FeaturesDefs.EQ |
            FeaturesDefs.LIGHT |
            FeaturesDefs.AUX_SOURCE_CONFIGURABLE;


    private static final int TYMPHANY_FEATURES = FeaturesDefs.CONNECTION_INFO |
            FeaturesDefs.PLAY_CONTROL |
            FeaturesDefs.SONG_INFO |
            FeaturesDefs.VOLUME |
            FeaturesDefs.SPEAKER_INFO |
            FeaturesDefs.RENAME |
            FeaturesDefs.SOUNDS_SETTINGS;

    static final int OZZY_FEATURES = FeaturesDefs.M_BUTTON |
            FeaturesDefs.ANC |
            FeaturesDefs.TIMER_OFF |
            FeaturesDefs.EQ |
            FeaturesDefs.EQ_EXTENDED |
            FeaturesDefs.BATTERY_LEVEL |
            FeaturesDefs.BLUETOOTH_SOURCE |
            FeaturesDefs.AUX_SOURCE |
            FeaturesDefs.AUTO_CONNECT |
            FeaturesDefs.SOUNDS_SETTINGS |
            FeaturesDefs.SOUNDS_SETTINGS_MEDIA_CONTROL |
            FeaturesDefs.PAIRING_MODE_STATUS;

    private static final int TYMPHANY_MAX_NAME_LENGTH = 17;

    private DeviceSubType deviceSubType;

    public enum DeviceSubType {
        JOPLIN_S {
            @Override
            public String getProductName() {
                return "ACTON II";
            }

            @Override
            public String getUserManualUrl() {
                return UrlBuilder.buildOnlineManualUrl(
                        WebsiteLinkHolder.MARSHALL_USER_MANUAL_URL_ACTON_POSTFIX);
            }
        },
        JOPLIN_M {
            @Override
            public String getProductName() {
                return "STANMORE II";
            }

            @Override
            public String getUserManualUrl() {
                return UrlBuilder.buildOnlineManualUrl(
                        WebsiteLinkHolder.MARSHALL_USER_MANUAL_URL_STANMORE_POSTFIX);
            }
        },
        JOPLIN_L {
            @Override
            public String getProductName() {
                return "WOBURN II";
            }

            @Override
            public String getUserManualUrl() {
                return UrlBuilder.buildOnlineManualUrl(
                        WebsiteLinkHolder.MARSHALL_USER_MANUAL_URL_WOBURN_POSTFIX);
            }
        },
        JOPLIN_S_LITE {
            @Override
            public String getProductName() {
                return "ACTON II LITE";
            }

            @Override
            public String getUserManualUrl() {
                return UrlBuilder.buildOnlineManualUrl(
                        WebsiteLinkHolder.MARSHALL_USER_MANUAL_URL_ACTON_POSTFIX);
            }
        },
        JOPLIN_M_LITE {
            @Override
            public String getProductName() {
                return "STANMORE II LITE";
            }

            @Override
            public String getUserManualUrl() {
                return UrlBuilder.buildOnlineManualUrl(
                        WebsiteLinkHolder.MARSHALL_USER_MANUAL_URL_STANMORE_POSTFIX);
            }
        },
        JOPLIN_L_LITE {
            @Override
            public String getProductName() {
                return "WOBURN II LITE";
            }

            @Override
            public String getUserManualUrl() {
                return UrlBuilder.buildOnlineManualUrl(
                        WebsiteLinkHolder.MARSHALL_USER_MANUAL_URL_WOBURN_POSTFIX);
            }
        },
        OZZY {
            @Override
            public String getProductName() {
                return "OZZY";
            }

            @Override
            public String getUserManualUrl() {
                return UrlBuilder.buildOnlineManualUrl(
                        WebsiteLinkHolder.MARSHALL_USER_MANUAL_URL_MONITOR_II_POSTFIX);
            }
        };

        /**
         * used to unify with iOS send message to firebase events in {@link FirebaseAnalyticsManager}
         *
         * @return String common name of device
         */
        public abstract String getProductName();

        /**
         * provides static reference to correct user manual url
         *
         * @return user manual url for each speaker
         */
        public abstract String getUserManualUrl();
    }

    /**
     * Constructor for {@link TymphanyDevice}. Requires device basic parameters and features.
     *
     * @param deviceInfo unique identification of the device.
     */
    public TymphanyDevice(@NonNull DeviceInfo deviceInfo,
                          @NonNull TASystemService taSystemService,
                          @NonNull TAPlayControlService taPlayControlService,
                          @NonNull TADigitalSignalProcessingService taDspService,
                          //TODO: Remove this and use inheritance instead
                          @NonNull DeviceSubType deviceSubType,
                          @NonNull TASystem taSystem,
                          int features) {
        super(deviceInfo, TYMPHANY_FEATURES | features, new TymphanyDeviceStateController(
                TYMPHANY_FEATURES | features,
                deviceSubType,
                taSystemService,
                taPlayControlService,
                taDspService,
                taSystem));

        this.deviceSubType = deviceSubType;
    }

    @Override
    @NonNull
    public NameState getValidityState(@NonNull String newName) {
        if (newName.trim().length() == 0) {
            return NameState.EMPTY;
        } else if (newName.length() > 0 && newName.length() <= getMaxNameLength()) {
            return NameState.VALID;
        } else {
            return NameState.TOO_LONG;
        }
    }

    @Override
    public int getMaxNameLength() {
        return TYMPHANY_MAX_NAME_LENGTH;
    }

    /**
     * Get current Joplin speaker type.
     *
     * @return current {@link DeviceSubType}
     */
    @NonNull
    public DeviceSubType getDeviceSubType() {
        return deviceSubType;
    }

    /**
     * Return true if device is Lite version of Joplin
     *
     * @return flag indicating Lite version of Joplin speaker.
     */
    public boolean isLiteDevice() {
        return deviceSubType == DeviceSubType.JOPLIN_L_LITE ||
                deviceSubType == DeviceSubType.JOPLIN_M_LITE ||
                deviceSubType == DeviceSubType.JOPLIN_S_LITE;
    }

    /**
     * Return true if device is Joplin type.
     *
     * @return flag indicating if device is a Joplin speaker.
     */
    public boolean isJoplinDevice() {
        return deviceSubType == DeviceSubType.JOPLIN_L ||
                deviceSubType == DeviceSubType.JOPLIN_M ||
                deviceSubType == DeviceSubType.JOPLIN_S ||
                deviceSubType == DeviceSubType.JOPLIN_L_LITE ||
                deviceSubType == DeviceSubType.JOPLIN_M_LITE ||
                deviceSubType == DeviceSubType.JOPLIN_S_LITE;
    }

    /**
     * Get current TASystem object. This field can be updated after controller creation.
     *
     * @return current {@link TASystem}
     */
    public TASystem getTaSystem() {
        return ((TymphanyDeviceStateController) getBaseDeviceStateController()).getTaSystem();
    }

    /**
     * Update current TASystem. Needed for SDKs that use randomized addresses.
     *
     * @param system newlu udpated {@link TASystem}
     */
    public void setTaSystem(TASystem system) {
        ((TymphanyDeviceStateController) getBaseDeviceStateController()).setTaSystem(system);
    }


}

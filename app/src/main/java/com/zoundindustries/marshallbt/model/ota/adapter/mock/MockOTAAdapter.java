package com.zoundindustries.marshallbt.model.ota.adapter.mock;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zoundindustries.marshallbt.model.adapters.mock.MockSDK;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.ota.FirmwareVersion;
import com.zoundindustries.marshallbt.model.ota.IOTAService;
import com.zoundindustries.marshallbt.model.ota.OtaApi;
import com.zoundindustries.marshallbt.model.ota.pojo.FirmwareFileMetaData;
import com.zoundindustries.marshallbt.utils.FirmwareFileHelper;
import com.zoundindustries.marshallbt.utils.NetworkUtils;

import java.io.File;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Mock implementation of OTAdapter.
 */
public class MockOTAAdapter implements IOTAService {

    private static final String API_KEY = "Kq3hU8bnT62J4RP0s9u8b4zAZirVcIiwOyWj2M2e";

    private static final double PROGRESS_SCALE_FACTOR = 0.5;

    private static final String MOCK_BRAND = "zound";
    private static final String MOCK_MODEL = "sample";
    private static final String MOCK_VERSION = "latest";

    private Disposable downloadUrlDisposable;
    private Disposable downloadFileDisposable;

    private MockSDK mockSDK;

    private BehaviorSubject<Double> updateProgressSubject;
    private BehaviorSubject<UpdateState> updateStateSubject;

    private OtaApi otaApi;

    /**
     * Constructor for MockOTAAdapter with mock and retrofit dependencies.
     *
     * @param mockSDK simulation of the speakers SDK.
     * @param otaApi  retrofit instance to download firmware.
     */
    public MockOTAAdapter(MockSDK mockSDK, OtaApi otaApi) {
        this.mockSDK = mockSDK;
        this.otaApi = otaApi;
    }

    @Override
    @Nullable
    public Observable<Double> getFirmwareUpdateProgress(@NonNull BaseDevice device) {
        updateProgressSubject = BehaviorSubject.create();
        mockSDK.startMonitorUpdateProgress(device, progress -> {
            if (progress >= 100) {
                updateProgressSubject.onNext(progress);
                updateProgressSubject.onComplete();
                updateStateSubject.onNext(UpdateState.DOWNLOADING_FROM_SERVER);
                updateStateSubject.onComplete();
            }
            updateProgressSubject.onNext(progress);
        });
        return updateProgressSubject;
    }

    @Nullable
    @Override
    public Observable<UpdateState> getFirmwareUpdateState(@NonNull BaseDevice device) {
        updateStateSubject = BehaviorSubject.create();
        return updateStateSubject;
    }

    @Override
    public void startOTAUpdate(@NonNull BaseDevice device,
                               @NonNull FirmwareFileMetaData metaData) {
        updateStateSubject.onNext(UpdateState.DOWNLOADING_FROM_SERVER);

        FirmwareVersion firmwareVersion = getFirmwareVersionFromDevice(device);
        if (firmwareVersion == null) {
            return;
        }

        downloadUrlDisposable = otaApi
                .getDownloadURL(API_KEY, firmwareVersion.getBrand(),
                        firmwareVersion.getModel(),
                        firmwareVersion.getVersion())
                .subscribeOn(Schedulers.io())
                .subscribe(softwareFileResponse ->
                        startOtaDownloadFromUrl(device, softwareFileResponse));
    }

    /**
     * Clear the registrations and references for the MockOTAAdapter class.
     */
    public void dispose() {

        if (downloadUrlDisposable != null) {
            downloadUrlDisposable.dispose();
        }

        if (downloadFileDisposable != null) {
            downloadFileDisposable.dispose();
        }
    }

    @Nullable
    private FirmwareVersion getFirmwareVersionFromDevice(BaseDevice device) {
        FirmwareVersion firmwareVersion = null;
        switch (device.getDeviceInfo().getDeviceType()) {
            case MOCK:
                return new FirmwareVersion.Builder()
                        .setBrand(MOCK_BRAND)
                        .setModel(MOCK_MODEL)
                        .setVersion(MOCK_VERSION)
                        .create();
        }
        return firmwareVersion;
    }

    private void startOtaDownloadFromUrl(@NonNull BaseDevice device,
                                         @NonNull Response<FirmwareFileMetaData>
                                                 softwareFileResponse) {

        FirmwareFileMetaData body = softwareFileResponse.body();
        if (body != null) {
            File saveLocation = FirmwareFileMetaData.getFirmwareFile(body, new FirmwareFileHelper(null));

            if (saveLocation.exists()) {
                updateStateSubject.onNext(UpdateState.FLASHING_DEVICE);
                mockSDK.startOTAUpdate(device);
                return;
            }

            String url = body.getUrl();

            if (url != null) {
                downloadOtaFile(url, device, saveLocation);
            }
        }
    }

    private void downloadOtaFile(@NonNull String url, @NonNull BaseDevice device,
                                 File saveLocation) {
        downloadFileDisposable = otaApi.downloadOtaFile(API_KEY, url)
                .subscribeOn(Schedulers.io())
                .subscribe(responseBodyResponse -> {
                    if (mockSDK != null &&
                            responseBodyResponse.body() != null &&
                            saveFirmwareToFile(responseBodyResponse, saveLocation)) {

                        // todo change mock to real device flashing
                        updateStateSubject.onNext(UpdateState.FLASHING_DEVICE);
                        mockSDK.startOTAUpdate(device);
                    }
                });
    }

    private double getTotalProgress(long progress) {
        return ((Double) (progress * PROGRESS_SCALE_FACTOR)).intValue();
    }

    private boolean saveFirmwareToFile(@NonNull Response<ResponseBody> responseBodyResponse,
                                       @NonNull File saveLocation) {
        ResponseBody body = responseBodyResponse.body();
        return body != null &&
                NetworkUtils.writeResponseBodyToDisk(body, saveLocation);
    }
}

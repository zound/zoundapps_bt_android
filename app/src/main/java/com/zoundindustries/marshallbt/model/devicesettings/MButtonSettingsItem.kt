package com.zoundindustries.marshallbt.model.devicesettings

/**
 * Class wrapping data needed for M-Button settings item.
 *
 * @property type        defined in [MButtonActionType].
 * @property header      text shown as an item header on the list.
 * @property description text shown as an item description on the list.
 */
class MButtonSettingsItem(val type: MButtonActionType,
                          val header: String,
                          val description: String) {

    /**
     * Enum defining all possible types of M-Button settings set on the device.
     */
    enum class MButtonActionType {
        NONE,
        EQUALIZER,
        GOOGLE_ASSISTANT,
        NATIVE_ASSISTANT
    }
}

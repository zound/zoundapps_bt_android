package com.zoundindustries.marshallbt.model.device.state

import com.zoundindustries.marshallbt.utils.FeaturesDefs

/**
 * Class extending {@link BaseDeviceState} used to monitor information about off timer state.
 */
class OffTimerState : BaseDeviceState<Int>(FeaturesDefs.TIMER_OFF)
package com.zoundindustries.marshallbt.model.devicesettings;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zoundindustries.marshallbt.model.BaseManager;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.services.DeviceService;

import io.reactivex.Observable;

/**
 * Class providing general API for handling coupling of devices.
 */

public class CouplingManager extends BaseManager implements ICouplingService {

    /**
     * Constructor for {@link CouplingManager} invoking {@link DeviceService} connection.
     *
     * @param context used for binding with service.
     */
    public CouplingManager(@NonNull Context context) {
        super(context);
    }

    @Nullable
    @Override
    public Observable<BaseDevice> startScanForReadyToCoupleDevices(@NonNull BaseDevice device) {
        if (isBound) {
            return deviceService.startScanForReadyToCoupleDevices(device);
        }
        return null;
    }

    @Override
    public void stopGetDevicesReadyToCouple() {
        if (isBound) {
            deviceService.stopGetDevicesReadyToCouple();
        }
    }
}

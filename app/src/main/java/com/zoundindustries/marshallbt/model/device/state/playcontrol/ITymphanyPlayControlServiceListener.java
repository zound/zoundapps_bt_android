package com.zoundindustries.marshallbt.model.device.state.playcontrol;

import com.tym.tymappplatform.TAProtocol.TAProtocol.TASystem;
import com.tym.tymappplatform.TAService.TAPlayControlService.TAPlayControlServiceObserver;
import com.tym.tymappplatform.utils.SongTrackInfo;
import com.tym.tymappplatform.utils.TACommonDefinitions;

/**
 * Interface wrapping {@link TAPlayControlServiceObserver} to simplify usage of the delegates.
 */
public interface ITymphanyPlayControlServiceListener extends TAPlayControlServiceObserver {

    @Override
    default void didUpdateVolume(TASystem taSystem, int i) {
        //nop
    }

    @Override
    default void didUpdateTonesEQ(TASystem taSystem, int[] ints) {
        //nop
    }

    @Override
    default void didUpdateTrueWireless(TASystem taSystem,
                                       TACommonDefinitions.TWSStatusType twsStatusType,
                                       TACommonDefinitions.ChannelType channelType,
                                       byte[] bytes) {
        //nop
    }

    @Override
    default void didUpdateAudioStatus(TASystem taSystem,
                                      TACommonDefinitions.AudioSourceType audioSourceType,
                                      TACommonDefinitions.PlayBackStatusType playBackStatusType,
                                      int i,
                                      boolean b,
                                      boolean b1) {
        //nop
    }

    @Override
    default void didReadAudioSource(TASystem taSystem,
                                    TACommonDefinitions.AudioSourceType audioSourceType) {
        //nop
    }

    @Override
    default void didReadAudioPlayback(TASystem taSystem,
                                      TACommonDefinitions.PlayBackStatusType playBackStatusType) {
        //nop
    }

    @Override
    default void didReadAudioANC(TASystem taSystem, int i) {
        //nop
    }

    @Override
    default void didReadAudioPowerSound(TASystem taSystem, boolean b) {
        //nop
    }

    @Override
    default void didReadAudioOtherSound(TASystem taSystem, boolean b) {
        //nop
    }

    @Override
    default void didUpdatePlaySongTrackInfo(TASystem taSystem, SongTrackInfo songTrackInfo) {
        //nop
    }

    @Override
    default void didUpdateShareMe(TASystem taSystem,
                                  TACommonDefinitions.ShareMeStatusType shareMeStatusType,
                                  String s) {
        //nop
    }


    @Override
    default void wasCustomButtonPressed(TASystem taSystem,
                                TACommonDefinitions.CustomButtonActionType customButtonActionType) {

    }

    @Override
    default void didReadANC(TASystem taSystem, TACommonDefinitions.ANCMode ancMode,
                            int i,
                            int i1) {
        //nop
    }
}

package com.zoundindustries.marshallbt.model.device.state;

import com.zoundindustries.marshallbt.model.device.BaseDevice;

/**
 * Utility class to keep current state for the device. Used to keep the state for the device list
 * on the Home screen.
 */
public class RowItemState {

    private boolean isPlaying;
    private BaseDevice.ConnectionState connectionState;

    private RowItemState(RowItemStateBuilder builder) {
        this.isPlaying = builder.isPlaying;
        this.connectionState = builder.connectionState;
    }

    /**
     * Set playing bit for state variable.
     *
     * @param value to be set.
     */
    public void setPlaying(boolean value) {
        isPlaying = value;
    }

    /**
     * Set playingInfoText bit for state variable.
     *
     * @param value to be set.
     */
    public void setConnectionState(BaseDevice.ConnectionState value) {
        connectionState = value;
    }

    /**
     * Get current bit for playing state.
     *
     * @return current value.
     */
    public boolean isPlaying() {
        return isPlaying;
    }

    /**
     * Get current bit for playing state.
     *
     * @return current value.
     */
    public BaseDevice.ConnectionState getConnectionState() {
        return connectionState;
    }

    /**
     * Builder for RowItemState. Allows to create rowItemStates with specific set of values.
     */
    public static class RowItemStateBuilder {
        private boolean isPlaying = false;
        private BaseDevice.ConnectionState connectionState = BaseDevice.ConnectionState.DISCONNECTED;

        /**
         * Set builder isPlaying value
         *
         * @param isPlaying value to be set
         * @return builder object
         */
        public RowItemStateBuilder isPlaying(boolean isPlaying) {
            this.isPlaying = isPlaying;
            return this;
        }

        /**
         * Set builder connectionState value
         *
         * @param state value to be set
         * @return builder object
         */
        public RowItemStateBuilder connectionState(BaseDevice.ConnectionState state) {
            this.connectionState = state;
            return this;
        }

        /**
         * Build the RowItemState object basing on set states
         *
         * @return RowItemState object
         */
        public RowItemState build() {
            return new RowItemState(this);
        }

    }
}

package com.zoundindustries.marshallbt.model.devicesettings;

/**
 * Class encapsulating name and type for the settings item.
 * This generic settings item type should be used for all settings types.
 * This object is defining the row on the settings list.
 * The name field will be displayed to the user.
 */
public class DeviceSettingsItem {

    /**
     * Definition of possible settings types.
     */
    public enum SettingType {
        ABOUT,
        M_BUTTON,
        ANC,
        RENAME,
        COUPLE,
        FORGET,
        EQUALISER,
        EQ_EXTENDED,
        LIGHT,
        SOUNDS,
        TIMER_OFF
    }

    private String name;
    private SettingType type;

    /**
     * Constructor with all needed parameters.
     *
     * @param name that will be displayed in settings row.
     * @param type that will define the behaviour of the row:
     *             - onClick events
     *             - Possible UI custom changes.
     */
    public DeviceSettingsItem(String name, SettingType type) {
        this.name = name;
        this.type = type;
    }

    /**
     * Get the displayed name form the settings row item.
     * @return the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Get the settings item type to customize the settings row behaviour.
     * @return the type.
     */
    public SettingType getType() {
        return type;
    }
}

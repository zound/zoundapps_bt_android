package com.zoundindustries.marshallbt.model.devicesettings

/**
 * Class representing the anc data structure
 */
class AncData(val ancMode: AncMode, val ancValue: Int, val monitorValue: Int)
package com.zoundindustries.marshallbt.model.autopairing;

/**
 * Class wrapping asynchronous event for changed system BT BOND_STATE.
 * The event contains address to distinguish device that has changed the state.
 */
public class BondState {

    private String address;
    private int actionId;

    /**
     * Constructor for bond state event.
     * @param address indicates which device has changed the BT bonding state.
     * @param actionId new state.
     */
    public BondState(String address, int actionId) {
        this.address = address;
        this.actionId = actionId;
    }

    public String getAddress() {
        return address;
    }

    public int getActionId() {
        return actionId;
    }
}

package com.zoundindustries.marshallbt.model.devicesettings

/**
 * Class representing modes of ANC feature
 */
enum class AncMode {
    ON,
    MONITOR,
    OFF
}

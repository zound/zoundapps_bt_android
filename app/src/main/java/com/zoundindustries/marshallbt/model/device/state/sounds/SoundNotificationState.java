package com.zoundindustries.marshallbt.model.device.state.sounds;

import com.zoundindustries.marshallbt.model.device.SoundNotificationInfo;
import com.zoundindustries.marshallbt.model.device.state.BaseDeviceState;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

/**
 * Class extending {@link BaseDeviceState} used to set and monitor sounds notification feature
 * It will be automatically added to all devices with defined FeaturesDefs.CONNECTION_INFO feature.
 */
public class SoundNotificationState extends BaseDeviceState<SoundNotificationInfo> {

    /**
     * Constructor for SoundNotificationState. Contains required feature definition.
     */
    public SoundNotificationState() {
        super(FeaturesDefs.SOUNDS_SETTINGS);
    }
}

package com.zoundindustries.marshallbt.model.homescreen

/**
 * Class wrapping data needed for visualization of links for home screen list item.
 * Links are redirecting to the specific features in settings.
 *
 * @param imageId to be shown for the link.
 * @param label description of the feature to redirect to.
*/
class HomeScreenItemLinkResources (var imageId: Int, var label: String?)

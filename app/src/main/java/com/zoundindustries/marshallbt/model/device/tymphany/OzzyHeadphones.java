package com.zoundindustries.marshallbt.model.device.tymphany;

import androidx.annotation.NonNull;

import com.tym.tymappplatform.TAProtocol.TAProtocol.TASystem;
import com.tym.tymappplatform.TAService.TADigitalSignalProcessingService.TADigitalSignalProcessingService;
import com.tym.tymappplatform.TAService.TAPlayControlService.TAPlayControlService;
import com.tym.tymappplatform.TAService.TASystemService.TASystemService;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;

public class OzzyHeadphones extends TymphanyDevice {

    private static final int OZZY_MAX_NAME_LENGTH = 15;

    /**
     * Constructor for {@link TymphanyDevice}. Requires device basic parameters and features.
     *
     * @param deviceInfo unique identification of the device.
     */
    public OzzyHeadphones(@NonNull DeviceInfo deviceInfo,
                          @NonNull TASystemService taSystemService,
                          @NonNull TAPlayControlService taPlayControlService,
                          @NonNull TADigitalSignalProcessingService taDspService,
                          @NonNull TASystem taSystem) {
        super(deviceInfo,
                taSystemService,
                taPlayControlService, taDspService,
                DeviceSubType.OZZY,
                taSystem,
                OZZY_FEATURES);
    }

    @Override
    public int getMaxNameLength() {
        return OZZY_MAX_NAME_LENGTH;
    }
}

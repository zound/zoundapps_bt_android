package com.zoundindustries.marshallbt.model.device.tymphany;

import androidx.annotation.NonNull;
import android.util.Log;

import com.zoundindustries.marshallbt.model.mainmenu.joplin.WebsiteLinkHolder;

import java.util.Locale;

/**
 * Class used for creating URLs with different language codes.
 * URLs should be displayed basing on system language in first place, otherwise: English
 */
public class UrlBuilder {

    public static final String TAG = UrlBuilder.class.getSimpleName();

    private static final String MARSHALL_PRIVACY_POLICY =
            "https://www.marshallheadphones.com/marshall-bluetooth-app-pp-";

    //region strings with language codes for online manual
    private static final String MANUAL_URL_ENGLISH = "en";
    private static final String MANUAL_URL_DANISH = "da";
    private static final String MANUAL_URL_DUTCH = "nl";
    private static final String MANUAL_URL_GERMAN = "de";
    private static final String MANUAL_URL_FRENCH = "fr";
    private static final String MANUAL_URL_ITALIAN = "it";
    private static final String MANUAL_URL_NORWEGIAN = "no";
    private static final String MANUAL_URL_PORTUGUESE = "pt";
    private static final String MANUAL_URL_RUSSIAN = "ru";
    private static final String MANUAL_URL_SPANISH = "es";
    private static final String MANUAL_URL_SWEDISH = "se";
    private static final String MANUAL_URL_FINNISH = "fi";
    private static final String MANUAL_URL_INDONESIAN = "id";
    private static final String MANUAL_URL_JAPANESE = "jp";
    private static final String MANUAL_URL_FILIPINO = "ph";
    private static final String MANUAL_URL_CHINESE_SIMPLE = "zh-cn";
    private static final String MANUAL_URL_CHINESE_TRADITIONAL = "zh-tw";
    //endregion

    //region strings with language codes for privacy policy url
    private static final String POLICY_URL_ENGLISH = "en";
    private static final String POLICY_URL_FRENCH = "fr";
    private static final String POLICY_URL_GERMAN = "de";
    private static final String POLICY_URL_DUTCH = "nl";
    private static final String POLICY_URL_DANISH = "dk";
    private static final String POLICY_URL_FINNISH = "fi";
    private static final String POLICY_URL_ITALIAN = "it";
    private static final String POLICY_URL_PORTUGUESE = "pt";
    private static final String POLICY_URL_SPANISH = "es";
    private static final String POLICY_URL_SWEDISH = "se";
    private static final String POLICY_URL_RUSSIAN = "ru";
    private static final String POLICY_URL_CHINESE_SIMPLE = "cn-simp";
    private static final String POLICY_URL_INDONESIAN = "id";
    private static final String POLICY_URL_JAPANESE = "jp";
    private static final String POLICY_URL_FILIPINO = "ph";
    private static final String POLICY_URL_CHINESE_TRADITIONAL = "cn-trad";
    private static final String POLICY_URL_NORWEGIAN = "no";
    //endregion

    //region strings returned by Locale.getDefault().getLanguage()
    private static final String SYSTEM_ENGLISH = "en";
    private static final String SYSTEM_DANISH = "da";
    private static final String SYSTEM_DUTCH = "nl";
    private static final String SYSTEM_GERMAN = "de";
    private static final String SYSTEM_FRENCH = "fr";
    private static final String SYSTEM_ITALIAN = "it";
    private static final String SYSTEM_PORTUGUESE = "pt";
    private static final String SYSTEM_RUSSIAN = "ru";
    private static final String SYSTEM_SPANISH = "es";
    private static final String SYSTEM_FINNISH = "fi";
    private static final String SYSTEM_NORWEGIAN = "nb";
    private static final String SYSTEM_SWEDISH = "sv";
    private static final String SYSTEM_CHINESE = "zh";
    private static final String SYSTEM_INDONESIAN = "in";
    private static final String SYSTEM_JAPANESE = "ja";
    private static final String SYSTEM_FILIPINO = "fil";
    //endregion

    // used for distinguish Chinese languages - Locale.getDefault().getLanguage() returns "zh"
    private static final String CHINESE_TRADITIONAL_SCRIPT_LANGUAGE_CODE = "Hant";

    /**
     * @param userManualUrlPostfix last part of the url which is the same for every device
     * @return online manual url for device based on system language
     * <p>
     * When system language is not supported by online manual, user will be redirected to english
     */
    @NonNull
    public static String buildOnlineManualUrl(@NonNull String userManualUrlPostfix) {
        String systemLanguageCode = Locale.getDefault().getLanguage();
        String manualLanguageCode = getLanguageCodeForOnlineManualUrl(systemLanguageCode);

        Log.d(TAG, "buildOnlineManualUrl: convertedToUrlLanguage - " + manualLanguageCode);
        return WebsiteLinkHolder.MARSHALL_USER_MANUAL_URL_PREFIX + manualLanguageCode
                + userManualUrlPostfix;
    }

    /**
     * Build url for privacy policy link, depending on country code
     * @return string with localised privacy policy
     */
    @NonNull
    public static String buildPrivacyPolicyUrl() {
        String systemLanguageCode = Locale.getDefault().getLanguage();
        String policyLanguageCode = getLanguageCodeForPrivacyPolicyUrl(systemLanguageCode);

        return MARSHALL_PRIVACY_POLICY + policyLanguageCode;
    }

    private static String getLanguageCodeForOnlineManualUrl(@NonNull String systemLanguageCode) {
        switch (systemLanguageCode) {
            case SYSTEM_ENGLISH:
                return MANUAL_URL_ENGLISH;
            case SYSTEM_DANISH:
                return MANUAL_URL_DANISH;
            case SYSTEM_DUTCH:
                return MANUAL_URL_DUTCH;
            case SYSTEM_GERMAN:
                return MANUAL_URL_GERMAN;
            case SYSTEM_FRENCH:
                return MANUAL_URL_FRENCH;
            case SYSTEM_ITALIAN:
                return MANUAL_URL_ITALIAN;
            case SYSTEM_PORTUGUESE:
                return MANUAL_URL_PORTUGUESE;
            case SYSTEM_RUSSIAN:
                return MANUAL_URL_RUSSIAN;
            case SYSTEM_SPANISH:
                return MANUAL_URL_SPANISH;
            case SYSTEM_FINNISH:
                return MANUAL_URL_FINNISH;
            case SYSTEM_NORWEGIAN:
                return MANUAL_URL_NORWEGIAN;
            case SYSTEM_SWEDISH:
                return MANUAL_URL_SWEDISH;
            case SYSTEM_CHINESE:
                if (isScriptChineseTraditional()) {
                    return MANUAL_URL_CHINESE_TRADITIONAL;
                } else {
                    return MANUAL_URL_CHINESE_SIMPLE;
                }
            case SYSTEM_INDONESIAN:
                return MANUAL_URL_INDONESIAN;
            case SYSTEM_JAPANESE:
                return MANUAL_URL_JAPANESE;
            case SYSTEM_FILIPINO:
                return MANUAL_URL_FILIPINO;
            default:
                return MANUAL_URL_ENGLISH;
        }
    }

    private static String getLanguageCodeForPrivacyPolicyUrl(@NonNull String systemLanguageCode) {
        switch (systemLanguageCode) {
            case SYSTEM_ENGLISH:
                return POLICY_URL_ENGLISH;
            case SYSTEM_FRENCH:
                return POLICY_URL_FRENCH;
            case SYSTEM_GERMAN:
                return POLICY_URL_GERMAN;
            case SYSTEM_DUTCH:
                return POLICY_URL_DUTCH;
            case SYSTEM_DANISH:
                return POLICY_URL_DANISH;
            case SYSTEM_FINNISH:
                return POLICY_URL_FINNISH;
            case SYSTEM_ITALIAN:
                return POLICY_URL_ITALIAN;
            case SYSTEM_PORTUGUESE:
                return POLICY_URL_PORTUGUESE;
            case SYSTEM_SPANISH:
                return POLICY_URL_SPANISH;
            case SYSTEM_SWEDISH:
                return POLICY_URL_SWEDISH;
            case SYSTEM_RUSSIAN:
                return POLICY_URL_RUSSIAN;
            case SYSTEM_CHINESE:
                if (isScriptChineseTraditional()) {
                    return POLICY_URL_CHINESE_TRADITIONAL;
                } else {
                    return POLICY_URL_CHINESE_SIMPLE;
                }
            case SYSTEM_INDONESIAN:
                return POLICY_URL_INDONESIAN;
            case SYSTEM_JAPANESE:
                return POLICY_URL_JAPANESE;
            case SYSTEM_FILIPINO:
                return POLICY_URL_FILIPINO;
            case SYSTEM_NORWEGIAN:
                return POLICY_URL_NORWEGIAN;
            default:
                return POLICY_URL_ENGLISH;
        }
    }

    private static boolean isScriptChineseTraditional() {
        String script = Locale.getDefault().getScript();
        return script.equals(CHINESE_TRADITIONAL_SCRIPT_LANGUAGE_CODE);
    }
}

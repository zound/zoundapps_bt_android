package com.zoundindustries.marshallbt.model.device.state.volume;

import com.zoundindustries.marshallbt.model.device.state.BaseDeviceState;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

/**
 * Class extending {@link BaseDeviceState} used to set and monitor volume feature
 * It will be automatically added to all devices with defined FeaturesDefs.VOLUME feature.
 */
public class VolumeState extends BaseDeviceState<Integer> {

    /**
     * Constructor for VolumeState. Contains required feature definition.
     */
    public VolumeState() {
        super(FeaturesDefs.VOLUME);
    }
}

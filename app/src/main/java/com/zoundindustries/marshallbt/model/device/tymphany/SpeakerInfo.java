package com.zoundindustries.marshallbt.model.device.tymphany;

import androidx.annotation.NonNull;

import java.util.Objects;

/**
 * Base class for information about speaker
 */
public class SpeakerInfo {

    private String deviceName;
    private String macAddress;
    private String modelName;
    private String firmwareVersion;

    /**
     * Default constructor for {@link SpeakerInfo}
     */
    public SpeakerInfo() {
        this("", "", "", "");
    }

    /**
     * Constructor to the set all of the values about speaker
     *
     * @param deviceName      the displayed name of the device
     * @param macAddress      the MAC address
     * @param modelName       the name of the device model
     * @param firmwareVersion the current version of the firmware on device
     */
    public SpeakerInfo(@NonNull String deviceName,
                       @NonNull String macAddress,
                       @NonNull String modelName,
                       @NonNull String firmwareVersion) {
        this.deviceName = deviceName;
        this.macAddress = macAddress;
        this.modelName = modelName;
        this.firmwareVersion = firmwareVersion;
    }

    @NonNull
    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(@NonNull String deviceName) {
        this.deviceName = deviceName;
    }

    @NonNull
    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(@NonNull String macAddress) {
        this.macAddress = macAddress;
    }

    @NonNull
    public String getModelName() {
        return modelName;
    }

    public void setModelName(@NonNull String modelName) {
        this.modelName = modelName;
    }

    @NonNull
    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(@NonNull String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SpeakerInfo that = (SpeakerInfo) o;
        return Objects.equals(deviceName, that.deviceName) &&
                Objects.equals(macAddress, that.macAddress) &&
                Objects.equals(modelName, that.modelName) &&
                Objects.equals(firmwareVersion, that.firmwareVersion);
    }

    @Override
    public int hashCode() {

        return Objects.hash(deviceName, macAddress, modelName, firmwareVersion);
    }

    public String toString() {
        return modelName + " " + deviceName + "\nmac: " + macAddress + "\nfw: " + firmwareVersion;
    }
}

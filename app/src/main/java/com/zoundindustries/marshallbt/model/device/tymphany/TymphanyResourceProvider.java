package com.zoundindustries.marshallbt.model.device.tymphany;

import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice.DeviceSubType;
import com.zoundindustries.marshallbt.services.DeviceService.DeviceImageType;

/**
 * Class used for providing resources related to Joplin speakers
 */
public class TymphanyResourceProvider {

    /**
     * Used for providing correct image based on {@link DeviceImageType} which is used to decide
     * about size of image that is needed
     *
     * @param speaker       {@link TymphanyDevice}
     * @param imageType     {@link DeviceImageType}
     * @return image resource ID
     */
    public static int getImageResourceId(@NonNull BaseDevice speaker,
                                         @NonNull DeviceImageType imageType) {
        switch (((TymphanyDevice)speaker).getDeviceSubType()) {
            case OZZY:
                return getOzzyImageResourceId(imageType);
            case JOPLIN_L:
            case JOPLIN_M:
            case JOPLIN_S:
            case JOPLIN_L_LITE:
            case JOPLIN_M_LITE:
            case JOPLIN_S_LITE:
                return getJoplinImageResourceId(speaker, imageType);
            default:
                return 0;
        }
    }

    private static int getJoplinImageResourceId(@NonNull BaseDevice speaker,
                                               @NonNull DeviceImageType imageType) {
        DeviceSubType deviceSubType = ((TymphanyDevice) speaker).getDeviceSubType();
        DeviceInfo.DeviceColor deviceColor = speaker.getDeviceInfo().getDeviceColor();
        switch (imageType) {
            case SMALL:
                return getSmallJoplinImageId(deviceSubType, deviceColor);
            case MEDIUM:
                return getMediumJoplinImageId(deviceSubType, deviceColor);
            case LARGE:
            case ONBOARDING:
                return getLargeJoplinImageId(deviceSubType, deviceColor);
            default:
                return 0;
        }
    }

    private static int getSmallJoplinImageId(@NonNull DeviceSubType type,
                                             @NonNull DeviceInfo.DeviceColor deviceColor) {
        switch (type) {
            case JOPLIN_S_LITE:
            case JOPLIN_S:
                switch (deviceColor) {
                    case BLACK:
                        return R.drawable.acton_ii_black_small;
                    case WHITE:
                        return R.drawable.acton_ii_white_small;
                    case BROWN:
                        return R.drawable.acton_ii_black_small;
                }
            case JOPLIN_M_LITE:
            case JOPLIN_M:
                switch (deviceColor) {
                    case BLACK:
                        return R.drawable.stanmore_ii_black_small;
                    case WHITE:
                        return R.drawable.stanmore_ii_white_small;
                    case BROWN:
                        return R.drawable.stanmore_ii_black_small;
                }
            case JOPLIN_L_LITE:
            case JOPLIN_L:
                switch (deviceColor) {
                    case BLACK:
                        return R.drawable.woburn_ii_black_small;
                    case WHITE:
                        return R.drawable.woburn_ii_white_small;
                    case BROWN:
                        return R.drawable.woburn_ii_black_small;
                }
        }
        return 0;
    }

    private static int getMediumJoplinImageId(@NonNull DeviceSubType type,
                                              @NonNull DeviceInfo.DeviceColor deviceColor) {
        switch (type) {
            case JOPLIN_S:
            case JOPLIN_S_LITE:
                switch (deviceColor) {
                    case BLACK:
                        return R.drawable.acton_ii_black_medium;
                    case WHITE:
                        return R.drawable.acton_ii_white_medium;
                    case BROWN:
                        return R.drawable.acton_ii_black_medium;
                }
            case JOPLIN_M:
            case JOPLIN_M_LITE:
                switch (deviceColor) {
                    case BLACK:
                        return R.drawable.stanmore_ii_black_medium;
                    case WHITE:
                        return R.drawable.stanmore_ii_white_medium;
                    case BROWN:
                        return R.drawable.stanmore_ii_black_medium;
                }
            case JOPLIN_L:
            case JOPLIN_L_LITE:
                switch (deviceColor) {
                    case BLACK:
                        return R.drawable.woburn_ii_black_medium;
                    case WHITE:
                        return R.drawable.woburn_ii_white_medium;
                    case BROWN:
                        return R.drawable.woburn_ii_black_medium;
                }
        }
        return 0;
    }

    private static int getLargeJoplinImageId(@NonNull DeviceSubType type,
                                             @NonNull DeviceInfo.DeviceColor deviceColor) {
        switch (type) {
            case JOPLIN_S:
            case JOPLIN_S_LITE:
                switch (deviceColor) {
                    case BLACK:
                        return R.drawable.acton_ii_black_large;
                    case WHITE:
                        return R.drawable.acton_ii_white_large;
                    case BROWN:
                        return R.drawable.acton_ii_black_large;
                }
            case JOPLIN_M:
            case JOPLIN_M_LITE:
                switch (deviceColor) {
                    case BLACK:
                        return R.drawable.stanmore_ii_black_large;
                    case WHITE:
                        return R.drawable.stanmore_ii_white_large;
                    case BROWN:
                        return R.drawable.stanmore_ii_black_large;
                }
            case JOPLIN_L:
            case JOPLIN_L_LITE:
                switch (deviceColor) {
                    case BLACK:
                        return R.drawable.woburn_ii_black_large;
                    case WHITE:
                        return R.drawable.woburn_ii_white_large;
                    case BROWN:
                        return R.drawable.woburn_ii_black_large;
                }
        }
        return 0;
    }

    private static int getOzzyImageResourceId(@NonNull DeviceImageType imageType) {
        switch (imageType) {
            case SMALL:
                return R.drawable.ozzy_small;
            case MEDIUM:
                return R.drawable.ozzy_medium;
            case LARGE:
                //TODO: Return large when one is available
                return R.drawable.ozzy_medium;
            case ONBOARDING:
                return R.drawable.pairing_successful_image;
            default:
                return 0;
        }
    }
}

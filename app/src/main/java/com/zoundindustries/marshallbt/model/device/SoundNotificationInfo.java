package com.zoundindustries.marshallbt.model.device;

/**
 * Wrapper class to keep Power sound notification state and Media Control sound notification state.
 */
public class SoundNotificationInfo {

    private boolean powerSoundEnabled;
    private boolean mediaControlSoundEnabled;

    //TODO: perhaps these settings should be separated; there will be more, some are device-specifc
    public SoundNotificationInfo(boolean powerSoundEnabled, boolean mediaControlSoundEnabled) {
        this.powerSoundEnabled = powerSoundEnabled;
        this.mediaControlSoundEnabled = mediaControlSoundEnabled;
    }

    public boolean isPowerSoundEnabled() {
        return powerSoundEnabled;
    }

    public void setPowerSoundEnabled(boolean powerSoundEnabled) {
        this.powerSoundEnabled = powerSoundEnabled;
    }

    public boolean isMediaControlSoundEnabled() {
        return mediaControlSoundEnabled;
    }

    public void setMediaControlSoundEnabled(boolean mediaControlSoundEnabled) {
        this.mediaControlSoundEnabled = mediaControlSoundEnabled;
    }
}

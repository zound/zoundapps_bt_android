package com.zoundindustries.marshallbt.model.discovery.adapter.joplin;

import com.tym.tymappplatform.TAProtocol.TAProtocol.TASystem;
import com.tym.tymappplatform.TAService.TASystemService.TASystemServiceObserver;

/**
 * Interface wrapping {@link TASystemServiceObserver} to simplify usage of the delegates.
 */
public interface ITymphanySystemServiceListener extends TASystemServiceObserver {

    @Override
    default void didDiscoverSystem(TASystem taSystem, int i, byte[] bytes) {
        //nop
    }

    @Override
    default void didConnectionStateChanged(TASystem taSystem, int i) {
        //nop
    }

    @Override
    default void didUpdatePresetName(TASystem taSystem, int i, String s) {
        //nop
    }

    @Override
    default void didUpdateBrightness(TASystem taSystem, int i) {
        //nop
    }

    @Override
    default void didUpdateBatteryLevel(TASystem taSystem, int i) {
        //nop
    }

    @Override
    default void didUpdatePowerStatus(TASystem taSystem, int i) {
        //nop
    }

    @Override
    default void didUpdateModelNumber(TASystem taSystem, String s) {
        //nop
    }

    @Override
    default void didUpdateSerialNumber(TASystem taSystem, String s) {
        //nop
    }

    @Override
    default void didUpdateFirmwareRevision(TASystem taSystem, String s) {
        //nop
    }

    @Override
    default void didUpdateDeviceIdentifierCustomData(TASystem taSystem, byte[] bytes) {
        //nop
    }

    @Override
    default void didUpdatePowerCableConnectStatus(TASystem taSystem, int i) {
        //nop
    }

    @Override
    default void didUpdateCurrentTimeStatus(TASystem taSystem, int[] ints) {
        //nop
    }

    @Override
    default void didUpdateAlarmStatus(TASystem taSystem, int[] ints) {
        //nop
    }

    @Override
    default void didUpdateDeviceInformation(TASystem taSystem,
                                            byte[] bytes,
                                            String s,
                                            byte[] bytes1) {
        //nop
    }

    @Override
    default void didUpdateAutoOffTimerStatus(TASystem taSystem, int i) {
        //nop
    }

    @Override
    default void didUpdatePairingStatus(TASystem taSystem, int i) {
        //nop
    }
}

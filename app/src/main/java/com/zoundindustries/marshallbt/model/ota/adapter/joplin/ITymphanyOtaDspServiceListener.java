package com.zoundindustries.marshallbt.model.ota.adapter.joplin;

import com.tym.tymappplatform.TAProtocol.TAProtocol.TASystem;
import com.tym.tymappplatform.TAService.TAOtaSignalDataProcessingService.TAOtaSignalDataProcessingServiceObserver;
import com.tym.tymappplatform.utils.TACommonDefinitions;
import com.tym.tymappplatform.utils.TAErrorDescription;

/**
 * Interface providing empty default methods from ota dsp service observer
 */
public interface ITymphanyOtaDspServiceListener extends TAOtaSignalDataProcessingServiceObserver {

    @Override
    default void didUpdateOtaStatusMessage(TASystem taSystem,
                                           TACommonDefinitions.OTAStatusType otaStatusType,
                                           float v,
                                           TAErrorDescription taErrorDescription) {
        //nop
    }

    @Override
    default void didCheckOtaStatusMessage(TASystem taSystem,
                                          TACommonDefinitions.OTAStatusType otaStatusType,
                                          String s,
                                          TAErrorDescription taErrorDescription) {
        //nop
    }
}

package com.zoundindustries.marshallbt.model.devicesettings

import com.zoundindustries.marshallbt.utils.EqPresetType

/**
 * Class representing the tymphany tabbed EQ feature.
 *
 * We couldn't reach a generic approach of SDK, so the feature is tied to TymSDK
 */
class EQExtendedData(var tabType: EQTabType,
                     var presetTypeM2: EqPresetType,
                     var presetTypeM3: EqPresetType)

package com.zoundindustries.marshallbt.model.devicesettings;

/**
 * Interface used to pass eq seekBar values from eqGroup to EQViewModel
 */
public interface IEqProgressChangeListener {
    /**
     * This should be invoked when seekbar progress value is changed.
     * @param type type of EQ parameter that is changed
     * @param value value of progress
     * @param fromUser indicates if the call was made by user, or comes from device
     */
    void onProgressChanged(EQData.ValueType type, int value, boolean fromUser);
}

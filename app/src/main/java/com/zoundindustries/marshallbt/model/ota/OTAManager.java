package com.zoundindustries.marshallbt.model.ota;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zoundindustries.marshallbt.model.BaseManager;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.ota.pojo.FirmwareFileMetaData;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Helper class for handling OTA implementation and synchronization.
 */
public class OTAManager extends BaseManager implements IOTAService {


    /**
     * Constructor for OTAManager.
     */
    public OTAManager(Context context) {
        super(context);
    }

    @Nullable
    @Override
    public Observable<Double> getFirmwareUpdateProgress(@NonNull String deviceId) {
        if (isBound) {
            BaseDevice device = deviceService.getDeviceFromId(deviceId);
            if (device != null) {
                return deviceService.getFirmwareUpdateProgress(device);
            }
        }
        return null;
    }

    @Nullable
    @Override
    public Observable<IOTAService.UpdateState> getFirmwareUpdateState(@NonNull String deviceId) {
        if (isBound) {
            BaseDevice device = deviceService.getDeviceFromId(deviceId);
            if (device != null) {
                return deviceService.getFirmwareUpdateState(device);
            }
        }
        return null;
    }

    @Nullable
    @Override
    public Observable<Boolean> getFirmwareUpdateAvailability(@NonNull BaseDevice device) {
        return deviceService.getFirmwareUpdateAvailability(device);
    }

    @Nullable
    @Override
    public Observable<Response<FirmwareFileMetaData>>
    checkForFirmwareUpdate(@NonNull String deviceId, boolean isForced) {
        if (isBound) {
            return deviceService.checkForFirmwareUpdate(deviceId, isForced);
        }
        return null;
    }

    @Override
    public void startOTAUpdate(@NonNull String deviceId,
                               @NonNull FirmwareFileMetaData metaData) {
        if (isBound) {
            BaseDevice device = deviceService.getDeviceFromId(deviceId);
            if (device != null) {
                deviceService.startOTAUpdate(device, metaData);
            }
        }
    }

    @Override
    public boolean isOTAInProgress() {
        return deviceService.isOTAInProgress();
    }

    @Override
    public void finalizeForcedOTA() {
        deviceService.finalizeForcedOTA();
    }
}

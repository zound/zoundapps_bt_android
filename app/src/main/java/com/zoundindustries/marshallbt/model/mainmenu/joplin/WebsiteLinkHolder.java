package com.zoundindustries.marshallbt.model.mainmenu.joplin;

public abstract class WebsiteLinkHolder {
    public static final String MAIN_MENU_CONTACT_SUPPORT_URL =
            "https://www.marshallheadphones.com/marshall-bluetooth-app-contact";

    public static final String MAIN_MENU_CONTACT_GOTO_WEBSITE_URL =
            "https://www.marshallheadphones.com";

    public static final String FOSS_WEBSITE_URL =
            "https://www.marshallheadphones.com/E40";

    public static final String MARSHALL_USER_MANUAL_URL_PREFIX =
            "https://deyu07r2qbkt7.cloudfront.net/marshall-bluetooth-online-output/";

    public static final String MARSHALL_USER_MANUAL_URL_ACTON_POSTFIX = "/acton-ii/index.html";

    public static final String MARSHALL_USER_MANUAL_URL_STANMORE_POSTFIX = "/stanmore-ii/index.html";

    public static final String MARSHALL_USER_MANUAL_URL_WOBURN_POSTFIX = "/woburn-ii/index.html";

    public static final String MARSHALL_USER_MANUAL_URL_MONITOR_II_POSTFIX = "/monitor-ii-anc/index.html";

    public static final String MARSHALL_SHOP_URL = "http://www.marshallheadphones.com";

}

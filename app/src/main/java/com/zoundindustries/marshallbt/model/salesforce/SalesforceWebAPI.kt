package com.zoundindustries.marshallbt.model.salesforce

import io.reactivex.Observable
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

interface SalesforceWebAPI {

    /**
     * Get access token
     */
    @POST("/services/oauth2/token")
    fun getAccessToken(@Query("grant_type") grantType: String,
                       @Query("client_id") clientId: String,
                       @Query("client_secret") clientSecret: String,
                       @Query("username") username: String,
                       @Query("password") password: String)
            : Observable<Response<SalesforceAccessToken>>

    /**
     * Check subscription status
     */
    @GET("/services/data/v44.0/sobjects/Contact/customerID__c/{customerId}")
    fun checkAddress(@Path("customerId") customerId: String,
                     @Header("Authorization") accessToken: String )
            : Observable<Response<SalesforceCustomerData>>

    /**
     * Create customer account
     */
    @POST("/services/data/v44.0/sobjects/Account/")
    fun createAccount(@Header("Authorization") accessToken: String,
                      @Header("Content-Type") contentType: String,
                      @Body params: RequestBody)
            : Observable<Response<SalesforceCustomerCreationStatus>>

    /**
     * Update account
     */
    @PATCH("/services/data/v44.0/sobjects/Account/{customerId}")
    fun updateAccount(@Path("customerId") customerId: String,
                      @Header("Authorization") accessToken: String,
                      @Header("Content-Type") contentType: String,
                      @Body params: RequestBody)
            : Observable<Response<ResponseBody>>



}
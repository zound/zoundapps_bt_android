package com.zoundindustries.marshallbt.model.mainmenu;

import android.content.res.Resources;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.annotations.NonNull;

public class MainMenuItemManager {
    private Resources resources;
    private List<MainMenuItem> mainMenuItemList;

    public MainMenuItemManager(@NonNull Resources resources) {
        this.resources = resources;
        mainMenuItemList = new ArrayList<>();
    }

    @NonNull
    public List<MainMenuItem> getMainMenuItemList(ViewType mainMenuFragmentViewType) {
        setCorrectItemsBasedOnMenuId(mainMenuFragmentViewType);
        return mainMenuItemList;
    }

    private void setCorrectItemsBasedOnMenuId(ViewType mainMenuFragmentViewType) {
        mainMenuItemList.clear();
        switch (mainMenuFragmentViewType) {
            case MAIN_MENU_SETTINGS:
                mainMenuItemList.add(new MainMenuItem(
                        resources.getString(R.string.main_menu_item_email_subscription),
                        MainMenuItem.MenuItemType.EMAIL_SUBSCRIPTION));
                mainMenuItemList.add(new MainMenuItem(
                        resources.getString(R.string.main_menu_item_analytics),
                        MainMenuItem.MenuItemType.ANALYTICS));
                break;
            case MAIN_MENU_HELP:
                mainMenuItemList.add(new MainMenuItem(
                        resources.getString(R.string.main_menu_item_quick_guide),
                        MainMenuItem.MenuItemType.QUICK_GUIDE));
                mainMenuItemList.add(new MainMenuItem(
                        resources.getString(R.string.main_menu_item_online_manual),
                        MainMenuItem.MenuItemType.ONLINE_MANUAL));
                mainMenuItemList.add(new MainMenuItem(
                        resources.getString(R.string.main_menu_item_contact),
                        MainMenuItem.MenuItemType.CONTACT));
                break;
            case MAIN_MENU_ABOUT:
                mainMenuItemList.add(new MainMenuItem(
                        resources.getString(R.string.main_menu_item_terms_and_conditions),
                        MainMenuItem.MenuItemType.END_USER_LICENSE_AGREEMENT));
                mainMenuItemList.add(new MainMenuItem(resources
                        .getString(R.string.main_menu_item_foss),
                        MainMenuItem.MenuItemType.FREE_AND_OPEN_SOURCE_SOFTWARE));
                break;
            case MAIN_MENU_QUICK_GUIDE_LIST:
                mainMenuItemList.add(new MainMenuItem(
                        resources.getString(R.string.main_menu_item_quick_guide_item_1),
                        MainMenuItem.MenuItemType.QUICK_GUIDE_ACTON_II));
                mainMenuItemList.add(new MainMenuItem(
                        resources.getString(R.string.main_menu_item_quick_guide_item_2),
                        MainMenuItem.MenuItemType.QUICK_GUIDE_STANMORE_II));
                mainMenuItemList.add(new MainMenuItem(
                        resources.getString(R.string.main_menu_item_quick_guide_item_3),
                        MainMenuItem.MenuItemType.QUICK_GUIDE_WOBURN_II));
                mainMenuItemList.add(new MainMenuItem(
                        resources.getString(R.string.main_menu_item_quick_guide_item_4),
                        MainMenuItem.MenuItemType.QUICK_GUIDE_MONITOR_II));
                break;
            case MAIN_MENU_QUICK_GUIDE_JOPLIN:
                mainMenuItemList.add(new MainMenuItem(
                        resources.getString(R.string.main_menu_item_bluetooth_pairing),
                        MainMenuItem.MenuItemType.BLUETOOTH_PAIRING_JOPLIN));
                mainMenuItemList.add(new MainMenuItem(
                        resources.getString(R.string.main_menu_item_couple_speakers),
                        MainMenuItem.MenuItemType.COUPLE_SPEAKERS));
                mainMenuItemList.add(new MainMenuItem(
                        resources.getString(R.string.main_menu_item_play_pause_button),
                        MainMenuItem.MenuItemType.PLAY_PAUSE_BUTTON));
                break;
            case MAIN_MENU_QUICK_GUIDE_OZZY:
                mainMenuItemList.add(new MainMenuItem(
                        resources.getString(R.string.main_menu_item_bluetooth_pairing),
                        MainMenuItem.MenuItemType.BLUETOOTH_PAIRING_OZZY));
                mainMenuItemList.add(new MainMenuItem(
                        resources.getString(R.string.main_menu_item_anc_button),
                        MainMenuItem.MenuItemType.ANC));
                mainMenuItemList.add(new MainMenuItem(
                        resources.getString(R.string.main_menu_item_m_button),
                        MainMenuItem.MenuItemType.M_BUTTON));
                mainMenuItemList.add(new MainMenuItem(
                        resources.getString(R.string.main_menu_item_control_knob),
                        MainMenuItem.MenuItemType.CONTROL_KNOB));
                break;
            case MAIN_MENU_ONLINE_MANUAL:
                mainMenuItemList.add(new MainMenuItem(
                        resources.getString(R.string.device_model_acton),
                        MainMenuItem.MenuItemType.ONLINE_MANUAL_ACTON));
                mainMenuItemList.add(new MainMenuItem(
                        resources.getString(R.string.device_model_stanmore),
                        MainMenuItem.MenuItemType.ONLINE_MANUAL_STANMORE));
                mainMenuItemList.add(new MainMenuItem(
                        resources.getString(R.string.device_model_woburn),
                        MainMenuItem.MenuItemType.ONLINE_MANUAL_WOBURN));
                mainMenuItemList.add(new MainMenuItem(
                        resources.getString(R.string.device_model_monitor_ii),
                        MainMenuItem.MenuItemType.ONLINE_MANUAL_MONITOR_II));
                break;
        }
    }
}

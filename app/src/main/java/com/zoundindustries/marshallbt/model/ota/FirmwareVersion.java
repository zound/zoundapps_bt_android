package com.zoundindustries.marshallbt.model.ota;

import androidx.annotation.NonNull;

/**
 * Class wrapping parameters of the firmware update.
 */
public class FirmwareVersion {
    private String brand;
    private String model;
    private String version;

    /**
     * Constructor for FirmwareVersion.
     *
     * @param builder used to create FirmwareVersion.
     */
    public FirmwareVersion(@NonNull Builder builder) {
        this.brand = builder.brand;
        this.model = builder.model;
        this.version = builder.version;
    }

    /**
     * Get value for brand property of firmware file.
     *
     * @return brand value.
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Get value for model property of firmware file.
     *
     * @return model value.
     */
    public String getModel() {
        return model;
    }

    /**
     * Get value for version property of firmware file.
     *
     * @return version value.
     */
    public String getVersion() {
        return version;
    }

    /**
     * Builder pattern class for {@link FirmwareVersion}.
     */
    public static class Builder {
        private String brand;
        private String model;
        private String version;

        /**
         * Setter for brand value.
         *
         * @param brand new value.
         * @return reference to Builder.
         */
        public Builder setBrand(String brand) {
            this.brand = brand;
            return this;
        }

        /**
         * Setter for model value.
         *
         * @param model new value.
         * @return reference to Builder.
         */
        public Builder setModel(String model) {
            this.model = model;
            return this;
        }

        /**
         * Setter for version value.
         *
         * @param version new value.
         * @return reference to Builder.
         */
        public Builder setVersion(String version) {
            this.version = version;
            return this;
        }

        /**
         * Creates {@link FirmwareVersion} instance for set values.
         *
         * @return instance of {@link FirmwareVersion}.
         */
        public FirmwareVersion create() {
            return new FirmwareVersion(this);
        }
    }
}

package com.zoundindustries.marshallbt.model.device.tymphany;

import androidx.annotation.NonNull;

import com.tym.tymappplatform.TAProtocol.TAProtocol.TASystem;
import com.tym.tymappplatform.TAService.TADigitalSignalProcessingService.TADigitalSignalProcessingService;
import com.tym.tymappplatform.TAService.TAPlayControlService.TAPlayControlService;
import com.tym.tymappplatform.TAService.TASystemService.TASystemService;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

/**
 * Small model of Joplin speaker
 */
public class SmallJoplinSpeaker extends TymphanyDevice {

    private static final int SMALL_JOPLIN_FEATURES = JOPLIN_FEATURES |
            FeaturesDefs.BLUETOOTH_SOURCE |
            FeaturesDefs.AUX_SOURCE;

    /**
     * Constructor for {@link SmallJoplinSpeaker}. Requires device basic parameters and features.
     *
     * @param deviceInfo unique identification of the device.
     */
    public SmallJoplinSpeaker(@NonNull DeviceInfo deviceInfo,
                              @NonNull TASystemService taSystemService,
                              @NonNull TAPlayControlService taPlayControlService,
                              @NonNull TADigitalSignalProcessingService taDspService,
                              @NonNull TASystem taSystem) {
        super(deviceInfo, taSystemService, taPlayControlService, taDspService,
                DeviceSubType.JOPLIN_S, taSystem, SMALL_JOPLIN_FEATURES);
    }
}

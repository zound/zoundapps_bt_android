package com.zoundindustries.marshallbt.model.device.state.dsp;

import com.tym.tymappplatform.TAProtocol.TAProtocol.TASystem;
import com.tym.tymappplatform.TAService.TADigitalSignalProcessingService.TADigitalSignalProcessingServiceObserver;
import com.tym.tymappplatform.utils.TACommonDefinitions;
import com.tym.tymappplatform.utils.TAPropertyType;

import java.util.HashMap;

/**
 * Interface providing empty default methods from dsp service observer
 */
public interface ITymphanyDspServiceListener extends TADigitalSignalProcessingServiceObserver {

    @Override
    default void didUpdateEqualizer(TASystem taSystem,
                                    HashMap<TAPropertyType,
                                            Object> hashMap) {
        //NOP
    }

    @Override
    default void didUpdateEqualiser(TASystem taSystem,
                                    int[] ints) {
        //NOP
    }

    @Override
    default void didUpdateEQPresets(TASystem taSystem,
                            TACommonDefinitions.EQStepsType eqStepsType,
                            TACommonDefinitions.EQSettingsType eqSettingsType,
                            TACommonDefinitions.EQSettingsType eqSettingsType1) {
        //NOP
    }
}

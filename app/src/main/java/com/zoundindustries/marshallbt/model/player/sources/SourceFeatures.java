package com.zoundindustries.marshallbt.model.player.sources;

/**
 * Class wrapping the availability of sources
 */
public class SourceFeatures {

    private boolean isBluetoothSupported;
    private boolean isRcaSupported;
    private boolean isAuxSupported;

    public SourceFeatures(boolean isBluetoothSupported, boolean isRcaSupported,
                          boolean isAuxSupported) {
        this.isBluetoothSupported = isBluetoothSupported;
        this.isRcaSupported = isRcaSupported;
        this.isAuxSupported = isAuxSupported;
    }

    public boolean isBluetoothSupported() {
        return isBluetoothSupported;
    }

    public boolean isRcaSupported() {
        return isRcaSupported;
    }

    public boolean isAuxSupported() {
        return isAuxSupported;
    }

    public boolean isOnlyBluetoothSupported() {
        return isBluetoothSupported && !isAuxSupported && !isRcaSupported;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SourceFeatures that = (SourceFeatures) o;

        if (isBluetoothSupported != that.isBluetoothSupported) return false;
        if (isRcaSupported != that.isRcaSupported) return false;
        return isAuxSupported == that.isAuxSupported;
    }

    @Override
    public int hashCode() {
        int result = (isBluetoothSupported ? 1 : 0);
        result = 31 * result + (isRcaSupported ? 1 : 0);
        result = 31 * result + (isAuxSupported ? 1 : 0);
        return result;
    }
}

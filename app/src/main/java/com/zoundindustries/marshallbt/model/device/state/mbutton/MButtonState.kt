package com.zoundindustries.marshallbt.model.device.state.mbutton

import com.zoundindustries.marshallbt.model.device.state.BaseDeviceState
import com.zoundindustries.marshallbt.model.devicesettings.MButtonSettingsItem
import com.zoundindustries.marshallbt.utils.FeaturesDefs

/**
 * Class extending {@link BaseDeviceState} used to monitor current M-Button action type setting.
 */
class MButtonState : BaseDeviceState<MButtonSettingsItem.MButtonActionType>(FeaturesDefs.M_BUTTON)
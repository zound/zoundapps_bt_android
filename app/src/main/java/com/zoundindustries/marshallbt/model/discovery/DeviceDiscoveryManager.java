package com.zoundindustries.marshallbt.model.discovery;

import android.content.Context;
import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.model.BaseManager;
import com.zoundindustries.marshallbt.model.device.BaseDevice;

import io.reactivex.Observable;

/**
 * Class grouping discovery functionality. Responsible for getting all scan devices in the system.
 */
public class DeviceDiscoveryManager extends BaseManager implements IDiscoveryService {

    /**
     * Constructor for DeviceDiscoveryManager.
     *
     * @param context needed to communicate with the devices in the system.
     */
    public DeviceDiscoveryManager(@NonNull Context context) {
        super(context);
    }

    @Override
    public void startScanForDevices() {
        deviceService.startScanForDevices();
    }

    @Override
    public void stopScanForDevices() {
        deviceService.stopScanForDevices();
    }

    @NonNull
    @Override
    public Observable<BaseDevice> getDiscoveredDevices() {
        return deviceService.getDiscoveredDevices();
    }

    @NonNull
    @Override
    public Observable<BaseDevice> getDisappearedDevices() {
        return deviceService.getDisappearedDevices();
    }

    /**
     * Returns flag indicating if there is ongoing device scan in the system.
     *
     * @return scanning state.
     */
    public boolean isDeviceScanInProgress() {
        return deviceService.isDeviceScanInProgress();
    }
}

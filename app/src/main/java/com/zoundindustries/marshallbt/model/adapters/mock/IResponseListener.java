package com.zoundindustries.marshallbt.model.adapters.mock;

import androidx.annotation.NonNull;

/**
 * Interface simulating single command generic response
 */
public interface IResponseListener {

    /**
     * Simulated synchronous response
     *
     * @param state indicating success(true) and failed (false)
     */
    void onResponseReceived(@NonNull Boolean state);
}

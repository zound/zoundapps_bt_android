package com.zoundindustries.marshallbt.model.ota;

import com.zoundindustries.marshallbt.model.ota.pojo.FirmwareFileMetaData;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * Interface for Retrofit connections
 */
public interface OtaApi {

    @GET("prod/firmware/{brand}/{model}/{version}")
    Observable<Response<FirmwareFileMetaData>> getDownloadURL(@Header("x-api-key") String key,
                                                              @Path("brand") String brand,
                                                              @Path("model") String model,
                                                              @Path("version") String version);

    @GET
    @Streaming
    Observable<Response<ResponseBody>> downloadOtaFile(@Header("x-api-key") String key,
                                                       @Url String url);
}

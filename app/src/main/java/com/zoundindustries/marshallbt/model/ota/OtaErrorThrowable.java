package com.zoundindustries.marshallbt.model.ota;

import androidx.annotation.NonNull;

/**
 * Class used to handle specific cases of OTA errors
 */
public class OtaErrorThrowable extends Throwable {

    /**
     * Contains types of errors that could occur during OTA. Every type has its pre-defined message.
     */
    public enum ErrorType {
        NULL_SERVER_RESPONSE
                ("OTA error: null server response", GeneralErrorType.DOWNLOAD_ERROR),
        NULL_WEB_DATA
                ("OTA error: null web data", GeneralErrorType.DOWNLOAD_ERROR),
        UNDEFINED_DOWNLOAD
                ("OTA error: not defined download", GeneralErrorType.DOWNLOAD_ERROR),
        SDK_FLASHING_ERROR
                ("OTA error: sdk returned error status", GeneralErrorType.FLASHING_ERROR),
        UNDEFINED_FLASHING
                ("OTA error: not defined flashing", GeneralErrorType.FLASHING_ERROR),
        UNDEFINED
                ("OTA error: not defined", GeneralErrorType.UNDEFINED);

        private final GeneralErrorType generalErrorType;
        private final String message;

        /**
         * Constructor for error type enum, defining a message and error type
         *
         * @param message          message of error that is sent
         * @param generalErrorType general type of error
         */
        ErrorType(@NonNull String message, @NonNull GeneralErrorType generalErrorType) {
            this.message = message;
            this.generalErrorType = generalErrorType;
        }

        public String getMessage() {
            return message;
        }

        public GeneralErrorType getGeneralErrorType() {
            return generalErrorType;
        }
    }

    public enum GeneralErrorType {
        DOWNLOAD_ERROR,
        FLASHING_ERROR,
        UNDEFINED
    }

    private ErrorType errorType;

    /**
     * Constructor used when the error type is known.
     *
     * @param errorType type of error that occurred
     */
    public OtaErrorThrowable(ErrorType errorType) {
        super(errorType.getMessage());
        this.errorType = errorType;
    }

    /**
     * Used when the error is not defined or when we want to change the message
     *
     * @param errorType type of error that occurred
     * @param message   message to be sent
     */
    public OtaErrorThrowable(ErrorType errorType, String message) {
        super(message);
        this.errorType = errorType;
    }

    public ErrorType getErrorType() {
        return errorType;
    }
}

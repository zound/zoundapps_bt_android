package com.zoundindustries.marshallbt.model.device.state;

import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.utils.FeaturesDefs;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Class defining base behaviour of device state. It requires feature to be initialized for all new
 * device states types. This is generic type wrapper for accessing input and output values of the
 * device.
 */

public abstract class BaseDeviceState<T> {

    private final BehaviorSubject<T> valueSubject = BehaviorSubject.create();
    private int stateType;

    /**
     * Constructor for BaseDeviceState.
     *
     * @param stateType defines feature that is needed for extending state. Use features from
     *                  {@link FeaturesDefs}
     */
    public BaseDeviceState(int stateType) {
        this.stateType = stateType;
    }

    /**
     * Getter for observable on state value. Passed to errorViewModel classes for states monitoring.
     *
     * @return observable on value,
     */
    @NonNull
    public Observable<T> getObservable() {
        return valueSubject;
    }

    /**
     * Get current value synchronously.
     *
     * @return current state value.
     */
    public T getValue() {
        return valueSubject.getValue();
    }

    /**
     * Set value for the state. Value change will be emitted via subject observable.
     *
     * @param value new value.
     */
    public void setValue(@NonNull T value) {
        valueSubject.onNext(value);
    }

    /**
     * Gets state type that is one of the features defined in {@link FeaturesDefs}
     *
     * @return state type feature required for state
     */
    public int getStateType() {
        return stateType;
    }
}

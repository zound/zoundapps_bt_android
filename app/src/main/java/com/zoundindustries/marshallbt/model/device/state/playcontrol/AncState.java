package com.zoundindustries.marshallbt.model.device.state.playcontrol;

import com.zoundindustries.marshallbt.model.device.state.BaseDeviceState;
import com.zoundindustries.marshallbt.model.devicesettings.AncData;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

public class AncState extends BaseDeviceState<AncData> {
    /**
     * Constructor for BaseDeviceState.
     */
    public AncState() {
        super(FeaturesDefs.ANC);
    }
}

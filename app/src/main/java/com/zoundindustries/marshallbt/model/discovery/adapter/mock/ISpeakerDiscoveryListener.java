package com.zoundindustries.marshallbt.model.discovery.adapter.mock;

import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.model.device.BaseDevice;

import java.util.List;

/**
 * Interface simulating discovery of new {@link BaseDevice}
 */
public interface ISpeakerDiscoveryListener {

    /**
     * Simulated scan for devices response
     *
     * @param devices new devices found while scanning
     */
    void onSpeakersDiscovered(@NonNull List<BaseDevice> devices);
}

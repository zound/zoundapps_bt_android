package com.zoundindustries.marshallbt.model.ota;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.zoundindustries.marshallbt.model.ota.pojo.SpeakerInfoCache;
import com.zoundindustries.marshallbt.model.ota.pojo.SpeakerInfoCacheHolder;
import com.zoundindustries.marshallbt.services.DeviceService;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

/**
 * Class provides possibility to read/write information related to Speakers that should be saved
 * inside app storage and retrieved later in time.
 * Speakers macAddress is the base for every operation related to adding/getting {@link SpeakerInfoCache}
 */
public final class SpeakerCacheManager {

    private static final String SPEAKER_INFO_FILE = "speakerInfoFile.json";

    private Gson gson;
    private Context context;
    private SpeakerInfoCacheHolder speakerInfoCacheHolder;

    /**
     * Constructor which should be only used inside {@link DeviceService}
     */
    public SpeakerCacheManager(@NonNull Context context) {
        this.context = context;
        gson = new Gson();

        InputStream inputStream = getFileFromCache();
        if (inputStream != null) {
            speakerInfoCacheHolder = parseDataFromJson(inputStream);
        } else {
            speakerInfoCacheHolder = new SpeakerInfoCacheHolder();
        }
    }

    /**
     * Method used for adding new {@link SpeakerInfoCache} to storage.
     *
     * @param macAddress of speaker that should be added to storage
     * @return true in case when adding had succeed, otherwise false
     */
    public boolean addSpeakerInfoCache(@NonNull String macAddress) {
        SpeakerInfoCache speakerInfoCache = new SpeakerInfoCache(macAddress);

        if (isAlreadyCached(speakerInfoCache)) {
            return false;
        }
        speakerInfoCacheHolder.getSpeakerInfoCacheList().add(speakerInfoCache);

        return saveSpeakerInfoCacheToFile();
    }

    /**
     * Method used for removing {@link SpeakerInfoCache} from storage.
     *
     * @param macAddress of speaker that should be removed from storage
     * @return true in case when removing had succeed, otherwise false
     */
    public boolean removeSpeakerInfoCache(@NonNull String macAddress) {
        for (SpeakerInfoCache cache : speakerInfoCacheHolder.getSpeakerInfoCacheList()) {
            if (cache.getMacAddress().equals(macAddress)) {
                speakerInfoCacheHolder.getSpeakerInfoCacheList().remove(cache);
                return saveSpeakerInfoCacheToFile();
            }
        }
        return false;
    }

    /**
     * Method used for retrieving {@link SpeakerInfoCache} from storage based on MAC Address
     *
     * @param macAddress of speaker
     * @return {@link SpeakerInfoCache} in case when speaker exist otherwise null
     */
    @Nullable
    public SpeakerInfoCache getSpeakerInfoCache(@NonNull String macAddress) {
        return speakerInfoCacheHolder.getSpeakerInfoCache(macAddress);
    }

    /**
     * Method used for updating stored {@link SpeakerInfoCache} information
     *
     * @param macAddress        of updated Speaker
     * @param isTwsCoupled      status of speaker
     * @param twsChannelType    might be null
     * @param twsConnectionInfo might be null
     */
    public void updateTwsStatusOfSpeaker(@NonNull String macAddress,
                                         boolean isTwsCoupled,
                                         @Nullable String twsChannelType,
                                         @Nullable String twsConnectionInfo) {
        SpeakerInfoCache speakerInfoCache = speakerInfoCacheHolder.getSpeakerInfoCache(macAddress);
        if (speakerInfoCache != null) {
            speakerInfoCache.setIsTwsCoupled(isTwsCoupled);
            speakerInfoCache.setTwsChannelType(twsChannelType);
            speakerInfoCache.setTwsConnectionInfo(twsConnectionInfo);
            saveSpeakerInfoCacheToFile();
        }
    }

    /**
     * Method updating isConfigured information in device cache. Not yet configured devices will
     * invoke additional UI onboarding for the user.
     *
     * @param macAddress   of the device to be updated
     * @param isConfigured new value of the flag
     */
    public void updateIsConfigured(@NonNull String macAddress,
                                   boolean isConfigured) {
        SpeakerInfoCache speakerInfoCache = speakerInfoCacheHolder.getSpeakerInfoCache(macAddress);
        if (speakerInfoCache == null) {
            addSpeakerInfoCache(macAddress);
            updateIsConfigured(macAddress, isConfigured);
        } else {
            speakerInfoCache.setConfigured(isConfigured);
            saveSpeakerInfoCacheToFile();
        }
    }

    /**
     * Method used for retrieving boolean based on if {@link SpeakerInfoCache} exist
     *
     * @param macAddress of speaker
     * @return true in case when {@link SpeakerInfoCache}  exist otherwise false
     */
    public boolean isSpeakerInfoCached(@NonNull String macAddress) {
        return speakerInfoCacheHolder.getSpeakerInfoCache(macAddress) != null;
    }

    private boolean saveSpeakerInfoCacheToFile() {
        String json = serializeCacheToGSON();
        return saveJsonToFile(json);
    }

    @NonNull
    private String serializeCacheToGSON() {
        return gson.toJson(speakerInfoCacheHolder);
    }

    private boolean isAlreadyCached(@NonNull SpeakerInfoCache speakerInfoCache) {
        for (SpeakerInfoCache cache : speakerInfoCacheHolder.getSpeakerInfoCacheList()) {
            if (cache.getMacAddress().equals(speakerInfoCache.getMacAddress())) {
                return true;
            }
        }
        return false;
    }

    @Nullable
    private InputStream getFileFromCache() {
        try {
            return context.openFileInput(SPEAKER_INFO_FILE);
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    @NonNull
    private SpeakerInfoCacheHolder parseDataFromJson(@NonNull InputStream inputStream) {
        SpeakerInfoCacheHolder speakerInfoCacheHolder = new SpeakerInfoCacheHolder();
        Reader json = new InputStreamReader(inputStream);
        JsonParser parser = new JsonParser();
        JsonArray jsonArray = parser.parse(json).getAsJsonObject()
                .getAsJsonArray(SpeakerInfoCache.class.getSimpleName());

        for (JsonElement jsonElement : jsonArray) {
            SpeakerInfoCache speakerInfoCache = gson.fromJson(jsonElement, SpeakerInfoCache.class);
            speakerInfoCacheHolder.getSpeakerInfoCacheList().add(speakerInfoCache);
        }
        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return speakerInfoCacheHolder;
    }

    private boolean saveJsonToFile(@NonNull String json) {
        try {
            FileOutputStream output = context.openFileOutput(SPEAKER_INFO_FILE, Context.MODE_PRIVATE);
            output.write(json.getBytes(), 0, json.length());
            output.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @RestrictTo(RestrictTo.Scope.TESTS)
    public List<SpeakerInfoCache> getSpeakerInfoCacheList() {
        return speakerInfoCacheHolder.getSpeakerInfoCacheList();

    }

    @RestrictTo(RestrictTo.Scope.TESTS)
    public SpeakerInfoCacheHolder parseDataTestPurpose(@NonNull InputStream inputStream) {
        return parseDataFromJson(inputStream);
    }
}

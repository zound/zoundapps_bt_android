package com.zoundindustries.marshallbt.model.device.state;

import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.tym.tymappplatform.TAProtocol.TAProtocol.TAProtocol;
import com.tym.tymappplatform.TAProtocol.TAProtocol.TASystem;
import com.tym.tymappplatform.TAService.TADigitalSignalProcessingService.TADigitalSignalProcessingService;
import com.tym.tymappplatform.TAService.TAPlayControlService.TAPlayControlService;
import com.tym.tymappplatform.TAService.TASystemService.TASystemService;
import com.tym.tymappplatform.utils.SongTrackInfo;
import com.tym.tymappplatform.utils.TACommonDefinitions;
import com.tym.tymappplatform.utils.TACommonDefinitions.CustomButtonActionType;
import com.zoundindustries.marshallbt.model.coupling.DeviceCouplingInfo;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.BaseDevice.ConnectionState;
import com.zoundindustries.marshallbt.model.device.BaseDevice.CouplingChannelType;
import com.zoundindustries.marshallbt.model.device.BaseDevice.CouplingConnectionState;
import com.zoundindustries.marshallbt.model.device.SoundNotificationInfo;
import com.zoundindustries.marshallbt.model.device.state.dsp.ITymphanyDspServiceListener;
import com.zoundindustries.marshallbt.model.device.state.playcontrol.ITymphanyPlayControlServiceListener;
import com.zoundindustries.marshallbt.model.device.tymphany.ReconnectionScanController;
import com.zoundindustries.marshallbt.model.device.tymphany.SpeakerInfo;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice;
import com.zoundindustries.marshallbt.model.devicesettings.AncData;
import com.zoundindustries.marshallbt.model.devicesettings.AncMode;
import com.zoundindustries.marshallbt.model.devicesettings.EQData;
import com.zoundindustries.marshallbt.model.devicesettings.EQExtendedData;
import com.zoundindustries.marshallbt.model.devicesettings.EQTabType;
import com.zoundindustries.marshallbt.model.devicesettings.MButtonSettingsItem;
import com.zoundindustries.marshallbt.model.discovery.adapter.joplin.ITymphanySystemServiceListener;
import com.zoundindustries.marshallbt.model.player.sources.SongInfo;
import com.zoundindustries.marshallbt.utils.EqPresetType;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

import java.util.Arrays;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.BehaviorSubject;

import static com.zoundindustries.marshallbt.utils.joplin.ConversionUtils.bytes2HexString;
import static com.zoundindustries.marshallbt.utils.joplin.ConversionUtils.getBytesFromMac;

/**
 * Class implementing states specific for the Joplin device.
 * This is main access point for all device properties of Joplin type.
 */
@SuppressWarnings("unchecked")
public class TymphanyDeviceStateController extends BaseDeviceStateController implements
        BaseDeviceStateController.Inputs,
        BaseDeviceStateController.Outputs,
        ITymphanySystemServiceListener,
        ITymphanyPlayControlServiceListener,
        ITymphanyDspServiceListener {

    private final static String TAG = TymphanyDeviceStateController.class.getSimpleName();

    //TODO: check the target value for connection
    private static final int DEFAULT_CONNECTION_DELAY = 0;

    private static final int SCALING_FACTOR = 10;
    private static final int CONNECTION_TIMEOUT = 60000;
    private static final int PAIRING_MODE_DELAY = 2000;
    private static final int PAIRING_MODE_ON = 2;
    private static final int PAIRING_MODE_OFF = 3;

    private final int FW_BRIGHTNESS_MIN = 35;
    private final int FW_BRIGHTNESS_MAX = 70;
    private final int APP_BRIGHTNESS_MIN = 0;
    private final int APP_BRIGHTNESS_MAX = 100;

    private final TASystemService taSystemService;
    private final TAPlayControlService taPlayControlService;
    private final TADigitalSignalProcessingService taDspService;
    private final SoundNotificationInfo soundNotificationInfo;
    private final ReconnectionScanController reconnectionScanController;

    private TASystem taSystem;
    private TymphanyDevice.DeviceSubType deviceSubType;
    private CountDownTimer connectionTimer;
    private SpeakerInfo speakerInfo;
    private int[] eqValues;
    private Handler handler;

    private BehaviorSubject<String> deviceName = BehaviorSubject.create();
    private BehaviorSubject<String> macAddress = BehaviorSubject.create();
    private BehaviorSubject<String> modelName = BehaviorSubject.create();
    private BehaviorSubject<String> firmwareVersion = BehaviorSubject.create();
    private BehaviorSubject<String> coupleId = BehaviorSubject.create();

    private Disposable speakerInfoDisposable;
    private Disposable connectionDisposable;
    private Disposable reconnectionDisposable;

    /**
     * Constructor for {@link TymphanyDeviceStateController}.
     *
     * @param features supported by the device.
     */
    public TymphanyDeviceStateController(int features,
                                         @NonNull TymphanyDevice.DeviceSubType deviceSubType,
                                         @NonNull TASystemService taSystemService,
                                         @NonNull TAPlayControlService taPlayControlService,
                                         @NonNull TADigitalSignalProcessingService taDspService,
                                         @NonNull TASystem taSystem) {
        this(features,
                deviceSubType,
                taSystemService,
                taPlayControlService,
                taDspService,
                taSystem,
                new Handler(Looper.getMainLooper()));
    }

    @VisibleForTesting
    public TymphanyDeviceStateController(int features,
                                         @NonNull TymphanyDevice.DeviceSubType deviceSubType,
                                         @NonNull TASystemService taSystemService,
                                         @NonNull TAPlayControlService taPlayControlService,
                                         @NonNull TADigitalSignalProcessingService taDspService,
                                         @NonNull TASystem taSystem,
                                         @NonNull Handler handler) {
        super(features);
        this.taSystemService = taSystemService;
        this.taPlayControlService = taPlayControlService;
        this.taDspService = taDspService;
        this.taSystem = taSystem;
        this.deviceSubType = deviceSubType;

        initConnectionTimeout();
        reconnectionScanController = new ReconnectionScanController(taSystemService, taSystem);
        speakerInfo = new SpeakerInfo();
        eqValues = new int[5];
        soundNotificationInfo = new SoundNotificationInfo(false, false);
        this.handler = handler;

        taSystemService.registerObserver(TymphanyDeviceStateController.this);
        taPlayControlService.registerObserver(TymphanyDeviceStateController.this);
        taDspService.registerObserver(TymphanyDeviceStateController.this);

        initPreConnectionStateValues();
    }


    // -------------------------------------------------------------------------------------
    // ------ ------ ------ ------            OUTPUTS           ------ ------ ------  ------

    @NonNull
    @Override
    public Observable<Integer> getVolume() {
        return getStateFromFeature(FeaturesDefs.VOLUME).getObservable();
    }

    @NonNull
    @Override
    public Observable<Integer> getBatteryLevel() {
        return getStateFromFeature(FeaturesDefs.BATTERY_LEVEL).getObservable();
    }

    @NonNull
    @Override
    public Observable<Boolean> getPairingModeStatus() {
        return getStateFromFeature(FeaturesDefs.PAIRING_MODE_STATUS).getObservable();
    }

    @NonNull
    @Override
    public Observable<Boolean> getPlaying() {
        return getStateFromFeature(FeaturesDefs.PLAY_CONTROL).getObservable();
    }

    @NonNull
    @Override
    public Observable<ConnectionState> getConnectionInfo() {
        return getStateFromFeature(FeaturesDefs.CONNECTION_INFO).getObservable();
    }

    @NonNull
    @Override
    public ConnectionState getConnectionInfoCurrentValue() {
        return (ConnectionState) getStateFromFeature(FeaturesDefs.CONNECTION_INFO)
                .getValue();
    }

    @NonNull
    @Override
    public Observable<DeviceCouplingInfo> getCouplingConnectionInfo() {
        return getStateFromFeature(FeaturesDefs.COUPLING_CONNECTION).getObservable();
    }

    @NonNull
    @Override
    public DeviceCouplingInfo getCouplingConnectionInfoCurrentValue() {
        return (DeviceCouplingInfo) getStateFromFeature(FeaturesDefs.COUPLING_CONNECTION)
                .getValue();
    }

    @NonNull
    @Override
    public Observable<CouplingChannelType> getCouplingChannelInfo() {
        return getStateFromFeature(FeaturesDefs.COUPLING_CHANNEL).getObservable();
    }

    @NonNull
    @Override
    public Observable<String> getCoupleId() {
        return coupleId;
    }

    @NonNull
    @Override
    public Observable<SoundNotificationInfo> getSoundNotificationInfo() {
        return getStateFromFeature(FeaturesDefs.SOUNDS_SETTINGS).getObservable();
    }

    @NonNull
    @Override
    public Observable<EQData> getEQ() {
        return getStateFromFeature(FeaturesDefs.EQ).getObservable();
    }

    @NonNull
    @Override
    public Observable<EQExtendedData> getEQPreset() {
        return getStateFromFeature(FeaturesDefs.EQ_EXTENDED).getObservable();
    }

    // ------------- anc --------------
    @Override
    public Observable<AncData> getAncData() {
        return getStateFromFeature(FeaturesDefs.ANC).getObservable();
    }

    // ------------ timer -------------
    @Override
    public Observable<Integer> getTimerValue() {
        return getStateFromFeature(FeaturesDefs.TIMER_OFF).getObservable();
    }

    @NonNull
    @Override
    public Observable<SpeakerInfo> getSpeakerInfo() {
        return getStateFromFeature(FeaturesDefs.SPEAKER_INFO).getObservable();
    }

    @NonNull
    @Override
    public SpeakerInfo getSpeakerInfoCurrentValue() {
        return (SpeakerInfo) getStateFromFeature(FeaturesDefs.SPEAKER_INFO).getValue();
    }

    @NonNull
    @Override
    public Observable<Integer> getBrightness() {
        return getStateFromFeature(FeaturesDefs.LIGHT).getObservable();
    }

    @Override
    public Observable<Boolean> isSourceActive(BaseDevice.SourceType sourceType) {

        int feature = 0;
        switch (sourceType) {
            case BLUETOOTH:
                feature = FeaturesDefs.BLUETOOTH_SOURCE;
                break;
            case AUX:
                feature = FeaturesDefs.AUX_SOURCE;
                break;
            case RCA:
                feature = FeaturesDefs.RCA_SOURCE;
                break;
        }

        return getStateFromFeature(feature).getObservable();
    }

    @NonNull
    @Override
    public boolean isAuxConfigurable() {
        return isFeatureSupported(FeaturesDefs.AUX_SOURCE_CONFIGURABLE);
    }

    @NonNull
    @Override
    public Observable<SongInfo> getSongInfo() {
        return getStateFromFeature(FeaturesDefs.SONG_INFO).getObservable();
    }

    @NonNull
    @Override
    public Observable<MButtonSettingsItem.MButtonActionType> getMButtonActionType() {
        return getStateFromFeature(FeaturesDefs.M_BUTTON).getObservable();
    }

    // -------------------------------------------------------------------------------------
    // ------ ------ ------ ------            INPUTS           ------ ------ ------  ------

    @Override
    public void setVolume(int value) {
        Log.d(TAG, "Set volume: " + value);
        taPlayControlService.writeVolume(taSystem, value);
        getStateFromFeature(FeaturesDefs.VOLUME).setValue(value);
    }

    @Override
    public void setPlaying(boolean playing) {
        Log.d(TAG, "Set playing: " + playing);
        if (playing) {
            taPlayControlService.play(taSystem);
            readPlaySongTrackInfo();
        } else {
            taPlayControlService.pause(taSystem);
        }
    }

    @Override
    public void setConnectionInfo(ConnectionState state) {
        Log.d(TAG, "Set connected: " + state + "for: " + taSystem.deviceName);
        if (state == ConnectionState.CONNECTED) {
            checkCurrentConnectionInfo();
        } else if (state == ConnectionState.DISCONNECTED) {
            taSystemService.disconnectToSystem(taSystem);
            getStateFromFeature(FeaturesDefs.CONNECTION_INFO).setValue(ConnectionState.DISCONNECTED);
        } else if (state == ConnectionState.TWS_CONNECTED) {
            if (getStateFromFeature(FeaturesDefs.CONNECTION_INFO).getValue() !=
                    ConnectionState.TWS_CONNECTED) {
                taSystemService.disconnectToSystem(taSystem);
                getStateFromFeature(FeaturesDefs.CONNECTION_INFO).setValue(state);
            }
        } else {
            getStateFromFeature(FeaturesDefs.CONNECTION_INFO).setValue(state);
        }
    }

    @Override
    public void setEQData(@NonNull EQData preset) {
        Log.d(TAG, "setEQData: " + Arrays.toString(preset.toIntArray()));
        int[] newValues = scaleDownPresetValues(preset.toIntArray());

        if (shouldUpdateEqValues(newValues)) {
            taDspService.writeEqualizerData(taSystem, newValues);
            eqValues = newValues;
            getStateFromFeature(FeaturesDefs.EQ).setValue(preset);
        }
    }

    @Override
    public void setEQPreset(EQTabType tabType, EqPresetType preset) {
        Log.d(TAG, "setEQPreset: " + preset.name() + " for tab: " + tabType.name() +
                " for system: " + taSystem.deviceName);
        taDspService.writeEQSettingsData(taSystem, tabType.toStepsType(), preset.toSettingsType());
    }

    @Override
    public void setEQTabType(EQTabType tabType) {
        Log.d(TAG, "setEQTabType: " + tabType.name() + " for system: " + taSystem.deviceName);
        taDspService.writeEQStepsData(taSystem, tabType.toStepsType());
    }

    // ------------- anc --------------

    @Override
    public void readAncStatus() {
        taPlayControlService.readANCStatus(taSystem);
    }

    @Override
    public void writeAncMode(AncMode ancMode) {
        if (unwrapAncMode(ancMode) != null) {
            taPlayControlService.writeANCMode(taSystem, unwrapAncMode(ancMode));
        }
    }

    @Override
    public void writeAncLevel(int level) {
        Log.d(TAG, "ANC - write anc level: " + level + " for  " + taSystem.deviceName);
        taPlayControlService.writeANCLevel(taSystem, level);
    }

    @Override
    public void writeMonitorLevel(int level) {
        Log.d(TAG, "ANC - write monitor level: " + level + " for  " + taSystem.deviceName);
        taPlayControlService.writeMonitorLevel(taSystem, level);
    }

    private TACommonDefinitions.ANCMode unwrapAncMode(AncMode ancMode) {
        switch (ancMode) {
            case ON:
                return TACommonDefinitions.ANCMode.ANC;
            case MONITOR:
                return TACommonDefinitions.ANCMode.Monitor;
            case OFF:
            default: // to hush the compiler
                return TACommonDefinitions.ANCMode.PBO;
        }
    }

    private AncMode wrapAncMode(TACommonDefinitions.ANCMode ancMode) {
        switch (ancMode) {
            case ANC:
                return AncMode.ON;
            case Monitor:
                return AncMode.MONITOR;
            case PBO:
            default: // to hush the compiler
                return AncMode.OFF;
        }
    }

    @Override
    public void didReadANC(TASystem taSystem, TACommonDefinitions.ANCMode ancMode, int i, int i1) {
        handler.post(() -> {
            if (isCurrentTaSystem(taSystem)) {
                Log.d(TAG, "didReadANC: " + wrapAncMode(ancMode) + "with values: "
                        + i + "anc/" + i1 + "mon for device " + taSystem.deviceName);
                if (isFeatureSupported(FeaturesDefs.ANC)) {
                    getStateFromFeature(FeaturesDefs.ANC).setValue(new AncData(wrapAncMode(ancMode), i, i1));
                }
            }
        });
    }

    // ------- timer ------

    @Override
    public void readAutoOffTimer() {
        taSystemService.readAutoOffTimer(taSystem);
    }

    @Override
    public void writeTimerValue(int value) {
        taSystemService.writeAutoOffTimer(taSystem, value);
    }

    @Override
    public void didUpdateAutoOffTimerStatus(TASystem taSystem, int i) {
        if (isCurrentTaSystem(taSystem)) {
            if (isFeatureSupported(FeaturesDefs.TIMER_OFF))
                getStateFromFeature(FeaturesDefs.TIMER_OFF).setValue(i);
        }
    }

    @Override
    public void setSpeakerInfo(SpeakerInfo speakerInfo) {
        Log.d(TAG, "Set speaker info: " + speakerInfo);
        if (!this.speakerInfo.getDeviceName().equals(speakerInfo.getDeviceName())) {
            deviceName.onNext(speakerInfo.getDeviceName());
            taSystemService.renameDevice(taSystem, speakerInfo.getDeviceName());
        }
    }

    @Override
    public void setBrightness(int brightness) {
        Log.d(TAG, "Set brightness: " + brightness);
        getStateFromFeature(FeaturesDefs.LIGHT).setValue(brightness);
        taSystemService.setBrightness(taSystem, getNormalizedBrightness(brightness,
                APP_BRIGHTNESS_MIN, APP_BRIGHTNESS_MAX, FW_BRIGHTNESS_MIN,
                FW_BRIGHTNESS_MAX));
    }

    @Override
    public void setAudioSource(BaseDevice.SourceType sourceType) {

        // how should we initialise this? null? or keep it from being null
        TACommonDefinitions.AudioSourceType taSourceType = null;

        switch (sourceType) {
            case BLUETOOTH:
                taSourceType = TACommonDefinitions.AudioSourceType.Bluetooth;
                break;
            case AUX:
                taSourceType = TACommonDefinitions.AudioSourceType.Aux;
                break;
            case RCA:
                taSourceType = TACommonDefinitions.AudioSourceType.RCA;
                break;
        }

        taPlayControlService.switchAudioSourceType(taSystem, taSourceType);
    }

    @Override
    public void setCouplingMasterConnected(@NonNull CouplingChannelType channelInfoState,
                                           @NonNull String MAC) {
        taPlayControlService.trueWirelessMACPairing(taSystem, getBytesFromMac(MAC));
        taPlayControlService.trueWirelessChannelSwitch(taSystem,
                getSdkChannelType(channelInfoState));
    }

    @Override
    public void setCouplingDisconnected() {
        taPlayControlService.trueWirelessDisconnect(taSystem);
    }

    @Override
    public void startPairingMode() {
        handler.postDelayed(() ->
                taSystemService.writeBlueToothStartPairing(taSystem), PAIRING_MODE_DELAY);
    }

    public void setSoundNotification(SoundNotificationInfo soundNotification) {
        Log.d(TAG, "setSoundNotification: power: " + soundNotification.isPowerSoundEnabled() +
                " media: " + soundNotification.isMediaControlSoundEnabled() + " for: " +
                taSystem.deviceName);
        //TODO: write if corresponding feature supported
        taPlayControlService.writeDevicePowerSounds(taSystem,
                soundNotification.isPowerSoundEnabled());
        if(isFeatureSupported(FeaturesDefs.SOUNDS_SETTINGS_MEDIA_CONTROL)) {
            taPlayControlService.writeDeviceMediaSounds(taSystem,
                    soundNotification.isMediaControlSoundEnabled());
        }
    }

    @Override
    public void setMButtonActionType(MButtonSettingsItem.MButtonActionType type) {
        getStateFromFeature(FeaturesDefs.M_BUTTON).setValue(type);
        taPlayControlService.writeCustomButtonConfig(taSystem, unwrapMButtonActionType(type));
    }

    private CustomButtonActionType unwrapMButtonActionType(MButtonSettingsItem.MButtonActionType type) {
        switch (type) {
            case NATIVE_ASSISTANT:
                return CustomButtonActionType.VoiceControlActivation;
            case EQUALIZER:
                return CustomButtonActionType.EQControl;
            case GOOGLE_ASSISTANT:
                return CustomButtonActionType.GoogleVoiceAssistant;
            default:
                return CustomButtonActionType.None;
        }
    }

    private MButtonSettingsItem.MButtonActionType wrapMButtonActionType(CustomButtonActionType type) {
        switch (type) {
            case VoiceControlActivation:
                return MButtonSettingsItem.MButtonActionType.NATIVE_ASSISTANT;
            case EQControl:
                return MButtonSettingsItem.MButtonActionType.EQUALIZER;
            case GoogleVoiceAssistant:
                return MButtonSettingsItem.MButtonActionType.GOOGLE_ASSISTANT;
            default:
                return MButtonSettingsItem.MButtonActionType.NONE;
        }
    }

    // ------ current state readers ------

    @Override
    public void readEQData() {
        taDspService.readEqualizerData(taSystem);
    }

    @Override
    public void readEQPreset() {
        taDspService.readEQPresetData(taSystem);
    }

    @Override
    public void readAudioStatus() {
        taPlayControlService.readAudioStatus(taSystem);
    }

    @Override
    public void readCouplingConnectionStatus() {
        taPlayControlService.readTrueWirelessStatus(taSystem);
    }

    @Override
    public void readSoundInfoStatus() {
        readSoundNotificationInfo();
    }

    @Override
    public void playNextSong() {
        taPlayControlService.next(taSystem);
    }

    @Override
    public void playPreviousSong() {
        taPlayControlService.previous(taSystem);
    }

    @Override
    public void readVolume() {
        taPlayControlService.readVolume(taSystem);
    }

    @Override
    public void readBatteryLevel() {
        taSystemService.readBatteryLevel(taSystem);
    }

    @Override
    public void readPairingMode() {
        taSystemService.readPairingModeStatus(taSystem);
    }

    @Override
    public void readMButtonActionType() {
        taPlayControlService.readCustomButtonConfig(taSystem);
    }

    // ------ listener overrides ------

    @Override
    public void didConnectionStateChanged(TASystem taSystem, int i) {
        handler.post(() -> {
            if (isCurrentTaSystem(taSystem)) {
                Log.d(TAG, "didConnectionStateChanged: " + i + " for system: " +
                        taSystem.deviceName);
                connectionTimer.cancel();
                if (i == TAProtocol.kDeviceStatusConnected &&
                        getStateFromFeature(FeaturesDefs.CONNECTION_INFO).getValue() !=
                                ConnectionState.CONNECTED) {
                    this.taSystem = taSystem;
                    handleConnectedState(taSystem);
                } else if (i == TAProtocol.kDeviceStatusDisconnected &&
                        getStateFromFeature(FeaturesDefs.CONNECTION_INFO).getValue() !=
                                ConnectionState.DISCONNECTED &&
                        getStateFromFeature(FeaturesDefs.CONNECTION_INFO).getValue() !=
                                ConnectionState.TWS_CONNECTED) {

                    handleDisconnectedState(taSystem);
                } else if (i == TAProtocol.kDeviceStatusTimeout) {
                    taSystemService.connectToSystem(taSystem);
                    setConnectionInfo(ConnectionState.CONNECTING);
                    connectionTimer.start();
                }
            }
        });
    }

    @Override
    public void didUpdateVolume(TASystem taSystem, int i) {
        handler.post(() -> {
            if (isCurrentTaSystem(taSystem)) {
                Log.d(TAG, "didUpdateVolume: " + i);
                getStateFromFeature(FeaturesDefs.VOLUME).setValue(i);
            }
        });
    }

    @Override
    public void didUpdateBatteryLevel(TASystem taSystem, int i) {
        handler.post(() -> {
            if (isCurrentTaSystem(taSystem)) {
                Log.d(TAG, "didUpdateBattery: " + i);
                getStateFromFeature(FeaturesDefs.BATTERY_LEVEL).setValue(i);
            }
        });
    }

    @Override
    public void didUpdateAudioStatus(TASystem taSystem,
                                     TACommonDefinitions.AudioSourceType audioSourceType,
                                     TACommonDefinitions.PlayBackStatusType playBackStatusType,
                                     int i,
                                     boolean power,
                                     boolean media) {
        handler.post(() -> {
            if (isCurrentTaSystem(taSystem)) {
                switch (playBackStatusType) {
                    case Playing:
                        Log.d(TAG, "didUpdateAudioStatus: Playing");
                        getStateFromFeature(FeaturesDefs.PLAY_CONTROL).setValue(true);
                        readPlaySongTrackInfo();
                        break;
                    case Stop:
                    case Pause:
                        Log.d(TAG, "didUpdateAudioStatus: Stopped");
                        getStateFromFeature(FeaturesDefs.PLAY_CONTROL).setValue(false);
                        break;
                }

                setSoundNotificationInfoState(power, media);
                setSourceTypeStates(audioSourceType);
            }
        });
    }

    @Override
    public void didUpdatePlaySongTrackInfo(TASystem taSystem, SongTrackInfo songTrackInfo) {
        handler.post(() -> {
            if (isCurrentTaSystem(taSystem)) {
                //TODO should we get it in milliseconds or seconds?
                int durationInSeconds = TextUtils.isEmpty(songTrackInfo.getAttributePlayTime()) ?
                        0 : Integer.parseInt(songTrackInfo.getAttributePlayTime()) * 1000;

                SongInfo songInfo = new SongInfo.SongInfoBuilder()
                        .setName(songTrackInfo.getAttributeTitle())
                        .setAlbum(songTrackInfo.getAttributeAlbum())
                        .setArtist(songTrackInfo.getAttributeArtist())
                        .setArtworkUrl(null)
                        .setDuration(durationInSeconds)
                        .build();

                getStateFromFeature(FeaturesDefs.SONG_INFO).setValue(songInfo);
            }
        });
    }

    @Override
    public void didUpdateEqualiser(TASystem taSystem, int[] ints) {
        handler.post(() -> {
            if (isCurrentTaSystem(taSystem)) {
                Log.d(TAG, "didUpdateEqualiser: " + Arrays.toString(ints) + " for system: " + taSystem.deviceName);
                getStateFromFeature(FeaturesDefs.EQ).setValue(new EQData(scaleUpPresetValues(ints)));
            }
        });
    }

    @Override
    public void didUpdateEQPresets(TASystem taSystem,
                                   TACommonDefinitions.EQStepsType eqStepsType,
                                   TACommonDefinitions.EQSettingsType eqSettingsType,
                                   TACommonDefinitions.EQSettingsType eqSettingsType1) {
        handler.post(() -> {
            if (isCurrentTaSystem(taSystem)) {
                Log.d(TAG, "didUpdateEQPresets step:" + eqStepsType.name() +
                        " setting1: " + eqSettingsType.name() +
                        " setting2: " + eqSettingsType1.name() +
                        " for system: " +
                        taSystem.deviceName);
                getStateFromFeature(FeaturesDefs.EQ_EXTENDED).setValue(
                        new EQExtendedData(
                                EQTabType.from(eqStepsType),
                                EqPresetType.from(eqSettingsType),
                                EqPresetType.from(eqSettingsType1)));
            }
        });
    }

    @Override
    public void didUpdateModelNumber(TASystem taSystem, String s) {
        handler.post(() -> {
            if (isCurrentTaSystem(taSystem)) {
                Log.d(TAG, "didUpdateModelNumber: " + s);
                modelName.onNext(s);
            }
        });
    }

    @Override
    public void didUpdateFirmwareRevision(TASystem taSystem, String s) {
        handler.post(() -> {
            if (isCurrentTaSystem(taSystem)) {
                Log.d(TAG, "didUpdateFirmwareRevision: " + s);
                firmwareVersion.onNext(s);
            }
        });
    }

    @Override
    public void didUpdateBrightness(TASystem taSystem, int i) {
        handler.post(() -> {
            if (isCurrentTaSystem(taSystem)) {
                Log.d(TAG, "didUpdateBrightness: " + i);
                getStateFromFeature(FeaturesDefs.LIGHT).setValue(getNormalizedBrightness(i,
                        FW_BRIGHTNESS_MIN, FW_BRIGHTNESS_MAX, APP_BRIGHTNESS_MIN,
                        APP_BRIGHTNESS_MAX));
            }
        });
    }

    @Override
    public void didUpdateTrueWireless(TASystem taSystem,
                                      TACommonDefinitions.TWSStatusType twsStatusType,
                                      TACommonDefinitions.ChannelType channelType,
                                      byte[] bytes) {
        handler.post(() -> {
            if (isCurrentTaSystem(taSystem)) {
                handleTwsConnectionStateChanged(twsStatusType, channelType, bytes);
            }
        });
    }

    @Override
    public void didUpdateDeviceInformation(TASystem taSystem,
                                           byte[] bytes,
                                           String s,
                                           byte[] bytes1) {
    }

    @Override
    public void didUpdatePairingStatus(TASystem taSystem, int i) {
        handler.post(() -> {
            if (isCurrentTaSystem(taSystem)) {
                Log.d(TAG, "didUpdatePairingMode: " + i);
                getStateFromFeature(FeaturesDefs.PAIRING_MODE_STATUS)
                        .setValue(i == PAIRING_MODE_ON);
            }
        });
    }

    @Override
    public void wasCustomButtonPressed(TASystem taSystem,
                                       TACommonDefinitions.CustomButtonActionType customButtonActionType) {
        handler.post(() -> {
            if (isCurrentTaSystem(taSystem)) {
                Log.d(TAG, "didUpdateCustomButton: " + customButtonActionType.name() +
                        "for: " + taSystem.deviceName);
                getStateFromFeature(FeaturesDefs.M_BUTTON)
                        .setValue(wrapMButtonActionType(customButtonActionType));
            }
        });

    }

    /**
     * Get current TASystem object. This field can be updated after controller creation.
     *
     * @return current {@link TASystem}
     */
    public TASystem getTaSystem() {
        return taSystem;
    }

    /**
     * Update current TASystem. Needed for SDKs that use randomized addresses.
     *
     * @param system newlu udpated {@link TASystem}
     */
    public void setTaSystem(TASystem system) {
        this.taSystem = system;
    }

    /**
     * Indicates if device reconnection is in progress. Scanning should be triggered by client
     * only when there is no reconnection for any device.
     *
     * @return flag indicating if there is reconnection ongoing.
     */
    public boolean isReconnectionInProgress() {
        return reconnectionScanController.isReconnectInProgress();
    }

    private void handleDisconnectedState(TASystem taSystem) {
        taSystemService.unSubscribeBatteryLevel(taSystem);
        taSystemService.unSubscribePairingStatus(taSystem);

        taDspService.unSubscribeEqualizerData(taSystem);

        taPlayControlService.unSubscribeCustomButtonConfig(taSystem);
        taPlayControlService.unSubscribeAudioStatus(taSystem);
        taPlayControlService.unSubscribePlaySongTrackInfo(taSystem);
        taPlayControlService.unSubscribeVolume(taSystem);
        taPlayControlService.unSubscribeTrueWirelessStatus(taSystem);
        taPlayControlService.unSubscribeANC(taSystem);
        taPlayControlService.unSubscribeCustomButtonConfig(taSystem);

        if (validateTASystem(taSystem)) taSystemService.unregisterSystem(taSystem);
        if (validateTASystem(taSystem)) taSystemService.closeSystem(taSystem);

        getStateFromFeature(FeaturesDefs.CONNECTION_INFO)
                .setValue(ConnectionState.DISCONNECTED);
    }

    private void handleConnectedState(TASystem taSystem) {
        getStateFromFeature(FeaturesDefs.CONNECTION_INFO)
                .setValue(ConnectionState.CONNECTED);
        startMonitorVolume();
        startMonitorPlayingState();
        startMonitorEqualiser();
        startMonitorBrightness();
        startMonitorCouplingConnectionState();
        startMonitorPairingMode();
        startMonitorAnc();
        startMonitorMButton();

        //todo maybe we should check for feature availability before every subscription?
        if(isFeatureSupported(FeaturesDefs.TIMER_OFF)) {
            taSystemService.subscribeAutoOffTimerStatus(taSystem);
        }

        readSoundNotificationInfo();
        readSpeakerInfo(taSystem);
        startMonitorPlaySongTrackInfo();
    }

    private void setSoundNotificationInfoState(boolean power, boolean media) {
        //TODO: there will be ANC and M-Button sounds; together with media they are device features
        soundNotificationInfo.setPowerSoundEnabled(power);
        soundNotificationInfo.setMediaControlSoundEnabled(media);
        getStateFromFeature(FeaturesDefs.SOUNDS_SETTINGS).setValue(soundNotificationInfo);
    }

    private CouplingChannelType getCouplingChannelType(TACommonDefinitions.ChannelType channelType) {
        CouplingChannelType couplingChannelType = CouplingChannelType.PARTY;
        switch (channelType) {
            case Party:
                couplingChannelType = CouplingChannelType.PARTY;
                break;
            case MasterAsLeft:
                couplingChannelType = CouplingChannelType.LEFT;
                break;
            case MasterAsRight:
                couplingChannelType = CouplingChannelType.RIGHT;
                break;
        }
        return couplingChannelType;
    }

    private void handleTwsConnectionStateChanged(@NonNull TACommonDefinitions.TWSStatusType twsStatusType,
                                                 @NonNull TACommonDefinitions.ChannelType channelType,
                                                 @NonNull byte[] bytes) {
        String coupledMac = bytes2HexString(bytes);
        Log.d(TAG, "didUpdateTrueWirelessConnection: " + twsStatusType + " peer MAC: " +
                coupledMac + " for device: " + taSystem.deviceName);

        switch (twsStatusType) {
            case TWSReconnecting:
            case Disconnected:
            case MasterPairing:
            case SlavePairing:
            case MasterMACAddressPairing:
                getStateFromFeature(FeaturesDefs.COUPLING_CONNECTION)
                        .setValue(new DeviceCouplingInfo());
                break;
            case ConnectedAsMaster:
                getStateFromFeature(FeaturesDefs.COUPLING_CONNECTION)
                        .setValue(new DeviceCouplingInfo(getCouplingChannelType(channelType),
                                CouplingConnectionState.CONNECTED_AS_MASTER,
                                coupledMac));
                break;
            case ConnectedAsSlave:
                getStateFromFeature(FeaturesDefs.COUPLING_CONNECTION)
                        .setValue(new DeviceCouplingInfo(getCouplingChannelType(channelType),
                                CouplingConnectionState.CONNECTED_AS_SLAVE,
                                coupledMac));
                break;
        }
    }

    @Override
    public void dispose() {
        taDspService.unregisterObserver(this);
        taPlayControlService.unSubscribeVolume(taSystem);
        taPlayControlService.unSubscribeAudioStatus(taSystem);
        taPlayControlService.unregisterObserver(this);
        taPlayControlService.unSubscribePlaySongTrackInfo(taSystem);
        taSystemService.unregisterObserver(this);
        handler.removeCallbacksAndMessages(null);
        reconnectionScanController.dispose();

        if (speakerInfoDisposable != null) {
            speakerInfoDisposable.dispose();
        }

        if (connectionDisposable != null) {
            connectionDisposable.dispose();
        }

        if (reconnectionDisposable != null) {
            reconnectionDisposable.dispose();
        }
    }

    @Override
    public void reconnect() {
        taSystemService.writeBlueToothStartReconnect(taSystem);
    }

    private boolean validateTASystem(TASystem taSystem) {
        return taSystem != null && taSystem.deviceAddress != null;
    }

    private void disconnectTASystem(TASystem taSystem) {
        if (validateTASystem(taSystem)) taSystemService.disconnectToSystem(taSystem);
        handleDisconnectedState(taSystem);
    }

    private void initConnectionTimeout() {
        connectionTimer = new CountDownTimer(CONNECTION_TIMEOUT, CONNECTION_TIMEOUT) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                disconnectTASystem(taSystem);
                Log.d(TAG, "TymphanyDeviceStateController: Set TIMEOUT");
                getStateFromFeature(FeaturesDefs.CONNECTION_INFO)
                        .setValue(ConnectionState.TIMEOUT);
            }
        };
    }

    //This method is initializing values that must be visualized before device connection.
    //The values are used in home screen before connecting to device.
    private void initPreConnectionStateValues() {
        if (isFeatureSupported(FeaturesDefs.CONNECTION_INFO)) {
            getStateFromFeature(FeaturesDefs.CONNECTION_INFO).setValue(ConnectionState.DISCONNECTED);
        }

        if (isFeatureSupported(FeaturesDefs.PLAY_CONTROL)) {
            getStateFromFeature(FeaturesDefs.PLAY_CONTROL).setValue(false);
        }

        if (isFeatureSupported(FeaturesDefs.SOUNDS_SETTINGS)) {
            getStateFromFeature(FeaturesDefs.SOUNDS_SETTINGS).setValue(soundNotificationInfo);
        }
        if (isFeatureSupported(FeaturesDefs.COUPLING_CONNECTION)) {
            getStateFromFeature(FeaturesDefs.COUPLING_CONNECTION).setValue(new DeviceCouplingInfo());
        }

        if (isFeatureSupported(FeaturesDefs.SPEAKER_INFO)) {
            getStateFromFeature(FeaturesDefs.SPEAKER_INFO).setValue(new SpeakerInfo(taSystem.deviceName,
                    taSystem.deviceAddress, "", ""));
        }

        if (isFeatureSupported(FeaturesDefs.SPEAKER_INFO)) {
            SpeakerInfo speakerInfo = (SpeakerInfo) getStateFromFeature(FeaturesDefs.SPEAKER_INFO).getValue();
            Log.d(TAG, "initPreConnectionStateValues: \n"
                    + "speakerInfo.getDeviceName()" + speakerInfo.getDeviceName()
                    + "\n speakerInfo.getFirmwareVersion()" + speakerInfo.getFirmwareVersion()
                    + "\n speakerInfo.getMacAddress()" + speakerInfo.getMacAddress()
                    + "speakerInfo.getModelName()" + speakerInfo.getModelName());
        }
    }

    private void checkCurrentConnectionInfo() {
        ConnectionState connectionState = (ConnectionState)
                getStateFromFeature(FeaturesDefs.CONNECTION_INFO).getValue();

        if (connectionState != ConnectionState.CONNECTED &&
                connectionState != ConnectionState.CONNECTING) {
            if (isFeatureSupported(FeaturesDefs.AUTO_CONNECT)) {
                getStateFromFeature(FeaturesDefs.CONNECTION_INFO).setValue(ConnectionState.CONNECTING);
                reconnectionScanController.startReconnectionWithScan();
                reconnectionDisposable = reconnectionScanController.getReconnectDeviceFound()
                        .take(1)
                        .subscribe(isFound -> {
                            if (isFound) {
                                taSystemService.connectToSystem(taSystem);
                                connectionTimer.start();
                            } else {
                                //Device should be set as "Not connected" on the main list
                                setConnectionInfo(ConnectionState.DISCONNECTED);
                            }
                        });
            } else {
                taSystemService.connectToSystem(taSystem);
                taSystemService.stopScanSystems();
                getStateFromFeature(FeaturesDefs.CONNECTION_INFO).setValue(ConnectionState.CONNECTING);
                connectionTimer.start();
            }
        }
    }

    private void startMonitorVolume() {
        readVolume();
        taPlayControlService.subscribeVolume(taSystem);
    }

    private void startMonitorPlayingState() {
        taPlayControlService.readPlaybackStatus(taSystem);
        taPlayControlService.subscribeAudioStatus(taSystem);
    }

    private void startMonitorCouplingConnectionState() {
        taPlayControlService.readTrueWirelessStatus(taSystem);
        taPlayControlService.subscribeTrueWirelessStatus(taSystem);
    }

    private void startMonitorEqualiser() {
        taDspService.subscribeEqualizerData(taSystem);
        taDspService.subscribeEQPresetData(taSystem);
    }

    private void startMonitorAnc() {
        taPlayControlService.subscribeANC(taSystem);
    }

    private void startMonitorBrightness() {
        taSystemService.getBrightnessStatus(taSystem);
    }

    private void startMonitorPlaySongTrackInfo() {
        taPlayControlService.subscribePlaySongTrackInfo(taSystem);
    }

    private void startMonitorPairingMode() {
        taSystemService.subscribePairingStatus(taSystem);
    }

    private void startMonitorMButton() {
        taPlayControlService.subscribeCustomButtonConfig(taSystem);
    }


    private void readPlaySongTrackInfo() {
        taPlayControlService.readPlaySongTrackInfo(taSystem);
    }

    private void readSoundNotificationInfo() {
        taPlayControlService.readAudioPowerSoundStatus(taSystem);
        taPlayControlService.readAudioMediaSoundStatus(taSystem);
    }

    private void readSpeakerInfo(TASystem taSystem) {
        taSystemService.readModelName(taSystem);
        taSystemService.readDeviceName(taSystem);
        taSystemService.readFirmwareRevision(taSystem);

        Log.d(TAG, "readSpeakerInfo: \n"
                + "taSystem.deviceName" + taSystem.deviceName
                + "\n taSystem.deviceAddress" + taSystem.deviceAddress);

        //TODO remove this part after we can use the callback for devicename which one is
        // invoked by readDeviceName()
        deviceName.onNext(taSystem.deviceName);
        macAddress.onNext(taSystem.deviceAddress);


        Log.d(TAG, "readSpeakerInfo: \n"
                + "taSystem.deviceName" + taSystem.deviceName
                + "\n taSystem.deviceAddress" + taSystem.deviceAddress);

        initSpeakerInfoObserver();
    }

    private void initSpeakerInfoObserver() {
        speakerInfoDisposable = Observable.combineLatest(
                deviceName,
                macAddress,
                modelName,
                firmwareVersion,
                (deviceName, macAddress, modelName, firmwareVersion) -> {
                    speakerInfo.setDeviceName(deviceName);
                    speakerInfo.setMacAddress(macAddress);
                    speakerInfo.setModelName(modelName);
                    speakerInfo.setFirmwareVersion(firmwareVersion);

                    Log.d(TAG, "initPreConnectionStateValues: \n"
                            + "speakerInfo.getDeviceName()" + speakerInfo.getDeviceName()
                            + "\n speakerInfo.getFirmwareVersion()" + speakerInfo.getFirmwareVersion()
                            + "\n speakerInfo.getMacAddress()" + speakerInfo.getMacAddress()
                            + "speakerInfo.getModelName()" + speakerInfo.getModelName());

                    return speakerInfo;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(speakerInfoResult -> {
                    getStateFromFeature(FeaturesDefs.SPEAKER_INFO).setValue(speakerInfoResult);
                    Log.d(TAG, "initSpeakerInfoObserver: \n"
                            + "speakerInfo.getDeviceName()" + speakerInfoResult.getDeviceName()
                            + "\n speakerInfo.getFirmwareVersion()" + speakerInfoResult.getFirmwareVersion()
                            + "\n speakerInfo.getMacAddress()" + speakerInfoResult.getMacAddress()
                            + "speakerInfo.getModelName()" + speakerInfoResult.getModelName());
                });
    }

    private void setSourceTypeStates(TACommonDefinitions.AudioSourceType audioSourceType) {

        boolean isAuxSource = false;
        boolean isBluetoothSource = false;
        boolean isRcaSource = false;

        switch (audioSourceType) {
            case Aux:
                isAuxSource = true;
                break;
            case Bluetooth:
                isBluetoothSource = true;
                break;
            case RCA:
                isRcaSource = true;
                break;
        }

        setValueIfFeatureSupported(FeaturesDefs.AUX_SOURCE, isAuxSource);
        setValueIfFeatureSupported(FeaturesDefs.BLUETOOTH_SOURCE, isBluetoothSource);
        setValueIfFeatureSupported(FeaturesDefs.RCA_SOURCE, isRcaSource);
    }

    private void setValueIfFeatureSupported(int feature, boolean setValue) {
        if (isFeatureSupported(feature)) {
            getStateFromFeature(feature).setValue(setValue);
        }
    }

    private boolean isCurrentTaSystem(TASystem taSystem) {
        return taSystem.deviceAddress.equals(this.taSystem.deviceAddress);
    }

    private int[] scaleDownPresetValues(int[] values) {
        for (int i = 0; i < values.length; i++) {
            values[i] = values[i] / SCALING_FACTOR;
        }
        return values;
    }

    private int[] scaleUpPresetValues(int[] values) {
        for (int i = 0; i < values.length; i++) {
            values[i] = values[i] * SCALING_FACTOR;
        }
        return values;
    }

    private boolean shouldUpdateEqValues(int[] newValues) {
        for (int i = 0; i < newValues.length; i++) {
            if (newValues[i] != eqValues[i]) {
                return true;
            }
        }
        return false;
    }

    private int getNormalizedBrightness(int actual, int oldMin, int oldMax, int newMin, int newMax) {
        // The firmware return value between 35 and 70, but we need to convert it to between
        // 0 and 100 for the application and vice-versa when we send the value to the firmware.
        return (int) ((float) ((actual - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
    }

    private TACommonDefinitions.ChannelType
    getSdkChannelType(CouplingChannelType type) {
        TACommonDefinitions.ChannelType channelType;
        switch (type) {
            case PARTY:
                channelType = TACommonDefinitions.ChannelType.Party;
                break;
            case LEFT:
                channelType = TACommonDefinitions.ChannelType.MasterAsLeft;
                break;
            case RIGHT:
                channelType = TACommonDefinitions.ChannelType.MasterAsRight;
                break;
            default:
                channelType = TACommonDefinitions.ChannelType.Party;
        }
        return channelType;
    }

}

package com.zoundindustries.marshallbt.model.device.state.dsp;

import com.zoundindustries.marshallbt.model.device.state.BaseDeviceState;
import com.zoundindustries.marshallbt.model.devicesettings.EQData;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

/**
 * Class extending {@link BaseDeviceState} used to set and monitor equaliser feature.
 * It will be automatically added to all devices with defined FeaturesDefs.EQ feature.
 */

public class EQState extends BaseDeviceState<EQData> {
    /**
     * Constructor for EQState. Contains required feature definition.
     */
    public EQState() {
        super(FeaturesDefs.EQ);
    }
}

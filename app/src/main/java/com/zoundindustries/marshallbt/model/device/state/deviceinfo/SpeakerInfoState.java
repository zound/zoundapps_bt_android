package com.zoundindustries.marshallbt.model.device.state.deviceinfo;

import com.zoundindustries.marshallbt.model.device.tymphany.SpeakerInfo;
import com.zoundindustries.marshallbt.model.device.state.BaseDeviceState;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

/**
 * Class extending {@link BaseDeviceState} used to monitor information about device
 */
public class SpeakerInfoState extends BaseDeviceState<SpeakerInfo> {

    /**
     * Constructor for SpeakerInfoState. Contains required feature definition.
     */
    public SpeakerInfoState() {
        super(FeaturesDefs.SPEAKER_INFO);
    }
}

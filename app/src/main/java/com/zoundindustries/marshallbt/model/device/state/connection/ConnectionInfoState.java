package com.zoundindustries.marshallbt.model.device.state.connection;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.state.BaseDeviceState;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

/**
 * Class extending {@link BaseDeviceState} used to set and monitor connection feature
 * It will be automatically added to all devices with defined FeaturesDefs.CONNECTION_INFO feature.
 */
public class ConnectionInfoState extends BaseDeviceState<BaseDevice.ConnectionState> {

    /**
     * Constructor for ConnectionInfoState. Contains required feature definition.
     */
    public ConnectionInfoState() {
        super(FeaturesDefs.CONNECTION_INFO);
    }
}

package com.zoundindustries.marshallbt.model.device.tymphany;

import androidx.annotation.NonNull;

import com.tym.tymappplatform.TAProtocol.TAProtocol.TASystem;
import com.tym.tymappplatform.TAService.TADigitalSignalProcessingService.TADigitalSignalProcessingService;
import com.tym.tymappplatform.TAService.TAPlayControlService.TAPlayControlService;
import com.tym.tymappplatform.TAService.TASystemService.TASystemService;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.DeviceInfo.DeviceGroup;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice.DeviceSubType;

/**
 * Factory class to create the specific Joplin speaker class.
 */
public class TymphanyDeviceFactory {

    /**
     * Create and return with the specific Joplin speaker
     *
     * @param type                 the specific type of the speaker
     * @param deviceColor          the color of the Joplin speaker
     * @param taSystem             information about the speaker
     * @param taSystemService      class that handle connection with speaker
     * @param taPlayControlService class that handle volume control
     * @param taDspService         class that handle equalizer
     * @return specific Joplin speaker
     */
    public static BaseDevice getTymphanyDevice(@NonNull DeviceSubType type,
                                               @NonNull DeviceInfo.DeviceColor deviceColor,
                                               @NonNull TASystem taSystem,
                                               @NonNull TASystemService taSystemService,
                                               @NonNull TAPlayControlService taPlayControlService,
                                               @NonNull TADigitalSignalProcessingService taDspService) {

        BaseDevice tymphanyDevice = null;
        DeviceInfo deviceInfo = new DeviceInfo(
                taSystem.deviceAddress,
                DeviceInfo.DeviceType.TYMPHANY_DEVICE,
                getGroupFromType(type),
                type.getProductName(),
                deviceColor);

        switch (type) {
            case JOPLIN_S:
                tymphanyDevice = new SmallJoplinSpeaker(
                        deviceInfo,
                        taSystemService,
                        taPlayControlService,
                        taDspService,
                        taSystem);
                break;
            case JOPLIN_M:
                tymphanyDevice = new MediumJoplinSpeaker(
                        deviceInfo,
                        taSystemService,
                        taPlayControlService,
                        taDspService,
                        taSystem);
                break;
            case JOPLIN_L:
                tymphanyDevice = new LargeJoplinSpeaker(
                        deviceInfo,
                        taSystemService,
                        taPlayControlService,
                        taDspService,
                        taSystem);
                break;
            case JOPLIN_S_LITE:
                tymphanyDevice = new SmallJoplinLiteSpeaker(
                        deviceInfo,
                        taSystemService,
                        taPlayControlService,
                        taDspService,
                        taSystem);
                break;
            case JOPLIN_M_LITE:
                tymphanyDevice = new MediumJoplinLiteSpeaker(
                        deviceInfo,
                        taSystemService,
                        taPlayControlService,
                        taDspService,
                        taSystem);
                break;
            case JOPLIN_L_LITE:
                tymphanyDevice = new LargeJoplinLiteSpeaker(
                        deviceInfo,
                        taSystemService,
                        taPlayControlService,
                        taDspService,
                        taSystem);
                break;
            case OZZY:
                tymphanyDevice = new OzzyHeadphones(
                        deviceInfo,
                        taSystemService,
                        taPlayControlService,
                        taDspService,
                        taSystem);
                break;
        }
        return tymphanyDevice;
    }

    private static DeviceGroup getGroupFromType(DeviceSubType type) {
        switch (type) {
            case OZZY:
                return DeviceGroup.HEADPHONES;
            case JOPLIN_L:
            case JOPLIN_M:
            case JOPLIN_S:
            case JOPLIN_L_LITE:
            case JOPLIN_M_LITE:
            case JOPLIN_S_LITE:
                return DeviceGroup.SPEAKER;
            default:
                return DeviceGroup.SPEAKER;
        }
    }
}

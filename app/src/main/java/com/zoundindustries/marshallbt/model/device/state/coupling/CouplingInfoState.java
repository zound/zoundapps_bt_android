package com.zoundindustries.marshallbt.model.device.state.coupling;

import com.zoundindustries.marshallbt.model.coupling.DeviceCouplingInfo;
import com.zoundindustries.marshallbt.model.device.BaseDevice.CouplingConnectionState;
import com.zoundindustries.marshallbt.model.device.state.BaseDeviceState;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

/**
 * Class extending {@link BaseDeviceState} used to monitor coupling connection state of the device.
 */
public class CouplingInfoState extends BaseDeviceState<DeviceCouplingInfo> {

    /**
     * Constructor for {@link CouplingInfoState}.
     */
    public CouplingInfoState() {
        super(FeaturesDefs.COUPLING_CONNECTION);
    }
}

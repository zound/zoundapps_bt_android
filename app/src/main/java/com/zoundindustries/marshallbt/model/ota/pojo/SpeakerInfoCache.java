package com.zoundindustries.marshallbt.model.ota.pojo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SpeakerInfoCache {

    @SerializedName("mac_address")
    @Expose
    private String macAddress;

    /**
     * tws - True Wireless Coupled
     */
    @SerializedName("is_tws_coupled")
    @Expose
    private Boolean isTwsCoupled;

    @SerializedName("tws_connection_info")
    @Expose
    private String twsConnectionInfo;

    @SerializedName("tws_channel_type")
    @Expose
    private String twsChannelType;

    @SerializedName("is_configured")
    @Expose
    private boolean isConfigured;

    public SpeakerInfoCache(String macAddress) {
        this.macAddress = macAddress;
        isTwsCoupled = false;
        twsConnectionInfo = "";
        twsChannelType = "";
        isConfigured = false;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(@NonNull String macAddress) {
        this.macAddress = macAddress;
    }

    public Boolean getIsTwsCoupled() {
        return isTwsCoupled;
    }

    public void setIsTwsCoupled(Boolean isTwsCoupled) {
        this.isTwsCoupled = isTwsCoupled;
    }

    public String getTwsConnectionInfo() {
        return twsConnectionInfo;
    }

    public void setTwsConnectionInfo(@Nullable String twsConnectionInfo) {
        this.twsConnectionInfo = twsConnectionInfo != null ? twsConnectionInfo : "";
    }

    public void setConfigured(boolean configured) {
        isConfigured = configured;
    }

    public String getTwsChannelType() {
        return twsChannelType;
    }

    public void setTwsChannelType(@Nullable String twsChannelType) {
        this.twsChannelType = twsChannelType != null ? twsChannelType : "";
    }

    public boolean isConfigured() {
        return isConfigured;
    }
}
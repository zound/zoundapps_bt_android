package com.zoundindustries.marshallbt.model.device.state.playcontrol;

import com.zoundindustries.marshallbt.model.device.state.BaseDeviceState;
import com.zoundindustries.marshallbt.model.player.sources.SongInfo;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

/**
 * Class extending {@link BaseDeviceState} used to set and monitor song information feature.
 * It will be automatically added to all devices with defined FeaturesDefs.SONG_INFO feature.
 */
public class SongInfoState extends BaseDeviceState<SongInfo> {

    /**
     * Constructor for SongInfoState. Contains required feature definition.
     */
    public SongInfoState() {
        super(FeaturesDefs.SONG_INFO);
    }
}

package com.zoundindustries.marshallbt.model.device.state.light;

import com.zoundindustries.marshallbt.model.device.state.BaseDeviceState;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

/**
 * Class extending {@link BaseDeviceState} used to monitor information about light of speaker.
 */
public class LightState extends BaseDeviceState<Integer> {

    /**
     * Constructor for BaseDeviceState. Contains required feature definition.
     */
    public LightState() {
        super(FeaturesDefs.LIGHT);
    }
}

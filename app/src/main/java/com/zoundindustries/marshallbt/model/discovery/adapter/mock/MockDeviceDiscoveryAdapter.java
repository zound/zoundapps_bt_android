package com.zoundindustries.marshallbt.model.discovery.adapter.mock;

import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.model.adapters.mock.MockSDK;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.discovery.IDiscoveryService;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Class that wraps Mock SDK with common {@link IDiscoveryService}
 */
public class MockDeviceDiscoveryAdapter implements IDiscoveryService {

    private MockSDK mockSDK;

    private final BehaviorSubject<BaseDevice> deviceAddSubject = BehaviorSubject.create();
    private boolean isScanningInProgress;

    public MockDeviceDiscoveryAdapter(MockSDK mockSDK) {
        this.mockSDK = mockSDK;
    }

    @NonNull
    @Override
    public void startScanForDevices() {
        if (!isScanningInProgress) {
            mockSDK.startMockScanForDevices(t -> {
                for (BaseDevice device : t) {
                    deviceAddSubject.onNext(device);
                }
            });
            isScanningInProgress = true;
        }
    }

    @NonNull
    @Override
    public Observable<BaseDevice> getDiscoveredDevices() {
        return deviceAddSubject;
    }

    @NonNull
    @Override
    public Observable<BaseDevice> getDisappearedDevices() {
        return null;
    }

    @Override
    public void stopScanForDevices() {
        final BehaviorSubject<Boolean> stopScanSubject = BehaviorSubject.create();
        isScanningInProgress = false;

        mockSDK.stopMockScanForDevices(state -> {
            stopScanSubject.onNext(state);
            stopScanSubject.onComplete();
        });
    }
}

package com.zoundindustries.marshallbt.model.ota;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.ota.pojo.FirmwareFileMetaData;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Interface defining OTA service methods.
 */
public interface IOTAService {

    /**
     * Enum defining current update state.
     */
    enum UpdateState {
        NOT_STARTED,
        PAUSED,
        RECONNECTING_FOR_RETRY,
        DOWNLOADING_FROM_SERVER,
        UNZIPPING,
        UPLOADING_TO_DEVICE,
        REBOOT_NEEDED,
        FLASHING_DEVICE,
        COMPLETED
    }

    /**
     * Get OTA firmware update observable state for specific device.
     *
     * @param deviceId that is monitored.
     * @return observable.
     */
    @Nullable
    default Observable<Double> getFirmwareUpdateProgress(@NonNull String deviceId) {
        return null;
    }

    /**
     * Get OTA firmware update observable progress for specific device.
     *
     * @param device that is monitored.
     * @return observable.
     */
    @Nullable
    default Observable<Double> getFirmwareUpdateProgress(@NonNull BaseDevice device) {
        return null;
    }

    @Nullable
    default Observable<Boolean> getFirmwareUpdateAvailability(@NonNull BaseDevice device) {
        return null;
    }

    /**
     * Get OTA firmware update state observable for specific device.
     *
     * @param device that is going to be updated.
     */
    @Nullable
    default Observable<IOTAService.UpdateState>
    getFirmwareUpdateState(@NonNull BaseDevice device) {
        return null;
    }

    /**
     * Get OTA firmware update state observable for specific device Id.
     *
     * @param deviceId that is going to be updated.
     */
    @Nullable
    default Observable<IOTAService.UpdateState>
    getFirmwareUpdateState(@NonNull String deviceId) {
        return null;
    }

    /**
     * Get OTA check for available update observable for specific {@link BaseDevice}.
     * Check is done against latest FW on the web server:
     * (https://w2ink5mdt1.execute-api.us-east-1.amazonaws.com/).
     *
     * @param deviceId that the FW is checked for.
     * @param isForced indicates if check should be done for forced OTA.
     *                 The flag switches the version comparision algorithm.
     * @return observabethe {@link FirmwareFileMetaData} value. ll emmit onError when new FW not
     * found or current FW is newer.
     */
    @Nullable
    default Observable<Response<FirmwareFileMetaData>>
    checkForFirmwareUpdate(@NonNull String deviceId, boolean isForced) {
        return null;
    }

    /**
     * Start OTA update for specific device based  on device ID.
     *
     * @param deviceId that is going to be updated.
     * @param metaData web response containing url to download FW binary files.
     *                 Should be separately obtained using checkForFirmwareUpdate method.
     */
    default void startOTAUpdate(@NonNull String deviceId,
                                @NonNull FirmwareFileMetaData metaData) {
    }

    /**
     * Start OTA update for specific device.
     *
     * @param device   that is going to be updated.
     * @param metaData web response containing url to download FW binary files.
     *                 Should be separately obtained using checkForFirmwareUpdate method.
     */
    default void startOTAUpdate(@NonNull BaseDevice device,
                                @NonNull FirmwareFileMetaData metaData) {
    }

    /**
     * Check if there is currently OTA ongoing.
     *
     * @return flag indicating OTA state.
     */
    default boolean isOTAInProgress() {
        return false;
    }

    /**
     * Finalize forced OTA to allow forced OTA queue check.
     * Next forced OTA check will be done only after this call.
     */
    default void finalizeForcedOTA() {
        //nop
    }
}

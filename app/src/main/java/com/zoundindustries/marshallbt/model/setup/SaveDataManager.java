package com.zoundindustries.marshallbt.model.setup;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Class made for saving data to CRM server
 */
public class SaveDataManager {

    private BehaviorSubject<Boolean> saveDataSubject = BehaviorSubject.create();

    /**
     * Save user data to server - currently mocked.
     * @return subject informing the saving is complete
     */
    @NonNull
    public Observable<Boolean> saveData() {

        // todo implement backend connection when it's ready
        new Thread(() -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            saveDataSubject.onNext(true);
            saveDataSubject.onComplete();
        }).start();
        return saveDataSubject;
    }
}

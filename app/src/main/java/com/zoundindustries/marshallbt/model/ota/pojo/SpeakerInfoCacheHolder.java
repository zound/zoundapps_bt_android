package com.zoundindustries.marshallbt.model.ota.pojo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SpeakerInfoCacheHolder {

    @SerializedName("SpeakerInfoCache")
    @Expose
    private List<SpeakerInfoCache> speakerInfoCache = new ArrayList<>();


    @Nullable
    public SpeakerInfoCache getSpeakerInfoCache(@NonNull String macAddress) {
        for (SpeakerInfoCache speakerInfoCache : speakerInfoCache) {
            if (speakerInfoCache.getMacAddress().equals(macAddress)) {
                return speakerInfoCache;
            }
        }
        return null;
    }

    public List<SpeakerInfoCache> getSpeakerInfoCacheList() {
        return speakerInfoCache;
    }

    public void setSpeakerInfoCacheList(List<SpeakerInfoCache> speakerInfoCache) {
        this.speakerInfoCache = speakerInfoCache;
    }
}
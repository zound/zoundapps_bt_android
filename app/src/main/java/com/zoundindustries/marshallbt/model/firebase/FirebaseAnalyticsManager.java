package com.zoundindustries.marshallbt.model.firebase;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.di.AppModule;
import com.zoundindustries.marshallbt.model.device.BaseDevice.SourceType;
import com.zoundindustries.marshallbt.model.ota.OtaErrorThrowable;
import com.zoundindustries.marshallbt.viewmodels.devicesettings.CouplingModeViewModel.Body.CouplingMode;
import com.zoundindustries.marshallbt.utils.EqPresetType;

/**
 * Class used for sending events to FirebaseAnalytics
 * Events: names, parameters and states are based on {@link FirebaseEventConstants}
 * <p>
 * <p>
 * Class is initiated in {@link BluetoothApplication} with usage of Dagger2
 */
public class FirebaseAnalyticsManager {

    private final FirebaseAnalytics firebaseAnalytics;

    /**
     * constructor used in {@link AppModule} to create global instance
     *
     * @param firebaseAnalytics is object from Firebase used for logging events.
     */
    public FirebaseAnalyticsManager(@NonNull FirebaseAnalytics firebaseAnalytics) {
        this.firebaseAnalytics = firebaseAnalytics;
    }

    /*************************************************************************************/
    /**                           DEVICE SETTINGS EVENTS                                **/
    //region

    /**
     * sends event to Firebase Analytics related to change in equalizer preset
     *
     * @param productName for which equalizer preset was changed
     * @param preset      new preset that was set by user
     */
    public void eventEqualizerPresetChanged(@NonNull String productName,
                                            @NonNull EqPresetType preset) {
        Bundle parameters = new Bundle();
        parameters.putString(FirebaseEventConstants.PARAMETER_KEY_DEVICE_MODEL, productName);
        parameters.putString(FirebaseEventConstants.PARAMETER_KEY_PRESET, preset.name().toLowerCase());

        logEventToFirebase(FirebaseEventConstants.EVENT_APP_EQUALIZER_PRESET_CHANGED, parameters);
    }

    /**
     * sends event to Firebase Analytics related to change in speaker name
     *
     * @param productName for which name was changed
     */
    public void eventSpeakerNameChanged(@NonNull String productName) {
        Bundle parameters = new Bundle();
        parameters.putString(FirebaseEventConstants.PARAMETER_KEY_DEVICE_MODEL, productName);

        logEventToFirebase(FirebaseEventConstants.EVENT_APP_SPEAKER_NAME_CHANGED, parameters);
    }

    /**
     * Log event of speaker coupling mode changed
     *
     * @param couplingMode mode which was selected
     */
    public void eventCouplingModeSwitched(@NonNull CouplingMode couplingMode) {
        String eventType = null;
        switch (couplingMode) {
            case NONE:
                break;
            case STANDARD:
                eventType = FirebaseEventConstants.EVENT_APP_COUPLING_SWITCHED_AMBIENT;
                break;
            case STEREO:
                eventType = FirebaseEventConstants.EVENT_APP_COUPLING_SWITCHED_STEREO;
                break;
        }
        if (eventType != null) {
            logEventToFirebase(eventType, null);
        }
    }

    /**
     * Log event of light settings change
     *
     * @param progressValue value of light that was set
     */
    public void eventLightSettingChanged(int progressValue) {
        Bundle parameters = new Bundle();
        parameters.putInt(FirebaseEventConstants.PARAMETER_LIGHT_VALUE, progressValue);

        logEventToFirebase(FirebaseEventConstants.EVENT_APP_LIGHT_SETTING_CHANGED, parameters);
    }
    //endregion

    /*************************************************************************************/
    /**
     * ONBOARDING EVENTS
     **/
    //region
    public void eventStayUpdatedSkipped() {
        logEventToFirebase(FirebaseEventConstants.EVENT_APP_STAY_UPDATED_SKIPPED, null);
    }

    public void eventShareDataSkipped() {
        logEventToFirebase(FirebaseEventConstants.EVENT_APP_SHARE_DATA_SKIPPED, null);
    }

    public void eventNewsletterSignedUp() {
        logEventToFirebase(FirebaseEventConstants.EVENT_APP_NEWSLETTER_SIGNUP, null);
    }

    /**
     * sends event to Firebase Analytics related to change in share analytics status
     *
     * @param isUserSharingData informs about user preferences related to share data
     */
    public void eventShareAnalyticsStatusChanged(boolean isUserSharingData) {
        setAnalyticsCollectionEnabled(isUserSharingData);
        Bundle parameters = new Bundle();
        parameters.putString(FirebaseEventConstants.PARAMETER_KEY_SWITCH_STATE,
                getSwitchState(isUserSharingData));

        logEventToFirebase(FirebaseEventConstants.EVENT_APP_SHARE_DATA_SWITCHED, parameters);
    }
    //endregion

    /*************************************************************************************/
    /**                               SOURCES EVENTS                                    **/
    //region

    /**
     * Log source change events basing on a source type
     *
     * @param sourceType type of source that was activated
     */
    public void eventSourceActivated(@NonNull SourceType sourceType) {
        String eventType = "";
        switch (sourceType) {
            case BLUETOOTH:
                eventType = FirebaseEventConstants.EVENT_APP_SOURCE_BLUETOOTH_ACTIVATED;
                break;
            case AUX:
                eventType = FirebaseEventConstants.EVENT_APP_SOURCE_AUX_ACTIVATED;
                break;
            case RCA:
                eventType = FirebaseEventConstants.EVENT_APP_SOURCE_RCA_ACTIVATED;
                break;
        }
        logEventToFirebase(eventType, null);
    }

    /**
     * Log tap of "play" player button
     */
    public void eventMediaButtonTouchedPlay() {
        logEventToFirebase(FirebaseEventConstants.EVENT_APP_MEDIA_BUTTON_TOUCHED_PLAY, null);
    }

    /**
     * Log tap of "pause" player button
     */
    public void eventMediaButtonTouchedPause() {
        logEventToFirebase(FirebaseEventConstants.EVENT_APP_MEDIA_BUTTON_TOUCHED_PAUSE, null);
    }

    /**
     * Log tap of "next" player button
     */
    public void eventMediaButtonTouchedNext() {
        logEventToFirebase(FirebaseEventConstants.EVENT_APP_MEDIA_BUTTON_TOUCHED_NEXT, null);
    }

    /**
     * Log tap of "previous" player button
     */
    public void eventMediaButtonTouchedPrev() {
        logEventToFirebase(FirebaseEventConstants.EVENT_APP_MEDIA_BUTTON_TOUCHED_PREV, null);
    }
    //endregion

    /*************************************************************************************/
    /**                                  OTA EVENTS                                     **/
    //region

    /**
     * Send "otaStarted" event to firebase
     */
    public void eventOtaStarted() {
        logEventToFirebase(FirebaseEventConstants.EVENT_APP_OTA_STARTED, null);
    }

    /**
     * Send "otaCompleted" event to firebase
     */
    public void eventOtaCompleted() {
        logEventToFirebase(FirebaseEventConstants.EVENT_APP_OTA_COMPLETED, null);
    }

    /**
     * Send "otaFailed" event to firebase
     */
    public void eventOtaFailed(@Nullable String errorMessage) {
        Bundle parameters = new Bundle();
        parameters.putString(FirebaseEventConstants.PARAMETER_KEY_OTA_ERROR_TYPE, errorMessage);

        logEventToFirebase(FirebaseEventConstants.EVENT_APP_OTA_FAILED, parameters);
    }

    /**
     * Send "forcedOtaStarted" event to firebase
     */
    public void eventForcedOtaStarted() {
        logEventToFirebase(FirebaseEventConstants.EVENT_APP_FORCED_OTA_STARTED, null);
    }

    /**
     * Send "forcedOtaCheckStarted" event to firebase
     */
    public void eventForcedOtaCheckStarted() {
        logEventToFirebase(FirebaseEventConstants.EVENT_APP_FORCED_OTA_CHECK_STARTED, null);
    }

    /**
     * Send "forcedOtaCheckFinished" event to firebase
     */
    public void eventForcedOtaCheckFinished() {
        logEventToFirebase(FirebaseEventConstants.EVENT_APP_FORCED_OTA_CHECK_FINISHED, null);
    }
    //endregion
    /*************************************************************************************/
    /**                                   ERROR EVENTS                                  **/
    //region

    /**
     * Send "otaErrorOccurred" event to firebase
     *
     * @param throwable error type specific throwable
     */
    public void eventErrorOccurredOta(@NonNull Throwable throwable) {
        String eventKey = FirebaseEventConstants.EVENT_APP_ERROR_OCCURRED_OTA_UNDEFINED;

        if (throwable instanceof OtaErrorThrowable) {
            switch (((OtaErrorThrowable) throwable).getErrorType().getGeneralErrorType()) {
                case DOWNLOAD_ERROR:
                    eventKey = FirebaseEventConstants.EVENT_APP_ERROR_OCCURRED_OTA_DOWNLOAD;
                    break;
                case FLASHING_ERROR:
                    eventKey = FirebaseEventConstants.EVENT_APP_ERROR_OCCURRED_OTA_FLASHING;
                    break;
            }
        }

        logEventToFirebase(eventKey, null);
    }

    /**
     * Log event of bluetooth error
     */
    public void eventErrorOccurredBluetooth() {
        logEventToFirebase(FirebaseEventConstants.EVENT_APP_ERROR_OCCURRED_BLUETOOTH, null);
    }

    /**
     * Log event of network error
     */
    public void eventErrorOccurredNetwork() {
        logEventToFirebase(FirebaseEventConstants.EVENT_APP_ERROR_OCCURRED_NETWORK, null);
    }
    //endregion
    /*************************************************************************************/
    /**                                  OTHER EVENTS                                   **/

    /**
     * Sends event to Firebase Analytics related to no devices found if location service is off.
     *
     * @param androidSystemVersion Android API level for which the event happen.
     */
    public void eventAppNoDevicesFoundWithNoLocation(@NonNull String androidSystemVersion) {
        Bundle parameters = new Bundle();
        parameters.putString(FirebaseAnalytics.Param.VALUE, androidSystemVersion);

        logEventToFirebase(FirebaseEventConstants.EVENT_APP_NO_DEVICES_FOUND_WITH_NO_LOCATION,
                parameters);
    }

    /**
     * Log event of volume change
     *
     * @param volumeValue set volume value
     */
    public void eventAppVolumeChanged(int volumeValue) {
        Bundle parameters = new Bundle();
        parameters.putInt(FirebaseEventConstants.PARAMETERS_VOLUME_VALUE, volumeValue);

        logEventToFirebase(FirebaseEventConstants.EVENT_VOLUME_CHANGED, parameters);
    }

    /**
     * Sends log event to FirebaseAnalytics server
     *
     * @param eventName  from {@link FirebaseEventConstants}
     * @param parameters Bundle that is created for each event that contains information about event
     */
    private void logEventToFirebase(@NonNull String eventName, @Nullable Bundle parameters) {
        firebaseAnalytics.logEvent(eventName, parameters);
    }

    /**
     * Responsible for enabling/disabling possibility to collect analytics data to FirebaseAnalytics
     */
    private void setAnalyticsCollectionEnabled(boolean isUserSharingData) {
        firebaseAnalytics.setAnalyticsCollectionEnabled(isUserSharingData);
    }

    @NonNull
    private String getSwitchState(boolean isSwitchChecked) {
        return isSwitchChecked ? FirebaseEventConstants.SWITCH_STATE_ON :
                FirebaseEventConstants.SWITCH_STATE_OFF;
    }

}
package com.zoundindustries.marshallbt.model.ota.adapter.mock;

public interface IOTAProgressListener {
    void onProgressUpdate(double progress);
}

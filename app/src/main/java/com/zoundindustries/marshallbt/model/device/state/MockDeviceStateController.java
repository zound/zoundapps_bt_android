package com.zoundindustries.marshallbt.model.device.state;

import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.model.adapters.mock.MockSDK;
import com.zoundindustries.marshallbt.model.coupling.DeviceCouplingInfo;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.BaseDevice.ConnectionState;
import com.zoundindustries.marshallbt.model.device.tymphany.SpeakerInfo;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Class configuring and accessing Mock devices states.
 */
@SuppressWarnings("unchecked")
public class MockDeviceStateController extends BaseDeviceStateController implements
        BaseDeviceStateController.Inputs,
        BaseDeviceStateController.Outputs {


    MockSDK mockSDK;

    private BaseDevice device;
    private BehaviorSubject<String> coupleId = BehaviorSubject.create();

    /**
     * Constructor for the MockDeviceStateController.
     *
     * @param features supported by the device.
     */
    public MockDeviceStateController(int features, @NonNull MockSDK mockSDK) {
        super(features);
        this.mockSDK = mockSDK;
    }

    /**
     * Setter for device specific to this controller.
     *
     * @param device to be set.
     */
    public void setDevice(@NonNull BaseDevice device) {
        this.device = device;
        startMonitorVolume();
        startMonitorUpdate();
    }

    @Override
    public void setVolume(int value) {
        mockSDK.changeMockVolume(device, value, null);
    }

    @Override
    public void setPlaying(boolean playing) {
        getStateFromFeature(FeaturesDefs.PLAY_CONTROL).setValue(playing);
    }

    @Override
    public void setConnectionInfo(BaseDevice.ConnectionState state) {
        if (state == BaseDevice.ConnectionState.CONNECTED) {
            getStateFromFeature(FeaturesDefs.CONNECTION_INFO)
                    .setValue(BaseDevice.ConnectionState.CONNECTING);
            new Thread(() -> {
                try {
                    Thread.sleep(1000);
                    getStateFromFeature(FeaturesDefs.CONNECTION_INFO)
                            .setValue(BaseDevice.ConnectionState.CONNECTED);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        } else {
            getStateFromFeature(FeaturesDefs.CONNECTION_INFO).setValue(state);
        }
    }

    @Override
    public void setCouplingMasterConnected(@NonNull BaseDevice.CouplingChannelType channelInfoState,
                                           @NonNull String MAC) {
        getStateFromFeature(FeaturesDefs.COUPLING_CONNECTION)
                .setValue(BaseDevice.CouplingConnectionState.CONNECTED_AS_MASTER);
    }

    @Override
    public void setHasUpdate(boolean hasUpdate) {
        getStateFromFeature(FeaturesDefs.OTA_UPDATE).setValue(hasUpdate);
    }

    @Override
    public void startPairingMode() {
        //nop
    }

    @Override
    public void setSpeakerInfo(@NonNull SpeakerInfo speakerInfo) {
        getStateFromFeature(FeaturesDefs.SPEAKER_INFO).setValue(speakerInfo);
    }

    @Override
    public void reconnect() {
        //nop
    }

    @Override
    @NonNull
    public Observable<Integer> getVolume() {
        return getStateFromFeature(FeaturesDefs.VOLUME).getObservable();
    }

    @Override
    @NonNull
    public Observable<Boolean> getPlaying() {
        return getStateFromFeature(FeaturesDefs.PLAY_CONTROL).getObservable();
    }

    @Override
    @NonNull
    public Observable<BaseDevice.ConnectionState> getConnectionInfo() {
        return getStateFromFeature(FeaturesDefs.CONNECTION_INFO).getObservable();
    }

    @NonNull
    @Override
    public BaseDevice.ConnectionState getConnectionInfoCurrentValue() {
        return (ConnectionState) getStateFromFeature(FeaturesDefs.CONNECTION_INFO).getValue();
    }

    @NonNull
    @Override
    public Observable<DeviceCouplingInfo> getCouplingConnectionInfo() {
        return getStateFromFeature(FeaturesDefs.COUPLING_CONNECTION).getObservable();
    }

    @NonNull
    @Override
    public DeviceCouplingInfo getCouplingConnectionInfoCurrentValue() {
        return (DeviceCouplingInfo) getStateFromFeature(FeaturesDefs.COUPLING_CONNECTION).getValue();
    }

    @NonNull
    @Override
    public Observable<BaseDevice.CouplingChannelType> getCouplingChannelInfo() {
        return getStateFromFeature(FeaturesDefs.COUPLING_CHANNEL).getObservable();
    }

    @NonNull
    @Override
    public Observable<String> getCoupleId() {
        return coupleId;
    }

    @NonNull
    @Override
    public Observable<Boolean> getHasUpdate() {
        return getStateFromFeature(FeaturesDefs.OTA_UPDATE).getObservable();
    }

    @NonNull
    @Override
    public Observable<SpeakerInfo> getSpeakerInfo() {
        return getStateFromFeature(FeaturesDefs.SPEAKER_INFO).getObservable();
    }

    @NonNull
    @Override
    public SpeakerInfo getSpeakerInfoCurrentValue() {
        return (SpeakerInfo) getStateFromFeature(FeaturesDefs.SPEAKER_INFO).getValue();
    }

    @NonNull
    @Override
    public boolean isAuxConfigurable() {
        return false;
    }

    private void startMonitorVolume() {
        mockSDK.startMonitorVolume(device, (baseDevice, newValue) -> {
            if (baseDevice.equals(device)) {
                MockDeviceStateController.this.getStateFromFeature(FeaturesDefs.VOLUME)
                        .setValue(newValue);
            }
        });
    }

    private void startMonitorUpdate() {
        mockSDK.startMonitorUpdate(device, MockDeviceStateController.this
                .getStateFromFeature(FeaturesDefs.OTA_UPDATE)::setValue);
    }
}
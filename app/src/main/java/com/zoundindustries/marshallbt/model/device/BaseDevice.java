package com.zoundindustries.marshallbt.model.device;

import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.model.device.state.BaseDeviceStateController;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

/**
 * Base class defining generic model for all devices in the system.
 */
public abstract class BaseDevice {

    /**
     * Defines coupling connection states.
     * CONNECTED_AS_MASTER - device is connected and acts as master - is an audio source,
     * CONNECTED_AS_SLAVE - device is connected and acts as slave - is an audio sink,
     * NOT_COUPLED - there is no active coupling for the device.
     */
    public enum CouplingConnectionState {
        CONNECTED_AS_MASTER,
        CONNECTED_AS_SLAVE,
        NOT_COUPLED
    }

    /**
     * Defines coupling channel type.
     * PARTY - both coupled devices play the same audio source on both channels,
     * LEFT - master device plays as left channel of the audio source,
     * RIGHT - master device plays as right channel of the audio source,
     */
    public enum CouplingChannelType {
        PARTY,
        LEFT,
        RIGHT;

        @NonNull
        public static CouplingChannelType getDefaultValue() {
            return LEFT;
        }
    }

    /**
     * Defines types of possible connection states of the device.
     * READY_TO_CONNECT     - device has been already configured and is ready for connection,
     * CONNECTING           - device is connecting using BLE protocol,
     * SETTING_PAIRING_MODE - enable pairing mode is required and indicated to the user,
     * BONDING              - device is automatically bonding. In this state disconnections are ignored.
     *                        Device needs to support AUTO_CONNECT and PAIRING_MODE_STATUS features.
     * CONNECTED            - device is connected and ready to be used/controlled by the app,
     * DISCONNECTED         - device has disconnected and is not available,
     * TIMEOUT              - BLE connection was not successful with timeout.
     */
    public enum ConnectionState {
        READY_TO_CONNECT,
        CONNECTING,
        SETTING_PAIRING_MODE,
        CONNECTED,
        TWS_CONNECTED,
        DISCONNECTED,
        TIMEOUT
    }

    /**
     * Defines different states for speaker name validation.
     */
    public enum NameState {
        VALID,
        TOO_LONG,
        EMPTY
    }

    /**
     * Defines types of sources available for the device.
     */
    public enum SourceType {
        BLUETOOTH,
        AUX,
        RCA
    }

    private final DeviceInfo deviceInfo;
    private final BaseDeviceStateController baseDeviceState;
    private final int features;

    /**
     * Constructor for BaseDevice. Requires device basic parameters and features.
     *
     * @param deviceInfo      unique identification of the device.
     * @param features        features bit mask defining supported features of the device.
     *                        All available features defined in {@link FeaturesDefs}
     * @param baseDeviceState pass SDK specific DeviceStateController to the device when creating
     *                        new device
     */
    public BaseDevice(@NonNull DeviceInfo deviceInfo, int features,
                      @NonNull BaseDeviceStateController baseDeviceState) {
        this.deviceInfo = deviceInfo;
        this.features = features;
        this.baseDeviceState = baseDeviceState;
    }

    /**
     * Getter for DeviceStateController to access inputs and outputs of the states.
     *
     * @return base type BaseDeviceStateController.
     */
    @NonNull
    public BaseDeviceStateController getBaseDeviceStateController() {
        return baseDeviceState;
    }

    /**
     * Getter for unique DeviceInfo object for the device.
     *
     * @return device info for the device
     */
    @NonNull
    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseDevice device = (BaseDevice) o;

        return deviceInfo.equals(device.deviceInfo);
    }

    @Override
    public int hashCode() {
        return deviceInfo.hashCode();
    }

    /**
     * Clear all device specific data (listeners, observables, etc.).
     */
    public void dispose() {
        baseDeviceState.inputs.dispose();
    }

    /**
     * This method checks the given speaker name validity and return with an enum for the different
     * states
     *
     * @param newName the new name of the speaker
     * @return a state for name validity
     */
    @NonNull
    public NameState getValidityState(@NonNull String newName) {
        return NameState.VALID;
    }

    /**
     * This method gives back the maximum possible length of speaker name
     *
     * @return the maximum length
     */
    public abstract int getMaxNameLength();
}

package com.zoundindustries.marshallbt.model.device.state.pairingmode;

import com.zoundindustries.marshallbt.model.device.state.BaseDeviceState;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

/**
 * Class extending {@link BaseDeviceState} used to monitor information about battery level of speaker.
 */
public class PairingModeState extends BaseDeviceState<Boolean> {

    /**
     * Constructor for BaseDeviceState. Contains required feature definition.
     */
    public PairingModeState() {
        super(FeaturesDefs.PAIRING_MODE_STATUS);
    }
}

package com.zoundindustries.marshallbt.model.device;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zoundindustries.marshallbt.model.BaseManager;
import com.zoundindustries.marshallbt.model.devicesettings.EQData;
import com.zoundindustries.marshallbt.model.devicesettings.EQPreset;
import com.zoundindustries.marshallbt.model.ota.pojo.SpeakerInfoCache;
import com.zoundindustries.marshallbt.services.DeviceService;
import com.zoundindustries.marshallbt.services.DeviceService.DeviceImageType;
import com.zoundindustries.marshallbt.ui.onboarding.BaseOnboardingContainerFragment;
import com.zoundindustries.marshallbt.utils.EqPresetType;

import java.util.List;

import io.reactivex.Observable;

/**
 * Class providing general API for getting device info from the {@link DeviceService}.
 */
public class DeviceManager extends BaseManager {

    /**
     * Constructor for DeviceManager invoking {@link DeviceService} connection.
     *
     * @param context used for binding with service.
     */
    public DeviceManager(@NonNull Context context) {
        super(context);
    }

    /**
     * Get {@link BaseDevice} from its ID.
     *
     * @param deviceId to be used to get the full {@link BaseDevice} object.
     * @return BaseDevice for the id.
     */
    @Nullable
    public BaseDevice getDeviceFromId(@Nullable String deviceId) {
        if (isBound) {
            return deviceService.getDeviceFromId(deviceId);
        }
        return null;
    }

    /**
     * Get image resource id for device
     *
     * @param deviceId  used to get image of this device
     * @param imageSize used to determine what size of image is required
     * @return resource id of image
     */
    public int getImageResourceId(@NonNull String deviceId, @NonNull DeviceImageType imageSize) {
        return deviceService.getImageResourceId(deviceId, imageSize);
    }


    public BaseOnboardingContainerFragment getOnboardingFragment(String deviceId) {
        return deviceService.getOnboardingFragment(deviceId);
    }
    /**
     * Get currently discovered devices. Used to allow c
     * lients to get snapshot of the current device
     * list and register for further changes.
     *
     * @return List with BaseDevices.
     */
    @NonNull
    public List<BaseDevice> getCurrentDevices() {
        return deviceService.getCurrentDevices();
    }

    /**
     * Get Id for the device that is currently showing the divider shown on the main screen list.
     *
     * @return Id of the device.
     */
    @NonNull
    public Observable<String> getDividerDeviceId() {
        return deviceService.getDividerDeviceId();
    }

    /**
     * Set currently last connected device to show it with the divider view.
     *
     * @param deviceId to be set as last connected.
     */
    public void setDividerDeviceId(@NonNull String deviceId) {
        deviceService.setDividerDeviceId(deviceId);
    }

    /**
     * Method used for adding new {@link SpeakerInfoCache} to storage.
     *
     * @param macAddress of speaker that should be added to storage
     * @return true in case when adding had succeed, otherwise false
     */
    public boolean saveSpeakerInfoCache(@NonNull String macAddress) {
        return deviceService.saveSpeakerInfoCache(macAddress);
    }

    /**
     * Method updating isConfigured information in device cache. Not yet configured devices will
     * invoke additional UI onboarding for the user.
     *
     * @param macAddress of the device to be updated
     */
    public void setDeviceConfigured(@NonNull String macAddress) {
        deviceService.setDeviceConfigured(macAddress);

    }

    /**
     * Remove the {@link BaseDevice} from the {@link DeviceService}.
     */
    public void removeDevice(@NonNull BaseDevice device) {
        deviceService.removeDevice(device);
    }

    /**
     * Method used for removing {@link SpeakerInfoCache} from storage.
     *
     * @param macAddress of speaker that should be removed from storage
     * @return true in case when removing had succeed, otherwise false
     */
    public boolean removeSpeakerInfoCache(@NonNull String macAddress) {
        return deviceService.removeSpeakerInfoCache(macAddress);
    }

    /**
     * Method used for updating stored {@link SpeakerInfoCache} information
     *
     * @param macAddress        of updated Speaker
     * @param isTwsCoupled      status of speaker
     * @param twsChannelType    might be null
     * @param twsConnectionInfo might be null
     */
    public void updateSpeakerInfoCache(@NonNull String macAddress,
                                       boolean isTwsCoupled,
                                       @Nullable String twsChannelType,
                                       @Nullable String twsConnectionInfo) {
        deviceService.updateSpeakerInfoCache(macAddress, isTwsCoupled,
                twsChannelType, twsConnectionInfo);
    }

    /**
     * Method used for retrieving {@link SpeakerInfoCache} from storage based on MAC Address
     *
     * @param macAddress of speaker
     * @return {@link SpeakerInfoCache} in case when speaker exist otherwise null
     */
    @Nullable
    public SpeakerInfoCache getSpeakerInfoCache(@NonNull String macAddress) {
        return deviceService.getSpeakerInfoCache(macAddress);
    }

    /**
     * Method used for retrieving boolean based on if {@link SpeakerInfoCache} exist
     *
     * @param macAddress of speaker
     * @return true in case when {@link SpeakerInfoCache}  exist otherwise false
     */
    public boolean isSpeakerInfoCached(@NonNull String macAddress) {
        return deviceService.isSpeakerInfoCached(macAddress);
    }

    /**
     * Method used to update the name of a device
     *
     * @param id      the id of the device
     * @param newName the new name of the device
     */
    public void updateDeviceName(@NonNull String id, @NonNull String newName) {
        deviceService.updateDeviceName(id, newName);
    }

    /**
     * Get the list of the equalizer presets
     *
     * @return the list of presets
     */
    @Nullable
    public List<EqPresetType> getEqualizerPresetTypes(@NonNull BaseDevice device) {
        return deviceService.getEqualizerPresetTypes(device);
    }

    /**
     * Get the preset with the chosen index
     *
     * @param index the index of the chosen preset
     * @return the preset
     */
    @Nullable
    public EQPreset getEqualizerPresetByIndex(@NonNull BaseDevice device, int index) {
        return deviceService.getEqualizerPresetByIndex(device, index);
    }

    /**
     * Save the equalizer values for the custom preset
     *
     * @param device                to distinguish different preset providers.
     * @param customEqualizerPreset the custom preset.
     */
    public void saveCustomEqualizerPreset(@NonNull BaseDevice device,
                                          @NonNull EQPreset customEqualizerPreset) {
        deviceService.saveCustomEqualizerPreset(device, customEqualizerPreset);
    }

    /**
     * Get the index of the given preset
     *
     * @param device          to distinguish different preset providers.
     * @param equalizerPreset the preset which ones index is needed
     * @return the index of the preset
     */
    public int getIndexOfEqualizerPreset(@NonNull BaseDevice device,
                                         @NonNull EQPreset equalizerPreset) {
        return deviceService.getIndexOfEqualizerPreset(device, equalizerPreset);
    }

    /**
     * Get the type for the given preset values
     *
     * @param device to distinguish different preset providers.
     * @param eqData the preset values
     * @return the type of the preset
     */
    @Nullable
    public EqPresetType getEqualizerPresetType(@NonNull BaseDevice device,
                                               @NonNull EQData eqData) {
        return deviceService.getEqualizerPresetType(device, eqData);
    }

    /**
     * Start periodic scan. It will continue until the service object is alive.
     * Periodic scan is turned off when OTA is running.
     */
    public void startPeriodicScan() {
        deviceService.startPeriodicScan();
    }

    /**
     * Control scanning from UI flow. Allows or disallows scanning for certain UX cases.
     *
     * @param isAllowed toggle flag.
     */
    public void setScanAllowed(boolean isAllowed) {
        if (deviceService != null) {
            if (!isAllowed) deviceService.stopScanForDevices();
            deviceService.setScanAllowed(isAllowed);
        }
    }
}

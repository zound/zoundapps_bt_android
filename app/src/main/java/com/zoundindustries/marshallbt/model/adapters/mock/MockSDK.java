package com.zoundindustries.marshallbt.model.adapters.mock;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.mock.MockSpeaker;
import com.zoundindustries.marshallbt.model.discovery.adapter.mock.ISpeakerDiscoveryListener;
import com.zoundindustries.marshallbt.model.ota.adapter.mock.IOTAProgressListener;
import com.zoundindustries.marshallbt.model.ota.adapter.mock.IOTAUpdateListener;
import com.zoundindustries.marshallbt.utils.MockConfig;
import com.zoundindustries.marshallbt.utils.MockConfigurator;

import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Class that simulates speaker behaviour.
 * Used for development.
 */
public class MockSDK {

    private static final String TAG = MockSDK.class.getSimpleName();

    private static final int DEFAULT_SCAN_DELAY = 10;
    private static final int COMMAND_RESPONSE_MILLIS = 10;
    private static final int VOLUME_SET_DELAY_MILLIS = 20;

    private MockConfig config;
    Map<BaseDevice, IPlayControlListener> volumeMap;
    Map<BaseDevice, IOTAProgressListener> updateMap;
    private boolean stopScan;

    public MockSDK() {
        volumeMap = Collections.synchronizedMap(new HashMap<>());
        updateMap = Collections.synchronizedMap(new HashMap<>());
        setupDefaultConfig();
    }

    /**
     * Setter for MockSDK configuration.
     *
     * @param config to be used for configuration.
     */
    public void setConfig(@NonNull MockConfig config) {
        this.config = config;
    }

    /**
     * Simulate start scanning procedure for all supported adapters.
     * Uses remote configuration of the mock sdk.
     *
     * @param speakerDiscoveryListener callback for found devices event.
     */
    public void startMockScanForDevices(@Nullable final ISpeakerDiscoveryListener
                                                speakerDiscoveryListener) {

        if (config.getDevices().size() > 0) {
            if (speakerDiscoveryListener != null) {
                speakerDiscoveryListener.onSpeakersDiscovered(config.getDevices());
            }
        }
        final Thread devicesThread = new Thread(() -> {

            try {
                MockConfigurator mockConfigurator = new MockConfigurator(this);
                MockConfig remoteConfig = mockConfigurator.getMockConfiguration();
                setConfig(remoteConfig);
                Thread.sleep(config.getScanDelay());

                if (config.isContinuousScan()) {
                    Thread scanThread = new Thread(() -> {
                        simulateNewDevice(speakerDiscoveryListener);
                    });
                    scanThread.start();
                }
            } catch (InterruptedException | IOException | ParseException e) {
                e.printStackTrace();
                Log.w(TAG, "Error while getting remote configuration, default config used");
            }

            if (speakerDiscoveryListener != null) {
                speakerDiscoveryListener.onSpeakersDiscovered(config.getDevices());
            }
        });

        devicesThread.start();
    }

    /**
     * Simulate stop scanning procedure for all supported adapters.
     *
     * @param responseListener callback for successful command.
     */
    public void stopMockScanForDevices(@Nullable final IResponseListener responseListener) {
        Thread stopMockScThread = new Thread(() -> {
            sleepMs(100);
            responseListener.onResponseReceived(true);
            stopScan = true;
        });
        stopMockScThread.start();
    }

    /**
     * Simulate change volume for selected {@link BaseDevice}.
     *
     * @param baseDevice       that volume is changed for.
     * @param volume           new volume value.
     * @param responseListener callback for volume changes.
     */
    public void changeMockVolume(@NonNull final BaseDevice baseDevice, final int volume,
                                 @Nullable final IResponseListener responseListener) {
        Thread stopMockScThread = new Thread(() -> {
            sleepMs(COMMAND_RESPONSE_MILLIS);
            if (responseListener != null) {
                responseListener.onResponseReceived(true);
            }
            sleepMs(VOLUME_SET_DELAY_MILLIS);
            IPlayControlListener playControlListener = volumeMap.get(baseDevice);
            if (playControlListener != null) {
                playControlListener.onVolumeChanged(baseDevice, volume);
            }
        });
        stopMockScThread.start();
    }

    /**
     * Register for volume changes for the specific device
     *
     * @param baseDevice          to be monitored.
     * @param playControlListener callback to be notified about volume changes.
     */
    public void startMonitorVolume(@NonNull BaseDevice baseDevice, @NonNull IPlayControlListener
            playControlListener) {
        if (!volumeMap.containsKey(baseDevice)) {
            volumeMap.put(baseDevice, playControlListener);
        }
    }

    /**
     * Register for OTA update progress for a device.
     *
     * @param device              to be monitored.
     * @param otaProgressListener callback for update progress changes.
     */
    public void startMonitorUpdateProgress(@NonNull BaseDevice device,
                                           @NonNull IOTAProgressListener otaProgressListener) {
        if (!updateMap.containsKey(device)) {
            updateMap.put(device, otaProgressListener);
            sleepMs(100);
        }
    }

    /**
     * Register for OTA update state for a device.
     *
     * @param baseDevice        to be checked.
     * @param otaUpdateListener callback for has update value changes.
     */
    public void startMonitorUpdate(@NonNull BaseDevice baseDevice,
                                   @NonNull IOTAUpdateListener otaUpdateListener) {
        new Thread(() -> {
            sleepMs(500);
            for (String deviceId : config.getUpdateDevices()) {
                otaUpdateListener.hasUpdate(baseDevice.getDeviceInfo().getId().equals(deviceId));

            }
        }).start();
    }

    /**
     * Start OTA update for selected device.
     *
     * @param baseDevice to start the update for.
     */
    public void startOTAUpdate(@NonNull BaseDevice baseDevice) {
        new Thread(() -> {
            //for mock we scale download and firmware update for 50% of the full progress
            //TODO: decide on scaling and showing combined progess
            for (int i = 50; i <= 100; i++) {
                sleepMs(100);
                if (updateMap.containsKey(baseDevice)) {
                    updateMap.get(baseDevice).onProgressUpdate(i);
                }

            }
        }).start();
    }


    private void setupDefaultConfig() {
        List<BaseDevice> devices = new ArrayList<>();
        List<String> updateDevices = new ArrayList<>();
        this.config = new MockConfig.Builder().setScanDelay(DEFAULT_SCAN_DELAY).setDevices(devices)
                .setContinuousScan(false).setUpdateDevices(updateDevices).create();
    }

    private void sleepMs(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void simulateNewDevice(@Nullable ISpeakerDiscoveryListener speakerDiscoveryListener) {
        while (!stopScan) {
            sleepMs(5000);
            BaseDevice randomDevice =
                    new MockSpeaker(new DeviceInfo(
                            String.valueOf(new Random().nextInt(100000)),
                            DeviceInfo.DeviceType.MOCK,
                            DeviceInfo.DeviceGroup.SPEAKER,
                            "RANDOM_",
                            DeviceInfo.DeviceColor.BLACK), MockSDK.this);
            randomDevice.getBaseDeviceStateController().inputs.setPlaying(false);
            randomDevice.getBaseDeviceStateController().inputs
                    .setConnectionInfo(BaseDevice.ConnectionState.CONNECTED);

            config.getDevices().add(randomDevice);
            if (speakerDiscoveryListener != null) {
                speakerDiscoveryListener.onSpeakersDiscovered(config.getDevices());
            }
        }
    }
}


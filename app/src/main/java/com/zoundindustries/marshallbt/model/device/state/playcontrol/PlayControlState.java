package com.zoundindustries.marshallbt.model.device.state.playcontrol;

import com.zoundindustries.marshallbt.model.device.state.BaseDeviceState;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

/**
 * Class extending {@link BaseDeviceState} used to set and monitor play control feature.
 * It will be automatically added to all devices with defined FeaturesDefs.PLAY_CONTROL feature.
 */
public class PlayControlState extends BaseDeviceState<Boolean> {

    /**
     * Constructor for PlayControlState. Contains required feature definition.
     */
    public PlayControlState() {
        super(FeaturesDefs.PLAY_CONTROL);
    }
}

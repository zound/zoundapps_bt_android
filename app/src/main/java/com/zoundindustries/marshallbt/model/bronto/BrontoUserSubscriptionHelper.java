package com.zoundindustries.marshallbt.model.bronto;

import androidx.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class BrontoUserSubscriptionHelper {
    private static final String BRONTO_LIST_ID = "0bc403ec00000000000000000000001cb314";
    private static final int BRONTO_REQUEST_TIMEOUT = 3000;
    private BrontoApi brontoApi;

    static final String BRONTO_USER_REGISTER_API_URL_PATH =
            "public/?q=direct_add&fn=Public_DirectAddForm&id=bkosuajvjrjswzqzgxklysuedhzybdj&createCookie=1";

    static final String BRONTO_USER_UNREGISTER_API_URL_PATH =
            "public/?q=direct_unsub&fn=Public_DirectUnsubForm&id=bkosuajvjrjswzqzgxklysuedhzybdj";

    public BrontoUserSubscriptionHelper(BrontoApi api) {
        brontoApi = api;
    }

    public Observable<Response<Void>> registerUserMailObservable(@NonNull String userMail) {
        return brontoApi.userMailRegisterRequest(userMail, BRONTO_LIST_ID)
                .subscribeOn(Schedulers.io())
                .timeout(BRONTO_REQUEST_TIMEOUT, TimeUnit.MILLISECONDS);
    }

    public Observable<Response<Void>> unregisterUserMailObservable(@NonNull String userMail) {
        return brontoApi.userMailUnregisterRequest(userMail)
                .subscribeOn(Schedulers.io())
                .timeout(BRONTO_REQUEST_TIMEOUT, TimeUnit.MILLISECONDS);
    }
}

package com.zoundindustries.marshallbt.model.device.tymphany;

import androidx.annotation.NonNull;

import com.tym.tymappplatform.TAProtocol.TAProtocol.TASystem;
import com.tym.tymappplatform.TAService.TADigitalSignalProcessingService.TADigitalSignalProcessingService;
import com.tym.tymappplatform.TAService.TAPlayControlService.TAPlayControlService;
import com.tym.tymappplatform.TAService.TASystemService.TASystemService;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

/**
 * Large model of Joplin Lite speaker
 */
public class LargeJoplinLiteSpeaker extends TymphanyDevice {

    private static final int LARGE_JOPLIN_FEATURES = JOPLIN_FEATURES |
            FeaturesDefs.BLUETOOTH_SOURCE |
                    FeaturesDefs.RCA_SOURCE |
                    FeaturesDefs.AUX_SOURCE;

    /**
     * Constructor for {@link LargeJoplinLiteSpeaker}. Requires device basic parameters and features.
     *
     * @param deviceInfo unique identification of the device.
     */
    public LargeJoplinLiteSpeaker(@NonNull DeviceInfo deviceInfo,
                                  @NonNull TASystemService taSystemService,
                                  @NonNull TAPlayControlService taPlayControlService,
                                  @NonNull TADigitalSignalProcessingService taDspService,
                                  @NonNull TASystem taSystem) {
        super(deviceInfo, taSystemService, taPlayControlService, taDspService,
                DeviceSubType.JOPLIN_L_LITE, taSystem, LARGE_JOPLIN_FEATURES);
    }
}

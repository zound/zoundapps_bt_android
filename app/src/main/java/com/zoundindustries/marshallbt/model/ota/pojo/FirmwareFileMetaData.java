package com.zoundindustries.marshallbt.model.ota.pojo;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.utils.FirmwareFileHelper;

import java.io.File;

/**
 * Model class used when downloading a software OTA file
 */
public class FirmwareFileMetaData implements Parcelable {

    private String brand;
    private String model;
    private String timeout;
    private String url;
    private String version;

    /**
     * Creates a new {@link File} based on the firmware file properties.
     * The file name will be as follows:
     * <brand>_<model>_<version>.zip
     *
     * @param firmwareFile object use to create a {@link File}.
     * @return newly created {@link File}
     */
    @NonNull
    public static File getFirmwareFile(@NonNull FirmwareFileMetaData firmwareFile,
                                       @NonNull FirmwareFileHelper fwFileHelper) {

        return new File(fwFileHelper.getStoragePathBase() + File.separator +
                firmwareFile.getBrand() + "_" +
                firmwareFile.getModel() + "_" +
                firmwareFile.getVersion() + ".zip");
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(brand);
        out.writeString(model);
        out.writeString(timeout);
        out.writeString(url);
        out.writeString(version);
    }

    public static final Parcelable.Creator<FirmwareFileMetaData> CREATOR = new Creator<FirmwareFileMetaData>() {
        @Override
        public FirmwareFileMetaData createFromParcel(Parcel parcel) {
            return new FirmwareFileMetaData(parcel);
        }

        @Override
        public FirmwareFileMetaData[] newArray(int size) {
            return new FirmwareFileMetaData[size];
        }
    };

    private FirmwareFileMetaData(Parcel in) {
        brand = in.readString();
        model = in.readString();
        timeout = in.readString();
        url = in.readString();
        version = in.readString();
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * Show {@link String} representation of the firmware file.
     *
     * @return String value of teh firmware file.
     */
    @NonNull
    public String toString() {
        // todo should we return something more than url?
        return url != null ? url : "null";
    }
}

package com.zoundindustries.marshallbt.model.salesforce

/**
 * Class wrapping status of subscription action.
 */
data class RequestStatus(val taskType: TaskType,
                         val status: Status,
                         val msg: String?,
                         val address: String?) {

    /**
     * Definition of possible newsletter subscription task types.
     */
    enum class TaskType {
        SUBSCRIBE,
        UNSUBSCRIBE
    }

    /**
     * Definition of possible states.
     */
    enum class Status {
        SUCCESS,
        ERROR
    }

}
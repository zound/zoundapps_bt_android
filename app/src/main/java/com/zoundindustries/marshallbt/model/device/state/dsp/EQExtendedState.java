package com.zoundindustries.marshallbt.model.device.state.dsp;

import com.zoundindustries.marshallbt.model.device.state.BaseDeviceState;
import com.zoundindustries.marshallbt.model.devicesettings.EQExtendedData;
import com.zoundindustries.marshallbt.model.devicesettings.EQPreset;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

public class EQExtendedState extends BaseDeviceState<EQExtendedData> {
    /**
     * Constructor for BaseDeviceState.
     */
    public EQExtendedState() {
        super(FeaturesDefs.EQ_EXTENDED);
    }
}

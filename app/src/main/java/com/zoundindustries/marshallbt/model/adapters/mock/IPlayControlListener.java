package com.zoundindustries.marshallbt.model.adapters.mock;

import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.model.device.BaseDevice;

/**
 * Interface simulating volume changed event for specific {@link BaseDevice}
 */
public interface IPlayControlListener {

    /**
     * Simulated volume changed event
     *
     * @param baseDevice that volume was changed for
     * @param newValue   new volume value
     */
    void onVolumeChanged(@NonNull BaseDevice baseDevice, int newValue);
}

package com.zoundindustries.marshallbt.model.ota.adapter.mock;

public interface IOTAUpdateListener {
    void hasUpdate(boolean hasUpdate);
}

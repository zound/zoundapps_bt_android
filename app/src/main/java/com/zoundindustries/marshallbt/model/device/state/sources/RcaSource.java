package com.zoundindustries.marshallbt.model.device.state.sources;

import com.zoundindustries.marshallbt.model.device.state.BaseDeviceState;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

/**
 * Class extending {@link BaseDeviceState} used to set and monitor rca source
 */
public class RcaSource extends BaseDeviceState<Boolean> {
    /**
     * Constructor for BaseDeviceState.
     */
    public RcaSource() {
        super(FeaturesDefs.RCA_SOURCE);
    }
}

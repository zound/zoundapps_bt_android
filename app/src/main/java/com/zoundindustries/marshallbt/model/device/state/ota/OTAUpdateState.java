package com.zoundindustries.marshallbt.model.device.state.ota;

import com.zoundindustries.marshallbt.model.device.state.BaseDeviceState;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

/**
 * Class extending {@link BaseDeviceState} used to set and monitor volume feature
 * It will be automatically added to all devices with defined FeaturesDefs.OTA_UPDATE feature.
 */
public class OTAUpdateState extends BaseDeviceState<Boolean> {

    /**
     * Constructor for OTAUpdateState. Contains required feature definition.
     */
    public OTAUpdateState() {
        super(FeaturesDefs.OTA_UPDATE);
    }
}
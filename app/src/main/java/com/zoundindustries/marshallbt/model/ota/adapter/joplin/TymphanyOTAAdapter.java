package com.zoundindustries.marshallbt.model.ota.adapter.joplin;

import android.content.Context;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.tym.tymappplatform.TAProtocol.TAProtocol.TASystem;
import com.tym.tymappplatform.TAService.TAOtaSignalDataProcessingService.TAOtaSignalDataProcessingService;
import com.tym.tymappplatform.utils.TACommonDefinitions;
import com.tym.tymappplatform.utils.TACommonDefinitions.OTAStatusType;
import com.tym.tymappplatform.utils.TAErrorDescription;
import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.BaseDevice.ConnectionState;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.state.TymphanyDeviceStateController;
import com.zoundindustries.marshallbt.model.device.tymphany.SpeakerInfo;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.model.ota.IOTAService;
import com.zoundindustries.marshallbt.model.ota.OTAManager;
import com.zoundindustries.marshallbt.model.ota.OtaApi;
import com.zoundindustries.marshallbt.model.ota.OtaErrorThrowable;
import com.zoundindustries.marshallbt.model.ota.OtaErrorThrowable.ErrorType;
import com.zoundindustries.marshallbt.model.ota.SpeakerCacheManager;
import com.zoundindustries.marshallbt.model.ota.pojo.FirmwareFileMetaData;
import com.zoundindustries.marshallbt.ui.ota.OTAActivity;
import com.zoundindustries.marshallbt.utils.FirmwareFileHelper;
import com.zoundindustries.marshallbt.utils.VersionComparator;
import com.zoundindustries.marshallbt.utils.joplin.JoplinOtaMd5Checker;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Class that wraps Joplin SDK with common {@link IOTAService} to provide OTA update
 * functionality and {@link ITymphanyOtaDspServiceListener} to get Tymphany SDK specific callbacks.
 */
public class TymphanyOTAAdapter implements IOTAService, ITymphanyOtaDspServiceListener {

    private static final String TAG = TymphanyOTAAdapter.class.getSimpleName();

    private static final String API_KEY = "iygP2NbSY25sHVzmcf9VA1lN7X5i6Dr016itQgba";
    private static final String FW_SERVER_REQUEST_BRAND = "zound";
    private static final String FW_SERVER_REQUEST_VERSION = "latest";

    private static final int UPDATE_PROGRESS_VALUE_MULTIPLIER = 100;
    private static final int UPDATE_TIMEOUT_MILLIS = 30000;
    private static final int RECONNECTION_TIMEOUT_MILLIS = 100000;
    private static final double TYMPHANY_COMPLETED_PROGRESS = 99.5;
    private static final String FORCED_OTA_VERSION = "6.0.0";

    private final FirmwareFileHelper fwFileHelper;
    private final Context context;

    private BehaviorSubject<Double> updateProgressSubject;
    private BehaviorSubject<UpdateState> updateStateSubject;
    private PublishSubject<OTAStatusType> taOtaStatusSubject;
    private BehaviorSubject<ConnectionState> connectionStateSubject;
    private PublishSubject<BaseDevice> reconnectSubject = PublishSubject.create();

    private final OtaApi otaApi;
    private final TAOtaSignalDataProcessingService taOtaService;
    private final Map<String, BehaviorSubject<Response<FirmwareFileMetaData>>> newFirmwareSubjects;
    private final List<BaseDevice> otaCheckList;
    private final List<BaseDevice> otaCheckedList;
    private final Map<String, BehaviorSubject<Boolean>> otaIndicatorVisibileMap;

    private TASystem taSystem;
    private BaseDevice device;
    private boolean checkInProgress;
    private boolean isOtaInProgress;
    private CountDownTimer updateTimer;
    private CountDownTimer reconnectionTimer;
    private SpeakerCacheManager speakerCacheManager;
    private FirmwareFileMetaData firmwareFileMetaData;
    private JoplinOtaMd5Checker md5Checker;

    private Disposable downloadFileDisposable;
    private Disposable downloadUrlDisposable;
    private Disposable speakerInfoDisposable;
    private Disposable otaStateDisposable;
    private Disposable fileUnzipDisposable;
    private Disposable checkForUpdateDisposable;
    private Disposable reconnectionDisposable;
    private FirebaseAnalyticsManager firebaseAnalyticsManager;
    private Disposable deviceConnectionDisposable;

    /**
     * Constructor with Tymphany OTA service and REST api parameters.
     *
     * @param taOtaService Tymphany SDK service providing OTA functionality.
     * @param otaApi       retrofit api object for getting new FW from web service.
     */
    public TymphanyOTAAdapter(@NonNull TAOtaSignalDataProcessingService taOtaService,
                              @NonNull OtaApi otaApi,
                              @NonNull Context context,
                              @NonNull SpeakerCacheManager speakerCacheManager,
                              @NonNull FirmwareFileHelper firmwareFileHelper,
                              @NonNull Map<String, BehaviorSubject<Boolean>> otaIndicatorVisibileMap) {
        this(taOtaService,
                otaApi,
                firmwareFileHelper,
                new JoplinOtaMd5Checker(firmwareFileHelper),
                context,
                speakerCacheManager,
                ((BluetoothApplication) context.getApplicationContext())
                        .getAppComponent().firebaseWrapper(),
                otaIndicatorVisibileMap);
    }

    /**
     * Constructor with Tymphany OTA service and REST api parameters.
     *
     * @param taOtaService Tymphany SDK service providing OTA functionality.
     * @param otaApi       retrofit api object for getting new FW from web service.
     * @param fwFileHelper helper injection used for tests
     */
    @VisibleForTesting
    public TymphanyOTAAdapter(@NonNull TAOtaSignalDataProcessingService taOtaService,
                              @NonNull OtaApi otaApi,
                              @NonNull FirmwareFileHelper fwFileHelper,
                              @NonNull JoplinOtaMd5Checker md5Checker,
                              @NonNull Context context,
                              @NonNull SpeakerCacheManager speakerCacheManager,
                              @NonNull FirebaseAnalyticsManager firebaseAnalyticsManager,
                              @NonNull Map<String, BehaviorSubject<Boolean>> otaIndicatorVisibileMap) {
        this.taOtaService = taOtaService;
        this.otaApi = otaApi;
        this.fwFileHelper = fwFileHelper;
        this.md5Checker = md5Checker;
        this.context = context;
        this.firebaseAnalyticsManager = firebaseAnalyticsManager;
        this.otaIndicatorVisibileMap = otaIndicatorVisibileMap;

        taOtaStatusSubject = PublishSubject.create();
        connectionStateSubject = BehaviorSubject.create();
        newFirmwareSubjects = new HashMap<>();
        otaCheckList = new ArrayList<>();
        otaCheckedList = new ArrayList<>();

        this.speakerCacheManager = speakerCacheManager;
        initUpdateTimeout();
        initReconnectionTimeout();
        initUpdateStateSubject();
        initUpdateProgressSubject();
    }

    @Override
    @Nullable
    public Observable<Double> getFirmwareUpdateProgress(@NonNull BaseDevice device) {
        return updateProgressSubject;
    }

    @NonNull
    @Override
    public Observable<UpdateState> getFirmwareUpdateState(@NonNull BaseDevice device) {
        return updateStateSubject;
    }

    @Override
    public void startOTAUpdate(@NonNull BaseDevice device,
                               @Nullable FirmwareFileMetaData metaData) {
        isOtaInProgress = true;
        // we're doing init of taSystem twice, here and in checkForUpdates. Can we do so?
        initTASystem(device);

        initUpdateProgressSubject();
        initUpdateStateSubject();

        initConnectionStateSubject(device);

        combineDeviceStatus();

        firmwareFileMetaData = metaData;

        initReconnection(device);
    }

    @Override
    public void didUpdateOtaStatusMessage(TASystem taSystem,
                                          TACommonDefinitions.OTAStatusType otaStatusType,
                                          float v,
                                          TAErrorDescription taErrorDescription) {
        taOtaStatusSubject.onNext(otaStatusType);

        updateProgressSubject.onNext((double) UPDATE_PROGRESS_VALUE_MULTIPLIER * v);
    }

    @Override
    public boolean isOTAInProgress() {
        return isOtaInProgress;
    }

    @Override
    public void finalizeForcedOTA() {
        firebaseAnalyticsManager.eventForcedOtaCheckFinished();
        removeFromOTACheckList(device);
        if (!otaCheckList.isEmpty()) {
            startForcedOtaCheck(otaCheckList.get(0),
                    speakerCacheManager.isSpeakerInfoCached(otaCheckList.get(0).getDeviceInfo().getId()));
        }
    }

    /**
     * Initialize observer for OTA events.
     */
    public void initOtaObserver() {
        taOtaService.registerObserver(this);
    }

    /**
     * Add device for check available OTA updates.
     * Used to indicate forced OTA for the device.
     *
     * @param device that OTA is checked for.
     */
    public void addDeviceToOtaCheckList(@NonNull TymphanyDevice device) {
        if (otaCheckedList.contains(device)) {
            return;
        } else if (!device.isJoplinDevice()) {
            addSpeakerInfoCacheIfNeeded(device.getDeviceInfo().getId());
        }

        String deviceName =
                device.getBaseDeviceStateController().outputs.getSpeakerInfoCurrentValue()
                        .getDeviceName();

        if (!isOtaInProgress && !checkInProgress && otaCheckList.isEmpty()) {
            firebaseAnalyticsManager.eventForcedOtaCheckStarted();
            Log.i(TAG, "addDeviceToOtaCheckList list empty for: " + deviceName);
            otaCheckList.add(device);
            startForcedOtaCheck(device, speakerCacheManager.isSpeakerInfoCached(device.getDeviceInfo().getId()));
        } else if (!otaCheckList.contains(device)) {
            firebaseAnalyticsManager.eventForcedOtaCheckStarted();
            Log.i(TAG, "addDeviceToOtaCheckList add to the checklist for: " + deviceName);
            otaCheckList.add(device);
        }
    }

    @Nullable
    public BehaviorSubject<Response<FirmwareFileMetaData>> checkForFirmwareUpdate(
            @NonNull BaseDevice device, boolean isForced) {
        String deviceName = device.getBaseDeviceStateController().outputs
                .getSpeakerInfoCurrentValue().getDeviceName();

        if (checkInProgress) {
            Log.i(TAG, "checkForFirmwareUpdate already in progress for: " + deviceName);
            return newFirmwareSubjects.get(device.getDeviceInfo().getId());
        } else {
            BehaviorSubject<Response<FirmwareFileMetaData>> subject = BehaviorSubject.create();
            newFirmwareSubjects.put(device.getDeviceInfo().getId(), subject);
            checkInProgress = true;

            initTASystem(device);

            // start checking connection state
            initConnectionStateSubject(device);

            if (!isOtaInProgress &&
                    device.getDeviceInfo().getDeviceType() == DeviceInfo.DeviceType.TYMPHANY_DEVICE) {
                Log.i(TAG, "checkForFirmwareUpdate start downloading file for: " + deviceName);
                String path = ((TymphanyDevice) device).getDeviceSubType().toString().toLowerCase();
                downloadUrlDisposable = otaApi
                        .getDownloadURL(API_KEY,
                                FW_SERVER_REQUEST_BRAND,
                                path,
                                FW_SERVER_REQUEST_VERSION)
                        .timeout(UPDATE_TIMEOUT_MILLIS, TimeUnit.SECONDS)
                        .subscribeOn(Schedulers.io())
                        .subscribe(softwareFileResponse -> {
                                    if (isForced) {
                                        Log.i(TAG, "checkForFirmwareUpdate is forced for: " +
                                                deviceName);

                                        subject.onNext(softwareFileResponse);
                                        handleFirmwareCheckCompleted(subject);
                                    } else {
                                        Log.i(TAG, "checkForFirmwareUpdate is not forced for: " +
                                                deviceName);
                                        addSpeakerInfoCacheIfNeeded(device.getDeviceInfo().getId());
                                        handleUpdateFileResponse(device, softwareFileResponse);
                                    }
                                },
                                throwable -> {
                                    handleFirmwareCheckCompleted(subject);
                                });
            } else {
                handleFirmwareCheckCompleted(subject);
                return null;
            }

            if (newFirmwareSubjects.get(device.getDeviceInfo().getId()) != null) {
                Log.i(TAG, "checkForFirmwareUpdate is NULL for: " + deviceName);
            }

            return newFirmwareSubjects.get(device.getDeviceInfo().getId());
        }
    }

    /**
     * Get {@link Observable} for the device to be reconnected.
     * This is needed for OTA flow. We need to inform device handling services about
     * reconnection need.
     *
     * @return Observable for the device to reconnect.
     */
    @NonNull
    public Observable<BaseDevice> getReconnectDevice() {
        return reconnectSubject;
    }

    /**
     * Clear the registrations and references for the MockOTAAdapter class.
     */
    public void dispose() {
        if (downloadUrlDisposable != null) {
            downloadUrlDisposable.dispose();
        }

        if (downloadFileDisposable != null) {
            downloadFileDisposable.dispose();
        }

        if (speakerInfoDisposable != null) {
            speakerInfoDisposable.dispose();
        }

        if (otaStateDisposable != null) {
            otaStateDisposable.dispose();
        }

        for (Map.Entry<String, BehaviorSubject<Response<FirmwareFileMetaData>>> deviceEntry :
                newFirmwareSubjects.entrySet()) {
            deviceEntry.getValue().onComplete();
        }
    }

    private void initReconnection(@NonNull BaseDevice device) {
        reconnectionDisposable = device.getBaseDeviceStateController().outputs.getConnectionInfo()
                .observeOn(AndroidSchedulers.mainThread())
                .takeUntil(connectionState -> connectionState == ConnectionState.CONNECTED ||
                        connectionState == ConnectionState.DISCONNECTED ||
                        connectionState == ConnectionState.TIMEOUT)
                .subscribe(connectionState -> {
                    if (connectionState == ConnectionState.DISCONNECTED ||
                            connectionState == ConnectionState.TIMEOUT) {
                        startRetryReconnection(device);
                    } else if (connectionState == ConnectionState.CONNECTED && taSystem != null) {
                        updateStateSubject.onNext(UpdateState.DOWNLOADING_FROM_SERVER);
                        getOtaFileAndStartUpdate(taSystem, firmwareFileMetaData);
                    }
                }, throwable -> {
                });
    }

    private void
    startForcedOtaCheck(@NonNull BaseDevice device, boolean isCached) {
        Log.i(TAG, "startForcedOtaCheck init");
        speakerInfoDisposable = device.getBaseDeviceStateController()
                .outputs.getSpeakerInfo()
                .observeOn(AndroidSchedulers.mainThread())
                .timeout(UPDATE_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS)
                .takeUntil(speakerInfo -> !TextUtils.isEmpty(speakerInfo.getFirmwareVersion()))
                .subscribe(speakerInfo -> {
                    if (!TextUtils.isEmpty(speakerInfo.getFirmwareVersion())) {
                        log(speakerInfo);
                        if (isForcedOTASupported(device, isCached, speakerInfo)) {
                            Log.i(TAG, "start forced ota for:"
                                    + speakerInfo.getDeviceName());
                            firebaseAnalyticsManager.eventForcedOtaStarted();
                            startOtaActivity(device);
                        } else {
                            Log.i(TAG, "check for normal ota for:" + speakerInfo.getDeviceName());
                            startNormalOtaCheck(device, speakerInfo);
                        }
                    }
                }, throwable -> startNextForcedOtaCheckIfNeeded(device));
    }

    private boolean isForcedOTASupported(@NonNull BaseDevice device,
                                         boolean isCached,
                                         SpeakerInfo speakerInfo) {
        TymphanyDevice dev = (TymphanyDevice) device;

        return !isCached &&
                device.getDeviceInfo().getDeviceType() == DeviceInfo.DeviceType.TYMPHANY_DEVICE &&
                !dev.isLiteDevice() &&
                dev.getDeviceSubType() != TymphanyDevice.DeviceSubType.OZZY &&
                isOldFirmwareVersion(speakerInfo);
    }

    private boolean isOldFirmwareVersion(@NonNull SpeakerInfo speakerInfo) {
        return VersionComparator.isNewerVersion(VersionComparator
                        .trimVersion(speakerInfo.getFirmwareVersion()),
                FORCED_OTA_VERSION);
    }

    private void startOtaActivity(@NonNull BaseDevice device) {
        context.startActivity(OTAActivity
                .create(context, device.getDeviceInfo().getId()));
        speakerInfoDisposable.dispose();
    }

    /**
     * Check if there is a possibility to run normal OTA procedure via OtaActivity
     *
     * @param device      device to run ota
     * @param speakerInfo device info
     */
    void startNormalOtaCheck(@NonNull BaseDevice device,
                             @NonNull SpeakerInfo speakerInfo) {
        OTAManager otaManager = new OTAManager(context);
        Observable<Response<FirmwareFileMetaData>> firmwareObservable =
                otaManager.checkForFirmwareUpdate(device.getDeviceInfo().getId(), false);
        if (firmwareObservable != null) {
            checkForUpdateDisposable = firmwareObservable
                    .observeOn(AndroidSchedulers.mainThread())
                    .timeout(10, TimeUnit.SECONDS)
                    .subscribe(firmwareFileMetaDataResponse -> {
                                handleLatestFirmwareVersion(
                                        device,
                                        firmwareFileMetaDataResponse.body(),
                                        speakerInfo);
                            },
                            throwable -> finalizeAndCheckForNextForcedOta(device)
                    );
        } else {
            finalizeAndCheckForNextForcedOta(device);
        }
    }

    private void handleLatestFirmwareVersion(@NonNull BaseDevice device,
                                             @Nullable FirmwareFileMetaData fileMetaData,
                                             @NonNull SpeakerInfo speakerInfo) {
        if (fileMetaData != null &&
                !isLatestFirmwareVersion(speakerInfo, fileMetaData.getVersion())) {
            Log.i(TAG, "handleLatestFirmwareVersion for:" + speakerInfo.getDeviceName());

            if (!speakerCacheManager.isSpeakerInfoCached(device.getDeviceInfo().getId())) {
                addSpeakerInfoCacheIfNeeded(speakerInfo.getMacAddress());
                startOtaActivity(device);
            } else {
                Log.i(TAG, "show ota dot for:" + speakerInfo.getDeviceName());
                otaIndicatorVisibileMap.get(
                        device.getBaseDeviceStateController()
                                .outputs.getSpeakerInfoCurrentValue()
                                .getMacAddress())
                        .onNext(true);
                finalizeAndCheckForNextForcedOta(device);
            }
        } else {
            // some null or device already at latest fw version, finish
            finalizeAndCheckForNextForcedOta(device);
        }
    }

    private void finalizeAndCheckForNextForcedOta(@NonNull BaseDevice device) {
        Log.d(TAG, "finalizeAndCheckForNextForcedOta");
        firebaseAnalyticsManager.eventForcedOtaCheckFinished();
        removeFromOTACheckList(device);
        if (!otaCheckList.isEmpty()) {
            startForcedOtaCheck(otaCheckList.get(0),
                    speakerCacheManager.isSpeakerInfoCached(otaCheckList.get(0).getDeviceInfo().getId()));
        }
    }

    private boolean isLatestFirmwareVersion(@NonNull SpeakerInfo speakerInfo,
                                            @NonNull String currentVersion) {
        return !VersionComparator.isNewerVersion(
                VersionComparator.trimVersion(speakerInfo.getFirmwareVersion()),
                currentVersion);
    }

    private void addSpeakerInfoCacheIfNeeded(@Nullable String macAddress) {
        if (macAddress != null && !speakerCacheManager.isSpeakerInfoCached(macAddress)) {
            speakerCacheManager.addSpeakerInfoCache(macAddress);
        }
    }

    private void initTASystem(@NonNull BaseDevice device) {
        this.device = device;
        taSystem = ((TymphanyDeviceStateController) device.getBaseDeviceStateController()).getTaSystem();
    }

    private void initUpdateStateSubject() {
        updateStateSubject = BehaviorSubject.createDefault(UpdateState.NOT_STARTED);
    }

    private void initUpdateProgressSubject() {
        updateProgressSubject = BehaviorSubject.createDefault(0.0);
    }

    private void initUpdateTimeout() {
        updateTimer = new CountDownTimer(UPDATE_TIMEOUT_MILLIS, UPDATE_TIMEOUT_MILLIS) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                sendUpdateError(new OtaErrorThrowable(ErrorType.UNDEFINED_FLASHING,
                        "Update timeout occurred during OTA update"));
            }
        };
    }

    private void initReconnectionTimeout() {
        reconnectionTimer = new CountDownTimer(RECONNECTION_TIMEOUT_MILLIS,
                RECONNECTION_TIMEOUT_MILLIS) {
            @Override
            public void onTick(long millisUntilFinished) {
                //nop
            }

            @Override
            public void onFinish() {
                sendUpdateError(new OtaErrorThrowable(ErrorType.UNDEFINED_FLASHING,
                        "Reconnection timeout occurred during OTA update"));
            }
        };
    }


    private void startRetryReconnection(@NonNull BaseDevice device) {
        updateStateSubject.onNext(UpdateState.RECONNECTING_FOR_RETRY);
        taOtaStatusSubject.onNext(OTAStatusType.NotStarted);
        reconnectSubject.onNext(device);
        updateTimer.start();
    }

    private void handleUpdateFileResponse(@NonNull BaseDevice device,
                                          Response<FirmwareFileMetaData>
                                                  softwareFileResponse) {
        FirmwareFileMetaData body = softwareFileResponse.body();
        BehaviorSubject<Response<FirmwareFileMetaData>> metaDataSubject =
                newFirmwareSubjects.get(device.getDeviceInfo().getId());

        if (body != null) {
            //todo we should not use RX for SpeakerInfo. Should be static content.
            speakerInfoDisposable = device.getBaseDeviceStateController()
                    .outputs
                    .getSpeakerInfo()
                    .filter(speakerInfo -> !TextUtils.isEmpty(speakerInfo.getFirmwareVersion()))
                    .takeUntil(speakerInfo -> !TextUtils.isEmpty(speakerInfo.getFirmwareVersion()))
                    .timeout(UPDATE_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS)
                    .subscribe(speakerInfo -> {
                        if (compareFirmwareVersions(softwareFileResponse, speakerInfo)) {
                            metaDataSubject.onNext(softwareFileResponse);
                            handleFirmwareCheckCompleted(metaDataSubject);
                        } else {
                            handleFirmwareCheckCompleted(metaDataSubject);
                            startNextForcedOtaCheckIfNeeded(device);
                        }
                    }, throwable -> {
                        startNextForcedOtaCheckIfNeeded(device);
                        handleFirmwareCheckCompleted(metaDataSubject);
                    });
        } else {
            metaDataSubject.onError(new Throwable("OTA: No FW found"));

            startNextForcedOtaCheckIfNeeded(device);
            handleFirmwareCheckCompleted(metaDataSubject);
        }
    }

    private void startNextForcedOtaCheckIfNeeded(@NonNull BaseDevice device) {
        firebaseAnalyticsManager.eventForcedOtaCheckFinished();
        if (otaCheckList.contains(device)) {
            removeFromOTACheckList(device);
        }
        if (!otaCheckList.isEmpty()) {
            startForcedOtaCheck(otaCheckList.get(0),
                    speakerCacheManager.isSpeakerInfoCached(otaCheckList.get(0).getDeviceInfo().getId()));
        }
    }

    private void handleFirmwareCheckCompleted(
            @NonNull BehaviorSubject<Response<FirmwareFileMetaData>> metaDataSubject) {
        Log.i(TAG, "checkForFirmwareUpdate is completed");
        metaDataSubject.onComplete();
        newFirmwareSubjects.remove(metaDataSubject);
        checkInProgress = false;
    }

    private void getOtaFileAndStartUpdate(@NonNull TASystem taSystem,
                                          @Nullable FirmwareFileMetaData fileMetaData) {
        if (fileMetaData != null) {
            File saveLocation = fwFileHelper.getFirmwareZipFile(fileMetaData);

            String url = fileMetaData.getUrl();
            if (!saveLocation.exists() && url != null) {
                downloadOtaFileAndStartUpdate(url, fileMetaData.getVersion(), taSystem, saveLocation);
            } else {
                unzipAndPerformOta(taSystem, saveLocation, fileMetaData.getVersion());
            }
        } else {
            sendUpdateError(new OtaErrorThrowable(ErrorType.NULL_WEB_DATA));
        }
    }

    private void downloadOtaFileAndStartUpdate(@NonNull String url,
                                               @NonNull String version,
                                               @NonNull TASystem taSystem,
                                               @NonNull File saveLocation) {
        downloadFileDisposable = otaApi.downloadOtaFile(API_KEY, url)
                .subscribeOn(Schedulers.io())
                .subscribe(responseBodyResponse -> {
                    if (responseBodyResponse.body() != null &&
                            saveFirmwareToFile(responseBodyResponse, saveLocation)) {
                        unzipAndPerformOta(taSystem, saveLocation, version);
                    } else {
                        //todo if saveFirmwareToFile fails, this won't be null server response error
                        sendUpdateError(new OtaErrorThrowable(ErrorType.NULL_SERVER_RESPONSE));
                    }
                }, throwable -> sendUpdateError(new OtaErrorThrowable(ErrorType.UNDEFINED_DOWNLOAD,
                        throwable.getMessage())));
    }

    private void unzipAndPerformOta(@NonNull TASystem taSystem, @NonNull File
            saveLocation, String version) {
        updateStateSubject.onNext(UpdateState.UNZIPPING);
        Log.i(TAG, "unzipAndPerformOta UNZIPPING for:" + taSystem.deviceName);
        fileUnzipDisposable = fwFileHelper.unzipFirmwareFiles(saveLocation)
                .take(1)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(isFileUnzipped -> {
                    if (isFileUnzipped) {
                        if (md5Checker.checkHexFileMD5()) {
                            Log.i(TAG, "unzipAndPerformOta UNZIPPED for:" +
                                    taSystem.deviceName);
                            updateStateSubject.onNext(UpdateState.UPLOADING_TO_DEVICE);
                            taOtaService.startOtaUpdate(taSystem,
                                    fwFileHelper.getFirmwareBinaryFile(),
                                    version);
                        } else {
                            // todo remove the files if the checksum is wrong
                            sendUpdateError(new OtaErrorThrowable(ErrorType.UNDEFINED_DOWNLOAD,
                                    "Md5 checksum mismatch"));
                        }
                    } else {
                        sendUpdateError(new OtaErrorThrowable(ErrorType.UNDEFINED_DOWNLOAD,
                                "File unzip error"));
                    }
                });
    }

    private void combineDeviceStatus() {
        if (otaStateDisposable == null || otaStateDisposable.isDisposed()) {
            otaStateDisposable = Observable.combineLatest(
                    connectionStateSubject,
                    taOtaStatusSubject,
                    this::handleUpdateStatus)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe();
        }
    }

    private Boolean handleUpdateStatus(ConnectionState connectionState,
                                       OTAStatusType otaStatusType) {
        switch (connectionState) {
            case CONNECTED:
                if (isOtaInProgress && updateStateSubject.getValue() ==
                        UpdateState.RECONNECTING_FOR_RETRY) {
                    if (taSystem != null && firmwareFileMetaData != null) {
                        updateTimer.cancel();
                        updateStateSubject.onNext(UpdateState.DOWNLOADING_FROM_SERVER);
                        getOtaFileAndStartUpdate(taSystem, firmwareFileMetaData);
                        break;
                    }
                }
                handleConnectedStatus(otaStatusType);
                break;
            case DISCONNECTED:
                handleDisconnectedStatus(otaStatusType);
                break;
            case TIMEOUT:
                if (otaStatusType != OTAStatusType.NotStarted) {
                    sendUpdateError(new OtaErrorThrowable(ErrorType.UNDEFINED,
                            "Connection timeout occured"));
                }
                break;

        }
        String progress = (updateProgressSubject != null
                && updateProgressSubject.getValue() != null) ?
                updateProgressSubject.getValue().toString() : "0";
        Log.i(TAG, "Combining:" +
                "\n\tconnection:\t" + connectionState.name() +
                "\n\tota:       \t" + otaStatusType.name() + "\n" +
                "\n\tprogress   \t" + progress);
        return true;
    }

    private void handleConnectedStatus(OTAStatusType otaStatusType) {
        switch (otaStatusType) {
            case Paused:
            case Validated:
            case NotStarted:
            case OTAReady:
                if (isOtaInProgress) {
                    updateTimer.start();
                }
                break;
            case Downloading:
                isOtaInProgress = true;
                updateTimer.cancel();
                updateTimer.start();
                updateStateSubject.onNext(UpdateState.UPLOADING_TO_DEVICE);
                break;
            case DownloadFinished:
                isOtaInProgress = true;
                break;
            case Activating:
                isOtaInProgress = true;
                updateTimer.cancel();
                reconnectionTimer.start();
                updateStateSubject.onNext(UpdateState.FLASHING_DEVICE);
                break;
            case MCUUpgrading:
                reconnectionTimer.cancel();
                isOtaInProgress = true;
                updateTimer.cancel();
                //Sometimes SDK is finished on 99.79 so we decide not to start timer
                // anymore above that
                if (updateProgressSubject.getValue() != null
                        && updateProgressSubject.getValue() < TYMPHANY_COMPLETED_PROGRESS) {
                    updateTimer.start();
                }
                updateStateSubject.onNext(UpdateState.FLASHING_DEVICE);
                break;
            case MCUUpgradeFinish:
                fwFileHelper.deleteUnzippedFiles();
                addSpeakerInfoCacheIfNeeded(device.getDeviceInfo().getId());
                isOtaInProgress = false;
                updateTimer.cancel();
                updateStateSubject.onNext(UpdateState.COMPLETED);
                updateStateSubject.onComplete();
                updateProgressSubject.onComplete();
                otaStateDisposable.dispose();

                otaIndicatorVisibileMap.get(
                        device.getBaseDeviceStateController()
                                .outputs.getSpeakerInfoCurrentValue()
                                .getMacAddress())
                        .onNext(false);
                break;
            case Error:
                sendUpdateError(new OtaErrorThrowable(ErrorType.SDK_FLASHING_ERROR));
                break;
        }
    }

    private void handleDisconnectedStatus(OTAStatusType otaStatusType) {
        if (updateStateSubject.getValue() == UpdateState.RECONNECTING_FOR_RETRY) {
            return;
        }
        updateTimer.cancel();
        if (otaStatusType == OTAStatusType.Error ||
                otaStatusType == OTAStatusType.Downloading ||
                (otaStatusType == OTAStatusType.MCUUpgrading &&
                        (updateProgressSubject.getValue() == null ||
                                updateProgressSubject.getValue() < TYMPHANY_COMPLETED_PROGRESS))) {
            sendUpdateError(new OtaErrorThrowable(ErrorType.SDK_FLASHING_ERROR));
            return;
        }

        updateStateSubject.onNext(UpdateState.REBOOT_NEEDED);
        reconnectSubject.onNext(device);
    }

    private boolean saveFirmwareToFile(@NonNull Response<ResponseBody> responseBodyResponse,
                                       @NonNull File saveLocation) {
        ResponseBody body = responseBodyResponse.body();
        return body != null && fwFileHelper.writeResponseBodyToDisk(body, saveLocation);
    }

    private void initConnectionStateSubject(@NonNull BaseDevice device) {
        if (otaStateDisposable == null || otaStateDisposable.isDisposed()) {
            deviceConnectionDisposable = device.getBaseDeviceStateController()
                    .outputs.getConnectionInfo()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(connectionState -> {
                        if (connectionState == ConnectionState.CONNECTED) {
                            taOtaService.subscribeOTAStatue(taSystem);
                        } else if (connectionState == ConnectionState.DISCONNECTED) {
                            taOtaService.unSubscribeOTAStatue(taSystem);
                        }
                        connectionStateSubject.onNext(connectionState);
                    });
        }
    }

    private boolean compareFirmwareVersions(
            @NonNull Response<FirmwareFileMetaData> softwareFileResponse,
            @NonNull SpeakerInfo speakerInfo) {
        return VersionComparator
                .isNewerVersion(
                        VersionComparator.trimVersion(speakerInfo.getFirmwareVersion()),
                        softwareFileResponse.body().getVersion());
    }

    private void removeFromOTACheckList(BaseDevice device) {
        otaCheckList.remove(device);
        otaCheckedList.add(device);
    }

    private void log(SpeakerInfo speakerInfo) {
        Log.i(TAG, "received speaker info:" +
                "\n\tname:\t" + speakerInfo.getDeviceName() +
                "\n\tFW version:       \t" + speakerInfo.getFirmwareVersion());
    }

    private void sendUpdateError(Throwable throwable) {
        isOtaInProgress = false;
        updateTimer.cancel();
        reconnectionTimer.cancel();

        taOtaService.stopOtaUpdate(taSystem);
        fwFileHelper.deleteUnzippedFiles();

        if (updateStateSubject != null && updateStateSubject.hasObservers()) {
            updateStateSubject.onError(throwable);
        }

        if (updateProgressSubject != null && updateProgressSubject.hasObservers()) {
            updateProgressSubject.onError(throwable);
        }

        if (otaStateDisposable != null) {
            otaStateDisposable.dispose();
        }

        if (fileUnzipDisposable != null) {
            fileUnzipDisposable.dispose();
        }

        if (checkForUpdateDisposable != null && !checkForUpdateDisposable.isDisposed()) {
            checkForUpdateDisposable.dispose();
        }

        if (reconnectionDisposable != null) {
            reconnectionDisposable.dispose();
        }

        if (deviceConnectionDisposable != null) {
            deviceConnectionDisposable.dispose();
        }
    }
}

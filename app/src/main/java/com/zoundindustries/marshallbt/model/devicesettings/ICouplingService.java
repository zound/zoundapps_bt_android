package com.zoundindustries.marshallbt.model.devicesettings;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zoundindustries.marshallbt.model.device.BaseDevice;

import io.reactivex.Observable;

/**
 * Interface defining Coupling service methods.
 */
public interface ICouplingService {

    /**
     * Method providing devices that are available for coupling.
     *
     * @return list of available devices or empty list if no device available.
     */
    @Nullable
    default Observable<BaseDevice> startScanForReadyToCoupleDevices(@NonNull BaseDevice device) {
        throw new UnsupportedOperationException();
    }

    /**
     * Method allowing to stop ongoing check for devices ready to couple.
     * Service is not aware of the states of clients so we need to make it possible to
     * stop ongoing check.
     */
    default void stopGetDevicesReadyToCouple() {
        throw new UnsupportedOperationException();
    }
}

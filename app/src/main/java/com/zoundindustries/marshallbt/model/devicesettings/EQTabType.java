package com.zoundindustries.marshallbt.model.devicesettings;

import com.tym.tymappplatform.utils.TACommonDefinitions;

/**
 * Types of eq tabs that are displayed in the tabbed EQ. Wrapper for EQStepsType
 */
public enum EQTabType {
    M1(0) {
        @Override
        public TACommonDefinitions.EQStepsType toStepsType() {
            return TACommonDefinitions.EQStepsType.FLAT;
        }
    },
    M2(1) {
        @Override
        public TACommonDefinitions.EQStepsType toStepsType() {
            return TACommonDefinitions.EQStepsType.EQStepsI;
        }
    },
    M3(2) {
        @Override
        public TACommonDefinitions.EQStepsType toStepsType() {
            return TACommonDefinitions.EQStepsType.EQStepsII;
        }
    };

    private int intValue;

    public int getIntValue() {
        return  intValue;
    }

    EQTabType(int value) {
        intValue = value;
    }

    public static EQTabType from(TACommonDefinitions.EQStepsType eqStepsType) {
        switch (eqStepsType) {
            case FLAT:
                return M1;
            case EQStepsI:
                return M2;
            case EQStepsII:
                return M3;
        }
        return null;
    }

    public abstract TACommonDefinitions.EQStepsType toStepsType();
}

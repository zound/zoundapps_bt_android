package com.zoundindustries.marshallbt.model.coupling;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.BaseDevice.CouplingChannelType;
import com.zoundindustries.marshallbt.model.device.BaseDevice.CouplingConnectionState;

/**
 * Class providing full information about coupling for the specific device.
 */
public class DeviceCouplingInfo {

    private BaseDevice.CouplingChannelType channelType;
    private BaseDevice.CouplingConnectionState couplingConnectionInfoState;
    private String peerMacAddress;

    /**
     * Default constructor for {@link DeviceCouplingInfo}
     */
    public DeviceCouplingInfo() {
        channelType = BaseDevice.CouplingChannelType.PARTY;
        couplingConnectionInfoState = BaseDevice.CouplingConnectionState.NOT_COUPLED;
        peerMacAddress = "";
    }

    /**
     * Constructor for setting all values.
     *
     * @param channelType                 for the coupled connection.
     * @param couplingConnectionInfoState current coupling connection state.
     * @param peerMacAddress              MAC address of the currently coupled device.
     */
    public DeviceCouplingInfo(@NonNull CouplingChannelType channelType,
                              @NonNull CouplingConnectionState couplingConnectionInfoState,
                              @Nullable String peerMacAddress) {
        this.channelType = channelType;
        this.couplingConnectionInfoState = couplingConnectionInfoState;
        this.peerMacAddress = peerMacAddress;
    }

    public CouplingChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(CouplingChannelType channelType) {
        this.channelType = channelType;
    }

    public CouplingConnectionState getCouplingConnectionInfoState() {
        return couplingConnectionInfoState;
    }

    public void setCouplingConnectionInfoState(CouplingConnectionState couplingConnectionInfoState) {
        this.couplingConnectionInfoState = couplingConnectionInfoState;
    }

    public String getPeerMacAddress() {
        return peerMacAddress;
    }

    public void setPeerMacAddress(String peerMacAddress) {
        this.peerMacAddress = peerMacAddress;
    }
}

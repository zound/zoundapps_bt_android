package com.zoundindustries.marshallbt.model;

import android.content.Context;
import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.model.autopairing.BondState;
import com.zoundindustries.marshallbt.services.DeviceService;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

/**
 * Base class for all managers that need DeviceService communication.
 * Implements service connection with callbacks.
 */
public abstract class BaseManager {

    private final Context context;
    protected DeviceService deviceService;

    protected boolean isBound;

    private Disposable serviceConnectionDisposable;
    private Observable<Boolean> serviceObservable;

    /**
     * Constructor for BaseManager invoking {@link DeviceService} connection.
     *
     * @param context used for binding with service.
     */
    public BaseManager(@NonNull Context context) {
        this.context = context;
        initObservers();
    }

    /**
     * Get Observable on service connection.
     *
     * @return observable on the state.
     */
    @NonNull
    public Observable<Boolean> isServiceConnected() {
        return serviceObservable;
    }

    /**
     * Clear connection observable.
     */
    public void dispose() {
        serviceConnectionDisposable.dispose();
    }

    /**
     * Get observable for service availability.
     * Client should wait for service to be ready and perform any further calls on service
     * after getting true value from this observable.
     * <p>
     * Service will initialize all dependent services in the background.
     *
     * @return true when service is ready to handle incoming requests, false if initialization
     * failed.
     */
    @NonNull
    public Observable<Boolean> isServiceReady() {
        BluetoothApplication app = (BluetoothApplication) context.getApplicationContext();
        return app.isServiceReady();
    }


    @NonNull
    public Observable<BondState> getSystemBondingState() {
        BluetoothApplication app = (BluetoothApplication) context.getApplicationContext();
        return app.getSystemBondingState();
    }

    @NonNull
    public Observable<Boolean> getSystemBtState() {
        BluetoothApplication app = (BluetoothApplication) context.getApplicationContext();
        return app.getSystemBtState();
    }

    private void initObservers() {
        BluetoothApplication app = (BluetoothApplication) context.getApplicationContext();
        serviceObservable = app.isServiceConnected();
        serviceConnectionDisposable = serviceObservable.subscribe(connected -> {
            if (connected) {
                deviceService = app.getDeviceService();
            }
            isBound = connected;
        });
    }
}

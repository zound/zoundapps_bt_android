package com.zoundindustries.marshallbt.model.device;

import androidx.annotation.NonNull;

import java.util.Objects;

/**
 * Class defining unique device information. Used for unambiguous device identification.
 */
public class DeviceInfo {


    /**
     * Enum defining hardware/SDK type of the device.
     */
    public enum DeviceType {
        MOCK,
        TYMPHANY_DEVICE
    }

    /**
     * Enum defining functional group of device.
     */
    public enum DeviceGroup {
        SPEAKER,
        HEADPHONES
    }

    /**
     * Enum defining the color of the device.
     */
    public enum DeviceColor {
        BLACK,
        WHITE,
        BROWN
    }

    private final DeviceType deviceType;
    private final DeviceGroup deviceGroup;
    private final DeviceColor deviceColor;
    private String id;
    private String productName;

    /**
     * Constructor for DeviceInfo.
     *
     * @param id         unique identifier for the device.
     *                   It can be different type for different SDK (e.g. serial number, MAC, etc).
     *                   It has to be unique in the scope of this app.
     * @param deviceType defining type of hardware/SDK of the device.
     */
    public DeviceInfo(@NonNull String id,
                      @NonNull DeviceType deviceType,
                      @NonNull DeviceGroup deviceGroup,
                      @NonNull String productName,
                      @NonNull DeviceColor deviceColor) {
        this.deviceType = deviceType;
        this.deviceGroup = deviceGroup;
        this.deviceColor = deviceColor;
        this.id = id;
        this.productName = productName;
    }

    /**
     * Getter for device unique Id.
     *
     * @return Id.
     */
    public String getId() {
        return id;
    }

    /**
     * Getter for device hardware/SDK type.
     *
     * @return device type.
     */
    public DeviceType getDeviceType() {
        return deviceType;
    }

    /**
     * Getter for device functional group.
     *
     * @return device group.
     */
    public DeviceGroup getDeviceGroup() {
        return deviceGroup;
    }

    /**
     * Getter for device product name.
     *
     * @return product name.
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Getter for device color
     *
     * @return color of the device
     */
    @NonNull
    public DeviceColor getDeviceColor() {
        return deviceColor;
    }

    /**
     * Setter for device id.
     *
     * @param id to be set.
     */
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeviceInfo that = (DeviceInfo) o;
        return deviceType == that.deviceType &&
                Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deviceType, id);
    }
}

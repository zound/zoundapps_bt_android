package com.zoundindustries.marshallbt.model.discovery.adapter.joplin;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

import com.tym.tymappplatform.TAProtocol.TAProtocol.TAProtocol;
import com.tym.tymappplatform.TAProtocol.TAProtocol.TASystem;
import com.tym.tymappplatform.TAService.TADigitalSignalProcessingService.TADigitalSignalProcessingService;
import com.tym.tymappplatform.TAService.TAPlayControlService.TAPlayControlService;
import com.tym.tymappplatform.TAService.TASystemService.TASystemService;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice.DeviceSubType;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDeviceFactory;
import com.zoundindustries.marshallbt.model.discovery.IDiscoveryService;
import com.zoundindustries.marshallbt.services.DeviceService;
import com.zoundindustries.marshallbt.utils.BluetoothSystemUtils;
import com.zoundindustries.marshallbt.utils.IListUpdateCallbackWrapper;
import com.zoundindustries.marshallbt.utils.ListDiffCallback;
import com.zoundindustries.marshallbt.utils.joplin.SpeakerColorUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Class that wraps Tymhpany SDK with common {@link IDiscoveryService} to provide devices discovery
 * functionality and {@link ITymphanySystemServiceListener} to get Tymphany SDK specific callbacks.
 */
@SuppressWarnings("unchecked")
public class TymphanyDeviceDiscoveryAdapter implements IDiscoveryService,
        ITymphanySystemServiceListener {

    //bytecodes for the type of speakers
    private static final byte JOPLIN_S_TYPE_BYTECODE = 0x00;
    private static final byte JOPLIN_M_TYPE_BYTECODE = 0x01;
    private static final byte JOPLIN_L_TYPE_BYTECODE = 0x02;
    private static final byte JOPLIN_S_LITE_TYPE_BYTECODE = 0x03;
    private static final byte JOPLIN_M_LITE_TYPE_BYTECODE = 0x04;
    private static final byte JOPLIN_L_LITE_TYPE_BYTECODE = 0x05;
    private static final byte OZZY_TYPE_BYTECODE = 0x10;

    //bytecodes for the color of speakers
    private static final byte JOPLIN_NOT_SUPPORTED_COLOR_BYTECODE = 0x10;
    private static final byte JOPLIN_WHITE_COLOR_BYTECODE = 0x11;
    private static final byte JOPLIN_BLACK_COLOR_BYTECODE = 0x12;
    private static final byte JOPLIN_BROWN_COLOR_BYTECODE = 0x13;

    private PublishSubject<BaseDevice> deviceAddedSubject = PublishSubject.create();
    private PublishSubject<BaseDevice> deviceRemovedSubject = PublishSubject.create();

    private TASystemService taSystemService;

    private final TAPlayControlService taPlayControlService;

    private TADigitalSignalProcessingService taDspService;

    private List<BaseDevice> joplinDevices;
    private List<BaseDevice> temporaryScanWatcherList;

    /**
     * Constructor for TymphanyDeviceDiscoveryAdapter.
     *
     * @param systemService that provides discovery functionality to the adapter.
     */
    public TymphanyDeviceDiscoveryAdapter(@NonNull TASystemService systemService,
                                          @NonNull TAPlayControlService taPlayControlService,
                                          @NonNull TADigitalSignalProcessingService taDspService) {
        joplinDevices = Collections.synchronizedList(new ArrayList<BaseDevice>());
        temporaryScanWatcherList = new ArrayList<>();

        this.taSystemService = systemService;
        this.taPlayControlService = taPlayControlService;

        // todo should we register dsp observer or not?
        this.taDspService = taDspService;
    }

    /**
     * Register observer for discovery events.
     */
    public void initDiscoveryObserver() {
        this.taSystemService.registerObserver(this);
    }

    @Override
    public void startScanForDevices() {
        //Removed for JMARAPP-714. Let's keep it for possible future releases.
        //temporaryScanWatcherList.clear();
        taSystemService.startScanSystems();
    }

    @NonNull
    @Override
    public Observable<BaseDevice> getDiscoveredDevices() {
        return deviceAddedSubject;
    }

    @NonNull
    @Override
    public Observable<BaseDevice> getDisappearedDevices() {
        return deviceRemovedSubject;
    }

    @Override
    public synchronized void stopScanForDevices() {
        taSystemService.stopScanSystems();
        //Removed for JMARAPP-714. Let's keep it for possible future releases.
        //handleDevicesListChange();
    }

    private void handleDevicesListChange() {
        ListDiffCallback callback = new ListDiffCallback(joplinDevices, temporaryScanWatcherList);
        DiffUtil.calculateDiff(callback).dispatchUpdatesTo(new IListUpdateCallbackWrapper() {
            @Override
            public void onRemoved(int position, int count) {
                removeDeviceIfNotPaired(position, count);
            }
        });
    }

    private synchronized void removeDeviceIfNotPaired(int position, int count) {
        List<BaseDevice> devicesToBeRemoved = new ArrayList<>();
        for (int i = position; i <= position + count - 1; i++) {
            BaseDevice device = joplinDevices.get(i);
            if (!new BluetoothSystemUtils().isDevicePaired(device)) {
                deviceRemovedSubject.onNext(device);
                devicesToBeRemoved.add(device);
            }
        }
        joplinDevices.removeAll(devicesToBeRemoved);
    }

    @Override
    public void didDiscoverSystem(TASystem taSystem, int i, byte[] bytes) {
        DeviceSubType type = getSpeakerTypeFromBytes(bytes);

        if (type == null) {
            //todo should we skip device with corrupted model info?
            return;
        }

        BaseDevice device = getDeviceFromSystem(taSystem);
        DeviceInfo.DeviceColor deviceColor = getDeviceColorFromBytes(type, bytes,
                taSystem.deviceAddress);

        if (!joplinDevices.contains(device)) {
            BaseDevice newSpeaker = TymphanyDeviceFactory.getTymphanyDevice(
                    type,
                    deviceColor,
                    taSystem,
                    taSystemService,
                    taPlayControlService,
                    taDspService);

            joplinDevices.add(newSpeaker);
            deviceAddedSubject.onNext(newSpeaker);

            //Removed for JMARAPP-714. Let's keep it for possible future releases.
            //addDeviceToTempScanWatcherList(newSpeaker);
        } else {

            //This is workaround needed for randomized MAC address.
            //We need to update internal systemAddress field as soon as it is changed.
            TymphanyDevice tymDevice = (TymphanyDevice) device;
            if (tymDevice != null && isTaSystemUpdateNeeded(tymDevice, taSystem)) {
                for (TASystem system : taSystemService.getConnectedSystems()) {
                    if (system.deviceAddress.equals(taSystem.deviceAddress)) {
                        deviceAddedSubject.onNext(device);
                        return;
                    }
                }
                tymDevice.setTaSystem(taSystem);
            }
            //Removed for JMARAPP-714. Let's keep it for possible future releases.
            //addDeviceToTempScanWatcherList(device);
            deviceAddedSubject.onNext(device);
        }
    }

    /**
     * This method is used to update TaSystem.systemAddress.
     * systemAddress is a private address used for BLE. It is randomized and we need to update it in
     * certain situations. It is needed to keep our BaseDevice object updated and always be able
     * to establish BLE connection with current private address.
     *
     * @param system to be checked and updated if needed.
     */
    private void updateTaSystem(TASystem system) {
        for (BaseDevice device : joplinDevices) {
            TymphanyDevice tymDev = (TymphanyDevice) device;
            if (device.getDeviceInfo().getId().equals(system.deviceAddress) &&
                    isTaSystemUpdateNeeded(tymDev, system)) {
                ((TymphanyDevice) device).setTaSystem(system);
            }
        }
    }

    private boolean isTaSystemUpdateNeeded(TymphanyDevice device, TASystem taSystem) {
        return !device.getTaSystem().systemAddress.equals(taSystem.systemAddress);
    }

    @Override
    public void didUpdateDeviceInformation(TASystem taSystem,
                                           byte[] bytes,
                                           String s,
                                           byte[] bytes1) {

    }

    @Override
    public void didUpdateAutoOffTimerStatus(TASystem taSystem, int i) {
        //TODO: implement when needed
    }

    @Override
    public void didUpdatePairingStatus(TASystem taSystem, int i) {

    }

    @Override
    public void didConnectionStateChanged(TASystem taSystem, int i) {
        if (i == TAProtocol.kDeviceStatusConnected) {
            updateTaSystem(taSystem);
        }
    }

    /**
     * Remove the {@link BaseDevice} from the {@link DeviceService}.
     */
    public void removeDevice(@NonNull BaseDevice device) {
        joplinDevices.remove(device);
    }

    private void addDeviceToTempScanWatcherList(BaseDevice newSpeaker) {
        if (!temporaryScanWatcherList.contains(newSpeaker)) {
            temporaryScanWatcherList.add(newSpeaker);
        }
    }

    /**
     * Clean all rx and callbacks references.
     */
    public void dispose() {
        taSystemService.unregisterObserver(this);
    }

    private synchronized BaseDevice getDeviceFromSystem(TASystem system) {
        for (BaseDevice device : joplinDevices) {
            if (device.getDeviceInfo().getId().equals(system.deviceAddress)) {
                return device;
            }
        }
        return null;
    }

    @Nullable
    private DeviceSubType getSpeakerTypeFromBytes(byte[] bytes) {
        if (bytes == null) {
            return null;
        }

        DeviceSubType returnType = null;

        switch (bytes[1]) {
            case JOPLIN_S_TYPE_BYTECODE:
                returnType = DeviceSubType.JOPLIN_S;
                break;
            case JOPLIN_M_TYPE_BYTECODE:
                returnType = DeviceSubType.JOPLIN_M;
                break;
            case JOPLIN_L_TYPE_BYTECODE:
                returnType = DeviceSubType.JOPLIN_L;
                break;
            case JOPLIN_S_LITE_TYPE_BYTECODE:
                returnType = DeviceSubType.JOPLIN_S_LITE;
                break;
            case JOPLIN_M_LITE_TYPE_BYTECODE:
                returnType = DeviceSubType.JOPLIN_M_LITE;
                break;
            case JOPLIN_L_LITE_TYPE_BYTECODE:
                returnType = DeviceSubType.JOPLIN_L_LITE;
                break;
            case OZZY_TYPE_BYTECODE:
                returnType = DeviceSubType.OZZY;
                break;
        }
        return returnType;
    }

    @NonNull
    private DeviceInfo.DeviceColor getDeviceColorFromBytes(@NonNull DeviceSubType type,
                                                           @NonNull byte[] bytes,
                                                           @NonNull String macAddress) {
        switch (bytes[0]) {
            case JOPLIN_NOT_SUPPORTED_COLOR_BYTECODE:
                return SpeakerColorUtils.isWhiteSpeaker(type,
                        macAddress.replace(":", ""));
            case JOPLIN_WHITE_COLOR_BYTECODE:
                return DeviceInfo.DeviceColor.WHITE;
            case JOPLIN_BROWN_COLOR_BYTECODE:
                return DeviceInfo.DeviceColor.BROWN;
            case JOPLIN_BLACK_COLOR_BYTECODE:
            default:
                return DeviceInfo.DeviceColor.BLACK;
        }
    }
}

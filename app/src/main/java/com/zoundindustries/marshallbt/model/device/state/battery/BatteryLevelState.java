package com.zoundindustries.marshallbt.model.device.state.battery;

import com.zoundindustries.marshallbt.model.device.state.BaseDeviceState;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

/**
 * Class extending {@link BaseDeviceState} used to monitor information about battery level of speaker.
 */
public class BatteryLevelState extends BaseDeviceState<Integer> {
    /**
     * Constructor for BaseDeviceState. Contains required feature definition.
     */
    public BatteryLevelState() { super(FeaturesDefs.BATTERY_LEVEL); }
}

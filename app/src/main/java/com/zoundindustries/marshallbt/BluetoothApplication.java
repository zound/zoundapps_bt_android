package com.zoundindustries.marshallbt;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import androidx.annotation.NonNull;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.zoundindustries.marshallbt.di.AppComponent;
import com.zoundindustries.marshallbt.di.AppInjector;
import com.zoundindustries.marshallbt.di.AppModule;
import com.zoundindustries.marshallbt.model.autopairing.BondState;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.services.DeviceService;
import com.zoundindustries.marshallbt.ui.error.types.BluetoothErrorActivity;
import com.zoundindustries.marshallbt.utils.BluetoothSystemUtils;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

import static android.bluetooth.BluetoothDevice.ACTION_BOND_STATE_CHANGED;
import static android.bluetooth.BluetoothDevice.EXTRA_BOND_STATE;
import static android.bluetooth.BluetoothDevice.EXTRA_DEVICE;

/**
 * BluetoothApplication class with setup for Activity injection.
 */
public class BluetoothApplication extends Application implements HasActivityInjector {

    @Inject
    DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

    private BehaviorSubject<Boolean> serviceConnectionSubject = BehaviorSubject.create();
    private BehaviorSubject<Boolean> serviceReadySubject = BehaviorSubject.create();
    private PublishSubject<BondState> bondState = PublishSubject.create();
    private PublishSubject<Boolean> systemBtState = PublishSubject.create();

    private DeviceService deviceService;
    private RefWatcher refWatcher;

    /**
     * Static method providing refWatcher to check for memory leaks with LeakCanary.
     * Should used in onDestroy method for long lasting objects (e.g. Fragments, Services).
     * <p>
     * Exaple usage:
     * RefWatcher refWatcher = BluetoothApplication.getRefWatcher(getActivity());
     * refWatcher.watch(this);
     *
     * @param context for getting the application context.
     * @return RefWatcher object.
     */
    public static RefWatcher getRefWatcher(Context context) {
        BluetoothApplication application = (BluetoothApplication) context.getApplicationContext();
        return application.refWatcher;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }
        refWatcher = LeakCanary.install(this);

        AppInjector.init(this, new AppModule(this));

        setupCalligraphy();
        connectDeviceService();

        registerBluetoothBroadcastReceiver();
        registerBTBondStateReceiver();
    }

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }

    /**
     * Get Observable on service connection.
     *
     * @return observable on the state.
     */
    @NonNull
    public Observable<Boolean> isServiceConnected() {
        return serviceConnectionSubject.hide();
    }

    /**
     * Get Observable on service initialization completion.
     *
     * @return observable on the state.
     */
    @NonNull
    public Observable<Boolean> isServiceReady() {
        return serviceReadySubject;
    }

    /**
     * Get Observable on system bond state.
     *
     * @return observable on the state.
     */
    @NonNull
    public Observable<BondState> getSystemBondingState() {
        return bondState;
    }

    /**
     * Get Observable on system BT state.
     *
     * @return observable on the state.
     */
    @NonNull
    public Observable<Boolean> getSystemBtState() {
        return systemBtState;
    }

    /**
     * Get currently connected device service.
     * The service is main service keeping all devices data.
     *
     * @return the service.
     */
    @NonNull
    public DeviceService getDeviceService() {
        return deviceService;
    }

    /**
     * Get dagger component to allow using singletons from AppModule.
     *
     * @return DaggerAppComponent instance.
     */
    @NonNull
    public AppComponent getAppComponent() {
        return AppInjector.getDaggerAppComponent();
    }

    private void connectDeviceService() {
        ServiceConnection connection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                DeviceService.LocalBinder binder = (DeviceService.LocalBinder) iBinder;
                deviceService = binder.getService();
                serviceConnectionSubject.onNext(true);
                initServiceReadyObserver();
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                serviceConnectionSubject.onNext(false);
                serviceReadySubject.onNext(false);
            }
        };

        Intent intent = new Intent(this, DeviceService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @SuppressLint("CheckResult")
    private void initServiceReadyObserver() {
        deviceService.isServiceReady().subscribe(serviceReadySubject::onNext);
    }

    private void setupCalligraphy() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Roboto-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    private void registerBluetoothBroadcastReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);

        registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();

                if (!BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                    return;
                }

                int btState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);
                if (btState == BluetoothAdapter.STATE_OFF) {
                    systemBtState.onNext(false);
                    deviceService.disconnectConnectedDevices();
                    startActivity(BluetoothErrorActivity.create(getApplicationContext()));
                } else if (btState == BluetoothAdapter.STATE_ON) {
                    systemBtState.onNext(true);
                    for (BluetoothDevice device : new BluetoothSystemUtils().getPairedDevices()) {
                        BaseDevice baseDevice = deviceService.getDeviceFromId(device.getAddress());
                        if (baseDevice != null) {
                            baseDevice.getBaseDeviceStateController().inputs
                                    .setConnectionInfo(BaseDevice.ConnectionState.CONNECTED);
                        }
                    }
                }
            }

        }, filter);
    }

    private void registerBTBondStateReceiver() {
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent == null) {
                    return;
                }
                if (intent.getAction().equals(ACTION_BOND_STATE_CHANGED)) {
                    bondState.onNext(
                            new BondState(((BluetoothDevice) intent.getParcelableExtra(EXTRA_DEVICE))
                                    .getAddress(),
                                    intent.getIntExtra(EXTRA_BOND_STATE, 0)));
                }

            }
        }, new IntentFilter(ACTION_BOND_STATE_CHANGED));
    }
}

package com.zoundindustries.marshallbt.ui.devicesettings.tabbedViews.eq;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.devicesettings.EQData;
import com.zoundindustries.marshallbt.ui.customviews.EQBarsView;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * First tab of the tabbed equaliser - static
 */
public class EQTabM1Fragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_eq_tab_flat, container, false);
        setupEqBarsView(root);
        return root.getRootView();
    }

    private void setupEqBarsView(View root) {
        EQBarsView bars = root.findViewById(R.id.customEQBarsFlat);
        bars.setEQValues(new EQData(new int[]{50, 50, 50, 50, 50}), false);
        bars.setEnabled(false);
        bars.setLabels(new ArrayList<>(Arrays
                .asList(getResources().getString(R.string.equaliser_screen_preset_bass),
                        getResources().getString(R.string.equaliser_screen_preset_low),
                        getResources().getString(R.string.equaliser_screen_preset_mid),
                        getResources().getString(R.string.equaliser_screen_preset_upper),
                        getResources().getString(R.string.equaliser_screen_preset_high))));
    }

}

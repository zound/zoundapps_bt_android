package com.zoundindustries.marshallbt.ui.welcome;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentWelcomeBinding;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.ui.setup.SetupActivity;
import com.zoundindustries.marshallbt.ui.setup.TacFragment;
import com.zoundindustries.marshallbt.viewmodels.welcome.WelcomeViewModel;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

/**
 * Fragment used for control initial screen content and animations
 * {@link HasSupportFragmentInjector} is used for display back button when {@link TacFragment} is active
 */
public class WelcomeFragment extends BaseFragment implements HasSupportFragmentInjector {

    private WelcomeViewModel.Body viewModel;
    private FragmentWelcomeBinding binding;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    public WelcomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this,
                viewModelFactory).get(WelcomeViewModel.Body.class);
        initObservers();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentWelcomeBinding.inflate(inflater);

        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupTermsText();
        startAnimation();
        setupToolbar(R.string.main_menu_empty_toolbar, View.GONE, true);
    }

    private void setupTermsText() {
        binding.tacTextview.setMovementMethod(LinkMovementMethod.getInstance());
        String termsPrefix = getResources().getString(R.string.welcome_screen_agree);
        SpannableString spannableString = new SpannableString(termsPrefix + getResources()
                .getString(R.string.welcome_screen_terms_and_conditions));

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                viewModel.inputs.onTacLinkClicked();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.primaryText));
                ds.setUnderlineText(true);
            }
        };

        spannableString.setSpan(clickableSpan, termsPrefix.length(), spannableString.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        binding.tacTextview.setText(spannableString);
    }

    private void initObservers() {
        viewModel.isViewChanged().observe(this, viewType -> {
            switch (viewType) {
                case SETUP_SCREEN:
                    if (getContext() != null) {
                        startActivity(SetupActivity.create(getContext()));
                        getActivity().finish();
                    }
                    break;
                case TAC_SCREEN:
                    startFragment(R.id.welcome_screen_fragment_container,
                            new TacFragment(), null, true);
                    break;
            }

        });
    }

    private void startAnimation() {
        Animation animAlphaFrom0To100 = AnimationUtils.loadAnimation(getContext(),
                R.anim.fade_in_alpha_100);
        Animation animAlphaFrom0To90 = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in_alpa_90);

        binding.introductionTitle.startAnimation(animAlphaFrom0To100);
        //This line cause to databinding not to work, when trying to change visibility of button
        /*binding.buttonConfirm.startAnimation(animAlphaFrom0To90);*/
        binding.textTosIntroduction.startAnimation(animAlphaFrom0To100);
    }

    @Override
    public void onStop() {
        super.onStop();
        viewModel.inputs.resetViewTransitionVariables();
    }
}

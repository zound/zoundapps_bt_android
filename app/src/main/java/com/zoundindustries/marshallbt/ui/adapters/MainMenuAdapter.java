package com.zoundindustries.marshallbt.ui.adapters;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.ListItemMainMenuBinding;
import com.zoundindustries.marshallbt.model.mainmenu.MainMenuItem;
import com.zoundindustries.marshallbt.ui.mainmenu.MainMenuFragment;
import com.zoundindustries.marshallbt.viewmodels.mainmenu.MainMenuViewModel;

import java.util.List;

/**
 * Adapter class providing data to the settings mainmenu navigation list.
 * Which will be displayed in ({@link MainMenuFragment})
 */
public class MainMenuAdapter extends ArrayAdapter<MainMenuItem> {

    private final List<MainMenuItem> mainMenuItemList;

    private MainMenuViewModel.Body viewModel;

    public MainMenuAdapter(@NonNull Context context,
                           @NonNull MainMenuViewModel.Body mainMenuViewModel,
                           int resource) {
        super(context, resource);
        this.viewModel = mainMenuViewModel;
        mainMenuItemList = viewModel.getMainMenuItemList().getValue();
    }

    @Override
    public int getCount() {
        return mainMenuItemList.size();
    }

    @NonNull
    @Override
    public MainMenuItem getItem(int position) {
        return mainMenuItemList.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        final ViewHolder viewHolder;
        if (convertView == null) {
            ListItemMainMenuBinding binding = DataBindingUtil
                    .inflate(LayoutInflater.from(parent.getContext()),
                            R.layout.list_item_main_menu,
                            parent, false);
            convertView = binding.getRoot();
            viewHolder = new ViewHolder(binding);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        setupNavigationItems(position, viewHolder);

        return convertView;
    }

    private void setupNavigationItems(int position, ViewHolder viewHolder) {
        MainMenuItem mainMenuItem = mainMenuItemList.get(position);
        String name = mainMenuItem.getName();
        viewHolder.name.setText(name);

        viewHolder.binding.mainMenuItem.setOnClickListener(
                view -> viewModel.inputs.onMainMenuItemClicked(mainMenuItem.getType(), false));
    }

    public static class ViewHolder {
        ListItemMainMenuBinding binding;

        public TextView name;

        ViewHolder(@NonNull ListItemMainMenuBinding binding) {
            this.binding = binding;
            name = binding.mainMenuRowTitle;
        }
    }
}

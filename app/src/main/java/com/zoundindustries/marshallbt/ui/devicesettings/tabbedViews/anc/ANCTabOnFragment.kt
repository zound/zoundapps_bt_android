package com.zoundindustries.marshallbt.ui.devicesettings.tabbedViews.anc

import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import androidx.lifecycle.Observer

import com.zoundindustries.marshallbt.R


/**
 * Fragment providing screen for ANC ON settings allowing read and modify the values.
 */
class ANCTabOnFragment : ANCTabBaseFragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()

        viewModel?.notifyAncValue?.observe(viewLifecycleOwner, Observer { value ->
            value?.let{
                binding?.ancOnValue?.text = formatAncString(value)
                binding?.ancOnBar?.progress = scaleProgressToView(value)
            }
        })
    }

    private fun setupViews() {
        binding?.ancOnDescription?.text = resources
                .getString(R.string.anc_settings_screen_on_subtitle)

        binding?.ancOnBar?.setOnSeekBarChangeListener(object : SimplifiedSeekBarChangeListener() {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    binding?.ancOnValue?.text = formatAncString(scaleProgressFromView(progress))
                    viewModel?.onAncValueChanged(scaleProgressFromView(progress))
                }
            }
        })
    }
}

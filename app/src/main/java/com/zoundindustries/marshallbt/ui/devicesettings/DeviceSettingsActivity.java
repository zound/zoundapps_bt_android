package com.zoundindustries.marshallbt.ui.devicesettings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.ActivityDeviceSettingsBinding;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.ota.pojo.FirmwareFileMetaData;
import com.zoundindustries.marshallbt.ui.BaseToolbarActivity;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.ui.devicesettings.tabbedViews.anc.MainANCTabsFragment;
import com.zoundindustries.marshallbt.ui.devicesettings.tabbedViews.eq.MainEQTabsFragment;
import com.zoundindustries.marshallbt.viewmodels.devicesettings.ScanningViewModel;
import com.zoundindustries.marshallbt.viewmodels.factories.devicesettings.ScanningViewModelFactory;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;
import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_OTA_FIRMWARE_META_DATA;

public class DeviceSettingsActivity extends BaseToolbarActivity {

    public static final String EXTRA_DEVICE_COUPLING_SLAVE_ID =
            "device_settings_coupling_slave_device_id";
    public static final String EXTRA_DEVICE_COUPLING_MODE =
            "device_settings_coupling_mode";
    public static final String EXTRA_FRAGMENT_TO_SHOW = "device_settings_fragment_to_show";
    public static final String EXTRA_DEVICE_MASTER_NAME = "device_settings_coupling_master_name";
    public static final String EXTRA_DEVICE_SLAVE_NAME = "device_settings_coupling_slave_name";

    private ActivityDeviceSettingsBinding binding;
    private ScanningViewModel.Body viewModel;

    private PublishSubject<Boolean> publishSubject = PublishSubject.create();

    /**
     * Encapsulates intent creation and setup of the Activity.
     *
     * @param context for the Activity start.
     * @param id      device id to be passed to the Activity.
     * @return configured starting Intent.
     */
    @NonNull
    public static Intent create(@NonNull Context context, @NonNull String id) {
        return create(context, id, null, false, null);
    }

    /**
     * Encapsulates intent creation and setup of the Activity.
     *
     * @param context  for the Activity start
     * @param viewType fragment type that should be displayed as a main view for Activity.
     * @param id       device id to be passed to the Activity.
     * @return configured starting Intent
     */
    @NonNull
    public static Intent create(@NonNull Context context,
                                @NonNull String id,
                                @NonNull ViewType viewType) {
        return create(context, id, viewType, false, null);
    }

    /**
     * Encapsulates intent creation and setup of the Activity.
     *
     * @param context for the Activity start
     * @return configured starting Intent
     */
    @NonNull
    public static Intent create(@NonNull Context context,
                                @NonNull String id,
                                @Nullable ViewType fragmentType,
                                boolean shouldRetryOta,
                                @Nullable FirmwareFileMetaData firmwareFileMetaData) {
        Intent intent = new Intent(context, DeviceSettingsActivity.class);
        intent.putExtra(EXTRA_DEVICE_ID, id);
        if (fragmentType != null) {
            intent.putExtra(EXTRA_FRAGMENT_TO_SHOW, fragmentType.ordinal());
        }

        if (firmwareFileMetaData != null) {
            intent.putExtra(EXTRA_OTA_FIRMWARE_META_DATA, firmwareFileMetaData);
        }
        intent.putExtra(AboutDeviceFragment.EXTRA_SHOULD_RETRY_OTA, shouldRetryOta);
        return intent;
    }

    @Override
    public void setToolbarName(@NonNull String name) {
        binding.deviceSettingsToolbar.toolbarTitle.setText(name);
    }

    @Override
    public Observable<Boolean> getBackPressedObservable() {
        return publishSubject;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_device_settings);

        viewModel = ViewModelProviders.of(
                this,
                new ScanningViewModelFactory(getApplication()))
                .get(ScanningViewModel.Body.class);

        viewModel.setScanningAllowed(false);

        Bundle extras = getIntent().getExtras();
        Bundle argBundle = new Bundle();

        boolean shouldRetryOta;
        FirmwareFileMetaData firmwareFileMetaData;

        if (extras != null) {
            String deviceId = extras.getString(EXTRA_DEVICE_ID);
            shouldRetryOta = extras.getBoolean(AboutDeviceFragment.EXTRA_SHOULD_RETRY_OTA);
            firmwareFileMetaData = extras.getParcelable(EXTRA_OTA_FIRMWARE_META_DATA);

            argBundle.putString(EXTRA_DEVICE_ID, deviceId);
            argBundle.putBoolean(AboutDeviceFragment.EXTRA_SHOULD_RETRY_OTA, shouldRetryOta);
            argBundle.putParcelable(EXTRA_OTA_FIRMWARE_META_DATA, firmwareFileMetaData);
        }

        if (savedInstanceState == null) {
            startFragment(R.id.device_settings_fragment_container, getFragmentFromExtras(),
                    argBundle, true);
        }
        setupToolbar();
    }

    @Override
    public void onBackPressed() {
        if (isBackButtonEnabled) {
            super.onBackPressed();
        }
    }

    @NonNull
    private Fragment getFragmentFromExtras() {

        ViewType fragmentType = ViewType.values()[getIntent()
                .getExtras().getInt(EXTRA_FRAGMENT_TO_SHOW)];

        Fragment fragmentToDisplay;

        switch (fragmentType) {
            case ABOUT_DEVICE:
                fragmentToDisplay = new AboutDeviceFragment();
                break;
            case ANC:
                fragmentToDisplay = new MainANCTabsFragment();
                break;
            case EQ_EXTENDED:
                fragmentToDisplay = new MainEQTabsFragment();
                break;
            default:
                fragmentToDisplay = new DeviceSettingsFragment();
                break;
        }
        return fragmentToDisplay;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.setScanningAllowed(true);
    }

    private void setupToolbar() {
        binding.deviceSettingsToolbar.toolbarImage.setOnClickListener(v ->
                publishSubject.onNext(true));
    }

    @Override
    public void setToolbarImageVisibility(int visibility) {
        binding.deviceSettingsToolbar.toolbarImage.setVisibility(visibility);
    }

    @Override
    public void changeToolbarColor(@NonNull ToolbarItemsColor toolbarItemsColor) {
        super.changeToolbarColor(toolbarItemsColor);
        binding.deviceSettingsToolbar.toolbarImage.setImageDrawable(toolbarDrawable);
        binding.deviceSettingsToolbar.toolbar.setBackgroundColor(toolbarBackgroundColorId);
        binding.deviceSettingsToolbar.toolbarTitle.setTextColor(toolbarTextColor);
    }

    @Override
    public void setToolbarVisibility(Boolean isVisible) {
        binding.deviceSettingsToolbar.toolbar.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }
}

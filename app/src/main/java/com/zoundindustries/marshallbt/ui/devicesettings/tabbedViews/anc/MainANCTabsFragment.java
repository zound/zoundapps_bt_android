package com.zoundindustries.marshallbt.ui.devicesettings.tabbedViews.anc;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.devicesettings.AncMode;
import com.zoundindustries.marshallbt.ui.ViewFlowController;
import com.zoundindustries.marshallbt.ui.devicesettings.tabbedViews.BaseTabsFragment;
import com.zoundindustries.marshallbt.ui.devicesettings.tabbedViews.TabContainer;
import com.zoundindustries.marshallbt.viewmodels.devicesettings.ANCViewModel;
import com.zoundindustries.marshallbt.viewmodels.factories.devicesettings.AncViewModelFactory;

import java.util.ArrayList;
import java.util.List;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

/**
 * Fragment wrapping ANC modes tab view including ViewPager for all possible modes.
 */
public class MainANCTabsFragment extends BaseTabsFragment {

    private String deviceId;

    private ANCViewModel.Body viewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments == null || getActivity() == null) return;

        viewModel = ViewModelProviders.of(
                getActivity(),
                new AncViewModelFactory(getActivity().getApplication(),
                        arguments.getString(EXTRA_DEVICE_ID, ""))).get(ANCViewModel.Body.class);

        deviceId = arguments.getString(EXTRA_DEVICE_ID, "");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolbar(R.string.device_settings_menu_item_anc_uc, View.VISIBLE, true);

        setupListeners();
        setupObservers();
    }

    @Override
    public void onResume() {
        super.onResume();

        AncMode tabType = viewModel.getNotifyTabType().getValue();

        if (tabType != null) {
            binding.pager.setCurrentItem(ancMode2TabIndex(tabType));
        }
    }

    @Override
    public List<TabContainer> getTabsList() {
        List<TabContainer> tabs = new ArrayList<>();

        Bundle b = new Bundle();
        b.putString(EXTRA_DEVICE_ID, deviceId);

        Fragment f = new ANCTabOnFragment();
        f.setArguments(b);
        TabContainer t1 = new TabContainer(f,
                getResources().getString(R.string.anc_settings_screen_on_uc));
        tabs.add(t1);

        Fragment g = new ANCTabMonitorFragment();
        g.setArguments(b);
        TabContainer t2 = new TabContainer(g,
                getResources().getString(R.string.anc_settings_screen_monitoring_uc));
        tabs.add(t2);

        Fragment h = new ANCTabOffFragment();
        h.setArguments(b);
        TabContainer t3 = new TabContainer(h,
                getResources().getString(R.string.anc_settings_screen_off_uc));
        tabs.add(t3);

        return tabs;
    }

    private void setupObservers() {
        viewModel.getNotifyTabType().observe(getViewLifecycleOwner(), ancTabType -> {
            binding.pager.setCurrentItem(ancMode2TabIndex(ancTabType));
            binding.tabsContainer.setState(R.id.eq_tabs_connected, 0, 0);
        });

        viewModel.getOutputs().getNotifyViewChanged().observe(getViewLifecycleOwner(), viewType -> {
            if (getActivity() != null) {
                Context context = getContext();
                if (context != null) {
                    if (viewType == ViewFlowController.ViewType.BACK) {
                        performBackPressedOrFinish();
                    }
                }
            }
        });
    }

    private void setupListeners() {
        binding.pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                viewModel.onAncTabChanged(tabIndex2AncMode(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private int ancMode2TabIndex(AncMode ancMode) {
        switch (ancMode) {
            case ON:
                return 0;
            case MONITOR:
                return 1;
            case OFF:
            default:
                return 2;
        }
    }

    private AncMode tabIndex2AncMode(int index) {
        switch (index) {
            case 0:
                return AncMode.ON;
            case 1:
                return AncMode.MONITOR;
            case 2:
            default:
                return AncMode.OFF;
        }
    }
}


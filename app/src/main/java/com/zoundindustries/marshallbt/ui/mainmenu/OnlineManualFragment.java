package com.zoundindustries.marshallbt.ui.mainmenu;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentOnlineManualBinding;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.ui.customviews.IToolbarConfigurator.ToolbarItemsColor;


/**
 * Fragment for displaying online manual for devices.
 * For display manual {@link WebView} is used.
 * Link to device is based on device type that was passed as fragment argument
 */
public class OnlineManualFragment extends BaseFragment {

    private FragmentOnlineManualBinding binding;

    public OnlineManualFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        if (getActivity() != null && getArguments() != null) {
            binding = FragmentOnlineManualBinding.inflate(inflater);
            return binding.getRoot();
        } else {
            if (getActivity() != null) {
                getActivity().finish();
            }
            return null;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prepareWebView(getArguments());
        setupToolbar(R.string.main_menu_item_user_manual_uc, View.VISIBLE, true);
    }

    @Override
    public void onStart() {
        super.onStart();
        changeToolbarColor(ToolbarItemsColor.BLACK);
    }

    @Override
    public void onStop() {
        super.onStop();
        changeToolbarColor(ToolbarItemsColor.WHITE);
    }

    private void prepareWebView(@NonNull Bundle arguments) {
        setWebViewSettings();
        loadUrlInWebView(arguments);
    }

    private void setWebViewSettings() {
        binding.onlineManualWebview.getSettings().setJavaScriptEnabled(true);
        binding.onlineManualWebview.getSettings().setSupportZoom(true);
    }

    private void loadUrlInWebView(@NonNull Bundle arguments) {
        binding.onlineManualWebview.loadUrl(
                getSpeakerType(arguments).getUserManualUrl());
    }

    @NonNull
    private TymphanyDevice.DeviceSubType getSpeakerType(@NonNull Bundle arguments) {
        return TymphanyDevice.DeviceSubType.values()
                [arguments.getInt(TymphanyDevice.EXTRA_DEVICE_TYPES)];
    }
}

package com.zoundindustries.marshallbt.ui.player.sources;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.ActivitySourcesBinding;
import com.zoundindustries.marshallbt.model.player.sources.SourceFeatures;
import com.zoundindustries.marshallbt.ui.BaseToolbarActivity;
import com.zoundindustries.marshallbt.ui.ViewFlowController;
import com.zoundindustries.marshallbt.ui.adapters.HomeDevicesAdapter;
import com.zoundindustries.marshallbt.ui.adapters.SourcesPagerAdapter;
import com.zoundindustries.marshallbt.utils.SimplifiedOnPageChangeListener;
import com.zoundindustries.marshallbt.viewmodels.factories.player.sources.SourcesViewModelFactory;
import com.zoundindustries.marshallbt.viewmodels.player.sources.SourcesViewModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

/**
 * Activity showing the sources for playing.
 */
public class SourcesActivity extends BaseToolbarActivity {

    private SourcesViewModel.Body viewModel;
    private ActivitySourcesBinding binding;

    private SourcesPagerAdapter sourcesPagerAdapter;
    private String deviceId;

    private PublishSubject<Boolean> publishSubject = PublishSubject.create();

    private int previousScrollState;
    private boolean isScrollFromUser;

    /**
     * Encapsulates intent creation and setup of the Activity.
     *
     * @param context for the Activity start
     * @return configured starting Intent
     */
    public static Intent create(@NonNull Context context, @NonNull String id) {
        Intent intent = new Intent(context, SourcesActivity.class);
        intent.putExtra(EXTRA_DEVICE_ID, id);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //todo handle if getExtras or deviceId is null
        deviceId = getIntent().getExtras().getString(EXTRA_DEVICE_ID);

        SourcesViewModelFactory viewModelFactory =
                new SourcesViewModelFactory(getApplication(), deviceId);

        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(SourcesViewModel.Body.class);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_sources);
        binding.setViewModel(viewModel);

        setupViewPager();

        setupToolbar();

        initObservers();
    }

    @Override
    public void setToolbarName(@NonNull String name) {
        binding.sourcesToolbar.toolbarTitle.setText(name);
    }

    @Override
    public Observable<Boolean> getBackPressedObservable() {
        return publishSubject;
    }

    private void initObservers() {
        viewModel.getSourceFeatures().observe(this, sourceFeatures -> {
            sourcesPagerAdapter.setPages(sourceFeatures, viewModel.isAuxConfigurable());

            if (!viewModel.isAuxConfigurable()) {
                binding.sourcesHorizontalScrollView.setVisibility(View.GONE);
                setToolbarName(getString(R.string.player_audio_uc));
            } else {
                binding.sourcesHorizontalScrollView.setTabs(getIconsByFeature(sourceFeatures));
                setToolbarName(sourcesPagerAdapter.getPageTitle(0).toString());
            }

        });

        viewModel.outputs.isViewChanged().observe(this, viewType -> {
            if (viewType == ViewFlowController.ViewType.BACK) {
                try {
                    onBackPressed();
                } catch (IllegalStateException exception) {
                    finish();
                }
            }
        });
    }

    private void setupToolbar() {
        binding.sourcesToolbar.toolbarImage.setOnClickListener(v -> publishSubject.onNext(true));
    }

    private void setupViewPager() {
        sourcesPagerAdapter = new SourcesPagerAdapter(
                getSupportFragmentManager(), this, deviceId);

        binding.viewPager.setAdapter(sourcesPagerAdapter);
        binding.viewPager.addOnPageChangeListener(new SimplifiedOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                setToolbarName(sourcesPagerAdapter.getPageTitle(position).toString());
                if (isScrollFromUser) {
                    binding.sourcesHorizontalScrollView.scrollToChild(position);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                isScrollFromUser = previousScrollState == ViewPager.SCROLL_STATE_DRAGGING
                        && state == ViewPager.SCROLL_STATE_SETTLING;

                previousScrollState = state;
            }
        });

        binding.sourcesHorizontalScrollView.setViewPager(binding.viewPager);
    }

    @NonNull
    private List<Integer> getIconsByFeature(SourceFeatures sourceFeatures) {
        List<Integer> imageResources = new ArrayList<>();

        if (sourceFeatures.isBluetoothSupported()) {
            imageResources.add(R.drawable.bluetooth_small);
        }
        if (sourceFeatures.isRcaSupported()) {
            imageResources.add(R.drawable.rca_small);
        }
        if (sourceFeatures.isAuxSupported()) {
            imageResources.add(R.drawable.aux_small);
        }
        return imageResources;
    }

    @Override
    public void setToolbarImageVisibility(int visibility) {
        binding.sourcesToolbar.toolbarImage.setVisibility(visibility);
    }

    @Override
    public void changeToolbarColor(@NonNull ToolbarItemsColor toolbarItemsColor) {
        super.changeToolbarColor(toolbarItemsColor);
        binding.sourcesToolbar.toolbarImage.setImageDrawable(toolbarDrawable);
        binding.sourcesToolbar.toolbar.setBackgroundColor(toolbarBackgroundColorId);
        binding.sourcesToolbar.toolbarTitle.setTextColor(toolbarTextColor);
    }

    @Override
    public void setToolbarVisibility(Boolean isVisible) {
        if (getSupportActionBar() == null) {
            return;
        }

        if (isVisible) {
            getSupportActionBar().hide();
        } else {
            getSupportActionBar().show();
        }
    }

    /**
     * For whole application exit animation is set to from left to right
     * For player we want to have top to bottom, so we have to override base behavior
     * (as we have set global animation in styles.xml)
     * entry animation is set in bundle while creating Activity in:
     * {@link HomeDevicesAdapter#setAnimationForBundle(Context)}
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom);
    }
}

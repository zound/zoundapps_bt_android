package com.zoundindustries.marshallbt.ui.player.sources;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentBtSourceBinding;
import com.zoundindustries.marshallbt.model.player.sources.SourceFeatures;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.utils.DebouncedSeekBarListener;
import com.zoundindustries.marshallbt.viewmodels.factories.player.sources.BluetoothSourceViewModelFactory;
import com.zoundindustries.marshallbt.viewmodels.factories.player.sources.SourcesViewModelFactory;
import com.zoundindustries.marshallbt.viewmodels.player.sources.BluetoothSourceViewModel;
import com.zoundindustries.marshallbt.viewmodels.player.sources.SourcesViewModel;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

/**
 * Class to display bluetooth source screen
 */
public class BluetoothSourceFragment extends BaseFragment {

    private BluetoothSourceViewModel.Body viewModel;

    private FragmentBtSourceBinding binding;
    private SourcesViewModel.Body sourcesViewModel;
    private SourceFeatures sourceFeatures;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() == null || getActivity() == null) return;

        String deviceId = getArguments().getString(EXTRA_DEVICE_ID);

        BluetoothSourceViewModelFactory viewModelFactory =
                new BluetoothSourceViewModelFactory(getActivity().getApplication(),
                        deviceId);

        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(BluetoothSourceViewModel.Body.class);


        SourcesViewModelFactory sourcesViewModelFactory =
                new SourcesViewModelFactory(getActivity().getApplication(), deviceId);

        sourcesViewModel = ViewModelProviders.of(getActivity(), sourcesViewModelFactory)
                .get(SourcesViewModel.Body.class);
        sourceFeatures = sourcesViewModel.outputs.getSourceFeatures().getValue();

        initObservers();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = FragmentBtSourceBinding.inflate(inflater);
        binding.setLifecycleOwner(this);
        binding.setViewModel(viewModel);

        binding.bluetoothPlayer.setViewModel(viewModel);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.volumeSeekBar.setOnSeekBarChangeListener(new DebouncedSeekBarListener() {
            @Override
            public void runTask(@NonNull SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    viewModel.inputs.setVolume(progress);
                }
            }
        });
    }

    private void initObservers() {
        viewModel.outputs.isViewChanged().observe(this, viewType -> {
            switch (viewType) {
                case HOME_SCREEN:
                    if (getActivity() != null) {
                        getActivity().finish();
                    }
            }
        });

        viewModel.isAuxConfigurable().observe(this, isConfigurable -> {
            if (isConfigurable) {
                // we care for the source changes only if the aux is configurable
                viewModel.outputs.isSourceActivated().observe(this, isSourceActivated -> {
                    if (isSourceActivated) {
                        binding.activateButton.setVisibility(View.INVISIBLE);
                    } else {
                        binding.activateButton.setVisibility(View.VISIBLE);
                    }
                });

                binding.speakerSourceTypeImageView.setImageResource(R.drawable.source_bluetooth);
                binding.connectionInfo.setText(
                        getString(R.string.bluetooth_screen_subtitle_connected,
                                viewModel.outputs.getDisplayedDeviceName().getValue()));
            } else {
                binding.activateButton.setVisibility(View.INVISIBLE);
                binding.connectionInfo.setText(R.string.player_audio_source_aux_activated);
                binding.speakerSourceTypeImageView.setImageResource(R.drawable.source_aux);
            }
        });
    }
}

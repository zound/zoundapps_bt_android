package com.zoundindustries.marshallbt.ui.devicesettings;

import androidx.lifecycle.ViewModelProviders;
import android.content.ActivityNotFoundException;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshallbt.databinding.FragmentForgetDeviceBinding;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.utils.SystemServicesUtils;
import com.zoundindustries.marshallbt.viewmodels.devicesettings.ForgetDeviceViewModel;
import com.zoundindustries.marshallbt.viewmodels.factories.devicesettings.ForgetDeviceViewModelFactory;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

public class ForgetDeviceFragment extends BaseFragment {

    private ForgetDeviceViewModel.Body viewModel;
    private FragmentForgetDeviceBinding binding;
    private BaseDevice device;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            ForgetDeviceViewModelFactory viewModelFactory = new ForgetDeviceViewModelFactory(
                            getActivity().getApplication(),
                            getArguments().getString(EXTRA_DEVICE_ID, ""));

            viewModel = ViewModelProviders.of(this, viewModelFactory)
                    .get(ForgetDeviceViewModel.Body.class);

            initObservers();
        } else if (getActivity() != null) {
            getActivity().finish();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = FragmentForgetDeviceBinding.inflate(inflater);
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolbar(viewModel.getTitle(), View.VISIBLE, true);
        binding.descriptionForget.setText(viewModel.getDescription());
        binding.buttonForget.setText(viewModel.getTitle());
    }

    private void initObservers() {
        viewModel.outputs.isViewChanged().observe(this, viewType -> {
            if (getActivity() != null) {
                switch (viewType) {
                    case HOME_SCREEN:
                        if (getActivity() != null) {
                            getActivity().finish();
                        }
                        break;
                    case GLOBAL_SETTINGS_BLUETOOTH:
                        openBluetoothGlobalSettings();
                        if (getActivity() != null) {
                            getActivity().finish();
                        }
                        break;
                    case BACK:
                        performBackPressedOrFinish();
                        break;
                }
            }
        });
    }

    private void openBluetoothGlobalSettings() {
        try {
            startActivity(SystemServicesUtils.getBluetoothSettingsIntent());
        } catch (ActivityNotFoundException exception) {
            startActivity(SystemServicesUtils.getGlobalSettingsIntent());
        }
    }
}

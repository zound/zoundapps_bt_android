package com.zoundindustries.marshallbt.ui.ota;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentOtaAvailableBinding;
import com.zoundindustries.marshallbt.ui.BaseFragment;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

/**
 * Displays a "update available" information and is a first screen of ota done from home screen
 */
public class OTAAvailableFragment extends BaseFragment {

    private String deviceId;
    private FragmentOtaAvailableBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (getActivity() != null) {
            Bundle extras = getActivity().getIntent().getExtras();
            if (extras != null) {
                deviceId = extras.getString(EXTRA_DEVICE_ID);
            }
        }

        binding = FragmentOtaAvailableBinding.inflate(inflater);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle argBundle = new Bundle();
        argBundle.putString(EXTRA_DEVICE_ID, deviceId);

        binding.nextButton.setOnClickListener(clickView ->
                startFragment(R.id.ota_fragment_container,
                        new OTAProgressFragment(),
                        argBundle,
                        true));
    }
}

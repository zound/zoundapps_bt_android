package com.zoundindustries.marshallbt.ui.mainmenu;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.ui.setup.BottomReachedIndicatingScrollView;
import com.zoundindustries.marshallbt.viewmodels.mainmenu.MainMenuViewModel;
import com.zoundindustries.marshallbt.viewmodels.mainmenu.MainMenuViewModel.QuickGuideFragmentLayoutType;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;

/**
 * Displays content for 'Quick Guide' item from 'Help' which is nested in Main Menu
 * Main Menu {@link MainMenuFragment}-> 'Help' -> 'Quick Guide'
 * Each Quick Guide item has it's own layout for easier maintenance
 */
public class QuickGuideFragment extends BaseFragment {

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    public QuickGuideFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        View view = null;
        if (bundle != null) {
            QuickGuideFragmentLayoutType layoutType = QuickGuideFragmentLayoutType.values()[
                    bundle.getInt(MainMenuViewModel.QUICK_GUIDE_LAYOUT_TYPE)];

            int displayNameResourceId = 0;
            switch (layoutType) {
                case BLUETOOTH_PAIRING:
                    view = inflater.inflate(R.layout.fragment_quick_guide_bluetooth_pairing,
                            container, false);
                    displayNameResourceId = R.string.main_menu_item_bluetooth_pairing_uc;
                    break;
                case COUPLE_SPEAKERS:
                    view = inflater.inflate(R.layout.fragment_quick_guide_couple_speakers,
                            container, false);
                    displayNameResourceId = R.string.device_settings_menu_item_couple_uc;
                    break;
                case PLAY_PAUSE_BUTTON:
                    view = inflater.inflate(R.layout.fragment_quick_guide_play_pause_button,
                            container, false);
                    displayNameResourceId = R.string.main_menu_item_play_pause_button_uc;
                    break;
            }
            setScrollListener(view);
            setupToolbar(displayNameResourceId, View.VISIBLE, true);
        } else {
            // TODO: Firebase events
            if (getActivity() != null && !getActivity().isFinishing()) {
                getActivity().finish();
            }
        }
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        setupToolbar(R.string.main_menu_item_quick_guide_uc, View.VISIBLE, true);
    }

    private void setScrollListener(View view) {
        BottomReachedIndicatingScrollView scrollView = view.findViewById(R.id.quick_guide_scroll_view);
        View gradientFooterView = view.findViewById(R.id.gradient_footer_view);
        scrollView.setOnScrollListener(isBottomEndReached ->
                gradientFooterView.setVisibility(
                        isBottomEndReached ? View.INVISIBLE : View.VISIBLE));
    }
}

package com.zoundindustries.marshallbt.ui.setup;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentSetupStayUpdatedBinding;
import com.zoundindustries.marshallbt.di.Injectable;
import com.zoundindustries.marshallbt.model.device.tymphany.UrlBuilder;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.ui.mainmenu.UnsubscribeFragment;
import com.zoundindustries.marshallbt.utils.CommonPreferences;
import com.zoundindustries.marshallbt.viewmodels.setup.StayUpdatedViewModel;

import javax.inject.Inject;

/**
 * Fragment to show email address input screen in app setup flow.
 */
public class StayUpdatedFragment extends BaseFragment implements Injectable {

    private FragmentSetupStayUpdatedBinding binding;
    private StayUpdatedViewModel.Body viewModel;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    public StayUpdatedFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this,
                viewModelFactory).get(StayUpdatedViewModel.Body.class);
        initObservers();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSetupStayUpdatedBinding.inflate(inflater);
        binding.setLifecycleOwner(this);
        binding.setViewModel(viewModel);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupPrivacyPolicyText();
        setupSubscribedUserEmailText();
        setupToolbar(R.string.main_menu_empty_toolbar, View.VISIBLE, true);
    }

    private void setupPrivacyPolicyText() {
        binding.privacyPolicyTextView.setMovementMethod(LinkMovementMethod.getInstance());
        String termsPrefix = getResources().getString(R.string.stay_updated_footer);
        SpannableString spannableString = new SpannableString(termsPrefix + getResources()
                .getString(R.string.stay_updated_privacy_policy));

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                viewModel.inputs.privacyPolicyLinkTapped();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getActivity().getResources().getColor(R.color.primaryText));
                ds.setUnderlineText(true);
            }
        };
        spannableString.setSpan(clickableSpan, termsPrefix.length(), spannableString.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        binding.privacyPolicyTextView.setText(spannableString);
    }

    private void setupSubscribedUserEmailText() {
        CommonPreferences preferences = new CommonPreferences(getContext());
        if (preferences.isEmailSubscribed()) {
            String emailPrefix = getResources().getString(R.string.stay_updated_email_confirmation);
            String email = preferences.getUserEmail();
            SpannableString spannableString = new SpannableString(emailPrefix + email);
            spannableString.setSpan(new StyleSpan(Typeface.BOLD), emailPrefix.length(), spannableString.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            binding.subscribedUserEmailTextView.setText(spannableString);
        }
    }

    private void initObservers() {
        viewModel.outputs.isViewChanged().observe(this, viewType -> {
            switch (viewType) {
                case HOME_SCREEN:
                    goToHomeScreen();
                    break;
                case SHARE_DATA_SCREEN:
                    startFragment(R.id.setup_fragment_container,
                            new ShareDataFragment(), null, false);
                    break;
                case MAIN_MENU_UNSUBSCRIBE:
                    startFragment(R.id.main_menu_fragment_container,
                            new UnsubscribeFragment(), null, true);
                    break;
                case PRIVACY_POLICY_SCREEN:
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse(UrlBuilder.buildPrivacyPolicyUrl())));
                    break;
            }
        });
    }

    private void goToHomeScreen() {
        if (getActivity() != null && !getActivity().isFinishing()) {
            getActivity().finish();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        viewModel.inputs.resetViewTransitionVariables();
    }
}
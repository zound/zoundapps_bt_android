package com.zoundindustries.marshallbt.ui.customviews;

import com.zoundindustries.marshallbt.ui.customviews.ExtendedSpinner;

/**
 * Interface defining callbacks for {@link ExtendedSpinner}.
 */
public interface IExtendedSpinnerListener {

    /**
     * Callback emitted when item is selected on the spinner. Extended with detection of
     * user action.
     *
     * @param position index from the spinner list.
     * @param fromUser flag indicating id selection was made bu user.
     */
    void onItemSelected(int position, boolean fromUser);
}

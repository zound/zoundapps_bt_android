package com.zoundindustries.marshallbt.ui.error;

import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.ActivityErrorBinding;
import com.zoundindustries.marshallbt.ui.BaseActivity;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.viewmodels.error.ErrorViewModel;
import com.zoundindustries.marshallbt.viewmodels.factories.error.ErrorViewModelFactory;

/**
 * Activity displaying standard error message
 */
public abstract class BaseErrorActivity extends BaseActivity {

    public static final String EXTRA_ERROR_ORIGIN = "error_origin_screen";

    protected ActivityErrorBinding binding;
    protected ErrorViewModel.Body errorViewModel;

    protected ViewType errorType = ViewType.UNKNOWN;
    protected ViewType errorOrigin = ViewType.UNKNOWN;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        errorType = getErrorType();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            errorOrigin = (ViewType) extras.getSerializable(EXTRA_ERROR_ORIGIN);
        }

        errorViewModel = ViewModelProviders.of(this,
                new ErrorViewModelFactory(errorType, errorOrigin))
                .get(ErrorViewModel.Body.class);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_error);
        binding.setLifecycleOwner(this);
        binding.setViewModel(errorViewModel);

        init();
    }

    @Override
    public void onBackPressed() {
        if (isHardwareBackButtonActive()) {
            errorViewModel.systemBackButtonPressed();
        }
    }


    /**
     * Setter for error title message. Needs to be implemented by derived class to show message
     * specific to error {@link ViewType}.
     */
    protected abstract int getTitleTextId();

    /**
     * Setter for error title message. Needs to be implemented by derived class to show message
     * specific to error {@link ViewType}.
     */
    protected abstract int getDescriptionTextId();

    /**
     * Setter for main action button text. Needs to be implemented by derived class to show main
     * action button text specific to error {@link ViewType}.
     */
    protected abstract int getActionButtonTextId();

    /**
     * Every class that uses baseErrorActivity should decide if back button should be shown
     *
     * @return boolean value that represents back button visibility
     */
    protected abstract boolean isBackButtonVisible();

    /**
     * Every class that uses baseErrorActivity should decide if hardware back button should be
     * active.
     *
     * @return boolean value that represents hardware back button behavior
     */
    protected abstract boolean isHardwareBackButtonActive();

    /**
     * Every class that uses baseErrorActivity must define its type via this method.
     */
    protected abstract ViewType getErrorType();

    private void init() {
        binding.errorTitle.setText(getTitleTextId());
        binding.errorDescription.setText(getDescriptionTextId());
        binding.actionButton.setText(getActionButtonTextId());

        setupBackButton();
    }

    private void setupBackButton() {
        boolean visibility = isBackButtonVisible();

        if (!visibility) {
            setBackButtonVisibility(false);
            return;
        }
        setBackButtonVisibility(true);
    }

    private void setBackButtonVisibility(boolean isVisible) {
        binding.backButton.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
    }
}
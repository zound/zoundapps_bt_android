package com.zoundindustries.marshallbt.ui.ota;

import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshallbt.databinding.FragmentOtaCompletedBinding;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.viewmodels.factories.ota.OTACompletedViewModelFactory;
import com.zoundindustries.marshallbt.viewmodels.ota.OTACompletedViewModel;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

/**
 * Fragment showing completed message after OTA firmware update was completed.
 */
public class OTACompletedFragment extends BaseFragment {

    private OTACompletedViewModel.Body otaCompletedViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        OTACompletedViewModelFactory viewModelFactory =
                new OTACompletedViewModelFactory(getActivity().getApplication(),
                        getArguments().getString(EXTRA_DEVICE_ID, ""));

        otaCompletedViewModel = ViewModelProviders.of(this,
                viewModelFactory).get(OTACompletedViewModel.Body.class);

        FragmentOtaCompletedBinding binding = FragmentOtaCompletedBinding.inflate(inflater);

        binding.setViewModel(otaCompletedViewModel);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        initObservers();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void initObservers() {
        otaCompletedViewModel.outputs.isViewChanged()
                .observe(this, viewType -> {
                    if (getActivity() != null) {
                        switch (viewType) {
                            case BACK:
                                getActivity().finish();
                                break;
                        }
                    } else {
                        //TODO think of recovering from this erroneous state...
                    }
                });
    }
}

package com.zoundindustries.marshallbt.ui;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.homescreen.HomeScreenItemLinkResources;

/**
 * Class wrapping image and text controls to visualize device item link control.
 */
public class HomeItemLinkLayout extends LinearLayout {

    private ImageView image;
    private TextView label;

    /**
     * Constructor for the custom link layout.
     *
     * @param context to pass to super class.
     * @param attrs   to setup the view.
     */
    public HomeItemLinkLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        String service = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(service);

        if (layoutInflater != null) {
            LinearLayout layout = (LinearLayout) layoutInflater
                    .inflate(R.layout.list_link_item, this, true);

            image = layout.findViewById(R.id.link_image);
            label = layout.findViewById(R.id.label);

            image.setClickable(false);
            label.setClickable(false);
        }
    }

    /**
     * Setup view based on {@link HomeScreenItemLinkResources}.
     *
     * @param resources containing image and label.
     */
    public void setLinkResources(HomeScreenItemLinkResources resources) {
        image.setImageResource(resources.getImageId());
        label.setText(resources.getLabel());
    }
}

package com.zoundindustries.marshallbt.ui.mainmenu;


import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentMainMenuFossBinding;
import com.zoundindustries.marshallbt.model.mainmenu.joplin.WebsiteLinkHolder;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.viewmodels.mainmenu.MainMenuFossViewModel;

/**
 * Displays content for Main Menu 'About' item
 */
public class MainMenuFossFragment extends BaseFragment {

    private MainMenuFossViewModel.Body viewModel;

    public MainMenuFossFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this)
                .get(MainMenuFossViewModel.Body.class);
        initObserver();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        FragmentMainMenuFossBinding binding = FragmentMainMenuFossBinding.inflate(inflater);
        binding.setLifecycleOwner(this);
        binding.setViewModel(viewModel);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolbar(R.string.main_menu_item_foss_uc, View.VISIBLE, true);
    }

    private void initObserver() {
        viewModel.outputs.isViewChanged().observe(this, viewType -> {
            switch (viewType) {
                case MAIN_MENU_FOSS_WEBSITE:
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse(WebsiteLinkHolder.FOSS_WEBSITE_URL)));
                    break;
            }
        });
    }
}

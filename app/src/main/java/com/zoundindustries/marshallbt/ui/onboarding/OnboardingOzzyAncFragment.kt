package com.zoundindustries.marshallbt.ui.onboarding

import com.zoundindustries.marshallbt.R


/**
 * Fragment for displaying Anc onboarding screen.
 */
class OnboardingOzzyAncFragment : OnboardingTabBaseFragment() {
    override val headerId: Int
        get() = R.string.ozzy_onboarding_anc_title_uc
    override val descriptionId: Int
        get() = R.string.ozzy_onboarding_anc_subtitle
    override val imageResId: Int
        get() = R.drawable.carousel_1_image
}
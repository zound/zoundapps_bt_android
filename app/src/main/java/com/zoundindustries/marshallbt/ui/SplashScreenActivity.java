package com.zoundindustries.marshallbt.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.ui.homescreen.HomeActivity;
import com.zoundindustries.marshallbt.ui.setup.SetupActivity;
import com.zoundindustries.marshallbt.ui.welcome.WelcomeActivity;
import com.zoundindustries.marshallbt.utils.CommonPreferences;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

/**
 * Initial Activity showing Marshall logo when app is starting.
 */
public class SplashScreenActivity extends BaseActivity implements HasSupportFragmentInjector {

    public static final int NEXT_SCREEN_START_DELAY_MILLIS = 2000;

    /**
     * Encapsulates intent creation and setup of the Activity.
     *
     * @param context for the Activity start
     * @return configured starting Intent
     */
    @NonNull
    public static Intent create(@NonNull Context context) {
        return new Intent(context, SplashScreenActivity.class);
    }

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CommonPreferences preferences = new CommonPreferences(getApplicationContext());

        new Handler().postDelayed(() -> {
            if (preferences.shouldDisplayWelcomeScreen()) {
                startActivity(WelcomeActivity.create(getApplicationContext()));
                finish();
            } else if (preferences.isFirstTimeSubscription()
                    || preferences.isFirstTimeShareData()) {
                startActivity(SetupActivity.create(getApplicationContext()));
                finish();
            } else {
                startActivity(HomeActivity.create(getApplicationContext()));
                finish();
            }
        }, NEXT_SCREEN_START_DELAY_MILLIS);
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }
}

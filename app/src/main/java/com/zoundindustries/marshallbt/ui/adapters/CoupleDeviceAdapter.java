package com.zoundindustries.marshallbt.ui.adapters;

import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.ListItemCoupleDeviceBinding;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.viewmodels.devicesettings.CouplingListViewModel;

import java.util.List;

/**
 * RecyclerView adapter keeping devices that are available for coupling for the selected device.
 * This data should be static for every CouplingListViewModel instance.
 */
public class CoupleDeviceAdapter extends RecyclerView.Adapter<CoupleDeviceAdapter.ViewHolder> {

    public static final int NO_ITEM_SELECTED = -1;
    public static final int HEADER_POSITION = 0;

    private final CouplingListViewModel.Body coupleDeviceViewModel;
    private List<BaseDevice> coupleSpeakerDevices;
    private int selectedItemPosition;


    /**
     * Constructor for {@link CoupleDeviceAdapter}.
     *
     * @param coupleSpeakerDevices  devices that will be presented as possible to couple with.
     * @param coupleDeviceViewModel view model to get user events from every row on the list.
     */
    public CoupleDeviceAdapter(@NonNull List<BaseDevice> coupleSpeakerDevices,
                               @NonNull CouplingListViewModel.Body coupleDeviceViewModel) {
        this.coupleSpeakerDevices = coupleSpeakerDevices;
        this.coupleDeviceViewModel = coupleDeviceViewModel;
    }

    @Override
    public CoupleDeviceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ListItemCoupleDeviceBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.list_item_couple_device, parent,
                false);
        return new CoupleDeviceAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BaseDevice device = coupleSpeakerDevices.get(position);
        holder.binding.textSpeakerName
                .setText(device.getBaseDeviceStateController().outputs
                        .getSpeakerInfoCurrentValue().getDeviceName());
        holder.binding.speakerImageView.setImageResource(coupleDeviceViewModel.getSpeakerImageResourceId(device.getDeviceInfo().getId()));
        setupCoupleDeviceItem(holder, position);
    }

    @Override
    public int getItemCount() {
        return coupleSpeakerDevices.size();
    }

    private void setHeaderItem(ViewHolder holder) {
        holder.binding.layoutSpeakerInfo.setSelected(true);
        holder.binding.layoutSpeakerInfo.setClickable(false);
        holder.binding.layoutSpeakerInfo.setFocusable(false);
        holder.binding.checkMark.setVisibility(View.VISIBLE);
    }

    private void setupCoupleDeviceItem(@NonNull ViewHolder holder, int position) {
        if (position == HEADER_POSITION) {
            setHeaderItem(holder);
        } else {
            holder.binding.layoutSpeakerInfo.setClickable(true);
            holder.binding.layoutSpeakerInfo.setFocusable(true);
            setItemSelected(holder, selectedItemPosition == position);

            holder.binding.layoutSpeakerInfo.setOnClickListener(v -> {
                if (selectedItemPosition != position) {
                    selectedItemPosition = position;
                    setItemSelected(holder, true);
                    coupleDeviceViewModel.onItemSelected(coupleSpeakerDevices.get(position));
                    notifyDataSetChanged();
                } else {
                    selectedItemPosition = NO_ITEM_SELECTED;
                    setItemSelected(holder, false);
                    coupleDeviceViewModel.onItemSelected(null);
                }
            });
        }
    }

    private void setItemSelected(@NonNull ViewHolder holder, boolean isSelected) {
        holder.binding.layoutSpeakerInfo.setSelected(isSelected);
        holder.binding.checkMark.setVisibility(isSelected ? View.VISIBLE : View.GONE);
    }

    /**
     * Class that keeps view references for every row view object.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        public ListItemCoupleDeviceBinding binding;

        public ViewHolder(@NonNull final ListItemCoupleDeviceBinding binding) {
            super(binding.layoutSpeakerInfo);
            this.binding = binding;
        }
    }
}

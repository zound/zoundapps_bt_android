package com.zoundindustries.marshallbt.ui.player.sources;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.zoundindustries.marshallbt.R;

import java.util.List;

import static android.widget.LinearLayout.HORIZONTAL;

/**
 * Custom {@link HorizontalScrollView} to handle view paging of audio source fragments
 */
public class SourcesHorizontalScrollView extends HorizontalScrollView {

    private static final int HIGHLIGHT_UNDERLINE_STROKE_WIDTH = 10;

    private ViewPager viewPager;
    private LinearLayout linearLayout;
    private Paint paint;
    private int layoutHorizontalPadding;
    private final int imageHorizontalMarginPx = getResources().getDimensionPixelSize(
            R.dimen.player_source_tab_image_horizontal_margin);
    private final int imageVerticalMarginPx = getResources().getDimensionPixelSize(
            R.dimen.player_source_tab_image_vertical_margin);
    private final int imageWidthPx = getResources().getDimensionPixelSize(
            R.dimen.player_source_tab_image_width);
    private final int totalImageWidth = 2 * imageHorizontalMarginPx + imageWidthPx;
    private final int highlightColor = getResources().getColor(R.color.dark_beige);

    /**
     * Constructor that is called if we create this view from Java code
     *
     * @param context the context for this view
     */
    public SourcesHorizontalScrollView(Context context) {
        super(context);
        init();
    }

    /**
     * Constructor that is called if we create this view from xml code
     *
     * @param context the context for this view
     * @param attrs   attributes to this view from xml
     */
    public SourcesHorizontalScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    /**
     * Constructor that is called if we create this view from xml code with style reference
     *
     * @param context      the context for this view
     * @param attrs        attributes to this view from xml
     * @param defStyleAttr a reference to a style which one contains default values for this view
     */
    public SourcesHorizontalScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    /**
     * Method to set the viewPager and image resources for the tabs
     * @param viewPager which one contains the fragments for sources
     */
    public void setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;
    }

    /**
     * Set the images for tabs depend on features
     * @param iconResources the icon imageresources
     */
    public void setTabs(List<Integer> iconResources) {


        LinearLayout.LayoutParams layoutParams =
                new LinearLayout.LayoutParams(
                        imageWidthPx,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

        layoutParams.setMargins(imageHorizontalMarginPx, imageVerticalMarginPx,
                imageHorizontalMarginPx, imageVerticalMarginPx);

        for (int i = 0; i < iconResources.size(); i++) {
            int currentIndex = i;
            ImageView imageView = new ImageView(getContext());
            imageView.setImageResource(iconResources.get(i));
            imageView.setOnClickListener(v -> scrollToChild(currentIndex));
            linearLayout.addView(imageView, layoutParams);
        }

        setLayoutPadding();
        scrollToChild(0);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        super.onTouchEvent(ev);
        switch (ev.getAction()) {
            case MotionEvent.ACTION_UP:
                // When the user stops scroll the view we align the closest tab to the center
                alignChildToCenter();
                break;
        }
        return true;
    }

    /**
     * Scroll to the specific child in {@link SourcesHorizontalScrollView} and to the specific page
     * in the {@link ViewPager} either.
     * @param index the index of the page and scrollview element 
     */
    public void scrollToChild(int index) {
        smoothScrollTo(linearLayout.getChildAt(index).getLeft() -
                imageHorizontalMarginPx -
                layoutHorizontalPadding, 0);
        setHighlight(index);
        viewPager.setCurrentItem(index, true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // The middle of the highlight line is at the middle of the scrollview, so we need to
        // subtract the half size of the image to get the start point. But as we scroll the view the
        // canvas will moving either, so we need to add the scrolled distance to the start point.
        // The end of the line is one image size far from the start point.
        float startX = getWidth() / 2 + getScrollX() - totalImageWidth / 2;
        float stopX = startX + totalImageWidth;
        // Draw the highlight line at the center of the ScrollView
        // (the position always changes if view is scrolling)
        canvas.drawLine(
                startX,
                canvas.getHeight(),
                stopX,
                canvas.getHeight(),
                paint
        );
    }

    private void init() {
        setPaint();
        setLayoutBehaviours();
        setHorizontalScrollBarEnabled(false);
    }

    private void setPaint() {
        paint = new Paint();
        paint.setStrokeWidth(HIGHLIGHT_UNDERLINE_STROKE_WIDTH);
        paint.setColor(highlightColor);
    }

    private void setLayoutBehaviours() {
        linearLayout = new LinearLayout(getContext());

        LinearLayout.LayoutParams layoutParams =
                new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

        linearLayout.setClipToPadding(false);
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setOrientation(HORIZONTAL);
        addView(linearLayout, layoutParams);
    }

    private void setLayoutPadding() {
        // We need to add padding to the left and right side of the LinearLayout, because we only
        // can scroll the View if the content is wider than the scrollview.
        //
        // Padding always has size:
        //      (width of all of the tabs) - (width of 1 element)
        //
        // let's consider two situations:
        //  * view scrolled max to left
        //          |[ X ][ x ][ x ]__________|
        //  * view scrolled max to right
        //          | ________ [ X ][ x ][ x ]|
        //
        //          Where [ x ] is an item, and ____ is padding
        //
        // To make the content wide enough to scroll to the most left/right element we need to add
        // (n-1) size padding to both sides
        layoutHorizontalPadding = (linearLayout.getChildCount() - 1) * (totalImageWidth);
        linearLayout.setPadding(layoutHorizontalPadding, 0, layoutHorizontalPadding,0);
        getLayoutParams().width = 2 * layoutHorizontalPadding + totalImageWidth;
    }

    private void alignChildToCenter() {
        float center = getWidth() / 2 + getScrollX();
        float minDistance = Integer.MAX_VALUE;
        int closestChildIndex = 0;

        for (int i = 0; i < linearLayout.getChildCount(); i++) {
            float childCenter = linearLayout.getChildAt(i).getLeft() +
                    linearLayout.getChildAt(i).getWidth()/2;

            if(Math.abs(childCenter - center) < minDistance) {
                closestChildIndex = i;
                minDistance = Math.abs(childCenter - center);
            }
        }
        scrollToChild(closestChildIndex);
    }

    private void setHighlight(int index) {
        for (int i = 0; i < linearLayout.getChildCount(); i++) {
            ((ImageView)linearLayout.getChildAt(i)).clearColorFilter();
        }
        ((ImageView)linearLayout.getChildAt(index)).setColorFilter(highlightColor);
    }
}
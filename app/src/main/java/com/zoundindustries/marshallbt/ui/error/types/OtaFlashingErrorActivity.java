package com.zoundindustries.marshallbt.ui.error.types;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.ota.pojo.FirmwareFileMetaData;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.ui.devicesettings.DeviceSettingsActivity;
import com.zoundindustries.marshallbt.ui.error.BaseErrorActivity;
import com.zoundindustries.marshallbt.ui.homescreen.HomeActivity;
import com.zoundindustries.marshallbt.ui.ota.OTAActivity;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;
import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_OTA_FIRMWARE_META_DATA;

/**
 * Class used to show ota flashing error screen
 */
public class OtaFlashingErrorActivity extends BaseErrorActivity {
    //todo add a "baseOtaErrorActivity"
    private String deviceId;
    private FirmwareFileMetaData firmwareFileMetaData;

    /**
     * Encapsulates intent creation and setup of the Activity.
     *
     * @param context for the Activity start
     * @return configured starting Intent
     */
    @NonNull
    public static Intent create(@NonNull Context context,
                                @NonNull String id,
                                @NonNull ViewType errorOrigin,
                                @NonNull FirmwareFileMetaData firmwareFileMetaData) {
        Intent intent = new Intent(context, OtaFlashingErrorActivity.class);
        intent.putExtra(EXTRA_DEVICE_ID, id);
        intent.putExtra(EXTRA_ERROR_ORIGIN, errorOrigin);
        intent.putExtra(EXTRA_OTA_FIRMWARE_META_DATA, firmwareFileMetaData);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            deviceId = extras.getString(EXTRA_DEVICE_ID, "");
            errorOrigin = (ViewType) extras.getSerializable(EXTRA_ERROR_ORIGIN);
            firmwareFileMetaData = extras.getParcelable(EXTRA_OTA_FIRMWARE_META_DATA);
        }
        initObservers();
    }

    @Override
    protected int getTitleTextId() {
        return R.string.error_firmware_update_title_uc;
    }

    @Override
    protected int getDescriptionTextId() {
        return R.string.error_firmware_update_subtitle;
    }

    @Override
    protected int getActionButtonTextId() {
        return R.string.appwide_try_again_uc;
    }

    @Override
    protected boolean isBackButtonVisible() {
        return false;
    }

    @Override
    protected boolean isHardwareBackButtonActive() {
        return false;
    }

    @Override
    protected ViewType getErrorType() {
        return ViewType.ERROR_OTA_FLASHING;
    }

    private void initObservers() {
        errorViewModel.outputs.isViewChanged().observe(this, viewType -> {
            switch (viewType) {
                case HOME_SCREEN:
                    startActivity(HomeActivity.create(this));
                    break;
                case ABOUT_DEVICE:
                    startActivity(DeviceSettingsActivity.create(this, deviceId,
                            ViewType.ABOUT_DEVICE, true, firmwareFileMetaData));
                    break;
                case OTA_PROGRESS:
                    startActivity(OTAActivity.create(this, deviceId));
                    break;
            }
            finish();
        });
    }
}

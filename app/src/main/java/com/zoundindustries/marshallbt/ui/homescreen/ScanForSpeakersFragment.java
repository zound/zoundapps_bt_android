package com.zoundindustries.marshallbt.ui.homescreen;


import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshallbt.databinding.FragmentScanForSpeakersBinding;
import com.zoundindustries.marshallbt.di.Injectable;
import com.zoundindustries.marshallbt.ui.BaseFragment;

/**
 * Fragment showing waiting view when scanning for new devices ion the system.
 */
public class ScanForSpeakersFragment extends BaseFragment implements Injectable {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return FragmentScanForSpeakersBinding.inflate(inflater).getRoot();
    }
}

package com.zoundindustries.marshallbt.ui.mainmenu;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.ActivityMainMenuBinding;
import com.zoundindustries.marshallbt.ui.BaseToolbarActivity;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class MainMenuActivity extends BaseToolbarActivity implements HasSupportFragmentInjector {

    private ActivityMainMenuBinding binding;
    private PublishSubject<Boolean> publishSubject = PublishSubject.create();

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    static final String MAIN_MENU_FRAGMENT_VIEW_TYPE = "MAIN_MENU_FRAGMENT_VIEW_TYPE";

    /**
     * Encapsulates intent creation and setup of the Activity.
     *
     * @param context  for the Activity start
     * @param viewType is type of {@link MainMenuFragment} which will be opened
     *                 (displayed list of items)
     * @return configured starting Intent
     */
    @NonNull
    public static Intent create(@NonNull Context context, ViewType viewType) {
        Intent intent = new Intent(context, MainMenuActivity.class);
        intent.putExtra(MAIN_MENU_FRAGMENT_VIEW_TYPE, viewType.ordinal());
        return intent;
    }

    @Override
    public void setToolbarName(@NonNull String name) {
        binding.mainMenuToolbar.toolbarTitle.setText(name);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main_menu);
        int viewType = getIntent().getExtras().getInt(MAIN_MENU_FRAGMENT_VIEW_TYPE);
        if (savedInstanceState == null) {
            startFragment(R.id.main_menu_fragment_container,
                    new MainMenuFragment(), createArguments(viewType), true);
        }
        setupToolbar();
    }

    @NonNull
    private Bundle createArguments(int itemNameResourceId) {
        Bundle arguments = new Bundle();
        arguments.putInt(MAIN_MENU_FRAGMENT_VIEW_TYPE, itemNameResourceId);
        return arguments;
    }

    private void setupToolbar() {
        binding.mainMenuToolbar.toolbarImage.setOnClickListener(view ->
                publishSubject.onNext(true));
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    @Override
    public Observable<Boolean> getBackPressedObservable() {
        return publishSubject;
    }

    @Override
    public void setToolbarImageVisibility(int visibility) {
        binding.mainMenuToolbar.toolbarImage.setVisibility(visibility);
    }

    @Override
    public void changeToolbarColor(@NonNull ToolbarItemsColor toolbarItemsColor) {
        super.changeToolbarColor(toolbarItemsColor);
        binding.mainMenuToolbar.toolbarImage.setImageDrawable(toolbarDrawable);
        binding.mainMenuToolbar.toolbar.setBackgroundColor(toolbarBackgroundColorId);
        binding.mainMenuToolbar.toolbarTitle.setTextColor(toolbarTextColor);
    }

    @Override
    public void setToolbarVisibility(Boolean isVisible) {
        binding.mainMenuToolbar.toolbar.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }
}

package com.zoundindustries.marshallbt.ui.setup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.ui.BaseActivity;
import com.zoundindustries.marshallbt.ui.error.types.NoNetworkErrorActivity;
import com.zoundindustries.marshallbt.utils.CommonPreferences;
import com.zoundindustries.marshallbt.utils.NetworkUtils;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

/**
 * Setup activity used to gather user's email adress and an agreement for data collection and
 * terms and conditions
 */
public class SetupActivity extends BaseActivity implements HasSupportFragmentInjector {

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    /**
     * Encapsulates intent creation and setup of the Activity.
     *
     * @param context for the Activity start
     * @return configured starting Intent
     */
    @NonNull
    public static Intent create(@NonNull Context context) {
        return new Intent(context, SetupActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);

        CommonPreferences preferences = new CommonPreferences(getApplication());
        if (preferences.isFirstTimeSubscription()) {
            startFragment(new StayUpdatedFragment());
        } else {
            startFragment(new ShareDataFragment());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!NetworkUtils.isOnline(this)) {
            startActivity(NoNetworkErrorActivity.create(this));
        }
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    private void startFragment(@NonNull Fragment fragment) {
        getSupportFragmentManager().beginTransaction().add(R.id.setup_fragment_container,
                fragment).commit();
    }
}

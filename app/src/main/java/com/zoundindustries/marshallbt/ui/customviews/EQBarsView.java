package com.zoundindustries.marshallbt.ui.customviews;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.h6ah4i.android.widget.verticalseekbar.VerticalSeekBar;
import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.devicesettings.EQData;
import com.zoundindustries.marshallbt.ui.devicesettings.EQSeekBar;
import com.zoundindustries.marshallbt.utils.DebouncedSeekBarListener;

import java.util.ArrayList;
import java.util.List;

/**
 * View class wrapping setting of EQ bars values and labels with an easy API.
 */
public class EQBarsView extends ConstraintLayout {
    //TODO: thiw will be optimized and switched to use

    private static final int EQ_BARS_NUMBER = 5;
    private static final int ANIMATION_DURATION = 300;

    int barTouchCounter;
    IEQBarsViewValueChangeListener valueChangeListener;

    private List<VerticalSeekBar> bars;

    private TextView label1;
    private TextView label2;
    private TextView label3;
    private TextView label4;
    private TextView label5;

    public EQBarsView(Context context) {
        super(context);
        init(context);
    }

    public EQBarsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public EQBarsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    /**
     * Set listener vor any of the bars value change.
     *
     * @param listener that will be notified of value change.
     */
    public void setOnValueChangedListener(IEQBarsViewValueChangeListener listener) {
        this.valueChangeListener = listener;
    }

    /**
     * Set int values for EQ bars progress. Value range is [0-100].
     *
     * @param barData array with bars progress values. It is fixed size 5.
     */
    public void setEQValues(EQData barData, boolean shouldAnimate) {

        //values can be set programmatically only if there is no user touch interaction with any
        // of the bars
        if (barTouchCounter > 0) {
            return;
        }

        if (shouldAnimate) {
            animateToPosition(bars.get(0), barData.getBass()).setDuration(ANIMATION_DURATION).start();
            animateToPosition(bars.get(1), barData.getLow()).setDuration(ANIMATION_DURATION).start();
            animateToPosition(bars.get(2), barData.getMid()).setDuration(ANIMATION_DURATION).start();
            animateToPosition(bars.get(3), barData.getUpper()).setDuration(ANIMATION_DURATION).start();
            animateToPosition(bars.get(4), barData.getHigh()).setDuration(ANIMATION_DURATION).start();
        } else {
            bars.get(0).setProgress(barData.getBass());
            bars.get(1).setProgress(barData.getLow());
            bars.get(2).setProgress(barData.getMid());
            bars.get(3).setProgress(barData.getUpper());
            bars.get(4).setProgress(barData.getHigh());
        }
    }

    /**
     * Set lables for bars. Text will appear under each bar with corresponding index number from
     * left to right.
     *
     * @param labels array with labels for bars. It is fixed size 5.
     */
    public void setLabels(ArrayList<String> labels) {
        if (labels.size() != EQ_BARS_NUMBER) {
            throw new IllegalArgumentException("Array size must be " + EQ_BARS_NUMBER);
        }
        label1.setText(labels.get(0));
        label2.setText(labels.get(1));
        label3.setText(labels.get(2));
        label4.setText(labels.get(3));
        label5.setText(labels.get(4));
    }

    /**
     * Set bars view enabled/disabled.
     *
     * @param enabled flag indicating enabled/disabled state.
     */
    public void setEnabled(boolean enabled) {
        for (VerticalSeekBar bar : bars) {
            bar.setEnabled(enabled);
            bar.getProgressDrawable().setColorFilter(
                    getResources().getColor(enabled ? R.color.colorAccent :
                            R.color.colorPrimaryDark),
                    PorterDuff.Mode.SRC_IN);
            bar.getThumb().setColorFilter(
                    getResources().getColor(enabled ? R.color.share_data_switch_thumb_color :
                            R.color.highlight),
                    PorterDuff.Mode.SRC_IN
            );
        }
    }

    /**
     * Animate the {@link EQSeekBar} and change the progress value to the given position
     *
     * @param position that the new value of progress
     */
    public ValueAnimator animateToPosition(VerticalSeekBar bar, int position) {
        ValueAnimator animator = ValueAnimator.ofInt(bar.getProgress(), position);
        animator.addUpdateListener(animation -> {
            bar.setProgress((Integer) animation.getAnimatedValue());
            onSizeChanged(getWidth(), getHeight(), 0, 0);
        });
        return animator;
    }

    private void notifyValueChanged(EQData data) {
        if (valueChangeListener != null) {
            valueChangeListener.onValueChanged(data);
        }
    }


    private void init(Context context) {
        String service = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(service);
        ConstraintLayout layout = (ConstraintLayout) layoutInflater
                .inflate(R.layout.extended_eq_bars, this, true);

        bars = new ArrayList<>();

        bars.add(layout.findViewById(R.id.bar1));
        bars.add(layout.findViewById(R.id.bar2));
        bars.add(layout.findViewById(R.id.bar3));
        bars.add(layout.findViewById(R.id.bar4));
        bars.add(layout.findViewById(R.id.bar5));

        label1 = layout.findViewById(R.id.label1);
        label2 = layout.findViewById(R.id.label2);
        label3 = layout.findViewById(R.id.label3);
        label4 = layout.findViewById(R.id.label4);
        label5 = layout.findViewById(R.id.label5);

        setupOnProgressChangedListeners();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setDebounceProgressChangedListener(int index) {
        bars.get(index).setOnSeekBarChangeListener(new DebouncedSeekBarListener() {
            @Override
            public void runTask(@NonNull SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    notifyValueChanged(new EQData(new int[]{
                            index == 0 ? progress : bars.get(0).getProgress(),
                            index == 1 ? progress : bars.get(1).getProgress(),
                            index == 2 ? progress : bars.get(2).getProgress(),
                            index == 3 ? progress : bars.get(3).getProgress(),
                            index == 4 ? progress : bars.get(4).getProgress()}));
                }
            }
        });

        bars.get(index).setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    barTouchCounter++;
                    break;
                case MotionEvent.ACTION_UP:
                    v.performClick();
                    barTouchCounter--;
                    break;
                default:
                    break;
            }
            return false;
        });
    }

    private void setupOnProgressChangedListeners() {
        for (int i = 0; i < bars.size(); i++) {
            setDebounceProgressChangedListener(i);
        }
    }

}



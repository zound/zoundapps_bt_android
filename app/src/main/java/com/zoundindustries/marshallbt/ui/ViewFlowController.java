package com.zoundindustries.marshallbt.ui;

import android.os.Bundle;
import androidx.annotation.NonNull;

/**
 * Class that is used for handling switching between views.
 * <p>
 * To be able to register for view changes and leave this logic to viewmodels, not view classes,
 * there is a ViewType enum that holds all the views that could be displayed in the app.
 */
public class ViewFlowController {

    /**
     * Holds types of all views that appear in the app
     */
    public enum ViewType {
        UNKNOWN,
        FINISH_ACTIVITY,
        BACK,
        HOME_SCREEN,
        OTA_PROGRESS,
        OTA_COMPLETED,
        SETUP_SCREEN,

        // device settings
        DEVICE_SETTINGS,
        ABOUT_DEVICE,

        // m-button
        M_BUTTON,
        M_BUTTON_GVA_DETAIL,

        RENAME,
        EQUALISER,
        EQ_EXTENDED,
        ANC,
        LIGHT,
        TIMER_OFF,
        COUPLE_SPEAKERS,
        COUPLE_MODE_SELECTION,
        COUPLE_DONE,
        COUPLE_HEADS_UP,
        FORGET_DEVICE,
        SOUNDS,

        //Other
        SAVE_DATA_SCREEN,
        SHARE_DATA_SCREEN,
        TAC_SCREEN,
        EMPTY_DEVICE_LIST,
        NON_EMPTY_DEVICE_LIST,
        DECOUPLING_SPINNER,
        GLOBAL_SETTINGS_BLUETOOTH,
        GLOBAL_SETTINGS_NETWORK,
        GLOBAL_SETTINGS_LOCATION,
        ERROR_OTA_DOWNLOAD,
        ERROR_OTA_FLASHING,
        ERROR_BLUETOOTH,
        ERROR_LOCATION,
        ERROR_NO_NETWORK,
        ERROR_OTA_UNDEFINED,
        SOURCES,
        ENABLE_PAIRING,

        //Main menu
        MAIN_MENU_SETTINGS,
        MAIN_MENU_HELP,
        MAIN_MENU_ABOUT,
        MAIN_MENU_EMAIL_SUBSCRIPTION,
        MAIN_MENU_ANALYTICS,
        MAIN_MENU_QUICK_GUIDE_LIST,
        MAIN_MENU_QUICK_GUIDE_JOPLIN,
        MAIN_MENU_QUICK_GUIDE_OZZY,
        MAIN_MENU_ONLINE_MANUAL,
        MAIN_MENU_CONTACT,
        MAIN_MENU_EULA,
        MAIN_MENU_FOSS,
        MAIN_MENU_QUICK_GUIDE_JOPLIN_BLUETOOTH_PAIRING,
        MAIN_MENU_QUICK_GUIDE_OZZY_BLUETOOTH_PAIRING,
        MAIN_MENU_QUICK_GUIDE_JOPLIN_COUPLE_SPEAKERS,
        MAIN_MENU_QUICK_GUIDE_JOPLIN_PLAY_PAUSE_BUTTON,
        MAIN_MENU_QUICK_GUIDE_OZZY_ANC,
        MAIN_MENU_QUICK_GUIDE_OZZY_M_BUTTON,
        MAIN_MENU_QUICK_GUIDE_OZZY_CONTROL_KNOB,
        MAIN_MENU_CONTACT_WEBSITE,
        MAIN_MENU_CONTACT_SUPPORT,
        MAIN_MENU_UNSUBSCRIBE,
        PRIVACY_POLICY_SCREEN,
        MAIN_MENU_ONLINE_MANUAL_DEVICE,
        MAIN_MENU_FOSS_WEBSITE,

        //Onboarding
        ONBOARDING_GUIDE;

        private Bundle args;

        /**
         * Get arguments set for passing to the target view.
         *
         * @return Bundle with set arguments.
         */
        @NonNull
        public Bundle getArgs() {
            return args;
        }

        /**
         * Set arguments for passing to the target view.
         *
         * @param args to be set to the Bundle.
         */
        public ViewType setArgs(@NonNull Bundle args) {
            this.args = args;
            return this;
        }
    }
}

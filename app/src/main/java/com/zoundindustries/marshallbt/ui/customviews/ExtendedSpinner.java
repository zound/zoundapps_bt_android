package com.zoundindustries.marshallbt.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;

import androidx.appcompat.widget.AppCompatSpinner;

/**
 * Class wrapping standard {@link AppCompatSpinner} and adds detecting user actions on
 * item selection.
 */
public class ExtendedSpinner extends AppCompatSpinner {

    private IExtendedSpinnerListener extendedSpinnerListener;
    private boolean isUserAction;

    public ExtendedSpinner(Context context) {
        super(context);
        init();
    }

    public ExtendedSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ExtendedSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    public boolean performClick() {
        isUserAction = true;
        return super.performClick();
    }

    /**
     * Set extendedSpinnerListener to get user action extension on item selection event.
     *
     * @param extendedSpinnerListener to be notified.
     */
    public void setExtendedSpinnerListener(IExtendedSpinnerListener extendedSpinnerListener) {
        this.extendedSpinnerListener = extendedSpinnerListener;
    }

    private void init() {
        super.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                extendedSpinnerListener.onItemSelected(position, isUserAction);
                isUserAction = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}

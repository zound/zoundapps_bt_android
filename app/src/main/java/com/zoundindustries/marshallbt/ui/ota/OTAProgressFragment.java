package com.zoundindustries.marshallbt.ui.ota;

import android.annotation.SuppressLint;
import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentOtaProgressBinding;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.ui.error.types.OtaDownloadingErrorActivity;
import com.zoundindustries.marshallbt.ui.error.types.OtaFlashingErrorActivity;
import com.zoundindustries.marshallbt.ui.error.types.OtaUndefinedErrorActivity;
import com.zoundindustries.marshallbt.viewmodels.factories.ota.OTAProgressViewModelFactory;
import com.zoundindustries.marshallbt.viewmodels.ota.OTAProgressViewModel;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

/**
 * Fragment showing progress of OTA firmware update with advertising slides.
 */
public class OTAProgressFragment extends BaseFragment {

    private static final String TAG = OTAProgressFragment.class.getSimpleName();

    private OTAProgressViewModel.Body otaViewModel;
    private int[] layouts;
    private ViewPager.OnPageChangeListener onPageChangeListener;
    private FragmentOtaProgressBinding binding;

    public OTAProgressFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String deviceId = getArguments().getString(EXTRA_DEVICE_ID, "");

        OTAProgressViewModelFactory viewModelFactory =
                new OTAProgressViewModelFactory(getActivity().getApplication(), deviceId);

        otaViewModel = ViewModelProviders.of(this,
                viewModelFactory).get(OTAProgressViewModel.Body.class);

        initObservers(deviceId);
    }

    @SuppressLint("CheckResult")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = FragmentOtaProgressBinding.inflate(inflater);

        binding.setViewModel(otaViewModel);
        binding.setLifecycleOwner(this);
        binding.updateProgress.setEnabled(false);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViewPager(binding);
    }

    private void initObservers(String deviceId) {

        otaViewModel.outputs.isViewChanged().observe(this,
                viewType -> {
                    switch (viewType) {
                        case OTA_COMPLETED:
                            getActivity()
                                    .getSupportFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.ota_fragment_container,
                                            getOtaCompletedFragment(deviceId))
                                    .commit();
                            break;
                        case ERROR_OTA_FLASHING:
                            startActivity(OtaFlashingErrorActivity.create(getContext(),
                                    deviceId, ViewType.OTA_PROGRESS,
                                    otaViewModel.outputs.getFirmwareUpdateMetaData()));
                            getActivity().finish();
                            break;
                        case ERROR_OTA_DOWNLOAD:
                            startActivity(OtaDownloadingErrorActivity.create(getContext(),
                                    deviceId, ViewType.OTA_PROGRESS,
                                    otaViewModel.outputs.getFirmwareUpdateMetaData()));
                            getActivity().finish();
                            break;
                        case ERROR_OTA_UNDEFINED:
                            startActivity(OtaUndefinedErrorActivity.create(getContext(),
                                    deviceId, ViewType.OTA_PROGRESS,
                                    otaViewModel.outputs.getFirmwareUpdateMetaData()));
                            getActivity().finish();
                            break;
                    }
                });
    }

    private void initViewPager(FragmentOtaProgressBinding binding) {
        ViewPager viewPager = binding.viewPager;

        layouts = new int[]{
                R.layout.ota_slide1,
                R.layout.ota_slide2,
                R.layout.ota_slide3};

        viewPager.setAdapter(new OTAViewPagerAdapter());
        setupViewPagerListener();
        viewPager.addOnPageChangeListener(onPageChangeListener);
        otaViewModel.inputs.onPageChanged(0);
    }

    @NonNull
    private Fragment getOtaCompletedFragment(@NonNull String deviceId) {
        Fragment fragment = new OTACompletedFragment();
        Bundle argBundle = new Bundle();
        argBundle.putString(EXTRA_DEVICE_ID, deviceId);
        fragment.setArguments(argBundle);
        return fragment;
    }

    private void setupViewPagerListener() {
        onPageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                otaViewModel.inputs.onPageChanged(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        };
    }

    /**
     * Class customizing viewPagerAdapter to use custom views.
     */
    public class OTAViewPagerAdapter extends PagerAdapter {

        private LayoutInflater layoutInflater;

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {

            layoutInflater = LayoutInflater.from(OTAProgressFragment.this.getActivity());

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
            return view == obj;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position,
                                @NonNull Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}

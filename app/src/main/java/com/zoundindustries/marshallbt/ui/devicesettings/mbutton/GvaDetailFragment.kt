package com.zoundindustries.marshallbt.ui.devicesettings.mbutton

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.zoundindustries.marshallbt.R
import com.zoundindustries.marshallbt.databinding.FragmentGvaDetailBinding
import com.zoundindustries.marshallbt.ui.BaseFragment

/**
 * Fragment displaying details of Google Voice Assistant
 */
class GvaDetailFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentGvaDetailBinding =
                DataBindingUtil.inflate(inflater,
                        R.layout.fragment_gva_detail,
                        container,
                        false)

        binding.gvaDetailButton.setOnClickListener {
            performBackPressedOrFinish()
        }

        return binding.root
    }
}
package com.zoundindustries.marshallbt.ui.devicesettings;

import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentCoupleDeviceBinding;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.ui.adapters.CoupleDeviceAdapter;
import com.zoundindustries.marshallbt.viewmodels.devicesettings.CouplingListViewModel;
import com.zoundindustries.marshallbt.viewmodels.factories.devicesettings.CouplingListViewModelFactory;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

/**
 * Class used to show couple function for speakers.
 */
public class CouplingListFragment extends BaseFragment {

    private CouplingListViewModel.Body viewModel;
    private CoupleDeviceAdapter coupleDevicesAdapter;
    private FragmentCoupleDeviceBinding binding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CouplingListViewModelFactory viewModelFactory = new CouplingListViewModelFactory(
                        getActivity().getApplication(),
                        getArguments().getString(EXTRA_DEVICE_ID,
                                ""));

        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(CouplingListViewModel.Body.class);

        coupleDevicesAdapter =
                new CoupleDeviceAdapter(viewModel.getDevicesAvailableForCoupling().getValue(),
                        viewModel);

        initLiveData();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = FragmentCoupleDeviceBinding.inflate(inflater);
        binding.setLifecycleOwner(this);
        binding.setViewModel(viewModel);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.listCoupleDevice.setAdapter(coupleDevicesAdapter);
        binding.listCoupleDevice.setLayoutManager(new LinearLayoutManager(getActivity()));
        setupToolbar(R.string.device_settings_menu_item_couple_uc, View.VISIBLE, true);
    }

    private void initLiveData() {
        viewModel.getDevicesAvailableForCoupling()
                .observe(this, devices -> coupleDevicesAdapter.notifyDataSetChanged());
        viewModel.isItemSelected().observe(this, isItemSelected ->
                binding.buttonNext.setVisibility(isItemSelected ? View.VISIBLE : View.GONE));
        viewModel.isViewChanged().observe(this, viewType -> {
            switch (viewType) {
                case COUPLE_MODE_SELECTION:
                    startFragment(R.id.device_settings_fragment_container,
                            new CouplingModeFragment(), viewType.getArgs(), true);
                    break;
                case BACK:
                    performBackPressedOrFinish();
                    break;
                case HOME_SCREEN:
                    if (getActivity() != null) {
                        getActivity().finish();
                    }
                    break;
            }
        });
    }
}

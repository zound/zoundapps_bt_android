package com.zoundindustries.marshallbt.ui.ota;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.ui.BaseActivity;
import com.zoundindustries.marshallbt.ui.error.types.NoNetworkErrorActivity;
import com.zoundindustries.marshallbt.utils.NetworkUtils;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

public class OTAActivity extends BaseActivity {

    /**
     * Encapsulates intent creation and setup of the Activity.
     *
     * @param context for the Activity start
     * @return configured starting Intent
     */
    @NonNull
    public static Intent create(@NonNull Context context, @NonNull String id) {
        Intent intent = new Intent(context, OTAActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(EXTRA_DEVICE_ID, id);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ota);
        Bundle extras = getIntent().getExtras();
        String extraId = extras.getString(EXTRA_DEVICE_ID);
        if (!TextUtils.isEmpty(extraId)) {
            Bundle argBundle = new Bundle();
            argBundle.putString(EXTRA_DEVICE_ID, extraId);
            startFragment(new OTAAvailableFragment(), argBundle);
        } else {
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!NetworkUtils.isOnline(this)) {
            startActivity(NoNetworkErrorActivity.create(this));
        }
    }

    @Override
    public void onBackPressed() {
        //we should not allow exiting to previous screen
        //do nothing, ignore back press
    }

    private void startFragment(@NonNull Fragment fragment, @Nullable Bundle arguments) {
        if (arguments != null) {
            fragment.setArguments(arguments);
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.ota_fragment_container,
                fragment).commit();
    }

}

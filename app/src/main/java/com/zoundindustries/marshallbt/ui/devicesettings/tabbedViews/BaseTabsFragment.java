package com.zoundindustries.marshallbt.ui.devicesettings.tabbedViews;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentTabbedLoadingBinding;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.ui.adapters.TabsPagerAdapter;

import java.util.List;

/**
 * Base class for fragment with tabs. Child has to provide list of fragments to display.
 */
public abstract class BaseTabsFragment extends BaseFragment {

    private static final int OFFSCREEN_PAGE_LIMIT = 2;

    protected FragmentTabbedLoadingBinding binding;

    public abstract List<TabContainer> getTabsList();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentTabbedLoadingBinding.inflate(inflater);
        binding.setLifecycleOwner(this);
        binding.tabsContainer.loadLayoutDescription(R.xml.eq_tabs_states);
        binding.pager.setOffscreenPageLimit(OFFSCREEN_PAGE_LIMIT);

        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();

        TabsPagerAdapter adapter =
                new TabsPagerAdapter(
                        getChildFragmentManager(),
                        getTabsList());

        binding.pager.setAdapter(adapter);
        binding.tabs.setupWithViewPager(binding.pager);
    }
}



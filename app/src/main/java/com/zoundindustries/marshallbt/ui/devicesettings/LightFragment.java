package com.zoundindustries.marshallbt.ui.devicesettings;

import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentLightBinding;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.utils.DebouncedSeekBarListener;
import com.zoundindustries.marshallbt.viewmodels.devicesettings.LightViewModel;
import com.zoundindustries.marshallbt.viewmodels.factories.devicesettings.LightViewModelFactory;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

/**
 * Class used to show device settings light
 */
public class LightFragment extends BaseFragment {
    private LightViewModel.Body viewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LightViewModelFactory viewModelFactory = new LightViewModelFactory(
                getActivity().getApplication(),
                getArguments().getString(EXTRA_DEVICE_ID, ""));

        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(LightViewModel.Body.class);

        initObservers();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        FragmentLightBinding binding = FragmentLightBinding.inflate(inflater);
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);

        binding.lightSeekbar.setOnSeekBarChangeListener(new DebouncedSeekBarListener() {
            @Override
            public void runTask(@NonNull SeekBar seekBar, int progress, boolean fromUser) {
                viewModel.inputs.lightSeekBarProgressChanged(seekBar, progress, fromUser);
            }

            @Override
            public void onStartTrackingTouch(@NonNull SeekBar seekBar) {
                binding.lightSeekbar.showProgress();
            }

            @Override
            public void onStopTrackingTouch(@NonNull SeekBar seekBar) {
                binding.lightSeekbar.hideProgress();
            }
        });

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolbar(R.string.device_settings_menu_item_light_uc, View.VISIBLE, true);
    }

    private void initObservers() {
        viewModel.outputs.isViewChanged().observe(this, viewType -> {
            if (getActivity() != null) {
                switch (viewType) {
                    case BACK:
                        performBackPressedOrFinish();
                        break;
                    case HOME_SCREEN:
                        if (getActivity() != null) {
                            getActivity().finish();
                        }
                        break;
                }
            }
        });
    }

}

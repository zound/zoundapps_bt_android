package com.zoundindustries.marshallbt.ui.error.types;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.ui.ViewFlowController;
import com.zoundindustries.marshallbt.ui.error.BaseErrorActivity;
import com.zoundindustries.marshallbt.utils.SystemServicesUtils;
import com.zoundindustries.marshallbt.viewmodels.btstate.BtStateViewModel;
import com.zoundindustries.marshallbt.viewmodels.factories.btstate.BtStateViewModelFactory;

/**
 * Used to display bluetooth error
 */
public class BluetoothErrorActivity extends BaseErrorActivity {

    private BtStateViewModel.Body btStateViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BluetoothApplication) getApplication()).getAppComponent()
                .firebaseWrapper().eventErrorOccurredBluetooth();

        btStateViewModel = ViewModelProviders.of(this,
                new BtStateViewModelFactory(getApplication()))
                .get(BtStateViewModel.Body.class);

        initObservers();
    }

    private void initObservers() {
        errorViewModel.outputs.isViewChanged().observe(this, viewType -> {
            if (viewType != null) {
                switch (viewType) {
                    case GLOBAL_SETTINGS_BLUETOOTH:
                        openBluetoothGlobalSettings();
                        break;
                }
            }
        });

        //close this activity if BT is turned ON from quick menu
        btStateViewModel.getOutputs().isBtAvailable().observe(this, btAvailable -> {
            if (btAvailable) {
                finish();
            }
        });
    }

    @Override
    protected int getTitleTextId() {
        return R.string.error_no_bluetooth_title_uc;
    }

    @Override
    protected int getDescriptionTextId() {
        return R.string.error_no_bluetooth_subtitle;
    }

    @Override
    protected int getActionButtonTextId() {
        return R.string.appwide_go_to_settings_uc;
    }

    @Override
    protected boolean isBackButtonVisible() {
        return false;
    }

    @Override
    protected boolean isHardwareBackButtonActive() {
        return true;
    }

    @Override
    protected ViewFlowController.ViewType getErrorType() {
        return ViewFlowController.ViewType.ERROR_BLUETOOTH;
    }

    /**
     * Create intent of this class. Used in purpose of starting this activity.
     *
     * @param context for the Activity start
     * @return Intent of BluetoothErrorActivity
     */
    @NonNull
    public static Intent create(@NonNull Context context) {
        Intent intent = new Intent(context, BluetoothErrorActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    private void openBluetoothGlobalSettings() {
        try {
            startActivity(SystemServicesUtils.getBluetoothSettingsIntent());
        } catch (ActivityNotFoundException exception) {
            startActivity(SystemServicesUtils.getGlobalSettingsIntent());
        }
    }
}

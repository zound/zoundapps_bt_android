package com.zoundindustries.marshallbt.ui.error.types;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.ui.error.BaseErrorActivity;
import com.zoundindustries.marshallbt.utils.NetworkUtils;

/**
 * Error activity used when network is not turned on on device. Redirects user to phone settings.
 */
public class NoNetworkErrorActivity extends BaseErrorActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BluetoothApplication) getApplication()).getAppComponent()
                .firebaseWrapper().eventErrorOccurredNetwork();
        initObservers();
    }

    private void initObservers() {
        errorViewModel.outputs.isViewChanged().observe(this, viewType -> {
            if (viewType != null) {
                if (viewType == ViewType.GLOBAL_SETTINGS_NETWORK) {
                    openNetworkGlobalSettings();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (NetworkUtils.isOnline(this)) {
            finish();
        }
    }

    @Override
    protected int getTitleTextId() {
        return R.string.error_no_internet_title_uc;
    }

    @Override
    protected int getDescriptionTextId() {
        return R.string.error_no_internet_subtitle;
    }

    @Override
    protected int getActionButtonTextId() {
        return R.string.appwide_go_to_settings_uc;
    }

    @Override
    protected boolean isBackButtonVisible() {
        return false;
    }

    @Override
    protected boolean isHardwareBackButtonActive() {
        return true;
    }

    @Override
    protected ViewType getErrorType() {
        return ViewType.ERROR_NO_NETWORK;
    }

    /**
     * Create intent of this class. Used in purpose of starting this activity.
     * @param context for the Activity start
     * @return Intent of NoNetworkErrorActivity
     */
    @NonNull
    public static Intent create(@NonNull Context context) {
        return new Intent(context, NoNetworkErrorActivity.class);
    }

    private void openNetworkGlobalSettings() {
        Intent intent=new Intent(Settings.ACTION_WIRELESS_SETTINGS);
        startActivity(intent);
    }
}

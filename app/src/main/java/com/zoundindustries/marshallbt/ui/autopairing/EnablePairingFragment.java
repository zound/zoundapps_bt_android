package com.zoundindustries.marshallbt.ui.autopairing;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.transition.TransitionManager;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentEnablePairingOngoingBinding;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.ui.ViewFlowController;
import com.zoundindustries.marshallbt.ui.customviews.IToolbarConfigurator;
import com.zoundindustries.marshallbt.viewmodels.autopairing.EnablePairingViewModel;
import com.zoundindustries.marshallbt.viewmodels.factories.autopairing.EnablePairingViewModelFactory;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;
import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_PAIRING_MODE_ENABLED;
import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_PAIRING_MODE_ENABLED_DEVICE_NAME;

/**
 * Fragment shown only for devices with AUTO_PAIRING support.
 * It guides user through the setup process.
 */
public class EnablePairingFragment extends BaseFragment {

    private EnablePairingViewModel.Body viewModel;
    private FragmentEnablePairingOngoingBinding binding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        String deviceId = args != null ? args.getString(EXTRA_DEVICE_ID, "") : "";

        EnablePairingViewModelFactory viewModelFactory = new EnablePairingViewModelFactory(
                getActivity().getApplication(), deviceId);

        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(EnablePairingViewModel.Body.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_enable_pairing_ongoing, container, false);

        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.enablePairingContainer.loadLayoutDescription(R.xml.enable_pairing_states);
        setupOnClickListeners();
        setupPairingMode(getArguments() != null &&
                getArguments().getBoolean(EXTRA_PAIRING_MODE_ENABLED, false));
        initObservers();
    }

    private void setupOnClickListeners() {
        binding.pairingDoneButton.setOnClickListener(v -> {
            if (getActivity() != null) {
                viewModel.onDoneClicked();
                getActivity().onBackPressed();
            }
        });

        binding.pairingContinueButton.setOnClickListener(v -> {
            restartApp();
        });
    }

    private void restartApp() {
        if (getActivity() != null && getContext() != null) {
            Intent intent = getContext().getPackageManager()
                    .getLaunchIntentForPackage(getContext().getPackageName());
            if (intent != null) {
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                getActivity().finish();
            }
        }
    }


    private void setupEnabledStateAttributes() {
        binding.enablePairingStep2Description.setNumber(0);
        binding.enablePairingTitle.setText(getResources().getString(R.string.enable_pairing_ongoing_fast_pairing_uc));
        binding.enablePairingStep2Description.setParagraph(getResources().getString(R.string.enable_pairing_description_3));
        binding.enablePairingStep2Description.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        binding.enablePairingStep2Description.setLayoutGravity(Gravity.CENTER);
    }

    private void setupPairingMode(boolean isEnabled) {
        if (isEnabled) {
            Bundle args = getArguments();
            String name = args != null ? args.getString(EXTRA_PAIRING_MODE_ENABLED_DEVICE_NAME, "") : "";

            ((IToolbarConfigurator) getActivity()).setToolbarVisibility(true);
            ((TextView) binding.pairingEnabledRow.findViewById(R.id.pairingDeviceName)).setText(name);
            binding.enablePairingContainer.setState(R.id.enabled, 0, 0);
            setupEnabledStateAttributes();
        } else {
            ((IToolbarConfigurator) getActivity()).setToolbarVisibility(false);
            binding.enablePairingContainer.setState(R.id.ongoing, 0, 0);
        }
    }

    private void initObservers() {
        viewModel.outputs.getPairingState()
                .observe(this, enablePairingViewState ->
                {
                    switch (enablePairingViewState) {
                        case ONGOING:
                            ((IToolbarConfigurator) getActivity()).setToolbarVisibility(false);
                            //new Handler().postDelayed(() -> binding.enablePairingContainer.setState(R.id.ongoing,0 ,0), 5000);
                            break;
                        case DONE:
                            ((IToolbarConfigurator) getActivity()).setToolbarVisibility(false);
                            TransitionManager.beginDelayedTransition(binding.enablePairingContainer);
                            binding.enablePairingContainer.setState(R.id.done, 0, 0);
                            break;
                        case RETRY:
                            ((IToolbarConfigurator) getActivity()).setToolbarVisibility(false);
                            TransitionManager.beginDelayedTransition(binding.enablePairingContainer);
                            binding.enablePairingContainer.setState(R.id.retry, 0, 0);
                            break;
                    }
                });
        viewModel.outputs.isViewChanged()
                .observe(this, viewType -> {
                    if (viewType == ViewFlowController.ViewType.FINISH_ACTIVITY) {
                        getActivity().finish();
                    }
                });
    }
}

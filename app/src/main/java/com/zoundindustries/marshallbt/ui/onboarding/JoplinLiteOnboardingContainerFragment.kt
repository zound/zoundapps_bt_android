package com.zoundindustries.marshallbt.ui.onboarding

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.zoundindustries.marshallbt.R


private const val ONBOARDING_JOPLIN_LITE_PAGES_NUMBER = 3

/**
 * Fragment showing Joplin Lite onboarding.
 */
class JoplinLiteOnboardingContainerFragment : BaseOnboardingContainerFragment() {

    override val numberOfPages: Int
        get() = ONBOARDING_JOPLIN_LITE_PAGES_NUMBER

    override fun setupFragments(position: Int): Fragment {
        return when (position) {
            0 -> Fragment(R.layout.ota_slide1)
            1 -> Fragment(R.layout.ota_slide2)
            2 -> addBundleToFragment(JoplinOnboardingContinueFragment(R.layout.fragment_onboarding_page_joplin_continue), arguments)
            else -> {
                Fragment(R.layout.ota_slide1)
            }
        }
    }

    private fun addBundleToFragment(fragment: Fragment, bundle: Bundle?): Fragment {
        fragment.arguments = bundle
        return fragment
    }
}
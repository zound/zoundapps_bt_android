package com.zoundindustries.marshallbt.ui.devicesettings.tabbedViews.anc

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.lifecycle.ViewModelProviders

import com.zoundindustries.marshallbt.databinding.FragmentAncTabBinding
import com.zoundindustries.marshallbt.ui.BaseFragment
import com.zoundindustries.marshallbt.viewmodels.devicesettings.ANCViewModel
import com.zoundindustries.marshallbt.viewmodels.factories.devicesettings.AncViewModelFactory

import com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID

internal const val ANC_FORMAT = "%d0%%"
private const val ANC_MINIMAL_VALUE = 1

/**
 * Base class for tab view included in MainANCTabsFragment.
 */
abstract class ANCTabBaseFragment : BaseFragment() {

    protected var viewModel: ANCViewModel.Body? = null
    /**
     * Common binding to be used across every inheriting class. ANC views have common layout.
     */
    protected var binding: FragmentAncTabBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val arguments = arguments

        activity?.let { activity ->
            if (arguments != null) {
                viewModel = ViewModelProviders.of(
                        activity,
                        AncViewModelFactory(activity.application,
                                arguments.getString(EXTRA_DEVICE_ID, "")))
                        .get(ANCViewModel.Body::class.java)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentAncTabBinding.inflate(inflater)
        binding?.lifecycleOwner = this

        return binding?.root
    }


    protected fun scaleProgressFromView(progress: Int): Int {
        return progress + ANC_MINIMAL_VALUE
    }

    protected fun scaleProgressToView(value: Int): Int {
        return value - ANC_MINIMAL_VALUE
    }

    protected fun formatAncString(value: Int) = String.format(ANC_FORMAT, value)
}

/**
 * Helper class to simplify listener creation
 */
abstract class SimplifiedSeekBarChangeListener: SeekBar.OnSeekBarChangeListener {

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
        //nop
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        //nop
    }
}

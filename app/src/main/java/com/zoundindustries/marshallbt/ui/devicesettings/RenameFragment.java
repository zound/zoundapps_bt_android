package com.zoundindustries.marshallbt.ui.devicesettings;

import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentRenameBinding;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.viewmodels.devicesettings.RenameViewModel;
import com.zoundindustries.marshallbt.viewmodels.factories.devicesettings.RenameViewModelFactory;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

/**
 * Class used to show rename functionality
 */
public class RenameFragment extends BaseFragment {

    private RenameViewModel.Body viewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        RenameViewModelFactory viewModelFactory = new RenameViewModelFactory(
                getActivity().getApplication(),
                getArguments().getString(EXTRA_DEVICE_ID, ""));

        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(RenameViewModel.Body.class);

        initObservers();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        FragmentRenameBinding binding = FragmentRenameBinding.inflate(inflater);

        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolbar(R.string.device_settings_menu_item_rename_uc, View.VISIBLE, true);
    }

    private void initObservers() {
        viewModel.outputs.isViewChanged().observe(this, viewType -> {
            if (getActivity() != null) {
                switch (viewType) {
                    case BACK:
                        performBackPressedOrFinish();
                        break;
                    case HOME_SCREEN:
                        if (getActivity() != null) {
                            getActivity().finish();
                        }
                        break;
                }
            }
        });
    }
}

package com.zoundindustries.marshallbt.ui.adapters;

import android.animation.LayoutTransition;
import android.app.ActivityOptions;
import android.app.Application;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleEventObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.ListItemHomeDeviceBinding;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.ui.ViewFlowController;
import com.zoundindustries.marshallbt.ui.animation.BarAnimator;
import com.zoundindustries.marshallbt.ui.autopairing.EnablePairingFragment;
import com.zoundindustries.marshallbt.ui.devicesettings.DeviceSettingsActivity;
import com.zoundindustries.marshallbt.ui.homescreen.HomeActivity;
import com.zoundindustries.marshallbt.ui.onboarding.BaseOnboardingContainerFragment;
import com.zoundindustries.marshallbt.ui.player.sources.SourcesActivity;
import com.zoundindustries.marshallbt.utils.DebouncedSeekBarListener;
import com.zoundindustries.marshallbt.utils.SystemServicesUtils;
import com.zoundindustries.marshallbt.utils.ViewExtrasGlobals;
import com.zoundindustries.marshallbt.viewmodels.homescreen.DeviceItemViewModel;

import java.util.List;

/**
 * RecyclerView adapter keeping all devices in the system and updating the devices on the list.
 */
public class HomeDevicesAdapter extends RecyclerView.Adapter<HomeDevicesAdapter.ViewHolder> {

    private static final int EQ_BAR_ANIMATION_DURATION = 1100;

    private final LifecycleOwner lifeCycleOwner;
    private List<BaseDevice> speakerDevices;
    private DeviceItemViewModel.Body viewModel;

    /**
     * Constructor for HomeDevicesAdapter.
     *
     * @param devices        list of devices to be shown
     * @param lifecycleOwner to be registered for LiveData.
     */
    public HomeDevicesAdapter(@NonNull List<BaseDevice> devices,
                              @NonNull LifecycleOwner lifecycleOwner) {
        this.lifeCycleOwner = lifecycleOwner;
        this.speakerDevices = devices;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ListItemHomeDeviceBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.list_item_home_device, parent,
                false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ListItemHomeDeviceBinding binding = holder.binding;
        DeviceItemViewModel.Body model = binding.getViewModel();
        if (model == null) {
            // this vm was on purpose taken from its constructor, not from a factory
            DeviceItemViewModel.Body viewModel = new DeviceItemViewModel.Body((Application)
                    holder.binding.getRoot().getContext().getApplicationContext(),
                    speakerDevices.get(position));

            setupItemAnimation(binding);
            binding.setViewModel(viewModel);
            binding.setLifecycleOwner(lifeCycleOwner);
            lifeCycleOwner.getLifecycle().addObserver((LifecycleEventObserver) (source, event) -> {
                if (event == Lifecycle.Event.ON_DESTROY) {
                    binding.getViewModel().dispose();
                }
            });

            initObservers(holder);
        }
    }

    @Override
    public void onViewAttachedToWindow(ViewHolder holder) {
        super.onViewAttachedToWindow(holder);

        new BarAnimator(holder.binding.getRoot().getContext(),
                EQ_BAR_ANIMATION_DURATION, true,
                holder.leftAudioBar, holder.middleAudioBar, holder.rightAudioBar)
                .animate();
    }

    @Override
    public int getItemCount() {
        if (speakerDevices == null) {
            return 0;
        }
        return speakerDevices.size();
    }

    @Override
    public long getItemId(int position) {
        return speakerDevices.get(position).getDeviceInfo().getId().hashCode();
    }

    /**
     * Dispose adapter. Clear devices list.
     */
    public void dispose() {
        if (speakerDevices != null) {
            speakerDevices.clear();
        }
    }

    private void initObservers(ViewHolder holder) {
        //todo: Use this code when red dot indication is implemented
        /*holder.binding.getViewModel().outputs.getOtaFileMetaData().observe(lifeCycleOwner,
                });*/

        holder.binding.getViewModel().outputs
                .isNonConfigurableAuxSourceActive().observe(lifeCycleOwner, isActive -> {
            if (isActive) {
                holder.binding.linkEq.setAlpha(0.5f);
                holder.binding.linkEq.setClickable(false);
            } else {
                holder.binding.linkEq.setAlpha(1.0f);
                holder.binding.linkEq.setClickable(true);
            }
        });

        holder.binding.getViewModel().outputs.isViewChanged().observe(lifeCycleOwner, viewType -> {
            Context context = holder.itemView.getContext();

            switch (viewType) {
                case DEVICE_SETTINGS:
                    context.startActivity(DeviceSettingsActivity.create(context,
                            viewType.getArgs().getString(ViewExtrasGlobals.EXTRA_DEVICE_ID)));
                    break;
                case SOURCES:
                    context.startActivity(SourcesActivity.create(context,
                            viewType.getArgs().getString(ViewExtrasGlobals.EXTRA_DEVICE_ID)),
                            setAnimationForBundle(context));
                    break;
                case GLOBAL_SETTINGS_BLUETOOTH:
                    try {
                        context.startActivity(SystemServicesUtils.getBluetoothSettingsIntent());
                    } catch (ActivityNotFoundException | NullPointerException exception) {
                        context.startActivity(SystemServicesUtils.getGlobalSettingsIntent());
                    }
                    break;
                case ENABLE_PAIRING:
                    EnablePairingFragment enablePairingFragment = new EnablePairingFragment();
                    enablePairingFragment.setArguments(viewType.getArgs());
                    FragmentTransaction enablePairingTransaction = ((HomeActivity) context)
                            .getSupportFragmentManager()
                            .beginTransaction();
                    enablePairingTransaction.setCustomAnimations(R.anim.slide_in_right,
                            R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);

                    enablePairingTransaction.replace(R.id.home_fragment_container,
                            enablePairingFragment)
                            .addToBackStack(enablePairingFragment.getClass().getSimpleName())
                            .commit();
                    break;
                case ANC:
                    context.startActivity(DeviceSettingsActivity.create(context,
                            viewType.getArgs().getString(ViewExtrasGlobals.EXTRA_DEVICE_ID),
                            ViewFlowController.ViewType.ANC));
                    break;

                case EQ_EXTENDED:
                    context.startActivity(DeviceSettingsActivity.create(context,
                            viewType.getArgs().getString(ViewExtrasGlobals.EXTRA_DEVICE_ID),
                            ViewFlowController.ViewType.EQ_EXTENDED));
                    break;
                case ONBOARDING_GUIDE:
                    BaseOnboardingContainerFragment onboardingTabFragment = holder.binding.getViewModel().outputs.getOnboardingFragment();
                    if (onboardingTabFragment == null) {
                        return; // no onboarding to be shown
                    }

                    onboardingTabFragment.setArguments(viewType.getArgs());

                    FragmentTransaction onboardingTransaction = ((HomeActivity) context)
                            .getSupportFragmentManager()
                            .beginTransaction();
                    onboardingTransaction.setCustomAnimations(R.anim.slide_in_right,
                            R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);

                    onboardingTransaction.replace(R.id.home_fragment_container,
                            onboardingTabFragment)
                            .addToBackStack(onboardingTabFragment.getClass().getSimpleName())
                            .commit();
            }
        });
    }

    private void setupItemAnimation(ListItemHomeDeviceBinding binding) {
        if (binding.stateVisualization.getLayoutTransition() != null) {
            binding.stateVisualization.getLayoutTransition()
                    .enableTransitionType(LayoutTransition.CHANGING);
        }
        if (binding.batteryLevel.getLayoutTransition() != null) {
            binding.batteryLevel.getLayoutTransition()
                    .enableTransitionType(LayoutTransition.CHANGING);
        }
    }

    /**
     * This is custom animation for player activity (different from animation for whole app)
     * So we have put correct animation in bundle for new Activity, but we can only pass
     * here entry animation, in case of finishing (exit animation) it has to be done inside Activity
     * so in {@link SourcesActivity#onBackPressed()} is animation for exit.
     *
     * @param context used for creating ActivityOptions
     * @return bundle with animation
     */
    private Bundle setAnimationForBundle(Context context) {
        ActivityOptions activityOptions = ActivityOptions.makeCustomAnimation(context,
                R.anim.slide_in_bottom, R.anim.slide_out_top);
        return activityOptions.toBundle();
    }

    /**
     * Class that keeps view references for every row view object.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        public ListItemHomeDeviceBinding binding;

        private final SeekBar progressBar;
        private final View leftAudioBar;
        private final View middleAudioBar;
        private final View rightAudioBar;

        public ViewHolder(@NonNull final ListItemHomeDeviceBinding binding) {
            super(binding.deviceItemContainer);
            this.binding = binding;

            progressBar = binding.volumeSeekBar;
            leftAudioBar = binding.leftAudioBar;
            middleAudioBar = binding.middleAudioBar;
            rightAudioBar = binding.rightAudioBar;

            progressBar.setOnSeekBarChangeListener(new DebouncedSeekBarListener() {
                @Override
                public void runTask(@NonNull SeekBar seekBar, int progress, boolean fromUser) {
                    if (fromUser) {
                        binding.getViewModel().inputs.setVolume(progress);
                    }
                }
            });
        }
    }
}

package com.zoundindustries.marshallbt.ui.mainmenu.ozzyquickguide

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zoundindustries.marshallbt.R

/**
 * Fragment showing Quick Guide ANC for Ozzy headphones.
 */
class QuickGuideOzzyAncFragment : QuickGuideOzzyBaseFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_quick_guide_ozzy_anc, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar(R.string.main_menu_item_anc_button_uc, View.VISIBLE, true)
    }
}
package com.zoundindustries.marshallbt.ui.devicesettings.tabbedViews.eq;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zoundindustries.marshallbt.model.devicesettings.EQTabType;

import static com.zoundindustries.marshallbt.model.devicesettings.EQTabType.M2;

/**
 * Second tab of the tabbed equaliser - with dynamic values
 */
public class EQTabM2Fragment extends EQTabBaseFragment {

    @Override
    public EQTabType getTabType() {
        return M2;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null) {
            viewModel.outputs.notifyM2PresetValue().observe(getActivity(), presetPosition -> {
                if (presetPosition != null) {
                    binding.eqSpinner.setSelection(presetPosition);
                }
            });

            viewModel.outputs.notifyM2CustomPresetValues().observe(getActivity(), customValues ->
                    handler.postDelayed(
                            () -> binding.customEQBars.setEQValues(customValues, true),
                            EQ_BARS_ANIMATION_DELAY));

            setupOnValueChangedListener();
        }
    }

    private void setupOnValueChangedListener() {
        binding.customEQBars.setOnValueChangedListener(data -> viewModel.inputs.writeEQData(data));
    }
}

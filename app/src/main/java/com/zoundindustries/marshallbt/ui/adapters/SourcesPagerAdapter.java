package com.zoundindustries.marshallbt.ui.adapters;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.player.sources.SourceFeatures;
import com.zoundindustries.marshallbt.ui.player.sources.AuxSourceFragment;
import com.zoundindustries.marshallbt.ui.player.sources.BluetoothSourceFragment;
import com.zoundindustries.marshallbt.ui.player.sources.RcaSourceFragment;

import java.util.ArrayList;
import java.util.List;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

/**
 * Adapter class providing data to the sources screen.
 */
public class SourcesPagerAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> fragmentList;
    private List<String> titleList;
    private Context context;
    private String deviceId;

    /**
     * Constructor for {@link SourcesPagerAdapter}
     *
     * @param fragmentManager for super class to handle the fragment states
     * @param context         for string resources of titles
     * @param deviceId        to pass to the fragments
     */
    public SourcesPagerAdapter(@NonNull FragmentManager fragmentManager, @NonNull Context context,
                               @NonNull String deviceId) {
        super(fragmentManager);
        this.deviceId = deviceId;
        this.context = context;
        fragmentList = new ArrayList<>();
        titleList = new ArrayList<>();
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titleList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    /**
     * Set the fragments and titles depend on the source features
     *
     * @param sourceFeatures the availability of source features
     */
    public void setPages(SourceFeatures sourceFeatures, boolean isAuxConfigurable) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_DEVICE_ID, deviceId);

        if (sourceFeatures.isBluetoothSupported()) {
            createPage(new BluetoothSourceFragment(), bundle,
                    R.string.player_audio_source_bluetooth);
        }

        if (sourceFeatures.isRcaSupported()) {
            createPage(new RcaSourceFragment(), bundle, R.string.player_audio_source_rca);
        }

        if (sourceFeatures.isAuxSupported() && isAuxConfigurable) {
            createPage(new AuxSourceFragment(), bundle, R.string.player_audio_source_aux);
        }

        notifyDataSetChanged();
    }

    private void createPage(Fragment fragment, Bundle bundle, int titleResource) {
        fragment.setArguments(bundle);
        fragmentList.add(fragment);
        titleList.add(context.getResources().getString(titleResource));
    }

}

package com.zoundindustries.marshallbt.ui;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.ui.customviews.IToolbarConfigurator;

import io.reactivex.Observable;

/**
 * Class created to make it possible to have common method in activities that use
 * {@link IToolbarConfigurator}, this way we can eliminate boiler plate related to same
 * implementation of method by activities.
 */
public abstract class BaseToolbarActivity extends BaseActivity implements IToolbarConfigurator {

    //protected as those values are used by subtype activities
    protected Drawable toolbarDrawable;
    protected int toolbarBackgroundColorId;
    protected int toolbarTextColor;
    protected boolean isBackButtonEnabled = true;

    @Override
    public void setToolbarName(@NonNull String name) {
        //NOP
    }

    @Override
    public Observable<Boolean> getBackPressedObservable() {
        return null;
    }

    @Override
    public void setToolbarImageVisibility(int visibility) {
        //NOP
    }

    @Override
    public void changeToolbarColor(@NonNull ToolbarItemsColor toolbarItemsColor) {
        switch (toolbarItemsColor) {
            case BLACK:
                toolbarDrawable = getResources().getDrawable(R.drawable.back_button_black, null);
                toolbarBackgroundColorId = getResources().getColor(R.color.toolbar_secondary_background_color);
                toolbarTextColor = getResources().getColor(R.color.toolbar_primary_background_color);
                break;
            case WHITE:
                toolbarDrawable = getResources().getDrawable(R.drawable.back_button, null);
                toolbarBackgroundColorId = getResources().getColor(R.color.toolbar_primary_background_color);
                toolbarTextColor = getResources().getColor(R.color.toolbar_secondary_background_color);
                break;
        }
    }

    @Override
    public void setBackButtonEnabled(boolean isEnabled) {
        isBackButtonEnabled = isEnabled;
    }

    @Override
    public void setDrawerBackgroundColor(int color) {
        //NOP
    }
}

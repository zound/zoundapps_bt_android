package com.zoundindustries.marshallbt.ui.devicesettings.tabbedViews.anc

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat

import com.zoundindustries.marshallbt.R

/**
 * Fragment providing screen for ANC OFF state. No interaction is available for the user
 * in this state.
 */
class ANCTabOffFragment : ANCTabBaseFragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    private fun setupViews() {
        binding?.ancOnDescription?.text = resources
                .getString(R.string.anc_settings_screen_off_subtitle)
        context?.let { context ->
            binding?.ancOnValue?.setTextColor(ContextCompat
                    .getColor(context, R.color.secondaryText))
            binding?.ancOnValue?.text = resources
                    .getString(R.string.anc_settings_screen_off_uc)

            binding?.ancOnBar?.isEnabled = false
            binding?.ancOnBar?.thumb = ContextCompat
                    .getDrawable(context, R.drawable.slider_round_thumb_disabled)
            binding?.ancOnBar?.progress = 0
        }

    }
}

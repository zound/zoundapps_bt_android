package com.zoundindustries.marshallbt.ui.animation;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.zoundindustries.marshallbt.R;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Animator class for graphical sound visualization bars.
 */
public final class BarAnimator {
    private final Context context;
    private List<View> views;
    private int duration;
    private Random random;
    private boolean enableRandom;

    /**
     * Constructror for BarAnimator.
     *
     * @param view         to be animated.
     * @param duration     of the animation.
     * @param enableRandom enable random transformation values.
     */
    public BarAnimator(@NonNull Context context, int duration, boolean enableRandom, View... view) {
        this.context = context;
        this.views = Arrays.asList(view);
        this.duration = duration;
        this.random = new Random(999);
        this.enableRandom = enableRandom;
    }

    /**
     * Start view animation.
     */
    public void animate() {

        for (View view : views) {
            view.startAnimation(getAnimation());
        }
    }

    @NonNull
    private Animation getAnimation() {
        Animation scaleAnimation = AnimationUtils.loadAnimation(context, R.anim.eq_bars_scale);
        scaleAnimation.setInterpolator(new BounceInterpolator());
        scaleAnimation.setDuration(duration + (enableRandom ? random.nextInt(50) : 0));

        return scaleAnimation;
    }
}

package com.zoundindustries.marshallbt.ui;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.squareup.leakcanary.RefWatcher;
import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.ui.customviews.IToolbarConfigurator;
import com.zoundindustries.marshallbt.ui.customviews.IToolbarConfigurator.ToolbarItemsColor;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * Class for setup leakCanary to detect memory leaks in the app.
 * Every Fragment should use this class as a base.
 * May be extended with other common code.
 */
public class BaseFragment extends Fragment {

    private Disposable disposable;

    @Override
    public void onDestroy() {
        super.onDestroy();
        RefWatcher refWatcher = BluetoothApplication.getRefWatcher(getActivity());
        refWatcher.watch(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        setupToolbarBackButton();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    /**
     * Setup toolbar title and imageVisibility.
     * Used by Fragments to change toolbar behavior.
     *
     * @param titleId         title to be shown on toolbar.
     * @param imageVisibility sets toolbar back image visible or gone. This does not hide whole toolbar.
     */
    protected void setupToolbar(@StringRes int titleId,
                                int imageVisibility,
                                boolean isVisible) {
        IToolbarConfigurator configurator = getIToolbarConfigurator();
        if (configurator != null) {
            configurator.setToolbarName(getActivity()
                    .getResources()
                    .getString(titleId));
            configurator.setToolbarImageVisibility(imageVisibility);
            configurator.setToolbarVisibility(isVisible);
        }
    }

    /**
     * Change color of toolbar items.
     *
     * @param toolbarItemsColor color to be set.
     */
    protected void changeToolbarColor(@NonNull ToolbarItemsColor toolbarItemsColor) {
        IToolbarConfigurator configurator = getIToolbarConfigurator();
        if (configurator != null) {
            configurator.changeToolbarColor(toolbarItemsColor);
        }
    }

    /**
     * Set behavior on toolbar back button clicked.
     */
    protected void setupToolbarBackButton() {
        IToolbarConfigurator configurator = getIToolbarConfigurator();
        if (configurator != null) {
            Observable backPressObservable = configurator.getBackPressedObservable();
            if (backPressObservable != null) {
                disposable = configurator.getBackPressedObservable()
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .subscribe(aBoolean -> getActivity().onBackPressed());
            }
        }
    }

    /**
     * Toggles Activity back button behavior enabled and disabled.
     *
     * @param isEnabled toruns on and off back button behavior.
     */
    protected void setupBackButton(boolean isEnabled) {
        getIToolbarConfigurator().setBackButtonEnabled(isEnabled);
    }

    /**
     * checks for activity null check and instanceof
     * {@link IToolbarConfigurator}
     *
     * @return activity casted to {@link IToolbarConfigurator} in case when activity is instance of
     * this interface, otherwise null
     * <p>
     * responsible for reducing boiler plate in this fragment and its subtypes
     */
    @Nullable
    protected IToolbarConfigurator getIToolbarConfigurator() {
        Activity activity = getActivity();
        IToolbarConfigurator configurator = null;
        if (activity != null && activity instanceof IToolbarConfigurator) {
            configurator = (IToolbarConfigurator) activity;
        }
        return configurator;
    }

    /**
     * Start {@link Fragment} for container with arguments.
     *
     * @param ContainerViewId {@link Activity} container for fragment changes.
     * @param fragment        to be started.
     * @param arguments       to be passed to the new fragment.
     * @param addToBackStack  flag indicating if we should keep the new fragment on the stack.
     */
    protected void startFragment(int ContainerViewId,
                                 @NonNull BaseFragment fragment,
                                 @Nullable Bundle arguments,
                                 boolean addToBackStack) {
        if (arguments != null) {
            fragment.setArguments(arguments);
        }

        if (getFragmentManager() != null) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

            fragmentTransaction.setCustomAnimations(R.anim.slide_in_right,
                    R.anim.slide_out_left,
                    R.anim.slide_in_left,
                    R.anim.slide_out_right)
                    .replace(ContainerViewId, fragment);

            if (addToBackStack) {
                fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
            }

            fragmentTransaction.commit();
        }
    }

    protected void startSupportFragment(Fragment fragment, int container) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(
                        R.anim.slide_in_right,
                        R.anim.slide_out_left,
                        R.anim.slide_in_left,
                        R.anim.slide_out_right)
                .replace(container, fragment)
                .addToBackStack(fragment.getClass().getSimpleName())
                .commit();
    }

    /**
     * Try to use onBackPressed if {@link android.app.FragmentManager} is in proper state.
     * Finish the activity if the operation is not possible.
     */
    protected void performBackPressedOrFinish() {
        try {
            if (getActivity() != null) {
                getActivity().onBackPressed();
            }
        } catch (IllegalStateException exception) {
            getActivity().finish();
        }
    }
}

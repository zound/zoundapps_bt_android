package com.zoundindustries.marshallbt.ui.devicesettings.tabbedViews.eq;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentEqTabBinding;
import com.zoundindustries.marshallbt.model.devicesettings.EQTabType;
import com.zoundindustries.marshallbt.viewmodels.devicesettings.EQTabViewModel;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.ui.adapters.EQPresetAdapter;
import com.zoundindustries.marshallbt.viewmodels.factories.devicesettings.EQTabViewModelFactory;

import java.util.ArrayList;
import java.util.Arrays;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

public abstract class EQTabBaseFragment extends BaseFragment {

    protected static final int EQ_BARS_ANIMATION_DELAY = 100;

    protected Handler handler = new Handler();
    protected FragmentEqTabBinding binding;
    protected EQTabViewModel.Body viewModel;

    public abstract EQTabType getTabType();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();

        if (getActivity() != null) {
            if (arguments != null) {
                EQTabViewModelFactory viewModelFactory = new EQTabViewModelFactory(
                        getActivity().getApplication(),
                        arguments.getString(EXTRA_DEVICE_ID, ""));

                if (getParentFragment() != null) {
                    viewModel = ViewModelProviders.of(getParentFragment(), viewModelFactory)
                            .get(EQTabViewModel.Body.class);
                } else {
                    viewModel = ViewModelProviders.of(getActivity(), viewModelFactory)
                            .get(EQTabViewModel.Body.class);
                }
            } else {
                getActivity().finish();
            }


        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentEqTabBinding.inflate(inflater);
        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        EQPresetAdapter adapter = new EQPresetAdapter(getContext(),
                R.layout.list_eq_spinner_dropdown,
                R.id.name,
                viewModel.getPresets(),
                viewModel.outputs.isEQExtended());

        binding.eqSpinner.setAdapter(adapter);

        binding.customEQBars.setLabels(new ArrayList<>(Arrays
                .asList(getResources().getString(R.string.equaliser_screen_preset_bass),
                        getResources().getString(R.string.equaliser_screen_preset_low),
                        getResources().getString(R.string.equaliser_screen_preset_mid),
                        getResources().getString(R.string.equaliser_screen_preset_upper),
                        getResources().getString(R.string.equaliser_screen_preset_high))));

        binding.eqSpinner.setExtendedSpinnerListener((position, fromUser) ->
                viewModel.inputs.onEqPresetSelected(position, getTabType(), fromUser));
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }
}

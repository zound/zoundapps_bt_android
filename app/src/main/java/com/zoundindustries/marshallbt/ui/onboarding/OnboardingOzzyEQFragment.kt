package com.zoundindustries.marshallbt.ui.onboarding

import android.os.Bundle
import android.view.View
import androidx.constraintlayout.widget.ConstraintSet
import com.zoundindustries.marshallbt.R
import com.zoundindustries.marshallbt.ui.devicesettings.mbutton.MButtonSettingsFragment
import com.zoundindustries.marshallbt.utils.animateVisibilityChange
import kotlinx.android.synthetic.main.fragment_onboarding_base_tab.*


const val EXTRA_MBUTTON_SCREEN_ORIGIN = "extraMbuttonScreenOrigin"

enum class MbuttonScreenOrigin {
    SETTINGS,
    ONBOARDING,
    UNKNOWN;

    companion object {
        @JvmStatic
        fun from(value: Int?): MbuttonScreenOrigin {
            return when (value) {
                0 -> SETTINGS
                1 -> ONBOARDING
                else -> UNKNOWN
            }
        }
    }
}

/**
 * Fragment for displaying Equalizer onboarding screen.
 */
class OnboardingOzzyEQFragment : OnboardingTabBaseFragment() {
    override val headerId: Int
        get() = R.string.ozzy_onboarding_eq_title_uc
    override val descriptionId: Int
        get() = R.string.ozzy_onboarding_eq_subtitle
    override val imageResId: Int
        get() = R.drawable.carousel_3_image

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        setupOnClickListeners()
    }

    override fun onResume() {
        super.onResume()
        animateVisibilityChange(onboardingContinueButton, View.VISIBLE)
    }


    private fun setupViews() {
        val constraintSet = ConstraintSet()

        constraintSet.clone(onboardingBaseContainer)
        constraintSet.connect(
                R.id.onboardingSlideImage,
                ConstraintSet.TOP,
                R.id.onboardingSlideDescription,
                ConstraintSet.BOTTOM, 0)

        constraintSet.applyTo(onboardingBaseContainer)
    }

    private fun setupOnClickListeners() {
        onboardingContinueButton.setOnClickListener {

            val fragment = MButtonSettingsFragment()
            fragment.arguments = arguments
            arguments?.putInt(EXTRA_MBUTTON_SCREEN_ORIGIN, MbuttonScreenOrigin.ONBOARDING.ordinal)

            activity?.supportFragmentManager
                    ?.beginTransaction()
                    ?.setCustomAnimations(R.anim.slide_in_right,
                            R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                    ?.replace(R.id.home_fragment_container, fragment)
                    ?.addToBackStack(fragment.javaClass.simpleName)
                    ?.commit()
        }
    }
}
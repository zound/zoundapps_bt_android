package com.zoundindustries.marshallbt.ui.devicesettings;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatSeekBar;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.zoundindustries.marshallbt.utils.DebouncedSeekBarListener;

/**
 * Custom {@link AppCompatSeekBar} for vertical display.
 */
public class EQSeekBar extends AppCompatSeekBar {

    public static final int ROTATION_DEGREE = -90;
    public static final int HEIGHT_MULTIPLIER = -1;
    public static final int ANIMATION_DURATION_MS = 250;

    private DebouncedSeekBarListener changeListener;

    private int progress;

    public EQSeekBar(@NonNull Context context) {
        super(context);
    }

    public EQSeekBar(@NonNull Context context, @NonNull AttributeSet attrs) {
        super(context, attrs);
    }

    public EQSeekBar(@NonNull Context context, @NonNull AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setOnProgressChangedListener(@NonNull DebouncedSeekBarListener listener) {
        changeListener = listener;
    }

    /**
     * The seekbars in Android are displayed horizontally. To display one vertically, it needs to be
     * rotated. Rotating view in onDraw() changes the appearance of view, but does not change its
     * bounds. To correctly display bounds, we need to swap the view height and width. This way the
     * view is rotated and bounds are in the correct place.
     * <p>
     * This method needs to be run every time the view is re-drawn, for example - when setProgress is
     * invoked.
     *
     * @param width     the new width of the view.
     * @param height    the new height of the view.
     * @param oldWidth  the old width of the view.
     * @param oldHeight the old height of the view.
     */
    @Override
    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        super.onSizeChanged(height, width, oldHeight, oldWidth);
    }

    /**
     * Exchange the width- and heightMeasureSpec values to properly display the
     * {@link AppCompatSeekBar} vertically.
     *
     * @param widthMeasureSpec  horizontal space requirements as imposed by the parent.
     *                          The requirements are encoded with
     *                          {@link android.view.View.MeasureSpec}.
     * @param heightMeasureSpec vertical space requirements as imposed by the parent.
     *                          The requirements are encoded with
     *                          {@link android.view.View.MeasureSpec}.
     */
    @Override
    protected synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(heightMeasureSpec, widthMeasureSpec);
        setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
    }

    /**
     * Set the current progress to the given value.
     *
     * @param progress the new progress.
     */
    public synchronized void setEQProgress(int progress, boolean fromUser) {
        if (this.progress != progress) {
            this.progress = progress;
            changeListener.onProgressChanged(this, progress, fromUser);

            clearAnimation();
            if (!fromUser) {
                animateToPosition(progress).setDuration(ANIMATION_DURATION_MS).start();
            } else {
                super.setProgress(progress);
                onSizeChanged(getWidth(), getHeight(), 0, 0);
            }

            // Every time when we use the setProgress(), then the onDraw() method is triggered and
            // if we don't call the onSizeChanged(), it cannot draw the thumb on the whole Seekbar,
            // only on a little part of the bottom, because it is use the original horizontal sizes.
        }
    }

    /**
     * Modified to draw the {@link AppCompatSeekBar} vertically.
     *
     * @param canvas the canvas where the background will be drawn.
     */
    @Override
    protected synchronized void onDraw(Canvas canvas) {
        canvas.rotate(ROTATION_DEGREE);
        canvas.translate(HEIGHT_MULTIPLIER * getHeight(), 0);
        super.onDraw(canvas);
    }

    /**
     * Modified to handle the vertical touch events
     *
     * @param event the motion event.
     * @return true if the event is handled.
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!isEnabled()) {
            return false;
        }

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:
                int calculatedProgress = calculateVerticalProgress(event.getY());
                if (calculatedProgress != getProgress()) {
                    setEQProgress(calculatedProgress, true);
                }
                break;
            case MotionEvent.ACTION_UP:
                changeListener.onStopTrackingTouch(this);
                break;
            case MotionEvent.ACTION_CANCEL:
                break;
        }
        return true;
    }

    /**
     * Animate the {@link EQSeekBar} and change the progress value to the given position
     *
     * @param position that the new value of progress
     */
    public ValueAnimator animateToPosition(int position) {
        ValueAnimator animator = ValueAnimator.ofInt(getProgress(), position);
        animator.addUpdateListener(animation -> {
            setProgress((Integer) animation.getAnimatedValue());
            onSizeChanged(getWidth(), getHeight(), 0, 0);
        });
        return animator;
    }

    private int calculateVerticalProgress(float eventY) {
        int verticalProgress = getMax() - (int) (getMax() * eventY / getHeight());
        return Math.max(0, Math.min(100, verticalProgress));
    }
}

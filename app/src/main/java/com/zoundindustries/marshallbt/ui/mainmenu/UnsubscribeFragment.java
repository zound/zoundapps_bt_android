package com.zoundindustries.marshallbt.ui.mainmenu;


import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentUnsubscribeBinding;
import com.zoundindustries.marshallbt.di.Injectable;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.viewmodels.factories.mainmenu.MainMenuUnsubscribeViewModelFactory;
import com.zoundindustries.marshallbt.viewmodels.mainmenu.MainMenuUnsubscribeViewModel;

/**
 * Fragment used for handling cancelling subscription of user email from database
 */
public class UnsubscribeFragment extends BaseFragment implements Injectable {

    private MainMenuUnsubscribeViewModel.Body viewModel;

    public UnsubscribeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainMenuUnsubscribeViewModelFactory factory =
                new MainMenuUnsubscribeViewModelFactory(getActivity().getApplication());
        viewModel = ViewModelProviders.of(
                this, factory).get(MainMenuUnsubscribeViewModel.Body.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FragmentUnsubscribeBinding binding = FragmentUnsubscribeBinding.inflate(inflater);
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initObserver();
        setupToolbar(R.string.main_menu_empty_toolbar, View.GONE, true);
    }

    private void initObserver() {
        viewModel.outputs.isViewChanged().observe(this, viewType -> {
            if (getActivity() != null && viewType != null) {
                switch (viewType) {
                    case BACK:
                        performBackPressedOrFinish();
                        break;
                    case HOME_SCREEN:
                        getActivity().finish();
                }
            }
        });
    }
}

package com.zoundindustries.marshallbt.ui.devicesettings.tabbedViews.anc

import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import androidx.lifecycle.Observer

import com.zoundindustries.marshallbt.R

/**
 * Fragment providing screen for ANC Monitor settings allowing read and modify the values.
 */
class ANCTabMonitorFragment : ANCTabBaseFragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()

        viewModel?.notifyMonitorValue?.observe(viewLifecycleOwner, Observer { value ->
            value?.let {
                binding?.ancOnValue?.text = formatAncString(value)
                binding?.ancOnBar?.progress = scaleProgressToView(value)
            }
        })
    }

    private fun setupViews() {
        binding?.ancOnDescription?.text = resources
                .getString(R.string.anc_settings_screen_monitoring_subtitle)

        binding?.ancOnBar?.setOnSeekBarChangeListener(object : SimplifiedSeekBarChangeListener() {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    viewModel?.onMonitorValueChanged(scaleProgressFromView(progress))
                    binding?.ancOnValue?.text = formatAncString(scaleProgressFromView(progress))
                }
            }
        })
    }
}

package com.zoundindustries.marshallbt.ui.devicesettings.tabbedViews;

import androidx.fragment.app.Fragment;

/**
 * Object that keeps the tab fragment and its corresponding title.
 */
public class TabContainer {
    public Fragment fragment;
    public String title;

    public TabContainer(Fragment f, String s) {
        fragment = f;
        title = s;
    }
}

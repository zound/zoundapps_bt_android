package com.zoundindustries.marshallbt.ui.setup;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;

/**
 * A customized scrollview for detect when the bottom is reached.
 */
public class BottomReachedIndicatingScrollView extends ScrollView {

    /**
     * Used for communication with owner about current position of ScrollView
     */
    public interface IOnScrollListener {
        void onScrollPositionChanged(boolean isBottomEndReached);
    }

    private IOnScrollListener onScrollListener;

    public void setOnScrollListener(IOnScrollListener onScrollListener) {
        this.onScrollListener = onScrollListener;
    }

    public BottomReachedIndicatingScrollView(Context context) {
        super(context);
    }

    public BottomReachedIndicatingScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BottomReachedIndicatingScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * This is called in response to an internal scroll in this view (i.e., the
     * view scrolled its own contents). This is typically as a result of
     * {@link #scrollBy(int, int)} or {@link #scrollTo(int, int)} having been
     * called.
     * <p>
     * This detects if the view reached its bottom and sends a boolean value to
     * the {@link IOnScrollListener#onScrollPositionChanged(boolean)}
     *
     * @param currentHorizontalScroll Current horizontal scroll origin.
     * @param currentVerticalScroll   Current vertical scroll origin.
     * @param oldHorizontalScroll     Previous horizontal scroll origin.
     * @param oldVerticalScroll       Previous vertical scroll origin.
     */
    @Override
    protected void onScrollChanged(int currentHorizontalScroll, int currentVerticalScroll,
                                   int oldHorizontalScroll, int oldVerticalScroll) {
        super.onScrollChanged(currentHorizontalScroll, currentVerticalScroll,
                oldHorizontalScroll, oldVerticalScroll);
        if(onScrollListener != null) {
            onScrollListener.onScrollPositionChanged(isBottomEndReached());
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(onScrollListener != null) {
            onScrollListener.onScrollPositionChanged(isBottomEndReached());
        }
    }

    private boolean isBottomEndReached() {
        View view = getChildAt(getChildCount() - 1);
        int diff = view.getBottom() - (getHeight() + getScrollY());
        return diff <= 0;
    }
}

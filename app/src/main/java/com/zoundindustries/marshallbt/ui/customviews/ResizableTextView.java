package com.zoundindustries.marshallbt.ui.customviews;

import android.content.Context;
import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.TextViewCompat;
import android.util.AttributeSet;
import android.util.TypedValue;

import com.zoundindustries.marshallbt.R;

/**
 * Class created to resize header text based on string resource length(Differ for each translation)
 * Header should be displayed only in one line.
 */
public class ResizableTextView extends androidx.appcompat.widget.AppCompatTextView {

    public ResizableTextView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        Resources resources = context.getResources();
        setMaxLines(resources.getInteger(R.integer.header_text_number_of_lines));
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(this,
                resources.getInteger(R.integer.header_minimum_text_size),
                resources.getInteger(R.integer.header_maximum_text_size),
                resources.getInteger(R.integer.header_text_granularity),
                TypedValue.COMPLEX_UNIT_SP);
    }
}

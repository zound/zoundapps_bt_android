package com.zoundindustries.marshallbt.ui.mainmenu.ozzyquickguide

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zoundindustries.marshallbt.R
import com.zoundindustries.marshallbt.ui.BaseFragment
import com.zoundindustries.marshallbt.utils.animateVisibilityChange
import kotlinx.android.synthetic.main.fragment_quick_guide_ozzy.*

/**
 * Base abstract class to be extended by quick guide fragments for Ozzy headphones.
 * Keeps common layout for all quick guide views.
 */
abstract class QuickGuideOzzyBaseFragment : BaseFragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setScrollListener()
    }

    private fun setScrollListener() {
        ozzyQuickGuideDescriptionScroll.setOnScrollListener { isBottomEndReached ->
            ozzyQuickGuideGradientFooterView?.let { view ->
                animateVisibilityChange(view, if (isBottomEndReached) View.INVISIBLE else View.VISIBLE)
            }
        }
    }
}
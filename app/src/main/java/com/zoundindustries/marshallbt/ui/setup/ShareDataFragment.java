package com.zoundindustries.marshallbt.ui.setup;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentSetupShareDataBinding;
import com.zoundindustries.marshallbt.di.Injectable;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.ui.homescreen.HomeActivity;
import com.zoundindustries.marshallbt.viewmodels.setup.ShareDataViewModel;

import javax.inject.Inject;

/**
 * Fragment to show share data screen in app setup flow.
 */
public class ShareDataFragment extends BaseFragment implements Injectable {

    private ShareDataViewModel.Body viewModel;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private FragmentSetupShareDataBinding binding;

    public ShareDataFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this,
                viewModelFactory).get(ShareDataViewModel.Body.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentSetupShareDataBinding.inflate(inflater);
        binding.setLifecycleOwner(this);
        binding.setViewModel(viewModel);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.buttonConfirm.setText(viewModel.outputs.getConfirmationButtonText());
        initObservers();
        setupToolbar(R.string.main_menu_empty_toolbar, View.GONE, true);
    }

    @Override
    public void onStop() {
        super.onStop();
        viewModel.inputs.resetViewTransitionVariables();
    }

    private void initObservers() {
        viewModel.outputs.isViewChanged().observe(this, viewType -> {
            switch (viewType) {
                case HOME_SCREEN:
                    goToHomeScreen();
                    break;
                case SAVE_DATA_SCREEN:
                    startFragment(R.id.setup_fragment_container,
                            new SaveDataFragment(), null, false);
                    break;
            }
        });
    }

    private void goToHomeScreen() {
        switch (viewModel.outputs.getLayoutType()) {
            case INITIAL_SHARE_DATA:
                startHomeActivity();
                break;
            case MAIN_MENU_SHARE_DATA:
                if (getActivity() != null && !getActivity().isFinishing()) {
                    getActivity().finish();
                }
                break;
        }
    }

    private void startHomeActivity() {
        Context context = getContext();
        if (context != null) {
            startActivity(HomeActivity.create(context));
        }
        if (getActivity() != null && !getActivity().isFinishing()) {
            getActivity().finish();
        }
    }
}

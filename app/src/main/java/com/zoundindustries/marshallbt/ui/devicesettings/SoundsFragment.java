package com.zoundindustries.marshallbt.ui.devicesettings;

import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentSoundsBinding;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.viewmodels.devicesettings.SoundsViewModel;
import com.zoundindustries.marshallbt.viewmodels.factories.devicesettings.SoundsViewModelFactory;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

public class SoundsFragment extends BaseFragment {

    private SoundsViewModel.Body viewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SoundsViewModelFactory viewModelFactory = new SoundsViewModelFactory(
                getActivity().getApplication(),
                getArguments().getString(EXTRA_DEVICE_ID, ""));

        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(SoundsViewModel.Body.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        FragmentSoundsBinding binding = FragmentSoundsBinding.inflate(inflater);

        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initLiveData();
        setupToolbar(R.string.device_settings_menu_item_sounds_uc, View.VISIBLE, true);
    }

    private void initLiveData() {
        viewModel.isViewChanged().observe(this, viewType -> {
            switch (viewType) {
                case BACK:
                    performBackPressedOrFinish();
                    break;
                case HOME_SCREEN:
                    if (getActivity() != null) {
                        getActivity().finish();
                    }
                    break;
            }
        });
    }
}

package com.zoundindustries.marshallbt.ui.player.sources;

import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.zoundindustries.marshallbt.databinding.FragmentSourceBinding;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.utils.DebouncedSeekBarListener;
import com.zoundindustries.marshallbt.viewmodels.factories.player.sources.RcaSourceViewModelFactory;
import com.zoundindustries.marshallbt.viewmodels.player.sources.RcaSourceViewModel;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

/**
 * Class to display RCA source screen
 */

public class RcaSourceFragment extends BaseFragment {


    private RcaSourceViewModel.Body viewModel;
    private FragmentSourceBinding binding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() == null || getActivity() == null) return;

        RcaSourceViewModelFactory viewModelFactory =
                new RcaSourceViewModelFactory(getActivity().getApplication(),
                        getArguments().getString(EXTRA_DEVICE_ID));

        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(RcaSourceViewModel.Body.class);

        initObservers();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = FragmentSourceBinding.inflate(inflater);
        binding.setLifecycleOwner(this);
        binding.setViewModel(viewModel);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.volumeSeekBar.setOnSeekBarChangeListener(new DebouncedSeekBarListener() {
            @Override
            public void runTask(@NonNull SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    viewModel.inputs.setVolume(progress);
                }
            }
        });
    }

    private void initObservers() {
        viewModel.outputs.isViewChanged().observe(this, viewType -> {
            switch (viewType) {
                case HOME_SCREEN:
                    if (getActivity() != null) {
                        getActivity().finish();
                    }
            }
        });
    }
}

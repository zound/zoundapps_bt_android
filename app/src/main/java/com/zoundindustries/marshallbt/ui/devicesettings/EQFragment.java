package com.zoundindustries.marshallbt.ui.devicesettings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentEqBinding;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.ui.adapters.EQPresetAdapter;
import com.zoundindustries.marshallbt.utils.EqPresetType;
import com.zoundindustries.marshallbt.viewmodels.devicesettings.EQViewModel;
import com.zoundindustries.marshallbt.viewmodels.factories.devicesettings.EQViewModelFactory;

import java.util.List;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

/**
 * Class used to show device settings equaliser
 */
public class EQFragment extends BaseFragment {

    private EQViewModel.Body viewModel;
    private FragmentEqBinding binding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // todo check if Application or string != null
        EQViewModelFactory viewModelFactory = new EQViewModelFactory(
                getActivity().getApplication(),
                getArguments().getString(EXTRA_DEVICE_ID,
                        ""));

        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(EQViewModel.Body.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentEqBinding.inflate(inflater);
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.eqSeekbarGroup.setOnProgressListener((type, value, fromUser) -> {
            if (fromUser)
                viewModel.inputs.eqValueUpdatedByUser(type, value);
        });

        binding.eqSeekbarGroup.init();
        initObservers();
        setupToolbar(R.string.device_settings_menu_item_equaliser_uc, View.VISIBLE, true);
    }

    private void setupSpinnerAdapter(List<EqPresetType> presetTypeList) {
        EQPresetAdapter adapter = new EQPresetAdapter(getContext(),
                R.layout.list_eq_spinner_dropdown,
                R.id.name,
                presetTypeList,
                viewModel.outputs.isEQExtended());

        binding.spinner.setAdapter(adapter);

        // to prevent spinner selection invoked on opening the screen
        binding.spinner.setSelection(0, false);
        binding.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                viewModel.inputs.onSpinnerItemSelected(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //NOP
            }
        });
    }

    private void initObservers() {
        viewModel.outputs.getEqualizerPresetTypeList().observe(this, this::setupSpinnerAdapter);

        viewModel.outputs.getEqPreset().observe(this, preset -> {
            if (preset != null) {
                binding.eqSeekbarGroup.setPreset(preset.getEqData());
            }
        });

        viewModel.outputs.getSpinnerSelection().observe(this, selection -> {
            if (selection != null)
                binding.spinner.setSelection(selection);
        });

        viewModel.outputs.isViewChanged().observe(this, viewType -> {
            if (getActivity() != null) {
                switch (viewType) {
                    case BACK:
                        performBackPressedOrFinish();
                        break;
                    case HOME_SCREEN:
                        if (getActivity() != null) {
                            getActivity().finish();
                        }
                        break;
                }
            }
        });
    }
}

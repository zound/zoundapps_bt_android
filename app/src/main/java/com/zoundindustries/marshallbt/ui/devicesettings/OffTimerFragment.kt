package com.zoundindustries.marshallbt.ui.devicesettings

import android.animation.ObjectAnimator
import android.os.Bundle
import android.transition.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.zoundindustries.marshallbt.R
import com.zoundindustries.marshallbt.databinding.FragmentTimerReadyBinding
import com.zoundindustries.marshallbt.ui.BaseFragment
import com.zoundindustries.marshallbt.ui.ViewFlowController
import com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID
import com.zoundindustries.marshallbt.utils.getBlinkAnimation
import com.zoundindustries.marshallbt.viewmodels.devicesettings.*
import com.zoundindustries.marshallbt.viewmodels.factories.devicesettings.OffTimerViewModelFactory

const val STRING_FORMAT_LEADING_ZERO = "%02d"

/**
 * Data class keeping the timer tuple
 */
data class TimerValues(val hour: Int, val minute: Int) {
    fun toInt(): Int {
        return hour * 60 + minute
    }
}

/**
 * Class displaying the timer off fragment
 */
class OffTimerFragment : BaseFragment() {

    lateinit var binding: FragmentTimerReadyBinding

    var viewModel: OffTimerViewModel.Body? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        arguments?.getString(EXTRA_DEVICE_ID, "")?.let { deviceId ->
            activity?.application?.let { app ->
                val factory = OffTimerViewModelFactory(app, deviceId)
                viewModel = ViewModelProviders.of(this, factory)
                        .get(OffTimerViewModel.Body::class.java)
            }
        }

        binding = FragmentTimerReadyBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.timerContainer.loadLayoutDescription(R.xml.timer_off_states)

        return binding.root
    }

    private var viewTimerState = TimerState.STOPPED

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar(R.string.device_settings_menu_item_auto_off_timer_uc, View.VISIBLE, true)

        binding.timerContainer.setState(R.id.timer_off_loading, 0, 0)
        setupTimePickers()

        setupObservers()
    }

    override fun onResume() {
        super.onResume()
        changeButtonVisibility(binding.minutePicker.value == 0 &&
                binding.hourPicker.value == 0)
    }

    private fun setupObservers() {
        viewModel?.outputs?.notifyTimerValue?.observe(viewLifecycleOwner, Observer<Int> { value ->
            setTimerValue(value)
        })

        viewModel?.outputs?.notifyTimerState?.observe(viewLifecycleOwner, Observer { timerState ->
            updateViews(timerState)
            viewTimerState = timerState
        })


        viewModel?.outputs?.notifyViewChanged?.observe(viewLifecycleOwner, Observer { viewType ->
            context?.let {
                if (viewType == ViewFlowController.ViewType.BACK) {
                    performBackPressedOrFinish()
                }
            }
        })

    }

    private fun setTimerValue(value: Int?) {
        value?.let {
            val (hour, minute) = getTimerValues(value)
            binding.minutesText.text = String.format(STRING_FORMAT_LEADING_ZERO, minute)
            binding.hourText.text = String.format(STRING_FORMAT_LEADING_ZERO, hour)
        }
    }

    private fun getTimerValues(value: Int): TimerValues {
        return TimerValues((value / 60), value.rem(60))
    }

    private fun setupTimePickers() {
        val hourValues = IntProgression.fromClosedRange(0, TIMER_HOUR_MAX_RANGE, 1)
                .map { String.format(STRING_FORMAT_LEADING_ZERO, it) }
                .toTypedArray()

        binding.hourPicker.maxValue = hourValues.size - 1
        binding.hourPicker.minValue = 0
        binding.hourPicker.displayedValues = hourValues
        binding.hourPicker.setOnValueChangedListener { picker, oldVal, newVal ->
            changeButtonVisibility(newVal == 0 && binding.minutePicker.value == 0)
        }

        val minuteValues = IntProgression.fromClosedRange(0, TIMER_MINUTE_MAX_RANGE, TIMER_MINUTE_STEP)
                .map { String.format(STRING_FORMAT_LEADING_ZERO, it) }
                .toTypedArray()

        binding.minutePicker.displayedValues = minuteValues
        binding.minutePicker.maxValue = minuteValues.size - 1
        binding.minutePicker.minValue = 0
        binding.minutePicker.setOnValueChangedListener { picker, oldVal, newVal ->
            changeButtonVisibility(newVal == 0 && binding.hourPicker.value == 0)
        }
    }

    private fun changeButtonVisibility(shouldGreyOut: Boolean) {
        val alphaValue: Float

        if (shouldGreyOut) {
            alphaValue = 0.4f
            binding.timerButton.isClickable = false
        } else {
            alphaValue = 1f
            binding.timerButton.isClickable = true
        }

        val anim = ObjectAnimator.ofFloat(binding.timerButton, "alpha", alphaValue)
        anim.duration = 200
        anim.start()
    }

    override fun onStart() {
        super.onStart()
        binding.timerButton.setOnClickListener {
            if (viewTimerState == TimerState.STOPPED) {
                viewModel?.startTimer(getTimeFromPickers())
            } else {
                viewModel?.stopTimer()
            }
        }
    }

    private fun getTimeFromPickers(): Int {
        return TimerValues(binding.hourPicker.value,
                (binding.minutePicker.value * TIMER_MINUTE_STEP))
                .toInt()
    }

    private fun updateViews(timerState: TimerState) {
        TransitionManager.beginDelayedTransition(binding.timerContainer)
        when (timerState) {
            TimerState.STOPPED -> {
                changeButtonVisibility(binding.minutePicker.value == 0 &&
                        binding.hourPicker.value == 0)
                binding.blinkingDots.clearAnimation()
                binding.timerContainer.setState(R.id.timer_off_ready, 0, 0)
                binding.timerButton.setText(R.string.auto_off_timer_button_start_uc)
            }
            TimerState.RUNNING -> {
                changeButtonVisibility(false)
                binding.blinkingDots.startAnimation(getBlinkAnimation())
                binding.timerContainer.setState(R.id.timer_off_running, 0, 0)
                binding.timerButton.setText(R.string.auto_off_timer_button_stop_uc)
            }
        }
    }
}
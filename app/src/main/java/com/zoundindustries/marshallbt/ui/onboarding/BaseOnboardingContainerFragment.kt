package com.zoundindustries.marshallbt.ui.onboarding

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.zoundindustries.marshallbt.R
import com.zoundindustries.marshallbt.ui.BaseFragment
import com.zoundindustries.marshallbt.utils.animateVisibilityChange
import kotlinx.android.synthetic.main.fragment_onboarding_tab.*

private const val BUTTON_VISIBILITY_SET_DELAY = 100L

/**
 * Base class for fragment wrapping ViewPager for onboarding.
 * Allows to setup number of pages to be displayed.
 */
abstract class BaseOnboardingContainerFragment : BaseFragment() {

    /**
     * Number of pages to be displayed for onboarding.
     * There will be CONTINUE button shown on the last one
     */
    abstract val numberOfPages: Int

    /**
     * Return fragment for specific position.
     * Fragemnts need to be returned base on the position parameter.
     */
    abstract fun setupFragments(position: Int): Fragment

    private val handler = Handler()

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_onboarding_tab, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onboardingPager?.adapter = ScreenSlidePagerAdapter(this)
        onboardingPager?.offscreenPageLimit = numberOfPages
        onboardingPager?.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                onboardingDots.setImageResource(when (position) {
                    //TODO This right here breaks the generic approach of onboarding.
                    // To be generic, we need to draw [numberOfPages] of dots
                    0 -> R.drawable.ota_slide_dot1
                    1 -> R.drawable.ota_slide_dot2
                    2 -> R.drawable.ota_slide_dot3
                    else -> R.drawable.ota_slide_dot1
                })
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels)
                if (position == numberOfPages - 1) {
                    handler.postDelayed({
                        onboardingPager.isUserInputEnabled = false
                        animateVisibilityChange(onboardingDots, View.INVISIBLE)
                    }, BUTTON_VISIBILITY_SET_DELAY)
                }
            }
        })
    }

    override fun onDestroyOptionsMenu() {
        super.onDestroyOptionsMenu()
        handler.removeCallbacksAndMessages(null)
    }

    /**
     * A simple pager adapter that represents 3 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private inner class ScreenSlidePagerAdapter(host: Fragment) : FragmentStateAdapter(host) {
        override fun getItemCount(): Int = numberOfPages

        override fun createFragment(position: Int): Fragment = setupFragments(position)
    }
}
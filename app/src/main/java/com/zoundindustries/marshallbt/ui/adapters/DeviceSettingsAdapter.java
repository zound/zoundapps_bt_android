package com.zoundindustries.marshallbt.ui.adapters;

import androidx.lifecycle.LifecycleOwner;

import android.content.Context;

import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.ListItemDeviceSettingsBinding;
import com.zoundindustries.marshallbt.model.device.BaseDevice.ConnectionState;
import com.zoundindustries.marshallbt.model.devicesettings.DeviceSettingsItem;
import com.zoundindustries.marshallbt.viewmodels.devicesettings.DeviceSettingsListViewModel;

import java.util.List;

/**
 * Adapter class providing data to the settings list.
 * All row UI customizations and presentation should be done here.
 */
public class DeviceSettingsAdapter extends ArrayAdapter<DeviceSettingsItem> {

    private final Context context;
    private final DeviceSettingsListViewModel.Body deviceSettingsViewModel;
    private final List<DeviceSettingsItem> settingsList;

    private boolean isDeviceConnected;
    private boolean isDeviceTwsCoupled;
    private int updateIndicatorVisibility = View.GONE;
    private Boolean isNonConfigurableAuxSourceActive = false;

    /**
     * Constructor for DeviceSettingsAdapter with all required parameters.
     *
     * @param context                 used for inflating views and set {@link LifecycleOwner}
     * @param deviceSettingsViewModel used for interaction with data layers.
     * @param resource                parent layout for the settings list.
     */
    public DeviceSettingsAdapter(@NonNull Context context,
                                 @NonNull DeviceSettingsListViewModel.Body deviceSettingsViewModel,
                                 int resource) {

        super(context, resource);

        this.context = context;
        this.deviceSettingsViewModel = deviceSettingsViewModel;
        this.settingsList = deviceSettingsViewModel.getSettingsList();

        initObservers();
    }

    @Override
    public int getCount() {
        return settingsList.size();
    }

    @Nullable
    @Override
    public DeviceSettingsItem getItem(int position) {
        return settingsList.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ViewHolder viewHolder;

        if (convertView == null) {
            ListItemDeviceSettingsBinding binding = DataBindingUtil
                    .inflate(LayoutInflater.from(parent.getContext()),
                            R.layout.list_item_device_settings,
                            parent, false);

            convertView = binding.getRoot();
            viewHolder = new ViewHolder(binding);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        setupSettingsState(position, viewHolder);

        return convertView;
    }

    private void initObservers() {
        deviceSettingsViewModel.outputs.isConnected()
                .observe((LifecycleOwner) context, this::onConnectionChanged);

        deviceSettingsViewModel.outputs.isCouplingStateAvailable()
                .observe((LifecycleOwner) context, this::onCouplingReadyChanged);

        deviceSettingsViewModel.outputs.isUpdateAvailable()
                .observe((LifecycleOwner) context, this::onUpdateAvailabilityChanged);

        deviceSettingsViewModel.outputs.isNonConfigurableAuxSourceActive()
                .observe((LifecycleOwner) context, isActive -> {
                    isNonConfigurableAuxSourceActive = isActive;
                    notifyDataSetChanged();
                });
    }

    private void setupSettingsState(int position, ViewHolder viewHolder) {
        viewHolder.updateIndicator.setVisibility(View.GONE);
        DeviceSettingsItem.SettingType type = settingsList.get(position).getType();

        boolean isTypeCouple = type == DeviceSettingsItem.SettingType.COUPLE;
        boolean isTypeForget = type == DeviceSettingsItem.SettingType.FORGET;
        boolean isConnected = isTypeForget || isDeviceConnected;
        boolean isGreyedOutOnAuxSourceActive =
                isNonConfigurableAuxSourceActive &&
                        (type == DeviceSettingsItem.SettingType.M_BUTTON ||
                                type == DeviceSettingsItem.SettingType.EQ_EXTENDED);

        //Show coupling availability only when we have all needed info
        if (!deviceSettingsViewModel.isPaired()) {
            viewHolder.name.setAlpha(0.5f);
            viewHolder.binding.deviceSettingsItem.setClickable(false);
        } else if (isDeviceTwsCoupled) {
            viewHolder.name.setAlpha(isTypeCouple || isTypeForget ? 1.0f : 0.5f);
            viewHolder.binding.deviceSettingsItem.setClickable(isTypeCouple || isTypeForget);
        } else if (isGreyedOutOnAuxSourceActive) {
            viewHolder.name.setAlpha(0.5f);
            viewHolder.binding.deviceSettingsItem.setClickable(false);
        } else {
            viewHolder.name.setAlpha(isConnected ? 1.0f : 0.5f);
            viewHolder.binding.deviceSettingsItem.setClickable(isConnected);
        }

        if (deviceSettingsViewModel.isPaired() && isConnected || (isDeviceTwsCoupled && isTypeCouple)) {
            // options should be reachable only when device is connected (except forget speaker)
            if (!isGreyedOutOnAuxSourceActive) {
                viewHolder.binding.deviceSettingsItem.setOnClickListener(v ->
                        deviceSettingsViewModel
                                .inputs
                                .onSettingsItemClicked(settingsList.get(position).getType()));
            }
        }

        viewHolder.name.setText(settingsList.get(position).getName());

        if (type == DeviceSettingsItem.SettingType.ABOUT) {
            viewHolder.updateIndicator.setVisibility(updateIndicatorVisibility);
        }
    }

    private void onConnectionChanged(ConnectionState state) {
        isDeviceConnected = state == ConnectionState.CONNECTED;
        isDeviceTwsCoupled = state == ConnectionState.TWS_CONNECTED;
        notifyDataSetChanged();
    }

    private void onCouplingReadyChanged(boolean isReady) {
        notifyDataSetChanged();
    }

    private void onUpdateAvailabilityChanged(Boolean isAvailable) {
        if (isAvailable != null) {
            updateIndicatorVisibility = isAvailable ? View.VISIBLE : View.GONE;
        }
    }

    public static class ViewHolder {
        ListItemDeviceSettingsBinding binding;

        public TextView name;
        TextView updateIndicator;

        ViewHolder(@NonNull ListItemDeviceSettingsBinding binding) {
            this.binding = binding;
            name = binding.deviceSettingsRowTitle;
            updateIndicator = binding.deviceSettingsUpdateIndicator;
        }

    }
}

package com.zoundindustries.marshallbt.ui.onboarding

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.zoundindustries.marshallbt.R
import com.zoundindustries.marshallbt.ui.BaseFragment
import com.zoundindustries.marshallbt.ui.customviews.IToolbarConfigurator
import kotlinx.android.synthetic.main.fragment_onboarding_base_tab.*


private const val ENTER_ANIMATION_DELAY = 250L
private const val IMAGE_ANIMATION_DELAY = 150L

/**
 * Base Fragment defining common view for all onboarding screens.
 * It should be used for any onboarding guide screens placed in onboarding ViewPager.
 */
abstract class OnboardingTabBaseFragment : BaseFragment() {

    /**
     * String resource ID for quick guide header.
     */
    abstract val headerId: Int

    /**
     * String resource ID for quick guide description.
     */
    abstract val descriptionId: Int

    /**
     * String resource ID for quick guide image drawable.
     */
    abstract val imageResId: Int

    private val handler = Handler()

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_onboarding_base_tab, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupCustomToolBar()
        setupView()
    }

    override fun onDestroy() {
        handler.removeCallbacksAndMessages(null)
        //TODO: REMOVE WHEN ASSETS ARE GREY
        context?.let { context ->
            (activity as IToolbarConfigurator)
                    .setDrawerBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
        }
        super.onDestroy()
    }

    private fun setupCustomToolBar() {
        context?.let { context ->
            setupToolbar(headerId, View.GONE, false)
            (activity as IToolbarConfigurator)
                    .setDrawerBackgroundColor(ContextCompat.getColor(context, R.color.black))
        }
    }

    private fun setupView() {
        onboardingSlideTitle.text = resources.getString(headerId)
        onboardingSlideDescription.text = resources.getString(descriptionId)

        //TODO: REMOVE WHEN ASSETS ARE LOWER RESOLUTION. WORKAROUND FOR LAGGY IMAGE LOADING
        handler.postDelayed({
            onboardingSlideImage.setImageDrawable(context?.let { ContextCompat.getDrawable(it, imageResId) })
            handler.postDelayed({
                onboardingSlideImage.visibility = View.VISIBLE
            }, IMAGE_ANIMATION_DELAY)
        }, ENTER_ANIMATION_DELAY)
    }
}
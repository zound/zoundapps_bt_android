package com.zoundindustries.marshallbt.ui.devicesettings.mbutton

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.zoundindustries.marshallbt.R
import com.zoundindustries.marshallbt.databinding.FragmentMButtonSettingsLoadingBinding
import com.zoundindustries.marshallbt.ui.BaseFragment
import com.zoundindustries.marshallbt.ui.ViewFlowController
import com.zoundindustries.marshallbt.ui.adapters.MButtonSettingsAdapter
import com.zoundindustries.marshallbt.ui.customviews.IToolbarConfigurator
import com.zoundindustries.marshallbt.ui.onboarding.AllFinishedFragment
import com.zoundindustries.marshallbt.ui.onboarding.EXTRA_MBUTTON_SCREEN_ORIGIN
import com.zoundindustries.marshallbt.ui.onboarding.MbuttonScreenOrigin
import com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID
import com.zoundindustries.marshallbt.viewmodels.devicesettings.MButtonViewModel
import com.zoundindustries.marshallbt.viewmodels.factories.devicesettings.MButtonViewModelFactory

/**
 * Class used to allow user to read and write M-Button settings.
 */
class MButtonSettingsFragment : BaseFragment() {

    private var deviceId: String? = null
    private var mButtonSettingsAdapter: MButtonSettingsAdapter? = null
    private var viewModel: MButtonViewModel.Body? = null
    private var binding: FragmentMButtonSettingsLoadingBinding? = null
    private var screenOrigin: MbuttonScreenOrigin? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments == null || activity == null) return

        deviceId = arguments?.getString(EXTRA_DEVICE_ID, "")

        val screenOriginOrdinal = arguments?.getInt(EXTRA_MBUTTON_SCREEN_ORIGIN, -1)
        screenOrigin = MbuttonScreenOrigin.from(screenOriginOrdinal)

        viewModel = ViewModelProviders.of(
                this,
                MButtonViewModelFactory(activity?.application,
                        deviceId)).get(MButtonViewModel.Body::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentMButtonSettingsLoadingBinding.inflate(inflater)
        binding?.lifecycleOwner = this
        binding?.mButtonSettingsContainer?.loadLayoutDescription(R.xml.m_button_states)

        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel?.let { vm ->
            mButtonSettingsAdapter = MButtonSettingsAdapter(vm.settingsItemList, vm)
        }
        binding?.mButtonSettingsList?.adapter = mButtonSettingsAdapter
        binding?.mButtonSettingsList?.layoutManager = LinearLayoutManager(activity)
        binding?.mButtonSettingsList?.isNestedScrollingEnabled = false

        setupCustomToolBar()
        initObservers()
    }


    private fun initObservers() {
        binding?.mButtonNextButton?.setOnClickListener {
            val fragment = AllFinishedFragment()
            fragment.arguments = arguments
            startSupportFragment(fragment, R.id.home_fragment_container)
        }

        viewModel?.outputs?.notifyMButtonValue?.observe(viewLifecycleOwner, Observer { index ->
            binding?.mButtonSettingsContainer?.setState(R.id.m_button_connected, 0, 0)

            screenOrigin?.let {
                if (screenOrigin === MbuttonScreenOrigin.ONBOARDING) {
                    binding?.mButtonNextButton?.visibility = View.VISIBLE
                }
            }

            if (mButtonSettingsAdapter?.selectedPosition != index) {
                mButtonSettingsAdapter?.selectedPosition = index
            }

        })

        if (screenOrigin === MbuttonScreenOrigin.SETTINGS) {
            viewModel?.outputs?.notifyIsAuxSourceActive?.observe(viewLifecycleOwner, Observer { isActive ->
                if (isActive)
                    performBackPressedOrFinish()
            })
        }

        viewModel?.outputs?.viewChanged?.observe(viewLifecycleOwner, Observer { viewType ->
            if (activity != null) {
                when (viewType) {
                    ViewFlowController.ViewType.BACK -> performBackPressedOrFinish()
                    ViewFlowController.ViewType.HOME_SCREEN -> if (activity != null) {
                        activity?.finish()
                    }
                    ViewFlowController.ViewType.M_BUTTON_GVA_DETAIL ->
                        if (screenOrigin != null && screenOrigin === MbuttonScreenOrigin.ONBOARDING) {
                            val fragment = GvaDetailFragment()
                            fragment.arguments = arguments
                            startSupportFragment(fragment, R.id.home_fragment_container)
                        } else {
                            startFragment(R.id.device_settings_fragment_container,
                                    GvaDetailFragment(),
                                    null,
                                    true)
                        }
                }
            }
        })
    }

    private fun setupCustomToolBar() {
        context?.let { context ->
            setupToolbar(R.string.device_settings_menu_item_m_button_uc, View.VISIBLE, screenOrigin == MbuttonScreenOrigin.SETTINGS)

            //TODO: REMOVE WHEN ASSETS ARE GREY
            (activity as IToolbarConfigurator)
                    .setDrawerBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
        }
    }
}

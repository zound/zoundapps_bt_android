package com.zoundindustries.marshallbt.ui.devicesettings;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.device.BaseDevice.CouplingChannelType;

/**
 * View class for controlling left/right channel selection of the coupling feature.
 */
public class CouplingChannelController extends LinearLayout {

    TextView rightChannel;
    TextView leftChannel;

    /**
     * Runnable used for update width of Channels texvViews it waits until layout happens
     * as it is added to the message queue
     */
    private Runnable widthAdjustmentRunnable;
    private boolean isRightChannelTextLonger = false;

    /**
     * Constructor that is called if we create this view from Java code.
     *
     * @param context the context for this view.
     */
    public CouplingChannelController(Context context) {
        super(context);
    }

    /**
     * Constructor that is called if we create this view from xml code.
     *
     * @param context the context for this view.
     * @param attrs   attributes to this view from xml.
     */
    public CouplingChannelController(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        String service = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(service);
        LinearLayout layout = (LinearLayout) layoutInflater
                .inflate(R.layout.coupling_channel_controller, this, true);

        rightChannel = layout.findViewById(R.id.right_channel_text);
        leftChannel = layout.findViewById(R.id.left_channel_text);

        setupChannelViews();
    }

    /**
     * Method setting the channel selected. Selected style is not configurable at the moment.
     *
     * @param channel {@link CouplingChannelType} type to be selected. Posiible values are:
     *                PARTY - both coupled devices playing the same audio,
     *                LEFT - master device is playing left channel,
     *                RIGHT - master device is playing right channel.
     */
    public void setSelected(CouplingChannelType channel) {
        boolean isRightChannel = channel == CouplingChannelType.RIGHT;
        rightChannel.setSelected(isRightChannel);
        leftChannel.setSelected(!isRightChannel);

        rightChannel.setTextColor(isRightChannel ?
                getResources().getColor(R.color.primaryText) :
                getResources().getColor(R.color.dark_beige));

        leftChannel.setTextColor(isRightChannel ?
                getResources().getColor(R.color.dark_beige) :
                getResources().getColor(R.color.primaryText));
    }

    private void setupChannelViews() {
        //used for update width of channels textViews when they are ready to act
        widthAdjustmentRunnable = this::setupWidthOfChannels;
        post(widthAdjustmentRunnable);

        rightChannel.setSelected(true);
        leftChannel.setSelected(false);

        rightChannel.setTextColor(getResources().getColor(R.color.primaryText));
        leftChannel.setTextColor(getResources().getColor(R.color.dark_beige));

        rightChannel.setOnClickListener(v -> {
            if (!rightChannel.isSelected()) {
                setSelected(CouplingChannelType.RIGHT);
            }
        });

        leftChannel.setOnClickListener(v -> {
            if (!leftChannel.isSelected()) {
                setSelected(CouplingChannelType.LEFT);
            }
        });
    }

    private void setupWidthOfChannels() {
        if (rightChannel.getText().length() > leftChannel.getText().length()) {
            leftChannel.setWidth(rightChannel.getWidth());
        } else {
            rightChannel.setWidth(leftChannel.getWidth());
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(widthAdjustmentRunnable);
    }
}

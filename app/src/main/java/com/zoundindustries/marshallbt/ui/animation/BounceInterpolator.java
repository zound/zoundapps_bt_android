package com.zoundindustries.marshallbt.ui.animation;

import android.view.animation.Interpolator;

/**
 * Class for sound bar bounce interpolation.
 */
public class BounceInterpolator implements Interpolator {

    @Override
    public float getInterpolation(float input) {
        float retValue;

        if (input < 0.25f) {
            retValue = 3f * input;
        } else if (input < 0.5f) {
            retValue = 1 - input;
        } else if (input < 0.75f) {
            retValue = (2f * input) - 0.5f;
        } else if (input < 1) {
            retValue = 3.25f - (3f * input);
        } else {
            retValue = 0.25f;
        }

        return retValue;
    }
}

package com.zoundindustries.marshallbt.ui.adapters;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.zoundindustries.marshallbt.ui.devicesettings.tabbedViews.TabContainer;

import java.util.List;


/**
 * Pager adapter for tabs fragment
 */
public class TabsPagerAdapter extends FragmentStatePagerAdapter {

    public List<TabContainer> tabs;

    public TabsPagerAdapter(FragmentManager fragmentManager,
                            @NonNull List<TabContainer> tabs) {
        super(fragmentManager);
        this.tabs = tabs;
    }

    @Override
    public Fragment getItem(int position) {
        return tabs.get(position).fragment;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabs.get(position).title;
    }

    @Override
    public int getCount() {
        return tabs.size();
    }
}




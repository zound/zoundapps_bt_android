package com.zoundindustries.marshallbt.ui.devicesettings;

import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentDeviceSettingsBinding;
import com.zoundindustries.marshallbt.databinding.HeaderDeviceSettingsBinding;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.ui.adapters.DeviceSettingsAdapter;
import com.zoundindustries.marshallbt.ui.devicesettings.mbutton.MButtonSettingsFragment;
import com.zoundindustries.marshallbt.ui.devicesettings.tabbedViews.anc.MainANCTabsFragment;
import com.zoundindustries.marshallbt.ui.devicesettings.tabbedViews.eq.MainEQTabsFragment;
import com.zoundindustries.marshallbt.ui.onboarding.MbuttonScreenOrigin;
import com.zoundindustries.marshallbt.viewmodels.devicesettings.DeviceSettingsListViewModel;
import com.zoundindustries.marshallbt.viewmodels.factories.devicesettings.DeviceSettingsListViewModelFactory;

import static com.zoundindustries.marshallbt.ui.onboarding.OnboardingOzzyEQFragmentKt.EXTRA_MBUTTON_SCREEN_ORIGIN;
import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

public class DeviceSettingsFragment extends BaseFragment {

    private DeviceSettingsListViewModel.Body viewModel;

    private FragmentDeviceSettingsBinding binding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String deviceId = getArguments().getString(EXTRA_DEVICE_ID, "");

        viewModel = ViewModelProviders.of(getActivity(),
                new DeviceSettingsListViewModelFactory(getActivity().getApplication(), deviceId))
                .get(DeviceSettingsListViewModel.Body.class);

        initObservers();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = inflateViews(inflater, container);
        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.decouplingSpinner.setVisibility(View.GONE);
        binding.deviceSettingsList.setVisibility(View.VISIBLE);
        setupToolbar(R.string.device_settings_menu_title_settings_uc, View.VISIBLE, true);
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.inputs.setDeviceNameIfChanged();
    }

    private void initObservers() {
        viewModel.outputs.isViewChanged().observe(this, viewType -> {

            int fragmentContainer = R.id.device_settings_fragment_container;
            switch (viewType) {
                case EQUALISER:
                    startFragment(fragmentContainer,
                            new EQFragment(), viewType.getArgs(), true);
                    break;
                case EQ_EXTENDED:
                    startFragment(fragmentContainer,
                            new MainEQTabsFragment(), viewType.getArgs(), true);
                    break;
                case ABOUT_DEVICE:
                    startFragment(fragmentContainer,
                            new AboutDeviceFragment(), viewType.getArgs(), true);
                    break;
                case M_BUTTON:
                    Bundle arguments = viewType.getArgs();
                    arguments.putInt(EXTRA_MBUTTON_SCREEN_ORIGIN, MbuttonScreenOrigin.SETTINGS.ordinal());

                    startFragment(fragmentContainer,
                            new MButtonSettingsFragment(), viewType.getArgs(), true);
                    break;
                case LIGHT:
                    startFragment(fragmentContainer,
                            new LightFragment(), viewType.getArgs(), true);
                    break;
                case RENAME:
                    startFragment(fragmentContainer,
                            new RenameFragment(), viewType.getArgs(), true);
                    break;
                case FORGET_DEVICE:
                    startFragment(fragmentContainer,
                            new ForgetDeviceFragment(), viewType.getArgs(), true);
                    break;
                case COUPLE_SPEAKERS:
                    startFragment(fragmentContainer,
                            new CouplingListFragment(), viewType.getArgs(), true);
                    break;
                case COUPLE_DONE:
                    startFragment(fragmentContainer,
                            new CouplingDoneFragment(), viewType.getArgs(), true);
                    break;
                case SOUNDS:
                    startFragment(fragmentContainer,
                            new SoundsFragment(), viewType.getArgs(), true);
                    break;
                case ANC:
                    startFragment(fragmentContainer,
                            new MainANCTabsFragment(), viewType.getArgs(), true);
                    break;
                case TIMER_OFF:
                    startFragment(fragmentContainer,
                            new OffTimerFragment(), viewType.getArgs(), true);
                    break;
                case DECOUPLING_SPINNER:
                    binding.decouplingSpinner.setVisibility(View.VISIBLE);
                    binding.deviceSettingsList.setVisibility(View.GONE);
                    break;
                case HOME_SCREEN:
                    if (getActivity() != null) {
                        getActivity().finish();
                    }
                    break;
            }
        });
    }

    private FragmentDeviceSettingsBinding inflateViews(@NonNull LayoutInflater inflater,
                                                       @NonNull ViewGroup container) {
        FragmentDeviceSettingsBinding binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_device_settings,
                container,
                false);

        HeaderDeviceSettingsBinding headerBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.header_device_settings,
                binding.deviceSettingsList,
                false);
        headerBinding.setViewModel(viewModel);
        headerBinding.setLifecycleOwner(this);

        DeviceSettingsAdapter deviceSettingsAdapter = new DeviceSettingsAdapter(
                getContext(),
                viewModel,
                R.layout.list_item_device_settings);

        ListView settingsListView = binding.deviceSettingsList;
        settingsListView.setAdapter(deviceSettingsAdapter);
        settingsListView.addHeaderView(headerBinding.getRoot(),
                settingsListView,
                false);

        return binding;
    }
}

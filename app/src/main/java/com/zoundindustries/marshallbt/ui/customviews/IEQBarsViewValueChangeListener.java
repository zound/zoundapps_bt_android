package com.zoundindustries.marshallbt.ui.customviews;

import androidx.annotation.NonNull;

import com.zoundindustries.marshallbt.model.devicesettings.EQData;

/**
 * Interface defining callbacks for {@link EQBarsView}.
 */
public interface IEQBarsViewValueChangeListener {

    /**
     * Callback emitted if any of the EQ bars value has changed.
     *
     * @param data that was newly set.
     */
    void onValueChanged(@NonNull EQData data);
}
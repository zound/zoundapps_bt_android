package com.zoundindustries.marshallbt.ui.mainmenu;


import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentMainMenuBinding;
import com.zoundindustries.marshallbt.model.mainmenu.MainMenuItem;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.ui.customviews.IToolbarConfigurator;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.ui.adapters.MainMenuAdapter;
import com.zoundindustries.marshallbt.ui.mainmenu.ozzyquickguide.QuickGuideOzzyAncFragment;
import com.zoundindustries.marshallbt.ui.mainmenu.ozzyquickguide.QuickGuideOzzyControlKnobFragment;
import com.zoundindustries.marshallbt.ui.mainmenu.ozzyquickguide.QuickGuideOzzyMButtonFragment;
import com.zoundindustries.marshallbt.ui.mainmenu.ozzyquickguide.QuickGuideOzzyPairingFragment;
import com.zoundindustries.marshallbt.ui.setup.ShareDataFragment;
import com.zoundindustries.marshallbt.ui.setup.StayUpdatedFragment;
import com.zoundindustries.marshallbt.ui.setup.TacFragment;
import com.zoundindustries.marshallbt.utils.ViewExtrasGlobals;
import com.zoundindustries.marshallbt.viewmodels.factories.mainmenu.MainMenuViewModelFactory;
import com.zoundindustries.marshallbt.viewmodels.mainmenu.MainMenuViewModel;
import com.zoundindustries.marshallbt.viewmodels.mainmenu.MainMenuViewModel.MainMenuAnmationType;
import com.zoundindustries.marshallbt.viewmodels.mainmenu.MainMenuViewModel.QuickGuideFragmentLayoutType;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

import static com.zoundindustries.marshallbt.viewmodels.mainmenu.MainMenuViewModel.QUICK_GUIDE_LAYOUT_TYPE;

/**
 * Fragment which holds list of main menu settings items
 * with usage of {@link MainMenuAdapter}
 * based on logic in {@link MainMenuViewModel.Body}
 */
public class MainMenuFragment extends BaseFragment {

    private MainMenuViewModel.Body viewModel;
    private MainMenuAdapter mainMenuAdapter;
    private Disposable disposable;


    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    private ListView mainMenuListView;
    private Animation animationRightIn;
    private Animation animationLeftIn;

    public MainMenuFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            ViewType viewType = ViewType.values()[getArguments().
                    getInt(MainMenuActivity.MAIN_MENU_FRAGMENT_VIEW_TYPE)];
            MainMenuViewModelFactory viewModelFactory =
                    new MainMenuViewModelFactory(getActivity().getApplication(), viewType);
            viewModel = ViewModelProviders.of(this, viewModelFactory).
                    get(MainMenuViewModel.Body.class);
            animationRightIn = AnimationUtils.loadAnimation(getContext(), R.anim.slide_in_right);
            animationLeftIn = AnimationUtils.loadAnimation(getContext(), R.anim.slide_in_left);
            initObservers();
        } else {
            getActivity().finish();
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        FragmentMainMenuBinding binding = inflateViews(inflater, container);
        binding.setLifecycleOwner(this);
        binding.setViewModel(viewModel);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolbar(viewModel.getToolbarDisplayTextResourceId().getValue(), View.VISIBLE, true);
    }

    private FragmentMainMenuBinding inflateViews(@NonNull LayoutInflater inflater,
                                                 @NonNull ViewGroup container) {
        FragmentMainMenuBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_main_menu, container, false);
        mainMenuAdapter = new MainMenuAdapter(
                getContext(), viewModel, R.layout.list_item_main_menu);
        mainMenuListView = binding.mainMenuList;
        mainMenuListView.setAdapter(mainMenuAdapter);
        return binding;
    }

    private void initObservers() {
        viewModel.outputs.isViewChanged().observe(this, (ViewType viewChanged) -> {
            Animation targetAnimation = getAnimationIfSet(viewChanged);

            Bundle bundle = new Bundle();
            switch (viewChanged) {
                case MAIN_MENU_EMAIL_SUBSCRIPTION:
                    startFragment(new StayUpdatedFragment(), bundle);
                    break;
                case MAIN_MENU_ANALYTICS:
                    startFragment(new ShareDataFragment(), bundle);
                    break;
                case MAIN_MENU_CONTACT:
                    startFragment(new MainMenuContactFragment(), bundle);
                    break;
                case MAIN_MENU_EULA:
                    startFragment(new TacFragment(), bundle);
                    break;
                case MAIN_MENU_FOSS:
                    startFragment(new MainMenuFossFragment(), bundle);
                    break;
                case MAIN_MENU_QUICK_GUIDE_JOPLIN_BLUETOOTH_PAIRING:
                    bundle.putInt(QUICK_GUIDE_LAYOUT_TYPE,
                            QuickGuideFragmentLayoutType.BLUETOOTH_PAIRING.ordinal());
                    startFragment(new QuickGuideFragment(), bundle);
                    break;
                case MAIN_MENU_QUICK_GUIDE_JOPLIN_COUPLE_SPEAKERS:
                    bundle.putInt(QUICK_GUIDE_LAYOUT_TYPE,
                            QuickGuideFragmentLayoutType.COUPLE_SPEAKERS.ordinal());
                    startFragment(new QuickGuideFragment(), bundle);
                    break;
                case MAIN_MENU_QUICK_GUIDE_JOPLIN_PLAY_PAUSE_BUTTON:
                    bundle.putInt(QUICK_GUIDE_LAYOUT_TYPE,
                            QuickGuideFragmentLayoutType.PLAY_PAUSE_BUTTON.ordinal());
                    startFragment(new QuickGuideFragment(), bundle);
                    break;
                case MAIN_MENU_QUICK_GUIDE_OZZY_BLUETOOTH_PAIRING:
                    bundle.putInt(QUICK_GUIDE_LAYOUT_TYPE,
                            QuickGuideFragmentLayoutType.BLUETOOTH_PAIRING.ordinal());
                    startFragment(new QuickGuideOzzyPairingFragment(), bundle);
                    break;
                case MAIN_MENU_QUICK_GUIDE_OZZY_ANC:
                    startFragment(new QuickGuideOzzyAncFragment(), bundle);
                    break;
                case MAIN_MENU_QUICK_GUIDE_OZZY_M_BUTTON:
                    startFragment(new QuickGuideOzzyMButtonFragment(), bundle);
                    break;
                case MAIN_MENU_QUICK_GUIDE_OZZY_CONTROL_KNOB:
                    startFragment(new QuickGuideOzzyControlKnobFragment(), bundle);
                    break;

                /*
                  For below cases new fragment is not created, only list is updated.
                  Because of that, handling animation is done by changing ListView
                  basic animation for entering new screen(update listview) from right side.
                  animationLeftIn is used because we want to imitate that user goes back
                  to previous screen {@link ViewType#MAIN_MENU_HELP)
                 */
                case MAIN_MENU_QUICK_GUIDE_LIST:
                    mainMenuAdapter.notifyDataSetChanged();
                    mainMenuListView.startAnimation(targetAnimation);
                    break;
                case MAIN_MENU_QUICK_GUIDE_JOPLIN:
                    mainMenuAdapter.notifyDataSetChanged();
                    mainMenuListView.startAnimation(targetAnimation);
                    break;
                case MAIN_MENU_QUICK_GUIDE_OZZY:
                    mainMenuAdapter.notifyDataSetChanged();
                    mainMenuListView.startAnimation(targetAnimation);
                    break;
                case MAIN_MENU_ONLINE_MANUAL:
                    mainMenuAdapter.notifyDataSetChanged();
                    mainMenuListView.startAnimation(targetAnimation);
                    break;
                case MAIN_MENU_HELP:
                    mainMenuAdapter.notifyDataSetChanged();
                    break;
                case MAIN_MENU_ONLINE_MANUAL_DEVICE:
                    startFragment(new OnlineManualFragment(), viewChanged.getArgs());
                    mainMenuListView.startAnimation(targetAnimation);
                    break;
            }
        });

        viewModel.outputs.getToolbarDisplayTextResourceId().observe(this,
                titleId -> setupToolbar(titleId, View.VISIBLE, true));
    }

    @Override
    protected void setupToolbarBackButton() {
        Activity activity = getActivity();
        IToolbarConfigurator configurator = getIToolbarConfigurator();
        if (configurator != null) {
            disposable = configurator.getBackPressedObservable()
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .subscribe(alwaysTrueNotUsedValue -> {
                        MainMenuItem.MenuItemType currentViewType = viewModel.getCurrentViewType();
                        switch (currentViewType) {
                            case QUICK_GUIDE:
                            case ONLINE_MANUAL:
                                viewModel.onMainMenuItemClicked(MainMenuItem.MenuItemType.MAIN_MENU_HELP,
                                        true);
                                break;
                            case QUICK_GUIDE_ACTON_II:
                            case QUICK_GUIDE_STANMORE_II:
                            case QUICK_GUIDE_WOBURN_II:
                            case QUICK_GUIDE_MONITOR_II:
                                viewModel.onMainMenuItemClicked(MainMenuItem.MenuItemType.QUICK_GUIDE,
                                        true);
                                break;
                            default:
                                activity.onBackPressed();
                        }
                    });
        }
    }

    protected void startFragment(@NonNull BaseFragment fragment,
                                 @NonNull Bundle arguments) {
        startFragment(R.id.main_menu_fragment_container, fragment, arguments, true);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    private Animation getAnimationFromAnimationType(MainMenuViewModel.MainMenuAnmationType anmationType) {
        switch (anmationType) {
            case LEFT_IN:
                return animationLeftIn;
            case RIGHT_IN:
                return animationRightIn;
        }
        return animationRightIn;
    }

    private Animation getAnimationIfSet(ViewType viewChanged) {
        if (viewChanged.getArgs() == null) {
            return animationRightIn;
        }
        return getAnimationFromAnimationType(MainMenuAnmationType.values()[viewChanged.getArgs()
                .getInt(ViewExtrasGlobals.EXTRA_QUICK_GUIDE_ENTER_ANIMATION, 0)]);
    }
}

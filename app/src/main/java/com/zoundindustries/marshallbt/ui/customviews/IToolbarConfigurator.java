package com.zoundindustries.marshallbt.ui.customviews;

import android.graphics.Color;

import androidx.annotation.NonNull;

import io.reactivex.Observable;

/**
 * Class defining contract for classes that wish to share toolbar view for other objects
 * (e.g. fragments). This is used to configure toolbar across different fragments.
 */

public interface IToolbarConfigurator {

    /**
     * Enum used for change display color in toolbar items (like image color or text color)
     */
    enum ToolbarItemsColor {
        WHITE, //default
        BLACK
    }

    /**
     * Get toolbar View for customization.
     *
     * @return toolbar {@link android.view.View}.
     */
    void setToolbarName(@NonNull String name);

    /**
     * Get toolbar backbutton for customization of click events.
     */
    Observable<Boolean> getBackPressedObservable();

    /**
     * Toggle toolbar image visible/invisible.
     *
     * @param visibility flag to indicate if image is visible.
     */
    void setToolbarImageVisibility(int visibility);

    /**
     * Change color of toolbar controls. Available colors are defined in {@link ToolbarItemsColor}
     *
     * @param toolbarItemsColor color to be used.
     */
    void changeToolbarColor(@NonNull ToolbarItemsColor toolbarItemsColor);

    /**
     * Toggle toolbar back button enable/disable.
     *
     * @param isEnabled flag to set back button enabled/disabled.
     */
    void setBackButtonEnabled(boolean isEnabled);

    /**
     * Set toolbar visibility to VISIBLE/GONE.
     *
     * @param isVisible flag to set toolbar visibility.
     */
    void setToolbarVisibility(Boolean isVisible);

    /**
     * Set drawer view background color.
     *
     * @param color id to be set.
     */
    void setDrawerBackgroundColor(int color);
}

package com.zoundindustries.marshallbt.ui.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.zoundindustries.marshallbt.R
import com.zoundindustries.marshallbt.ui.BaseFragment

class JoplinOnboardingContinueFragment(val layout: Int) : BaseFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar(R.string.main_menu_empty_toolbar, View.GONE, false)

        view.findViewById<Button>(R.id.onboarding_joplin_continue_button).setOnClickListener {
            val fragment = AllFinishedFragment()
            fragment.arguments = arguments

            activity?.supportFragmentManager
                    ?.beginTransaction()
                    ?.setCustomAnimations(R.anim.slide_in_right,
                            R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                    ?.replace(R.id.home_fragment_container, fragment)
                    ?.addToBackStack(fragment.javaClass.simpleName)
                    ?.commit()
        }
    }
}
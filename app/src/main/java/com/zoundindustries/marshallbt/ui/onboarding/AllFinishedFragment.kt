package com.zoundindustries.marshallbt.ui.onboarding

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProviders
import com.zoundindustries.marshallbt.databinding.FragmentAllFinishedBinding
import com.zoundindustries.marshallbt.ui.BaseFragment
import com.zoundindustries.marshallbt.ui.devicesettings.mbutton.GvaDetailFragment
import com.zoundindustries.marshallbt.utils.ViewExtrasGlobals
import com.zoundindustries.marshallbt.viewmodels.factories.homescreen.AllFinishedViewModelFactory
import com.zoundindustries.marshallbt.viewmodels.homescreen.AllFinishedViewModel

class AllFinishedFragment : BaseFragment() {

    var viewModel: AllFinishedViewModel.Body? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val arguments = arguments
        if (arguments == null || activity == null) return

        arguments.getString(ViewExtrasGlobals.EXTRA_DEVICE_ID, "")?.let { deviceId ->

            activity?.let {
                viewModel = ViewModelProviders.of(this,
                        AllFinishedViewModelFactory(it.application, deviceId))
                        .get(AllFinishedViewModel.Body::class.java)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentAllFinishedBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        binding.allFinishedButton.setOnClickListener {
            val fm = activity?.supportFragmentManager
            fm?.let {
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            }
        }

        viewModel?.let {
            binding.allFinishedImage.setImageResource(it.getOnboardingFinishedImageId())
        }

        return binding.root
    }
}
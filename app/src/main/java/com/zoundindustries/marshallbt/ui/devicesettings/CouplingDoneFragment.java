package com.zoundindustries.marshallbt.ui.devicesettings;

import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentCoupleDeviceDoneBinding;
import com.zoundindustries.marshallbt.databinding.ListItemCoupleDeviceBinding;
import com.zoundindustries.marshallbt.model.device.BaseDevice.CouplingChannelType;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.viewmodels.devicesettings.CouplingDoneViewModel;
import com.zoundindustries.marshallbt.viewmodels.factories.devicesettings.CouplingDoneViewModelFactory;
import com.zoundindustries.marshallbt.viewmodels.devicesettings.CouplingModeViewModel.Body.CouplingMode;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

/**
 * Fragment
 */
public class CouplingDoneFragment extends BaseFragment {

    private FragmentCoupleDeviceDoneBinding binding;
    private CouplingDoneViewModel.Body viewModel;
    private CouplingMode couplingMode;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle argsBundle = getArguments();

        if (argsBundle != null) {
            String masterId = argsBundle
                    .getString(EXTRA_DEVICE_ID, "");
            String slaveId = argsBundle
                    .getString(DeviceSettingsActivity.EXTRA_DEVICE_COUPLING_SLAVE_ID,
                            "");
            couplingMode = CouplingMode.values()[argsBundle
                    .getInt(DeviceSettingsActivity.EXTRA_DEVICE_COUPLING_MODE)];

            CouplingDoneViewModelFactory viewModelFactory =
                    new CouplingDoneViewModelFactory(getActivity().getApplication(),
                            masterId,
                            slaveId);

            viewModel = ViewModelProviders.of(this, viewModelFactory)
                    .get(CouplingDoneViewModel.Body.class);

        } else {
            if (getActivity() != null) {
                getActivity().finish();
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = FragmentCoupleDeviceDoneBinding.inflate(inflater);
        binding.setLifecycleOwner(this);
        binding.setViewModel(viewModel);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupDevicesView();
        setupToolbar(R.string.device_settings_menu_item_couple_uc, View.VISIBLE, true);
        initLiveData();
    }

    private void initLiveData() {
        viewModel.outputs.getMasterName().observe(this,
                binding.masterDevice.textSpeakerName::setText);

        viewModel.outputs.getSlaveName().observe(this,
                binding.slaveDevice.textSpeakerName::setText);

        viewModel.outputs.isViewChanged().observe(this, viewType -> {
            switch (viewType) {
                case HOME_SCREEN:
                    if (getActivity() != null) {
                        getActivity().finish();
                    }
                    break;
                case BACK:
                    performBackPressedOrFinish();
                    break;
                case COUPLE_HEADS_UP:
                    startFragment(R.id.device_settings_fragment_container,
                            new CouplingHeadsUpFragment(),
                            viewType.getArgs(),
                            true);
                    break;
            }
        });
    }

    private void setupDevicesView() {
        binding.masterDevice.checkMark.setVisibility(View.GONE);
        binding.slaveDevice.checkMark.setVisibility(View.GONE);
        viewModel.getMasterImageResourceId().observe(this,
                binding.masterDevice.speakerImageView::setImageResource);

        viewModel.getSlaveImageResourceId().observe(this,
                binding.slaveDevice.speakerImageView::setImageResource);

        setupCouplingMode();
    }

    private void setupCouplingMode() {
        boolean isStandardMode =
                couplingMode == CouplingMode.STANDARD;

        viewModel.inputs.onChannelModeChanged(isStandardMode ? CouplingChannelType.PARTY :
                CouplingChannelType.getDefaultValue());

        setupCouplingModeViews(isStandardMode, binding.masterDevice);
        setupCouplingModeViews(isStandardMode, binding.slaveDevice);

        binding.coupleDoneTitle.setText(
                getResources().getString(isStandardMode ?
                        R.string.couple_screen_select_mode_ambient :
                        R.string.couple_screen_title_stereo));
        if (!isStandardMode) {
            setupChannelSwitching();
        }
    }

    private void setupCouplingModeViews(boolean isStandardMode,
                                        @NonNull ListItemCoupleDeviceBinding device) {
        device.couplingChannelSwitch.setVisibility(isStandardMode ? View.GONE : View.VISIBLE);
        device.playInformationTextView.setVisibility(isStandardMode ? View.VISIBLE : View.GONE);
        device.audioVisualisationBars.setVisibility(isStandardMode ? View.VISIBLE : View.GONE);

    }

    private void setupChannelSwitching() {
        binding.masterDevice.couplingChannelSwitch.setSelected(CouplingChannelType.LEFT);
        binding.slaveDevice.couplingChannelSwitch.setSelected(CouplingChannelType.RIGHT);

        setupMasterLeftChannel();

        setupMasterRightChannel();

        setupSlaveLeftChannel();

        setupSlaveRightChannel();
    }

    private void setupSlaveRightChannel() {
        binding.slaveDevice.couplingChannelSwitch.rightChannel.setOnClickListener(v -> {
            if (!binding.slaveDevice.couplingChannelSwitch.rightChannel.isSelected()) {
                toggleMasterChannelLeft();
                viewModel.inputs.onChannelModeChanged(CouplingChannelType.LEFT);
            }
        });
    }

    private void setupSlaveLeftChannel() {
        binding.slaveDevice.couplingChannelSwitch.leftChannel.setOnClickListener(v -> {
            if (!binding.slaveDevice.couplingChannelSwitch.leftChannel.isSelected()) {
                toggleMasterChannelRight();
                viewModel.inputs.onChannelModeChanged(CouplingChannelType.RIGHT);
            }
        });
    }

    private void setupMasterRightChannel() {
        binding.masterDevice.couplingChannelSwitch.rightChannel.setOnClickListener(v -> {
            if (!binding.masterDevice.couplingChannelSwitch.rightChannel.isSelected()) {
                toggleMasterChannelRight();
                viewModel.inputs.onChannelModeChanged(CouplingChannelType.RIGHT);
            }
        });
    }

    private void setupMasterLeftChannel() {
        binding.masterDevice.couplingChannelSwitch.leftChannel.setOnClickListener(v -> {
            if (!binding.masterDevice.couplingChannelSwitch.leftChannel.isSelected()) {
                toggleMasterChannelLeft();
                viewModel.inputs.onChannelModeChanged(CouplingChannelType.LEFT);
            }
        });
    }

    private void toggleMasterChannelLeft() {
        binding.masterDevice.couplingChannelSwitch.setSelected(CouplingChannelType.LEFT);
        binding.slaveDevice.couplingChannelSwitch.setSelected(CouplingChannelType.RIGHT);
    }

    private void toggleMasterChannelRight() {
        binding.masterDevice.couplingChannelSwitch.setSelected(CouplingChannelType.RIGHT);
        binding.slaveDevice.couplingChannelSwitch.setSelected(CouplingChannelType.LEFT);
    }
}

package com.zoundindustries.marshallbt.ui.welcome;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.ActivityWelcomeBinding;
import com.zoundindustries.marshallbt.ui.BaseToolbarActivity;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Activity viewing initial Marshall animation and redirects to following views.
 * with usage of {@link WelcomeFragment}
 */
public class WelcomeActivity extends BaseToolbarActivity implements
        HasSupportFragmentInjector {

    private ActivityWelcomeBinding binding;

    private PublishSubject<Boolean> publishSubject = PublishSubject.create();

    /**
     * Encapsulates intent creation and setup of the Activity.
     *
     * @param context for the Activity start
     * @return configured starting Intent
     */
    @NonNull
    public static Intent create(@NonNull Context context) {
        return new Intent(context, WelcomeActivity.class);
    }

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_welcome);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (savedInstanceState == null) {
            startFragment(new WelcomeFragment());
        }
        setupToolbar();
    }

    private void startFragment(@NonNull Fragment fragment) {
        getSupportFragmentManager().beginTransaction().add(R.id.welcome_screen_fragment_container,
                fragment).commit();
    }

    private void setupToolbar() {
        binding.welcomeScreenToolbar.toolbarImage.setOnClickListener(view ->
                publishSubject.onNext(true));
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    @Override
    public void setToolbarName(@NonNull String name) {
        //NOP
    }

    @Override
    public Observable<Boolean> getBackPressedObservable() {
        return publishSubject;
    }

    @Override
    public void setToolbarImageVisibility(int visibility) {
        binding.welcomeScreenToolbar.toolbarImage.setVisibility(visibility);
    }

    @Override
    public void changeToolbarColor(@NonNull ToolbarItemsColor toolbarItemsColor) {
        super.changeToolbarColor(toolbarItemsColor);
        binding.welcomeScreenToolbar.toolbarImage.setImageDrawable(toolbarDrawable);
        binding.welcomeScreenToolbar.toolbar.setBackgroundColor(toolbarBackgroundColorId);
        binding.welcomeScreenToolbar.toolbarTitle.setTextColor(toolbarTextColor);
    }

    @Override
    public void setToolbarVisibility(Boolean isVisible) {
        if (getSupportActionBar() == null) {
            return;
        }

        if (isVisible) {
            getSupportActionBar().hide();
        } else {
            getSupportActionBar().show();
        }
    }
}

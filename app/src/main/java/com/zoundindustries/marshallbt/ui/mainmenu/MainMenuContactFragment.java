package com.zoundindustries.marshallbt.ui.mainmenu;


import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.MainMenuFragmentContactBinding;
import com.zoundindustries.marshallbt.model.mainmenu.joplin.WebsiteLinkHolder;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.viewmodels.mainmenu.MainMenuContactViewModel;

/**
 * Displays content for 'Contact' item from 'Help' which is nested in Main Menu
 * Main Menu {@link MainMenuFragment}-> 'Help' -> 'Contact'
 */
public class MainMenuContactFragment extends BaseFragment {

    private MainMenuFragmentContactBinding binding;
    private MainMenuContactViewModel.Body viewModel;


    public MainMenuContactFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this)
                .get(MainMenuContactViewModel.Body.class);
        initObservers();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = MainMenuFragmentContactBinding.inflate(inflater);
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolbar(R.string.main_menu_item_contact_uc, View.VISIBLE, true);
    }

    private void initObservers() {
        viewModel.outputs.isViewChanged().observe(this, viewType -> {
            Intent intent = new Intent();
            switch (viewType) {
                case MAIN_MENU_CONTACT_WEBSITE:
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(WebsiteLinkHolder.MAIN_MENU_CONTACT_GOTO_WEBSITE_URL));
                    break;
                case MAIN_MENU_CONTACT_SUPPORT:
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(WebsiteLinkHolder.MAIN_MENU_CONTACT_SUPPORT_URL));
                    break;
            }
            startActivity(intent);
        });
    }
}

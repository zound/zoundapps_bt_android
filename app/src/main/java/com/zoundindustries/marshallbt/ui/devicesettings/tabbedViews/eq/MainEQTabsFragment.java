package com.zoundindustries.marshallbt.ui.devicesettings.tabbedViews.eq;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.transition.TransitionManager;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.devicesettings.EQTabType;
import com.zoundindustries.marshallbt.viewmodels.devicesettings.EQTabViewModel;
import com.zoundindustries.marshallbt.ui.ViewFlowController;
import com.zoundindustries.marshallbt.ui.devicesettings.tabbedViews.BaseTabsFragment;
import com.zoundindustries.marshallbt.ui.devicesettings.tabbedViews.TabContainer;
import com.zoundindustries.marshallbt.viewmodels.factories.devicesettings.EQTabViewModelFactory;

import java.util.ArrayList;
import java.util.List;

import static com.zoundindustries.marshallbt.model.devicesettings.EQTabType.M1;
import static com.zoundindustries.marshallbt.model.devicesettings.EQTabType.M2;
import static com.zoundindustries.marshallbt.model.devicesettings.EQTabType.M3;
import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

/**
 * Equaliser fragment with three tabs.
 */
public class MainEQTabsFragment extends BaseTabsFragment {

    private EQTabViewModel.Body viewModel;
    private String deviceId;

    private EQTabType currentTab;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments == null || getActivity() == null) return;

        deviceId = arguments.getString(EXTRA_DEVICE_ID, "");

        EQTabViewModelFactory factory =
                new EQTabViewModelFactory(
                        getActivity().getApplication(),
                        deviceId);

        viewModel = ViewModelProviders.of(getActivity(), factory)
                .get(EQTabViewModel.Body.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolbar(R.string.device_settings_menu_item_equaliser_uc, View.VISIBLE, true);

        initObservers();
    }

    @Override
    public void onStart() {
        super.onStart();

        if (binding.tabs.getTabAt(0) != null)
            binding.tabs.getTabAt(0).setIcon(R.drawable.ic_m1_grey);

        if (binding.tabs.getTabAt(1) != null)
            binding.tabs.getTabAt(1).setIcon(R.drawable.ic_m2_grey);

        if (binding.tabs.getTabAt(2) != null)
            binding.tabs.getTabAt(2).setIcon(R.drawable.ic_m3_grey);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (currentTab != null) {
            // the current tab is updated in pager onTabSelected method. On init it is different, though.
            binding.pager.setCurrentItem(currentTab.getIntValue());
        }
        setupTabsListeners();
    }

    @Override
    public List<TabContainer> getTabsList() {
        List<TabContainer> tabs = new ArrayList<>();

        Bundle b = new Bundle();
        b.putString(EXTRA_DEVICE_ID, deviceId);

        Fragment f = new EQTabM1Fragment();
        f.setArguments(b);
        TabContainer t1 = new TabContainer(f, "");
        tabs.add(t1);

        Fragment g = new EQTabM2Fragment();
        g.setArguments(b);
        TabContainer t2 = new TabContainer(g, "");
        tabs.add(t2);

        Fragment h = new EQTabM3Fragment();
        h.setArguments(b);
        TabContainer t3 = new TabContainer(h, "");
        tabs.add(t3);

        return tabs;
    }

    private void setupTabsListeners() {
        viewModel.outputs.notifyActiveTab().observe(getActivity(), newTab -> {
            if (currentTab != newTab) {
                currentTab = newTab;
                if (binding != null) {
                    switch (newTab) {
                        case M1:
                            binding.pager.setCurrentItem(0, true);
                            break;
                        case M2:
                            binding.pager.setCurrentItem(1, true);
                            break;
                        case M3:
                            binding.pager.setCurrentItem(2, true);
                            break;
                    }
                }
            }
        });

        binding.tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getIcon() != null && isAdded())
                    tab.getIcon().setTint(getResources().getColor(R.color.colorAccent));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                if (tab.getIcon() != null && isAdded())
                    tab.getIcon().setTint(getResources().getColor(R.color.highlight));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        binding.pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                EQTabType tabType = M1;

                switch (position) {
                    case 0:
                        tabType = M1;
                        break;
                    case 1:
                        tabType = M2;
                        break;
                    case 2:
                        tabType = M3;
                        break;
                }
                if (currentTab != tabType) {
                    viewModel.inputs.onEqTabChanged(tabType);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initObservers() {
        viewModel.outputs.isViewChanged().observe(this, viewType -> {
            if (getActivity() != null) {
                Context context = getContext();
                if (context != null) {
                    if (viewType == ViewFlowController.ViewType.BACK) {
                        performBackPressedOrFinish();
                    }
                }
            }
        });

        viewModel.outputs.notifyValuesLoaded().observe(this,
                loaded -> {
                    if (loaded) {
                        TransitionManager.beginDelayedTransition(binding.tabsContainer);
                        binding.tabsContainer.setState(R.id.eq_tabs_connected, 0, 0);
                    }
                });

        viewModel.outputs.notifyIsAuxSourceActive().observe(this,
                isActive -> {
                    if (isActive)
                        performBackPressedOrFinish();
                });
    }

}

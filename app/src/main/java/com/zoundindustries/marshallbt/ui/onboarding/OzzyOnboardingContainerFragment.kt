package com.zoundindustries.marshallbt.ui.onboarding

import android.os.Bundle
import androidx.fragment.app.Fragment


private const val ONBOARDING_OZZY_PAGES_NUMBER = 3

/**
 * Fragment showing ozzy onboarding.
 */
class OzzyOnboardingContainerFragment : BaseOnboardingContainerFragment() {
    override val numberOfPages: Int
        get() = ONBOARDING_OZZY_PAGES_NUMBER

    override fun setupFragments(position: Int): Fragment {
        return when (position) {
            0 -> OnboardingOzzyAncFragment()
            1 -> OnboardingOzzyMButtonFragment()
            2 -> addBundleToFragment(OnboardingOzzyEQFragment(), arguments)
            else -> {
                OnboardingOzzyAncFragment()
            }
        }
    }

    private fun addBundleToFragment(fragment: Fragment, bundle: Bundle?): Fragment {
        fragment.arguments = bundle
        return fragment
    }
}
package com.zoundindustries.marshallbt.ui.devicesettings;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatSeekBar;
import android.util.AttributeSet;

import com.zoundindustries.marshallbt.R;

/**
 * Custom {@link AppCompatSeekBar} to display the progress value above the thumb
 */
public class LightSeekBar extends AppCompatSeekBar {

    private int textSizePx;
    private int extraHorizontalPaddingPx;
    private int leftPaddingPx;
    private String progressText;
    private Paint paint;
    private Rect textBounds;
    private boolean isProgressVisible;

    /**
     * Constructor that is called if we create this view from Java code
     *
     * @param context the context for this view
     */
    public LightSeekBar(@NonNull Context context) {
        super(context);
        init();
    }

    /**
     * Constructor that is called if we create this view from xml code
     *
     * @param context the context for this view
     * @param attrs   attributes to this view from xml
     */
    public LightSeekBar(@NonNull Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    /**
     * Constructor that is called if we create this view from xml code with style reference
     *
     * @param context      the context for this view
     * @param attrs        attributes to this view from xml
     * @param defStyleAttr a reference to a style which one contains default values for this view
     */
    public LightSeekBar(@NonNull Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    /**
     * Show progress of the light seekbar
     */
    public void showProgress() {
        isProgressVisible = true;
    }

    /**
     * Hide progress of the light seekbar
     */
    public void hideProgress() {
        isProgressVisible = false;
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (isProgressVisible) {
            // Every time when the progress changes, need to recalculate the bound of the text, because
            // it is needed for positioning the text above the thumb
            progressText = String.valueOf(getProgress());
            paint.getTextBounds(progressText, 0, progressText.length(), textBounds);
            canvas.drawText(progressText, getXForText(), textBounds.height(), paint);
        }
    }

    private void init() {
        textBounds = new Rect();

        setDimensions();
        setPaint();
        setActualViewPadding();
    }

    private void setDimensions() {
        textSizePx = getResources().getDimensionPixelSize(
                R.dimen.light_seekbar_text_size);
        extraHorizontalPaddingPx = getResources().getDimensionPixelSize(
                R.dimen.light_seekbar_extra_horizontal_padding);
        leftPaddingPx = getResources().getDimensionPixelSize(
                R.dimen.light_seekbar_left_padding);
    }

    private void setPaint() {
        paint = new Paint();

        paint.setColor(getResources().getColor(R.color.primaryText));
        paint.setTextSize(textSizePx);
        paint.getTextBounds(String.valueOf(getMax()), 0,
                String.valueOf(getMax()).length(), textBounds);
        paint.setTypeface(Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Roboto-Bold.ttf"));
    }

    private void setActualViewPadding() {
        // Need to set padding for the text at the top and to the horizontal sides, because
        // without it some part of the text won't be visible
        int rightPadding = getPaddingLeft() - getThumbOffset() + textBounds.width() / 2;
        int verticalPadding = textBounds.height() + extraHorizontalPaddingPx;

        setPadding(leftPaddingPx, verticalPadding, rightPadding,
                verticalPadding);
    }

    private float getXForText() {
        return getPaddingLeft() - getThumbOffset() + getThumb().getBounds().exactCenterX() -
                (textBounds.width() / 2);
    }
}

package com.zoundindustries.marshallbt.ui.setup;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshallbt.databinding.FragmentSetupSaveDataBinding;
import com.zoundindustries.marshallbt.di.Injectable;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.ui.homescreen.HomeActivity;
import com.zoundindustries.marshallbt.viewmodels.setup.SaveDataViewModel;

import javax.inject.Inject;

/**
 * Fragment to show a spinner while saving user setup data
 */
public class SaveDataFragment extends BaseFragment implements Injectable {

    private FragmentSetupSaveDataBinding binding;
    private SaveDataViewModel.Body viewModel;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    public SaveDataFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(SaveDataViewModel.Body.class);

        binding = FragmentSetupSaveDataBinding.inflate(inflater);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initObservers();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initObservers() {
        viewModel.outputs.isViewCompleted().observe(this, viewComplete -> {
            if (viewComplete != null && viewComplete) {
                startActivity(HomeActivity.create(getContext()));
                if (getActivity() != null) {
                    getActivity().finish();
                }
            }
        });
    }
}

package com.zoundindustries.marshallbt.ui.customviews

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import com.zoundindustries.marshallbt.R

/**
 * Custom Layout class wrapping numbered paragraph text.
 * Additional number parameter allows to set numbering of the TextView.
 *
 * @param context used for super class LinearLayout.
 * @param attrs used to extend attributes of the view.
 */
class NumberedTextView(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {

    private val number: TextView
    private val paragraph: TextView
    private val container: LinearLayout

    init {
        val inflater: LayoutInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.numbered_text, this, true)

        number = findViewById(R.id.numbered_text_number)
        paragraph = findViewById(R.id.numbered_text_description)
        container = findViewById(R.id.numbered_text_container)

        setupViews(attrs)
    }

    override fun setTextAlignment(alignment: Int) {
        paragraph.textAlignment = alignment
    }

    /**
     * Set gravity of the main NumberedTextView container.
     *
     * @param gravity from Gravity class.
     */
    fun setLayoutGravity(gravity: Int) {
        container.gravity = gravity
    }

    /**
     * Set numbering of the current text.
     *
     * @param num to be displayed.
     * '0' means no numbering.
     */
    @SuppressLint("SetTextI18n")
    fun setNumber(num: Int) {
        number.text = if (num == 0) "" else "$num.\t"
    }

    /**
     * Set paragraph text to be show.
     *
     * @param text to be show.
     */
    fun setParagraph(text: String) {
        paragraph.text = text
    }

    @SuppressLint("SetTextI18n")
    private fun setupViews(attrs: AttributeSet) {
        attrs.let {
            val typedArray = context.obtainStyledAttributes(it, R.styleable.NumberedTextView)
            if (typedArray.hasValue(R.styleable.NumberedTextView_number) &&
                    typedArray.hasValue(R.styleable.NumberedTextView_paragraph)) {
                paragraph.text = typedArray.getText(R.styleable.NumberedTextView_paragraph) as String
                number.text = "${typedArray.getInt(R.styleable.NumberedTextView_number, 0)} .\t"
            }
            typedArray.recycle()
        }
    }
}
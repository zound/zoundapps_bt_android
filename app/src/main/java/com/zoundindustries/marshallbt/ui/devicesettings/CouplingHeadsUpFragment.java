package com.zoundindustries.marshallbt.ui.devicesettings;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentCouplingHeadsUpBinding;
import com.zoundindustries.marshallbt.ui.BaseFragment;

/**
 * Fragment showing coupling information to the user.
 */
public class CouplingHeadsUpFragment extends BaseFragment {

    private FragmentCouplingHeadsUpBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentCouplingHeadsUpBinding.inflate(inflater);
        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle argsBundle = getArguments();

        String masterName = "";
        String slaveName = "";

        if (argsBundle != null) {
            masterName = argsBundle
                    .getString(DeviceSettingsActivity.EXTRA_DEVICE_MASTER_NAME, "");
            slaveName = argsBundle
                    .getString(DeviceSettingsActivity.EXTRA_DEVICE_SLAVE_NAME, "");
        } else {
            if (getActivity() != null) {
                getActivity().finish();
            }
        }

        binding.headsUpDescription.setText(getString(R.string.couple_screen_heads_up_subtitle,
                masterName, slaveName));
        binding.headsUpButton.setOnClickListener(this::onAcceptButtonClicked);
        setupToolbar(R.string.device_settings_menu_item_couple_uc, View.GONE, true);
    }

    /**
     * Callback method for button clicked.
     *
     * @param v the view that was clicked.
     */
    public void onAcceptButtonClicked(@NonNull View v) {
        if (getActivity() != null) {
            getActivity().finish();
        }
    }
}

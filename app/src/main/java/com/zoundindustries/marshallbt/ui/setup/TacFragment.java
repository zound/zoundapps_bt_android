package com.zoundindustries.marshallbt.ui.setup;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentSetupTacBinding;
import com.zoundindustries.marshallbt.di.Injectable;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.ui.setup.BottomReachedIndicatingScrollView.IOnScrollListener;
import com.zoundindustries.marshallbt.viewmodels.setup.TacViewModel;

import java.io.InputStream;
import java.util.NoSuchElementException;
import java.util.Scanner;

import javax.inject.Inject;

/**
 * Fragment showing static terms and conditions string
 */

public class TacFragment extends BaseFragment implements Injectable {

    private TacViewModel.Body viewModel;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private FragmentSetupTacBinding binding;

    public TacFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(TacViewModel.Body.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentSetupTacBinding.inflate(inflater);

        binding.setLifecycleOwner(this);
        binding.setViewModel(viewModel);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListenerForGradientViewChange();

        binding.agreementText.setText(Html.fromHtml(
                convertStreamToString(getResources().openRawResource(R.raw.terms_and_conditions))));
        setupToolbar(R.string.main_menu_empty_toolbar, View.VISIBLE, true);
    }

    @NonNull
    private String convertStreamToString(@Nullable InputStream inputStream) {
        String val = "";
        if (inputStream != null) {
            Scanner s = new Scanner(inputStream).useDelimiter("\\A");
            if (s.hasNext()) {
                try {
                    val = s.next();
                } catch (NoSuchElementException | IllegalStateException e) {
                    // todo handle that exception somehow, maybe as firebase event?
                }
            }
        }
        return val;
    }

    private void setListenerForGradientViewChange() {
        IOnScrollListener onScrollListener = isBottomEndReached ->
                viewModel.inputs.scrollViewBottomReached(isBottomEndReached);
        binding.setupDescriptionScrollview.setOnScrollListener(onScrollListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        viewModel.inputs.resetViewTransitionVariables();
    }
}

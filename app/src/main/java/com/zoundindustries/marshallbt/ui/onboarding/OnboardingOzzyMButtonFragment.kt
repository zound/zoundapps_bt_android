package com.zoundindustries.marshallbt.ui.onboarding

import com.zoundindustries.marshallbt.R

/**
 * Fragment for displaying M-button onboarding screen.
 */
class OnboardingOzzyMButtonFragment : OnboardingTabBaseFragment() {
    override val headerId: Int
        get() = R.string.ozzy_onboarding_mbutton_title_uc
    override val descriptionId: Int
        get() = R.string.ozzy_onboarding_mbutton_subtitle
    override val imageResId: Int
        get() = R.drawable.carousel_2_image
}
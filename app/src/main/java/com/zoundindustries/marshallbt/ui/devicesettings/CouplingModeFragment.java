package com.zoundindustries.marshallbt.ui.devicesettings;

import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.FragmentCoupleDeviceModeBinding;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.viewmodels.devicesettings.CouplingModeViewModel;
import com.zoundindustries.marshallbt.viewmodels.devicesettings.CouplingModeViewModel.Body.CouplingMode;
import com.zoundindustries.marshallbt.viewmodels.factories.devicesettings.CouplingModeViewModelFactory;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;

/**
 * Class used to let user decide on the coupling mode.
 */
public class CouplingModeFragment extends BaseFragment {

    private CouplingModeViewModel.Body viewModel;
    private FragmentCoupleDeviceModeBinding binding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle argsBundle = getArguments();

        if (argsBundle != null) {
            CouplingModeViewModelFactory viewModelFactory = new CouplingModeViewModelFactory(
                    getActivity().getApplication(),
                    getArguments()
                            .getString(EXTRA_DEVICE_ID, ""),
                    getArguments()
                            .getString(DeviceSettingsActivity.EXTRA_DEVICE_COUPLING_SLAVE_ID,
                                    ""));

            viewModel = ViewModelProviders.of(this, viewModelFactory)
                    .get(CouplingModeViewModel.Body.class);

            initLiveData();
        } else {
            if (getActivity() != null) {
                getActivity().finish();
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = FragmentCoupleDeviceModeBinding.inflate(inflater);
        binding.setLifecycleOwner(this);
        binding.setViewModel(viewModel);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupSelectionFields();
        initModeSelectionLiveData();
        setupToolbar(R.string.device_settings_menu_item_couple_uc, View.VISIBLE, true);
    }

    private void initLiveData() {
        initModeSelectionLiveData();

        viewModel.outputs.isViewChanged().observe(this, viewType -> {
            switch (viewType) {
                case COUPLE_DONE:
                    startFragment(R.id.device_settings_fragment_container,
                            new CouplingDoneFragment(), viewType.getArgs(), true);
                    break;
                case BACK:
                    performBackPressedOrFinish();
                    break;
                case HOME_SCREEN:
                    if (getActivity() != null) {
                        getActivity().finish();
                    }
                    break;
            }
        });
    }

    private void initModeSelectionLiveData() {
        viewModel.outputs.getStandardModeSelected()
                .observe(this, this::setStandardModeSelected);

        viewModel.outputs.getStereoModeSelected()
                .observe(this, this::setStereoModeSelected);
    }

    private void setupSelectionFields() {
        binding.standardMode.setOnClickListener(v ->
                viewModel.inputs.onCouplingModeSelected(CouplingMode.STANDARD));

        binding.stereoMode.setOnClickListener(v ->
                viewModel.inputs.onCouplingModeSelected(CouplingMode.STEREO));
    }


    private void setStandardModeSelected(boolean isSelected) {
        binding.standardCheckMark.setVisibility(isSelected ? View.VISIBLE : View.GONE);
        binding.standardMode.setSelected(isSelected);
        setNextButtonVisibility(isSelected);
    }

    private void setStereoModeSelected(boolean isSelected) {
        binding.stereoCheckMark.setVisibility(isSelected ? View.VISIBLE : View.GONE);
        binding.stereoMode.setSelected(isSelected);
        setNextButtonVisibility(isSelected);
    }

    private void setNextButtonVisibility(boolean isSelected) {
        boolean shouldShowButton = (isSelected ||
                binding.standardMode.isSelected() ||
                binding.stereoMode.isSelected());
        binding.buttonNext.setVisibility(shouldShowButton ? View.VISIBLE : View.GONE);
    }

}

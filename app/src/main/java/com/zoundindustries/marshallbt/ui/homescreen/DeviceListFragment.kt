package com.zoundindustries.marshallbt.ui.homescreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.zoundindustries.marshallbt.R
import com.zoundindustries.marshallbt.model.device.BaseDevice
import com.zoundindustries.marshallbt.ui.BaseFragment
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType
import com.zoundindustries.marshallbt.ui.adapters.HomeDevicesAdapter
import com.zoundindustries.marshallbt.ui.customviews.IToolbarConfigurator
import com.zoundindustries.marshallbt.ui.error.types.LocationErrorActivity
import com.zoundindustries.marshallbt.viewmodels.factories.homescreen.DeviceListViewModelFactory
import com.zoundindustries.marshallbt.viewmodels.homescreen.DeviceListViewModel
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.*

/**
 * Fragment visualizing all homeDevices discovered in the system.
 */
class DeviceListFragment : BaseFragment() {

    private var viewModel: DeviceListViewModel.Body? = null
    private var homeDevicesAdapter: HomeDevicesAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity?.let { activity ->
            val viewModelFactory = DeviceListViewModelFactory(
                    activity.application, ArrayList<BaseDevice>())

            viewModel = ViewModelProviders.of(this, viewModelFactory)
                    .get(DeviceListViewModel.Body::class.java)
            viewModel?.inputs?.onListCreated()
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_home, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        checkDevicesFound()

        viewModel?.let { viewModel ->
            homeDevicesAdapter = HomeDevicesAdapter(viewModel.outputs.homeDevices, this)
            homeDevicesAdapter?.setHasStableIds(true)
            list_speakers.adapter = homeDevicesAdapter
            list_speakers.layoutManager = LinearLayoutManager(activity)
            swipe_refresh_layout.setOnRefreshListener { viewModel.onRefresh() }
            initLiveData()
        }
    }

    override fun onStart() {
        super.onStart()
        setupToolbar()
        viewModel?.onRefresh()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposeRecyclerView()
    }

    private fun setupDevicesListView(devicesFound: Boolean) {
        if (!devicesFound) {
            viewModel?.notifyFirebaseNoDevicesFoundIfNoLocation()
        }
        list_speakers.visibility = if (devicesFound) View.VISIBLE else View.GONE
        empty_list_view.visibility = if (devicesFound) View.GONE else View.VISIBLE
    }

    private fun setupToolbar() {
        activity?.let { activity ->
            (activity as IToolbarConfigurator).setToolbarVisibility(true)
        }
    }

    private fun initLiveData() {
        viewModel?.let { viewModel ->
            viewModel.listDiff.observe(this, Observer { diffResult ->
                homeDevicesAdapter?.let { homeDevicesAdapter ->
                    diffResult.dispatchUpdatesTo(homeDevicesAdapter)
                }
            })

            viewModel.outputs.isViewChanged
                    .observe(this, Observer { viewType ->
                        when (viewType) {
                            ViewType.EMPTY_DEVICE_LIST -> setupDevicesListView(false)
                            ViewType.NON_EMPTY_DEVICE_LIST -> setupDevicesListView(true)
                            ViewType.GLOBAL_SETTINGS_LOCATION -> if (activity != null) {
                                startActivity(LocationErrorActivity.create(activity!!
                                        .applicationContext))
                            }
                            else -> {
                                //nop
                            }
                        }
                    })

            viewModel.outputs.hasScanStopped().observe(this, Observer { hasScanStopped ->
                if (hasScanStopped && swipe_refresh_layout.isRefreshing) {
                    swipe_refresh_layout.isRefreshing = false
                }
            })

            viewModel.outputs.deviceRemoved.observe(this, Observer {
                homeDevicesAdapter?.notifyDataSetChanged()
            })
        }
    }

    private fun checkDevicesFound() {
        val devicesFound =
                arguments?.getBoolean(HomeActivity.FRAGMENT_ARGS_DEVICES_FOUND, false)

        devicesFound?.let {
            setupDevicesListView(devicesFound)
            if (devicesFound) {
                viewModel?.inputs?.startScanDevices()
            }
        }
    }

    private fun disposeRecyclerView() {
        disposeViewModels()
        list_speakers?.let {
            list_speakers.adapter = null
            list_speakers.clearOnChildAttachStateChangeListeners()
            list_speakers.clearOnScrollListeners()
            list_speakers.recycledViewPool.clear()
            list_speakers.removeAllViewsInLayout()
        }
        homeDevicesAdapter?.dispose()
    }

    private fun disposeViewModels() {
        // Disposing vms for every view. Every row view needs its own viewModel to attach to
        // device specific data in the service.
        list_speakers?.let {
            for (i in 0 until list_speakers.childCount) {
                (list_speakers.getChildViewHolder(list_speakers.getChildAt(i)) as HomeDevicesAdapter.ViewHolder)
                        .binding.viewModel?.dispose()
            }
        }
    }
}

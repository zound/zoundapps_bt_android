package com.zoundindustries.marshallbt.ui.error.types;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.ui.ViewFlowController;
import com.zoundindustries.marshallbt.ui.error.BaseErrorActivity;
import com.zoundindustries.marshallbt.utils.SystemServicesUtils;

/**
 * Activity for indicating turned off location in settings.
 */
public class LocationErrorActivity extends BaseErrorActivity {

    /**
     * Create intent of this class. Used in purpose of starting this activity.
     *
     * @param context for the Activity start
     * @return Intent of BluetoothErrorActivity
     */
    @NonNull
    public static Intent create(@NonNull Context context) {
        Intent intent = new Intent(context, LocationErrorActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObservers();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (SystemServicesUtils.isLocationEnabled(this)) {
            finish();
        }
    }

    private void initObservers() {
        errorViewModel.outputs.isViewChanged().observe(this, viewType -> {
            if (viewType != null) {
                switch (viewType) {
                    case GLOBAL_SETTINGS_LOCATION:
                        openLocationGlobalSettings();
                        break;
                }
            }
        });
    }

    @Override
    protected int getTitleTextId() {
        return R.string.android_error_no_location_title_uc;
    }

    @Override
    protected int getDescriptionTextId() {
        return R.string.android_error_no_location_subtitle;
    }

    @Override
    protected int getActionButtonTextId() {
        return R.string.appwide_go_to_settings_uc;
    }

    @Override
    protected boolean isBackButtonVisible() {
        return false;
    }

    @Override
    protected boolean isHardwareBackButtonActive() {
        return true;
    }

    @Override
    protected ViewFlowController.ViewType getErrorType() {
        return ViewFlowController.ViewType.ERROR_LOCATION;
    }

    private void openLocationGlobalSettings() {
        startActivity(SystemServicesUtils.getLocationSettingsIntent());
    }
}

package com.zoundindustries.marshallbt.ui.mainmenu;


import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import androidx.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.BulletSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zoundindustries.marshallbt.R;

/**
 * CustomView for displaying paragraphs in main menu,
 * view has two TextViews, one is used as number span,
 * second contains instruction(display text with manual)
 * The class is using AttributeSet from setting text values for those textViews.
 */
public class MainMenuCustomParagraphView extends LinearLayout {

    private final String bulletParagraphString;

    private TextView indexTextView;
    private TextView descriptionTextView;

    public MainMenuCustomParagraphView(Context context) {
        super(context);
        bulletParagraphString =
                context.getString(R.string.main_menu_custom_paragraph_bullet_index);
        init(context);
    }

    public MainMenuCustomParagraphView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        bulletParagraphString =
                context.getString(R.string.main_menu_custom_paragraph_bullet_index);
        init(getContext());
        arrangeTextViewsBasedOnAttributes(context, attrs);
    }

    private void init(Context context) {
        String service = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(service);
        LinearLayout layout = (LinearLayout) layoutInflater
                .inflate(R.layout.main_menu_custom_display_view_layout, this, true);
        indexTextView = layout.findViewById(R.id.number_text_view);
        descriptionTextView = layout.findViewById(R.id.description_text_view);
    }

    private void arrangeTextViewsBasedOnAttributes(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.MainMenuCustomParagraphView, 0, 0);
        setCorrectText(typedArray);

        if (isHeader(typedArray)) {
            setHeaderTextAppearance(context);
        }
        typedArray.recycle();
    }

    public void setCorrectText(TypedArray typedArray) {
        String indexText = typedArray.getString(R.styleable.MainMenuCustomParagraphView_index);
        if (indexText != null) {
            if (indexText.equals(bulletParagraphString)) {
                SpannableString string = new SpannableString("");
                string.setSpan(new BulletSpan(1), 0, 0,
                        Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                indexTextView.setText(string);
            } else {
                indexTextView.setText(typedArray.getString(
                        R.styleable.MainMenuCustomParagraphView_index));
            }
        }
        descriptionTextView.setText(
                typedArray.getString(R.styleable.MainMenuCustomParagraphView_description));
    }

    public boolean isHeader(TypedArray typedArray) {
        return typedArray.getInteger(R.styleable.MainMenuCustomParagraphView_textType,
                TextType.CONTENT.getType()) == TextType.HEADER.getType();
    }

    private void setHeaderTextAppearance(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            descriptionTextView.setTextAppearance(R.style.MainMenuQuickGuideHeader);
        } else {
            descriptionTextView.setTextAppearance(context, R.style.MainMenuQuickGuideHeader);
        }
    }

    private enum TextType {
        CONTENT(0), HEADER(1);
        int type;

        TextType(int type) {
            this.type = type;
        }

        public int getType() {
            return type;
        }
    }
}

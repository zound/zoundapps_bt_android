package com.zoundindustries.marshallbt.ui;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import com.zoundindustries.marshallbt.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Class for setup calligraphy to use external fonts in the app. Every Activity should use
 * this class as a base. May be extended with other common code.
 */
public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }

    protected void startFragment(int containerViewId,
                                 @NonNull Fragment fragment,
                                 @Nullable Bundle arguments,
                                 boolean addToBackStack) {
        if (arguments != null) {
            fragment.setArguments(arguments);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

            fragmentTransaction.setCustomAnimations(R.anim.slide_in_right,
                    R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);

            if (addToBackStack) {
                fragmentTransaction.replace(containerViewId,
                        fragment, fragment.getClass().getSimpleName());
            }

            fragmentTransaction.commit();
        }
    }
}
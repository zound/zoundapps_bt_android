package com.zoundindustries.marshallbt.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.ListEqSpinnerDropdownBinding;
import com.zoundindustries.marshallbt.utils.EqPresetType;

import java.util.List;

/**
 * Adapter for EQ preset list
 */
public class EQPresetAdapter extends ArrayAdapter<EqPresetType> {

    private List<EqPresetType> presets;
    private boolean isEQExtended;

    public EQPresetAdapter(@NonNull Context context,
                           int resource,
                           int textViewResource,
                           @NonNull List<EqPresetType> presetTypes,
                           boolean isEQExtended) {
        super(context, resource, textViewResource);
        this.presets = presetTypes;
        this.isEQExtended = isEQExtended;
    }

    @Override
    public int getCount() {
        return presets.size();
    }

    @Nullable
    @Override
    public EqPresetType getItem(int position) {
        return presets.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            ListEqSpinnerDropdownBinding binding = DataBindingUtil
                    .inflate(LayoutInflater.from(parent.getContext()),
                            R.layout.list_eq_spinner_dropdown,
                            parent, false);
            viewHolder = new ViewHolder(binding);
            convertView = binding.getRoot();
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.name.setText(getPresetName(parent.getContext(), presets.get(position)));
        viewHolder.name.setTextColor(parent.getContext()
                .getResources().getColor(R.color.dark_beige));

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            ListEqSpinnerDropdownBinding binding = DataBindingUtil
                    .inflate(LayoutInflater.from(parent.getContext()),
                            R.layout.list_eq_spinner_dropdown,
                            parent, false);
            viewHolder = new ViewHolder(binding);
            convertView = binding.getRoot();
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.name.setText(getPresetName(parent.getContext(), presets.get(position)));
        return convertView;
    }

    @NonNull
    private String getPresetName(@NonNull Context context, @NonNull EqPresetType presetType) {
        String value = "";
        switch (presetType) {
            case CUSTOM:
                value = context.getResources().getString(R.string.equaliser_screen_preset_custom_uc);
                break;
            case FLAT:
                value = context.getResources().getString(
                        isEQExtended ? R.string.equaliser_screen_preset_marshall_uc :
                                R.string.equaliser_screen_preset_flat_uc);
                break;
            case ROCK:
                value = context.getResources().getString(R.string.equaliser_screen_preset_rock_uc);
                break;
            case METAL:
                value = value = context.getResources().getString(
                        isEQExtended ? R.string.equaliser_screen_preset_spoken_uc :
                                R.string.equaliser_screen_preset_metal_uc);
                break;
            case POP:
                value = context.getResources().getString(R.string.equaliser_screen_preset_pop_uc);
                break;
            case HIPHOP:
                value = context.getResources().getString(R.string.equaliser_screen_preset_hip_hop_uc);
                break;
            case ELECTRONIC:
                value = context.getResources().getString(R.string.equaliser_screen_preset_electronic_uc);
                break;
            case JAZZ:
                value = context.getResources().getString(R.string.equaliser_screen_preset_jazz_uc);
                break;
        }

        return value;
    }

    public static class ViewHolder {
        ListEqSpinnerDropdownBinding binding;

        private TextView name;

        ViewHolder(@NonNull ListEqSpinnerDropdownBinding binding) {
            this.binding = binding;
            name = binding.name;
        }
    }
}

package com.zoundindustries.marshallbt.ui.devicesettings;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.SeekBar;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.EqGroupViewBinding;
import com.zoundindustries.marshallbt.databinding.EqViewBinding;
import com.zoundindustries.marshallbt.model.devicesettings.EQData;
import com.zoundindustries.marshallbt.model.devicesettings.IEqProgressChangeListener;
import com.zoundindustries.marshallbt.utils.DebouncedSeekBarListener;

/**
 * Custom layout to group {@link EQSeekBar} views
 */
public class EQGroup extends ConstraintLayout {

    public static final int ANIMATION_DURATION_MS = 5;
    public static final int ANIMATION_START_DELAY_MS = 5;

    private IEqProgressChangeListener onProgressListener;
    private EqGroupViewBinding binding;
    private Context context;

    public EQGroup(@NonNull Context context) {
        super(context);
        setContext(context);
    }

    public EQGroup(@NonNull Context context, @NonNull AttributeSet attrs) {
        super(context, attrs);
        setContext(context);
    }

    public EQGroup(@NonNull Context context, @NonNull AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setContext(context);
    }

    /**
     * Initialize the binding, {@link EQData} and observer
     */
    public void init() {
        //TODO find a way to call this method inside the class, because now we need to call it
        // from the fragment after the binding happened
        binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.eq_group_view, this, true);
        initEQObserver();
    }

    /**
     * Set a listener for handling progress changes
     *
     * @param onProgressListener listener to be set
     */
    public void setOnProgressListener(@Nullable IEqProgressChangeListener onProgressListener) {
        this.onProgressListener = onProgressListener;
    }

    /**
     * Set values on seekbars to given value
     *
     * @param preset eq preset to be set
     */
    public void setPreset(@NonNull EQData preset) {
        binding.eqBass.eqSeekBar.setEQProgress(preset.getBass(), false);
        binding.eqLow.eqSeekBar.setEQProgress(preset.getLow(), false);
        binding.eqMid.eqSeekBar.setEQProgress(preset.getMid(), false);
        binding.eqUpper.eqSeekBar.setEQProgress(preset.getUpper(), false);
        binding.eqHigh.eqSeekBar.setEQProgress(preset.getHigh(), false);
    }

    private void initEQObserver() {
        setEQBarListener(binding.eqBass, EQData.ValueType.BASS);
        setEQBarListener(binding.eqLow, EQData.ValueType.LOW);
        setEQBarListener(binding.eqMid, EQData.ValueType.MID);
        setEQBarListener(binding.eqUpper, EQData.ValueType.UPPER);
        setEQBarListener(binding.eqHigh, EQData.ValueType.HIGH);
    }

    private void setEQBarListener(@NonNull EqViewBinding view, @NonNull EQData.ValueType type) {
        view.eqSeekBar.setOnProgressChangedListener(new DebouncedSeekBarListener() {
            @Override
            public void runTask(@NonNull SeekBar seekBar, int progress, boolean fromUser) {
                onProgressListener.onProgressChanged(type, progress, fromUser);
            }
        });
    }

    private void setContext(@NonNull Context context) {
        this.context = context;
    }

}

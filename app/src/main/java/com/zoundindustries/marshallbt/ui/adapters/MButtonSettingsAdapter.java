package com.zoundindustries.marshallbt.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.ListItemMButtonSettingsBinding;
import com.zoundindustries.marshallbt.model.devicesettings.MButtonSettingsItem;
import com.zoundindustries.marshallbt.viewmodels.devicesettings.MButtonViewModel;

import java.util.List;

import static com.zoundindustries.marshallbt.utils.AnimationUtilsKt.animateVisibilityChange;

/**
 * Adapter holding M-Button device settings list and providing it to the RecyclerView.
 */
public class MButtonSettingsAdapter extends RecyclerView.Adapter<MButtonSettingsAdapter.ViewHolder> {

    private List<MButtonSettingsItem> mButtonSettings;
    private MButtonViewModel.Body viewModel;
    private int selectedPosition = -1;

    /**
     * Primary constructor for the {@link MButtonSettingsAdapter}
     *
     * @param mButtonSettings list with settings items.
     * @param viewModel       to react on rows selection.
     */
    public MButtonSettingsAdapter(@NonNull List<MButtonSettingsItem> mButtonSettings,
                                  @NonNull MButtonViewModel.Body viewModel) {
        this.mButtonSettings = mButtonSettings;
        this.viewModel = viewModel;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ListItemMButtonSettingsBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.list_item_m_button_settings,
                parent,
                false);

        return new MButtonSettingsAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.header.setText(mButtonSettings.get(position).getHeader());
        holder.description.setText(mButtonSettings.get(position).getDescription());

        holder.checkMark.setVisibility(selectedPosition == position ? View.VISIBLE : View.INVISIBLE);
        holder.checkMark.setAlpha(selectedPosition == position ? 1.0f : 0.0f);

        holder.binding.mButtonSettingsItemContainer.setOnClickListener(v -> {
            if (selectedPosition != position) {
                viewModel.onMButtonRowSelected(mButtonSettings.get(position).getType());
                animateVisibilityChange(holder.checkMark, View.VISIBLE);
                int previouslySelectedItem = selectedPosition;
                selectedPosition = position;
                notifyItemChanged(previouslySelectedItem, new Object());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mButtonSettings.size();
    }

    /**
     * Get currently selected position on the M-Button settings list.
     *
     * @return currently selected position as int (selected row is in range [0-2],
     * -1 when nothing yet selected)
     */
    public int getSelectedPosition() {
        return selectedPosition;
    }

    /**
     * Sets selected position and invalidates views.
     *
     * @param selectedPosition to be set.
     */
    public void setSelectedPosition(int selectedPosition) {
        notifyItemChanged(this.selectedPosition, new Object());
        this.selectedPosition = selectedPosition;
        notifyItemChanged(selectedPosition, new Object());
    }

    /**
     * Class that keeps view references for every row view object.
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        ListItemMButtonSettingsBinding binding;

        private final TextView header;
        private final TextView description;
        private final ImageView checkMark;

        /**
         * Constructor with binding reference.
         *
         * @param binding to keep track of all views.
         */
        ViewHolder(@NonNull final ListItemMButtonSettingsBinding binding) {
            super(binding.mButtonSettingsItemContainer);
            this.binding = binding;

            header = binding.mButtonItemHeader;
            description = binding.mButtonItemDescription;
            checkMark = binding.checkMark;
        }
    }
}

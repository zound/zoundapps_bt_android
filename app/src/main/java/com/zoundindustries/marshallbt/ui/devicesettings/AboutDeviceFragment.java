package com.zoundindustries.marshallbt.ui.devicesettings;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

import com.zoundindustries.marshallbt.databinding.FragmentAboutDeviceBinding;
import com.zoundindustries.marshallbt.model.ota.pojo.FirmwareFileMetaData;
import com.zoundindustries.marshallbt.ui.BaseFragment;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.ui.error.types.OtaDownloadingErrorActivity;
import com.zoundindustries.marshallbt.ui.error.types.OtaFlashingErrorActivity;
import com.zoundindustries.marshallbt.ui.error.types.OtaUndefinedErrorActivity;
import com.zoundindustries.marshallbt.viewmodels.devicesettings.AboutDeviceViewModel;
import com.zoundindustries.marshallbt.viewmodels.factories.devicesettings.AboutDeviceViewModelFactory;

import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_DEVICE_ID;
import static com.zoundindustries.marshallbt.utils.ViewExtrasGlobals.EXTRA_OTA_FIRMWARE_META_DATA;

/**
 * Class used to show information about the chosen device
 */
public class AboutDeviceFragment extends BaseFragment {

    public static final String EXTRA_SHOULD_RETRY_OTA = "about_activity_should_retry_ota";
    private static final String TAG = AboutDeviceFragment.class.getSimpleName();

    private AboutDeviceViewModel.Body viewModel;
    private String deviceId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();

        if (getActivity() != null) {
            if (arguments != null) {
                deviceId = arguments.getString(EXTRA_DEVICE_ID, "");
                boolean shouldRetryOta = arguments.getBoolean(EXTRA_SHOULD_RETRY_OTA, false);
                FirmwareFileMetaData firmwareFileMetaData =
                        arguments.getParcelable(EXTRA_OTA_FIRMWARE_META_DATA);

                AboutDeviceViewModelFactory viewModelFactory = new AboutDeviceViewModelFactory(
                        getActivity().getApplication(),
                        deviceId,
                        firmwareFileMetaData,
                        shouldRetryOta);

                viewModel = ViewModelProviders.of(this, viewModelFactory)
                        .get(AboutDeviceViewModel.Body.class);
            } else {
                getActivity().finish();
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        FragmentAboutDeviceBinding binding = FragmentAboutDeviceBinding.inflate(inflater);
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupBackButton(true);
        initObservers();
    }

    private void initObservers() {
        viewModel.outputs.isViewChanged().observe(this, viewType -> {
            if (getActivity() != null) {
                Context context = getContext();
                if (context != null) {
                    switch (viewType) {
                        case BACK:
                            performBackPressedOrFinish();
                            break;
                        case HOME_SCREEN:
                            if (getActivity() != null) {
                                getActivity().finish();
                            }
                            break;
                        case ERROR_OTA_DOWNLOAD:
                            startActivity(OtaDownloadingErrorActivity.create(context,
                                    deviceId, ViewType.ABOUT_DEVICE,
                                    viewModel.outputs.getFirmwareUpdateMetaData()));
                            break;
                        case ERROR_OTA_FLASHING:
                            startActivity(OtaFlashingErrorActivity.create(context,
                                    deviceId, ViewType.ABOUT_DEVICE,
                                    viewModel.outputs.getFirmwareUpdateMetaData()));
                            break;
                        case ERROR_OTA_UNDEFINED:
                            startActivity(OtaUndefinedErrorActivity.create(context,
                                    deviceId, ViewType.ABOUT_DEVICE,
                                    viewModel.outputs.getFirmwareUpdateMetaData()));
                            break;
                    }
                    getActivity().finish();
                }
            }
        });

        viewModel.outputs.getEnableBackFunctionality().observe(this,
                enableBackFunctionality -> {
                    setupToolbar(viewModel.getTitle(),
                            enableBackFunctionality ? View.VISIBLE : View.GONE, true);
                    setupBackButton(enableBackFunctionality);
                });
    }
}

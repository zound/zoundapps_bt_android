package com.zoundindustries.marshallbt.ui.homescreen;

import android.Manifest;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.databinding.ActivityHomeBinding;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.mainmenu.joplin.WebsiteLinkHolder;
import com.zoundindustries.marshallbt.ui.BaseToolbarActivity;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.ui.error.types.BluetoothErrorActivity;
import com.zoundindustries.marshallbt.ui.mainmenu.MainMenuActivity;
import com.zoundindustries.marshallbt.utils.SystemServicesUtils;
import com.zoundindustries.marshallbt.viewmodels.homescreen.HomeActivityViewModel;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Main Activity for visualizing device list discovery.
 * Handling all error states for discovery and invokes {@link DeviceListFragment}
 */
public class HomeActivity extends BaseToolbarActivity implements
        HasSupportFragmentInjector, EasyPermissions.PermissionCallbacks {

    public static final String FRAGMENT_ARGS_DEVICES_FOUND = "fragment_args_devices_found";

    private static final int APP_PERMISSIONS_ID = 1;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    private HomeActivityViewModel.Body homeActivityViewModel;

    private ActivityHomeBinding binding;

    private static final String[] permissionArray = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };

    /**
     * Encapsulates intent creation and setup of the Activity.
     *
     * @param context for the Activity start
     * @return configured starting Intent
     */
    @NonNull
    public static Intent create(@NonNull Context context) {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!isEachPermissionGranted()) {
            EasyPermissions.requestPermissions(this, getString(R.string.android_home_permission_storage),
                    APP_PERMISSIONS_ID, permissionArray);
        } else {
            startSetup();
        }
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    /**
     * Used for handling permissions granted result from {@link EasyPermissions}
     */
    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        startSetup();
    }

    /**
     * Used for handling permissions denied result from {@link EasyPermissions}
     */
    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        //nop
    }

    @Override
    public void onBackPressed() {
        if (binding != null) {
            View drawer = binding.navigationView;
            if (binding.drawerLayout.isDrawerOpen(drawer)) {
                binding.drawerLayout.closeDrawer(drawer);
            } else {
                super.onBackPressed();
            }
        }
    }

    private boolean isEachPermissionGranted() {
        return EasyPermissions.hasPermissions(this, permissionArray);
    }

    private void startSetup() {
        checkBluetoothAvailability();

        homeActivityViewModel = ViewModelProviders.of(this,
                viewModelFactory).get(HomeActivityViewModel.Body.class);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);

        initObservers();

        startFragment(new ScanForSpeakersFragment(), null);
    }

    private void checkBluetoothAvailability() {
        if (!SystemServicesUtils.isBluetoothEnabled()) {
            startActivity(new Intent(getApplicationContext(),
                    BluetoothErrorActivity.class));
        }
    }

    private void initObservers() {
        Observer<Boolean> deviceScanCompletedObserver = devicesFound -> {
            Bundle args = new Bundle();
            args.putBoolean(FRAGMENT_ARGS_DEVICES_FOUND, devicesFound);
            setupDrawer();
            startFragment(new DeviceListFragment(), args);
        };

        homeActivityViewModel.outputs.areHomeDevicesFound()
                .observe(this, deviceScanCompletedObserver);
    }

    private void setupDrawer() {
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this,
                binding.drawerLayout,
                (Toolbar) binding.scanToolbar,
                R.string.hamburger_menu_open_desc,
                R.string.hamburger_menu_close_desc);

        drawerToggle.setDrawerIndicatorEnabled(false);
        drawerToggle.setHomeAsUpIndicator(ResourcesCompat
                .getDrawable(getResources(),
                        R.drawable.menu,
                        this.getTheme()));
        drawerToggle.setToolbarNavigationClickListener(v ->
                binding.drawerLayout.openDrawer(GravityCompat.START));

        binding.navigationView.setItemIconTintList(null);
        binding.navigationView.setNavigationItemSelectedListener(menuItem ->
                handleDrawerActions(menuItem.getItemId()));
    }

    private boolean handleDrawerActions(int menuItemId) {
        Intent intent = new Intent();
        switch (menuItemId) {
            case R.id.hamburger_menu_settings_item:
                intent = new Intent(MainMenuActivity.create(this, ViewType.MAIN_MENU_SETTINGS));
                break;
            case R.id.hamburger_menu_help_item:
                intent = new Intent(MainMenuActivity.create(this, ViewType.MAIN_MENU_HELP));
                break;
            case R.id.hamburger_menu_shop_item:
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(WebsiteLinkHolder.MARSHALL_SHOP_URL));
                break;
            case R.id.hamburger_menu_about_item:
                intent = new Intent(MainMenuActivity.create(this, ViewType.MAIN_MENU_ABOUT));
                break;
        }
        startActivity(intent);
        binding.drawerLayout.closeDrawers();
        return false;
    }

    private void startFragment(@NonNull Fragment fragment, @Nullable Bundle arguments) {
        if (arguments != null) {
            fragment.setArguments(arguments);
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container,
                fragment).commit();
    }

    @Override
    public void setToolbarVisibility(Boolean isVisible) {
        binding.scanToolbar.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
    }


    @Override
    public void setDrawerBackgroundColor(int color) {
        binding.drawerContainer.setBackgroundColor(color);
    }
}
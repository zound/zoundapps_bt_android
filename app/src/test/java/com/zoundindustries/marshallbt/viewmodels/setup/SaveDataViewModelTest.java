package com.zoundindustries.marshallbt.viewmodels.setup;


import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.zoundindustries.marshallbt.TestUtils;
import com.zoundindustries.marshallbt.model.setup.SaveDataManager;
import com.zoundindustries.marshallbt.utils.LiveDataHelper;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import io.reactivex.subjects.BehaviorSubject;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class SaveDataViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private SaveDataManager saveDataManager = mock(SaveDataManager.class);

    @Before
    public void setup() {
        TestUtils.setupRx();
    }



    @Test
    public void testSavingData() {
        BehaviorSubject<Boolean> testSubject = BehaviorSubject.create();
        when(saveDataManager.saveData()).thenReturn(testSubject);
        SaveDataViewModel.Body viewModel = new SaveDataViewModel.Body(saveDataManager);

        testSubject.onNext(true);
        assertTrue(LiveDataHelper.unboxBoolean(viewModel.isViewCompleted()));
    }
}



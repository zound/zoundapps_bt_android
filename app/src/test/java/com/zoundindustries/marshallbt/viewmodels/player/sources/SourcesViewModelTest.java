package com.zoundindustries.marshallbt.viewmodels.player.sources;


import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.player.sources.SourceFeatures;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;
import com.zoundindustries.marshallbt.viewmodels.BaseJoplinSdkViewModelTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class SourcesViewModelTest extends BaseJoplinSdkViewModelTest {

    private final SourceFeatures sourceFeatures = new SourceFeatures(true,
            true ,true);

    private SourcesViewModel.Body viewModel;
    
    @Before
    public void setup() {
        baseSetup();

        viewModel = new SourcesViewModel.Body(mockApplication, deviceId, mockDeviceManager);
        when(mockBaseStateController.isFeatureSupported(FeaturesDefs.BLUETOOTH_SOURCE)).
                thenReturn(true);
        when(mockBaseStateController.isFeatureSupported(FeaturesDefs.RCA_SOURCE)).thenReturn(true);
        when(mockBaseStateController.isFeatureSupported(FeaturesDefs.AUX_SOURCE)).thenReturn(true);

        testConnectionSubject.onNext(true);
        testConnectedSubject.onNext(BaseDevice.ConnectionState.DISCONNECTED);
    }

    @Test
    public void getFeaturesTest() {
        assertEquals(sourceFeatures, viewModel.getSourceFeatures().getValue());
    }
}

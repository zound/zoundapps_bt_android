package com.zoundindustries.marshallbt.viewmodels.setup;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import android.view.View;

import com.zoundindustries.marshallbt.utils.LiveDataHelper;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

@RunWith(JUnit4.class)
public class TacViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private TacViewModel.Body viewModel;
    private View mockView;

    @Before
    public void setup() {
        viewModel = new TacViewModel.Body();
        mockView = mock(View.class);
    }

    @Test
    public void testBackButtonTapped() {
        viewModel.inputs.backTapped(mockView);
        assertTrue(LiveDataHelper.unboxBoolean(viewModel.outputs.getBack()));
    }

    @Test
    public void testResetViewTransitionVariables() {
        viewModel.inputs.resetViewTransitionVariables();
        assertFalse(LiveDataHelper.unboxBoolean(viewModel.outputs.getBack()));
    }

    @Test
    public void testScrollViewReachedBottom() {
        viewModel.inputs.scrollViewBottomReached(true);
        assertTrue(View.GONE == LiveDataHelper.unboxInteger(viewModel.outputs.getFooterVisibility()));
    }

    @Test
    public void testScrollViewNotReachedBottom() {
        viewModel.inputs.scrollViewBottomReached(false);
        assertTrue(View.VISIBLE == LiveDataHelper.unboxInteger(viewModel.outputs.getFooterVisibility()));
    }
}

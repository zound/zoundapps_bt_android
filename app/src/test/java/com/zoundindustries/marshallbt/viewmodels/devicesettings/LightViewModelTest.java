package com.zoundindustries.marshallbt.viewmodels.devicesettings;

import android.app.Application;
import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;
import androidx.appcompat.widget.AppCompatSeekBar;

import com.zoundindustries.marshallbt.TestUtils;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice;
import com.zoundindustries.marshallbt.model.device.state.BaseDeviceStateController;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.ui.ViewFlowController;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import io.reactivex.subjects.BehaviorSubject;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class LightViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private AppCompatSeekBar mockSeekBar;
    private Application mockApp;
    private DeviceManager mockDeviceManager;
    private TymphanyDevice mockSpeaker;
    private DeviceInfo mockDeviceInfo;

    private LightViewModel.Body lightViewModel;
    private BehaviorSubject<Boolean> testDeviceConnectionSubject = BehaviorSubject.create();
    private BaseDeviceStateController mockBaseStateController;
    private BehaviorSubject<Integer> testLightSubject = BehaviorSubject.create();

    @Before
    public void setup() {
        TestUtils.setupRx();

        mockApp = mock(Application.class);
        mockSeekBar = mock(AppCompatSeekBar.class);
        mockSpeaker = mock(TymphanyDevice.class);
        mockDeviceInfo = mock(DeviceInfo.class);
        mockDeviceManager = mock(DeviceManager.class);
        mockBaseStateController = mock(BaseDeviceStateController.class);
        BaseDeviceStateController.Inputs mockStateControllerInputs = mock(BaseDeviceStateController.Inputs.class);
        BaseDeviceStateController.Outputs mockStateControllerOutputs = mock(BaseDeviceStateController.Outputs.class);

        mockBaseStateController.inputs = mockStateControllerInputs;
        mockBaseStateController.outputs = mockStateControllerOutputs;

        when(mockDeviceManager.isServiceReady()).thenReturn(testDeviceConnectionSubject);
        when(mockDeviceManager.getDeviceFromId(any(String.class))).thenReturn(mockSpeaker);
        when(mockSpeaker.getBaseDeviceStateController()).thenReturn(mockBaseStateController);
        when(mockSpeaker.getDeviceInfo()).thenReturn(mockDeviceInfo);
        when(mockDeviceInfo.getId()).thenReturn("12345");
        when(mockStateControllerOutputs.getBrightness()).thenReturn(testLightSubject);

        lightViewModel = new LightViewModel.Body(mockApp, mockDeviceInfo.getId(),
                mockDeviceManager, mock(FirebaseAnalyticsManager.class));
    }

    @Test
    public void lightSeekBarProgressChangedTest() {
        int progress = 50;

        lightViewModel.inputs.lightSeekBarProgressChanged(mockSeekBar, progress, true);

        assertEquals(lightViewModel.outputs.getBrightnessProgress().getValue().intValue(),progress);
    }

    @Test
    public void testIsViewChanged() {
        MutableLiveData<ViewFlowController.ViewType> testObservable =
                lightViewModel.outputs.isViewChanged();
        BehaviorSubject<BaseDevice.ConnectionState> testConnectedSubject = BehaviorSubject.create();

        when(mockBaseStateController.isFeatureSupported(FeaturesDefs.CONNECTION_INFO))
                .thenReturn(true);
        when(mockBaseStateController.outputs.getConnectionInfo()).thenReturn(testConnectedSubject);

        testConnectedSubject.onNext(BaseDevice.ConnectionState.DISCONNECTED);
        testDeviceConnectionSubject.onNext(true);

        assertTrue(testObservable.getValue() == ViewFlowController.ViewType.HOME_SCREEN);
    }
}

package com.zoundindustries.marshallbt.viewmodels.error;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import android.view.View;

import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;

@RunWith(JUnit4.class)
public class ErrorViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private ErrorViewModel.Body errorViewModel;

    private View mockView;

    @Before
    public void setup() {
        mockView = mock(View.class);
        errorViewModel = new ErrorViewModel.Body(ViewType.ERROR_OTA_FLASHING, ViewType.UNKNOWN);
    }

    @Test
    public void testBackButtonClicked() {
        errorViewModel.outputs.isViewChanged();
        errorViewModel.inputs.backButtonClicked(mockView);

        assertTrue(errorViewModel.isViewChanged().getValue() == ViewType.HOME_SCREEN);
    }

    @Test
    public void testActionButtonClicked() {
        errorViewModel.inputs.actionButtonClicked(mockView);
        //todo filled with logic when the method is implemented
    }
}

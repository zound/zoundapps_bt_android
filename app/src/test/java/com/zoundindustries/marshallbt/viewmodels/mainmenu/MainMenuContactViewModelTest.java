package com.zoundindustries.marshallbt.viewmodels.mainmenu;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.zoundindustries.marshallbt.ui.ViewFlowController;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

public class MainMenuContactViewModelTest {

    private MainMenuContactViewModel.Body viewModel;

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setUp() {
        viewModel = new MainMenuContactViewModel.Body();
    }

    @Test
    public void testOnGoToWebsiteButtonClicked() {
        viewModel.inputs.onGoToWebsiteButtonClicked(null);
        assertTrue(viewModel.outputs.isViewChanged().getValue() == ViewFlowController.ViewType.MAIN_MENU_CONTACT_WEBSITE);
    }

    @Test
    public void testOnContactSupportButtonClicked() {
        viewModel.inputs.onContactSupportButtonClicked(null);
        assertTrue(viewModel.outputs.isViewChanged().getValue() == ViewFlowController.ViewType.MAIN_MENU_CONTACT_SUPPORT);
    }
}
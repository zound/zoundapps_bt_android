package com.zoundindustries.marshallbt.viewmodels.mainmenu;

import android.app.Application;
import android.content.res.Resources;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.zoundindustries.marshallbt.BuildConfig;
import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.mainmenu.MainMenuItem;
import com.zoundindustries.marshallbt.model.mainmenu.MainMenuItem.MenuItemType;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MainMenuViewModelTest {

    private MainMenuViewModel.Body viewModel;
    private Application mockApplication;
    private Resources mockResources;

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setUp() {
        mockApplication = mock(Application.class);
        mockResources = mock(Resources.class);
        viewModel = createViewModel(ViewType.MAIN_MENU_SETTINGS);
    }

    /**
     * This test is checking for correct displayed name for fragment, based on what item has been chosen
     * It also check what ViewType is passed as a consequence of that click
     */
    @Test
    public void testOnMainMenuItemClickedAndIsViewChanged() {
        for (MenuItemType type : MenuItemType.values()) {
            //checking if new model is needed -> if new hamburger menu category, then we have to
            // create new viewmodel, as in constructor we pass argument
            createViewModelIfNeeded(type);
            //click on menu item
            viewModel.inputs.onMainMenuItemClicked(type, true);
            //checking the display name of viewmodel
            int resourceNameId = viewModel.outputs.getToolbarDisplayTextResourceId().getValue();
            assertEquals(getCorrectResourceId(type), resourceNameId);
            ViewType viewType = viewModel.outputs.isViewChanged().getValue();
            assertEquals(getCorrectViewType(type), viewType);
        }
    }

    //view model is need in case of changing category of hamburger
    //we start with settings in @Before, so no need to handle that creation
    //NOTE: We don't need case for shop (and is unavailable) as it is separate activity
    //NOTE: because it is used in iteration of MenuItemType
    // in {@link MainMenuViewModelTest#testOnMainMenuItemClickedAndIsViewChanged}
    // it is important to check order of items in enum and it's better not to change it
    private void createViewModelIfNeeded(MenuItemType type) {
        switch (type) {
            case CONTACT:
                viewModel = createViewModel(ViewType.MAIN_MENU_HELP);
                break;
            case END_USER_LICENSE_AGREEMENT:
                viewModel = createViewModel(ViewType.MAIN_MENU_ABOUT);
                break;
            case EMAIL_SUBSCRIPTION:
                //we start with settings in @Before, so no need to handle that creation
                break;
        }
    }

    private int getCorrectResourceId(MenuItemType type) {
        int resId = 0;
        switch (type) {
            case QUICK_GUIDE_ACTON_II:
            case QUICK_GUIDE_STANMORE_II:
            case QUICK_GUIDE_WOBURN_II:
            case QUICK_GUIDE_MONITOR_II:
            case QUICK_GUIDE:
                resId = R.string.main_menu_item_quick_guide_uc;
                break;
            case PLAY_PAUSE_BUTTON:
                resId = R.string.main_menu_item_play_pause_button_uc;
                break;
            case COUPLE_SPEAKERS:
                resId = R.string.main_menu_item_couple_speakers;
                break;
            case ANC:
                resId = R.string.main_menu_item_anc_button_uc;
                break;
            case M_BUTTON:
                resId = R.string.main_menu_item_m_button_uc;
                break;
            case CONTROL_KNOB:
                resId = R.string.main_menu_item_control_knob_uc;
                break;
            case BLUETOOTH_PAIRING_JOPLIN:
            case BLUETOOTH_PAIRING_OZZY:
                resId = R.string.main_menu_item_bluetooth_pairing_uc;
                break;
            case MAIN_MENU_HELP:
            case CONTACT:
                resId = R.string.main_menu_item_help_uc;
                break;
            case ONLINE_MANUAL:
            case ONLINE_MANUAL_ACTON:
            case ONLINE_MANUAL_WOBURN:
            case ONLINE_MANUAL_STANMORE:
            case ONLINE_MANUAL_MONITOR_II:
                resId = R.string.main_menu_item_online_manual_uc;
                break;
            case ANALYTICS:
            case EMAIL_SUBSCRIPTION:
                resId = R.string.main_menu_item_settings_uc;
                break;
            case END_USER_LICENSE_AGREEMENT:
            case FREE_AND_OPEN_SOURCE_SOFTWARE:
                resId = R.string.appwide_about_uc;
                break;
            case ROOT:
                resId = R.string.main_menu_item_settings_uc;
                break;
        }
        return resId;
    }

    private ViewType getCorrectViewType(MenuItemType menuItemType) {
        ViewType viewType = null;
        switch (menuItemType) {
            case QUICK_GUIDE:
                viewType = ViewType.MAIN_MENU_QUICK_GUIDE_LIST;
                break;
            case PLAY_PAUSE_BUTTON:
                viewType = ViewType.MAIN_MENU_QUICK_GUIDE_JOPLIN_PLAY_PAUSE_BUTTON;
                break;
            case COUPLE_SPEAKERS:
                viewType = ViewType.MAIN_MENU_QUICK_GUIDE_JOPLIN_COUPLE_SPEAKERS;
                break;
            case BLUETOOTH_PAIRING_JOPLIN:
                viewType = ViewType.MAIN_MENU_QUICK_GUIDE_JOPLIN_BLUETOOTH_PAIRING;
                break;
            case BLUETOOTH_PAIRING_OZZY:
                viewType = ViewType.MAIN_MENU_QUICK_GUIDE_OZZY_BLUETOOTH_PAIRING;
                break;
            case ANC:
                viewType = ViewType.MAIN_MENU_QUICK_GUIDE_OZZY_ANC;
                break;
            case M_BUTTON:
                viewType = ViewType.MAIN_MENU_QUICK_GUIDE_OZZY_M_BUTTON;
                break;
            case CONTROL_KNOB:
                viewType = ViewType.MAIN_MENU_QUICK_GUIDE_OZZY_CONTROL_KNOB;
                break;
            case QUICK_GUIDE_ACTON_II:
            case QUICK_GUIDE_STANMORE_II:
            case QUICK_GUIDE_WOBURN_II:
                viewType = ViewType.MAIN_MENU_QUICK_GUIDE_JOPLIN;
                break;
            case QUICK_GUIDE_MONITOR_II:
                viewType = ViewType.MAIN_MENU_QUICK_GUIDE_OZZY;
                break;
            case MAIN_MENU_HELP:
                viewType = ViewType.MAIN_MENU_HELP;
                break;
            case CONTACT:
                viewType = ViewType.MAIN_MENU_CONTACT;
                break;
            case ONLINE_MANUAL:
                viewType = ViewType.MAIN_MENU_ONLINE_MANUAL;
                break;
            case ONLINE_MANUAL_ACTON:
            case ONLINE_MANUAL_STANMORE:
            case ONLINE_MANUAL_WOBURN:
            case ONLINE_MANUAL_MONITOR_II:
                viewType = ViewType.MAIN_MENU_ONLINE_MANUAL_DEVICE;
                break;
            case ANALYTICS:
                viewType = ViewType.MAIN_MENU_ANALYTICS;
                break;
            case EMAIL_SUBSCRIPTION:
                viewType = ViewType.MAIN_MENU_EMAIL_SUBSCRIPTION;
                break;
            case END_USER_LICENSE_AGREEMENT:
                viewType = ViewType.MAIN_MENU_EULA;
                break;
            case FREE_AND_OPEN_SOURCE_SOFTWARE:
                viewType = ViewType.MAIN_MENU_FOSS;
                break;
        }
        return viewType;
    }

    /**
     * setting appVersionText is done in {@link MainMenuViewModel.Body#initStates(Resources)}
     * which is invoked in constructor because of that we have to create constructor for each
     * hamburger menu item
     */
    @Test
    public void testGetAppVersionText() {
        //appVersionText should have no data -> null
        assertNull(viewModel.outputs.getAppVersionText().getValue());

        viewModel = createViewModel(ViewType.MAIN_MENU_SETTINGS);
        assertNull(viewModel.outputs.getAppVersionText().getValue());

        viewModel = createViewModel(ViewType.MAIN_MENU_HELP);
        assertNull(viewModel.outputs.getAppVersionText().getValue());

        //app version should have value only in case of 'About' item
        String versionTestString = "Test version String";
        when(mockResources.getString(R.string.main_menu_about_name,
                BuildConfig.VERSION_NAME)).thenReturn(versionTestString);
        viewModel = createViewModel(ViewType.MAIN_MENU_ABOUT);
        assertNotNull(viewModel.outputs.getAppVersionText().getValue());
    }

    /**
     * setting item list is done in {@link MainMenuViewModel.Body#initStates(Resources)}
     * and in {@link MainMenuViewModel.Body#onMainMenuItemClicked(MenuItemType)}
     * (but only for 2 cases)
     * which is invoked in constructor because of that we have to create constructor for each
     * hamburger menu item
     */
    @Test
    public void testGetMainMenuItem_Settings() {
        String subscription = "main_menu_settings_email_subscription";
        String analytics = "main_menu_settings_analytics";
        when(mockResources.getString(R.string.main_menu_item_email_subscription))
                .thenReturn(subscription);
        when(mockResources.getString(R.string.main_menu_item_analytics))
                .thenReturn(analytics);
        checkIfViewModelContainsCorrectItems(new String[]{subscription, analytics},
                ViewType.MAIN_MENU_SETTINGS);
    }

    @Test
    public void testGetMainMenuItem_Help() {
        String guide = "main_menu_help_quick_guide";
        String manual = "main_menu_help_online_manual";
        String contact = "main_menu_help_contact";
        when(mockResources.getString(R.string.main_menu_item_quick_guide)).thenReturn(guide);
        when(mockResources.getString(R.string.main_menu_item_online_manual)).thenReturn(manual);
        when(mockResources.getString(R.string.main_menu_item_contact)).thenReturn(contact);
        checkIfViewModelContainsCorrectItems(new String[]{guide, manual, contact},
                ViewType.MAIN_MENU_HELP);
    }

    @Test
    public void testGetMainMenuItem_About() {
        String licence = "main_menu_about_end_user_licence_agreement";
        String foss = "main_menu_about_free_and_open_source_software";
        when(mockResources.getString(
                R.string.main_menu_item_terms_and_conditions))
                .thenReturn(licence);
        when(mockResources.getString(
                R.string.main_menu_item_foss))
                .thenReturn(foss);
        checkIfViewModelContainsCorrectItems(new String[]{licence, foss},
                ViewType.MAIN_MENU_ABOUT);
    }

    @Test
    public void testGetMainMenuItem_QuickGuide() {
        String bluetoothPairing = "main_menu_help_quick_guide_bluetooth_pairing";
        String coupleSpeakers = "main_menu_help_quick_guide_couple_speakers";
        String playPauseButton = "main_menu_help_quick_guide_play_pause_button";
        when(mockResources.getString(R.string.main_menu_item_bluetooth_pairing))
                .thenReturn(bluetoothPairing);
        when(mockResources.getString(R.string.main_menu_item_couple_speakers))
                .thenReturn(coupleSpeakers);
        when(mockResources.getString(R.string.main_menu_item_play_pause_button))
                .thenReturn(playPauseButton);
        viewModel = createViewModel(ViewType.MAIN_MENU_SETTINGS);
        viewModel.onMainMenuItemClicked(MenuItemType.QUICK_GUIDE_ACTON_II, true);
        compareLists(new String[]{bluetoothPairing, coupleSpeakers, playPauseButton},
                viewModel.getMainMenuItemList().getValue());
    }

    @Test
    public void testGetMainMenuItem_OnlineManual() {
        String manual_acton = "main_menu_help_online_manual_acton";
        String manual_stanmore = "main_menu_help_online_manual_stanmore";
        String manual_woburn = "main_menu_help_online_manual_woburn";
        String manual_monitor = "main_menu_help_online_manual_monitor";
        when(mockResources.getString(R.string.device_model_acton))
                .thenReturn(manual_acton);
        when(mockResources.getString(R.string.device_model_stanmore))
                .thenReturn(manual_stanmore);
        when(mockResources.getString(R.string.device_model_woburn))
                .thenReturn(manual_woburn);
        when(mockResources.getString(R.string.device_model_monitor_ii))
                .thenReturn(manual_woburn);
        viewModel = createViewModel(ViewType.MAIN_MENU_ONLINE_MANUAL);
        viewModel.onMainMenuItemClicked(MenuItemType.ONLINE_MANUAL, true);
        compareLists(new String[]{manual_acton, manual_stanmore, manual_woburn, manual_monitor},
                viewModel.getMainMenuItemList().getValue());
    }

    private void checkIfViewModelContainsCorrectItems(String[] expectedList, ViewType viewType) {
        List<MainMenuItem> menuItemsList =
                getMainMenuItemListFromViewModel(viewType);
        compareLists(expectedList, menuItemsList);
    }

    private void compareLists(String[] expectedList, List<MainMenuItem> viewModelList) {
        List<String> mainMenuItemListNames = getMockMainMenuItemList(expectedList);
        if (viewModelList != null) {
            for (MainMenuItem item : viewModelList) {
                assertTrue(mainMenuItemListNames.contains(item.getName()));
            }
        }
    }

    private List<String> getMockMainMenuItemList(String[] itemsNameArray) {
        return new ArrayList<>(Arrays.asList(itemsNameArray));
    }

    private List<MainMenuItem> getMainMenuItemListFromViewModel(ViewType viewType) {
        viewModel = createViewModel(viewType);
        return viewModel.outputs.getMainMenuItemList().getValue();
    }

    private MainMenuViewModel.Body createViewModel(ViewType viewType) {
        return new MainMenuViewModel.Body(mockApplication, viewType, mockResources);
    }
}
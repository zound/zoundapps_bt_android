package com.zoundindustries.marshallbt.viewmodels.devicesettings;

import androidx.lifecycle.MutableLiveData;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.devicesettings.EQData;
import com.zoundindustries.marshallbt.model.devicesettings.EQPreset;
import com.zoundindustries.marshallbt.utils.CommonPreferences;
import com.zoundindustries.marshallbt.utils.EqPresetType;
import com.zoundindustries.marshallbt.utils.joplin.JoplinEQPresetListManager;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.ui.ViewFlowController;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;
import com.zoundindustries.marshallbt.viewmodels.BaseJoplinSdkViewModelTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import io.reactivex.subjects.BehaviorSubject;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class EQViewModelTest extends BaseJoplinSdkViewModelTest {

    private CommonPreferences mockPreferences = mock(CommonPreferences.class);

    private BehaviorSubject<EQData> testEqSubject = BehaviorSubject.create();
    private BehaviorSubject<BaseDevice.ConnectionState> testConnectedSubject =
            BehaviorSubject.create();
    private EQViewModel.Body viewModel;
    private JoplinEQPresetListManager joplinEQPresetListManager;

    private int presetNo = 2;

    @Before
    public void setup() {
        baseSetup();

        FirebaseAnalyticsManager mockFirebaseAnalyticsManager = mock(FirebaseAnalyticsManager.class);

        doNothing().when(mockBaseStateController.inputs).setEQData(any(EQData.class));

        when(mockBaseStateController.isFeatureSupported(FeaturesDefs.CONNECTION_INFO))
                .thenReturn(true);

        when(mockBaseStateController.outputs.getEQ()).thenReturn(testEqSubject);
        when(mockBaseStateController.outputs.getConnectionInfo()).thenReturn(testConnectedSubject);
        when(mockDeviceManager.getEqualizerPresetType(any(BaseDevice.class), any()))
                .thenReturn(EqPresetType.ROCK);
        when(mockDeviceManager.getIndexOfEqualizerPreset(any(BaseDevice.class), any())).thenReturn(presetNo);

        viewModel = new EQViewModel.Body(mockApplication, deviceId, mockDeviceManager,
                mockFirebaseAnalyticsManager);

        joplinEQPresetListManager = new JoplinEQPresetListManager(mockPreferences);

        testConnectionSubject.onNext(true);
        testConnectedSubject.onNext(BaseDevice.ConnectionState.DISCONNECTED);
    }

    // this test works only when all tests are run, when running only this class it fails.
    // testEQSubject returns null if it is run by single
    @Test
    public void testSpinnerSelectionOutput() {
        MutableLiveData<Integer> testObservable = viewModel.outputs.getSpinnerSelection();
        testEqSubject.onNext(joplinEQPresetListManager.getPresetByIndex(presetNo).getEqData());

        assertEquals((Integer) presetNo, testObservable.getValue());
    }

    // OnSpinnerItemSelected input is run with a handler (it doesn't work in unit tests),
    // but only setPredefinedEQPreset() is invoked inside, so this is tested inside
    @Test
    public void testOnSpinnerItemSelected() {

        when(mockDeviceManager.getEqualizerPresetTypes(mockDevice)).thenReturn(
                joplinEQPresetListManager.getPresetTypes());
        when(mockDeviceManager.getEqualizerPresetByIndex(mockDevice, presetNo)).thenReturn(
                joplinEQPresetListManager.getPresetByIndex(presetNo));

        // set any preset on the beginning to avoid nullpointers
        viewModel.setPredefinedEQPreset(presetNo);

        // simulate preset selection on spinner
        MutableLiveData<EQPreset> testObservable = viewModel.outputs.getEqPreset();

        assertEquals(joplinEQPresetListManager.getPresetByIndex(presetNo),
                testObservable.getValue());

    }

    @Test
    public void testOnEqPresetUpdatedByUser() {

        // set some preset
        viewModel.inputs.onSpinnerItemSelected(presetNo);

        MutableLiveData<EQPreset> testObservable = viewModel.outputs.getEqPreset();

        testObservable.setValue(joplinEQPresetListManager.getPresetByIndex(presetNo));
        assertEquals(testObservable.getValue(), joplinEQPresetListManager.getPresetByIndex(presetNo));

        // change a value
        int value = 7;
        viewModel.inputs.eqValueUpdatedByUser(EQData.ValueType.BASS, value);
        assertEquals(value, testObservable.getValue().getEqData().getBass());
    }


    @Test
    public void testIsViewChanged() {
        MutableLiveData<ViewFlowController.ViewType> testObservable =
                viewModel.outputs.isViewChanged();

        assertTrue(testObservable.getValue() == ViewFlowController.ViewType.HOME_SCREEN);
    }

}

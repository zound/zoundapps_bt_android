package com.zoundindustries.marshallbt.viewmodels;

import android.app.Application;
import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import android.content.res.Resources;
import android.view.View;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.TestUtils;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.device.tymphany.SpeakerInfo;
import com.zoundindustries.marshallbt.model.device.state.BaseDeviceStateController;
import com.zoundindustries.marshallbt.model.devicesettings.CouplingManager;

import org.junit.Rule;

import io.reactivex.subjects.BehaviorSubject;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Does some inits for testing and mocks couple of objects used for testing:
 *
 *      * connection subject
 *      * device manager
 *      * base device state controller
 *      * application
 *      * view
 */
public abstract class BaseJoplinSdkViewModelTest {
    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    protected BehaviorSubject<Boolean> testConnectionSubject = BehaviorSubject.create();

    protected BehaviorSubject<BaseDevice.ConnectionState> testConnectedSubject =
            BehaviorSubject.create();

    protected BaseDeviceStateController mockBaseStateController;
    protected BaseDeviceStateController mockBaseStateController2;
    protected DeviceManager mockDeviceManager;
    protected CouplingManager mockCouplingManager;

    protected Application mockApplication;
    protected View mockView;
    protected String deviceId = "123456789";
    protected String deviceId2 = "987654321";
    protected BaseDevice mockDevice;
    protected BaseDevice mockDevice2;
    protected SpeakerInfo mockSpeakerinfo;
    protected SpeakerInfo mockSpeakerinfo2;

    protected void baseSetup() {
        TestUtils.setupRx();

        String deviceName = "testName";
        String deviceName2 = "testName2";

        mockView = mock(View.class);

        mockApplication = mock(Application.class);
        mockDeviceManager = mock(DeviceManager.class);
        mockCouplingManager = mock(CouplingManager.class);
        mockDevice = mock(BaseDevice.class);
        mockDevice2 = mock(BaseDevice.class);
        mockSpeakerinfo = mock(SpeakerInfo.class);
        mockSpeakerinfo2 = mock(SpeakerInfo.class);
        DeviceInfo mockDeviceInfo = mock(DeviceInfo.class);
        DeviceInfo mockDeviceInfo2 = mock(DeviceInfo.class);
        Resources mockResources = mock(Resources.class);


        mockBaseStateController = mock(BaseDeviceStateController.class);
        mockBaseStateController2 = mock(BaseDeviceStateController.class);

        BaseDeviceStateController.Inputs mockStateControllerInputs =
                mock(BaseDeviceStateController.Inputs.class);
        BaseDeviceStateController.Outputs mockStateControllerOutputs =
                mock(BaseDeviceStateController.Outputs.class);
        BaseDeviceStateController.Inputs mockStateControllerInputs2 =
                mock(BaseDeviceStateController.Inputs.class);
        BaseDeviceStateController.Outputs mockStateControllerOutputs2 =
                mock(BaseDeviceStateController.Outputs.class);

        mockBaseStateController.inputs = mockStateControllerInputs;
        mockBaseStateController.outputs = mockStateControllerOutputs;

        mockBaseStateController2.inputs = mockStateControllerInputs2;
        mockBaseStateController2.outputs = mockStateControllerOutputs2;

        // resources
        when(mockApplication.getResources()).thenReturn(mockResources);
        when(mockResources.getString(R.string.device_settings_menu_item_about_speaker_uc)).thenReturn("testName");

        // managers
        when(mockDeviceManager.isServiceReady()).thenReturn(testConnectionSubject);
        when(mockDeviceManager.getDeviceFromId(deviceId)).thenReturn(mockDevice);
        when(mockDeviceManager.getDeviceFromId(deviceId2)).thenReturn(mockDevice2);

        when(mockCouplingManager.isServiceReady()).thenReturn(testConnectionSubject);

        // device info
        when(mockDeviceInfo.getId()).thenReturn(deviceId);
        when(mockDevice.getDeviceInfo()).thenReturn(mockDeviceInfo);

        //device info2
        when(mockDeviceInfo2.getId()).thenReturn(deviceId2);
        when(mockDevice2.getDeviceInfo()).thenReturn(mockDeviceInfo2);

        // controller
        when(mockDevice.getBaseDeviceStateController()).thenReturn(mockBaseStateController);

        //controller2
        when(mockDevice2.getBaseDeviceStateController()).thenReturn(mockBaseStateController2);

        // speaker info1
        when(mockBaseStateController.outputs.getSpeakerInfoCurrentValue()).thenReturn(mockSpeakerinfo);


        when(mockBaseStateController.outputs.getConnectionInfo()).thenReturn(testConnectedSubject);

        // speaker info2
        when(mockBaseStateController2.outputs.getSpeakerInfoCurrentValue()).thenReturn(mockSpeakerinfo2);

        //device name1
        when(mockBaseStateController.outputs.getSpeakerInfoCurrentValue()
                .getDeviceName()).thenReturn(deviceName);

        //device name2
        when(mockBaseStateController2.outputs.getSpeakerInfoCurrentValue()
                .getDeviceName()).thenReturn(deviceName2);

    }
}

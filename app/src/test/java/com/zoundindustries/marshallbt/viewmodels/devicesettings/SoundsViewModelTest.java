package com.zoundindustries.marshallbt.viewmodels.devicesettings;

import com.zoundindustries.marshallbt.model.device.BaseDevice.ConnectionState;
import com.zoundindustries.marshallbt.model.device.SoundNotificationInfo;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.viewmodels.BaseJoplinSdkViewModelTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import io.reactivex.subjects.BehaviorSubject;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class SoundsViewModelTest extends BaseJoplinSdkViewModelTest {

    private SoundsViewModel.Body soundsViewModel;
    private BehaviorSubject<ConnectionState> testDeviceConnectionInfoSubject =
            BehaviorSubject.create();

    @Before
    public void setup() {
        super.baseSetup();

        doNothing()
                .when(mockBaseStateController.inputs)
                .setSoundNotification(any(SoundNotificationInfo.class));

        when(mockBaseStateController.outputs.getConnectionInfo())
                .thenReturn(testDeviceConnectionInfoSubject);

        soundsViewModel = new SoundsViewModel.Body(mockApplication, deviceId, mockDeviceManager);

        testConnectionSubject.onNext(true);
    }

    @Test
    public void testOnPowerSwitched() {

        //Simulate user turns on Power Sound Notifications
        soundsViewModel.onPowerSwitched(mockView, true);

        assertEquals(soundsViewModel.isPowerSoundEnabled().getValue(), true);

        verify(mockBaseStateController.inputs, times(1))
                .setSoundNotification(any(SoundNotificationInfo.class));

        //Simulate user turns off Media Control Sound Notifications
        soundsViewModel.onPowerSwitched(mockView, false);

        assertEquals(soundsViewModel.isPowerSoundEnabled().getValue(), false);
    }

    @Test
    public void testOnMediaControlSwitched() {

        //Simulate user turns on Power Sound Notifications
        soundsViewModel.onMediaControlSwitched(mockView, true);

        assertEquals(soundsViewModel.isMediaControlSoundEnabled().getValue(), true);

        verify(mockBaseStateController.inputs, times(1))
                .setSoundNotification(any(SoundNotificationInfo.class));

        //Simulate user turns off Media Control Sound Notifications
        soundsViewModel.onMediaControlSwitched(mockView, false);

        assertEquals(soundsViewModel.isPowerSoundEnabled().getValue(), false);
    }

    @Test
    public void testOnViewChanged() {

        //Simulate disconnection
        testDeviceConnectionInfoSubject.onNext(ConnectionState.DISCONNECTED);

        assertEquals(soundsViewModel.isViewChanged().getValue(), ViewType.HOME_SCREEN);
    }
}

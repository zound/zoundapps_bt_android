package com.zoundindustries.marshallbt.viewmodels.ota;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import android.view.View;

import com.zoundindustries.marshallbt.model.ota.OTAManager;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.viewmodels.BaseJoplinSdkViewModelTest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;

@SuppressWarnings("unchecked")
@RunWith(JUnit4.class)
public class OTACompletedViewModelTest extends BaseJoplinSdkViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private OTACompletedViewModel.Body otaCompletedViewModel;
    private OTAManager mockOtaManager;
    private View mockView;

    @Before
    public void setup() {
        baseSetup();

        mockView = mock(View.class);
        mockOtaManager = mock(OTAManager.class);

        otaCompletedViewModel =
                new OTACompletedViewModel.Body(
                        mockDevice.getDeviceInfo().getId(),
                        mockDeviceManager,
                        mockOtaManager);

        testConnectionSubject.onNext(true);
    }

    @Test
    public void testOnNextButtonClicked() {
        otaCompletedViewModel.inputs.onNextButtonClicked(mockView);
        assertEquals(ViewType.BACK, otaCompletedViewModel.outputs.isViewChanged().getValue());

    }
}

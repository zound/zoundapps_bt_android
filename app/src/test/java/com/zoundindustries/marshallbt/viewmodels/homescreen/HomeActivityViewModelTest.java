package com.zoundindustries.marshallbt.viewmodels.homescreen;

import android.app.Application;
import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.zoundindustries.marshallbt.TestUtils;
import com.zoundindustries.marshallbt.model.adapters.mock.MockSDK;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.device.mock.MockSpeaker;
import com.zoundindustries.marshallbt.model.discovery.DeviceDiscoveryManager;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;

import io.reactivex.subjects.BehaviorSubject;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class HomeActivityViewModelTest {


    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private HomeActivityViewModel.Body homActivityViewModel;
    private BehaviorSubject<Boolean> testConnection;
    private BehaviorSubject<Boolean> testServiceReady;

    private Application mockApplication;
    private MockSDK mockSDK;
    private DeviceDiscoveryManager mockSpeakersDiscoveryManager;
    private DeviceManager mockDeviceManager;

    @Before
    public void setup() {
        TestUtils.setupRx();

        mockApplication = mock(Application.class);
        mockSpeakersDiscoveryManager = mock(DeviceDiscoveryManager.class);
        mockDeviceManager = mock(DeviceManager.class);

        testConnection = BehaviorSubject.create();
        testServiceReady = BehaviorSubject.create();
        mockSDK = new MockSDK();

        when(mockSpeakersDiscoveryManager.isServiceConnected()).thenReturn(testConnection);
        when(mockSpeakersDiscoveryManager.isServiceReady()).thenReturn(testServiceReady);
        when(mockDeviceManager.getCurrentDevices()).thenReturn(new ArrayList<>());

        homActivityViewModel = new HomeActivityViewModel.Body(mockApplication,
                mockSpeakersDiscoveryManager, mockDeviceManager);
    }

    private BaseDevice getDevice() {
        return new MockSpeaker(new DeviceInfo("123456",
                DeviceInfo.DeviceType.MOCK,
                DeviceInfo.DeviceGroup.SPEAKER,
                "TestName",
                DeviceInfo.DeviceColor.BLACK),
                mockSDK);
    }

    @Test
    public void testScanForDeviceList() {
        BehaviorSubject<BaseDevice> testSubject = BehaviorSubject.create();
        BaseDevice testDevice = getDevice();

        when(mockSpeakersDiscoveryManager.getDiscoveredDevices()).thenReturn(testSubject);

        testConnection.onNext(true);
        testServiceReady.onNext(true);
        testSubject.onNext(testDevice);

        assertTrue(homActivityViewModel.outputs.areHomeDevicesFound().getValue());
    }
}

package com.zoundindustries.marshallbt.viewmodels.player.sources;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.utils.LiveDataHelper;
import com.zoundindustries.marshallbt.viewmodels.BaseJoplinSdkViewModelTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import io.reactivex.subjects.BehaviorSubject;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class AuxSourceViewModelTest extends BaseJoplinSdkViewModelTest {

    private BehaviorSubject<Boolean> testSourceSubject = BehaviorSubject.create();
    private BehaviorSubject<Integer> testVolumeSubject = BehaviorSubject.create();

    private AuxSourceViewModel.Body viewModel;

    @Before
    public void setup() {
        baseSetup();

        doNothing().when(mockBaseStateController.inputs).setAudioSource(BaseDevice.SourceType.AUX);

        when(mockBaseStateController.outputs.isSourceActive(BaseDevice.SourceType.AUX))
                .thenReturn(testSourceSubject);
        when(mockBaseStateController.outputs.getVolume()).thenReturn(testVolumeSubject);

        viewModel = new AuxSourceViewModel.Body(mockApplication, deviceId,
                mockDeviceManager,
                mock(FirebaseAnalyticsManager.class));

        testConnectionSubject.onNext(true);
    }

    @Test
    public void testActivateButtonClick() {
        viewModel.inputs.onActivateButtonClicked(mockView);
        testSourceSubject.onNext(true);
        assertTrue(LiveDataHelper.unboxBoolean(viewModel.outputs.isSourceActivated()));
    }

    @Test
    public void testSetVolume() {
        int value = 17;
        viewModel.inputs.setVolume(value);

        assertEquals(value, (int) LiveDataHelper.unboxInteger(viewModel.outputs.getVolume()));
    }
}
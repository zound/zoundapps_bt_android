package com.zoundindustries.marshallbt.viewmodels.mainmenu;

import android.app.Application;
import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import android.content.res.Resources;
import android.view.View;

import com.zoundindustries.marshallbt.TestUtils;
import com.zoundindustries.marshallbt.model.salesforce.SalesforceWebAPIController;
import com.zoundindustries.marshallbt.ui.ViewFlowController;
import com.zoundindustries.marshallbt.utils.CommonPreferences;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import io.reactivex.subjects.BehaviorSubject;
import retrofit2.Response;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class MainMenuUnsubscribeViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private MainMenuUnsubscribeViewModel.Body viewModel;
    private BehaviorSubject<Response<Void>> subject = BehaviorSubject.create();
    private Response<Void> mockResponse;
    private Resources mockResources;
    private View mockView;

    @Before
    public void setUp() {
        TestUtils.setupRx();

        Application mockApplication = mock(Application.class);
        mockResponse = mock(Response.class);
        mockResources = mock(Resources.class);
        SalesforceWebAPIController salesforceWebAPIController = mock(SalesforceWebAPIController.class);

        CommonPreferences mockPreferences = mock(CommonPreferences.class);
        when(mockPreferences.getUserEmail()).thenReturn("test@mail.com");

        mockView = mock(View.class);

        viewModel = new MainMenuUnsubscribeViewModel.Body(mockApplication,
                salesforceWebAPIController, mockResources, mockPreferences);
    }

    @Test
    public void testOnCancelButtonClicked() {
        viewModel.inputs.onCancelButtonClicked(mockView);
        assertEquals(ViewFlowController.ViewType.BACK,
                viewModel.outputs.isViewChanged().getValue());
    }
}
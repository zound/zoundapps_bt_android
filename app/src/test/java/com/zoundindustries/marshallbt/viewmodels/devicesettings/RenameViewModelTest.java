package com.zoundindustries.marshallbt.viewmodels.devicesettings;

import android.app.Application;
import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;
import android.content.res.Resources;
import android.view.View;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.TestUtils;
import com.zoundindustries.marshallbt.model.adapters.mock.MockSDK;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.BaseDevice.NameState;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.device.state.TymphanyDeviceStateController;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice;
import com.zoundindustries.marshallbt.model.device.tymphany.SpeakerInfo;
import com.zoundindustries.marshallbt.model.device.state.BaseDeviceStateController;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.services.DeviceService.DeviceImageType;
import com.zoundindustries.marshallbt.ui.ViewFlowController;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import io.reactivex.subjects.BehaviorSubject;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class RenameViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private final int MAX_NAME_LENGTH = 17;

    private Application mockApp;
    private Resources mockResources;
    private View mockView;
    private MockSDK mockSDK;
    private TymphanyDeviceStateController mockDeviceStateController;
    private DeviceManager mockDeviceManager;
    private TymphanyDevice mockSpeaker;
    private DeviceInfo mockDeviceInfo;
    private SpeakerInfo mockSpeakerInfo;

    private RenameViewModel.Body renameViewModel;
    private BehaviorSubject<Boolean> testDeviceConnectionSubject = BehaviorSubject.create();
    private BehaviorSubject<BaseDevice.ConnectionState> testDeviceConnectionInfoSubject =
            BehaviorSubject.create();

    @Before
    public void setup() {
        TestUtils.setupRx();

        mockApp = mock(Application.class);
        mockResources = mock(Resources.class);
        mockView = mock(View.class);
        mockSpeaker = mock(TymphanyDevice.class);
        mockDeviceInfo = mock(DeviceInfo.class);
        mockSpeakerInfo = mock(SpeakerInfo.class);
        mockDeviceManager = mock(DeviceManager.class);
        mockDeviceStateController = mock(TymphanyDeviceStateController.class);

        BaseDeviceStateController.Inputs mockInputs = mock(BaseDeviceStateController.Inputs.class);
        BaseDeviceStateController.Outputs mockOutputs = mock(BaseDeviceStateController.Outputs.class);
        FirebaseAnalyticsManager mockFirebaseAnalyticsManager = mock(FirebaseAnalyticsManager.class);

        mockDeviceStateController.inputs = mockInputs;
        mockDeviceStateController.outputs = mockOutputs;

        when(mockApp.getResources()).thenReturn(mockResources);
        when(mockResources.getString(any(Integer.class))).thenReturn("");
        when(mockDeviceManager.isServiceReady()).thenReturn(testDeviceConnectionSubject);
        when(mockDeviceManager.getDeviceFromId(any(String.class))).thenReturn(mockSpeaker);
        when(mockSpeaker.getDeviceInfo()).thenReturn(mockDeviceInfo);
        when(mockDeviceInfo.getId()).thenReturn("12345");
        when(mockSpeaker.getBaseDeviceStateController()).thenReturn(mockDeviceStateController);
        when(mockSpeaker.getMaxNameLength()).thenReturn(MAX_NAME_LENGTH);
        when(mockDeviceStateController.outputs.getConnectionInfo())
                .thenReturn(testDeviceConnectionInfoSubject);
        when(mockDeviceStateController.outputs.getSpeakerInfoCurrentValue()).
                thenReturn(mockSpeakerInfo);
        doNothing().when(mockInputs).setSpeakerInfo(any(SpeakerInfo.class));

        mockSDK = new MockSDK();
        renameViewModel = new RenameViewModel.Body(
                mockApp, mockDeviceInfo.getId(), mockDeviceManager, mockFirebaseAnalyticsManager);

        testDeviceConnectionSubject.onNext(true);
    }

    @Test
    public void testNameChangedWithValidName() {
        // the testable name
        String validName = "a valid name";

        //counter how many characters left
        int leftCharacters = MAX_NAME_LENGTH - validName.length();

        //Set for mocking, which Namestate and string we want to return for validName
        when(mockSpeaker.getValidityState(any(String.class))).thenReturn(NameState.VALID);
        when(mockResources.getString(R.string.rename_screen_characters_left,
                leftCharacters)).thenReturn("You have " + leftCharacters + " characters left");

        //Only the name important for us for validation, so the other parameters can be 0
        renameViewModel.inputs.nameChanged(validName, 0, 0, 0);

        //check the visibility of the next button and what is the warning message
        assertTrue(renameViewModel.outputs.getNextButtonVisibility().getValue()
                == View.VISIBLE);
        assertEquals(renameViewModel.outputs.getWarningMessage().getValue(), mockApp.getResources()
                .getString(R.string.rename_screen_characters_left, leftCharacters));
    }

    @Test
    public void testNameChangedWithTooLongName() {
        // the testable name
        String tooLongName = "llllllllllllllllllll";

        //Set for mocking, which Namestate and string we want to return for tooLongName
        when(mockSpeaker.getValidityState(any(String.class))).thenReturn(NameState.TOO_LONG);
        when(mockResources.getString(R.string.rename_screen_name_too_long))
                .thenReturn("Sorry, your name is too long");

        //Only the name important for us for validation, so the other parameters can be 0
        renameViewModel.inputs.nameChanged(tooLongName, 0, 0, 0);

        //check the visibility of the next button and what is the warning message
        assertTrue(renameViewModel.outputs.getNextButtonVisibility().getValue()
                == View.INVISIBLE);
        assertEquals(renameViewModel.outputs.getWarningMessage().getValue(), mockApp.getResources()
                .getString(R.string.rename_screen_name_too_long));
    }

    @Test
    public void testNameChangedWithEmptyName() {
        // the testable name
        String emptyName = " ";

        //counter how many characters left
        int leftCharacters = MAX_NAME_LENGTH - emptyName.length();

        //Set for mocking, which Namestate and string we want to return for emptyName
        when(mockSpeaker.getValidityState(any(String.class))).thenReturn(NameState.EMPTY);
        when(mockResources.getString(R.string.rename_screen_characters_left,
                leftCharacters)).thenReturn("You have " + leftCharacters + " characters left");

        //Only the name important for us for validation, so the other parameters can be 0
        renameViewModel.inputs.nameChanged(emptyName, 0, 0, 0);

        //check the visibility of the next button and what is the warning message
        assertTrue(renameViewModel.outputs.getNextButtonVisibility().getValue()
                == View.INVISIBLE);
        assertEquals(renameViewModel.outputs.getWarningMessage().getValue(), mockApp.getResources()
                .getString(R.string.rename_screen_characters_left, leftCharacters));
    }

    @Test
    public void testNextButtonTapped() {
        renameViewModel.inputs.nextButtonTapped(mockView);
        assertEquals(renameViewModel.outputs.isViewChanged().getValue(),
                ViewFlowController.ViewType.BACK);
    }

    @Test
    public void testGetImageResourceId() {
        MutableLiveData<Integer> imageResourceId;
        final int resIdExpected = 0;

        // simulate getting speaker image from DeviceManager
        when(mockDeviceManager.getImageResourceId(any(String.class),
                any(DeviceImageType.class))).thenReturn(resIdExpected);

        // get image resource ID for speaker
        imageResourceId = renameViewModel.outputs.getSpeakerImageResourceId();

        Assert.assertEquals(imageResourceId.getValue().intValue(), resIdExpected);
    }
}

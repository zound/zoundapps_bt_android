package com.zoundindustries.marshallbt.viewmodels.devicesettings;

import androidx.lifecycle.MutableLiveData;
import android.view.View;

import com.zoundindustries.marshallbt.model.device.BaseDevice.ConnectionState;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.viewmodels.BaseJoplinSdkViewModelTest;
import com.zoundindustries.marshallbt.viewmodels.devicesettings.CouplingModeViewModel.Body.CouplingMode;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import io.reactivex.subjects.BehaviorSubject;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class CouplingModeViewModelTest extends BaseJoplinSdkViewModelTest {

    private View mockView;

    private CouplingModeViewModel.Body viewModel;
    private BehaviorSubject<ConnectionState> connectionInfoSubject;

    @Before
    public void setup() {
        super.baseSetup();

        mockView = mock(View.class);

        connectionInfoSubject = BehaviorSubject.create();
        when(mockBaseStateController.outputs.getConnectionInfo()).thenReturn(connectionInfoSubject);

        viewModel = new CouplingModeViewModel.Body(mockApplication,
                deviceId,
                deviceId2,
                mockDeviceManager,
                mock(FirebaseAnalyticsManager.class));

        testConnectionSubject.onNext(true);
    }

    @Test
    public void testOnCouplingModeSelected() {
        MutableLiveData<Boolean> testStereo = viewModel.outputs.getStereoModeSelected();
        MutableLiveData<Boolean> testStandard = viewModel.outputs.getStandardModeSelected();

        //Check values never set
        assertFalse(testStereo.getValue());
        assertFalse(testStandard.getValue());

        //simulate user selected STEREO mode
        viewModel.inputs.onCouplingModeSelected(CouplingMode.STEREO);

        assertTrue(testStereo.getValue());
        assertFalse(testStandard.getValue());

        //simulate user unselected STEREO mode
        viewModel.inputs.onCouplingModeSelected(CouplingMode.STEREO);

        assertFalse(testStereo.getValue());
        assertFalse(testStandard.getValue());

        //simulate user selected STANDARD mode
        viewModel.inputs.onCouplingModeSelected(CouplingMode.STANDARD);

        assertFalse(testStereo.getValue());
        assertTrue(testStandard.getValue());

        //simulate user unselected STANDARD mode
        viewModel.inputs.onCouplingModeSelected(CouplingMode.STANDARD);

        assertFalse(testStereo.getValue());
        assertFalse(testStandard.getValue());

        //simulate user toggled STEREO/STANDARD modes
        viewModel.inputs.onCouplingModeSelected(CouplingMode.STEREO);
        viewModel.inputs.onCouplingModeSelected(CouplingMode.STANDARD);

        assertFalse(testStereo.getValue());
        assertTrue(testStandard.getValue());

        //simulate user toggled STANDARD/STEREO modes
        viewModel.inputs.onCouplingModeSelected(CouplingMode.STANDARD);
        viewModel.inputs.onCouplingModeSelected(CouplingMode.STEREO);

        assertTrue(testStereo.getValue());
        assertFalse(testStandard.getValue());
    }

    @Test
    public void testOnNextButtonClicked() {
        MutableLiveData<ViewType> test = viewModel.isViewChanged();

        //select device to couple
        viewModel.inputs.onCouplingModeSelected(CouplingMode.STEREO);

        //continue with coupling
        viewModel.inputs.onNextButtonClicked(mockView);

        assertTrue(test.getValue() == ViewType.COUPLE_DONE);
    }

    @Test
    public void testOnDeviceDisconnected() {

        //simulate device was connected
        connectionInfoSubject.onNext(ConnectionState.CONNECTED);

        //simulate device was disconnected
        connectionInfoSubject.onNext(ConnectionState.DISCONNECTED);

        assertTrue(viewModel.isViewChanged().getValue() == ViewType.HOME_SCREEN);
    }
}

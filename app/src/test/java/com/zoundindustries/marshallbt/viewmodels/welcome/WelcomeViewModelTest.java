package com.zoundindustries.marshallbt.viewmodels.welcome;

import android.app.Application;
import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import android.view.View;

import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.utils.CommonPreferences;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SuppressWarnings("unchecked")
@RunWith(JUnit4.class)
public class WelcomeViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private WelcomeViewModel.Body welcomeViewModel;

    private CommonPreferences mockPreferences;
    private View mockView;


    @Before
    public void setup() {
        Application mockApplication = mock(Application.class);
        mockPreferences = mock(CommonPreferences.class);
        mockView = mock(View.class);

        welcomeViewModel = new WelcomeViewModel.Body(mockApplication, mockPreferences);
    }

    @Test
    public void testOnNextButtonClicked() {
        when(mockPreferences.shouldDisplayWelcomeScreen()).thenReturn(true);

        welcomeViewModel.inputs.onAcceptButtonClicked(mockView);
        assertEquals(ViewType.SETUP_SCREEN,
                welcomeViewModel.outputs.isViewChanged().getValue());
    }

    @Test
    public void testOnTacAgreementChecked() {
        welcomeViewModel.inputs.onTacAgreementChecked(mockView, true);

        assertEquals((Integer)View.VISIBLE,
                welcomeViewModel.outputs.getConfirmButtonVisibility().getValue());
    }

    @Test
    public void testOnTacAgreementUnchecked() {
        welcomeViewModel.inputs.onTacAgreementChecked(mockView, false);

        assertEquals((Integer)View.INVISIBLE,
                welcomeViewModel.outputs.getConfirmButtonVisibility().getValue());
    }

    @Test
    public void testTacLinkTapped() {
        welcomeViewModel.inputs.onTacLinkClicked();
        assertEquals(ViewType.TAC_SCREEN,
                welcomeViewModel.outputs.isViewChanged().getValue());
    }

    @Test
    public void testResetViewTransitionVariables() {
        welcomeViewModel.inputs.resetViewTransitionVariables();
        assertEquals(ViewType.UNKNOWN,
                welcomeViewModel.outputs.isViewChanged().getValue());
    }
}

package com.zoundindustries.marshallbt.viewmodels.devicesettings;

import android.app.Application;
import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;
import android.content.res.Resources;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice;
import com.zoundindustries.marshallbt.model.device.tymphany.SpeakerInfo;
import com.zoundindustries.marshallbt.model.device.state.BaseDeviceStateController;
import com.zoundindustries.marshallbt.model.devicesettings.DeviceSettingsItem;
import com.zoundindustries.marshallbt.model.ota.OTAManager;
import com.zoundindustries.marshallbt.services.DeviceService.DeviceImageType;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

import io.reactivex.subjects.BehaviorSubject;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class DeviceSettingsListViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private BaseDeviceStateController mockBaseStateController;
    private DeviceInfo mockDeviceInfo;
    private SpeakerInfo mockSpeakerInfo;

    private DeviceSettingsListViewModel.Body deviceSettingsListViewModel;
    private BehaviorSubject<Boolean> testConnectionSubject = BehaviorSubject.create();
    private DeviceManager mockDeviceManager;
    private TymphanyDevice mockDevice;


    @Before
    public void setup() {
        String deviceId = "123456789";
        String deviceName = "testName";

        mockBaseStateController = mock(BaseDeviceStateController.class);
        mockDeviceInfo = mock(DeviceInfo.class);
        mockSpeakerInfo = mock(SpeakerInfo.class);

        Application mockApplication = mock(Application.class);
        mockDevice = mock(TymphanyDevice.class);
        mockDeviceManager = mock(DeviceManager.class);
        Resources mockResources = mock(Resources.class);
        BaseDeviceStateController.Outputs mockStateControllerOutputs =
                mock(BaseDeviceStateController.Outputs.class);

        mockBaseStateController.outputs = mockStateControllerOutputs;

        when(mockApplication.getResources()).thenReturn(mockResources);
        when(mockDeviceManager.isServiceReady()).thenReturn(testConnectionSubject);
        when(mockDeviceManager.getDeviceFromId(deviceId)).thenReturn(mockDevice);
        when(mockDevice.getDeviceInfo()).thenReturn(mockDeviceInfo);
        when(mockDevice.getBaseDeviceStateController()).thenReturn(mockBaseStateController);
        when(mockDeviceInfo.getId()).thenReturn(deviceId);
        when(mockBaseStateController.outputs.getSpeakerInfoCurrentValue())
                .thenReturn(mockSpeakerInfo);
        when(mockBaseStateController.outputs.getSpeakerInfoCurrentValue()
                .getDeviceName()).thenReturn(deviceName);
        when(mockResources.getString(R.string.appwide_about)).thenReturn("testName");

        deviceSettingsListViewModel = new DeviceSettingsListViewModel.Body(mockApplication,
                deviceId, mockDeviceManager, mock(OTAManager.class));
    }

    @Test
    public void testOnSettingsItemClicked() {
        MutableLiveData<ViewType> test =
                deviceSettingsListViewModel.outputs.isViewChanged();

        testConnectionSubject.onNext(true);

        deviceSettingsListViewModel.inputs
                .onSettingsItemClicked(DeviceSettingsItem.SettingType.ABOUT);
        assertTrue(test.getValue() == ViewType.ABOUT_DEVICE);

        deviceSettingsListViewModel.inputs
                .onSettingsItemClicked(DeviceSettingsItem.SettingType.EQUALISER);
        assertTrue(test.getValue() == ViewType.EQUALISER);

        deviceSettingsListViewModel.inputs
                .onSettingsItemClicked(DeviceSettingsItem.SettingType.LIGHT);
        assertTrue(test.getValue() == ViewType.LIGHT);

        deviceSettingsListViewModel.inputs
                .onSettingsItemClicked(DeviceSettingsItem.SettingType.RENAME);
        assertTrue(test.getValue() == ViewType.RENAME);

        deviceSettingsListViewModel.inputs
                .onSettingsItemClicked(DeviceSettingsItem.SettingType.COUPLE);
        assertTrue(test.getValue() == ViewType.COUPLE_SPEAKERS);
    }

    @Test
    public void testGetDeviceName() {

        //simulate connection feature being disabled
        when(mockBaseStateController
                .isFeatureSupported(FeaturesDefs.CONNECTION_INFO)).thenReturn(false);
        when(mockBaseStateController
                .isFeatureSupported(FeaturesDefs.COUPLING_CONNECTION)).thenReturn(false);

        //simulate service connected
        testConnectionSubject.onNext(true);
        MutableLiveData<String> testObservable =
                deviceSettingsListViewModel.outputs.getDeviceName();

        assertEquals(testObservable.getValue(),mockBaseStateController.outputs
                .getSpeakerInfoCurrentValue().getDeviceName());
    }

    @Test
    public void testIsConnected() {

        //simulate connection feature being disabled
        when(mockBaseStateController
                .isFeatureSupported(FeaturesDefs.CONNECTION_INFO)).thenReturn(false);

        //simulate service connected
        testConnectionSubject.onNext(true);

        MutableLiveData<BaseDevice.ConnectionState> testObservable = deviceSettingsListViewModel.outputs.isConnected();

        testObservable.setValue(BaseDevice.ConnectionState.CONNECTED);

        assertEquals(BaseDevice.ConnectionState.CONNECTED, testObservable.getValue());

        testObservable.setValue(BaseDevice.ConnectionState.DISCONNECTED);

        assertEquals(BaseDevice.ConnectionState.DISCONNECTED, testObservable.getValue());
    }

    @Test
    public void testGetSettingsList() {
        //start with 1 since we always show "forget speaker"
        int numberOfFeatures = 1;

        when(mockBaseStateController
                .isFeatureSupported(FeaturesDefs.LIGHT)).thenReturn(true);
        numberOfFeatures++;

        when(mockBaseStateController
                .isFeatureSupported(FeaturesDefs.EQ)).thenReturn(true);
        numberOfFeatures++;

        when(mockBaseStateController
                .isFeatureSupported(FeaturesDefs.SPEAKER_INFO)).thenReturn(true);
        numberOfFeatures ++;

        List<DeviceSettingsItem> settingsList = deviceSettingsListViewModel.getSettingsList();

        //simulate service connected
        testConnectionSubject.onNext(true);

        //assert 4 since we get 3 features + "forget speaker" which is always added to the list
        assertTrue(settingsList.size() == numberOfFeatures);
        assertTrue(settingsList.get(0).getName().equals("testName"));
    }

    @Test
    public void testGetImageResourceId() {
        MutableLiveData<Integer> imageResourceId;
        final int resIdExpected = 0;

        // simulate getting speaker image from DeviceManager
        when(mockDeviceManager.getImageResourceId(any(String.class),
                any(DeviceImageType.class))).thenReturn(resIdExpected);

        //simulate service connected
        testConnectionSubject.onNext(true);

        // get image resource ID for speaker
        imageResourceId = deviceSettingsListViewModel.outputs.getSpeakerImageResourceId();

        Assert.assertEquals(imageResourceId.getValue().intValue(), resIdExpected);
    }
}

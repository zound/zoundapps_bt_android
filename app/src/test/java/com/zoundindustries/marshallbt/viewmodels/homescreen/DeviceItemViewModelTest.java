package com.zoundindustries.marshallbt.viewmodels.homescreen;

import android.app.Application;
import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;
import android.content.res.Resources;
import android.os.Handler;
import android.view.View;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.TestUtils;
import com.zoundindustries.marshallbt.model.adapters.mock.MockSDK;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.BaseDevice.ConnectionState;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.device.tymphany.SpeakerInfo;
import com.zoundindustries.marshallbt.model.device.mock.MockSpeaker;
import com.zoundindustries.marshallbt.model.device.state.BaseDeviceStateController;
import com.zoundindustries.marshallbt.model.discovery.DeviceDiscoveryManager;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.model.ota.OTAManager;
import com.zoundindustries.marshallbt.model.ota.pojo.FirmwareFileMetaData;
import com.zoundindustries.marshallbt.services.DeviceService.DeviceImageType;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.utils.BluetoothSystemUtils;
import com.zoundindustries.marshallbt.utils.LiveDataHelper;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import io.reactivex.observers.TestObserver;
import io.reactivex.subjects.BehaviorSubject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SuppressWarnings("unchecked")
@RunWith(JUnit4.class)
public class DeviceItemViewModelTest {

    public static final int MOCK_CONNECTION_DELAY = 1100;
    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private DeviceItemViewModel.Body deviceItemViewModel;
    private BaseDeviceStateController mockDeviceStateController;
    private DeviceManager mockDeviceManager;
    private DeviceDiscoveryManager mockDeviceDiscoveryManager;
    private View mockView;
    private Handler mockHandler;
    private MockSpeaker device;
    private SpeakerInfo mockSpeakerInfo;

    private static final double DELTA = 0.01;

    private BluetoothSystemUtils mockBluetoothSystemUtils;
    private OTAManager mockOTAManager;

    private BehaviorSubject<Boolean> serviceReadySubject = BehaviorSubject.create();
    private BehaviorSubject<String> dividerIdSubject = BehaviorSubject.create();
    private BehaviorSubject<retrofit2.Response<FirmwareFileMetaData>> checkForFirmwareSubject = BehaviorSubject.create();

    @Before
    public void setup() {
        TestUtils.setupRx();

        Application mockApplication = mock(Application.class);
        Resources mockResources = mock(Resources.class);
        MockSDK mockSDK = new MockSDK();
        mockView = mock(View.class);
        mockHandler = mock(Handler.class);
        mockBluetoothSystemUtils = mock(BluetoothSystemUtils.class);
        mockOTAManager = mock(OTAManager.class);

        setupMockHandler();

        device = new MockSpeaker(new DeviceInfo("123456",
                DeviceInfo.DeviceType.MOCK,
                DeviceInfo.DeviceGroup.SPEAKER,
                "TestName",
                DeviceInfo.DeviceColor.BLACK),
                mockSDK);

        mockDeviceManager = mock(DeviceManager.class);
        mockDeviceDiscoveryManager = mock(DeviceDiscoveryManager.class);
        mockSpeakerInfo = mock(SpeakerInfo.class);
        mockDeviceStateController = mock(BaseDeviceStateController.class);

        when(mockApplication.getResources()).thenReturn(mockResources);
        when(mockResources.getColor(R.color.transparent)).thenReturn(0);
        when(mockResources.getColor(R.color.dark_beige)).thenReturn(0);
        when(mockDeviceManager.isServiceReady()).thenReturn(serviceReadySubject);
        when(mockDeviceManager.getDividerDeviceId()).thenReturn(dividerIdSubject);
        when(mockDeviceManager.isSpeakerInfoCached("123456")).thenReturn(true);
        doNothing().when(mockDeviceDiscoveryManager).startScanForDevices();
        when(mockOTAManager.checkForFirmwareUpdate(device.getDeviceInfo().getId(), false)).thenReturn(checkForFirmwareSubject);
        when(mockResources.getDimension(R.dimen.default_margin_small)).thenReturn(0f);
        when(mockResources.getDimension(R.dimen.default_margin_very_small)).thenReturn(0f);
        when(mockApplication.getString(R.string.speaker_status_connected)).thenReturn("Connected");
        when(mockApplication.getString(R.string.speaker_status_connecting)).thenReturn("Connecting");
        when(mockApplication.getString(R.string.speaker_status_playing)).thenReturn("Playing");
        when(mockApplication.getString(R.string.appwide_connect_uc)).thenReturn("CONNECT");
        when(mockApplication.getString(R.string.speaker_status_not_connected)).thenReturn("Disconnected");

        mockDeviceStateController = getDevice().getBaseDeviceStateController();
        mockDeviceStateController.inputs.setSpeakerInfo(mockSpeakerInfo);
        deviceItemViewModel = new DeviceItemViewModel.Body(mockApplication,
                getDevice(),
                mockBluetoothSystemUtils,
                mockDeviceManager,
                mockDeviceDiscoveryManager,
                mockOTAManager,
                mock(FirebaseAnalyticsManager.class),
                mockHandler);

        serviceReadySubject.onNext(true);
    }

    private void setupMockHandler() {
        when(mockHandler.postDelayed(any(Runnable.class), anyLong())).thenAnswer(invocation -> {
            Runnable runnable = invocation.getArgument(0);
            runnable.run();
            return null;
        });
    }

    private BaseDevice getDevice() {
        return device;
    }

    @Test
    public void testGetVolume() {
        int testValue = 55;
        TestObserver<Integer> test = mockDeviceStateController.outputs.getVolume().test();
        ((BehaviorSubject<Integer>) mockDeviceStateController.outputs.getVolume()).onNext(testValue);
        test.assertValue(testValue);
    }

    @Test
    public void testSetVolume() throws InterruptedException {
        int testValue = 55;
        TestObserver<Integer> test = mockDeviceStateController.outputs.getVolume().test();
        deviceItemViewModel.inputs.setVolume(testValue);
        Thread.sleep(300);
        test.assertValue(testValue);
    }

    @Test
    public void testSetConnected() throws InterruptedException {
        TestObserver<BaseDevice.ConnectionState> test =
                mockDeviceStateController.outputs.getConnectionInfo().test();
        deviceItemViewModel.inputs.setConnectionState(ConnectionState.CONNECTED);

        int index = 0;

        test.assertValueAt(index++, ConnectionState.CONNECTING);

        Thread.sleep(MOCK_CONNECTION_DELAY);

        test.assertValueAt(index, ConnectionState.CONNECTED);
    }

    @Test
    public void testGetDisconnected() {
        TestObserver<BaseDevice.ConnectionState> test =
                mockDeviceStateController.outputs.getConnectionInfo().test();
        ((BehaviorSubject<BaseDevice.ConnectionState>) mockDeviceStateController
                .outputs.getConnectionInfo())
                .onNext(ConnectionState.DISCONNECTED);
        test.assertValue(ConnectionState.DISCONNECTED);
    }

    @Test
    public void testSetPlaying() {
        TestObserver<Boolean> test = mockDeviceStateController.outputs.getPlaying().test();
        deviceItemViewModel.inputs.setPlaying(true);
        test.assertValue(true);
    }

    @Test
    public void testGetPlaying() {
        TestObserver<Boolean> test = mockDeviceStateController.outputs.getPlaying().test();
        ((BehaviorSubject<Boolean>) mockDeviceStateController.outputs.getPlaying()).onNext(false);
        test.assertValue(false);
    }

    @Test
    public void testSetHasUpdate() {
        TestObserver<Boolean> test = mockDeviceStateController.outputs.getHasUpdate().test();
        deviceItemViewModel.inputs.setUpdate(true);
        test.assertValue(true);
    }

    @Test
    public void testGetHasUpdate() {
        TestObserver<Boolean> test = mockDeviceStateController.outputs.getHasUpdate().test();
        ((BehaviorSubject<Boolean>) mockDeviceStateController.outputs.getHasUpdate()).onNext(false);
        test.assertValue(false);
    }

    @Test
    public void testSetConnectedInfoState() throws InterruptedException {
        when(mockBluetoothSystemUtils.isDevicePaired(device)).thenReturn(true);
        device.getBaseDeviceStateController().inputs.setPlaying(false);
        device.getBaseDeviceStateController().
                inputs.setConnectionInfo(ConnectionState.CONNECTED);

        //Delay to wait for UI animation
        Thread.sleep(200);

        //Delay to wait for UI animationassertEquals("Connecting", deviceItemViewModel.outputs.getPlayingInfoText().getValue());

        Thread.sleep(MOCK_CONNECTION_DELAY);

        assertEquals("Connected", deviceItemViewModel.outputs.getPlayingInfoText().getValue());
        assertEquals(1.0f, LiveDataHelper.unboxFloat(deviceItemViewModel.outputs
                .getPlayingInfoAlpha()), DELTA);
        assertEquals(false, deviceItemViewModel.outputs.isDeviceReadyForConnection().getValue());
    }

    @Test
    public void testSetDisconnectedInfoState() {
        device.getBaseDeviceStateController().inputs.setPlaying(false);
        device.getBaseDeviceStateController()
                .inputs.setConnectionInfo(ConnectionState.DISCONNECTED);

        assertEquals("Disconnected", deviceItemViewModel.outputs.getPlayingInfoText().getValue());
        assertEquals(0.5f, LiveDataHelper.unboxFloat(deviceItemViewModel.outputs
                .getPlayingInfoAlpha()), DELTA);
        assertEquals(false, deviceItemViewModel.outputs.isDeviceReadyForConnection().getValue());
    }

    @Test
    public void testSetPlayingInfoState() throws InterruptedException {
        when(mockBluetoothSystemUtils.isDevicePaired(device)).thenReturn(true);
        device.getBaseDeviceStateController().inputs.setPlaying(true);
        device.getBaseDeviceStateController()
                .inputs.setConnectionInfo(ConnectionState.CONNECTED);

        //Delay to wait for UI animation
        Thread.sleep(200);

        assertEquals(View.INVISIBLE, LiveDataHelper.unboxInteger(deviceItemViewModel.outputs
                .getPlayingInfoVisibility()).intValue());

        Thread.sleep(MOCK_CONNECTION_DELAY);

        assertEquals(View.VISIBLE, LiveDataHelper.unboxInteger(deviceItemViewModel.outputs
                .getPlayingInfoVisibility()).intValue());
        assertEquals("Playing", deviceItemViewModel.outputs.getPlayingInfoText().getValue());
        assertEquals(false, deviceItemViewModel.outputs.isDeviceReadyForConnection().getValue());
    }

    @Test
    public void testSetNotPlayingInfoState() {
        device.getBaseDeviceStateController().inputs.setPlaying(true);
        device.getBaseDeviceStateController()
                .inputs.setConnectionInfo(ConnectionState.READY_TO_CONNECT);

        assertEquals(View.INVISIBLE, LiveDataHelper.unboxInteger(deviceItemViewModel.outputs
                .getPlayingInfoVisibility()).intValue());
        assertEquals(true, deviceItemViewModel.outputs.isDeviceReadyForConnection().getValue());
    }

    @Test
    public void testSetAvailableInfoState() throws InterruptedException {
        device.getBaseDeviceStateController().inputs.setPlaying(false);
        device.getBaseDeviceStateController()
                .inputs.setConnectionInfo(ConnectionState.READY_TO_CONNECT);

        //Delay to wait for UI animation
        Thread.sleep(200);

        assertEquals(View.INVISIBLE,
                LiveDataHelper.unboxInteger(deviceItemViewModel.outputs.getPlayingInfoVisibility())
                        .intValue());

        assertEquals("CONNECT", deviceItemViewModel.outputs.getPlayingInfoText().getValue());
        assertEquals(true, deviceItemViewModel.outputs.isDeviceReadyForConnection().getValue());
    }

    @Test
    public void testGetDevice() {
        assertEquals(getDevice(), deviceItemViewModel.outputs.getDevice());
    }

    @Test
    public void testOnItemClickedPaired() throws InterruptedException {
        when(mockBluetoothSystemUtils.isDevicePaired(device)).thenReturn(true);

        deviceItemViewModel.inputs.setUpdate(true);
        deviceItemViewModel.inputs.setPlaying(false);
        deviceItemViewModel.inputs.setConnectionState(ConnectionState.CONNECTED);


        device.getBaseDeviceStateController().outputs.getConnectionInfo().test()
                .assertValueAt(0, ConnectionState.CONNECTING);

        Thread.sleep(MOCK_CONNECTION_DELAY);

        deviceItemViewModel.inputs.onItemClicked(mockView);

        assertEquals(ViewType.SOURCES, deviceItemViewModel.outputs.isViewChanged().getValue());
    }

    @Test
    public void testOnItemClickedNotPaired() {
        when(mockBluetoothSystemUtils.isDevicePaired(device)).thenReturn(false);

        deviceItemViewModel.inputs.setUpdate(true);
        deviceItemViewModel.inputs.setPlaying(false);
        deviceItemViewModel.inputs.setConnectionState(ConnectionState.READY_TO_CONNECT);

        deviceItemViewModel.inputs.onItemClicked(mockView);

        //when user taps item on non-paired speaker, nothing happens
        assertEquals(ViewType.HOME_SCREEN, deviceItemViewModel.outputs.isViewChanged().getValue());
    }

    @Test
    public void testConnectButtonClicked() throws InterruptedException {
        when(mockBluetoothSystemUtils.isDevicePaired(device)).thenReturn(false);

        deviceItemViewModel.inputs.setUpdate(true);
        deviceItemViewModel.inputs.setPlaying(false);
        deviceItemViewModel.inputs.setConnectionState(ConnectionState.READY_TO_CONNECT);

        deviceItemViewModel.inputs.onConnectButtonClicked(mockView);

        TestObserver test =
                device.getBaseDeviceStateController().outputs.getConnectionInfo().test();

        test.assertValueAt(0, ConnectionState.CONNECTING);

        Thread.sleep(MOCK_CONNECTION_DELAY);

        test.assertValueAt(1, ConnectionState.CONNECTED);
    }

    @Test
    public void testOnDeviceMenuClicked() {
        MutableLiveData<ViewType> testObservable = deviceItemViewModel.outputs.isViewChanged();

        deviceItemViewModel.inputs.onDeviceMenuClicked(mockView);

        assertTrue(testObservable.getValue() == ViewType.DEVICE_SETTINGS);
    }

    @Test
    public void testGetImageResourceId() {
        MutableLiveData<Integer> imageResourceId;
        final int resIdExpected = 0;

        // simulate getting speaker image from DeviceManager
        when(mockDeviceManager.getImageResourceId(any(String.class),
                any(DeviceImageType.class))).thenReturn(resIdExpected);

        // get image resource ID for speaker
        imageResourceId = deviceItemViewModel.outputs.getSpeakerImageResourceId();

        assertEquals(imageResourceId.getValue().intValue(), resIdExpected);
    }

    @Test
    public void testSetDividerNotSet() {
        // Divider is disabled. To be considered before release.
        /*deviceItemViewModel.inputs.setUpdate(true);
        deviceItemViewModel.inputs.setPlaying(false);
        deviceItemViewModel.inputs.setConnectionState(ConnectionState.CONNECTED);

        assertTrue(deviceItemViewModel.outputs.getDividerVisibility().getValue() ==
                View.GONE);*/
    }

    @Test
    public void testSetDividerSet() {
        /*when(mockBluetoothSystemUtils.isDevicePaired(device)).thenReturn(true);
        deviceItemViewModel.inputs.setUpdate(true);
        deviceItemViewModel.inputs.setPlaying(false);
        deviceItemViewModel.inputs.setConnectionState(ConnectionState.CONNECTED);

        dividerIdSubject.onNext("123456");

        assertTrue(deviceItemViewModel.outputs.getDividerVisibility().getValue() ==
                View.VISIBLE);*/
    }
}
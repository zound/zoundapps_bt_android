package com.zoundindustries.marshallbt.viewmodels.setup;

import android.app.Application;
import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import android.content.res.Resources;
import android.view.View;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.utils.CommonPreferences;
import com.zoundindustries.marshallbt.utils.LiveDataHelper;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class ShareDataViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private ShareDataViewModel.Body viewModel;
    private View mockView = mock(View.class);
    private Application mockApplication;
    private CommonPreferences mockPreferences;
    private FirebaseAnalyticsManager mockFirebaseAnalyticsManager;
    private Resources mockResources;

    @Before
    public void setup() {
        mockApplication = mock(Application.class);
        mockResources = mock(Resources.class);
        mockPreferences = mock(CommonPreferences.class);
        mockFirebaseAnalyticsManager = mock(FirebaseAnalyticsManager.class);
        when(mockPreferences.isFirstTimeShareData()).thenReturn(true);
        when(mockApplication.getResources()).thenReturn(mockResources);
        when(mockResources.getString(R.string.appwide_next_uc)).thenReturn("NEXT");
        when(mockResources.getString(R.string.appwide_done_uc)).thenReturn("DONE");
        viewModel = new ShareDataViewModel.Body(mockApplication, mockPreferences,
                mockFirebaseAnalyticsManager);
    }

    @Test
    public void testShareDataSwitch() {
        viewModel.inputs.shareDataSwitched(mockView, false);
        assertFalse(LiveDataHelper.unboxBoolean(viewModel.outputs.isShareDataSwitchEnabled()));

        viewModel.inputs.shareDataSwitched(mockView, true);
        assertTrue(LiveDataHelper.unboxBoolean(viewModel.outputs.isShareDataSwitchEnabled()));
    }

    @Test
    public void testNextButtonTappedInitial() {
        //@Before isFirstTimeShareData == true so on first time confirm button:

        //should have next text
        assertEquals("NEXT", viewModel.outputs.getConfirmationButtonText());

        //should go to save data screen
        viewModel.inputs.nextButtonTapped(mockView);
        assertEquals(ViewType.SAVE_DATA_SCREEN, LiveDataHelper.unboxViewType(
                viewModel.outputs.isViewChanged()));
    }

    @Test
    public void testDoneButtonTappedSettings() {
        //not first time -> from main menu settings
        when(mockPreferences.isFirstTimeShareData()).thenReturn(false);
        viewModel = new ShareDataViewModel.Body(mockApplication, mockPreferences,
                mockFirebaseAnalyticsManager);

        //text should be done
        assertEquals("DONE", viewModel.outputs.getConfirmationButtonText());

        //should move to home screen
        viewModel.inputs.nextButtonTapped(mockView);
        assertEquals(ViewType.HOME_SCREEN, LiveDataHelper.unboxViewType(
                viewModel.outputs.isViewChanged()));
    }

    @Test
    public void testResetViewTransitionVariables() {
        viewModel.inputs.resetViewTransitionVariables();
        assertEquals(ViewType.UNKNOWN, LiveDataHelper.unboxViewType(
                viewModel.outputs.isViewChanged()));
    }

    @Test
    public void testSkipButtonTappedInitial() {
        //@Before isFirstTimeShareData == true so on first time skip button click

        // check for visibility
        assertEquals((Integer) View.VISIBLE, viewModel.outputs.getSkipButtonVisibility().getValue());
        //should go to save data screen
        viewModel.inputs.skipButtonTapped(mockView);
        assertEquals(ViewType.SAVE_DATA_SCREEN, LiveDataHelper.unboxViewType(
                viewModel.outputs.isViewChanged()));
    }

    @Test
    public void testSkipButtonTappedSettings() {
        //not first time -> from main menu settings
        when(mockPreferences.isFirstTimeShareData()).thenReturn(false);
        viewModel = new ShareDataViewModel.Body(mockApplication, mockPreferences,
                mockFirebaseAnalyticsManager);

        //should not be visible at all for MainMenu Settings Share Data
        assertEquals((Integer) View.GONE, viewModel.outputs.getSkipButtonVisibility().getValue());
    }
}

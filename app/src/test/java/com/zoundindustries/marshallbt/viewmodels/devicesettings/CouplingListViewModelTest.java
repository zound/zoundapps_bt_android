package com.zoundindustries.marshallbt.viewmodels.devicesettings;

import androidx.lifecycle.MutableLiveData;
import android.view.View;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.BaseDevice.ConnectionState;
import com.zoundindustries.marshallbt.services.DeviceService.DeviceImageType;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.viewmodels.BaseJoplinSdkViewModelTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.subjects.BehaviorSubject;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class CouplingListViewModelTest extends BaseJoplinSdkViewModelTest {

    private View mockView;

    private CouplingListViewModel.Body viewModel;
    private List<BaseDevice> coupleDevices;
    private BehaviorSubject<BaseDevice> readyForCoupleDevicesSubject;
    private BehaviorSubject<BaseDevice.ConnectionState> connectionInfoSubject;

    @Before
    public void setup() {
        super.baseSetup();

        mockView = mock(View.class);

        coupleDevices = new ArrayList<>();
        readyForCoupleDevicesSubject = BehaviorSubject.create();
        connectionInfoSubject = BehaviorSubject.create();

        when(mockCouplingManager.startScanForReadyToCoupleDevices(mockDevice))
                .thenReturn(readyForCoupleDevicesSubject);
        when(mockBaseStateController.outputs.getConnectionInfo()).thenReturn(connectionInfoSubject);

        viewModel = new CouplingListViewModel.Body(mockApplication,
                deviceId,
                mockCouplingManager,
                mockDeviceManager,
                coupleDevices);

        testConnectionSubject.onNext(true);
    }

    @Test
    public void testOnItemSelected() {
        MutableLiveData<Boolean> test = viewModel.outputs.isItemSelected();

        assertFalse(test.getValue());

        //select device to couple
        viewModel.inputs.onItemSelected(mockDevice2);

        assertTrue(test.getValue());
    }

    @Test
    public void testOnNextButtonClicked() {
        MutableLiveData<ViewType> test = viewModel.isViewChanged();

        //select device to couple
        viewModel.inputs.onItemSelected(mockDevice2);

        //continue with coupling
        viewModel.inputs.onNextButtonClicked(mockView);

        assertTrue(test.getValue() == ViewType.COUPLE_MODE_SELECTION);
    }

    @Test
    public void testGetDevicesAvailableForCoupling() {

        //simulate ready to couple device discovered
        readyForCoupleDevicesSubject.onNext(mockDevice2);

        MutableLiveData<List<BaseDevice>> test = viewModel.getDevicesAvailableForCoupling();

        assertTrue(test.getValue().contains(mockDevice2));
        assertTrue(coupleDevices.contains(mockDevice2));
    }

    @Test
    public void testOnDeviceDisconnected() {

        //simulate ready to couple device discovered
        readyForCoupleDevicesSubject.onNext(mockDevice2);

        //simulate device was connected
        connectionInfoSubject.onNext(ConnectionState.CONNECTED);

        //simulate device was disconnected
        connectionInfoSubject.onNext(ConnectionState.DISCONNECTED);

        assertTrue(viewModel.outputs.isViewChanged().getValue() == ViewType.HOME_SCREEN);
    }

    @Test
    public void testGetImageResourceId() {
        int imageResourceId;
        final int resIdExpected = 0;

        // simulate getting speaker image from DeviceManager
        when(mockDeviceManager.getImageResourceId(deviceId,
                DeviceImageType.SMALL)).thenReturn(resIdExpected);

        // get image resource ID for speaker
        imageResourceId = viewModel.outputs.getSpeakerImageResourceId(deviceId);

        assertEquals(imageResourceId, resIdExpected);
    }
}

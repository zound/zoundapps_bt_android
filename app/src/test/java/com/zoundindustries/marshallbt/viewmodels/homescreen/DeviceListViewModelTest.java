package com.zoundindustries.marshallbt.viewmodels.homescreen;

import android.app.Application;
import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.zoundindustries.marshallbt.TestUtils;
import com.zoundindustries.marshallbt.model.adapters.mock.MockSDK;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.device.mock.MockSpeaker;
import com.zoundindustries.marshallbt.model.discovery.DeviceDiscoveryManager;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.utils.BluetoothSystemUtils;
import com.zoundindustries.marshallbt.utils.comparators.HomeListPairingComparator;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;

import io.reactivex.subjects.BehaviorSubject;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SuppressWarnings("unchecked")
@RunWith(JUnit4.class)
public class DeviceListViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private DeviceListViewModel.Body deviceListViewModel;
    private BehaviorSubject<Boolean> testConnection;
    private BehaviorSubject<Boolean> testServiceReady;

    private Application mockApplication;
    private MockSDK mockSDK;
    private DeviceDiscoveryManager mockSpeakersDiscoveryManager;
    private DeviceManager mockDeviceManager;
    private HomeListPairingComparator mockHomeListComparator;
    private BluetoothSystemUtils mockBluetoothSystemUtils;
    private FirebaseAnalyticsManager mockFirebaseAnalyticsManager;

    @Before
    public void setup() {
        TestUtils.setupRx();

        mockApplication = mock(Application.class);
        mockSpeakersDiscoveryManager = mock(DeviceDiscoveryManager.class);
        mockDeviceManager = mock(DeviceManager.class);
        mockHomeListComparator = mock(HomeListPairingComparator.class);
        mockBluetoothSystemUtils = mock(BluetoothSystemUtils.class);
        mockFirebaseAnalyticsManager = mock(FirebaseAnalyticsManager.class);

        testConnection = BehaviorSubject.create();
        testServiceReady = BehaviorSubject.create();
        mockSDK = new MockSDK();

        when(mockSpeakersDiscoveryManager.isServiceConnected()).thenReturn(testConnection);
        when(mockSpeakersDiscoveryManager.isServiceReady()).thenReturn(testServiceReady);
        doNothing().when(mockSpeakersDiscoveryManager).startScanForDevices();
        when(mockDeviceManager.getCurrentDevices()).thenReturn(new ArrayList<>());
        when(mockBluetoothSystemUtils.isDevicePaired(any(BaseDevice.class))).thenReturn(true);

        deviceListViewModel = new DeviceListViewModel.Body(mockApplication,new ArrayList<>(),
                mockSpeakersDiscoveryManager,
                mockDeviceManager,
                mockBluetoothSystemUtils,
                mockFirebaseAnalyticsManager);
    }

    private BaseDevice getDevice1() {
        return new MockSpeaker(new DeviceInfo("123456",
                DeviceInfo.DeviceType.MOCK,
                DeviceInfo.DeviceGroup.SPEAKER,
                "TestName",
                DeviceInfo.DeviceColor.BLACK),
                mockSDK);
    }

    private BaseDevice getDevice2() {
        return new MockSpeaker(new DeviceInfo("654321",
                DeviceInfo.DeviceType.MOCK,
                DeviceInfo.DeviceGroup.SPEAKER,
                "TestName",
                DeviceInfo.DeviceColor.BLACK),
                mockSDK);
    }

    @Test
    public void testScanForDeviceList() {
        BehaviorSubject<BaseDevice> testSubjectAdd = BehaviorSubject.create();
        BehaviorSubject<BaseDevice> testSubjectRemove = BehaviorSubject.create();
        BaseDevice testDevice1 = getDevice1();
        BaseDevice testDevice2 = getDevice2();

        when(mockSpeakersDiscoveryManager.getDiscoveredDevices()).thenReturn(testSubjectAdd);
        when(mockSpeakersDiscoveryManager.getDisappearedDevices()).thenReturn(testSubjectRemove);
        when(mockHomeListComparator
                .compare(any(BaseDevice.class), any(BaseDevice.class))).thenReturn(0);

        testConnection.onNext(true);
        testServiceReady.onNext(true);

        testSubjectAdd.onNext(testDevice1);

        deviceListViewModel.startScanDevices();

        assertTrue(deviceListViewModel.outputs.getHomeDevices().contains(testDevice1));

        testSubjectAdd.onNext(testDevice2);

        assertTrue(deviceListViewModel.outputs.getHomeDevices().contains(testDevice1));
        assertTrue(deviceListViewModel.outputs.getHomeDevices().contains(testDevice2));


        testSubjectRemove.onNext(testDevice2);

        assertFalse(deviceListViewModel.outputs.getHomeDevices().contains(testDevice2));
    }

    @Test
    public void testOnRefresh() {
        BehaviorSubject<BaseDevice> testSubject = BehaviorSubject.create();

        when(mockSpeakersDiscoveryManager.getDiscoveredDevices()).thenReturn(testSubject);

        testConnection.onNext(true);

        deviceListViewModel.onRefresh();

        verify(mockSpeakersDiscoveryManager, times(1)).startScanForDevices();
    }
}

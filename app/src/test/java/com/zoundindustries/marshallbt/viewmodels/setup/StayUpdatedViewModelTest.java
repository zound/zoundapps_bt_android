package com.zoundindustries.marshallbt.viewmodels.setup;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import android.content.res.Resources;
import android.view.View;

import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.TestUtils;
import com.zoundindustries.marshallbt.di.AppComponent;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.model.salesforce.SalesforceWebAPIController;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.utils.CommonPreferences;
import com.zoundindustries.marshallbt.utils.LiveDataHelper;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import io.reactivex.subjects.BehaviorSubject;
import retrofit2.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class StayUpdatedViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private StayUpdatedViewModel.Body viewModel;
    private View mockView = mock(View.class);
    private BehaviorSubject<Response<Void>> subject = BehaviorSubject.create();
    private Response mockResponse;
    private Resources mockResources;
    private CommonPreferences mockPreferences;
    private SalesforceWebAPIController mockSalesforceWebAPIController;
    private BluetoothApplication mockApplication;
    private String userEmail;
    private FirebaseAnalyticsManager mockFirebaseAnalyticsManager;

    @Before
    public void setup() {
        TestUtils.setupRx();

        mockApplication = mock(BluetoothApplication.class);
        mockResources = mock(Resources.class);
        when(mockApplication.getResources()).thenReturn(mockResources);

        AppComponent appComponent = mock(AppComponent.class);
        when(mockApplication.getAppComponent()).thenReturn(appComponent);

        mockResponse = mock(Response.class);

        mockSalesforceWebAPIController = mock(SalesforceWebAPIController.class);

        mockPreferences = mock(CommonPreferences.class);
        when(mockPreferences.isFirstTimeSubscription()).thenReturn(true);

        mockFirebaseAnalyticsManager = mock(FirebaseAnalyticsManager.class);

        userEmail = "test@test.com";

        viewModel = new StayUpdatedViewModel.Body(mockApplication,
                mockSalesforceWebAPIController,
                userEmail,
                mockPreferences,
                mockFirebaseAnalyticsManager);
    }

    @Test
    public void testCorrectEmailInput() {
        viewModel.inputs.emailChanged("test@test.com", 0, 0, 0);

        assertTrue(LiveDataHelper.unboxInteger(viewModel.outputs
                .getNextButtonVisibility()) == View.VISIBLE);
        assertTrue(View.GONE == LiveDataHelper.unboxInteger(viewModel.outputs
                .isWrongEmailIndicatorVisible()));
    }

    @Test
    public void testIncorrectEmailInput() {
        viewModel.inputs.emailChanged("test@test", 0, 0, 0);

        assertTrue(LiveDataHelper.unboxInteger(viewModel.outputs
                .getNextButtonVisibility()) == View.INVISIBLE);
        assertTrue(View.VISIBLE == LiveDataHelper.unboxInteger(viewModel.outputs
                .isWrongEmailIndicatorVisible()));
        assertEquals(mockResources.getString(R.string.stay_updated_email_validation_fail), viewModel.outputs.setEmailWarningText().getValue());
    }

    @Test
    public void testToShortEmailInput() {
        viewModel.inputs.emailChanged("te", 0, 0, 0);

        assertTrue(LiveDataHelper.unboxInteger(viewModel.outputs
                .getNextButtonVisibility()) == View.INVISIBLE);
        assertTrue(View.GONE == LiveDataHelper.unboxInteger(viewModel.outputs
                .isWrongEmailIndicatorVisible()));
    }

    @Test
    public void testSkipButtonTapped() {
        viewModel.inputs.skipButtonTapped(mockView);

        assertEquals(ViewType.SHARE_DATA_SCREEN, LiveDataHelper.unboxViewType(
                viewModel.outputs.isViewChanged()));
    }

    @Test
    public void testResetViewTransitionVariables() {
        viewModel.inputs.resetViewTransitionVariables();
        assertEquals(ViewType.UNKNOWN, LiveDataHelper.unboxViewType(
                viewModel.outputs.isViewChanged()));
    }

    @Test
    public void testUserMaleValue() {
        String mail = "test@test.com";
        viewModel.inputs.emailChanged(mail, 0, 0, 0);
        assertEquals(viewModel.getUserEmail(), mail);
    }

    @Test
    public void testPrivacyPolicyLinkTapped() {
        viewModel.inputs.privacyPolicyLinkTapped();
        assertTrue(viewModel.outputs.isViewChanged().getValue()
                == ViewType.PRIVACY_POLICY_SCREEN);
    }

    @Test
    public void testUnsubscribeButtonClicked() {
        viewModel.inputs.unsubscribeButtonClicked(null);
        assertTrue(viewModel.outputs.isViewChanged().getValue()
                == ViewType.MAIN_MENU_UNSUBSCRIBE);
    }

    @Test
    public void testInitialLayoutType() {
        //in @Before we set initial values for layoutType based on isFirstSubscription
        assertEquals(StayUpdatedViewModel.StayUpdateLayoutType.INITIAL_SUBSCRIBE,
                viewModel.getLayoutType().getValue());

        checkForFirstTimeSubscribeLayoutVisibility();
    }

    private void checkForFirstTimeSubscribeLayoutVisibility() {

        //checking for visibility of views
        assertEquals((Integer) View.GONE,
                viewModel.outputs.getSubscribedUserEmailTextViewVisibility().getValue());

        assertEquals((Integer) View.INVISIBLE,
                viewModel.outputs.getUnsubscribeButtonVisibility().getValue());

        assertEquals((Integer) View.INVISIBLE,
                viewModel.outputs.getProgressBarVisibility().getValue());

        assertEquals((Integer) View.GONE,
                viewModel.outputs.isWrongEmailIndicatorVisible().getValue());

        assertEquals((Integer) View.INVISIBLE,
                viewModel.outputs.getNextButtonVisibility().getValue());
    }

    @Test
    public void testUnsubscribeLayoutType() {
        //need to initial viewModel with new mock return value for unsubscribe
        when(mockPreferences.isFirstTimeSubscription()).thenReturn(false);
        when(mockPreferences.isEmailSubscribed()).thenReturn(true);
        viewModel = new StayUpdatedViewModel.Body(mockApplication,
                mockSalesforceWebAPIController,
                userEmail,
                mockPreferences,
                mockFirebaseAnalyticsManager);

        checkForUnsubscribeLayoutVisibility();
    }

    private void checkForUnsubscribeLayoutVisibility() {
        //checking for visibility of views
        assertEquals(StayUpdatedViewModel.StayUpdateLayoutType.UNSUBSCRIBE,
                viewModel.outputs.getLayoutType().getValue());

        assertEquals((Integer) View.VISIBLE,
                viewModel.outputs.getSubscribedUserEmailTextViewVisibility().getValue());

        assertEquals((Integer) View.VISIBLE,
                viewModel.outputs.getUnsubscribeButtonVisibility().getValue());

        assertEquals((Integer) View.GONE,
                viewModel.outputs.getEditTextForEmailVisibility().getValue());

        assertEquals((Integer) View.GONE,
                viewModel.outputs.getSkipButtonVisibility().getValue());

        assertEquals((Integer) View.INVISIBLE,
                viewModel.outputs.getProgressBarVisibility().getValue());

        assertEquals((Integer) View.GONE,
                viewModel.outputs.isWrongEmailIndicatorVisible().getValue());

        assertEquals((Integer) View.INVISIBLE,
                viewModel.outputs.getNextButtonVisibility().getValue());
    }

    @Test
    public void testSubscribeAgainLayoutType() {
        //need to initial viewModel with new mock return value for subscribe again (main menu)
        when(mockPreferences.isFirstTimeSubscription()).thenReturn(false);
        when(mockPreferences.isEmailSubscribed()).thenReturn(false);
        viewModel = new StayUpdatedViewModel.Body(mockApplication,
                mockSalesforceWebAPIController,
                userEmail,
                mockPreferences,
                mockFirebaseAnalyticsManager);

        assertEquals(StayUpdatedViewModel.StayUpdateLayoutType.SUBSCRIBE_AGAIN,
                viewModel.outputs.getLayoutType().getValue());

        checkForSubscribeAgainLayoutVisibility();
    }

    private void checkForSubscribeAgainLayoutVisibility() {
        //checking for visibility of views
        assertEquals((Integer) View.GONE,
                viewModel.outputs.getSubscribedUserEmailTextViewVisibility().getValue());

        assertEquals((Integer) View.INVISIBLE,
                viewModel.outputs.getUnsubscribeButtonVisibility().getValue());

        assertEquals((Integer) View.GONE,
                viewModel.outputs.getSkipButtonVisibility().getValue());

        assertEquals((Integer) View.INVISIBLE,
                viewModel.outputs.getProgressBarVisibility().getValue());

        assertEquals((Integer) View.GONE,
                viewModel.outputs.isWrongEmailIndicatorVisible().getValue());

        assertEquals((Integer) View.INVISIBLE,
                viewModel.outputs.getNextButtonVisibility().getValue());
    }
}

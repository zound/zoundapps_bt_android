package com.zoundindustries.marshallbt.viewmodels.player.sources;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.model.player.sources.SongInfo;
import com.zoundindustries.marshallbt.utils.LiveDataHelper;
import com.zoundindustries.marshallbt.viewmodels.BaseJoplinSdkViewModelTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import io.reactivex.subjects.BehaviorSubject;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class BluetoothSourceViewModelTest extends BaseJoplinSdkViewModelTest {

    private BehaviorSubject<Boolean> testSourceSubject = BehaviorSubject.create();
    private BehaviorSubject<Integer> testVolumeSubject = BehaviorSubject.create();
    private BehaviorSubject<SongInfo> testSongInfoSubject= BehaviorSubject.create();
    private BehaviorSubject<Boolean> testPlayingSubject= BehaviorSubject.create();

    private BluetoothSourceViewModel.Body viewModel;

    @Before
    public void setup() {
        baseSetup();

        doNothing().when(mockBaseStateController.inputs).setAudioSource(BaseDevice.SourceType.BLUETOOTH);

        when(mockBaseStateController.outputs.isSourceActive(BaseDevice.SourceType.BLUETOOTH))
                .thenReturn(testSourceSubject);
        when(mockBaseStateController.outputs.getVolume()).thenReturn(testVolumeSubject);

        when(mockBaseStateController.outputs.getSongInfo()).thenReturn(testSongInfoSubject);

        when(mockBaseStateController.outputs.getPlaying()).thenReturn(testPlayingSubject);

        viewModel = new BluetoothSourceViewModel.Body(mockApplication, deviceId,
                mockDeviceManager, mock(FirebaseAnalyticsManager.class));

        testConnectionSubject.onNext(true);
    }

    @Test
    public void testActivateButtonClick() {
        viewModel.inputs.onActivateButtonClicked(mockView);
        testSourceSubject.onNext(true);
        assertTrue(LiveDataHelper.unboxBoolean(viewModel.outputs.isSourceActivated()));
    }

    @Test
    public void testSetVolume() {
        int value = 17;
        viewModel.inputs.setVolume(value);

        assertEquals(value, (int) LiveDataHelper.unboxInteger(viewModel.outputs.getVolume()));
    }

    @Test
    public void testPlayButtonClick() {
        viewModel.isPlaying().setValue(false);

        //play
        viewModel.playerInputs.onPlayButtonClicked(mockView);

        assertTrue(LiveDataHelper.unboxBoolean(viewModel.playerOutputs.isPlaying()));

        //pause
        viewModel.playerInputs.onPauseButtonClicked((mockView));

        assertFalse(LiveDataHelper.unboxBoolean(viewModel.playerOutputs.isPlaying()));
    }

    @Test
    public void testNextButtonClick() {
        viewModel.playerInputs.onNextButtonClicked(mockView);
        verify(mockBaseStateController.inputs, times(1)).playNextSong();
    }

    @Test
    public void testPreviousButtonClick() {
        viewModel.playerInputs.onPreviousButtonClicked(mockView);
        verify(mockBaseStateController.inputs, times(1)).playPreviousSong();
    }

}

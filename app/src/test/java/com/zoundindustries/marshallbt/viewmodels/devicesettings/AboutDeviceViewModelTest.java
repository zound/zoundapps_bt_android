package com.zoundindustries.marshallbt.viewmodels.devicesettings;

import android.app.Application;
import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;
import android.view.View;

import com.zoundindustries.marshallbt.TestUtils;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.DeviceManager;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice;
import com.zoundindustries.marshallbt.model.device.tymphany.SpeakerInfo;
import com.zoundindustries.marshallbt.model.device.state.BaseDeviceStateController;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.model.ota.IOTAService;
import com.zoundindustries.marshallbt.model.ota.OTAManager;
import com.zoundindustries.marshallbt.model.ota.pojo.FirmwareFileMetaData;
import com.zoundindustries.marshallbt.ui.ViewFlowController;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import io.reactivex.subjects.BehaviorSubject;
import retrofit2.Response;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class AboutDeviceViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private View mockView;
    private OTAManager mockOtaManager;
    private BaseDeviceStateController mockBaseStateController;

    private BehaviorSubject<Boolean> testOTAConnectionSubject = BehaviorSubject.create();
    private BehaviorSubject<Response<FirmwareFileMetaData>> testOTACheckFirmwareSubject
            = BehaviorSubject.create();
    private BehaviorSubject<Boolean> testDeviceConnectionSubject = BehaviorSubject.create();
    private BehaviorSubject<SpeakerInfo> testDeviceSpeakerInfoSubject = BehaviorSubject.create();
    private BehaviorSubject<Double> testSubject;
    private BehaviorSubject<IOTAService.UpdateState> testUpdateStateSubject = BehaviorSubject.create();

    private AboutDeviceViewModel.Body aboutDeviceViewModel;

    @Before
    public void setup() {
        TestUtils.setupRx();

        mockView = mock(View.class);
        TymphanyDevice mockSpeaker = mock(TymphanyDevice.class);
        mockOtaManager = mock(OTAManager.class);
        Application mockApp = mock(Application.class);
        DeviceInfo mockDeviceInfo = mock(DeviceInfo.class);
        DeviceManager mockDeviceManager = mock(DeviceManager.class);
        mockBaseStateController = mock(BaseDeviceStateController.class);
        BaseDeviceStateController.Inputs mockStateControllerInputs = mock(BaseDeviceStateController.Inputs.class);
        BaseDeviceStateController.Outputs mockStateControllerOutputs = mock(BaseDeviceStateController.Outputs.class);
        FirmwareFileMetaData mockFirmwareFileMetaData = mock(FirmwareFileMetaData.class);
        FirebaseAnalyticsManager mockFirebaseAnalyticsManager = mock(FirebaseAnalyticsManager.class);

        mockBaseStateController.inputs = mockStateControllerInputs;
        mockBaseStateController.outputs = mockStateControllerOutputs;

        testSubject = BehaviorSubject.create();

        when(mockOtaManager.isServiceReady()).thenReturn(testOTAConnectionSubject);
        when(mockOtaManager.checkForFirmwareUpdate(mockDeviceInfo.getId(), false)).thenReturn(testOTACheckFirmwareSubject);
        when(mockSpeaker.getDeviceInfo()).thenReturn(mockDeviceInfo);
        when(mockSpeaker.getBaseDeviceStateController()).thenReturn(mockBaseStateController);
        when(mockDeviceInfo.getId()).thenReturn("12345");
        when(mockOtaManager.getFirmwareUpdateProgress(mockSpeaker
                .getDeviceInfo().getId())).thenReturn(testSubject);
        when(mockOtaManager.getFirmwareUpdateState(mockDeviceInfo.getId()))
                .thenReturn(testUpdateStateSubject);
        when(mockDeviceManager.isServiceReady()).thenReturn(testDeviceConnectionSubject);
        when(mockDeviceManager.getDeviceFromId(any(String.class))).thenReturn(mockSpeaker);
        when(mockStateControllerOutputs.getSpeakerInfo()).thenReturn(testDeviceSpeakerInfoSubject);

        aboutDeviceViewModel = new AboutDeviceViewModel.Body(mockApp,
                mockDeviceInfo.getId(),
                mockDeviceManager,
                mockOtaManager,
                mockFirmwareFileMetaData,
                false,
                mockFirebaseAnalyticsManager);
    }

    @Test
    public void testUpdateButtonClicked() {
        aboutDeviceViewModel.onUpdateButtonClicked(mockView);

        assertTrue(aboutDeviceViewModel.getUpdateButtonVisibility().getValue() == View.GONE);
        assertTrue(aboutDeviceViewModel.getUpdatingProgressVisibility().getValue() == View.VISIBLE);

        doNothing().when(mockOtaManager).startOTAUpdate(any(String.class), any(FirmwareFileMetaData.class));

        testOTAConnectionSubject.onNext(true);
        testSubject.onNext(50.0);

        assertEquals(50.0,
                aboutDeviceViewModel.getFirmwareUpdateProgress().getValue());


        testSubject.onComplete();
        assertTrue(aboutDeviceViewModel.getUpdateButtonVisibility().getValue() == View.GONE);
        assertTrue(aboutDeviceViewModel.getUpdatingProgressVisibility().getValue() == View.VISIBLE);
    }

    @Test
    public void testUpdateButtonClickedWithError() {
        aboutDeviceViewModel.onUpdateButtonClicked(mockView);

        assertTrue(aboutDeviceViewModel.getUpdateButtonVisibility().getValue() == View.GONE);
        assertTrue(aboutDeviceViewModel.getUpdatingProgressVisibility().getValue() == View.VISIBLE);

        doNothing().when(mockOtaManager).startOTAUpdate(any(String.class), any(FirmwareFileMetaData.class));

        testOTAConnectionSubject.onNext(true);

        MutableLiveData<Double> firmwareUpdateProgress =
                aboutDeviceViewModel.outputs.getFirmwareUpdateProgress();
        testSubject.onNext(50.0);
        assertEquals(50.0, firmwareUpdateProgress.getValue());

        testSubject.onError(new Exception());
        assertTrue(aboutDeviceViewModel.getUpdateButtonVisibility().getValue() == View.VISIBLE);
        assertTrue(aboutDeviceViewModel.getUpdatingProgressVisibility().getValue() == View.GONE);
    }

    @Test
    public void testIsViewChanged() {
        MutableLiveData<ViewFlowController.ViewType> testObservable =
                aboutDeviceViewModel.outputs.isViewChanged();

        BehaviorSubject<BaseDevice.ConnectionState> testConnectionStateSubject =
                BehaviorSubject.create();

        when(mockBaseStateController.isFeatureSupported(FeaturesDefs.CONNECTION_INFO))
                .thenReturn(true);
        when(mockBaseStateController
                .outputs.getConnectionInfo()).thenReturn(testConnectionStateSubject);

        testUpdateStateSubject.onNext(IOTAService.UpdateState.DOWNLOADING_FROM_SERVER);

        testOTAConnectionSubject.onNext(true);
        testDeviceConnectionSubject.onNext(true);
        testConnectionStateSubject.onNext(BaseDevice.ConnectionState.DISCONNECTED);

        assertEquals(ViewFlowController.ViewType.HOME_SCREEN, testObservable.getValue());
    }

}

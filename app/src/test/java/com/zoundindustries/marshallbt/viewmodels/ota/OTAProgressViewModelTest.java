package com.zoundindustries.marshallbt.viewmodels.ota;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.adapters.mock.MockSDK;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.mock.MockSpeaker;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.model.ota.IOTAService.UpdateState;
import com.zoundindustries.marshallbt.model.ota.OTAManager;
import com.zoundindustries.marshallbt.model.ota.pojo.FirmwareFileMetaData;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.viewmodels.BaseJoplinSdkViewModelTest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import io.reactivex.subjects.BehaviorSubject;
import retrofit2.Response;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OTAProgressViewModelTest extends BaseJoplinSdkViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private Drawable mockDrawable1;
    private Drawable mockDrawable2;
    private Drawable mockDrawable3;
    private Resources mockResources;
    private MockSDK mockSDK;
    private OTAManager mockOtaManager;
    private Response<FirmwareFileMetaData> mockResponse;
    private FirmwareFileMetaData mockFirmwareFileMetaData;

    private OTAProgressViewModel.Body otaProgressViewModel;
    private BehaviorSubject<Boolean> testConnectionSubject = BehaviorSubject.create();
    private BehaviorSubject<BaseDevice.ConnectionState> testConnectionInfoSubject =
            BehaviorSubject.create();
    private BehaviorSubject<Response<FirmwareFileMetaData>> testFirmwareMetaDataInfoSubject =
            BehaviorSubject.create();

    @Before
    public void setup() {
        baseSetup();

        FirebaseAnalyticsManager mockFirebaseAnalyticsManager = mock(FirebaseAnalyticsManager.class);

        mockDrawable1 = mock(Drawable.class);
        mockDrawable2 = mock(Drawable.class);
        mockDrawable3 = mock(Drawable.class);
        mockResources = mock(Resources.class);
        mockOtaManager = mock(OTAManager.class);
        mockResponse = mock(Response.class);
        mockFirmwareFileMetaData = mock(FirmwareFileMetaData.class);

        mockSDK = new MockSDK();

        when(mockOtaManager.isServiceReady()).thenReturn(testConnectionSubject);
        when(mockBaseStateController.outputs.getConnectionInfo()).thenReturn(testConnectionInfoSubject);
        when(mockResponse.body()).thenReturn(mockFirmwareFileMetaData);
        when(mockFirmwareFileMetaData.getBrand()).thenReturn("zound");
        when(mockFirmwareFileMetaData.getModel()).thenReturn("joplin_s");
        when(mockFirmwareFileMetaData.getVersion()).thenReturn("6.0.2");
        when(mockOtaManager.checkForFirmwareUpdate(deviceId, true))
                .thenReturn(testFirmwareMetaDataInfoSubject);

        otaProgressViewModel =
                new OTAProgressViewModel.Body(mockApplication,
                        mockDevice.getDeviceInfo().getId(),
                        mockOtaManager,
                        mockFirebaseAnalyticsManager);
    }

    private BaseDevice getDevice() {
        return new MockSpeaker(new DeviceInfo("123456",
                DeviceInfo.DeviceType.MOCK,
                DeviceInfo.DeviceGroup.SPEAKER,
                "TestName",
                DeviceInfo.DeviceColor.BLACK),
                mockSDK);
    }

    @Test
    public void testOnPageChanged() {
        when(mockApplication.getResources()).thenReturn(mockResources);
        when(mockResources.getDrawable(R.drawable.ota_slide_dot1)).thenReturn(mockDrawable1);
        when(mockResources.getDrawable(R.drawable.ota_slide_dot2)).thenReturn(mockDrawable2);
        when(mockResources.getDrawable(R.drawable.ota_slide_dot3)).thenReturn(mockDrawable3);

        testConnectionSubject.onNext(true);

        MutableLiveData<Drawable> test = otaProgressViewModel.outputs
                .getDots();
        otaProgressViewModel.inputs.onPageChanged(0);
        assertEquals(test.getValue(), mockDrawable1);

        otaProgressViewModel.inputs.onPageChanged(1);
        assertEquals(test.getValue(), mockDrawable2);

        otaProgressViewModel.inputs.onPageChanged(2);
        assertEquals(test.getValue(), mockDrawable3);
    }

    @Test
    public void testGetFirmwareUpdateProgress() {
        BehaviorSubject<Double> testSubject = BehaviorSubject.create();
        when(mockOtaManager.getFirmwareUpdateProgress(mockDevice
                .getDeviceInfo().getId())).thenReturn(testSubject);

        doNothing().when(mockOtaManager).startOTAUpdate(any(String.class),
                any(FirmwareFileMetaData.class));

        testConnectionSubject.onNext(true);
        testFirmwareMetaDataInfoSubject.onNext(mockResponse);
        testSubject.onNext(50.0);

        assertEquals(50.0,
                otaProgressViewModel.getFirmwareUpdateProgress().getValue());
    }

    @Test
    public void testShowCompletedFragment() {
        BehaviorSubject<UpdateState> testSubject = BehaviorSubject.create();

        when(mockOtaManager.getFirmwareUpdateState(mockDevice
                .getDeviceInfo().getId())).thenReturn(testSubject);

        testConnectionSubject.onNext(true);
        testFirmwareMetaDataInfoSubject.onNext(mockResponse);
        testSubject.onNext(UpdateState.COMPLETED);
        testSubject.onComplete();

        assertEquals(ViewType.OTA_COMPLETED,
                otaProgressViewModel.outputs.isViewChanged().getValue());
    }

}

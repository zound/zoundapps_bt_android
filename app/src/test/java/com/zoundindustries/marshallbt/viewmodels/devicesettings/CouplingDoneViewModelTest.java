package com.zoundindustries.marshallbt.viewmodels.devicesettings;

import androidx.lifecycle.MutableLiveData;
import android.view.View;

import com.zoundindustries.marshallbt.model.coupling.DeviceCouplingInfo;
import com.zoundindustries.marshallbt.model.device.BaseDevice.ConnectionState;
import com.zoundindustries.marshallbt.model.device.BaseDevice.CouplingChannelType;
import com.zoundindustries.marshallbt.model.device.BaseDevice.CouplingConnectionState;
import com.zoundindustries.marshallbt.services.DeviceService.DeviceImageType;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.viewmodels.BaseJoplinSdkViewModelTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import io.reactivex.subjects.BehaviorSubject;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class CouplingDoneViewModelTest extends BaseJoplinSdkViewModelTest {

    private View mockView;

    private CouplingDoneViewModel.Body viewModelCoupled;
    private BehaviorSubject<ConnectionState> connectionInfoSubject;
    private BehaviorSubject<DeviceCouplingInfo> twsConnectionInfoSubject;

    @Before
    public void setup() {
        super.baseSetup();

        mockView = mock(View.class);

        connectionInfoSubject = BehaviorSubject.create();
        twsConnectionInfoSubject = BehaviorSubject.create();

        when(mockBaseStateController.outputs.getConnectionInfo()).thenReturn(connectionInfoSubject);
        when(mockBaseStateController.outputs.getCouplingConnectionInfo())
                .thenReturn(twsConnectionInfoSubject);
        viewModelCoupled = new CouplingDoneViewModel.Body(mockApplication,
                deviceId,
                deviceId2,
                mockDeviceManager);

        testConnectionSubject.onNext(true);
    }

    @Test
    public void testOnChannelModeChanged() {

        //simulate user selected LEFT channel
        viewModelCoupled.inputs.onChannelModeChanged(CouplingChannelType.LEFT);

        assertTrue(viewModelCoupled.outputs.getCurrentChannelType() ==
                CouplingChannelType.LEFT);

        //simulate user selected RIGHT channel
        viewModelCoupled.inputs.onChannelModeChanged(CouplingChannelType.RIGHT);

        assertTrue(viewModelCoupled.outputs.getCurrentChannelType() ==
                CouplingChannelType.RIGHT);

        //simulate user selected PARTY channel
        viewModelCoupled.inputs.onChannelModeChanged(CouplingChannelType.PARTY);

        assertTrue(viewModelCoupled.outputs.getCurrentChannelType() ==
                CouplingChannelType.PARTY);
    }

    @Test
    public void testGetDeviceNames() {
        assertTrue(viewModelCoupled.outputs.getMasterName().getValue()
                .equals(mockDevice.getBaseDeviceStateController()
                        .outputs.getSpeakerInfoCurrentValue().getDeviceName()));
        assertTrue(viewModelCoupled.outputs.getSlaveName().getValue()
                .equals(mockDevice2.getBaseDeviceStateController()
                        .outputs.getSpeakerInfoCurrentValue().getDeviceName()));
    }

    @Test
    public void testOnDoneButtonClickedNotCoupled() {
        MutableLiveData<ViewType> testObserver = viewModelCoupled.isViewChanged();
        DeviceCouplingInfo testDeviceCouplingInfo = new DeviceCouplingInfo();

        //select coupling channel mode
        viewModelCoupled.inputs.onChannelModeChanged(CouplingChannelType.LEFT);

        //continue with coupling
        viewModelCoupled.inputs.onDoneButtonClicked(mockView);

        connectionInfoSubject.onNext(ConnectionState.CONNECTED);

        testDeviceCouplingInfo.setCouplingConnectionInfoState(CouplingConnectionState.NOT_COUPLED);
        twsConnectionInfoSubject.onNext(testDeviceCouplingInfo);

        verify(mockDevice.getBaseDeviceStateController().inputs, times(1))
                .setCouplingMasterConnected(CouplingChannelType.LEFT,
                        mockDevice2.getDeviceInfo().getId());
    }

    @Test
    public void testOnDeviceDisconnected() {

        //simulate device was connected
        connectionInfoSubject.onNext(ConnectionState.CONNECTED);

        //simulate device was disconnected
        connectionInfoSubject.onNext(ConnectionState.DISCONNECTED);

        assertTrue(viewModelCoupled.isViewChanged().getValue() == ViewType.HOME_SCREEN);
    }

    @Test
    public void testGetImageResourceId() {
        MutableLiveData<Integer> imageResourceIdMaster;
        MutableLiveData<Integer> imageResourceIdSlave;
        final int resIdExpectedMaster = 0;
        final int resIdExpectedSlave = 1;

        // simulate getting master speaker image from DeviceManager
        when(mockDeviceManager.getImageResourceId(deviceId,
                DeviceImageType.SMALL)).thenReturn(resIdExpectedMaster);

        // simulate getting slave speaker image from DeviceManager
        when(mockDeviceManager.getImageResourceId(deviceId2,
                DeviceImageType.SMALL)).thenReturn(resIdExpectedSlave);

        // get image resource ID for master
        imageResourceIdMaster = viewModelCoupled.outputs.getMasterImageResourceId();

        // get image resource ID for slave
        imageResourceIdSlave = viewModelCoupled.outputs.getSlaveImageResourceId();

        //simulate service connected
        testConnectionSubject.onNext(true);

        assertEquals(imageResourceIdMaster.getValue().intValue(), resIdExpectedMaster);

        assertEquals(imageResourceIdSlave.getValue().intValue(), resIdExpectedSlave);
    }
}

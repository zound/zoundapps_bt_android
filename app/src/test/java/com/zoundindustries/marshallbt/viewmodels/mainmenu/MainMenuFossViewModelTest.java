package com.zoundindustries.marshallbt.viewmodels.mainmenu;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;

public class MainMenuFossViewModelTest {

    private MainMenuFossViewModel.Body viewModel;

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setUp() {
        viewModel = new MainMenuFossViewModel.Body();
    }

    @Test
    public void testOnGoToWebsiteButtonClicked() {
        // isViewChanged is changed only in case when user clicks goToWebsite button
        assertNull(viewModel.outputs.isViewChanged().getValue());

        //Fragment - FOSS
        viewModel.inputs.onGoToWebsiteButtonClicked(null);
        assertTrue(viewModel.outputs.isViewChanged().getValue()
                == ViewType.MAIN_MENU_FOSS_WEBSITE);
    }
}
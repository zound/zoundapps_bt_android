package com.zoundindustries.marshallbt.viewmodels.devicesettings;

import androidx.lifecycle.MutableLiveData;
import android.view.View;

import com.zoundindustries.marshallbt.services.DeviceService.DeviceImageType;
import com.zoundindustries.marshallbt.ui.ViewFlowController.ViewType;
import com.zoundindustries.marshallbt.viewmodels.BaseJoplinSdkViewModelTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class ForgetDeviceViewModelTest extends BaseJoplinSdkViewModelTest {

    private View mockView;

    private ForgetDeviceViewModel.Body viewModel;

    @Before
    public void setup() {
        super.baseSetup();

        mockView = mock(View.class);

        viewModel = new ForgetDeviceViewModel.Body(mockApplication, deviceId, mockDeviceManager);

        testConnectionSubject.onNext(true);
    }

    @Test
    public void testOnForgetButtonClicked() {
        MutableLiveData<ViewType> testObservable = viewModel.outputs.isViewChanged();

        //simulate user press forget speaker button
        viewModel.inputs.onForgetButtonClicked(mockView);

        assertTrue(testObservable.getValue() == ViewType.GLOBAL_SETTINGS_BLUETOOTH);
    }

    @Test
    public void testGetDeviceName() {
        MutableLiveData<String> testObserver = viewModel.getDeviceName();

        assertEquals(testObserver.getValue(), mockBaseStateController.outputs.
                getSpeakerInfoCurrentValue().getDeviceName());
    }

    @Test
    public void testGetImageResourceId() {
        MutableLiveData<Integer> imageResourceId;
        final int resIdExpected = 0;

        // simulate getting speaker image from DeviceManager
        when(mockDeviceManager.getImageResourceId(any(String.class),
                any(DeviceImageType.class))).thenReturn(resIdExpected);

        // get image resource ID for speaker
        imageResourceId = viewModel.outputs.getSpeakerImageResourceId();

        Assert.assertEquals(imageResourceId.getValue().intValue(), resIdExpected);
    }
}

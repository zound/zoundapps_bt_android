package com.zoundindustries.marshallbt.model.adapters.joplin;


import com.tym.tymappplatform.TAProtocol.TAProtocol.TASystem;
import com.tym.tymappplatform.TAService.TADigitalSignalProcessingService.TADigitalSignalProcessingService;
import com.tym.tymappplatform.TAService.TAPlayControlService.TAPlayControlService;
import com.tym.tymappplatform.TAService.TASystemService.TASystemService;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.tymphany.SmallJoplinSpeaker;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice.DeviceSubType;
import com.zoundindustries.marshallbt.model.discovery.adapter.joplin.TymphanyDeviceDiscoveryAdapter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import io.reactivex.observers.TestObserver;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(JUnit4.class)
public class TymhpanyDeviceDiscoveryAdapterTest {

    private TymphanyDeviceDiscoveryAdapter tymhpanyDeviceDiscoveryAdapter;

    private TASystemService mockTaSystemService;
    private TADigitalSignalProcessingService mockTaDspService;
    private TAPlayControlService mockTaPlayControlService;
    private TymphanyDevice mockJoplinSpeaker;
    private TASystem mockTaSystem;

    @Before
    public void setup() {
        mockJoplinSpeaker = mock(TymphanyDevice.class);
        mockTaSystem = mock(TASystem.class);
        mockTaSystemService = mock(TASystemService.class);
        mockTaDspService = mock(TADigitalSignalProcessingService.class);
        mockTaPlayControlService = mock(TAPlayControlService.class);

        tymhpanyDeviceDiscoveryAdapter = new TymphanyDeviceDiscoveryAdapter(mockTaSystemService,
                mockTaPlayControlService, mockTaDspService);
    }

    @Test
    public void testStartScanForDevices() {

        TestObserver<BaseDevice> test = tymhpanyDeviceDiscoveryAdapter.getDisappearedDevices()
                .test();

        test.onNext(mockJoplinSpeaker);

        test.assertValue(mockJoplinSpeaker);
    }

    @Test
    public void testStopScanForDevices() {
        tymhpanyDeviceDiscoveryAdapter.stopScanForDevices();

        verify(mockTaSystemService, times(1)).stopScanSystems();
    }

    @Test
    public void testDidDiscoverSystem() {
        String testDeviceName = "TestName";
        String testDeviceId = "123456789";

        TymphanyDevice testSpeaker = new SmallJoplinSpeaker(
                new DeviceInfo(
                        testDeviceId,
                        DeviceInfo.DeviceType.TYMPHANY_DEVICE,
                        DeviceInfo.DeviceGroup.SPEAKER,
                        DeviceSubType.JOPLIN_S.getProductName(),
                        DeviceInfo.DeviceColor.BLACK),
                mockTaSystemService,
                mockTaPlayControlService,
                mockTaDspService,
                mockTaSystem);

        mockTaSystem.deviceName = testDeviceName;
        mockTaSystem.deviceAddress = testDeviceId;

        TestObserver<BaseDevice> test =
                tymhpanyDeviceDiscoveryAdapter.getDiscoveredDevices().test();

        // 0x30 -> 48 in decimal
        tymhpanyDeviceDiscoveryAdapter.didDiscoverSystem(mockTaSystem, 0,
                new byte[]{0, 0, 0, 48});

        test.assertValueAt(0, testSpeaker);
    }

    @Test
    public void testDispose() {
        tymhpanyDeviceDiscoveryAdapter.dispose();

        verify(mockTaSystemService, times(1))
                .unregisterObserver(tymhpanyDeviceDiscoveryAdapter);
    }
}


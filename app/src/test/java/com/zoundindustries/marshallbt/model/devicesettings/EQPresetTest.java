package com.zoundindustries.marshallbt.model.devicesettings;

import com.zoundindustries.marshallbt.utils.CommonPreferences;
import com.zoundindustries.marshallbt.utils.EqPresetType;
import com.zoundindustries.marshallbt.utils.joplin.JoplinEQPresetListManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;

@RunWith(JUnit4.class)
public class EQPresetTest {
    private CommonPreferences mockPreferences = mock(CommonPreferences.class);

    private JoplinEQPresetListManager joplinEQPresetListManager =
            new JoplinEQPresetListManager(mockPreferences);
    private ArrayList<EQPreset> presets = new ArrayList<>();

    private int[] customValues;
    private int[] flatValues;
    private int[] rockValues;
    private int[] metalValues;
    private int[] popValues;
    private int[] hiphopValues;
    private int[] electronicValues;
    private int[] jazzValues;

    private EQData testEQData;
    private EQPreset testEQPreset;

    private int valueBass = 50;
    private int valueLow = 60;
    private int valueMid = 70;
    private int valueUpper = 80;
    private int valueHigh = 90;

    @Before
    public void setup() {
        for (int i = 0; i < joplinEQPresetListManager.getPresetTypes().size(); i++) {
            presets.add(joplinEQPresetListManager.getPresetByIndex(i));
        }

        customValues = presets.get(0).getEqData().toIntArray();
        flatValues = presets.get(1).getEqData().toIntArray();
        rockValues = presets.get(2).getEqData().toIntArray();
        metalValues = presets.get(3).getEqData().toIntArray();
        popValues = presets.get(4).getEqData().toIntArray();
        hiphopValues = presets.get(5).getEqData().toIntArray();
        electronicValues = presets.get(6).getEqData().toIntArray();
        jazzValues = presets.get(7).getEqData().toIntArray();

        testEQData = new EQData(rockValues);
        testEQPreset = new EQPreset(testEQData, EqPresetType.ROCK);
    }

    @Test
    public void testEQPresetConstructor() {
        EQData testPresetCustom = new EQData(customValues);
        EQData testPresetFlat = new EQData(flatValues);
        EQData testPresetRock = new EQData(rockValues);
        EQData testPresetMetal = new EQData(metalValues);
        EQData testPresetPop = new EQData(popValues);
        EQData testPresetHiphop = new EQData(hiphopValues);
        EQData testPresetElectronic = new EQData(electronicValues);
        EQData testPresetJazz = new EQData(jazzValues);

        assertEquals(presets.get(0).getEqType(), joplinEQPresetListManager
                .getEqualizerPresetType(testPresetCustom));
        assertEquals(presets.get(1).getEqType(), joplinEQPresetListManager
                .getEqualizerPresetType(testPresetFlat));
        assertEquals(presets.get(2).getEqType(), joplinEQPresetListManager
                .getEqualizerPresetType(testPresetRock));
        assertEquals(presets.get(3).getEqType(), joplinEQPresetListManager
                .getEqualizerPresetType(testPresetMetal));
        assertEquals(presets.get(4).getEqType(), joplinEQPresetListManager
                .getEqualizerPresetType(testPresetPop));
        assertEquals(presets.get(5).getEqType(), joplinEQPresetListManager
                .getEqualizerPresetType(testPresetHiphop));
        assertEquals(presets.get(6).getEqType(), joplinEQPresetListManager
                .getEqualizerPresetType(testPresetElectronic));
        assertEquals(presets.get(7).getEqType(), joplinEQPresetListManager
                .getEqualizerPresetType(testPresetJazz));
    }

    @Test
    public void testSetAndGetValue() {
        testEQData.setValue(EQData.ValueType.BASS, valueBass);
        testEQData.setValue(EQData.ValueType.LOW, valueLow);
        testEQData.setValue(EQData.ValueType.MID, valueMid);
        testEQData.setValue(EQData.ValueType.UPPER, valueUpper);
        testEQData.setValue(EQData.ValueType.HIGH, valueHigh);

        testEQPreset.setEqType(EqPresetType.ROCK);

        assertTrue(new EQPreset(
                new EQData(new int[]{valueBass, valueLow, valueMid, valueUpper, valueHigh}),
                EqPresetType.ROCK)
                .equals(testEQPreset));

        assertEquals(valueBass, testEQData.getBass());
        assertEquals(valueLow, testEQData.getLow());
        assertEquals(valueMid, testEQData.getMid());
        assertEquals(valueUpper, testEQData.getUpper());
        assertEquals(valueHigh, testEQData.getHigh());
        assertEquals(EqPresetType.ROCK, testEQPreset.getEqType());
    }

    @Test
    public void testValuesEqual() {
        assertTrue(testEQData.equals(presets.get(2).getEqData()));
    }

    @Test
    public void testGettingTypeFromValues() {
        assertEquals(EqPresetType.CUSTOM, joplinEQPresetListManager
                .getEqualizerPresetType(new EQData(customValues)));
        assertEquals(EqPresetType.FLAT, joplinEQPresetListManager
                .getEqualizerPresetType(new EQData(flatValues)));
        assertEquals(EqPresetType.ROCK, joplinEQPresetListManager
                .getEqualizerPresetType(new EQData(rockValues)));
        assertEquals(EqPresetType.POP, joplinEQPresetListManager
                .getEqualizerPresetType(new EQData(popValues)));
        assertEquals(EqPresetType.HIPHOP, joplinEQPresetListManager
                .getEqualizerPresetType(new EQData(hiphopValues)));
        assertEquals(EqPresetType.ELECTRONIC, joplinEQPresetListManager
                .getEqualizerPresetType(new EQData(electronicValues)));
        assertEquals(EqPresetType.JAZZ, joplinEQPresetListManager
                .getEqualizerPresetType(new EQData(jazzValues)));
        assertEquals(EqPresetType.METAL, joplinEQPresetListManager
                .getEqualizerPresetType(new EQData(metalValues)));
    }
}

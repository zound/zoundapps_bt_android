package com.zoundindustries.marshallbt.model.adapters.joplin;

import com.zoundindustries.marshallbt.R;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyResourceProvider;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice;
import com.zoundindustries.marshallbt.services.DeviceService.DeviceImageType;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class TymphanyResourceProviderTest {

    private TymphanyDevice mockJoplinSpeaker;
    private TymphanyResourceProvider tymphanyResourceProvider;
    private DeviceInfo deviceInfo;

    @Before
    public void setUp() {
        mockJoplinSpeaker = mock(TymphanyDevice.class);
        tymphanyResourceProvider = new TymphanyResourceProvider();
        deviceInfo = mock(DeviceInfo.class);

        when(mockJoplinSpeaker.getDeviceInfo()).thenReturn(deviceInfo);
    }

    @Test
    public void testGetJoplinImageResourceId_ACTION_II() {
        // simulate we need image for JOPLIN_S speaker
        when(mockJoplinSpeaker.getDeviceSubType()).thenReturn(TymphanyDevice.DeviceSubType.JOPLIN_S);
        int resourceId;

        // take black images
        when(mockJoplinSpeaker.getDeviceInfo().getDeviceColor())
                .thenReturn(DeviceInfo.DeviceColor.BLACK);

        // take small image
        resourceId = tymphanyResourceProvider.getImageResourceId(mockJoplinSpeaker,
                DeviceImageType.SMALL);
        assertEquals(R.drawable.acton_ii_black_small, resourceId);

        // take medium image
        resourceId = tymphanyResourceProvider.getImageResourceId(mockJoplinSpeaker,
                DeviceImageType.MEDIUM);
        assertEquals(R.drawable.acton_ii_black_medium, resourceId);

        // take large image
        resourceId = tymphanyResourceProvider.getImageResourceId(mockJoplinSpeaker,
                DeviceImageType.LARGE);
        assertEquals(R.drawable.acton_ii_black_large, resourceId);

        //take white images
        when(mockJoplinSpeaker.getDeviceInfo().getDeviceColor())
                .thenReturn(DeviceInfo.DeviceColor.WHITE);

        // take small image
        resourceId = tymphanyResourceProvider.getImageResourceId(mockJoplinSpeaker,
                DeviceImageType.SMALL);
        assertEquals(R.drawable.acton_ii_white_small, resourceId);

        // take medium image
        resourceId = tymphanyResourceProvider.getImageResourceId(mockJoplinSpeaker,
                DeviceImageType.MEDIUM);
        assertEquals(R.drawable.acton_ii_white_medium, resourceId);

        // take large image
        resourceId = tymphanyResourceProvider.getImageResourceId(mockJoplinSpeaker,
                DeviceImageType.LARGE);
        assertEquals(R.drawable.acton_ii_white_large, resourceId);
    }

    @Test
    public void testGetJoplinImageResourceId_STANMORE_II() {
        // simulate we need image for JOPLIN_M speaker
        when(mockJoplinSpeaker.getDeviceSubType()).thenReturn(TymphanyDevice.DeviceSubType.JOPLIN_M);
        int resourceId;

        // take black images
        when(mockJoplinSpeaker.getDeviceInfo().getDeviceColor())
                .thenReturn(DeviceInfo.DeviceColor.BLACK);

        // take small image
        resourceId = tymphanyResourceProvider.getImageResourceId(mockJoplinSpeaker,
                DeviceImageType.SMALL);
        assertEquals(R.drawable.stanmore_ii_black_small, resourceId);

        // take medium image
        resourceId = tymphanyResourceProvider.getImageResourceId(mockJoplinSpeaker,
                DeviceImageType.MEDIUM);
        assertEquals(R.drawable.stanmore_ii_black_medium, resourceId);

        // take large image
        resourceId = tymphanyResourceProvider.getImageResourceId(mockJoplinSpeaker,
                DeviceImageType.LARGE);
        assertEquals(R.drawable.stanmore_ii_black_large, resourceId);

        //take white images
        when(mockJoplinSpeaker.getDeviceInfo().getDeviceColor())
                .thenReturn(DeviceInfo.DeviceColor.WHITE);

        // take small image
        resourceId = tymphanyResourceProvider.getImageResourceId(mockJoplinSpeaker,
                DeviceImageType.SMALL);
        assertEquals(R.drawable.stanmore_ii_white_small, resourceId);

        // take medium image
        resourceId = tymphanyResourceProvider.getImageResourceId(mockJoplinSpeaker,
                DeviceImageType.MEDIUM);
        assertEquals(R.drawable.stanmore_ii_white_medium, resourceId);

        // take large image
        resourceId = tymphanyResourceProvider.getImageResourceId(mockJoplinSpeaker,
                DeviceImageType.LARGE);
        assertEquals(R.drawable.stanmore_ii_white_large, resourceId);
    }

    @Test
    public void testGetJoplinImageResourceId_WOBURN_II() {
        // simulate we need image for JOPLIN_L speaker
        when(mockJoplinSpeaker.getDeviceSubType()).thenReturn(TymphanyDevice.DeviceSubType.JOPLIN_L);
        int resourceId;

        // take black images
        when(mockJoplinSpeaker.getDeviceInfo().getDeviceColor())
                .thenReturn(DeviceInfo.DeviceColor.BLACK);

        // take small image
        resourceId = tymphanyResourceProvider.getImageResourceId(mockJoplinSpeaker,
                DeviceImageType.SMALL);
        assertEquals(R.drawable.woburn_ii_black_small, resourceId);

        // take medium image
        resourceId = tymphanyResourceProvider.getImageResourceId(mockJoplinSpeaker,
                DeviceImageType.MEDIUM);
        assertEquals(R.drawable.woburn_ii_black_medium, resourceId);

        // take large image
        resourceId = tymphanyResourceProvider.getImageResourceId(mockJoplinSpeaker,
                DeviceImageType.LARGE);
        assertEquals(R.drawable.woburn_ii_black_large, resourceId);

        //take white images
        when(mockJoplinSpeaker.getDeviceInfo().getDeviceColor())
                .thenReturn(DeviceInfo.DeviceColor.WHITE);

        // take small image
        resourceId = tymphanyResourceProvider.getImageResourceId(mockJoplinSpeaker,
                DeviceImageType.SMALL);
        assertEquals(R.drawable.woburn_ii_white_small, resourceId);

        // take medium image
        resourceId = tymphanyResourceProvider.getImageResourceId(mockJoplinSpeaker,
                DeviceImageType.MEDIUM);
        assertEquals(R.drawable.woburn_ii_white_medium, resourceId);

        // take large image
        resourceId = tymphanyResourceProvider.getImageResourceId(mockJoplinSpeaker,
                DeviceImageType.LARGE);
        assertEquals(R.drawable.woburn_ii_white_large, resourceId);
    }
}
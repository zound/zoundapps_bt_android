package com.zoundindustries.marshallbt.model.device.state;

import com.zoundindustries.marshallbt.model.device.BaseDevice;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

@RunWith(JUnit4.class)
public class RowItemStateTest {

    private RowItemState rowItemState;

    @Before
    public void setup() {
        rowItemState = new RowItemState.RowItemStateBuilder().build();
    }

    @Test
    public void testPlaying() {
        rowItemState.setPlaying(true);
        assertTrue(rowItemState.isPlaying());

        rowItemState.setPlaying(false);
        assertFalse(rowItemState.isPlaying());
    }

    @Test
    public void testConnected() {
        rowItemState.setConnectionState(BaseDevice.ConnectionState.CONNECTED);
        assertEquals(BaseDevice.ConnectionState.CONNECTED, rowItemState.getConnectionState());

        rowItemState.setConnectionState(BaseDevice.ConnectionState.DISCONNECTED);
        assertEquals(BaseDevice.ConnectionState.DISCONNECTED,rowItemState.getConnectionState());
    }
}

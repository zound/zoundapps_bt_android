package com.zoundindustries.marshallbt.model.device;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import android.content.Context;

import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice;
import com.zoundindustries.marshallbt.services.DeviceService;
import com.zoundindustries.marshallbt.services.DeviceService.DeviceImageType;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import io.reactivex.subjects.BehaviorSubject;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DeviceManagerTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private DeviceService mockDeviceService;
    private TymphanyDevice mockDevice;

    private DeviceManager deviceManager;
    private BehaviorSubject<Boolean> testConnectionSubject = BehaviorSubject.create();

    @Before
    public void setup() {
        mockDeviceService = mock(DeviceService.class);
        mockDevice = mock(TymphanyDevice.class);

        BluetoothApplication mockApplication = mock(BluetoothApplication.class);
        Context mockContext = mock(Context.class);

        when(mockContext.getApplicationContext()).thenReturn(mockApplication);
        when(mockApplication.getDeviceService())
                .thenReturn(mockDeviceService);
        when(mockApplication.isServiceConnected()).thenReturn(testConnectionSubject);

        deviceManager = new DeviceManager(mockContext);
    }

    @Test
    public void testGetDeviceFromId() {

        //simulate device returned from service
        when(mockDeviceService.getDeviceFromId(any(String.class))).thenReturn(mockDevice);

        //simulate service connected
        testConnectionSubject.onNext(true);

        assertEquals(deviceManager.getDeviceFromId("123456789"), mockDevice);
    }

    @Test
    public void testGetSpeakerImageResourceId() {
        final int resIdExpectedValue = 0;
        int imageResourceId;

        // simulate getting any speaker image from DeviceService
        when(mockDeviceService.getImageResourceId(any(String.class), any(DeviceImageType.class)))
                .thenReturn(resIdExpectedValue);

        imageResourceId = mockDeviceService.getImageResourceId("123456789",
                DeviceImageType.SMALL);
        assertEquals(imageResourceId, resIdExpectedValue);
    }
}

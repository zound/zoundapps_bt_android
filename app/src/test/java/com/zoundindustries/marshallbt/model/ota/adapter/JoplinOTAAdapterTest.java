package com.zoundindustries.marshallbt.model.ota.adapter;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import android.content.Context;

import com.tym.tymappplatform.TAProtocol.TAProtocol.TASystem;
import com.tym.tymappplatform.TAService.TAOtaSignalDataProcessingService.TAOtaSignalDataProcessingService;
import com.tym.tymappplatform.utils.TACommonDefinitions;
import com.zoundindustries.marshallbt.TestUtils;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.state.BaseDeviceStateController;
import com.zoundindustries.marshallbt.model.device.state.TymphanyDeviceStateController;
import com.zoundindustries.marshallbt.model.device.tymphany.SpeakerInfo;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice;
import com.zoundindustries.marshallbt.model.firebase.FirebaseAnalyticsManager;
import com.zoundindustries.marshallbt.model.ota.IOTAService;
import com.zoundindustries.marshallbt.model.ota.OtaApi;
import com.zoundindustries.marshallbt.model.ota.SpeakerCacheManager;
import com.zoundindustries.marshallbt.model.ota.adapter.joplin.TymphanyOTAAdapter;
import com.zoundindustries.marshallbt.model.ota.pojo.FirmwareFileMetaData;
import com.zoundindustries.marshallbt.utils.FirmwareFileHelper;
import com.zoundindustries.marshallbt.utils.joplin.JoplinOtaMd5Checker;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import io.reactivex.observers.TestObserver;
import io.reactivex.subjects.BehaviorSubject;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class JoplinOTAAdapterTest {

    @Rule
    public TemporaryFolder storageDirectory = new TemporaryFolder();

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private TymphanyOTAAdapter joplinOTAAdapter;
    private File saveLocation;

    private BehaviorSubject<Response<FirmwareFileMetaData>> fileResponseSubject;
    private BehaviorSubject<Response<ResponseBody>> fileBodySubject;
    private BehaviorSubject<SpeakerInfo> speakerInfoSubject;
    private BehaviorSubject<BaseDevice.ConnectionState> connectionInfoSubject;

    private TymphanyDevice mockDevice;
    private Context mockContext;
    private Response mockFileResponse;
    private Response mockDownloadedFileResponse;
    private ResponseBody mockDownloadedFileResponseBody;
    private FirmwareFileMetaData mockFileMetaData;
    private FirmwareFileHelper mockFwFileHelper;
    private TASystem mockTaSystem;
    private File mockNonExistingSaveLocation;
    private BehaviorSubject<Boolean> unzipFileSubject;
    private FirebaseAnalyticsManager mockFirebaseAnalyticsManager;

    @Before
    public void setup() {
        TestUtils.setupRx();
        fileResponseSubject = BehaviorSubject.create();
        speakerInfoSubject = BehaviorSubject.create();
        connectionInfoSubject = BehaviorSubject.create();
        fileBodySubject = BehaviorSubject.create();
        fileBodySubject = BehaviorSubject.create();
        unzipFileSubject = BehaviorSubject.create();
        saveLocation = storageDirectory.getRoot();

        String deviceId = "123456789";
        String deviceName = "testName";

        mockDevice = mock(TymphanyDevice.class);
        mockContext = mock(Context.class);
        mockDownloadedFileResponse = mock(Response.class);
        mockDownloadedFileResponseBody = mock(ResponseBody.class);
        mockNonExistingSaveLocation = mock(File.class);
        mockFileResponse = mock(Response.class);
        mockFileMetaData = mock(FirmwareFileMetaData.class);
        mockFwFileHelper = mock(FirmwareFileHelper.class);
        mockTaSystem = mock(TASystem.class);
        mockFirebaseAnalyticsManager = mock(FirebaseAnalyticsManager.class);

        JoplinOtaMd5Checker mockJoplinOtaMd5Checker = mock(JoplinOtaMd5Checker.class);

        TAOtaSignalDataProcessingService mockTaOtaDSPService =
                mock(TAOtaSignalDataProcessingService.class);

        OtaApi mockOtaApi = mock(OtaApi.class);

        DeviceInfo mockDeviceInfo = mock(DeviceInfo.class);
        TymphanyDeviceStateController mockBaseStateController =
                mock(TymphanyDeviceStateController.class);

        BaseDeviceStateController.Inputs mockStateControllerInputs =
                mock(BaseDeviceStateController.Inputs.class);
        BaseDeviceStateController.Outputs mockStateControllerOutputs =
                mock(BaseDeviceStateController.Outputs.class);
        mockBaseStateController.inputs = mockStateControllerInputs;
        mockBaseStateController.outputs = mockStateControllerOutputs;

        when(mockDevice.getBaseDeviceStateController()).thenReturn(mockBaseStateController);
        when(mockDevice.getDeviceInfo()).thenReturn(mockDeviceInfo);
        when(mockDevice.getBaseDeviceStateController()).thenReturn(mockBaseStateController);
        when(mockDevice.getDeviceSubType()).thenReturn(TymphanyDevice.DeviceSubType.JOPLIN_S);
        when(mockDeviceInfo.getId()).thenReturn(deviceId);
        when(mockDeviceInfo.getDeviceType()).thenReturn(DeviceInfo.DeviceType.TYMPHANY_DEVICE);
        when(mockBaseStateController.getTaSystem()).thenReturn(mockTaSystem);
        when(mockStateControllerOutputs.getSpeakerInfo()).thenReturn(speakerInfoSubject);
        when(mockStateControllerOutputs.getConnectionInfo()).thenReturn(connectionInfoSubject);
        when(mockStateControllerOutputs.getConnectionInfoCurrentValue())
                .thenReturn(BaseDevice.ConnectionState.CONNECTED);
        when(mockStateControllerOutputs.getSpeakerInfoCurrentValue()).thenReturn(new SpeakerInfo());
        when(mockOtaApi.getDownloadURL(any(String.class),
                any(String.class),
                any(String.class),
                any(String.class))).thenReturn(fileResponseSubject);
        when(mockOtaApi.downloadOtaFile(any(String.class), any(String.class))).thenReturn(fileBodySubject);
        when(mockFileMetaData.getUrl()).thenReturn("http://testurl.com");
        when(mockFileMetaData.getVersion()).thenReturn("6.0.2");
        when(mockJoplinOtaMd5Checker.checkHexFileMD5()).thenReturn(true);
        when(mockFwFileHelper.getFirmwareMd5File()).thenReturn(mock(File.class));
        when(mockFwFileHelper.getFirmwareBinaryFile()).thenReturn(mock(File.class));
        when(mockFwFileHelper.getStoragePathBase()).thenReturn("storage/test/");
        when(mockFwFileHelper.unzipFirmwareFiles(any(File.class))).thenReturn(unzipFileSubject);

        doNothing().when(mockFwFileHelper).deleteUnzippedFiles();
        doNothing().when(mockTaOtaDSPService).subscribeOTAStatue(mockTaSystem);
        doNothing().when(mockFirebaseAnalyticsManager).eventForcedOtaStarted();
        doNothing().when(mockFirebaseAnalyticsManager).eventForcedOtaCheckStarted();
        doNothing().when(mockFirebaseAnalyticsManager).eventForcedOtaCheckFinished();

        joplinOTAAdapter = new TymphanyOTAAdapter(mockTaOtaDSPService,
                mockOtaApi,
                mockFwFileHelper,
                mockJoplinOtaMd5Checker,
                mockContext,
                new SpeakerCacheManager(mockContext),
                mockFirebaseAnalyticsManager,
                mock(HashMap.class));
    }

    @Test
    public void testStartOTAUpdateWithExistingFile() {
        when(mockFwFileHelper.getFirmwareZipFile(
                any(FirmwareFileMetaData.class)))
                .thenReturn(saveLocation);

        // we need to be connected to perform ota - we combine ota and connection states
        connectionInfoSubject.onNext(BaseDevice.ConnectionState.CONNECTED);

        joplinOTAAdapter.checkForFirmwareUpdate(mockDevice, false);

        joplinOTAAdapter.startOTAUpdate(mockDevice, mockFileMetaData);


        TestObserver<IOTAService.UpdateState> testOtaState =
                joplinOTAAdapter.getFirmwareUpdateState(mockDevice).test();

        unzipFileSubject.onNext(true);

        int index = 0;

        testOtaState.assertValueAt(index++, IOTAService.UpdateState.UNZIPPING);

        checkSdkFlashingStates(testOtaState, index);

        // TODO add progress value checking when progress is displayed correctly
    }

    @Test
    public void testStartOTAUpdateWithNonExistingFile() {
        when(mockDownloadedFileResponse.body()).thenReturn(mockDownloadedFileResponseBody);
        when(mockFwFileHelper.getFirmwareZipFile(any(FirmwareFileMetaData.class)))
                .thenReturn(mockNonExistingSaveLocation);
        when(mockFwFileHelper.writeResponseBodyToDisk(
                any(ResponseBody.class),
                any(File.class)))
                .thenReturn(true);


        connectionInfoSubject.onNext(BaseDevice.ConnectionState.CONNECTED);

        joplinOTAAdapter.checkForFirmwareUpdate(mockDevice, false);

        int index = 0;

        joplinOTAAdapter.startOTAUpdate(mockDevice, mockFileMetaData);

        TestObserver<IOTAService.UpdateState> testOtaState =
                joplinOTAAdapter.getFirmwareUpdateState(mockDevice).test();

        unzipFileSubject.onNext(true);

        testOtaState.assertValueAt(index++, IOTAService.UpdateState.DOWNLOADING_FROM_SERVER);

        fileBodySubject.onNext(mockDownloadedFileResponse);

        testOtaState.assertValueAt(index++, IOTAService.UpdateState.UNZIPPING);

        checkSdkFlashingStates(testOtaState, index);

        //todo add progress value checking
    }

    @Test
    public void testStartOTAUpdateFailedEmptyBody() {

        joplinOTAAdapter.startOTAUpdate(mockDevice, null);

        TestObserver<IOTAService.UpdateState> testOtaState =
                joplinOTAAdapter.getFirmwareUpdateState(mockDevice).test();

        testOtaState.onNext(IOTAService.UpdateState.DOWNLOADING_FROM_SERVER);

        int index = 0;

        testOtaState.assertValueAt(index++, IOTAService.UpdateState.NOT_STARTED);
        // checkForErrorMessages passes, so onError is invoked correctly, however...
        // todo assertError is not received, possible because throwables are not the same
        // testOtaState.assertError(new OtaErrorThrowable(OtaErrorThrowable.ErrorType.NULL_WEB_DATA));
    }

    @Test
    public void testCheckForFirmwareUpdateAvailable() throws InterruptedException {
        SpeakerInfo speakerInfo = new SpeakerInfo("testName",
                "00:00:aa:bb:cc:11",
                "joplin_s",
                "6.0.0(12345)");

        when(mockFileResponse.body()).thenReturn(mockFileMetaData);

        // to create update state subject
        joplinOTAAdapter.getFirmwareUpdateState(mockDevice);

        TestObserver<Response<FirmwareFileMetaData>> testCheckNewFirmware =
                joplinOTAAdapter.checkForFirmwareUpdate(mockDevice, false).test();

        //simulate rest api and speakerInfo responses
        fileResponseSubject.onNext(mockFileResponse);
        speakerInfoSubject.onNext(speakerInfo);

        testCheckNewFirmware.assertValueAt(0, mockFileResponse);
    }

    @Test
    public void testCheckForFirmwareUpdateNotAvailable() {
        when(mockFileResponse.body()).thenReturn(mockFileMetaData);
        SpeakerInfo speakerInfo = new SpeakerInfo("testName",
                "00:00:aa:bb:cc:11",
                "joplin_s",
                "3.0.5(12345)");

        // to create update state subject
        joplinOTAAdapter.getFirmwareUpdateState(mockDevice);

        TestObserver<Response<FirmwareFileMetaData>> testCheckNewFirmware =
                joplinOTAAdapter.checkForFirmwareUpdate(mockDevice, false).test();

        //simulate rest api and speakerInfo responses
        fileResponseSubject.onNext(mockFileResponse);
        speakerInfoSubject.onNext(speakerInfo);

        testCheckNewFirmware.assertComplete();
    }

    private void checkSdkFlashingStates(TestObserver<IOTAService.UpdateState> testOtaState,
                                        int index) {
        joplinOTAAdapter.didUpdateOtaStatusMessage(mockTaSystem,
                TACommonDefinitions.OTAStatusType.Downloading,
                0.1f,
                null);

        //there is additional UPLOADING_TO_DEVICE value set after unzip
        index++;

        testOtaState.assertValueAt(index++, IOTAService.UpdateState.UPLOADING_TO_DEVICE);

        joplinOTAAdapter.didUpdateOtaStatusMessage(mockTaSystem,
                TACommonDefinitions.OTAStatusType.Activating,
                0.7f,
                null);

        testOtaState.assertValueAt(index++, IOTAService.UpdateState.FLASHING_DEVICE);

        joplinOTAAdapter.didUpdateOtaStatusMessage(mockTaSystem,
                TACommonDefinitions.OTAStatusType.MCUUpgrading,
                0.7f,
                null);

        testOtaState.assertValueAt(index++, IOTAService.UpdateState.FLASHING_DEVICE);

        joplinOTAAdapter.didUpdateOtaStatusMessage(mockTaSystem,
                TACommonDefinitions.OTAStatusType.MCUUpgradeFinish,
                1.0f,
                null);

        testOtaState.assertValueAt(index, IOTAService.UpdateState.COMPLETED);
    }

    private boolean checkForErrorMessages(List<Throwable> errors, String message) {
        boolean isCorrectMessage = false;

        for (Throwable error : errors) {
            if (error.getMessage().equals(message)) {
                isCorrectMessage = true;
            }
        }
        return isCorrectMessage;
    }
}

package com.zoundindustries.marshallbt.model.device.tymphany;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.tym.tymappplatform.TAProtocol.TAProtocol.TASystem;
import com.tym.tymappplatform.TAService.TADigitalSignalProcessingService.TADigitalSignalProcessingService;
import com.tym.tymappplatform.TAService.TAPlayControlService.TAPlayControlService;
import com.tym.tymappplatform.TAService.TASystemService.TASystemService;
import com.zoundindustries.marshallbt.model.device.BaseDevice.NameState;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.mockito.Mockito.mock;

@RunWith(JUnit4.class)
public class TymphanyDeviceTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private TymphanyDevice speaker;

    @Before
    public void setup() {
        speaker = new SmallJoplinSpeaker(mock(DeviceInfo.class), mock(TASystemService.class),
                mock(TAPlayControlService.class), mock(TADigitalSignalProcessingService.class),
                mock(TASystem.class));
    }

    @Test
    public void testGetValidityStateWithEmptyName() {
        Assert.assertEquals(speaker.getValidityState(""), NameState.EMPTY);
        Assert.assertEquals(speaker.getValidityState("  "), NameState.EMPTY);
    }

    @Test
    public void testGetValidityStateWithValidName() {
        Assert.assertEquals(speaker.getValidityState("valid name"), NameState.VALID);
        Assert.assertEquals(speaker.getValidityState("@34%?(*"), NameState.VALID);
        Assert.assertEquals(speaker.getValidityState("      t"), NameState.VALID);
    }

    @Test
    public void testGetValidityStateWithTooLongName() {
        Assert.assertEquals(speaker.getValidityState("wwwwwwwwwwwwwwwwww"), NameState.TOO_LONG);
    }
}

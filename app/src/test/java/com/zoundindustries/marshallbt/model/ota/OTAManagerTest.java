package com.zoundindustries.marshallbt.model.ota;

import android.content.Context;

import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice;
import com.zoundindustries.marshallbt.model.ota.pojo.FirmwareFileMetaData;
import com.zoundindustries.marshallbt.services.DeviceService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import io.reactivex.observers.TestObserver;
import io.reactivex.subjects.BehaviorSubject;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class OTAManagerTest {

    private DeviceService mockDeviceService;
    private TymphanyDevice mockJoplinSpeaker;

    private OTAManager otaManager;
    private BehaviorSubject<Boolean> testConnectionSubject = BehaviorSubject.create();
    private FirmwareFileMetaData mockFileMetaData;

    @Before
    public void setup() {
        BluetoothApplication mockApplication = mock(BluetoothApplication.class);
        Context mockContext = mock(Context.class);

        mockDeviceService = mock(DeviceService.class);
        mockJoplinSpeaker = mock(TymphanyDevice.class);
        mockFileMetaData = mock(FirmwareFileMetaData.class);

        when(mockContext.getApplicationContext()).thenReturn(mockApplication);
        when(mockApplication.getDeviceService())
                .thenReturn(mockDeviceService);
        when(mockApplication.isServiceConnected()).thenReturn(testConnectionSubject);

        otaManager = new OTAManager(mockContext);
    }

    @Test
    public void testGetFirmwareUpdateProgress() {
        String testDeviceId = "123456789";

        //Learn mock service to return mock values
        BehaviorSubject<Double> testSubject = BehaviorSubject.create();
        when(mockDeviceService.getDeviceFromId(testDeviceId)).thenReturn(mockJoplinSpeaker);
        when(mockDeviceService.getFirmwareUpdateProgress(mockJoplinSpeaker))
                .thenReturn(testSubject);

        //Simulate service connected
        testConnectionSubject.onNext(true);

        //Start scanning
        TestObserver<Double> test = otaManager.getFirmwareUpdateProgress(testDeviceId).test();

        //Simulate update progress
        test.onNext(50.0);

        test.assertValue(50.0);
    }

    @Test
    public void testGetFirmwareUpdateState() {
        String testDeviceId = "123456789";

        //Learn mock service to return mock values
        BehaviorSubject<IOTAService.UpdateState> testSubject = BehaviorSubject.create();
        when(mockDeviceService.getDeviceFromId(testDeviceId)).thenReturn(mockJoplinSpeaker);
        when(mockDeviceService.getFirmwareUpdateState(mockJoplinSpeaker))
                .thenReturn(testSubject);

        //Simulate service connected
        otaManager.isServiceConnected();
        testConnectionSubject.onNext(true);

        //Start scanning
        TestObserver<IOTAService.UpdateState> test = otaManager.getFirmwareUpdateState(testDeviceId)
                .test();

        int index = 0;

        //Simulate update state
        test.onNext(IOTAService.UpdateState.NOT_STARTED);
        test.assertValueAt(index++, IOTAService.UpdateState.NOT_STARTED);

        test.onNext(IOTAService.UpdateState.DOWNLOADING_FROM_SERVER);
        test.assertValueAt(index++, IOTAService.UpdateState.DOWNLOADING_FROM_SERVER);

        test.onNext(IOTAService.UpdateState.FLASHING_DEVICE);
        test.assertValueAt(index++, IOTAService.UpdateState.FLASHING_DEVICE);

        test.onNext(IOTAService.UpdateState.UPLOADING_TO_DEVICE);
        test.assertValueAt(index++, IOTAService.UpdateState.UPLOADING_TO_DEVICE);

        test.onNext(IOTAService.UpdateState.COMPLETED);
        test.assertValueAt(index, IOTAService.UpdateState.COMPLETED);
    }

    @Test
    public void tesStartOTAUpdate() {
        String testDeviceId = "123456789";
        when(mockDeviceService.getDeviceFromId(testDeviceId)).thenReturn(mockJoplinSpeaker);

        //Simulate service connected
        otaManager.isServiceConnected();
        testConnectionSubject.onNext(true);

        otaManager.startOTAUpdate(testDeviceId, mockFileMetaData);

        verify(mockDeviceService, times(1))
                .startOTAUpdate(mockJoplinSpeaker, mockFileMetaData);
    }
}

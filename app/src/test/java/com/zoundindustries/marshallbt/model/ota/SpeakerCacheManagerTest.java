package com.zoundindustries.marshallbt.model.ota;

import android.app.Application;

import com.zoundindustries.marshallbt.model.ota.pojo.SpeakerInfoCache;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.io.InputStream;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
public class SpeakerCacheManagerTest {
    private SpeakerCacheManager speakerCacheManager;
    private Application application;
    private String mockMacAddress = "mockMacAddress";
    private String mockMacAddress2 = "mockMacAddress2";

    @Before
    public void setUp() {
        application = RuntimeEnvironment.application;
        speakerCacheManager = new SpeakerCacheManager(application);
    }

    @Test
    public void testAddSpeakerInfoToCacheOneSpeaker() {
        assertTrue(speakerCacheManager.addSpeakerInfoCache(mockMacAddress));
        assertEquals(1, speakerCacheManager.getSpeakerInfoCacheList().size());
    }

    @Test
    public void testAddSpeakerInfoToCacheTwoSpeakers() {
        assertTrue(speakerCacheManager.addSpeakerInfoCache(mockMacAddress));
        assertTrue(speakerCacheManager.addSpeakerInfoCache(mockMacAddress2));
        assertEquals(2, speakerCacheManager.getSpeakerInfoCacheList().size());
    }

    @Test
    public void testAddSpeakerInfoToCacheSameSpeakerTwice() {
        assertTrue(speakerCacheManager.addSpeakerInfoCache(mockMacAddress));
        assertFalse(speakerCacheManager.addSpeakerInfoCache(mockMacAddress));
        assertEquals(1, speakerCacheManager.getSpeakerInfoCacheList().size());
    }

    @Test
    public void testUpdateTwcStatusOfSpeakerExistingSpeaker() {
        assertTrue(speakerCacheManager.addSpeakerInfoCache(mockMacAddress));
        speakerCacheManager.updateTwsStatusOfSpeaker(mockMacAddress, true, null, null);
        assertTrue(Objects.requireNonNull(speakerCacheManager.getSpeakerInfoCache(mockMacAddress)).getIsTwsCoupled());
        assertNotNull(Objects.requireNonNull(speakerCacheManager.getSpeakerInfoCache(mockMacAddress)).getTwsChannelType());
        assertNotNull(Objects.requireNonNull(speakerCacheManager.getSpeakerInfoCache(mockMacAddress)).getTwsConnectionInfo());
        assertEquals(1, speakerCacheManager.getSpeakerInfoCacheList().size());
    }

    @Test
    public void testUpdateTwcStatusOfSpeakerNonExistingSpeaker() {
        speakerCacheManager.updateTwsStatusOfSpeaker(mockMacAddress, true, null, null);
        assertEquals(0, speakerCacheManager.getSpeakerInfoCacheList().size());
    }

    @Test
    public void testIsSpeakerInfoCachedExistingSpeaker() {
        assertTrue(speakerCacheManager.addSpeakerInfoCache(mockMacAddress));
        assertTrue(Objects.requireNonNull(speakerCacheManager.isSpeakerInfoCached(mockMacAddress)));
    }

    @Test
    public void testIsSpeakerInfoCachedNonExistingSpeaker() {
        assertFalse(Objects.requireNonNull(speakerCacheManager.isSpeakerInfoCached(mockMacAddress)));
    }

    @Test
    public void testCheckIfUpdatedSpeakerInfoCacheIsStoredNullValues() {
        assertTrue(speakerCacheManager.addSpeakerInfoCache(mockMacAddress));
        speakerCacheManager.updateTwsStatusOfSpeaker(mockMacAddress, true, null, null);
        assertTrue(Objects.requireNonNull(speakerCacheManager.getSpeakerInfoCache(mockMacAddress)).getIsTwsCoupled());
        assertNotNull(Objects.requireNonNull(speakerCacheManager.getSpeakerInfoCache(mockMacAddress)).getTwsChannelType());
        assertNotNull(Objects.requireNonNull(speakerCacheManager.getSpeakerInfoCache(mockMacAddress)).getTwsConnectionInfo());
        assertEquals(1, speakerCacheManager.getSpeakerInfoCacheList().size());

        speakerCacheManager = new SpeakerCacheManager(application);
        assertEquals(1, speakerCacheManager.getSpeakerInfoCacheList().size());
        assertNotNull(speakerCacheManager.getSpeakerInfoCache(mockMacAddress));
        assertTrue(Objects.requireNonNull(speakerCacheManager.getSpeakerInfoCache(mockMacAddress)).getIsTwsCoupled());
        assertNotNull(Objects.requireNonNull(speakerCacheManager.getSpeakerInfoCache(mockMacAddress)).getTwsChannelType());
        assertNotNull(Objects.requireNonNull(speakerCacheManager.getSpeakerInfoCache(mockMacAddress)).getTwsConnectionInfo());
    }

    @Test
    public void testCheckIfUpdatedSpeakerInfoCacheIsStoredNonNullValues() {
        assertTrue(speakerCacheManager.addSpeakerInfoCache(mockMacAddress));
        speakerCacheManager.updateTwsStatusOfSpeaker(mockMacAddress, true, mockMacAddress, mockMacAddress);
        assertTrue(Objects.requireNonNull(speakerCacheManager.getSpeakerInfoCache(mockMacAddress)).getIsTwsCoupled());
        assertNotNull(Objects.requireNonNull(speakerCacheManager.getSpeakerInfoCache(mockMacAddress)).getTwsChannelType());
        assertNotNull(Objects.requireNonNull(speakerCacheManager.getSpeakerInfoCache(mockMacAddress)).getTwsConnectionInfo());
        assertEquals(1, speakerCacheManager.getSpeakerInfoCacheList().size());

        speakerCacheManager = new SpeakerCacheManager(application);
        assertEquals(1, speakerCacheManager.getSpeakerInfoCacheList().size());
        assertNotNull(speakerCacheManager.getSpeakerInfoCache(mockMacAddress));
        assertTrue(Objects.requireNonNull(speakerCacheManager.getSpeakerInfoCache(mockMacAddress)).getIsTwsCoupled());
        assertEquals(mockMacAddress, Objects.requireNonNull(speakerCacheManager.getSpeakerInfoCache(mockMacAddress)).getTwsChannelType());
        assertEquals(mockMacAddress, Objects.requireNonNull(speakerCacheManager.getSpeakerInfoCache(mockMacAddress)).getTwsConnectionInfo());
    }

    @Test
    public void testGtSpeakerInfoFromCacheNoExisting() {
        assertNull(speakerCacheManager.getSpeakerInfoCache(mockMacAddress));
    }

    @Test
    public void testGSpeakerInfoFromCacheExisting() {
        assertTrue(speakerCacheManager.addSpeakerInfoCache(mockMacAddress));
        assertNotNull(speakerCacheManager.getSpeakerInfoCache(mockMacAddress));
    }

    @Test
    public void testParseMockJsonFileWithSpeakersInfo() {
        InputStream mockInputStream = ClassLoader.getSystemResourceAsStream("mockSpeakerCacheInfo.json");
        List<SpeakerInfoCache> speakerInfoListFromCache = speakerCacheManager.parseDataTestPurpose(mockInputStream).getSpeakerInfoCacheList();
        assertNotNull(speakerInfoListFromCache);
        assertTrue(speakerInfoListFromCache.size() == 2);
        assertTrue(speakerInfoListFromCache.get(0).getMacAddress().equals(mockMacAddress));
        assertTrue(speakerInfoListFromCache.get(1).getMacAddress().equals(mockMacAddress2));
    }

    @Test
    public void testRemoveExistingItem() {
        assertTrue(speakerCacheManager.addSpeakerInfoCache(mockMacAddress));
        assertTrue(speakerCacheManager.removeSpeakerInfoCache(mockMacAddress));
        assertEquals(0, speakerCacheManager.getSpeakerInfoCacheList().size());
    }

    @Test
    public void testRemoveNonExistingItem() {
        assertFalse(speakerCacheManager.removeSpeakerInfoCache(mockMacAddress));
        assertEquals(0, speakerCacheManager.getSpeakerInfoCacheList().size());
    }
}
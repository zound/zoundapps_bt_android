package com.zoundindustries.marshallbt.model.device.state;

import android.os.Handler;

import com.tym.tymappplatform.TAProtocol.TAProtocol.TAProtocol;
import com.tym.tymappplatform.TAProtocol.TAProtocol.TASystem;
import com.tym.tymappplatform.TAService.TADigitalSignalProcessingService.TADigitalSignalProcessingService;
import com.tym.tymappplatform.TAService.TAPlayControlService.TAPlayControlService;
import com.tym.tymappplatform.TAService.TASystemService.TASystemService;
import com.tym.tymappplatform.utils.TACommonDefinitions;
import com.zoundindustries.marshallbt.TestUtils;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.tymphany.SpeakerInfo;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice;
import com.zoundindustries.marshallbt.model.devicesettings.EQData;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class JoplinDeviceStateControllerTest {

    private TASystem mockTaSystem;
    private Handler mockHandler;
    private TymphanyDeviceStateController joplinDeviceStateController;

    @Before
    public void setup() {
        TestUtils.setupRx();
        mockTaSystem = mock(TASystem.class);
        mockHandler = mock(Handler.class);

        TASystemService mockTaSystemService = mock(TASystemService.class);
        TAPlayControlService mockTaPlayControlService = mock(TAPlayControlService.class);

        mockTaSystem.deviceName = "testName";
        mockTaSystem.deviceAddress = "123456789";

        when(mockTaSystemService.connectToSystem(mockTaSystem)).thenReturn(true);
        doNothing().when(mockTaPlayControlService).writeVolume(any(TASystem.class), any(Integer.class));
        doNothing().when(mockTaPlayControlService).subscribeVolume(any(TASystem.class));
        setupMockHandler();
        TADigitalSignalProcessingService mockTaDspService = mock(TADigitalSignalProcessingService.class);

        joplinDeviceStateController =
                new TymphanyDeviceStateController(
                        FeaturesDefs.CONNECTION_INFO |
                                FeaturesDefs.VOLUME |
                                FeaturesDefs.PLAY_CONTROL |
                                FeaturesDefs.SPEAKER_INFO |
                                FeaturesDefs.EQ |
                                FeaturesDefs.LIGHT |
                                FeaturesDefs.RENAME |
                                FeaturesDefs.SOUNDS_SETTINGS |
                                FeaturesDefs.COUPLING_CHANNEL |
                                FeaturesDefs.COUPLING_CONNECTION,
                        TymphanyDevice.DeviceSubType.JOPLIN_L,
                        mockTaSystemService,
                        mockTaPlayControlService,
                        mockTaDspService,
                        mockTaSystem,
                        mockHandler);
    }

    private void setupMockHandler() {
        when(mockHandler.post(any(Runnable.class))).thenAnswer(invocation -> {
            Runnable runnable = invocation.getArgument(0);
            runnable.run();
            return null;
        });
    }

    @Test
    public void testSetConnected() {

        TestObserver<BaseDevice.ConnectionState> testObserver =
                joplinDeviceStateController.outputs.getConnectionInfo().test();

        joplinDeviceStateController.inputs.setConnectionInfo(BaseDevice.ConnectionState.CONNECTED);

        int index = 0;

        // Check that initial value was set. This is set by initPreConnectionStateValues
        testObserver.assertValueAt(index++, BaseDevice.ConnectionState.DISCONNECTED);

        joplinDeviceStateController
                .didConnectionStateChanged(mockTaSystem, TAProtocol.kDeviceStatusConnected);
        testObserver.assertValueAt(index++, BaseDevice.ConnectionState.CONNECTING);

        testObserver.assertValueAt(index++, BaseDevice.ConnectionState.CONNECTED);

        joplinDeviceStateController
                .didConnectionStateChanged(mockTaSystem, TAProtocol.kDeviceStatusDisconnected);
        testObserver.assertValueAt(index, BaseDevice.ConnectionState.DISCONNECTED);
    }

    @Test
    public void testSetPlaying() {

        TestObserver<Boolean> testObserver =
                joplinDeviceStateController.outputs.getPlaying().test();

        int index = 0;

        // Check that initial value was set. This is set by initPreConnectionStateValues
        testObserver.assertValueAt(index++, false);

        // Simulate SDK call with  Playing status
        joplinDeviceStateController.didConnectionStateChanged(mockTaSystem, 0);
        joplinDeviceStateController.didUpdateAudioStatus(mockTaSystem,
                TACommonDefinitions.AudioSourceType.NotSource,
                TACommonDefinitions.PlayBackStatusType.Playing,
                0,
                false, false);

        testObserver.assertValueAt(index++, true);

        // Simulate SDK call with Pause status
        joplinDeviceStateController.didConnectionStateChanged(mockTaSystem, 0);
        joplinDeviceStateController.didUpdateAudioStatus(mockTaSystem,
                TACommonDefinitions.AudioSourceType.NotSource,
                TACommonDefinitions.PlayBackStatusType.Pause,
                0,
                false, false);

        testObserver.assertValueAt(index++, false);

        // Simulate SDK call with Stop status
        joplinDeviceStateController.didConnectionStateChanged(mockTaSystem, 0);
        joplinDeviceStateController.didUpdateAudioStatus(mockTaSystem,
                TACommonDefinitions.AudioSourceType.NotSource,
                TACommonDefinitions.PlayBackStatusType.Stop,
                0,
                false, false);

        testObserver.assertValueAt(index, false);
    }

    @Test
    public void testSetVolume() {
        TestObserver<Integer> testObserver =
                joplinDeviceStateController.outputs.getVolume().test();

        joplinDeviceStateController.didUpdateVolume(mockTaSystem, 44);

        int index = 0;
        testObserver.assertValueAt(index++, 44);

        joplinDeviceStateController.didUpdateVolume(mockTaSystem, 0);

        testObserver.assertValueAt(index, 0);
    }

    @Test
    public void testSetEQ() {
        TestObserver<EQData> testObserver =
                joplinDeviceStateController.outputs.getEQ().test();

        int[] data = new int[]{1, 1, 1, 1, 0};

        int[] scaledData = new int[]{10, 10, 10, 10, 0};
        EQData scaledPreset = new EQData(scaledData);

        joplinDeviceStateController.didUpdateEqualiser(mockTaSystem, data);

        testObserver.assertValueAt(0, scaledPreset);
    }

    @Test
    public void testSetBrightness() {
        TestObserver<Integer> testObserver =
                joplinDeviceStateController.outputs.getBrightness().test();

        joplinDeviceStateController.didUpdateBrightness(mockTaSystem, 70);

        int index = 0;
        testObserver.assertValueAt(index++, 100);

        joplinDeviceStateController.didUpdateBrightness(mockTaSystem, 35);

        testObserver.assertValueAt(index, 0);
    }

    @Test
    public void testSetSpeakerInfo() throws InterruptedException {

        String modelName = "Model-1";
        String firmwareVersion = "1.2";
        String coupleId = "00:01:A1:02:C3:07";

        TestObserver<SpeakerInfo> testObserver =
                joplinDeviceStateController.outputs.getSpeakerInfo().test();

        joplinDeviceStateController.didConnectionStateChanged(mockTaSystem, 0);
        joplinDeviceStateController.didUpdateModelNumber(mockTaSystem, modelName);
        joplinDeviceStateController.didUpdateFirmwareRevision(mockTaSystem, firmwareVersion);
        joplinDeviceStateController.didUpdateTrueWireless(mockTaSystem,
                TACommonDefinitions.TWSStatusType.ConnectedAsMaster,
                TACommonDefinitions.ChannelType.Party,
                new byte[]{0, 1, -95, 2, -61, 7});
        joplinDeviceStateController.didUpdateDeviceInformation(mockTaSystem,
                mockTaSystem.deviceAddress.getBytes(),
                mockTaSystem.deviceName,
                new byte[]{0, 1, -95, 2, -61, 7});

        SpeakerInfo speakerInfo = new SpeakerInfo(mockTaSystem.deviceName,
                mockTaSystem.deviceAddress,
                modelName,
                firmwareVersion);

        testObserver.assertValueAt(1, speakerInfo);
    }
}

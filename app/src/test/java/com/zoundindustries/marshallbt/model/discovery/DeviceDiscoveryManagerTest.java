package com.zoundindustries.marshallbt.model.discovery;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import android.content.Context;

import com.zoundindustries.marshallbt.BluetoothApplication;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.tymphany.TymphanyDevice;
import com.zoundindustries.marshallbt.services.DeviceService;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.observers.TestObserver;
import io.reactivex.subjects.BehaviorSubject;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class DeviceDiscoveryManagerTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private BluetoothApplication mockApplication;
    private Context mockContext;
    private DeviceService mockDeviceService;
    private TymphanyDevice mockJoplinSpeaker;

    private DeviceDiscoveryManager deviceDiscoveryManager;
    private BehaviorSubject<Boolean> testConnectionSubject = BehaviorSubject.create();

    @Before
    public void setup() {
        mockApplication = mock(BluetoothApplication.class);
        mockContext = mock(Context.class);
        mockDeviceService = mock(DeviceService.class);
        mockJoplinSpeaker = mock(TymphanyDevice.class);

        when(mockContext.getApplicationContext()).thenReturn(mockApplication);
        when(mockApplication.getDeviceService())
                .thenReturn(mockDeviceService);
        when(mockApplication.isServiceConnected()).thenReturn(testConnectionSubject);

        deviceDiscoveryManager = new DeviceDiscoveryManager(mockContext);
    }

    @Test
    public void testStartScanForDevices() {
        BehaviorSubject<BaseDevice> testSubject = BehaviorSubject.create();
        when(mockDeviceService.getDiscoveredDevices()).thenReturn(testSubject);

        //Simulate service connected
        testConnectionSubject.onNext(true);

        //Start scanning
        TestObserver<BaseDevice> test = deviceDiscoveryManager.getDiscoveredDevices().test();

        //Simulate device being found
        test.onNext(mockJoplinSpeaker);

        test.assertValue(mockJoplinSpeaker);
    }

    @Test
    public void testStopScanForDevices() {
        BehaviorSubject<BaseDevice> testSubject = BehaviorSubject.create();
        when(mockDeviceService.getDiscoveredDevices()).thenReturn(testSubject);

        List<BaseDevice> testDevices = new ArrayList<>();
        testDevices.add(mockJoplinSpeaker);

        //Simulate service connected
        testConnectionSubject.onNext(true);

        //Start scanning
        deviceDiscoveryManager.stopScanForDevices();

        verify(mockDeviceService, times(1)).stopScanForDevices();
    }
}

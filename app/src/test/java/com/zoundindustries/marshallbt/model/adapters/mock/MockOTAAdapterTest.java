package com.zoundindustries.marshallbt.model.adapters.mock;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.mock.MockSpeaker;
import com.zoundindustries.marshallbt.model.ota.IOTAService;
import com.zoundindustries.marshallbt.model.ota.OtaApi;
import com.zoundindustries.marshallbt.model.ota.adapter.mock.MockOTAAdapter;
import com.zoundindustries.marshallbt.model.ota.pojo.FirmwareFileMetaData;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import io.reactivex.observers.TestObserver;
import io.reactivex.subjects.BehaviorSubject;
import retrofit2.Response;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SuppressWarnings("unchecked")
@RunWith(JUnit4.class)
public class MockOTAAdapterTest {

    private MockOTAAdapter mockOTAAdapter;
    private MockSDK mockSDK;

    private OtaApi mockOtaApi;
    private BaseDevice mockDevice;
    private FirmwareFileMetaData mockFileMetaData;


    @Before
    public void setup() {
        mockOtaApi = mock(OtaApi.class);
        mockDevice = mock(MockSpeaker.class);
        mockFileMetaData = mock(FirmwareFileMetaData.class);


        mockSDK = new MockSDK();
        mockOTAAdapter = new MockOTAAdapter(mockSDK, mockOtaApi);
    }

    private BaseDevice getDevice() {
        return new MockSpeaker(new DeviceInfo("123456",
                DeviceInfo.DeviceType.MOCK,
                DeviceInfo.DeviceGroup.SPEAKER,
                "TestName",
                DeviceInfo.DeviceColor.BLACK),
                mockSDK);
    }

    @Test
    public void testStartOtaUpdate() {
        BaseDevice testDevice = getDevice();
        BehaviorSubject<Response<FirmwareFileMetaData>> testSubject = BehaviorSubject.create();
        when(mockOtaApi.getDownloadURL(any(String.class), any(String.class), any(String.class),
                any(String.class))).thenReturn(testSubject);

        TestObserver<IOTAService.UpdateState> test =
                mockOTAAdapter.getFirmwareUpdateState(testDevice).test();

        mockOTAAdapter.startOTAUpdate(testDevice, mockFileMetaData);

        test.assertValue(IOTAService.UpdateState.DOWNLOADING_FROM_SERVER);
    }
}

package com.zoundindustries.marshallbt.utils.comparators;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;
import com.zoundindustries.marshallbt.viewmodels.BaseJoplinSdkViewModelTest;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class HomeListConnectionComparatorTest extends BaseJoplinSdkViewModelTest {

    private HomeListConnectionComparator homeListConnectionComparator;

    @Before
    public void setup() {
        super.baseSetup();

        when(mockBaseStateController.isFeatureSupported(FeaturesDefs.CONNECTION_INFO))
                .thenReturn(true);
        when(mockBaseStateController2.isFeatureSupported(FeaturesDefs.CONNECTION_INFO))
                .thenReturn(true);
        homeListConnectionComparator = new HomeListConnectionComparator();
    }

    @Test
    public void testListCompareFirstGreater() {
        when(mockBaseStateController.outputs.getConnectionInfoCurrentValue())
                .thenReturn(BaseDevice.ConnectionState.CONNECTED);
        when(mockBaseStateController2.outputs.getConnectionInfoCurrentValue())
                .thenReturn(BaseDevice.ConnectionState.DISCONNECTED);

        int testResult = homeListConnectionComparator.compare(mockDevice, mockDevice2);

        assertTrue(testResult < 0);
    }

    @Test
    public void testListCompareFirstLesser() {

        when(mockBaseStateController.outputs.getConnectionInfoCurrentValue())
                .thenReturn(BaseDevice.ConnectionState.DISCONNECTED);
        when(mockBaseStateController2.outputs.getConnectionInfoCurrentValue())
                .thenReturn(BaseDevice.ConnectionState.CONNECTED);

        int testResult = homeListConnectionComparator.compare(mockDevice, mockDevice2);

        assertTrue(testResult > 0);
    }

    @Test
    public void testListCompareEqualConnected() {

        when(mockBaseStateController.outputs.getConnectionInfoCurrentValue())
                .thenReturn(BaseDevice.ConnectionState.CONNECTED);
        when(mockBaseStateController2.outputs.getConnectionInfoCurrentValue())
                .thenReturn(BaseDevice.ConnectionState.CONNECTED);

        int testResult = homeListConnectionComparator.compare(mockDevice, mockDevice2);


        assertTrue(testResult == 0);
    }

    @Test
    public void testListCompareEqualDisconnected() {

        when(mockBaseStateController.outputs.getConnectionInfoCurrentValue())
                .thenReturn(BaseDevice.ConnectionState.DISCONNECTED);
        when(mockBaseStateController2.outputs.getConnectionInfoCurrentValue())
                .thenReturn(BaseDevice.ConnectionState.DISCONNECTED);

        int testResult = homeListConnectionComparator.compare(mockDevice, mockDevice2);


        assertTrue(testResult == 0);
    }
}

package com.zoundindustries.marshallbt.utils.comparators;

import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.utils.BluetoothSystemUtils;
import com.zoundindustries.marshallbt.utils.comparators.HomeListPairingComparator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class HomeListPairingComparatorTest {

    private BluetoothSystemUtils mockBluetoothSystemUtils;
    private BaseDevice mockDevice;
    private BaseDevice mockDevice2;

    private HomeListPairingComparator homeListComparator;

    @Before
    public void setup() {
        mockBluetoothSystemUtils = mock(BluetoothSystemUtils.class);
        mockDevice = mock(BaseDevice.class);
        mockDevice2 = mock(BaseDevice.class);

        homeListComparator = new HomeListPairingComparator(mockBluetoothSystemUtils);

        when(mockBluetoothSystemUtils.isDevicePaired(mockDevice)).thenReturn(true);
        when(mockBluetoothSystemUtils.isDevicePaired(mockDevice2)).thenReturn(false);
    }

    @Test
    public void testListCompareFirstGreater() {
        when(mockBluetoothSystemUtils.isDevicePaired(mockDevice)).thenReturn(true);
        when(mockBluetoothSystemUtils.isDevicePaired(mockDevice2)).thenReturn(false);

        int testResult = homeListComparator.compare(mockDevice, mockDevice2);

        assertTrue(testResult < 0);
    }

    @Test
    public void testListCompareFirstLesser() {
        when(mockBluetoothSystemUtils.isDevicePaired(mockDevice)).thenReturn(false);
        when(mockBluetoothSystemUtils.isDevicePaired(mockDevice2)).thenReturn(true);

        int testResult = homeListComparator.compare(mockDevice, mockDevice2);

        assertTrue(testResult > 0);
    }

    @Test
    public void testListCompareEqualPaired() {
        when(mockBluetoothSystemUtils.isDevicePaired(mockDevice)).thenReturn(true);
        when(mockBluetoothSystemUtils.isDevicePaired(mockDevice2)).thenReturn(true);

        int testResult = homeListComparator.compare(mockDevice, mockDevice2);

        assertTrue(testResult == 0);
    }

    @Test
    public void testListCompareEqualNotPaired() {
        when(mockBluetoothSystemUtils.isDevicePaired(mockDevice)).thenReturn(false);
        when(mockBluetoothSystemUtils.isDevicePaired(mockDevice2)).thenReturn(false);

        int testResult = homeListComparator.compare(mockDevice, mockDevice2);

        assertTrue(testResult == 0);
    }
}

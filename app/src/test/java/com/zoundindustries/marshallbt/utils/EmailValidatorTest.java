package com.zoundindustries.marshallbt.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class EmailValidatorTest {

    @Test
    public void testCorrectEmails() {
        assertTrue(EmailValidator.isValid("some.user@domain.com"));
        assertTrue(EmailValidator.isValid("someuser@domain.com"));
        assertTrue(EmailValidator.isValid("USER@domain.com"));
        assertTrue(EmailValidator.isValid("some.user@DOMAIN.COM"));
    }

    @Test
    public void testIncorrectEmails() {
        // empty e-mail
        assertFalse(EmailValidator.isValid( ""));

        // bad domain
        assertFalse(EmailValidator.isValid( "user@domain..com"));
        assertFalse(EmailValidator.isValid( "user@domain,com"));
        assertFalse(EmailValidator.isValid( "user@domain?com"));
        assertFalse(EmailValidator.isValid( "@"));
        assertFalse(EmailValidator.isValid( "user"));

        // bad usernames
        assertFalse(EmailValidator.isValid( "@domain.com"));
        assertFalse(EmailValidator.isValid( "@@domain.com"));
        assertFalse(EmailValidator.isValid( "some user@domain.com"));
        assertFalse(EmailValidator.isValid( "some,user@domain.com"));
        assertFalse(EmailValidator.isValid( "user;@domain.com"));
        assertFalse(EmailValidator.isValid( "user<>@domain.com"));
        assertFalse(EmailValidator.isValid( "user..@domain.com"));
        assertFalse(EmailValidator.isValid( "123456789012345678901234567890123456789012345" +
                "6789012345678901234+x@@domain.com"));
    }
}
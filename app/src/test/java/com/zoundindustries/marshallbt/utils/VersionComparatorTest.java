package com.zoundindustries.marshallbt.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class VersionComparatorTest {

    @Test
    public void testCurrentVersionOlder() {
        assertTrue(VersionComparator.isNewerVersion("1", "2"));
        assertTrue(VersionComparator.isNewerVersion("1", "1.1"));
        assertTrue(VersionComparator.isNewerVersion("1", "1.0.2"));
        assertTrue(VersionComparator.isNewerVersion("0.1", "0.1.2"));
        assertTrue(VersionComparator.isNewerVersion("1.0.1", "1.0.2"));
        assertTrue(VersionComparator.isNewerVersion("1.0.1", "1.1.0"));
        assertTrue(VersionComparator.isNewerVersion("1.0.1", "2.0.0"));
        assertTrue(VersionComparator.isNewerVersion("1.0.1", "2.0"));
        assertTrue(VersionComparator.isNewerVersion("1.0.1", "2"));
        assertTrue(VersionComparator.isNewerVersion("1.25.1", "2"));
        assertTrue(VersionComparator.isNewerVersion("1.0.1.2", "2.0"));
        assertTrue(VersionComparator.isNewerVersion("1.0.1.1", "2"));
        assertTrue(VersionComparator.isNewerVersion("1.25.1.2", "2"));
        assertTrue(VersionComparator.isNewerVersion("101.26.877", "102.5.55"));
    }

    @Test
    public void testCurrentVersionNewer() {
        assertFalse(VersionComparator.isNewerVersion("", "2"));
        assertFalse(VersionComparator.isNewerVersion("1", ""));
        assertFalse(VersionComparator.isNewerVersion("1", "0"));
        assertFalse(VersionComparator.isNewerVersion("1.1", "0"));
        assertFalse(VersionComparator.isNewerVersion("1.1", "1.0"));
        assertFalse(VersionComparator.isNewerVersion("0.1.1", "0.1.0"));
        assertFalse(VersionComparator.isNewerVersion("2", "1.2.6.45"));
        assertFalse(VersionComparator.isNewerVersion("1.2.155", "1.0.205"));
    }
}

package com.zoundindustries.marshallbt.utils;

import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListUpdateCallback;

import com.zoundindustries.marshallbt.model.adapters.mock.MockSDK;
import com.zoundindustries.marshallbt.model.device.BaseDevice;
import com.zoundindustries.marshallbt.model.device.DeviceInfo;
import com.zoundindustries.marshallbt.model.device.mock.MockSpeaker;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@SuppressWarnings("unchecked")
@RunWith(JUnit4.class)
public class ListDiffCallbackTest {

    private ListDiffCallback listDiffCallback;
    private MockSDK mockSDK;
    private List<BaseDevice> oldDevices = new ArrayList<>();
    private List<BaseDevice> newDevices = new ArrayList<>();

    @Before
    public void setup() {
        listDiffCallback = new ListDiffCallback(oldDevices, newDevices);
        mockSDK = new MockSDK();
    }

    @After
    public void tearDown() {
        oldDevices.clear();
        newDevices.clear();
    }

    @Test
    public void addItemTest() {

        MockSpeaker testSpeaker = new MockSpeaker(new DeviceInfo("123456",
                DeviceInfo.DeviceType.MOCK,
                DeviceInfo.DeviceGroup.SPEAKER,
                "TestName",
                DeviceInfo.DeviceColor.BLACK),
                mockSDK);
        MockSpeaker testSpeaker2 = new MockSpeaker(new DeviceInfo( "234567",
                DeviceInfo.DeviceType.MOCK,
                DeviceInfo.DeviceGroup.SPEAKER,
                "TestName",
                DeviceInfo.DeviceColor.BLACK),
                mockSDK);

        newDevices.add(testSpeaker);
        newDevices.add(testSpeaker2);
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(listDiffCallback);
        diffResult.dispatchUpdatesTo(new TestListUpdateCallback() {
            @Override
            public void onInserted(int position, int count) {
                assertEquals(0, position);
                assertEquals(2, count);
            }
        });
    }

    @Test
    public void testRemoveItem() {

        MockSpeaker testSpeaker = new MockSpeaker(new DeviceInfo("123456",
                DeviceInfo.DeviceType.MOCK,
                DeviceInfo.DeviceGroup.SPEAKER,
                "TestName",
                DeviceInfo.DeviceColor.BLACK),
                mockSDK);
        MockSpeaker testSpeaker2 = new MockSpeaker(new DeviceInfo("234567",
                DeviceInfo.DeviceType.MOCK,
                DeviceInfo.DeviceGroup.SPEAKER,
                "TestName",
                DeviceInfo.DeviceColor.BLACK),
                mockSDK);

        oldDevices.add(testSpeaker);
        oldDevices.add(testSpeaker2);

        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(listDiffCallback);
        diffResult.dispatchUpdatesTo(new TestListUpdateCallback() {
            @Override
            public void onRemoved(int position, int count) {
                assertEquals(0, position);
                assertEquals(2, count);
            }
        });
    }

    @Test
    public void testMoveItem() {

        MockSpeaker testSpeaker = new MockSpeaker(new DeviceInfo("123456",
                DeviceInfo.DeviceType.MOCK,
                DeviceInfo.DeviceGroup.SPEAKER,
                "TestName",
                DeviceInfo.DeviceColor.BLACK),
                mockSDK);
        MockSpeaker testSpeaker2 = new MockSpeaker(new DeviceInfo("234567",
                DeviceInfo.DeviceType.MOCK,
                DeviceInfo.DeviceGroup.SPEAKER,
                "TestName",
                DeviceInfo.DeviceColor.BLACK),
                mockSDK);

        oldDevices.add(testSpeaker);
        oldDevices.add(testSpeaker2);

        newDevices.add(testSpeaker2);
        newDevices.add(testSpeaker);

        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(listDiffCallback);
        diffResult.dispatchUpdatesTo(new TestListUpdateCallback() {
            @Override
            public void onMoved(int fromPosition, int toPosition) {
                assertEquals(1, fromPosition);
                assertEquals(0, toPosition);
            }
        });
    }

    private class TestListUpdateCallback implements ListUpdateCallback {
        @Override
        public void onInserted(int position, int count) {
            assert false;
        }

        @Override
        public void onRemoved(int position, int count) {
            assert false;
        }

        @Override
        public void onMoved(int fromPosition, int toPosition) {
            assert false;
        }

        @Override
        public void onChanged(int position, int count, Object payload) {
            assert false;
        }
    }

}

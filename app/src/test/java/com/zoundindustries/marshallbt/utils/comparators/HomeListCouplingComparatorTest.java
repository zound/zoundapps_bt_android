package com.zoundindustries.marshallbt.utils.comparators;

import com.zoundindustries.marshallbt.model.ota.pojo.SpeakerInfoCache;
import com.zoundindustries.marshallbt.utils.FeaturesDefs;
import com.zoundindustries.marshallbt.viewmodels.BaseJoplinSdkViewModelTest;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HomeListCouplingComparatorTest extends BaseJoplinSdkViewModelTest {

    private HomeListCouplingComparator homeListCouplingComparator;

    private SpeakerInfoCache mockSpeakerInfoCache1;
    private SpeakerInfoCache mockSpeakerInfoCache2;

    @Before
    public void setup() {
        super.baseSetup();

        mockSpeakerInfoCache1 = mock(SpeakerInfoCache.class);
        mockSpeakerInfoCache2 = mock(SpeakerInfoCache.class);

        when(mockBaseStateController.isFeatureSupported(FeaturesDefs.COUPLING_CONNECTION))
                .thenReturn(true);
        when(mockBaseStateController2.isFeatureSupported(FeaturesDefs.COUPLING_CONNECTION))
                .thenReturn(true);

        when(mockDeviceManager.getSpeakerInfoCache(deviceId)).thenReturn(mockSpeakerInfoCache1);
        when(mockDeviceManager.getSpeakerInfoCache(deviceId2)).thenReturn(mockSpeakerInfoCache2);

        homeListCouplingComparator = new HomeListCouplingComparator(mockDeviceManager);
    }

    @Test
    public void testListCompareFirstGreater() {
        //simulate TWS values
        when(mockDeviceManager.isSpeakerInfoCached(deviceId)).thenReturn(true);
        when(mockDeviceManager.isSpeakerInfoCached(deviceId2)).thenReturn(true);
        when(mockSpeakerInfoCache1.getIsTwsCoupled()).thenReturn(true);
        when(mockSpeakerInfoCache2.getIsTwsCoupled()).thenReturn(false);

        int testResult = homeListCouplingComparator.compare(mockDevice, mockDevice2);

        assertTrue(testResult < 0);
    }

    @Test
    public void testListCompareFirstLesser() {
        //simulate TWS values
        when(mockDeviceManager.isSpeakerInfoCached(deviceId)).thenReturn(true);
        when(mockDeviceManager.isSpeakerInfoCached(deviceId2)).thenReturn(true);
        when(mockSpeakerInfoCache1.getIsTwsCoupled()).thenReturn(false);
        when(mockSpeakerInfoCache2.getIsTwsCoupled()).thenReturn(true);

        int testResult = homeListCouplingComparator.compare(mockDevice, mockDevice2);

        assertTrue(testResult > 0);
    }

    @Test
    public void testListCompareEqualNotConnected() {
        //simulate TWS values
        when(mockDeviceManager.isSpeakerInfoCached(deviceId)).thenReturn(true);
        when(mockDeviceManager.isSpeakerInfoCached(deviceId2)).thenReturn(true);
        when(mockSpeakerInfoCache1.getIsTwsCoupled()).thenReturn(false);
        when(mockSpeakerInfoCache2.getIsTwsCoupled()).thenReturn(false);

        int testResult = homeListCouplingComparator.compare(mockDevice, mockDevice2);

        assertTrue(testResult == 0);
    }

    @Test
    public void testListCompareEqualConnected() {
        //simulate TWS values
        when(mockDeviceManager.isSpeakerInfoCached(deviceId)).thenReturn(true);
        when(mockDeviceManager.isSpeakerInfoCached(deviceId2)).thenReturn(true);
        when(mockSpeakerInfoCache1.getIsTwsCoupled()).thenReturn(true);
        when(mockSpeakerInfoCache2.getIsTwsCoupled()).thenReturn(true);
        int testResult = homeListCouplingComparator.compare(mockDevice, mockDevice2);

        assertTrue(testResult == 0);
    }
}
